#!/bin/bash

#. gridpane.sh

gpclone::end::failsync_pause::exit() {
  {
    if [[ -n $1 ]]; then
      echo "$1"
    fi
    echo "gpfailsync-server-worker-pause deleted, server tasks can resume as normal..." | tee -a /var/log/gridpane.log
    echo "******************************************************************************"
    echo "******************************************************************************"
    echo " "
    gridpane::conf_delete gpfailsync-server-worker-pause
    gridpane::conf_write gpfailsync-running false
  } | tee -a /var/log/gridpane.log /opt/gridpane/gpclone.log
  exit 187
}

gpclone::failsync_pause() {
  local current_epoch_time
  current_epoch_time=$(date +%s)
  {
    echo " "
    echo "******************************************************************************"
    echo "******************************************************************************"
    echo "$1"
    echo "gpfailsync-server-worker-pause ${current_epoch_time}, server tasks will be paused..."
    gridpane::conf_write gpfailsync-server-worker-pause "${current_epoch_time}"
  } | tee -a /var/log/gridpane.log /opt/gridpane/gpclone.log
}

gpclone::check::htdocs_config() {
  local siteurl="$1"
  local serverIP_to_use

  if [[ -z $2 ]]; then
    # shellcheck disable=SC2154
    serverIP_to_use="${serverIP}"
  else
    serverIP_to_use="$2"
  fi

  if [[ -f /var/www/${siteurl}/htdocs/wp-config.php ]]; then
    notification_body="WP-Config Check <br>Site Url:${siteurl}<br>Source Server:${serverIP_to_use}<br>The htdocs folder for ${siteurl} has a wp-config.php file, exiting..."
    gridpane::notify::app \
      "Cloning/Migrate Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${siteurl}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
    gpclone::end::failsync_pause::exit "Pre-existing wp-config file"
  fi
}

gpclone::export::site() {
  if [[ -z $1 ]]; then
    gpclone::end::failsync_pause::exit "We need to know which site to export... exiting..."
  fi

  local site_to_clone=$1
  local alternate

  if [[ $2 == "alternate" ]]; then
    alternate=".$2"
  fi

  echo "Check if /var/www/${site_to_clone}/htdocs has a wp-config.php ..." | tee -a /opt/gridpane/gpclone.log
  gpclone::check::htdocs_config "${site_to_clone}"

  echo "Processing /var/www/${site_to_clone}/htdocs ..." | tee -a /opt/gridpane/gpclone.log
  cd /var/www/"${site_to_clone}"/htdocs || return

  local index_file_to_check
  if [[ -f /var/www/"${site_to_clone}"/htdocs/index.php ]]; then
    index_file_to_check="/var/www/${site_to_clone}/htdocs/index.php"
  elif [[ -f /var/www/"${site_to_clone}"/htdocs/index.php.suspend ]]; then
    index_file_to_check="/var/www/${site_to_clone}/htdocs/index.php.suspend"
  fi

  if [[ "$(cat "${index_file_to_check}")" != *"Front to the WordPress application. This file doesn't do anything"* ]]; then
    notification_body="Process Failed<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>${site_to_clone} is not a valid WordPress site... skipping"
    gridpane::notify::app \
      "Cloning/Migrate Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
  else
    if ! type mydumper >/dev/null; then
      apt-get -y install mydumper
    fi

    workers::check::site_routing "${site_to_clone}"

    echo "Exporting ${site_to_clone} database..." | tee -a /opt/gridpane/gpclone.log
    local WPDBNAME
    local WPDBUSER
    local WPDBPASS
    local WPDBHOST
    local WBDBPORT
    # shellcheck disable=SC2002
    WPDBNAME=$(cat /var/www/"${site_to_clone}"/wp-config.php | grep DB_NAME | tr \" \' | cut -d \' -f 4)
    # shellcheck disable=SC2002
    WPDBUSER=$(cat /var/www/"${site_to_clone}"/wp-config.php | grep DB_USER | tr \" \' | cut -d \' -f 4)
    # shellcheck disable=SC2002
    WPDBPASS=$(cat "/var/www/"${site_to_clone}"/wp-config.php" | grep DB_PASSWORD | tr \" \' | cut -d \' -f 4)
    WPDBHOST=$(cat "/var/www/${site_to_clone}/wp-config.php" | grep DB_HOST | tr \" \' | cut -d \' -f 4)
    WPDBPORT=$(echo ${WPDBHOST} | awk -F':' '{print $2}')
    WPDBPORT=${WPDBPORT:-3306}

    local mysql_dump_output
    local mysql_dump_output_status
    if ! type mydumper >/dev/null; then
      mysql_dump_output=$(mysqldump --no-tablespaces -u"${WPDBUSER}" -p"${WPDBPASS}" -h"${WPDBHOST}" -P"${WPDBPORT}" "${WPDBNAME}" >/var/www/"${site_to_clone}"/htdocs/database.sql)
      mysql_dump_output_status=$?
    else
      mysql_dump_output=$(
      # shellcheck disable=SC2154
      mydumper -v 3 -B "${WPDBNAME}" -h "${WPDBHOST}" -P "${WPDBPORT}" -u "${WPDBUSER}" -p "${WPDBPASS}" --lock-all-tables \
        -o  "/var/www/${site_to_clone}/htdocs/database-${clone_time_epoch}" \
        2>&1
      )
      mysql_dump_output_status=$?
    fi

    # shellcheck disable=SC2181
    if [[ "$mysql_dump_output_status" == 0 ]]; then
      {
        echo "MySQL Dump Success..."
        if ! type mydumper >/dev/null; then
          echo "${mysql_dump_output}"
        else
          grep "Started dump at:" <<<"${mysql_dump_output}"
          grep "Finished dump at:" <<<"${mysql_dump_output}"
        fi
      } | tee -a /opt/gridpane/gpclone.log
    else
      {
        echo "MySQL Dump Error..."
        echo "${mysql_dump_output}"
      } | tee -a /opt/gridpane/gpclone.log

      [[ -f /var/www/"${site_to_clone}"/htdocs/database.sql ]] &&
        rm /var/www/"${site_to_clone}"/htdocs/database.sql
      [[ -d /var/www/${site_to_clone}/htdocs/database-${clone_time_epoch} ]] &&
        rm -rf /var/www/"${site_to_clone}"/htdocs/database-"${clone_time_epoch}"
    fi

    {
      if [[ -f /var/www/"${site_to_clone}"/htdocs/database.sql ||
            -d /var/www/${site_to_clone}/htdocs/database-${clone_time_epoch} ]]; then
        echo "DB Exported..."
        # shellcheck disable=SC2012
        [[ -f /var/www/"${site_to_clone}"/htdocs/database.sql ]] &&
          ls -lh /var/www/"${site_to_clone}"/htdocs/database.sql &&
          chmod 400 /var/www/"${site_to_clone}"/htdocs/database.sql

       [[ -d /var/www/${site_to_clone}/htdocs/database-${clone_time_epoch} ]] &&
          ls -lh "/var/www/${site_to_clone}/htdocs/database-${clone_time_epoch}"

      else
        echo "Failed to export DB..."
        # shellcheck disable=SC2154
        touch /tmp/"${nonce}".failed.db.export
      fi
    } | tee -a /opt/gridpane/gpclone.log

    #Need to get the DB prefix from wp-config...
    # shellcheck disable=SC2016
    table_prefix=$(sed -n -e '/$table_prefix/p' /var/www/"${site_to_clone}"/wp-config.php)
    echo "${table_prefix}" >/var/www/"${site_to_clone}"/htdocs/table.prefix
    chmod 400 /var/www/"${site_to_clone}"/htdocs/table.prefix

    currUser=$(gridpane::get::set::site::user "${site_to_clone}")
    echo "${currUser}" >/var/www/"${site_to_clone}"/htdocs/grid.user
    chmod 400 /var/www/"${site_to_clone}"/htdocs/grid.user

    chattr -i /var/www/"${site_to_clone}"/logs/"${site_to_clone}".env
    cp /var/www/"${site_to_clone}"/logs/"${site_to_clone}".env /var/www/"${site_to_clone}"/htdocs/"${site_to_clone}".env.clone"${alternate}"

    {
      echo "--------------------------------------------------------------------"
      echo "Creating archive package of site ${site_to_clone}"
      echo "--------------------------------------------------------------------"
      echo "$PWD:"
      echo "--------------------------------------------------------------------"
      ls -l "$PWD"
      echo "--------------------------------------------------------------------"
      echo "$PWD/wp-content/plugins:"
      echo "--------------------------------------------------------------------"
      ls -l "$PWD/wp-content/plugins"
      echo "--------------------------------------------------------------------"
      echo "$PWD/wp-content/mu-plugins:"
      echo "--------------------------------------------------------------------"
      ls -l "$PWD/wp-content/mu-plugins"
      echo "--------------------------------------------------------------------"
      echo "$PWD/wp-content/themes:"
      echo "--------------------------------------------------------------------"
      ls -l "$PWD/wp-content/themes"
      echo "--------------------------------------------------------------------"
    } | tee -a /opt/gridpane/gpclone.log

    tar -cf /var/www/"${site_to_clone}"/GPBUP-"${site_to_clone}"-CLONE.gz . --exclude '*.zip' --exclude '*.gz' --exclude 'wp-config.php' |
      tee -a /opt/gridpane/gpclone.log

    {
      echo "Site archive for ${site_to_clone} created, cleaning up a bit..."
      [[ -f /var/www/"${site_to_clone}"/htdocs/database.sql ]] &&
        rm /var/www/"${site_to_clone}"/htdocs/database.sql
      [[ -d /var/www/${site_to_clone}/htdocs/database-${clone_time_epoch} ]] &&
        rm -rf /var/www/"${site_to_clone}"/htdocs/database-"${clone_time_epoch}"
      rm /var/www/"${site_to_clone}"/htdocs/table.prefix
      rm /var/www/"${site_to_clone}"/htdocs/grid.user
      rm /var/www/"${site_to_clone}"/htdocs/*.env.*
      chattr +i /var/www/"${site_to_clone}"/logs/"${site_to_clone}".env

      [[ -e "${site_to_clone}".key ]] && rm "${site_to_clone}".key && echo "Removed source backup of key..."
      [[ -e "${site_to_clone}".cert ]] && rm "${site_to_clone}".cert && echo "Removed source backup of cert..."
      [[ -e "${site_to_clone}".trust ]] && rm "${site_to_clone}".trust && echo "Removed source backup of trust..."
      # shellcheck disable=SC2154
      [[ "$webserver" = nginx && -e "${site_to_clone}".nginx ]] && rm "${site_to_clone}".nginx && echo "Removed source backup of nginx..."
      [[ -e "${site_to_clone}".renew ]] && rm "${site_to_clone}".renew && echo "Removed source backup of renewal config..."

      echo "--------------------------------------------------------------------"
      echo "Export of ${site_to_clone} Done!"
    } | tee -a /opt/gridpane/gpclone.log
    sleep 3
  fi
}

gpclone::migrate_to_remote() {
  local site_to_clone="$1"
  local new_url="$2"
  local webserver_remote
  {
    echo "--------------------------------------------------------------------"
    echo "Site to Clone: ${site_to_clone}"
    echo "New URL: ${new_url}"
    echo "Remote IP ${remote_IP}"
    echo "--------------------------------------------------------------------"
  } | tee -a /opt/gridpane/gpclone.log

  gridpane::notify::app \
    "Cloning/Migrate Notice" \
    "${new_url} is building on ${remote_IP}... waiting..." \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${new_url}" \
    "${remote_IP}"

  sleep 10

  RETRY=0
  # We can't rely on using cache plugins for nginx to mark site build success...
  # New build options allow sites with no caching == no plugin == stall!
  # Swapping to login mu plugin instead.
  webserver_remote="$(/usr/bin/ssh root@"$remote_IP" "/usr/local/bin/gp conf read webserver -q")"
  while ssh -n root@"${remote_IP}" [ ! -f /var/www/"${new_url}"/htdocs/wp-content/mu-plugins/wp-cli-login-server.php ]; do
    echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log
    echo "Waiting for remote site to completely provision... (max 300s)... ${RETRY}s" | tee -a /opt/gridpane/gpclone.log
    sleep 1

    RETRY=$((RETRY + 1))
    if [[ ${RETRY} -gt 300 ]]; then
      notification_body="Building new ${new_url} site on ${remote_IP} stalled, exiting process..."
      gridpane::notify::app \
        "Cloning/Migrate Notice" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

      gpclone::end::failsync_pause::exit "${notification_body}"
    fi
  done

  sleep 5

  gridpane::notify::app \
    "Cloning/Migrate Notice" \
    "${new_url} build on ${remote_IP} completing..." \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${site_to_clone}"

  sleep 5

  {
    scp /var/www/"${site_to_clone}"/GPBUP-"${site_to_clone}"-CLONE.gz root@"${remote_IP}":/var/www/"${new_url}"/GPBUP-"${site_to_clone}"-CLONE.gz
    echo "--------------------------------------------------------------------"
    # shellcheck disable=SC2029
    ssh root@"${remote_IP}" "sleep 1 && ls -l /var/www/${new_url}"
    echo "--------------------------------------------------------------------"
    echo "Copied local WP export file for ${site_to_clone} to /var/www/${new_url}/GPBUP-${site_to_clone}-CLONE.gz on remote system ${remote_IP}"
  } | tee -a /opt/gridpane/gpclone.log

  check_for_promethean_remote=$(ssh root@"${remote_IP}" "sleep 1 && sudo /usr/local/bin/gp conf read server-build -q" 2>&1)
  {
    echo "--------------------------------------------------------------------"
    echo "Destination is: ${check_for_promethean_remote}"
    echo "--------------------------------------------------------------------"
  } | tee -a /opt/gridpane/gpclone.log

  if [[ ${check_for_promethean_remote} == *"prometheus"* ]]; then
    if [[ -f /var/www/${site_to_clone}/user-configs.php ]]; then
      {
        echo "Sending over user wp-config.php includes..."
        scp /var/www/"${site_to_clone}"/user-configs.php root@"${remote_IP}":/var/www/"${new_url}"/user-configs.php
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local source_php_ver
    source_php_ver=$(gridpane::get::site::php "${site_to_clone}")
    local destination_php_ver
    if [[ "$webserver_remote" = nginx ]]; then
      # shellcheck disable=SC2029
      destination_php_ver=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf php -ver-check ${new_url} -q")
    elif [[ "$webserver_remote" = openlitespeed ]]; then
      # shellcheck disable=SC2029
      destination_php_ver=$(ssh root@"$remote_IP" "/usr/local/bin/gpols get ${new_url} php-ver")
    fi

    if [[ ${source_php_ver} != "${destination_php_ver}" ]]; then
      {
        echo "Adjusting PHP for ${new_url} on $remote_IP to ${source_php_ver}"
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" "/usr/local/bin/gp site ${new_url} -switch-php ${source_php_ver}"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    if [[ "$webserver" = nginx &&
          "$webserver_remote" = nginx ]]; then
      {
        echo "Syncing Nginx"
        # shellcheck disable=SC2029
        ssh root@"${remote_IP}" \
          "sleep 1 && sudo mkdir -p /var/www/${new_url}/nginx; sleep 1 && sudo rm -rf /var/www/${new_url}/nginx/*"
        /usr/bin/rsync -avz --exclude '*-sockfile.conf' /var/www/"${site_to_clone}"/nginx/* root@"${remote_IP}":/var/www/"${new_url}"/nginx
        # shellcheck disable=SC2029
        ssh root@"${remote_IP}" \
          "sleep 1 && mv /var/www/${new_url}/nginx/${site_to_clone}-headers-csp.conf /var/www/${new_url}/nginx/${new_url}-headers-csp.conf"
        if [[ ${site_to_clone} == "staging."* &&
              ${new_url} != "staging."* ]]; then
          # shellcheck disable=SC2029
          ssh root@"${remote_IP}" "sleep 1 && [[ -f /var/www/${new_url}/nginx/x-robots-noindex-main-context.conf ]] && echo \"removing robots conf\" && rm /var/www/${new_url}/nginx/x-robots-noindex-main-context.conf"
        fi
        echo "--------------------------------------------------------------------"
      } | tee -a /opt/gridpane/gpclone.log

    elif [[ "$webserver" = openlitespeed &&
          "$webserver_remote" = openlitespeed ]]; then
        {
         # shellcheck disable=SC2029
          ssh root@"${remote_IP}" "sudo mkdir -p /var/www/${new_url}/ols; sudo rm -rf /var/www/${new_url}/ols/*"
          /usr/bin/rsync -avz /var/www/"${site_to_clone}"/ols/ root@"${remote_IP}":/var/www/"${new_url}"/ols
        } | tee -a /opt/gridpane/gpclone.log
    fi
  fi

  if [[ "$webserver" = nginx ]]; then
    http_or_https=$(/usr/local/bin/gp conf -ngx type-check "${site_to_clone}" -q)
  else
    http_or_https="$(/usr/local/bin/gpols get "$site_to_clone" proto)"
  fi

  if [[ ${http_or_https} == *"https"* ]]; then
    {
      echo "Origin is HTTPS, will send along SSL request..."
      echo "--------------------------------------------------------------------"
    } | tee -a /opt/gridpane/gpclone.log
    scheme="https"
  else
    scheme="http"
  fi

  notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${new_url}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>${site_to_clone} Clone package migrated to destination..."
  gridpane::notify::app \
    "Cloning/Migrate Notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${site_to_clone}"
  echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

  rm /var/www/"${site_to_clone}"/GPBUP-"${site_to_clone}"-CLONE.gz
  # shellcheck disable=SC2029
  ssh root@"$remote_IP" "touch /var/www/${new_url}/logs/server.clone"

  local cloning_site_cache
  local cloning_site_config
  local cloning_site_object_caching
  if [[ "$webserver" = nginx  ]]; then
    cloning_site_cache="$(gridpane::check::site::nginx::cache "$site_to_clone")"
    cloning_site_config="$(gridpane::check::site::nginx::config "$site_to_clone")"
    cloning_site_object_caching=$(sudo su - "${local_sys_user}" -c "timeout 30 /usr/local/bin/wp redis status --path=/var/www/${site_to_clone}/htdocs 2>&1")
  else
    cloning_site_cache="$(/usr/local/bin/gpols get "$site_to_clone" cache)"
    cloning_site_config="$(/usr/local/bin/gpols get "$site_to_clone" vhconf)"
    cloning_site_object_caching="$(gridpane::conf_read ols-redis-cache -site.env "$site_to_clone")"
  fi

  local remote_cloning_site_cache
  local remote_cloning_site_config
  local remote_cloning_site_object_caching
  if [[ "$webserver_remote" = nginx  ]]; then
    # shellcheck disable=SC2029
    remote_cloning_site_cache="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx cache-check ${new_url} -q")"
    [[ $remote_cloning_site_cache != "fastcgi" && $remote_cloning_site_cache != "redis"  ]] &&
      remote_cloning_site_cache="off"
    # shellcheck disable=SC2029
    remote_cloning_site_config="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx type-check ${new_url} -q")"
    # shellcheck disable=SC2029
    remote_cloning_site_object_caching=$(ssh root@"$remote_IP" "/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp redis status --path=/var/www/${new_url}/htdocs 2>&1\"")
  else
    # shellcheck disable=SC2029
    remote_cloning_site_cache="$(ssh root@"$remote_IP" "/usr/local/bin/gpols get ${new_url} cache")"
    # shellcheck disable=SC2029
    remote_cloning_site_config="$(ssh root@"$remote_IP" "/usr/local/bin/gpols get ${new_url} vhconf")"
    # shellcheck disable=SC2029
    remote_cloning_site_object_caching="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read ols-redis-cache -site.env ${new_url} -q")"
  fi

  local remote_sys_user
  local local_sys_user
  # shellcheck disable=SC2029
  remote_sys_user="$(ssh root@"$remote_IP" "/bin/grep sys-user: /var/www/${new_url}/logs/${new_url}.env" | /usr/bin/awk -F '[:]' '{print $2}')"
  local_sys_user="$(gridpane::get::set::site::user "$site_to_clone")"

  if [[ "$webserver" = nginx &&
        "$webserver_remote" = nginx ]]; then

    # shellcheck disable=SC2029
    destination_php_ver=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf php -ver-check ${new_url} -q")
    local phpFileVer
    phpFileVer=${destination_php_ver/./}
    local sourcePhpFileVer
    sourcePhpFileVer=${source_php_ver/./}

    local proceed_with_fpm
    case $destination_php_ver in
      7.1|7.2|7.3|7.4) echo "Destination PHP Version valid: $destination_php_ver" | tee -a /opt/gridpane/gpclone.log ;;
      *)
        echo "Destination PHP Version not valid: $destination_php_ver" | tee -a /opt/gridpane/gpclone.log
        proceed_with_fpm="false"
        ;;
    esac

    case $phpFileVer in
      71|72|73|74) echo "Destination PHP Sock Version valid: $phpFileVer" | tee -a /opt/gridpane/gpclone.log ;;
      *)
        echo "Destination PHP Sock Version not valid: $phpFileVer" | tee -a /opt/gridpane/gpclone.log
        proceed_with_fpm="false"
        ;;
    esac

    case $sourcePhpFileVer in
      71|72|73|74) echo "Source PHP Sock Version valid: $sourcePhpFileVer" | tee -a /opt/gridpane/gpclone.log ;;
      *)
        echo "Source PHP Sock Version not valid: $sourcePhpFileVer" | tee -a /opt/gridpane/gpclone.log
        proceed_with_fpm="false"
        ;;
    esac

    if [[ $(cat /etc/passwd) != *"/home/${local_sys_user}:"* ]]; then
      echo "Local System User not Valid: ${local_sys_user}" | tee -a /opt/gridpane/gpclone.log
      proceed_with_fpm="false"
    else
      echo "Local System User Valid: ${local_sys_user}" | tee -a /opt/gridpane/gpclone.log
    fi

    local check_for_remote_user
    # shellcheck disable=SC2029
    check_for_remote_user=$(ssh root@"$remote_IP" "[[ \$(cat /etc/passwd) == *\"/home/${remote_sys_user}:\"* ]] && echo \"true\"")
    if [[ ${check_for_remote_user} != *"true"* ]]; then
      echo "Remote System User not Valid: ${remote_sys_user}" | tee -a /opt/gridpane/gpclone.log
      proceed_with_fpm="false"
    else
      echo "Remote System User Valid: ${remote_sys_user}" | tee -a /opt/gridpane/gpclone.log
    fi

    if [[ ${proceed_with_fpm} != *"false"* ]]; then
      {
        echo "remote user: ${check_for_remote_user}"
        scp /etc/php/"${source_php_ver}"/fpm/pool.d/"${site_to_clone}".conf root@"${remote_IP}":/etc/php/"${destination_php_ver}"/fpm/pool.d/"${new_url}".conf
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "sed -i \"s/'${site_to_clone}${sourcePhpFileVer}'/'${new_url}${phpFileVer}'/g\" /etc/php/${destination_php_ver}/fpm/pool.d/${new_url}.conf; \
          sed -i \"s/\[${site_to_clone}${sourcePhpFileVer}\]/\[${new_url}${phpFileVer}\]/g\" /etc/php/${destination_php_ver}/fpm/pool.d/${new_url}.conf; \
          sed -i \"/user = ${local_sys_user}/c\user = ${remote_sys_user}\" /etc/php/${destination_php_ver}/fpm/pool.d/${new_url}.conf; \
          sed -i \"/group = ${local_sys_user}/c\group = ${remote_sys_user}\" /etc/php/${destination_php_ver}/fpm/pool.d/${new_url}.conf; \
          sed -i \"/listen = \/var/c\listen = \/var\/run\/php\/php${phpFileVer}-fpm-${new_url}.sock\" /etc/php/${destination_php_ver}/fpm/pool.d/${new_url}.conf"
        echo "-------------------------------------------------------------------------------------"
        echo "Destination /etc/php/${destination_php_ver}/fpm/pool.d/${new_url}.conf"
        echo "-------------------------------------------------------------------------------------"
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" "head -70 /etc/php/${destination_php_ver}/fpm/pool.d/${new_url}.conf"
        echo "-------------------------------------------------------------------------------------"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    [[ $cloning_site_cache != "fastcgi" &&
      $cloning_site_cache != "redis"  ]] &&
      cloning_site_cache="off"
    {
      local command_sequence
      # shellcheck disable=SC2029
      if [[ "$cloning_site_config" == *"wildcard"* && "$remote_cloning_site_config" != *"wildcard"* ]]; then
        command_sequence="/usr/local/bin/gp site ${new_url} -create-wildcard;"
      fi

      if [[ "$cloning_site_config" == *"www"* && "$remote_cloning_site_config" != *"www"* ]]; then
        command_sequence="${command_sequence} /usr/local/bin/gp site ${new_url} -route-domain-www;"
      elif [[ "$cloning_site_config" == *"root"* && "$remote_cloning_site_config" != *"root"* ]]; then
        command_sequence="${command_sequence} /usr/local/bin/gp site ${new_url} -route-domain-root;"
      fi

      if [[ "$cloning_site_cache" != "$remote_cloning_site_cache" ]]; then
        command_sequence="${command_sequence} /usr/local/bin/gp site $new_url -caching=$cloning_site_cache;"
      fi

      # shellcheck disable=SC2029
      [[ -n ${command_sequence} ]] &&
        ssh root@"$remote_IP" "${command_sequence}"

      # shellcheck disable=SC2029
      if [[ "$cloning_site_object_caching" == *"Status: Connected"* ]]; then
        ssh root@"$remote_IP" "/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp redis enable --path=/var/www/${new_url}/htdocs\""
      else
        ssh root@"$remote_IP" "/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp redis disable --path=/var/www/${new_url}/htdocs\""
      fi
    } | tee -a /opt/gridpane/gpclone.log

    local source_waf_type
    local destination_waf_type
    source_waf_type=$(gridpane::conf_read waf -site.env "${site_to_clone}")
    # shellcheck disable=SC2029
    destination_waf_type=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read waf -site.env ${new_url} -q")
    command_sequence=""
    if [[ ${source_waf_type} == *"modsec"* &&
          ${destination_waf_type} != *"modsec"* ]]; then
      waf::sync::site_to_app "${site_to_clone}" "modsec"
      if [[ ${destination_waf_type} == *"6"* ]]; then
        command_sequence="/usr/local/bin/gp site ${new_url} -6g-off;"
      elif [[ ${destination_waf_type} == *"7"* ]]; then
        command_sequence="/usr/local/bin/gp site ${new_url} -7g-off;"
      fi
      command_sequence="${command_sequence} /usr/local/bin/gp site ${new_url} -modsec-on;"
    elif [[ ${source_waf_type} == "6G" || ${source_waf_type} == "6g" ]] &&
        [[ ${destination_waf_type} != "6G" && ${destination_waf_type} != "6g" ]]; then
      waf::sync::site_to_app "${site_to_clone}" "6g"
      if [[ ${destination_waf_type} == *"modsec"* ]]; then
        command_sequence="/usr/local/bin/gp site ${new_url} -modsec-off;"
      elif [[ ${destination_waf_type} == *"7"* ]]; then
        command_sequence="/usr/local/bin/gp site ${new_url} -7g-off;"
      fi
      command_sequence="${command_sequence} /usr/local/bin/gp site ${new_url} -6g-on;"
    elif [[ ${source_waf_type} == "7G" || ${source_waf_type} == "7g" ]] &&
        [[ ${destination_waf_type} != "7G" && ${destination_waf_type} != "7g" ]]; then
      waf::sync::site_to_app "${site_to_clone}" "7g"
      if [[ ${destination_waf_type} == *"modsec"* ]]; then
        command_sequence="/usr/local/bin/gp site ${new_url} -modsec-off;"
      elif [[ ${destination_waf_type} == *"6"* ]]; then
        command_sequence="/usr/local/bin/gp site ${new_url} -6g-off;"
      fi
      command_sequence="${command_sequence} /usr/local/bin/gp site ${new_url} -7g-on;"
    fi

     # shellcheck disable=SC2029
    [[ -n ${command_sequence} ]] &&
      ssh root@"$remote_IP" "${command_sequence}"

    source_waf_type=$(gridpane::conf_read waf -site.env "${site_to_clone}")
    # shellcheck disable=SC2029
    destination_waf_type=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read waf -site.env ${new_url} -q")
    if [[ ${source_waf_type} == *"modsec"* &&
          ${destination_waf_type} == *"modsec"* ]]; then
      scp /var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-crs-anomaly-blocking-threshold.conf \
        root@"${remote_IP}":/var/www/"${new_url}"/modsec/"${new_url}"-crs-anomaly-blocking-threshold.conf
      scp /var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-crs-paranoia-level.conf \
        root@"${remote_IP}":/var/www/"${new_url}"/modsec/"${new_url}"-crs-paranoia-level.conf
      scp /var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-runtime-exceptions.conf \
        root@"${remote_IP}":/var/www/"${new_url}"/modsec/"${new_url}"-runtime-exceptions.conf
      scp /var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-runtime-whitelist.conf \
        root@"${remote_IP}":/var/www/"${new_url}"/modsec/"${new_url}"-runtime-whitelist.conf
    elif [[ ${source_waf_type} == "6G" || ${source_waf_type} == "6g" ]] &&
        [[ ${destination_waf_type} == "6G" || ${destination_waf_type} == "6g" ]]; then
      cp /etc/nginx/common/"${site_to_clone}"-6g.conf /etc/nginx/common/"${new_url}"-6g.conf.temp
      sed -i "/if=\${drop_log}/c \access_log \/var\/www\/${new_url}\/logs\/6g.log 6g_log if=\${drop_log};" /etc/nginx/common/"${new_url}"-6g.conf.temp
      sed -i "/*-6g-context.conf/c \include \/var\/www\/${new_url}\/nginx\/\*-6g-context.conf;" /etc/nginx/common/"${new_url}"-6g.conf.temp
      scp /etc/nginx/common/"${new_url}"-6g.conf.temp root@"${remote_IP}":/etc/nginx/common/"${new_url}"-6g.conf
      rm /etc/nginx/common/"${new_url}"-6g.conf.temp
    elif [[ ${source_waf_type} == "7G" || ${source_waf_type} == "7g" ]] &&
        [[ ${destination_waf_type} == "7G" || ${destination_waf_type} == "7g" ]]; then
      cp /etc/nginx/common/"${site_to_clone}"-7g.conf /etc/nginx/common/"${new_url}"-7g.conf.temp
      sed -i "/if=\${7g_drop}/c \access_log \/var\/www\/${new_url}\/logs\/7g.log 7g_log if=\${7g_drop};" /etc/nginx/common/"${new_url}"-7g.conf.temp
      sed -i "/*-7g-context.conf/c \include \/var\/www\/${new_url}\/nginx\/\*-7g-context.conf;" /etc/nginx/common/"${new_url}"-7g.conf.temp
      scp /etc/nginx/common/"${new_url}"-7g.conf.temp root@"${remote_IP}":/etc/nginx/common/"${new_url}"-7g.conf
      rm /etc/nginx/common/"${new_url}"-7g.conf.temp
    fi

    waf::sync::site_to_app "${site_to_clone}" "wpf2b"
    local site_wpfail2ban
    site_wpfail2ban=$(gridpane::conf_read wp-fail2ban -site.env "${site_to_clone}")
    local remote_site_wpfail2ban
    # shellcheck disable=SC2029
    remote_site_wpfail2ban=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read wp-fail2ban -site.env ${new_url} -q")
    if [[ ${site_wpfail2ban} == *"rue"* || ${site_wpfail2ban} == *"on"* ]] &&
      [[ ${remote_site_wpfail2ban} != *"rue"* && ${remote_site_wpfail2ban} != *"on"* ]]; then
      {
        echo "----------------------------------------------------------------------"
        echo "Enabling ${new_url} wpfail2ban configurations on $remote_IP"
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" "/usr/local/bin/gp site ${new_url} -enable-wp-fail2ban"
        sleep 3
      } | tee -a /opt/gridpane/gpclone.log

      local source_fail2ban_user_enumeration
      source_fail2ban_user_enumeration=$(grep "WP_FAIL2BAN_BLOCK_USER_ENUMERATION" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
      if [[ -n $source_fail2ban_user_enumeration ]]; then
        {
          # shellcheck disable=SC2029
          ssh root@"$remote_IP" \
            "/bin/sed -i \"/WP_FAIL2BAN_BLOCK_USER_ENUMERATION/d\" /var/www/${new_url}/wp-fail2ban-configs.php; \
            /bin/echo \"$source_fail2ban_user_enumeration\" | /usr/bin/tee -a /var/www/${new_url}/wp-fail2ban-configs.php"
        } | tee -a /opt/gridpane/gpclone.log
      fi

      local source_fail2ban_blocked_users
      source_fail2ban_blocked_users=$(grep "WP_FAIL2BAN_BLOCKED_USERS" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
      if [[ -n $source_fail2ban_blocked_users ]]; then
        {
          # shellcheck disable=SC2029
          ssh root@"$remote_IP" \
            "/bin/sed -i \"/WP_FAIL2BAN_BLOCKED_USERS/d\" /var/www/${new_url}/wp-fail2ban-configs.php; \
            /bin/echo \"$source_fail2ban_blocked_users\" | /usr/bin/tee -a /var/www/${new_url}/wp-fail2ban-configs.php"
        } | tee -a /opt/gridpane/gpclone.log
      fi

      local source_fail2ban_log_comments
      source_fail2ban_log_comments=$(grep "WP_FAIL2BAN_LOG_COMMENTS'" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
      if [[ -n $source_fail2ban_log_comments ]]; then
        {
          # shellcheck disable=SC2029
          ssh root@"$remote_IP" \
            "/bin/sed -i \"/WP_FAIL2BAN_LOG_COMMENTS'/d\" /var/www/${new_url}/wp-fail2ban-configs.php; \
            /bin/echo \"$source_fail2ban_log_comments\" | /usr/bin/tee -a /var/www/${new_url}/wp-fail2ban-configs.php"
        } | tee -a /opt/gridpane/gpclone.log
      fi

      local source_fail2ban_log_comments_extra
      source_fail2ban_log_comments_extra=$(grep "WP_FAIL2BAN_LOG_COMMENTS_EXTRA" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
      if [[ -n $source_fail2ban_log_comments_extra ]]; then
        {
          # shellcheck disable=SC2029
          ssh root@"$remote_IP" \
            "/bin/sed -i \"/WP_FAIL2BAN_LOG_COMMENTS_EXTRA/d\" /var/www/${new_url}/wp-fail2ban-configs.php; \
            /bin/echo \"$source_fail2ban_log_comments_extra\" | /usr/bin/tee -a /var/www/${new_url}/wp-fail2ban-configs.php"
        } | tee -a /opt/gridpane/gpclone.log
      fi

      local source_fail2ban_log_password_request
      source_fail2ban_log_password_request=$(grep "WP_FAIL2BAN_LOG_PASSWORD_REQUEST" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
      if [[ -n $source_fail2ban_log_password_request ]]; then
        {
          # shellcheck disable=SC2029
          ssh root@"$remote_IP" \
            "/bin/sed -i \"/WP_FAIL2BAN_LOG_PASSWORD_REQUEST/d\" /var/www/${new_url}/wp-fail2ban-configs.php; \
            /bin/echo \"$source_fail2ban_log_password_request\" | /usr/bin/tee -a /var/www/${new_url}/wp-fail2ban-configs.php"
        } | tee -a /opt/gridpane/gpclone.log
      fi

      local source_fail2ban_log_pingbacks
      source_fail2ban_log_pingbacks=$(grep "WP_FAIL2BAN_LOG_PINGBACKS" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
      if [[ -n $source_fail2ban_log_pingbacks ]]; then
        {
          # shellcheck disable=SC2029
          ssh root@"$remote_IP" \
            "/bin/sed -i \"/WP_FAIL2BAN_LOG_PINGBACKS/d\" /var/www/${new_url}/wp-fail2ban-configs.php; \
            /bin/echo \"$source_fail2ban_log_pingbacks\" | /usr/bin/tee -a /var/www/${new_url}/wp-fail2ban-configs.php"
        } | tee -a /opt/gridpane/gpclone.log
      fi

      local source_fail2ban_log_spam
      source_fail2ban_log_spam=$(grep "WP_FAIL2BAN_LOG_SPAM" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
      if [[ -n $source_fail2ban_log_spam ]]; then
        {
          # shellcheck disable=SC2029
          ssh root@"$remote_IP" \
            "/bin/sed -i \"/WP_FAIL2BAN_LOG_SPAM/d\" /var/www/${new_url}/wp-fail2ban-configs.php; \
            /bin/echo \"$source_fail2ban_log_spam\" | /usr/bin/tee -a /var/www/${new_url}/wp-fail2ban-configs.php"
        } | tee -a /opt/gridpane/gpclone.log
      fi
    fi

    local http_auth
    http_auth=$(gridpane::conf_read http-auth -site.env "${site_to_clone}")
    local remote_http_auth
    # shellcheck disable=SC2029
    remote_http_auth=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read http-auth -site.env ${new_url} -q")
    if [[ ${http_auth} == "true" || ${http_auth} == "on" ]] &&
      [[ ${remote_http_auth} != "true" && ${remote_http_auth} != "on" ]]; then
      {
        echo "----------------------------------------------------------------------"
        echo "HTTP Auth for ${new_url} on $remote_IP"
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" "/usr/local/bin/gp site ${new_url} -http-auth"
        echo "--------------------------------------------------------------------"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local smtp
    smtp=$(gridpane::conf_read smtp -site.env "${site_to_clone}")
    local remote_smtp
    # shellcheck disable=SC2029
    remote_smtp=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read smtp -site.env ${new_url} -q")
    if [[ ${smtp} == "true" || ${smtp} == "on" ]] &&
      [[ ${remote_smtp} != "true" && ${remote_smtp} != "on" ]]; then
      if [[ -f /var/www/"${site_to_clone}"/sendgrid-wp-configs.php ]]; then
        {
          echo "----------------------------------------------------------------------"
          echo "Enabling SMTP for ${new_url} on $remote_IP"
          local api_key
          api_key=$(grep "SMTP_PASS" /var/www/"${site_to_clone}"/sendgrid-wp-configs.php)
          api_key=${api_key//\"/}
          api_key=${api_key// /}
          api_key=${api_key#define(SMTP_PASS,}
          api_key=${api_key%');'}

#          ssh root@"$remote_IP" "/usr/local/bin/gp site ${new_url} -mailer enable SendGrid \"${api_key}\" simple"
          echo "--------------------------------------------------------------------"
        } | tee -a /opt/gridpane/gpclone.log
      fi
    fi

    if [[ $3 == "true"* ]]; then
      if [[ $3 == *"subdomain" ]]; then
        multisite_type="-multisubdom"
      elif [[ $3 == *"subdirectory" ]]; then
        multisite_type="-multisubdir"
      fi
      echo "Converting ${remote_IP} ${new_url} to multisite $3..." | tee -a /opt/gridpane/gpclone.log
      # shellcheck disable=SC2029
      ssh root@"${remote_IP}" "sleep 1 && /usr/local/bin/gp site ${new_url} ${multisite_type}" | tee -a /opt/gridpane/gpclone.log
    fi

  fi

  # shellcheck disable=SC2029
  ssh root@"${remote_IP}" "sleep 1 && cd /var/www/${new_url}/htdocs && sudo /usr/local/bin/gprestore -new-url ${site_to_clone} ${new_url} ${scheme}" | tee -a /opt/gridpane/gpclone.log

  if [[ "$webserver" = nginx &&
        "$webserver_remote" = openlitespeed ]]; then

    local command_sequence

    # Cache
    case "$cloning_site_cache" in
      redis)
        remote_site_cache=redis-page
      ;;
      fastcgi)
        remote_site_cache=page-only
      ;;
      php|off)
        remote_site_cache=off
      ;;
      *)
        remote_site_cache=off
    esac

    # touch /var/www/${site_to_clone}/logs/cross.server-type.clone - stop mismatching envs causing cache functions running erroneously in gprestore
    # shellcheck disable=SC2029
    command_sequence+="[[ -d /var/www/${new_url}/htdocs/wp-content/plugins/gridpane-redis-object-cache ]] && /bin/rm -rf /var/www/${new_url}/htdocs/wp-content/plugins/gridpane-redis-object-cache;"
    command_sequence+="[[ -d /var/www/${new_url}/htdocs/wp-content/plugins/nginx-helper ]] && /bin/rm -rf /var/www/${new_url}/htdocs/wp-content/plugins/nginx-helper;"
    command_sequence+="/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp plugin install litespeed-cache --activate --path=/var/www/${new_url}/htdocs\";"
    command_sequence+="/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp rewrite structure '/%category%/%postname%/' --path=/var/www/${new_url}/htdocs\";"
    command_sequence+="/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp option update litespeed.conf.purge-post_all 1 --path=/var/www/${new_url}/htdocs\";"
    command_sequence+="/usr/local/bin/gp site ${new_url} -caching=$remote_site_cache;"

    # 6G/7G WAF sync
    local source_waf_type
    local local_nginx_conf
    local local_nginx_6g_conf
    source_waf_type="$(gridpane::conf_read waf -site.env "${site_to_clone}" -q)"

    if [[ "$source_waf_type" = 6G || "$source_waf_type" = 6g ]]; then
      local_nginx_6g_conf="$(/bin/cat < /etc/nginx/common/"$site_to_clone"-6g.conf)"
      command_sequence+="/usr/local/bin/gp site ${new_url} 6g off;"
      source_waf_type=7G
    fi

    if [[ "$source_waf_type" = 7G || "$source_waf_type" = 7g ]]; then
      local_nginx_conf="$([[ -f /etc/nginx/common/"$site_to_clone"-7g.conf ]] && /bin/cat < /etc/nginx/common/"$site_to_clone"-7g.conf)"
      command_sequence+="/usr/local/bin/gp site ${new_url} 7g on;"

      [[ "$local_nginx_6g_conf" == *"#if (\$bad_bot)"* ||
         "$local_nginx_conf" == *"#if (\$bad_bot_7g)"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-bots off;"

      [[ "$local_nginx_6g_conf" == *"#if (\$not_allowed_method)"* ||
         "$local_nginx_conf" == *"#if (\$not_allowed_method_7g)"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-methods off;"

      [[ "$local_nginx_6g_conf" == *"#if (\$bad_querystring)"* ||
         "$local_nginx_conf" == *"#if (\$bad_querystring_7g)"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-query-string off;"

      [[ "$local_nginx_6g_conf" == *"#if (\$bad_referer)"* ||
         "$local_nginx_conf" == *"#if (\$bad_referer_7g)"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-referer off;"

      [[ "$local_nginx_6g_conf" == *"#if (\$bad_request)"* ||
         "$local_nginx_conf" == *"#if (\$bad_request_7g)"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-request off;"
    fi

    # modsec
    local site_modsec_paranoia
    local site_modsec_threshold

    if [[ "$source_waf_type" = modsec ]]; then
      site_modsec_paranoia="$(gridpane::conf_read modsec-paranoia -site.env "$site_to_clone")"
      site_modsec_threshold="$(gridpane::conf_read modsec-threshold -site.env "$site_to_clone")"
      command_sequence+="/usr/local/bin/gp site $new_url modsec -paranoia-level $site_modsec_paranoia;"
      command_sequence+="/usr/local/bin/gp site $new_url modsec -anomaly-threshold $site_modsec_threshold;"
    fi

    # Domains
    local site_route

    [[ "$cloning_site_config" == *"wildcard"* ]] &&
      command_sequence+="/usr/local/bin/gp site ${new_url} -create-wildcard;"

    site_route="$(gridpane::conf_read route -site.env "$site_to_clone")"
    [[ "$site_route" = www || "$site_route" = root ]] && command_sequence+="/usr/local/bin/gp site ${new_url} -route-domain-${site_route};"

    # shellcheck disable=SC2029
    ssh root@"$remote_IP" "${command_sequence}" | tee -a /opt/gridpane/gpclone.log

  elif [[ "$webserver" = openlitespeed &&
        "$webserver_remote" = openlitespeed ]]; then
    local command_sequence

    # Cache
    [[ "$cloning_site_cache" != off ]] &&
      command_sequence+="/usr/local/bin/gp site $new_url -caching=$cloning_site_cache;"

    # 7G WAF sync
    local source_waf_type
    source_waf_type="$(gridpane::conf_read waf -site.env "${site_to_clone}" -q)"

    if [[ "$source_waf_type" = 7G || "$source_waf_type" = 7g ]]; then
      [[ "$cloning_site_config" != *"# 7G:[USER AGENT]"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-bots off;"

      [[ "$cloning_site_config" != *"# 7G:[REQUEST METHOD]"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-methods off;"

      [[ "$cloning_site_config" != *"# 7G:[QUERY STRING]"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-query-string off;"

      [[ "$cloning_site_config" != *"# 7G:[HTTP REFERRER]"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-referer off;"

      [[ "$cloning_site_config" != *"# 7G:[REQUEST URI]"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-request off;"

      [[ "$cloning_site_config" != *"# 7G:[REMOTE HOST]"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-remote-host off;"
    fi

    # modsec
    local site_modsec_paranoia
    local site_modsec_threshold

    if [[ "$source_waf_type" = modsec ]]; then
      site_modsec_paranoia="$(/usr/local/bin/gpols get "$site_to_clone" modsec-paranoia)"
      site_modsec_threshold="$(/usr/local/bin/gpols get "$site_to_clone" modsec-threshold)"
      command_sequence+="/usr/local/bin/gp site $new_url modsec -paranoia-level $site_modsec_paranoia;"
      command_sequence+="/usr/local/bin/gp site $new_url modsec -anomaly-threshold $site_modsec_threshold;"
    fi

    # Sync LSAPI
    command_sequence+="/usr/local/bin/gp stack ols -site-lsapi $(gridpane::conf_read lsapi -site.env "$site_to_clone") $new_url -no-reload;"
    command_sequence+="/usr/local/bin/gp stack ols -site-lsapi-children $(gridpane::conf_read lsapi_children -site.env "$site_to_clone") $new_url -no-reload;"
    command_sequence+="/usr/local/bin/gp stack ols -site-lsapi-app-instances $(gridpane::conf_read lsapi_app_instances -site.env "$site_to_clone") $new_url -no-reload;"
    command_sequence+="/usr/local/bin/gp stack ols -site-lsapi-max-connections $(gridpane::conf_read lsapi_max_connections -site.env "$site_to_clone") $new_url -no-reload;"
    command_sequence+="/usr/local/bin/gp stack ols -site-lsapi-initial-request-timeout $(gridpane::conf_read lsapi_initial_request_timeout -site.env "$site_to_clone") $new_url -no-reload;"
    command_sequence+="/usr/local/bin/gp stack ols -site-lsapi-retry-timeout $(gridpane::conf_read lsapi_retry_timeout -site.env "$site_to_clone") $new_url -no-reload;"
    command_sequence+="/usr/local/bin/gp stack ols -site-lsapi-max-reqs $(gridpane::conf_read lsapi_max_reqs -site.env "$site_to_clone") $new_url -no-reload;"
    command_sequence+="/usr/local/bin/gp stack ols -site-lsapi-max-idle $(gridpane::conf_read lsapi_max_idle -site.env "$site_to_clone") $new_url;"

    # Domains
    local site_route

    [[ "$cloning_site_config" == *"wildcard-http"* ]] &&
      command_sequence+="/usr/local/bin/gp site ${new_url} -create-wildcard;"

    site_route="$(/usr/local/bin/gpols get "$site_to_clone" route)"
    [[ "$site_route" = www || "$site_route" = root ]] && command_sequence+="/usr/local/bin/gp site ${new_url} -route-domain-${site_route};"

    # shellcheck disable=SC2029
    ssh root@"$remote_IP" "$command_sequence" | tee -a /opt/gridpane/gpclone.log

  elif [[ "$webserver" = openlitespeed &&
        "$webserver_remote" = nginx ]]; then

    local command_sequence

    # Cache
    case "$cloning_site_cache" in
      redis|redis-only|redis-page)
        remote_site_cache=redis
      ;;
      redis-off|page-off|php|off)
        remote_site_cache=off
      ;;
      page|page-only)
        remote_site_cache=fastcgi
      ;;
      *)
        remote_site_cache=off
      ;;
    esac

    # touch /var/www/${site_to_clone}/logs/cross.server-type.clone - stop mismatching envs causing cache functions running erroneously in gprestore
    # shellcheck disable=SC2029
    command_sequence+="[[ -d /var/www/${new_url}/htdocs/wp-content/plugins/litespeed-cache ]] && /bin/rm -rf /var/www/${new_url}/htdocs/wp-content/plugins/litespeed-cache;"
    command_sequence+="/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp plugin install nginx-helper --activate --path=/var/www/${new_url}/htdocs\";"
    command_sequence+="/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp plugin install https://github.com/gridpane/gridpane-redis-object-cache/archive/master.zip --activate --path=/var/www/${new_url}/htdocs\";"
    command_sequence+="/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp redis enable --path=/var/www/${new_url}/htdocs\";"
    command_sequence+="/usr/local/bin/gp site ${new_url} -caching=$remote_site_cache;"

    # 7G WAF sync
    local source_waf_type
    source_waf_type="$(gridpane::conf_read waf -site.env "${site_to_clone}" -q)"

    if [[ "$source_waf_type" = 7G || "$source_waf_type" = 7g ]]; then
      [[ "$cloning_site_config" != *"# 7G:[USER AGENT]"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-bots off;"

      [[ "$cloning_site_config" != *"# 7G:[REQUEST METHOD]"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-methods off;"

      [[ "$cloning_site_config" != *"# 7G:[QUERY STRING]"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-query-string off;"

      [[ "$cloning_site_config" != *"# 7G:[HTTP REFERRER]"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-referer off;"

      [[ "$cloning_site_config" != *"# 7G:[REQUEST URI]"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${new_url} 7g -bad-request off;"
    fi

    # modsec
    local site_modsec_paranoia
    local site_modsec_threshold

    if [[ "$source_waf_type" = modsec ]]; then
      site_modsec_paranoia="$(/usr/local/bin/gpols get "$site_to_clone" modsec-paranoia)"
      site_modsec_threshold="$(/usr/local/bin/gpols get "$site_to_clone" modsec-threshold)"
      command_sequence+="/usr/local/bin/gp site $new_url modsec -paranoia-level $site_modsec_paranoia;"
      command_sequence+="/usr/local/bin/gp site $new_url modsec -anomaly-threshold $site_modsec_threshold;"
    fi

    # Domains
    local site_route

    [[ "$cloning_site_config" == *"wildcard-http"* ]] &&
      command_sequence+="/usr/local/bin/gp site ${new_url} -create-wildcard;"

    site_route="$(/usr/local/bin/gpols get "$site_to_clone" route)"
    [[ "$site_route" = www || "$site_route" = root ]] && command_sequence+="/usr/local/bin/gp site ${new_url} -route-domain-${site_route};"

    ssh root@"$remote_IP" "$command_sequence" | tee -a /opt/gridpane/gpclone.log

  fi

  echo "Successfully configured $new_url to remote $webserver_remote server." | tee -a /opt/gridpane/gpclone.log

  notification_body="Process Complete<br>Clone/Migration to $new_url is finished! Please disable any caching and check."
  gridpane::notify::app \
    "Cloning/Migrate Notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-clone" \
    "infinity" \
    "${new_url}" \
    "$remote_IP"
}

gpclone::clone_to_remote() {
  if [[ -z $1 ]]; then
    gpclone::end::failsync_pause::exit "We need the original site url passing... exiting"
  fi

  site_to_clone="$1"
  local webserver_remote
  {
    echo "--------------------------------------------------------------------"
    echo "Site to Clone: ${site_to_clone}"
    echo "Remote IP ${remote_IP}"
    if [[ $2 == "site-clone" ]]; then
      echo "Single Site Clone"
    else
      echo "Failover Clone"
    fi
    echo "--------------------------------------------------------------------"
  } | tee -a /opt/gridpane/gpclone.log

  RETRY=0

  webserver_remote="$(/usr/bin/ssh root@"$remote_IP" "/usr/local/bin/gp conf read webserver -q")"

  gridpane::notify::app \
    "Cloning/Migrate Notice" \
    "${site_to_clone} is building on ${remote_IP}... waiting..." \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${site_to_clone}"

  sleep 10

  while ssh -n root@"${remote_IP}" [ ! -f /var/www/"${site_to_clone}"/htdocs/wp-content/mu-plugins/wp-cli-login-server.php ]; do
    {
      echo "Waiting for remote site to provision (max 300s)... ${RETRY}s"
      echo "--------------------------------------------------------------------"
    } | tee -a /opt/gridpane/gpclone.log
    sleep 1

    RETRY=$((RETRY + 1))
    if [[ ${RETRY} -gt 300 ]]; then
      if [[ $2 == "site-clone" ]]; then
        notification_body="Building new ${site_to_clone} site on ${remote_IP} stalled, exiting process..."
        notification_duration="infinity"
        next="gpclone::end::failsync_pause::exit"
      else
        notification_body="Building new '${site_to_clone}' site on '${remote_IP}' stalled, moving on to next..."
        notification_duration="long"
        next="break"
      fi

      gridpane::notify::app \
        "Cloning/Migrate Notice" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "${notification_duration}" \
        "${site_to_clone}"
      echo "Cloning/Migrate Notice"

      "${next}"
    fi
  done

  sleep 5

  gridpane::notify::app \
    "Cloning/Migrate Notice" \
    "${site_to_clone} build on ${remote_IP} completing..." \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${site_to_clone}"

  sleep 5

  {
    echo "--------------------------------------------------------------------"
    scp /var/www/"${site_to_clone}"/GPBUP-"${site_to_clone}"-CLONE.gz root@"${remote_IP}":/var/www/"${site_to_clone}"/GPBUP-"${site_to_clone}"-CLONE.gz
    echo "Copied local WP export file for ${site_to_clone} to remote system ${remote_IP}"
    # shellcheck disable=SC2029
    ssh root@"${remote_IP}" "ls -l /var/www/${site_to_clone}"
  } | tee -a /opt/gridpane/gpclone.log

  check_for_promethean_remote=$(ssh root@"${remote_IP}" "/usr/local/bin/gp conf read server-build -q" 2>&1)
  {
    echo "--------------------------------------------------------------------"
    echo "Destination is: ${check_for_promethean_remote}"
    echo "--------------------------------------------------------------------"
  } | tee -a /opt/gridpane/gpclone.log

  if [[ ${check_for_promethean_remote} == "prometheus" ]]; then
    if [[ $2 == "site-clone" ]]; then
      if [[ -f /var/www/"${site_to_clone}"/user-configs.php ]]; then
        {
          echo "Sending over user wp-config.php includes..."
          scp /var/www/"${site_to_clone}"/user-configs.php root@"${remote_IP}":/var/www/"${site_to_clone}"/user-configs.php
        } | tee -a /opt/gridpane/gpclone.log
      fi

      if [[ "$webserver" = nginx && $(cat /etc/nginx/sites-available/"${site_to_clone}") == *"HTTPS"* ||
            "$webserver" = openlitespeed && "$(/usr/local/bin/gpols get "$site_to_clone" proto)" = https ]]; then
        [[ "$(gridpane::conf_read ssl -site.env "${site_to_clone}")" != "true" ]] &&
          gridpane::conf_write ssl true -site.env "${site_to_clone}"
        echo "SSL enabled for origin site.... syncing temporary SSL certificates..." | tee -a /opt/gridpane/gpclone.log

        echo "Syncing ${site_to_clone} Certbot SSL" | tee -a /opt/gridpane/gpclone.log
        if [[ -d /etc/letsencrypt/archive/${site_to_clone} ]]; then
          {
            # shellcheck disable=SC2029
            ssh root@"${remote_IP}" "sudo mkdir -p /etc/letsencrypt/archive/${site_to_clone} && sudo rm -rf /etc/letsencrypt/archive/${site_to_clone}/*"
            /usr/bin/rsync -avz /etc/letsencrypt/archive/"${site_to_clone}"/* root@"${remote_IP}":/etc/letsencrypt/archive/"${site_to_clone}"
          } | tee -a /opt/gridpane/gpclone.log
        fi

        if [[ -d /etc/letsencrypt/live/${site_to_clone} ]]; then
          {
            # shellcheck disable=SC2029
            ssh root@"${remote_IP}" "sudo mkdir -p /etc/letsencrypt/live/${site_to_clone} && sudo rm -rf /etc/letsencrypt/live/${site_to_clone}/*"
            /usr/bin/rsync -avz /etc/letsencrypt/live/"${site_to_clone}"/* root@"${remote_IP}":/etc/letsencrypt/live/"${site_to_clone}"
          } | tee -a /opt/gridpane/gpclone.log
        fi

        echo "Syncing ${site_to_clone} ACME SSL" | tee -a /opt/gridpane/gpclone.log
        if [[ "$webserver" = nginx ]]; then
          if [[ -d /etc/nginx/ssl/${site_to_clone} ]]; then
            {
              # shellcheck disable=SC2029
              ssh root@"${remote_IP}" "sudo mkdir -p /etc/nginx/ssl/${site_to_clone} && sudo rm -rf /etc/nginx/ssl/${site_to_clone}/*"
              /usr/bin/rsync -avz /etc/nginx/ssl/"${site_to_clone}"/* root@"${remote_IP}":/etc/nginx/ssl/"${site_to_clone}"
            } | tee -a /opt/gridpane/gpclone.log
          fi
        else
          if [[ -d /usr/local/lsws/conf/cert/${site_to_clone} ]]; then
            {
              # shellcheck disable=SC2029
              ssh root@"${remote_IP}" "sudo mkdir -p /usr/local/lsws/conf/cert/${site_to_clone} && sudo rm -rf /usr/local/lsws/conf/cert/${site_to_clone}/*"
              /usr/bin/rsync -avz /usr/local/lsws/conf/cert/"${site_to_clone}"/* root@"${remote_IP}":/usr/local/lsws/conf/cert/"${site_to_clone}"
            } | tee -a /opt/gridpane/gpclone.log
          fi
        fi

        if [[ -d /root/gridenv/acme-wildcard-configs/${site_to_clone} ]]; then
          {
            # shellcheck disable=SC2029
            ssh root@"${remote_IP}" "mkdir -p /root/gridenv/acme-wildcard-configs/${site_to_clone} && sudo rm -rf /root/gridenv/acme-wildcard-configs/${site_to_clone}/*"
            /usr/bin/rsync -avz --exclude '*.acme.sh.log' /root/gridenv/acme-wildcard-configs/"${site_to_clone}"/* root@"${remote_IP}":/root/gridenv/acme-wildcard-configs/"${site_to_clone}"
          } | tee -a /opt/gridpane/gpclone.log
        fi

        {
          echo "Syncing ACME CRON"
          crontab -l >/tmp/tempcron
          if [[ "$(cat /tmp/tempcron)" == *"${site_to_clone}"* ]]; then
            echo "${site_to_clone} CRON"
            min=$((1 + RANDOM % 59))
            hr=$((1 + RANDOM % 23))
            # shellcheck disable=SC2029
            ssh root@"${remote_IP}" "crontab -l > /tmp/tempcron && echo \"${min} ${hr} * * * \"/root/.acme.sh\"/acme.sh --cron --home \"/root/.acme.sh\" --config-home /root/gridenv/acme-wildcard-configs/${site_to_clone} >>/opt/gridpane/active.acme.monitoring.log 2>&1\" >>/tmp/tempcron && crontab /tmp/tempcron && rm /tmp/tempcron"
          fi
          rm /tmp/tempcron
          echo "--------------------------------------------------------------------"
        } | tee -a /opt/gridpane/gpclone.log
      fi

      if [[ "$(gridpane::conf_read waf -site.env "${site_to_clone}")" == "modsec" ]]; then
        {
          echo "Sync ModSec files"
          # shellcheck disable=SC2029
          ssh root@"${remote_IP}" "sudo mkdir -p /var/www/${site_to_clone}/modsec; sudo rm -rf /var/www/${site_to_clone}/modsec/*"
          /usr/bin/rsync -avz /var/www/"${site_to_clone}"/modsec/ root@"${remote_IP}":/var/www/"${site_to_clone}"/modsec
          echo "--------------------------------------------------------------------"
        } | tee -a /opt/gridpane/gpclone.log
      fi
    else
      # shellcheck disable=SC2029
       ssh root@"$remote_IP" "touch /var/www/${site_to_clone}/logs/failover.clone"
    fi

    # shellcheck disable=SC2154
    {
      echo "Sync $webserver_formatted Files"
      if [[ "$webserver" = nginx &&
            "$webserver_remote" = nginx ]]; then
        # shellcheck disable=SC2029
        ssh root@"${remote_IP}" "sudo mkdir -p /var/www/${site_to_clone}/nginx; sudo rm -rf /var/www/${site_to_clone}/nginx/*"
        /usr/bin/rsync -avz --exclude '*-sockfile.conf' /var/www/"${site_to_clone}"/nginx/ root@"${remote_IP}":/var/www/"${site_to_clone}"/nginx
      elif [[ "$webserver" = openlitespeed &&
            "$webserver_remote" = openlitespeed ]]; then
        # shellcheck disable=SC2029
        ssh root@"${remote_IP}" "sudo mkdir -p /var/www/${site_to_clone}/ols; sudo rm -rf /var/www/${site_to_clone}/ols/*"
        /usr/bin/rsync -avz /var/www/"${site_to_clone}"/ols/ root@"${remote_IP}":/var/www/"${site_to_clone}"/ols
      fi
      echo "--------------------------------------------------------------------"
    } | tee -a /opt/gridpane/gpclone.log

    if [[ -d /var/www/${site_to_clone}/dns ]]; then
      {
        echo "Sync DNS creds" | tee -a /opt/gridpane/gpclone.log
        # shellcheck disable=SC2029
        ssh root@"${remote_IP}" "sudo mkdir -p /var/www/${site_to_clone}/dns; sudo rm -rf /var/www/${site_to_clone}/dns/*"
        /usr/bin/rsync -avz /var/www/"${site_to_clone}"/dns/ root@"${remote_IP}":/var/www/"${site_to_clone}"/dns
        echo "--------------------------------------------------------------------"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local cloning_site_cache
    local cloning_site_config
    local remote_cloning_site_config
    local cloning_site_object_caching
    local remote_cloning_site_object_caching
    local remote_certbot_ssl_present
    local remote_acme_ssl_present

    if [[ "$webserver" = nginx  ]]; then
      cloning_site_cache="$(gridpane::check::site::nginx::cache "$site_to_clone")"
      cloning_site_config="$(gridpane::check::site::nginx::config "$site_to_clone")"
      cloning_site_object_caching=$(sudo su - "${local_sys_user}" -c "timeout 30 /usr/local/bin/wp redis status --path=/var/www/${site_to_clone}/htdocs 2>&1")
    else
      cloning_site_cache="$(/usr/local/bin/gpols get "$site_to_clone" cache)"
      cloning_site_config="$(/usr/local/bin/gpols get "$site_to_clone" vhconf)"
      cloning_site_object_caching="$(gridpane::conf_read ols-redis-cache -site.env "$site_to_clone")"
    fi

    certbot_cert_path="/etc/letsencrypt/live/${site_to_clone}/fullchain.pem"

    if [[ "$webserver_remote" = nginx  ]]; then
      acme_cert_path="/etc/nginx/ssl/${site_to_clone}/cert.pem"
      # shellcheck disable=SC2029
      remote_cloning_site_cache="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx cache-check ${site_to_clone} -q")"
      [[ $remote_cloning_site_cache != "fastcgi" && $remote_cloning_site_cache != "redis"  ]] &&
        remote_cloning_site_cache="off"
      # shellcheck disable=SC2029
      remote_cloning_site_config="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx type-check ${site_to_clone} -q")"
      # shellcheck disable=SC2029
      remote_cloning_site_object_caching=$(ssh root@"$remote_IP" "/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp redis status --path=/var/www/${site_to_clone}/htdocs 2>&1\"")
    else
      acme_cert_path="/usr/local/lsws/conf/cert/${site_to_clone}/cert.pem"
      # shellcheck disable=SC2029
      remote_cloning_site_cache="$(ssh root@"$remote_IP" "/usr/local/bin/gpols get $site_to_clone cache")"
      # shellcheck disable=SC2029
      remote_cloning_site_config="$(ssh root@"$remote_IP" "/usr/local/bin/gpols get $site_to_clone vhconf")"
      # shellcheck disable=SC2029
      remote_cloning_site_object_caching="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read ols-redis-cache -site.env $site_to_clone -q")"
    fi
    # shellcheck disable=SC2029
    remote_acme_ssl_present=$(ssh root@"$remote_IP" "[[ -f ${acme_cert_path} ]] && echo true")
    # shellcheck disable=SC2029
    remote_certbot_ssl_present=$(ssh root@"$remote_IP" "[[ -f ${certbot_cert_path} ]] && echo true")

    if [[ ${cloning_site_config} == *"https" || ${cloning_site_config} == *"vhssl" ]] &&
       [[ ${remote_cloning_site_config} != *"https" && ${remote_cloning_site_config} != *"vhssl" ]]; then

      if [[ ${remote_acme_ssl_present} == "true" ]]; then
        local is_remote_cert_wildcard
         # shellcheck disable=SC2029
        is_remote_cert_wildcard=$(ssh root@"$remote_IP" "openssl x509 -text -noout -in ${acme_cert_path}")
        local https_conf_type
        local is_wildcard
        local return_wildcard_string
        return_wildcard_string=""
        if [[ "${is_remote_cert_wildcard}" == *"*.${site_to_clone}"* ]]; then
          local return_wildcard_string="return-wildcard"
          is_wildcard="true"
        else
          is_wildcard="false"
        fi

        # shellcheck disable=SC2029
        [[ "$webserver" = nginx ]] &&
          https_conf_type="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx type-check ${site_to_clone} ${return_wildcard_string} return-https -q")"

        # If a remote server has a lot of modsec sites, then the conf update might now have finished by the time we do the remote check
        # So the firewall won't yet be enabled, and the return is wrong.
        if [[ "$webserver" = nginx &&
          $cloning_site_config == *"proxy"* &&
          $https_conf_type != *"proxy"* ]]; then
          sleep 10
           # shellcheck disable=SC2029
          https_conf_type="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx type-check ${site_to_clone} ${return_wildcard_string} return-https -q")"
          if [[ $cloning_site_config == *"proxy"* &&
            $https_conf_type != *"proxy"* ]]; then
            sleep 15
            # shellcheck disable=SC2029
            https_conf_type="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx type-check ${site_to_clone} ${return_wildcard_string} return-https -q")"
            # Still not matching? Creating a token on remote to double check during restore
            if [[ $cloning_site_config == *"proxy"* &&
              $https_conf_type != *"proxy"* ]]; then
               # shellcheck disable=SC2029
              ssh root@"$remote_IP" "touch /var/www/${site_to_clone}/logs/check.proxy.config"
            fi
          fi
        fi

        local generate_conf
        if [[ "$webserver_remote" = nginx ]]; then
           # shellcheck disable=SC2029
          generate_conf="$(
            ssh root@"$remote_IP" \
            "/usr/local/bin/gp conf nginx generate ${https_conf_type} ${remote_cloning_site_cache} ${site_to_clone}; \
            /usr/sbin/nginx -t && /usr/local/bin/gp nginx reload"
          )"
        else
          if [[ "$is_wildcard" = true ]]; then
            # shellcheck disable=SC2029
            generate_conf="$(
              ssh root@"$remote_IP" \
              "/usr/local/bin/gpols site ${site_to_clone} --wildcard --ssl --output"
            )"
          else
            # shellcheck disable=SC2029
            generate_conf="$(
              ssh root@"$remote_IP" \
              "/usr/local/bin/gpols site ${site_to_clone} --ssl --output"
            )"
          fi
        fi
      elif [[ ${remote_certbot_ssl_present} == "true" ]]; then

        # shellcheck disable=SC2029
        [[ "$webserver" = nginx ]] &&
          https_conf_type="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx type-check ${site_to_clone} ${return_wildcard_string} return-https -q")"

        # If a remote server has a lot of modsec sites, then the conf update might now have finished by the time we do the remote check
        # So the firewall won't yet be enabled, and the return is wrong.
        if [[ "$webserver" = nginx &&
          $cloning_site_config == *"proxy"* &&
          $https_conf_type != *"proxy"* ]]; then
          sleep 10
           # shellcheck disable=SC2029
          https_conf_type="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx type-check ${site_to_clone} ${return_wildcard_string} return-https -q")"
          if [[ $cloning_site_config == *"proxy"* &&
            $https_conf_type != *"proxy"* ]]; then
            sleep 15
            # shellcheck disable=SC2029
            https_conf_type="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx type-check ${site_to_clone} ${return_wildcard_string} return-https -q")"
            # Still not matching? Creating a token on remote to double check during restore
            if [[ $cloning_site_config == *"proxy"* &&
              $https_conf_type != *"proxy"* ]]; then
               # shellcheck disable=SC2029
              ssh root@"$remote_IP" "touch /var/www/${site_to_clone}/logs/check.proxy.config"
            fi
          fi
        fi
        is_wildcard="false"

        local generate_conf
        if [[ "$webserver_remote" = nginx ]]; then
           # shellcheck disable=SC2029
          generate_conf="$(
            ssh root@"$remote_IP" \
            "/usr/local/bin/gp conf nginx generate ${https_conf_type} ${remote_cloning_site_cache} ${site_to_clone}; \
            /usr/sbin/nginx -t && /usr/local/bin/gp nginx reload"
          )"
        else
          # shellcheck disable=SC2029
          generate_conf="$(
            ssh root@"$remote_IP" \
            "/usr/local/bin/gpols site ${site_to_clone} --ssl --output"
          )"
        fi
      fi

      {
        echo "${is_remote_cert_wildcard}"
        echo "${generate_conf}"
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" "/usr/local/bin/gp conf wp -update-host ${site_to_clone}"
      } | tee -a /opt/gridpane/gpclone.log

      local check_remote_https_domain
      if [[ "$webserver_remote" = nginx ]]; then
         # shellcheck disable=SC2029
        check_remote_https_domain=$(ssh -n root@"${remote_IP}" "sleep 1 && cat /etc/nginx/sites-enabled/${site_to_clone}")
      else
         # shellcheck disable=SC2029
        check_remote_https_domain=$(ssh -n root@"${remote_IP}" "sleep 1 && cat /usr/local/lsws/conf/vhosts/${site_to_clone}/vhconf.conf")
      fi

      if [[ "$webserver_remote" = nginx && ${check_remote_https_domain} == *"HTTPS"* ||
            "$webserver_remote" = openlitespeed && ${check_remote_https_domain} == *"ssl.conf"* ]]; then
        gridpane::callback::app \
          "callback" \
          "/domain/domain-update" \
          "site_url=${site_to_clone}" \
          "server_ip=${remote_IP}" \
          "domain_url=${site_to_clone}" \
          "ssl@true" \
          "chain_ssl@false" \
          "status=success" \
          "details=success" \
          "silent@false"

        # shellcheck disable=SC2029
        ssh root@"$remote_IP" "/usr/local/bin/gp conf write ssl true -site.env ${site_to_clone}"
      fi
    fi

    {
      echo "Checking for ${site_to_clone} Add on Alias domains..."
      echo "--------------------------------------------------------------------"
    } | tee -a /opt/gridpane/gpclone.log
    gpclone::process::clone_domains alias "${site_to_clone}" "$remote_IP"

    {
      echo "Checking for ${site_to_clone} Add on Redirect domains..."
      echo "--------------------------------------------------------------------"
    } | tee -a /opt/gridpane/gpclone.log
    gpclone::process::clone_domains redirect "${site_to_clone}" "$remote_IP"

    local http_auth
    http_auth=$(gridpane::conf_read http-auth -site.env "${site_to_clone}")
    local remote_http_auth
    # shellcheck disable=SC2029
    remote_http_auth=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read http-auth -site.env ${site_to_clone} -q")
    if [[ ${http_auth} == "true" || ${http_auth} == "on" ]] &&
      [[ ${remote_http_auth} != "true" && ${remote_http_auth} != "on" ]]; then
      {
        echo "----------------------------------------------------------------------"
        echo "HTTP Auth for ${site_to_clone} on $remote_IP"
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" "/usr/local/bin/gp site ${site_to_clone} -http-auth"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local smtp
    smtp=$(gridpane::conf_read smtp -site.env "${site_to_clone}")
    local remote_smtp
    # shellcheck disable=SC2029
    remote_smtp=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read smtp -site.env ${site_to_clone} -q")
    if [[ ${smtp} == "true" || ${smtp} == "on" ]] &&
      [[ ${remote_smtp} != "true" && ${remote_smtp} != "on" ]]; then
      if [[ -f /var/www/"${site_to_clone}"/sendgrid-wp-configs.php ]]; then
        {
          echo "----------------------------------------------------------------------"
          echo "Enabling SMTP for ${site_to_clone} on $remote_IP"
          local api_key
          api_key=$(grep "SMTP_PASS" /var/www/"${site_to_clone}"/sendgrid-wp-configs.php)
          api_key=${api_key//\"/}
          api_key=${api_key// /}
          api_key=${api_key#define(SMTP_PASS,}
          api_key=${api_key%');'}

#          ssh root@"$remote_IP" "/usr/local/bin/gp site ${site_to_clone} -mailer enable SendGrid \"${api_key}\" simple"
        } | tee -a /opt/gridpane/gpclone.log
      fi
    fi

    local source_php_ver
    source_php_ver=$(gridpane::get::site::php "${site_to_clone}")
    local destination_php_ver
    if [[ "$webserver_remote" = nginx ]]; then
      # shellcheck disable=SC2029
      destination_php_ver=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf php -ver-check ${site_to_clone} -q")
    elif [[ "$webserver_remote" = openlitespeed ]]; then
      # shellcheck disable=SC2029
      destination_php_ver=$(ssh root@"$remote_IP" "/usr/local/bin/gpols get ${site_to_clone} php-ver")
    fi

    if [[ ${source_php_ver} != "${destination_php_ver}" ]]; then
      {
        echo "Adjusting PHP for ${site_to_clone} on $remote_IP to ${source_php_ver}"
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" "/usr/local/bin/gp site ${site_to_clone} -switch-php ${source_php_ver}"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    if [[ $3 == "true"* ]]; then
      if [[ $3 == *"subdomain" ]]; then
        multisite_type="-multisubdom"
      elif [[ $3 == *"subdirectory" ]]; then
        multisite_type="-multisubdir"
      fi
      {
        echo "Converting ${remote_IP} ${site_to_clone} to multisite ${3}... after a sleep..."
        # shellcheck disable=SC2029
        ssh root@"${remote_IP}" "sleep 10 && /usr/local/bin/gp site ${site_to_clone} ${multisite_type}"
      } | tee -a /opt/gridpane/gpclone.log
    fi
  fi

  notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${site_to_clone}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>${site_to_clone} Clone package migrated to destination..."
  gridpane::notify::app \
    "Cloning/Migrate Notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${site_to_clone}"
  echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

  rm /var/www/"${site_to_clone}"/GPBUP-"${site_to_clone}"-CLONE.gz
  # shellcheck disable=SC2029
  ssh root@"$remote_IP" "touch /var/www/${site_to_clone}/logs/server.clone"

  local remote_sys_user
  local local_sys_user
  local cloning_site_cache
  local cloning_site_config
  local remote_cloning_site_config
  local cloning_site_object_caching
  local remote_cloning_site_object_caching
  # shellcheck disable=SC2029
  remote_sys_user="$(ssh root@"$remote_IP" "/bin/grep sys-user: /var/www/${site_to_clone}/logs/${site_to_clone}.env" | /usr/bin/awk -F '[:]' '{print $2}')"
  local_sys_user="$(gridpane::get::set::site::user "$site_to_clone")"

  if [[ "$webserver" = nginx &&
        "$webserver_remote" = nginx ]]; then

    # shellcheck disable=SC2029
    destination_php_ver=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf php -ver-check ${site_to_clone} -q")
    local phpFileVer
    phpFileVer=${destination_php_ver/./}
    local sourcePhpFileVer
    sourcePhpFileVer=${source_php_ver/./}

    local proceed_with_fpm
    case $destination_php_ver in
      7.1|7.2|7.3|7.4) echo "Destination PHP Version valid: $destination_php_ver" | tee -a /opt/gridpane/gpclone.log ;;
      *)
        echo "Destination PHP Version not valid: $destination_php_ver" | tee -a /opt/gridpane/gpclone.log
        proceed_with_fpm="false"
        ;;
    esac

    case $phpFileVer in
      71|72|73|74) echo "Destination PHP Sock Version valid: $phpFileVer" | tee -a /opt/gridpane/gpclone.log ;;
      *)
        echo "Destination PHP Sock Version not valid: $phpFileVer" | tee -a /opt/gridpane/gpclone.log
        proceed_with_fpm="false"
        ;;
    esac

    case $sourcePhpFileVer in
      71|72|73|74) echo "Source PHP Sock Version valid: $sourcePhpFileVer" | tee -a /opt/gridpane/gpclone.log ;;
      *)
        echo "Source PHP Sock Version not valid: $sourcePhpFileVer" | tee -a /opt/gridpane/gpclone.log
        proceed_with_fpm="false"
        ;;
    esac

    if [[ $(cat /etc/passwd) != *"/home/${local_sys_user}:"* ]]; then
      echo "Local System User not Valid: ${local_sys_user}" | tee -a /opt/gridpane/gpclone.log
      proceed_with_fpm="false"
    else
      echo "Local System User Valid: ${local_sys_user}" | tee -a /opt/gridpane/gpclone.log
    fi

    local check_for_remote_user
    # shellcheck disable=SC2029
    check_for_remote_user=$(ssh root@"$remote_IP" "[[ \$(cat /etc/passwd) == *\"/home/${remote_sys_user}:\"* ]] && echo \"true\"")
    if [[ ${check_for_remote_user} != *"true"* ]]; then
      echo "Remote System User not Valid: ${remote_sys_user}" | tee -a /opt/gridpane/gpclone.log
      proceed_with_fpm="false"
    else
      echo "Remote System User Valid: ${remote_sys_user}" | tee -a /opt/gridpane/gpclone.log
    fi

    if [[ ${proceed_with_fpm} != *"true"* ]]; then
      {
        scp /etc/php/"${source_php_ver}"/fpm/pool.d/"${site_to_clone}".conf root@"${remote_IP}":/etc/php/"${destination_php_ver}"/fpm/pool.d/"${site_to_clone}".conf
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "sed -i \"s/'${site_to_clone}${sourcePhpFileVer}'/'${site_to_clone}${phpFileVer}'/g\" /etc/php/${destination_php_ver}/fpm/pool.d/${site_to_clone}.conf; \
          sed -i \"s/\[${site_to_clone}${sourcePhpFileVer}\]/\[${site_to_clone}${phpFileVer}\]/g\" /etc/php/${destination_php_ver}/fpm/pool.d/${site_to_clone}.conf; \
          sed -i \"/user = ${local_sys_user}/c\user = ${remote_sys_user}\" /etc/php/${destination_php_ver}/fpm/pool.d/${site_to_clone}.conf; \
          sed -i \"/group = ${local_sys_user}/c\group = ${remote_sys_user}\" /etc/php/${destination_php_ver}/fpm/pool.d/${site_to_clone}.conf; \
          sed -i \"/listen = \/var/c\listen = \/var\/run\/php\/php${phpFileVer}-fpm-${site_to_clone}.sock\" /etc/php/${destination_php_ver}/fpm/pool.d/${site_to_clone}.conf"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    {
      local command_sequence
      if [[ "$cloning_site_config" == *"wildcard"* && "$remote_cloning_site_config" != *"wildcard"* ]]; then
        command_sequence="/usr/local/bin/gp site ${site_to_clone} -create-wildcard;"
      fi

      if [[ "$cloning_site_config" == *"www"* && "$remote_cloning_site_config" != *"www"* ]]; then
         command_sequence="${command_sequence} /usr/local/bin/gp site ${site_to_clone} -route-domain-www;"
      elif [[ "$cloning_site_config" == *"root"* && "$remote_cloning_site_config" != *"root"* ]]; then
        command_sequence="${command_sequence} /usr/local/bin/gp site ${site_to_clone} -route-domain-root;"
      fi

      if [[ "$cloning_site_cache" != "$remote_cloning_site_cache" ]]; then
        command_sequence="${command_sequence} /usr/local/bin/gp site ${site_to_clone} -caching=$cloning_site_cache;"
      fi

      # shellcheck disable=SC2029
      [[ -n ${command_sequence} ]] &&
        ssh root@"$remote_IP" "${command_sequence}"

      # shellcheck disable=SC2029
      if [[ "$cloning_site_object_caching" == *"Status: Connected"* && "$cloning_site_object_caching" != "$remote_cloning_site_object_caching" ]]; then
        ssh root@"$remote_IP" "/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp redis enable --path=/var/www/${site_to_clone}/htdocs\""
      else
        ssh root@"$remote_IP" "/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp redis disable --path=/var/www/${site_to_clone}/htdocs\""
      fi

      local source_waf_type
      local destination_waf_type
      source_waf_type=$(gridpane::conf_read waf -site.env "${site_to_clone}")
      # shellcheck disable=SC2029
      destination_waf_type=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read waf -site.env ${site_to_clone} -q")
      command_sequence=""
      if [[ ${source_waf_type} == *"modsec"* &&
            ${destination_waf_type} != *"modsec"* ]]; then
        waf::sync::site_to_app "${site_to_clone}" "modsec"
        if [[ ${destination_waf_type} == *"6"* ]]; then
          command_sequence="/usr/local/bin/gp site ${site_to_clone} -6g-off;"
        elif [[ ${destination_waf_type} == *"7"* ]]; then
          command_sequence="/usr/local/bin/gp site ${site_to_clone} -7g-off;"
        fi
        command_sequence="${command_sequence} /usr/local/bin/gp site ${site_to_clone} -modsec-on;"
      elif [[ ${source_waf_type} == "6G" || ${source_waf_type} == "6g" ]] &&
          [[ ${destination_waf_type} != "6G" && ${destination_waf_type} != "6g" ]]; then
        waf::sync::site_to_app "${site_to_clone}" "6g"
        if [[ ${destination_waf_type} == *"modsec"* ]]; then
          command_sequence="/usr/local/bin/gp site ${site_to_clone} -modsec-off;"
        elif [[ ${destination_waf_type} == *"7"* ]]; then
          command_sequence="/usr/local/bin/gp site ${site_to_clone} -7g-off;"
        fi
        command_sequence="${command_sequence} /usr/local/bin/gp site ${site_to_clone} -6g-on;"
      elif [[ ${source_waf_type} == "7G" || ${source_waf_type} == "7g" ]] &&
          [[ ${destination_waf_type} != "7G" && ${destination_waf_type} != "7g" ]]; then
        waf::sync::site_to_app "${site_to_clone}" "7g"
        if [[ ${destination_waf_type} == *"modsec"* ]]; then
          command_sequence="/usr/local/bin/gp site ${site_to_clone} -modsec-off;"
        elif [[ ${destination_waf_type} == *"6"* ]]; then
          command_sequence="/usr/local/bin/gp site ${site_to_clone} -6g-off;"
        fi
        command_sequence="${command_sequence} /usr/local/bin/gp site ${site_to_clone} -7g-on;"
      fi

       # shellcheck disable=SC2029
      [[ -n ${command_sequence} ]] &&
        ssh root@"$remote_IP" "${command_sequence}"

      source_waf_type=$(gridpane::conf_read waf -site.env "${site_to_clone}")
      # shellcheck disable=SC2029
      destination_waf_type=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read waf -site.env ${site_to_clone} -q")
      if [[ ${source_waf_type} == *"modsec"* &&
            ${destination_waf_type} == *"modsec"* ]]; then
        scp /var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-crs-anomaly-blocking-threshold.conf \
          root@"${remote_IP}":/var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-crs-anomaly-blocking-threshold.conf
        scp /var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-crs-paranoia-level.conf \
          root@"${remote_IP}":/var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-crs-paranoia-level.conf
        scp /var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-runtime-exceptions.conf \
          root@"${remote_IP}":/var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-runtime-exceptions.conf
        scp /var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-runtime-whitelist.conf \
          root@"${remote_IP}":/var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-runtime-whitelist.conf
      elif [[ ${source_waf_type} == "6G" || ${source_waf_type} == "6g" ]] &&
          [[ ${destination_waf_type} == "6G" || ${destination_waf_type} == "6g" ]]; then
        scp /etc/nginx/common/"${site_to_clone}"-6g.conf root@"${remote_IP}":/etc/nginx/common/"${site_to_clone}"-6g.conf
      elif [[ ${source_waf_type} == "7G" || ${source_waf_type} == "7g" ]] &&
          [[ ${destination_waf_type} == "7G" || ${destination_waf_type} == "7g" ]]; then
        scp /etc/nginx/common/"${site_to_clone}"-7g.conf root@"${remote_IP}":/etc/nginx/common/"${site_to_clone}"-7g.conf
      fi
      [[ -f /var/www/"${site_to_clone}"/logs/waf.sync ]] && rm /var/www/"${site_to_clone}"/logs/waf.sync
    } | tee -a /opt/gridpane/gpclone.log
  fi

  waf::sync::site_to_app "${site_to_clone}" "wpf2b"
  local site_wpfail2ban
  site_wpfail2ban=$(gridpane::conf_read wp-fail2ban -site.env "${site_to_clone}")
  local remote_site_wpfail2ban
  # shellcheck disable=SC2029
  remote_site_wpfail2ban=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read wp-fail2ban -site.env ${site_to_clone} -q")
  if [[ ${site_wpfail2ban} == *"rue"* || ${site_wpfail2ban} == *"on"* ]] &&
    [[ ${remote_site_wpfail2ban} != *"rue"* && ${remote_site_wpfail2ban} != *"on"* ]]; then
    {
      echo "----------------------------------------------------------------------"
      echo "Enabling ${site_to_clone} wpfail2ban configurations on $remote_IP"
      # shellcheck disable=SC2029
      ssh root@"$remote_IP" "/usr/local/bin/gp site ${site_to_clone} -enable-wp-fail2ban"
      sleep 3
    } | tee -a /opt/gridpane/gpclone.log

    local source_fail2ban_user_enumeration
    source_fail2ban_user_enumeration=$(grep "WP_FAIL2BAN_BLOCK_USER_ENUMERATION" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_user_enumeration ]]; then
      {
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "/bin/sed -i \"/WP_FAIL2BAN_BLOCK_USER_ENUMERATION/d\" /var/www/${site_to_clone}/wp-fail2ban-configs.php; \
          /bin/echo \"$source_fail2ban_user_enumeration\" | /usr/bin/tee -a /var/www/${site_to_clone}/wp-fail2ban-configs.php"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_blocked_users
    source_fail2ban_blocked_users=$(grep "WP_FAIL2BAN_BLOCKED_USERS" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_blocked_users ]]; then
      {
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "/bin/sed -i \"/WP_FAIL2BAN_BLOCKED_USERS/d\" /var/www/${site_to_clone}/wp-fail2ban-configs.php; \
          /bin/echo \"$source_fail2ban_blocked_users\" | /usr/bin/tee -a /var/www/${site_to_clone}/wp-fail2ban-configs.php"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_comments
    source_fail2ban_log_comments=$(grep "WP_FAIL2BAN_LOG_COMMENTS'" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_comments ]]; then
      {
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "/bin/sed -i \"/WP_FAIL2BAN_LOG_COMMENTS'/d\" /var/www/${site_to_clone}/wp-fail2ban-configs.php; \
          /bin/echo \"$source_fail2ban_log_comments\" | /usr/bin/tee -a /var/www/${site_to_clone}/wp-fail2ban-configs.php"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_comments_extra
    source_fail2ban_log_comments_extra=$(grep "WP_FAIL2BAN_LOG_COMMENTS_EXTRA" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_comments_extra ]]; then
      {
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "/bin/sed -i \"/WP_FAIL2BAN_LOG_COMMENTS_EXTRA/d\" /var/www/${site_to_clone}/wp-fail2ban-configs.php; \
          /bin/echo \"$source_fail2ban_log_comments_extra\" | /usr/bin/tee -a /var/www/${site_to_clone}/wp-fail2ban-configs.php"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_password_request
    source_fail2ban_log_password_request=$(grep "WP_FAIL2BAN_LOG_PASSWORD_REQUEST" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_password_request ]]; then
      {
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "/bin/sed -i \"/WP_FAIL2BAN_LOG_PASSWORD_REQUEST/d\" /var/www/${site_to_clone}/wp-fail2ban-configs.php; \
          /bin/echo \"$source_fail2ban_log_password_request\" | /usr/bin/tee -a /var/www/${site_to_clone}/wp-fail2ban-configs.php"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_pingbacks
    source_fail2ban_log_pingbacks=$(grep "WP_FAIL2BAN_LOG_PINGBACKS" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_pingbacks ]]; then
      {
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "/bin/sed -i \"/WP_FAIL2BAN_LOG_PINGBACKS/d\" /var/www/${site_to_clone}/wp-fail2ban-configs.php; \
          /bin/echo \"$source_fail2ban_log_pingbacks\" | /usr/bin/tee -a /var/www/${site_to_clone}/wp-fail2ban-configs.php"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_spam
    source_fail2ban_log_spam=$(grep "WP_FAIL2BAN_LOG_SPAM" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_spam ]]; then
      {
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "/bin/sed -i \"/WP_FAIL2BAN_LOG_SPAM/d\" /var/www/${site_to_clone}/wp-fail2ban-configs.php; \
          /bin/echo \"$source_fail2ban_log_spam\" | /usr/bin/tee -a /var/www/${site_to_clone}/wp-fail2ban-configs.php"
      } | tee -a /opt/gridpane/gpclone.log
    fi
  fi

  {
    # shellcheck disable=SC2029
    ssh root@"${remote_IP}" "cd /var/www/${site_to_clone}/htdocs && sudo /usr/local/bin/gprestore"
    echo "Restored ${site_to_clone} export/clone to remote system ${remote_IP}."
    echo "--------------------------------------------------------------------"
  } | tee -a /opt/gridpane/gpclone.log

  if [[ "$webserver" = nginx &&
        "$webserver_remote" = openlitespeed ]]; then

    local command_sequence

    # Cache
    case "$cloning_site_cache" in
      redis)
        remote_site_cache=redis-page
      ;;
      fastcgi)
        remote_site_cache=page-only
      ;;
      php|off)
        remote_site_cache=off
      ;;
      *)
        remote_site_cache=off
    esac

    # touch /var/www/${site_to_clone}/logs/cross.server-type.clone - stop mismatching envs causing cache functions running erroneously in gprestore
    # shellcheck disable=SC2029
    command_sequence+="[[ -d /var/www/${site_to_clone}/htdocs/wp-content/plugins/gridpane-redis-object-cache ]] && /bin/rm -rf /var/www/${site_to_clone}/htdocs/wp-content/plugins/gridpane-redis-object-cache;"
    command_sequence+="[[ -d /var/www/${site_to_clone}/htdocs/wp-content/plugins/nginx-helper ]] && /bin/rm -rf /var/www/${site_to_clone}/htdocs/wp-content/plugins/nginx-helper;"
    command_sequence+="/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp plugin install litespeed-cache --activate --path=/var/www/${site_to_clone}/htdocs\";"
    command_sequence+="/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp rewrite structure '/%category%/%postname%/' --path=/var/www/${site_to_clone}/htdocs\";"
    command_sequence+="/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp option update litespeed.conf.purge-post_all 1 --path=/var/www/${site_to_clone}/htdocs\";"
    command_sequence+="/usr/local/bin/gp site ${site_to_clone} -caching=$remote_site_cache;"

    # 6G/7G WAF sync
    local source_waf_type
    local local_nginx_conf
    local local_nginx_6g_conf
    source_waf_type="$(gridpane::conf_read waf -site.env "${site_to_clone}" -q)"

    if [[ "$source_waf_type" = 6G || "$source_waf_type" = 6g ]]; then
      local_nginx_6g_conf="$(/bin/cat < /etc/nginx/common/"$site_to_clone"-6g.conf)"
      command_sequence+="/usr/local/bin/gp site ${site_to_clone} 6g off;"
      source_waf_type=7G
    fi

    if [[ "$source_waf_type" = 7G || "$source_waf_type" = 7g ]]; then
      local_nginx_conf="$([[ -f /etc/nginx/common/"$site_to_clone"-7g.conf ]] && /bin/cat < /etc/nginx/common/"$site_to_clone"-7g.conf)"
      command_sequence+="/usr/local/bin/gp site ${site_to_clone} 7g on;"

      [[ "$local_nginx_6g_conf" == *"#if (\$bad_bot)"* ||
         "$local_nginx_conf" == *"#if (\$bad_bot_7g)"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${site_to_clone} 7g -bad-bots off;"

      [[ "$local_nginx_6g_conf" == *"#if (\$not_allowed_method)"* ||
         "$local_nginx_conf" == *"#if (\$not_allowed_method_7g)"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${site_to_clone} 7g -bad-methods off;"

      [[ "$local_nginx_6g_conf" == *"#if (\$bad_querystring)"* ||
         "$local_nginx_conf" == *"#if (\$bad_querystring_7g)"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${site_to_clone} 7g -bad-query-string off;"

      [[ "$local_nginx_6g_conf" == *"#if (\$bad_referer)"* ||
         "$local_nginx_conf" == *"#if (\$bad_referer_7g)"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${site_to_clone} 7g -bad-referer off;"

      [[ "$local_nginx_6g_conf" == *"#if (\$bad_request)"* ||
         "$local_nginx_conf" == *"#if (\$bad_request_7g)"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${site_to_clone} 7g -bad-request off;"
    fi

    # Domains
    local site_route

    [[ "$cloning_site_config" == *"wildcard"* ]] &&
      command_sequence+="/usr/local/bin/gp site ${site_to_clone} -create-wildcard;"

    site_route="$(gridpane::conf_read route -site.env "$site_to_clone")"
    [[ "$site_route" = www || "$site_route" = root ]] && command_sequence+="/usr/local/bin/gp site ${site_to_clone} -route-domain-${site_route};"

    # Multisite
    if [[ $3 == "true"* ]]; then
      if [[ $3 == *"subdomain" ]]; then
        multisite_type="-multisubdom"
      elif [[ $3 == *"subdirectory" ]]; then
        multisite_type="-multisubdir"
      fi
      command_sequence+="sleep 1;/usr/local/bin/gp site ${site_to_clone} ${multisite_type};"
    fi

    {
      # touch /var/www/${site_to_clone}/logs/cross.server-type.clone - stop mismatching envs causing cache functions running erroneously in gprestore
      # shellcheck disable=SC2029
      ssh root@"$remote_IP" "$command_sequence"
      echo "Successfully configured $site_to_clone to remote OpenLiteSpeed server."
    } | tee -a /opt/gridpane/gpclone.log

  elif [[ "$webserver" = openlitespeed &&
          "$webserver_remote" = nginx ]]; then

    {

      local command_sequence

      # Cache
      case "$cloning_site_cache" in
        redis|redis-only|redis-page)
          remote_site_cache=redis
        ;;
        redis-off|page-off|php|off)
          remote_site_cache=off
        ;;
        page|page-only)
          remote_site_cache=fastcgi
        ;;
        *)
          remote_site_cache=off
        ;;
      esac

      # touch /var/www/${site_to_clone}/logs/cross.server-type.clone - stop mismatching envs causing cache functions running erroneously in gprestore
      # shellcheck disable=SC2029
      command_sequence+="[[ -d /var/www/${site_to_clone}/htdocs/wp-content/plugins/litespeed-cache ]] && /bin/rm -rf /var/www/${site_to_clone}/htdocs/wp-content/plugins/litespeed-cache;"
      command_sequence+="/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp plugin install nginx-helper --activate --path=/var/www/${site_to_clone}/htdocs\";"
      command_sequence+="/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp plugin install https://github.com/gridpane/gridpane-redis-object-cache/archive/master.zip --activate --path=/var/www/${site_to_clone}/htdocs\";"
      command_sequence+="/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp redis enable --path=/var/www/${site_to_clone}/htdocs\";"
      command_sequence+="/usr/local/bin/gp site ${site_to_clone} -caching=$remote_site_cache;"

      # 7G WAF sync
      local source_waf_type
      source_waf_type="$(gridpane::conf_read waf -site.env "${site_to_clone}" -q)"

      if [[ "$source_waf_type" = 7G || "$source_waf_type" = 7g ]]; then
        command_sequence+="/usr/local/bin/gp site ${site_to_clone} 7g on;"

        [[ "$cloning_site_config" != *"# 7G:[USER AGENT]"* ]] &&
          command_sequence+="/usr/local/bin/gp site ${site_to_clone} 7g -bad-bots off;"

        [[ "$cloning_site_config" != *"# 7G:[REQUEST METHOD]"* ]] &&
          command_sequence+="/usr/local/bin/gp site ${site_to_clone} 7g -bad-methods off;"

        [[ "$cloning_site_config" != *"# 7G:[QUERY STRING]"* ]] &&
          command_sequence+="/usr/local/bin/gp site ${site_to_clone} 7g -bad-query-string off;"

        [[ "$cloning_site_config" != *"# 7G:[HTTP REFERRER]"* ]] &&
          command_sequence+="/usr/local/bin/gp site ${site_to_clone} 7g -bad-referer off;"

        [[ "$cloning_site_config" != *"# 7G:[REQUEST URI]"* ]] &&
          command_sequence+="/usr/local/bin/gp site ${site_to_clone} 7g -bad-request off;"
      fi

      # Domains
      local site_route

      [[ "$cloning_site_config" == *"wildcard-http"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${site_to_clone} -create-wildcard;"

      site_route="$(gridpane::conf_read route -site.env "$site_to_clone")"
      [[ "$site_route" = www || "$site_route" = root ]] && command_sequence+="/usr/local/bin/gp site ${site_to_clone} -route-domain-${site_route};"


      # shellcheck disable=SC2029
      ssh root@"$remote_IP" "$command_sequence"

      echo "Successfully configured $site_to_clone to remote Nginx server."
    } | tee -a /opt/gridpane/gpclone.log

  elif [[ "$webserver" = openlitespeed &&
          "$webserver_remote" = openlitespeed ]]; then
    {
      local command_sequence

      # Cache
      [[ "$cloning_site_cache" != off ]] &&
        command_sequence+="${command_sequence} /usr/local/bin/gp site ${site_to_clone} -caching=$cloning_site_cache;"

      # Sync LSAPI
      command_sequence+="/usr/local/bin/gp stack ols -site-lsapi $(gridpane::conf_read lsapi -site.env "$site_to_clone") $site_to_clone -no-reload;"
      command_sequence+="/usr/local/bin/gp stack ols -site-lsapi-children $(gridpane::conf_read lsapi_children -site.env "$site_to_clone") $site_to_clone -no-reload;"
      command_sequence+="/usr/local/bin/gp stack ols -site-lsapi-app-instances $(gridpane::conf_read lsapi_app_instances -site.env "$site_to_clone") $site_to_clone -no-reload;"
      command_sequence+="/usr/local/bin/gp stack ols -site-lsapi-max-connections $(gridpane::conf_read lsapi_max_connections -site.env "$site_to_clone") $site_to_clone -no-reload;"
      command_sequence+="/usr/local/bin/gp stack ols -site-lsapi-initial-request-timeout $(gridpane::conf_read lsapi_initial_request_timeout -site.env "$site_to_clone") $site_to_clone -no-reload;"
      command_sequence+="/usr/local/bin/gp stack ols -site-lsapi-retry-timeout $(gridpane::conf_read lsapi_retry_timeout -site.env "$site_to_clone") $site_to_clone -no-reload;"
      command_sequence+="/usr/local/bin/gp stack ols -site-lsapi-max-reqs $(gridpane::conf_read lsapi_max_reqs -site.env "$site_to_clone") $site_to_clone -no-reload;"
      command_sequence+="/usr/local/bin/gp stack ols -site-lsapi-max-idle $(gridpane::conf_read lsapi_max_idle -site.env "$site_to_clone") $site_to_clone;"

      # modsec
      local site_modsec_check
      local site_modsec_paranoia
      local site_modsec_threshold
      site_modsec_check="$(/usr/local/bin/gpols get "$site_to_clone" waf)"

      if [[ "$site_modsec_check" = modsec ]]; then
        site_modsec_paranoia="$(/usr/local/bin/gpols get "$site_to_clone" modsec-paranoia)"
        site_modsec_threshold="$(/usr/local/bin/gpols get "$site_to_clone" modsec-threshold)"
        command_sequence+="/usr/local/bin/gp site ${site_to_clone} modsec on;"
        command_sequence+="/usr/local/bin/gp site ${site_to_clone} modsec -paranoia-level $site_modsec_paranoia;"
        command_sequence+="/usr/local/bin/gp site ${site_to_clone} modsec -anomaly-threshold $site_modsec_threshold;"
      fi

      # Domains
      local site_route

      [[ "$cloning_site_config" == *"wildcard-http"* ]] &&
        command_sequence+="/usr/local/bin/gp site ${site_to_clone} -create-wildcard;"

      site_route="$(/usr/local/bin/gpols get "$site_to_clone" route)"
      [[ "$site_route" = www || "$site_route" = root ]] && command_sequence+="/usr/local/bin/gp site ${site_to_clone} -route-domain-${site_route};"

      # shellcheck disable=SC2029
      ssh root@"$remote_IP" "$command_sequence"

      echo "Successfully configured $site_to_clone to remote OpenLiteSpeed server."
    } | tee -a /opt/gridpane/gpclone.log

  fi

  notification_body="Process Complete<br>Clone/Migration to:<br>$remote_IP<br>${site_to_clone}<br> is finished! Please disable any caching and check."
  gridpane::notify::app \
    "Cloning/Migrate Notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-clone" \
    "infinity" \
    "${site_to_clone}"
  echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

  if [[ $2 != "site-clone" ]]; then
    echo "Updating failover_set_at"
    gridpane::callback::app \
      "/site/site-update" \
      "site_url=${site_to_clone}" \
      "server_ip=${remote_IP}" \
      "failover_set_at=now" \
      "silent@True"
  fi

  sleep 1
}

gpclone::process::sync_domains() {
  if [[ $1 == "alias" ]]; then
    domain_env="/var/www/$2/logs/$2-additional-domains.env"
  else
    domain_env="/var/www/$2/logs/$2-301-domains.env"
  fi

  local remote_IP="$3"
  local webserver_remote="$(/usr/bin/ssh root@"$remote_IP" "/usr/local/bin/gp conf read webserver -q")"

  if [[ -f "${domain_env}" ]]; then
    # shellcheck disable=SC2162
    while read add_on_domain_from_original; do
      add_on_domain_from_original=${add_on_domain_from_original#:}
      add_on_domain_from_original=${add_on_domain_from_original%:}

      if [[ "$webserver_remote" = nginx ]]; then
        existing_remote_virtual_servers=$(ssh -n root@"${remote_IP}" "ls /etc/nginx/sites-enabled/${add_on_domain_from_original}")
      else
        existing_remote_virtual_servers=$(ssh -n root@"${remote_IP}" "dir /usr/local/lsws/conf/vhosts")
      fi

      if [[ "$webserver_remote" = nginx &&
            ${existing_remote_virtual_servers} != "/etc/nginx/sites-enabled/${add_on_domain_from_original}" ]] ||
         [[ "$webserver_remote" = openlitespeed &&
            ${existing_remote_virtual_servers} != *" ${add_on_domain_from_original} "* ]]; then

        if [[ -f /var/www/${site_to_clone}/dns/${add_on_domain_from_original}.creds ]]; then
          add_on_domain_creds=$(cat /var/www/"${site_to_clone}"/dns/"${add_on_domain_from_original}".creds)
          if [[ ${add_on_domain_creds} == *"dnsme"* ]]; then
            dns_management="dnsme"
          elif [[ ${add_on_domain_creds} == *"cloudflare"* ]]; then
            dns_management="cloudflare"
          fi
          if [[ ${add_on_domain_creds} == *"challenge-domain"* ]]; then
            dns_management="${dns_management}_challenge"
          else
            dns_management="${dns_management}_full"
          fi
        else
          dns_management="none_none"
        fi

        {
          echo "--------------------------------------------------------------------"
          echo "Adding ${add_on_domain_from_original}"
          echo "Type: $1"
          echo "DNS Management: ${dns_management}"
        } | tee -a /opt/gridpane/gpclone.log

        gridpane::callback::app \
          "callback" \
          "/domain/add-domain" \
          "site_url=${site_to_clone}" \
          "server_ip=${remote_IP}" \
          "domain_url=${add_on_domain_from_original}" \
          "type=$1" \
          "dns_management=${dns_management}"

      fi

      if [[ "$webserver" = nginx && $(cat /etc/nginx/sites-available/"${add_on_domain_from_original}") == *"HTTPS"* ||
            "$webserver" = openlitespeed && "$(/usr/local/bin/gpols get "$add_on_domain_from_original" proto)" = https ]]; then

        RETRY=0
        local domain_added="false"
        while [[ ${domain_added} == "false" ]]; do

          if [[ "$webserver_remote" = nginx ]]; then
            enabled_remote_virtual_servers=$(ssh -n root@"${remote_IP}" "ls /etc/nginx/sites-enabled/${add_on_domain_from_original}")
          else
            enabled_remote_virtual_servers=$(ssh -n root@"${remote_IP}" "dir /usr/local/lsws/conf/vhosts")
          fi

          if [[ ${enabled_remote_virtual_servers} == "/etc/nginx/sites-enabled/${add_on_domain_from_original}" ||
                ${enabled_remote_virtual_servers} == *" ${add_on_domain_from_original} "* ]]; then
            echo "Domain ${add_on_domain_from_original} added to remote ${remote_IP}" | tee -a /opt/gridpane/gpclone.log
            domain_added="true"
          else
            {
              echo "Waiting for domain ${add_on_domain_from_original} to be added to remote ${remote_IP}"
              echo "${enabled_remote_virtual_servers}"
            } | tee -a /opt/gridpane/gpclone.log
            sleep 1
          fi

          RETRY=$(($RETRY + 1))
          if [[ ${RETRY} -gt 300 ]]; then
            echo "Waited 300 seconds for domain ${add_on_domain_from_original} to be added to remote ${remote_IP}... moving on" | tee -a /opt/gridpane/gpclone.log
            break
          fi
        done

        if [[ ${domain_added} == "true" ]]; then

          if [[ "$webserver_remote" = nginx ]]; then
            if [[ $1 == "alias" ]]; then
              gen_remote_config=$(ssh -n root@"${remote_IP}" "sleep 1 && /usr/local/bin/gp conf nginx generate $(/usr/local/bin/gp conf nginx type-check "${add_on_domain_from_original}" return-https -q) $(/usr/local/bin/gp conf nginx cache-check "${site_to_clone}" -q) ${site_to_clone} ${add_on_domain_from_original}")
            else
              gen_remote_config=$(ssh -n root@"${remote_IP}" "sleep 1 && /usr/local/bin/gp conf nginx generate $(/usr/local/bin/gp conf nginx type-check "${add_on_domain_from_original}" return-https -q) ${site_to_clone} ${add_on_domain_from_original}")
            fi
          else
            gen_remote_config=$(ssh -n root@"${remote_IP}" "sleep 1 && /usr/local/bin/gpols site ${add_on_domain_from_original}")
          fi

          if [[ ${gen_remote_config} != *"pain"* ]]; then
            echo "Reconfiguring ${webserver_formatted}..." | tee -a /opt/gridpane/gpclone.log
            local check_remote_https_domain

            if [[ "$webserver_remote" = nginx ]]; then
              check_remote_https_domain=$(ssh -n root@"${remote_IP}" "sleep 1 && cat /etc/nginx/sites-enabled/${add_on_domain_from_original}")
              reload_nginx="true"
            else
              check_remote_https_domain=$(ssh -n root@"${remote_IP}" "sleep 1 && cat /usr/local/lsws/conf/vhosts/${add_on_domain_from_original}/vhconf.conf")
            fi

            if [[ "$webserver_remote" = nginx && ${check_remote_https_domain} == *"HTTPS"* ||
                  "$webserver_remote" = openlitespeed && ${check_remote_https_domain} == *"ssl.conf"* ]]; then
              gridpane::callback::app \
                "callback" \
                "/domain/domain-update" \
                "site_url=${site_to_clone}" \
                "server_ip=${remote_IP}" \
                "domain_url=${add_on_domain_from_original}" \
                "ssl@true" \
                "chain_ssl@false" \
                "status=success" \
                "details=success" \
                "silent@false"
            fi
            check_remote_https_domain=""
          fi
        fi
      fi
    done <"${domain_env}"
  else
    echo "No ${site_to_clone} $1 domains found..." | tee -a /opt/gridpane/gpclone.log
  fi
}

gpclone::process::clone_domains() {
  if [[ $1 == "alias" ]]; then
    domain_env="/var/www/$2/logs/$2-additional-domains.env"
  else
    domain_env="/var/www/$2/logs/$2-301-domains.env"
  fi

  local remote_IP="$3"
  local webserver_remote="$(/usr/bin/ssh root@"$remote_IP" "/usr/local/bin/gp conf read webserver -q")"

  if [[ -f "${domain_env}" ]]; then
    # shellcheck disable=SC2162
    while read add_on_domain_from_original; do
      add_on_domain_from_original=${add_on_domain_from_original#:}
      add_on_domain_from_original=${add_on_domain_from_original%:}

      if [[ -f /var/www/${site_to_clone}/dns/${add_on_domain_from_original}.creds ]]; then
        add_on_domain_creds=$(cat /var/www/"${site_to_clone}"/dns/"${add_on_domain_from_original}".creds)
        if [[ ${add_on_domain_creds} == *"dnsme"* ]]; then
          dns_management="dnsme"
        elif [[ ${add_on_domain_creds} == *"cloudflare"* ]]; then
          dns_management="cloudflare"
        fi
        if [[ ${add_on_domain_creds} == *"challenge-domain"* ]]; then
          dns_management="${dns_management}_challenge"
        else
          dns_management="${dns_management}_full"
        fi
      else
        dns_management="none_none"
      fi

      {
        echo "--------------------------------------------------------------------"
        echo "Adding ${add_on_domain_from_original}"
        echo "Type: $1"
        echo "DNS Management: ${dns_management}"
      } | tee -a /opt/gridpane/gpclone.log

      gridpane::callback::app \
        "callback" \
        "/domain/add-domain" \
        "site_url=${site_to_clone}" \
        "server_ip=${remote_IP}" \
        "domain_url=${add_on_domain_from_original}" \
        "type=$1" \
        "dns_management=${dns_management}"

      if [[ "$webserver" = nginx && $(cat /etc/nginx/sites-available/"${add_on_domain_from_original}") == *"HTTPS"* ||
            "$webserver" = openlitespeed && "$(/usr/local/bin/gpols get "$add_on_domain_from_original" proto)" = https ]]; then

        echo "SSL enabled for ${add_on_domain_from_original} syncing temporary SSL certificates..." | tee -a /opt/gridpane/gpclone.log

        if [[ -d /etc/letsencrypt/archive/${add_on_domain_from_original} ]]; then
          {
            echo "Syncing ${add_on_domain_from_original} Certbot SSL"
            ssh -n root@"${remote_IP}" "sleep 1 && sudo mkdir -p /etc/letsencrypt/archive/${add_on_domain_from_original} && sudo rm -rf /etc/letsencrypt/archive/${add_on_domain_from_original}/*"
            /usr/bin/rsync -avz /etc/letsencrypt/archive/"${add_on_domain_from_original}"/* root@"${remote_IP}":/etc/letsencrypt/archive/"${add_on_domain_from_original}"
          } | tee -a /opt/gridpane/gpclone.log
        fi

        if [[ -d /etc/letsencrypt/live/${add_on_domain_from_original} ]]; then
          {
            ssh -n root@"${remote_IP}" "sleep 1 && sudo mkdir -p /etc/letsencrypt/live/${add_on_domain_from_original} && sudo rm -rf /etc/letsencrypt/live/${add_on_domain_from_original}/*"
            /usr/bin/rsync -avz /etc/letsencrypt/live/"${add_on_domain_from_original}"/* root@"${remote_IP}":/etc/letsencrypt/live/"${add_on_domain_from_original}"
          } | tee -a /opt/gridpane/gpclone.log
        fi

        #TODO (JEff) @cim we need to make this stuff Nginx||OLS <-> Nginx||Ols
        if [[ "$webserver" = nginx ]]; then
          if [[ -d /etc/nginx/ssl/${add_on_domain_from_original} ]]; then
            {
              echo "Syncing ${add_on_domain_from_original} ACME SSL"
              ssh -n root@"${remote_IP}" "sleep 1 && sudo mkdir -p /etc/nginx/ssl/${add_on_domain_from_original} && sudo rm -rf /etc/nginx/ssl/${add_on_domain_from_original}/*"
              /usr/bin/rsync -avz /etc/nginx/ssl/"${add_on_domain_from_original}"/* root@"${remote_IP}":/etc/nginx/ssl/"${add_on_domain_from_original}"
            } | tee -a /opt/gridpane/gpclone.log
          fi
        else
          if [[ -d /usr/local/lsws/conf/cert/${add_on_domain_from_original} ]]; then
            {
              echo "Syncing ${add_on_domain_from_original} ACME SSL"
              ssh -n root@"${remote_IP}" "sleep 1 && sudo mkdir -p /usr/local/lsws/conf/cert/${add_on_domain_from_original} && sudo rm -rf /usr/local/lsws/conf/cert/${add_on_domain_from_original}/*"
              /usr/bin/rsync -avz /usr/local/lsws/conf/cert/"${add_on_domain_from_original}"/* root@"${remote_IP}":/usr/local/lsws/conf/cert/"${add_on_domain_from_original}"
            } | tee -a /opt/gridpane/gpclone.log
          fi
        fi

        if [[ -d /root/gridenv/acme-wildcard-configs/${add_on_domain_from_original} ]]; then
          {
            ssh -n root@"${remote_IP}" "sleep 1 && mkdir -p /root/gridenv/acme-wildcard-configs/${add_on_domain_from_original} && sudo rm -rf /root/gridenv/acme-wildcard-configs/${add_on_domain_from_original}/*"
            /usr/bin/rsync -avz --exclude '*.acme.sh.log' /root/gridenv/acme-wildcard-configs/"${add_on_domain_from_original}"/* root@"${remote_IP}":/root/gridenv/acme-wildcard-configs/"${add_on_domain_from_original}"
          } | tee -a /opt/gridpane/gpclone.log
        fi

        {
          crontab -l >/tmp/tempcron
          rm /tmp/tempcron
          if [[ "$(cat /tmp/tempcron)" == *"/root/gridenv/acme-wildcard-configs/${add_on_domain_from_original}"* ]]; then
            echo "Syncing ${add_on_domain_from_original} ACME CRON"
            echo "${add_on_domain_from_original} CRON"
            min=$((1 + RANDOM % 59))
            hr=$((1 + RANDOM % 23))
            ssh -n root@"${remote_IP}" "sleep 1 && crontab -l > /tmp/tempcron && echo \"${min} ${hr} * * * \"/root/.acme.sh\"/acme.sh --cron --home \"/root/.acme.sh\" --config-home /root/gridenv/acme-wildcard-configs/${add_on_domain_from_original} >>/opt/gridpane/active.acme.monitoring.log 2>&1\" >> /tmp/tempcron && crontab /tmp/tempcron && rm /tmp/tempcron"
          fi
        } | tee -a /opt/gridpane/gpclone.log

        RETRY=0
        local domain_added="false"
        while [[ ${domain_added} == "false" ]]; do

          if [[ "$webserver_remote" = nginx ]]; then
            enabled_remote_virtual_servers=$(ssh -n root@"${remote_IP}" "ls /etc/nginx/sites-enabled/${add_on_domain_from_original}")
          else
            enabled_remote_virtual_servers=$(ssh -n root@"${remote_IP}" "dir /usr/local/lsws/conf/vhosts")
          fi

          if [[ ${enabled_remote_virtual_servers} == "/etc/nginx/sites-enabled/${add_on_domain_from_original}" ||
                ${enabled_remote_virtual_servers} == *" ${add_on_domain_from_original} "* ]]; then
            echo "Domain ${add_on_domain_from_original} added to remote ${remote_IP}" | tee -a /opt/gridpane/gpclone.log
            domain_added="true"
          else
            {
              echo "Waiting for domain ${add_on_domain_from_original} to be added to remote ${remote_IP}"
              echo "${enabled_remote_virtual_servers}"
            } | tee -a /opt/gridpane/gpclone.log
            sleep 1
          fi

          RETRY=$(($RETRY + 1))
          if [[ ${RETRY} -gt 300 ]]; then
            echo "Waited 300 seconds for domain ${add_on_domain_from_original} to be added to remote ${remote_IP}... moving on" | tee -a /opt/gridpane/gpclone.log
            break
          fi
        done

        if [[ ${domain_added} == "true" ]]; then

          if [[ "$webserver_remote" = nginx ]]; then
            if [[ $1 == "alias" ]]; then
              gen_remote_config=$(ssh -n root@"${remote_IP}" "sleep 1 && /usr/local/bin/gp conf nginx generate $(/usr/local/bin/gp conf nginx type-check "${add_on_domain_from_original}" return-https -q) $(/usr/local/bin/gp conf nginx cache-check "${site_to_clone}" -q) ${site_to_clone} ${add_on_domain_from_original}")
            else
              gen_remote_config=$(ssh -n root@"${remote_IP}" "sleep 1 && /usr/local/bin/gp conf nginx generate $(/usr/local/bin/gp conf nginx type-check "${add_on_domain_from_original}" return-https -q) ${site_to_clone} ${add_on_domain_from_original}")
            fi
          else
            gen_remote_config=$(ssh -n root@"${remote_IP}" "sleep 1 && /usr/local/bin/gpols site ${add_on_domain_from_original}")
          fi

          echo "${gen_remote_config}" | tee -a /opt/gridpane/gpclone.log

          if [[ ${gen_remote_config} != *"pain"* ]]; then
            echo "Reconfiguring ${webserver_formatted}..." | tee -a /opt/gridpane/gpclone.log
            local check_remote_https_domain
            if [[ "$webserver_remote" = nginx ]]; then
              check_remote_https_domain=$(ssh -n root@"${remote_IP}" "sleep 1 && cat /etc/nginx/sites-enabled/${add_on_domain_from_original}")
              reload_nginx="true"
            else
              check_remote_https_domain=$(ssh -n root@"${remote_IP}" "sleep 1 && cat /usr/local/lsws/conf/vhosts/${add_on_domain_from_original}/vhconf.conf")
            fi

            if [[ "$webserver_remote" = nginx && ${check_remote_https_domain} == *"HTTPS"* ||
                  "$webserver_remote" = openlitespeed && ${check_remote_https_domain} == *"ssl.conf"* ]]; then
              gridpane::callback::app \
                "callback" \
                "/domain/domain-update" \
                "site_url=${site_to_clone}" \
                "server_ip=${remote_IP}" \
                "domain_url=${add_on_domain_from_original}" \
                "ssl@true" \
                "chain_ssl@false" \
                "status=success" \
                "details=success" \
                "silent@false"
            fi
            check_remote_https_domain=""
          fi
        fi
      fi
    done <"${domain_env}"
  else
    echo "No ${site_to_clone} $1 domains found..." | tee -a /opt/gridpane/gpclone.log
  fi
}

gpclone::add::remote_site() {
  if [[ -z $1 ||
        -z $2 ]]; then
    gpclone::end::failsync_pause::exit "We need the original site url and the remote IP address passing... exiting"
  fi

  local site_to_clone="$1"
  local failover="$3"

  local php_version
  php_version="$(gridpane::get::site::php "${site_to_clone}")"

  # TODO (JEFF) we need to replace the site table caching value from nginx_caching to server_caching
  # to allow for litespeed caching options... needs to go in app ticket.
  local nginx_caching
  local config_type
  if [[ "$webserver" = nginx ]]; then
    nginx_caching="$(gridpane::check::site::nginx::cache "${site_to_clone}")"
    config_type="$(gridpane::check::site::nginx::config "${site_to_clone}")"
  else
    nginx_caching="$(/usr/local/bin/gpols get "$site_to_clone" cache)"
    config_type="$(/usr/local/bin/gpols get "$site_to_clone" proto)"
  fi

  local dns_management
  dns_management=$(gpclone::process::site_dns "${site_to_clone}")

  local waf
  waf="$(gridpane::conf_read waf -site.env "${site_to_clone}")"
  local waf_key_value
  case $waf in
    6G|6g) waf_key_value="waf=6G" ;;
    7G|7g) waf_key_value="waf=7G" ;;
    modsec) waf_key_value="waf=modsec" ;;
  esac

  local smtp
  smtp="$(gridpane::conf_read smtp -site.env "${site_to_clone}")"
  local smtp_key_value
  [[ ${smtp} == *"true"* || ${smtp} == *"on"* ]] &&
    smtp_key_value="smtp=sendgrid"

  local php_pm_key_value
  if [[ "$webserver" = nginx ]]; then
    local php_pm
    php_pm=$(gridpane::conf_read pm -site.env "${site_to_clone}")
    case $php_pm in
      dynamic|static|ondemand) php_pm_key_value="pm=${php_pm}" ;;
    esac
  fi

  local local_sys_user
  local_sys_user="$(gridpane::get::set::site::user "${site_to_clone}")"

  if [[ -d "/var/www/staging.${site_to_clone}" && -d "/var/www/canary.${site_to_clone}" ]]; then
    echo "Unsynced site ${site_to_clone} found with staging and updates, building three remote sites..." | tee -a /opt/gridpane/gpclone.log
    if [[ ${failover} == "true" ]]; then
      local local_sys_user
      local_sys_user="$(gridpane::get::set::site::user "${site_to_clone}")"
      # TODO (JEFF) we need to replace the site table caching value from nginx_caching to server_caching
      # then adjust callback accordingly...
      # app will need to be able to know if it receives payload of nginx_caching to update server_caching in db
      gpcurl=$(gridpane::callback::app \
        "/add-site" \
        "--" \
        "url=${site_to_clone}" \
        "-s" "server_ip=${remote_IP}" \
        "-s" "source_ip=${serverIP}" \
        "dns_management=${dns_management}" \
        "-s" "php_version=${php_version}" \
        "nginx_caching=${nginx_caching}" \
        "-s" "failover=${failover}" \
        "checkedAdvancedOptions[]=staging" \
        "checkedAdvancedOptions[]=canary" \
        "username=${local_sys_user}" \
        "${waf_key_value}" "${smtp_key_value}" "${php_pm_key_value}" \
        2>&1
      )

      echo "This is a Snapshot Failover Sync... creating synced-to/synced-from logs.." | tee -a /opt/gridpane/gpclone.log

      mkdir -p /var/www/staging."${site_to_clone}"/logs
      touch /var/www/staging."${site_to_clone}"/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/staging."${site_to_clone}"/logs/synced-to.log

      mkdir -p /var/www/canary."${site_to_clone}"/logs
      touch /var/www/canary."${site_to_clone}"/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/canary."${site_to_clone}"/logs/synced-to.log

      mkdir -p /var/www/"${site_to_clone}"/logs
      touch /var/www/"${site_to_clone}"/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/"${site_to_clone}"/logs/synced-to.log

      # shellcheck disable=SC2029
      ssh root@"${remote_IP}" "mkdir -p /var/www/staging.${site_to_clone}/logs && touch /var/www/staging.${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/staging.${site_to_clone}/logs/synced-from.log"
      # shellcheck disable=SC2029
      ssh root@"${remote_IP}" "mkdir -p /var/www/canary.${site_to_clone}/logs && touch /var/www/canary.${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/canary.${site_to_clone}/logs/synced-from.log"
      # shellcheck disable=SC2029
      ssh root@"${remote_IP}" "mkdir -p /var/www/${site_to_clone}/logs && touch /var/www/${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/${site_to_clone}/logs/synced-from.log"
    else
      local local_sys_user
      local_sys_user="$(gridpane::get::set::site::user "${site_to_clone}")"
      # TODO (JEFF) we need to replace the site table caching value from nginx_caching to server_caching
      # then adjust callback accordingly...
      # app will need to be able to know if it receives payload of nginx_caching to update server_caching in db
      gpcurl=$(gridpane::callback::app \
        "/add-site" \
        "--" \
        "url=${site_to_clone}" \
        "-s" "server_ip=${remote_IP}" \
        "-s" "source_ip=${serverIP}" \
        "dns_management=${dns_management}" \
        "-s" "php_version=${php_version}" \
        "nginx_caching=${nginx_caching}" \
        "checkedAdvancedOptions[]=staging" \
        "checkedAdvancedOptions[]=canary" \
        "username=${local_sys_user}" \
        "${waf_key_value}" "${smtp_key_value}" "${php_pm_key_value}" \
        2>&1
      )

      echo "This is NOT a Snapshot Failover Sync, this is a migration, not creating synced-to/synced-from logs..." | tee -a /opt/gridpane/gpclone.log
    fi

    echo "${gpcurl}" | tee -a /opt/gridpane/gpclone.log
    sleep 1
  elif [[ -d "/var/www/staging.${site_to_clone}" ]]; then
    echo "Unsynced site ${site_to_clone} found with staging, building two remote sites..." | tee -a /opt/gridpane/gpclone.log
    if [[ ${failover} == "true" ]]; then
      local local_sys_user
      local_sys_user="$(gridpane::get::set::site::user "${site_to_clone}")"
      # TODO (JEFF) we need to replace the site table caching value from nginx_caching to server_caching
      # then adjust callback accordingly...
      # app will need to be able to know if it receives payload of nginx_caching to update server_caching in db
      gpcurl=$(gridpane::callback::app \
        "/add-site" \
        "--" \
        "url=${site_to_clone}" \
        "-s" "server_ip=${remote_IP}" \
        "-s" "source_ip=${serverIP}" \
        "dns_management=${dns_management}" \
        "-s" "php_version=${php_version}" \
        "nginx_caching=${nginx_caching}" \
        "-s" "failover=${failover}" \
        "checkedAdvancedOptions[]=staging" \
        "username=${local_sys_user}" \
        "${waf_key_value}" "${smtp_key_value}" "${php_pm_key_value}" \
        2>&1
      )

      echo "This is a Snapshot Failover Sync... creating synced-to/synced-from logs.." | tee -a /opt/gridpane/gpclone.log

      mkdir -p /var/www/staging."${site_to_clone}"/logs
      touch /var/www/staging."${site_to_clone}"/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/staging."${site_to_clone}"/logs/synced-to.log

      mkdir -p /var/www/"${site_to_clone}"/logs
      touch /var/www/"${site_to_clone}"/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/"${site_to_clone}"/logs/synced-to.log

      # shellcheck disable=SC2029
      ssh root@"${remote_IP}" "mkdir -p /var/www/staging.${site_to_clone}/logs && touch /var/www/staging.${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/staging.${site_to_clone}/logs/synced-from.log"
      # shellcheck disable=SC2029
      ssh root@"${remote_IP}" "mkdir -p /var/www/${site_to_clone}/logs && touch /var/www/${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/${site_to_clone}/logs/synced-from.log"
    else
      local local_sys_user
      local_sys_user="$(gridpane::get::set::site::user "${site_to_clone}")"
      # TODO (JEFF) we need to replace the site table caching value from nginx_caching to server_caching
      # then adjust callback accordingly...
      # app will need to be able to know if it receives payload of nginx_caching to update server_caching in db
      gpcurl=$(gridpane::callback::app \
        "/add-site" \
        "--" \
        "url=${site_to_clone}" \
        "-s" "server_ip=${remote_IP}" \
        "-s" "source_ip=${serverIP}" \
        "dns_management=${dns_management}" \
        "-s" "php_version=${php_version}" \
        "nginx_caching=${nginx_caching}" \
        "checkedAdvancedOptions[]=staging" \
        "username=${local_sys_user}" \
        "${waf_key_value}" "${smtp_key_value}" "${php_pm_key_value}" \
        2>&1
      )

      echo "This is NOT a Snapshot Failover Sync, this is a migration, not creating synced-to/synced-from logs..." | tee -a /opt/gridpane/gpclone.log
    fi

    echo "${gpcurl}" | tee -a /opt/gridpane/gpclone.log
    sleep 1
  elif [[ -d "/var/www/canary.${site_to_clone}" ]]; then
    echo "Unsynced site ${site_to_clone} found with updates, building two remote sites..."
    if [[ ${failover} == "true" ]]; then
      local local_sys_user
      local_sys_user="$(gridpane::get::set::site::user "${site_to_clone}")"
      # TODO (JEFF) we need to replace the site table caching value from nginx_caching to server_caching
      # then adjust callback accordingly...
      # app will need to be able to know if it receives payload of nginx_caching to update server_caching in db
      gpcurl=$(gridpane::callback::app \
        "/add-site" \
        "--" \
        "url=${site_to_clone}" \
        "-s" "server_ip=${remote_IP}" \
        "-s" "source_ip=${serverIP}" \
        "dns_management=${dns_management}" \
        "-s" "php_version=${php_version}" \
        "nginx_caching=${nginx_caching}" \
        "-s" "failover=${failover}" \
        "checkedAdvancedOptions[]=canary" \
        "username=${local_sys_user}" \
        "${waf_key_value}" "${smtp_key_value}" "${php_pm_key_value}" \
        2>&1
      )

      echo "This is a Snapshot Failover Sync... creating synced-to/synced-from logs.." | tee -a /opt/gridpane/gpclone.log

      mkdir -p /var/www/canary."${site_to_clone}"/logs
      touch /var/www/canary."${site_to_clone}"/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/canary."${site_to_clone}"/logs/synced-to.log

      mkdir -p /var/www/"${site_to_clone}"/logs
      touch /var/www/"${site_to_clone}"/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/"${site_to_clone}"/logs/synced-to.log

      # shellcheck disable=SC2029
      ssh root@"${remote_IP}" "mkdir -p /var/www/canary.${site_to_clone}/logs && touch /var/www/canary.${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/canary.${site_to_clone}/logs/synced-from.log"
      # shellcheck disable=SC2029
      ssh root@"${remote_IP}" "mkdir -p /var/www/${site_to_clone}/logs && touch /var/www/${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/${site_to_clone}/logs/synced-from.log"
    else
      local local_sys_user
      local_sys_user="$(gridpane::get::set::site::user "${site_to_clone}")"
      # TODO (JEFF) we need to replace the site table caching value from nginx_caching to server_caching
      # then adjust callback accordingly...
      # app will need to be able to know if it receives payload of nginx_caching to update server_caching in db
      gpcurl=$(gridpane::callback::app \
        "/add-site" \
        "--" \
        "url=${site_to_clone}" \
        "-s" "server_ip=${remote_IP}" \
        "-s" "source_ip=${serverIP}" \
        "dns_management=${dns_management}" \
        "-s" "php_version=${php_version}" \
        "nginx_caching=${nginx_caching}" \
        "checkedAdvancedOptions[]=canary" \
        "username=${local_sys_user}" \
        "${waf_key_value}" "${smtp_key_value}" "${php_pm_key_value}" \
        2>&1
      )

      echo "This is NOT a Snapshot Failover Sync, this is a migration, not creating synced-to/synced-from logs..." | tee -a /opt/gridpane/gpclone.log
    fi

    echo "${gpcurl}" | tee -a /opt/gridpane/gpclone.log
    sleep 1
  else
    echo "Unsynced site ${site_to_clone} found with no staging or updates, building one remote site..." | tee -a /opt/gridpane/gpclone.log
    if [[ ${failover} == "true" ]]; then
      local local_sys_user
      local_sys_user="$(gridpane::get::set::site::user "${site_to_clone}")"
      # TODO (JEFF) we need to replace the site table caching value from nginx_caching to server_caching
      # then adjust callback accordingly...
      # app will need to be able to know if it receives payload of nginx_caching to update server_caching in db
      gpcurl=$(gridpane::callback::app \
        "/add-site" \
        "--" \
        "url=${site_to_clone}" \
        "-s" "server_ip=${remote_IP}" \
        "-s" "source_ip=${serverIP}" \
        "dns_management=${dns_management}" \
        "-s" "php_version=${php_version}" \
        "nginx_caching=${nginx_caching}" \
        "-s" "failover=${failover}" \
        "username=${local_sys_user}" \
        "${waf_key_value}" "${smtp_key_value}" "${php_pm_key_value}" \
        2>&1
      )

      echo "This is a Snapshot Failover Sync... creating synced-to/synced-from logs.." | tee -a /opt/gridpane/gpclone.log

      mkdir -p /var/www/"${site_to_clone}"/logs
      touch /var/www/"${site_to_clone}"/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/"${site_to_clone}"/logs/synced-to.log

      # shellcheck disable=SC2029
      ssh root@"${remote_IP}" "mkdir -p /var/www/${site_to_clone}/logs && touch /var/www/${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/${site_to_clone}/logs/synced-from.log"
    else
      local local_sys_user
      local_sys_user="$(gridpane::get::set::site::user "${site_to_clone}")"
      # TODO (JEFF) we need to replace the site table caching value from nginx_caching to server_caching
      # then adjust callback accordingly...
      # app will need to be able to know if it receives payload of nginx_caching to update server_caching in db
      gpcurl=$(gridpane::callback::app \
        "/add-site" \
        "--" \
        "url=${site_to_clone}" \
        "-s" "server_ip=${remote_IP}" \
        "-s" "source_ip=${serverIP}" \
        "dns_management=${dns_management}" \
        "-s" "php_version=${php_version}" \
        "nginx_caching=${nginx_caching}" \
        "username=${local_sys_user}" \
        "${waf_key_value}" "${smtp_key_value}" "${php_pm_key_value}" \
        2>&1
      )

      echo "This is NOT a Snapshot Failover Sync, this is a migration, not creating synced-to/synced-from logs..." | tee -a /opt/gridpane/gpclone.log
    fi

    echo "${gpcurl}" | tee -a /opt/gridpane/gpclone.log
    sleep 1
  fi
}

gpclone::add::new_site() {
  if [[ -z $1 ||
        -z $2 ]]; then
    gpclone::end::failsync_pause::exit "We need to know if you want to make a site on the same server or a new server, and the url of the site to make... exiting"
  fi

  same_or_different_server="$1"
  site_to_add="$2"

  local dns_management

  if [[ -f /var/www/${site_to_clone}/logs/template.site ]]; then
    dns_management=$(grep -w "dns-integration:.*" "/var/www/${site_to_clone}/logs/template.site" | cut -f 2 -d ':') || true
  else
    dns_management=$(gpclone::process::site_dns "${site_to_clone}")
  fi

  [[ -d /var/www/${site_to_add}/htdocs ]] &&
    gridpane::conf_write dns-management "${dns_management}" -site.env "${site_to_add}"

  if [[ ${same_or_different_server} == *"same-server" ]]; then
    server_IP="${serverIP}"
    source_IP="${serverIP}"
  elif [[ ${same_or_different_server} == *"different-server" ]]; then
    [[ -z $3 ]] &&
      gpclone::end::failsync_pause::exit "No remote IP passed, we can't make this site on a different server... exiting..."

    check_ip=$(gridpane::ip_check "$3" "clone")
    echo "${check_ip}" | tee -a /opt/gridpane/gpclone.log
    server_IP="$3"
    source_IP="${serverIP}"
  else
    gpclone::end::failsync_pause::exit "Unsure if you want to clone to the same server or a different server, exiting..."
  fi

  echo "server_ip ${server_IP}"
  echo "source_ip ${source_IP}"
  echo "dns_management ${dns_management}"
  echo "url ${site_to_add}"

  local php_version
  php_version="$(gridpane::get::site::php "${site_to_clone}")"

  # TODO (JEFF) we need to replace the site table caching value from nginx_caching to server_caching
  # then adjust callback accordingly...
  local nginx_caching
  local config_type
  if [[ "$webserver" = nginx ]]; then
    nginx_caching="$(gridpane::check::site::nginx::cache "${site_to_clone}")"
    config_type="$(gridpane::check::site::nginx::config "${site_to_clone}")"
  else
    nginx_caching="$(/usr/local/bin/gpols get "$site_to_clone" cache)"
    config_type="$(/usr/local/bin/gpols get "$site_to_clone" proto)"
  fi

  echo "PHP ${php_version}"
  echo "Caching ${nginx_caching}"

  local waf
  waf="$(gridpane::conf_read waf -site.env "${site_to_clone}")"
  local waf_key_value
  case $waf in
    6G|6g) waf_key_value="waf=6G" ;;
    7G|7g) waf_key_value="waf=7G" ;;
    modsec) waf_key_value="waf=modsec" ;;
  esac

  local smtp
  smtp="$(gridpane::conf_read smtp -site.env "${site_to_clone}")"
  local smtp_key_value
  [[ ${smtp} == *"true"* || ${smtp} == *"on"* ]] &&
    smtp_key_value="smtp=sendgrid"

  local php_pm_key_value
  if [[ "$webserver" = nginx ]]; then
    local php_pm
    php_pm=$(gridpane::conf_read pm -site.env "${site_to_clone}")
    case $php_pm in
      dynamic|static|ondemand) php_pm_key_value="pm=${php_pm}" ;;
    esac
  fi

  local local_sys_user
  local_sys_user="$(gridpane::get::set::site::user "${site_to_clone}")"
  # TODO (JEFF) we need to replace the site table caching value from nginx_caching to server_caching
  # then adjust callback accordingly...
  # app will need to be able to know if it receives payload of nginx_caching to update server_caching in db
  # shellcheck disable=SC2090,SC2086
   gridpane::callback::app \
      "callback" \
      "/add-site" \
      "--" \
      "url=${site_to_add}" \
      "-s" "server_ip=${server_IP}" \
      "-s" "source_ip=${source_IP}" \
      "dns_management=${dns_management}" \
      "-s" "php_version=${php_version}" \
      "nginx_caching=${nginx_caching}" \
      "username=${local_sys_user}" \
      ${waf_key_value} ${smtp_key_value} ${php_pm_key_value} | tee -a /opt/gridpane/gpclone.log
}

gpclone::process::site_dns() {
  local dns_creds_file
  local dns_management
  if [[ -f /var/www/"$1"/dns/"$1".creds ]]; then
    dns_creds_file=$(cat /var/www/"$1"/dns/"$1".creds)
    if [[ ${dns_creds_file} == *"dnsme"* ]]; then
      dns_management="dnsme"
    elif [[ ${dns_creds_file} == *"cloudflare"* ]]; then
      dns_management="cloudflare"
    fi
    if [[ ${dns_creds_file} == *"challenge"* ]]; then
      dns_management="${dns_management}_challenge"
    else
      dns_management="${dns_management}_full"
    fi
  else
    dns_management="none_none"
  fi
  echo "$dns_management"
  local quiet_store
  quiet_store=$(gridpane::conf_write dns-management "${dns_management}" -site.env "$1")
}

gpclone::extract::replace()  {
  if [[ -z $1 ||
        -z $2 ]]; then
    gpclone::end::failsync_pause::exit "We need to know the source and destination urls.. exiting..."
  fi

  source="$1"
  dest="$2"
  is_multisite="$3"
  dest_siteowner=$(gridpane::get::set::site::user "${dest}")

  if [[ ! -f /var/www/"${dest}"/GPBUP-"${source}"-CLONE.gz ]]; then
    gpclone::end::failsync_pause::exit "Error! Archive missing, Exiting!!!"
  fi

  rm -rf /var/www/"${dest}"/htdocs/*
  tar -xf /var/www/"${dest}"/GPBUP-"${source}"-CLONE.gz -C /var/www/"${dest}"/htdocs --overwrite | tee -a /opt/gridpane/gpclone.log

  {
      echo "--------------------------------------------------------------------"
      echo "Files extracted to ${dest}"
      echo "--------------------------------------------------------------------"
      echo "/var/www/${dest}/htdocs:"
      echo "--------------------------------------------------------------------"
      ls -l /var/www/"${dest}"/htdocs
      echo "--------------------------------------------------------------------"
      echo "/var/www/${dest}/htdocs/wp-content/plugins:"
      echo "--------------------------------------------------------------------"
      ls -l "/var/www/${dest}/htdocs/wp-content/plugins"
      echo "--------------------------------------------------------------------"
      echo "/var/www/${dest}/htdocs/wp-content/mu-plugins:"
      echo "--------------------------------------------------------------------"
      ls -l "/var/www/${dest}/htdocs/wp-content/mu-plugins"
      if [[ -e /var/www/${dest}/htdocs/wp-content/mu-plugins/wp-fail2ban.php &&
           ${dest} != "${source}" ]]; then
        echo "Removing incorrect symlink:"
        echo "wp-fail2ban.php -> /var/www/${source}/htdocs/wp-content/plugins/wp-fail2ban/wp-fail2ban.php"
        rm /var/www/"${dest}"/htdocs/wp-content/mu-plugins/wp-fail2ban.php
      fi
      echo "--------------------------------------------------------------------"
      echo "/var/www/${dest}/htdocs/wp-content/themes:"
      echo "--------------------------------------------------------------------"
      ls -l "/var/www/${dest}/htdocs/wp-content/themes"
      echo "--------------------------------------------------------------------"
    } | tee -a /opt/gridpane/gpclone.log

  echo "Move clone env..." | tee -a /opt/gridpane/gpclone.log

  mv /var/www/"${dest}"/htdocs/"${source}".env.clone /var/www/"${dest}"/logs/"${source}".env.clone
  chown -R "${dest_siteowner}":"${dest_siteowner}" /var/www/"${dest}"/htdocs/
  # shellcheck disable=SC2164
  cd /var/www/"${dest}"/htdocs

  gridpane::check::wordfence_bork \
    "${dest}" \
    "${source}" | tee -a /opt/gridpane/gpclone.log

  gridpane::check::mu_symlink \
    "${dest}" \
    "wp-fail2ban.php" \
    "/var/www/${source}/htdocs/wp-content/plugins/wp-fail2ban/wp-fail2ban.php" \
    "/var/www/${dest}/htdocs/wp-content/plugins/wp-fail2ban/wp-fail2ban.php" | tee -a /opt/gridpane/gpclone.log

  tableprefix=$(cat /var/www/"${dest}"/htdocs/table.prefix)
  if [[ -z ${tableprefix} ]]; then
    echo "Table prefix missing, sticking to the defaults..." | tee -a /opt/gridpane/gpclone.log
  else
    sed -i "/\$table_prefix =/c\\${tableprefix}" /var/www/"${dest}"/wp-config.php
    echo "Table Prefixes Updated..." | tee -a /opt/gridpane/gpclone.log
  fi

  if [[ -f /var/www/"${dest}"/htdocs/database.gz ]]; then
    echo "Extracting /var/www/${dest}/htdocs/database.gz DB and importing now..." | tee -a /opt/gridpane/gpclone.log
    tar -xzf /var/www/"${dest}"/htdocs/database.gz | tee -a /opt/gridpane/gpclone.log
  elif [[ -f /var/www/"${dest}"/database.gz ]]; then
    echo "Extracting /var/www/${dest}/database.gz DB and importing now..." | tee -a /opt/gridpane/gpclone.log
    tar -xzf /var/www/"${dest}"/database.gz | tee -a /opt/gridpane/gpclone.log
    mv /var/www/"${dest}"/database.sql /var/www/"${dest}"/htdocs/database.sql
  fi

  if [[ ! -f /var/www/"${dest}"/htdocs/database.sql &&
        ! -d /var/www/"${dest}"/htdocs/database-${clone_time_epoch} ]]; then
    notification_body="Process Failed<br>Source:${source}<br>Destination:${dest}<br>Source Server:${serverIP}<br>Destination Server:${serverIP}<br>Database File is missing..."
    gridpane::notify::app \
      "Cloning/Migrate Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
  fi

  {
    if [[ -d /var/www/"${dest}"/htdocs/database-${clone_time_epoch} ]]; then
      echo "Importing DB from /var/www/${dest}/htdocs/database-${clone_time_epoch} now..."
    elif [[ -f /var/www/"${dest}"/htdocs/database.sql ]]; then
      echo "Importing DB from /var/www/${dest}/htdocs/database.sql now..."
    else
      echo "Missing DB source... this is gonna fail..."
    fi
  } | tee -a /opt/gridpane/gpclone.log

  if [[ -f /var/www/"${dest}"/htdocs/database.sql ||
        -d /var/www/"${dest}"/htdocs/database-${clone_time_epoch} ]]; then
    {
      echo "Drop Database for ${dest}..."
      sudo su - "${dest_siteowner}" -c "timeout 300 /usr/local/bin/wp db drop --yes --path=/var/www/${dest}/htdocs 2>&1"
      echo "Creating new Database for ${dest}..."
      sudo su - "${dest_siteowner}" -c "timeout 300 /usr/local/bin/wp db create --path=/var/www/${dest}/htdocs 2>&1"
      echo "Importing ${source} db to ${dest}"
    } | tee -a /opt/gridpane/gpclone.log
  fi

  local import_db
  local import_db_status
  if [[ -d /var/www/"${dest}"/htdocs/database-${clone_time_epoch} ]]; then
    local wp_db_name
    local wp_db_user
    local wp_db_pass
    local wp_db_host
    local wp_db_port
    # shellcheck disable=SC2002
    wp_db_name=$(cat /var/www/"${dest}"/wp-config.php | grep DB_NAME | cut -d \' -f 4)
    # shellcheck disable=SC2002
    wp_db_user=$(cat /var/www/"${dest}"/wp-config.php | grep DB_USER | cut -d \' -f 4)
    # shellcheck disable=SC2002
    wp_db_pass=$(cat /var/www/"${dest}"/wp-config.php | grep DB_PASSWORD | cut -d \' -f 4)
    # shellcheck disable=SC2002
    wp_db_host=$(cat "/var/www/${dest}/wp-config.php" | grep DB_HOST | tr \" \' | cut -d \' -f 4)
    wp_db_port=$(echo "${wp_db_host}" | awk -F':' '{print $2}')
    wp_db_port=${wp_db_port:-3306}

    chown "${dest_siteowner}":"${dest_siteowner}" /var/www/"${dest}"/htdocs/database-"${clone_time_epoch}"
    import_db=$(sudo su - "${dest_siteowner}" -c "timeout --preserve-status 7200 myloader -v 3 -d /var/www/${dest}/htdocs/database-${clone_time_epoch} -B ${wp_db_name} -h "${wp_db_host}" -P "${wp_db_port}" -u ${wp_db_user} -p ${wp_db_pass} -o 2>&1")
    import_db_status=$?
  elif [[ -f /var/www/"${dest}"/htdocs/database.sql ]]; then
    chown "${dest_siteowner}":"${dest_siteowner}" /var/www/"${dest}"/htdocs/database.sql
    import_db=$(sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp db import /var/www/${dest}/htdocs/database.sql --path=/var/www/${dest}/htdocs 2>&1")
    import_db_response=$?
  fi

  echo "${import_db}" | tee -a /opt/gridpane/gpclone.log

  if [[ ${import_db_status} == 0 ]]; then
    echo "Database Imported..." | tee -a /opt/gridpane/gpclone.log
    chmod 600 /var/www/"${dest}"/wp-config.php

    [[ -f /var/www/"${dest}"/htdocs/database.gz ]] &&
      rm /var/www/"${dest}"/htdocs/database.gz
    [[ -d /var/www/"${dest}"/htdocs/database-${clone_time_epoch} ]] &&
      rm -rf /var/www/"${dest}"/htdocs/database-${clone_time_epoch}
    [[ -f /var/www/"${dest}"/htdocs/database.sql ]] &&
      rm /var/www/"${dest}"/htdocs/database.sql
    [[ -f /var/www/"$dest"/htdocs/table.prefix ]] &&
      rm /var/www/"$dest"/htdocs/table.prefix
    [[ -f /var/www/"$dest"/GPBUP-"$source"-CLONE.gz ]] &&
      rm /var/www/"$dest"/GPBUP-"$source"-CLONE.gz
    # @jeff is this the correct path? GPBUP-"$source"-CLONE.gz doesn't show up in htdocs
    # will leave it here for now
    [[ -f /var/www/"$dest"/htdocs/GPBUP-"$source"-CLONE.gz ]] &&
      rm /var/www/"$dest"/htdocs/GPBUP-"$source"-CLONE.gz

    gridpane::check::search_replace_db
    if [[ -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
      # shellcheck disable=SC2002,SC2155
      local db_name=$(cat /var/www/"${dest}"/wp-config.php | grep DB_NAME | tr \" \' | cut -d \' -f 4)
      # shellcheck disable=SC2002,SC2155
      local db_user=$(cat /var/www/"${dest}"/wp-config.php | grep DB_USER | tr \" \' | cut -d \' -f 4)
      # shellcheck disable=SC2002,SC2155
      local db_pass=$(cat /var/www/"${dest}"/wp-config.php | grep DB_PASSWORD | tr \" \' | cut -d \' -f 4)
      # shellcheck disable=SC2002
      local db_host=$(cat "/var/www/${dest}/wp-config.php" | grep DB_HOST | tr \" \' | cut -d \' -f 4)
      local db_port=$(echo ${db_host} | awk -F':' '{print $2}')
      local db_port=${db_port:-3306}
    fi

    local search_replace_db_output
    local scheme
    scheme="http"
    {
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - http://${source} -> ${scheme}://${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'http://${source}' '${scheme}://${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "http://${source}" -r "${scheme}://${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'http://${source}' '${scheme}://${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - http://www.${source} -> ${scheme}://${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'http://www.${source}' '${scheme}://${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "http://www.${source}" -r "${scheme}://${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'http://www.${source}' '${scheme}://${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - http:\/\/${source} -> ${scheme}:\/\/${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'http:\/\/${source}' '${scheme}:\/\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "http:\\\/\\\/${source}" -r "${scheme}:\\\/\\\/${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'http:\/\/${source}' '${scheme}:\/\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - http:\/\/www.${source} -> ${scheme}:\/\/${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'http:\/\/www.${source}' '${scheme}:\/\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "http:\\\/\\\/www.${source}" -r "${scheme}:\\\/\\\/${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'http:\/\/www.${source}' '${scheme}:\/\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - http%3A%2F%2F${source} -> ${scheme}%3A%2F%2F${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'http%3A%2F%2F${source}' '${scheme}%3A%2F%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "http%3A%2F%2F${source}" -r "${scheme}%3A%2F%2F${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'http%3A%2F%2F${source}' '${scheme}%3A%2F%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - http%3A%2F%2Fwww.${source} -> ${scheme}%3A%2F%2F${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'http%3A%2F%2Fwww.${source}' '${scheme}%3A%2F%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "http%3A%2F%2Fwww.${source}" -r "${scheme}%3A%2F%2F${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'http%3A%2F%2Fwww.${source}' '${scheme}%3A%2F%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then\
        echo "Search replace - https://${source} -> ${scheme}://${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'https://${source}' '${scheme}://${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "https://${source}" -r "${scheme}://${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'https://${source}' '${scheme}://${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - https://www.${source} -> ${scheme}://${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'https://www.${source}' '${scheme}://${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "https://www.${source}" -r "${scheme}://${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'https://www.${source}' '${scheme}://${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - https:\/\/${source} -> ${scheme}:\/\/${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'https:\/\/${source}' '${scheme}:\/\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "https:\\\/\\\/${source}" -r "${scheme}:\\\/\\\/${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'https:\/\/${source}' '${scheme}:\/\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - https:\/\/www.${source} -> ${scheme}:\/\/${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'https:\/\/www.${source}' '${scheme}:\/\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "https:\\\/\\\/www.${source}" -r "${scheme}:\\\/\\\/${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'https:\/\/www.${source}' '${scheme}:\/\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - https%3A%2F%2F${source} -> ${scheme}%3A%2F%2F${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'https%3A%2F%2F${source}' '${scheme}%3A%2F%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "https%3A%2F%2F${source}" -r "${scheme}%3A%2F%2F${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'https%3A%2F%2F${source}' '${scheme}%3A%2F%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - https%3A%2F%2Fwww.${source} -> ${scheme}%3A%2F%2F${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'https%3A%2F%2Fwww.${source}' '${scheme}%3A%2F%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "https%3A%2F%2Fwww.${source}" -r "${scheme}%3A%2F%2F${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'https%3A%2F%2Fwww.${source}' '${scheme}%3A%2F%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - www/${source} -> www/${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'www/${source}' 'www/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "www/${source}" -r "www/${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'www/${source}' 'www/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - www\/${source} -> www\/${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'www\/${source}' 'www\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "www\\\/${source}" -r "www\\\/${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'www\/${source}' 'www\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - www%2F${source} -> www%2F${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'www%2F${source}' 'www%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "www%2F${source}" -r "www%2F${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace 'www%2F${source}' 'www%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - @${source} -> @${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace '@${source}' '@${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "@${source}" -r "@${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace '@${source}' '@${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
        echo "Search replace - %40${source} -> %40${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace '%40${source}' '%40${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      else
        search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "%40${source}" -r "%40${dest}" --exclude-cols=guid --allow-old-php 2>&1)
        if [[ $? != 0 ]]; then
          echo "${search_replace_db_output}"
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace '%40${source}' '%40${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          gridpane::grep_output::search_replace_db "${search_replace_db_output}"
        fi
      fi
      if [[ ${is_multisite} == "true"* ]]; then
        echo "Multisite Clone Search Replace - WP_SITE WP_BLOGS Tables"
        echo "Pattern to search replace:  ${source} -> ${dest}"
        if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
          sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace '${source}' '${dest}' wp_site wp_blogs --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
        else
          search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "${db_host}" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -P ${db_port} -s "${source}" -r "${dest}" -t "wp_site,wp_blogs" --exclude-cols=guid --allow-old-php 2>&1)
          if [[ $? != 0 ]]; then
            echo "${search_replace_db_output}"
            sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace '${source}' '${dest}' wp_site wp_blogs --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
          else
            gridpane::grep_output::search_replace_db "${search_replace_db_output}"
          fi
        fi
        echo "Multisite Primary Domain Swap - WP_*_Options Tables | Must use wp-cli"
        echo "Pattern to search replace:  .${source} -> .${dest}"
        sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace '.${source}' '.${dest}' 'wp_*options' --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1"
      fi
      echo "Reset Permissions and Clear Cache"
      /usr/local/bin/gp fix cached "${dest}"
      /usr/local/bin/gp fix perms "${dest}"
    } | tee -a /opt/gridpane/gpclone.log

  else
    [[ -d /var/www/"${dest}"/htdocs/database-${clone_time_epoch} ]] &&
      rm -rf /var/www/"${dest}"/htdocs/database-"${clone_time_epoch}"
    [[ -f /var/www/"${dest}"/htdocs/database.sql ]] &&
      rm /var/www/"${dest}"/htdocs/database.sql
    echo "Failed to import DB..." | tee -a /opt/gridpane/gpclone.log
    touch /tmp/"${nonce}".failed.db.export
  fi
}

gpclone::export::db_only() {
  if [[ -z $1 ]]; then
    gpclone::end::failsync_pause::exit "We need to know the source site to export the db from... exiting..."
  fi

  local site_to_export_db="$1"

  echo "Processing DB only export at /var/www/${site_to_export_db}/htdocs ..." | tee -a /opt/gridpane/gpclone.log
  cd /var/www/"${site_to_export_db}"/htdocs || return

  local checkwp=$(cat /var/www/"${site_to_export_db}"/htdocs/index.php)
  if [[ ${checkwp} != *"Front to the WordPress application. This file doesn't do anything"* ]]; then
    echo "This is not a valid WordPress install, skipping!!!" | tee -a /opt/gridpane/gpclone.log
  else
    if ! type mydumper >/dev/null; then
        apt-get -y install mydumper
    fi

    workers::check::site_routing "${site_to_export_db}"

    echo "Exporting ${site_to_export_db} database..." | tee -a /opt/gridpane/gpclone.log
    local WPDBNAME
    local WPDBUSER
    local WPDBPASS
    local WPDBHOST
    local WPDBPORT
    # shellcheck disable=SC2002
    WPDBNAME=$(cat /var/www/"${site_to_export_db}"/wp-config.php | grep DB_NAME | tr \" \' | cut -d \' -f 4)
    # shellcheck disable=SC2002
    WPDBUSER=$(cat /var/www/"${site_to_export_db}"/wp-config.php | grep DB_USER | tr \" \' | cut -d \' -f 4)
    # shellcheck disable=SC2002
    WPDBPASS=$(cat /var/www/"${site_to_export_db}"/wp-config.php | grep DB_PASSWORD | tr \" \' | cut -d \' -f 4)
    WPDBHOST=$(cat "/var/www/${site_to_export_db}/wp-config.php" | grep DB_HOST | tr \" \' | cut -d \' -f 4)
    WPDBPORT=$(echo ${WPDBHOST} | awk -F':' '{print $2}')
    WPDBPORT=${WPDBPORT:-3306}

    local mysql_dump_output
    local mysql_dump_output_status
    if ! type mydumper >/dev/null; then
      mysql_dump_output=$(mysqldump --no-tablespaces -u"${WPDBUSER}" -p"${WPDBPASS}" -h"${WPDBHOST}" -P"${WPDBPORT}" "${WPDBNAME}" >/var/www/"${site_to_export_db}"/htdocs/database.sql)
      mysql_dump_output_status=$?
    else
      mysql_dump_output=$(
      # shellcheck disable=SC2154
      mydumper -v 3 -B "${WPDBNAME}" -h "${WPDBHOST}" -P "${WPDBPORT}" -u "${WPDBUSER}" -p "${WPDBPASS}" --lock-all-tables \
        -o  "/var/www/${site_to_export_db}/htdocs/database-${clone_time_epoch}" \
        2>&1
      )
      mysql_dump_output_status=$?
    fi

    if [[ "${mysql_dump_output_status}" == 0 ]]; then
      echo "MySQL Dump Success..."
      [[ -f /var/www/"${site_to_export_db}"/htdocs/database.sql ]] && chmod 400 /var/www/"${site_to_export_db}"/htdocs/database.sql
      echo "DB Export of ${site_to_export_db} Done!" | tee -a /opt/gridpane/gpclone.log
    else
      [[ -f /var/www/"${site_to_export_db}"/htdocs/database.sql ]] && rm /var/www/"${site_to_clone}"/htdocs/database.sql
      [[ -d /var/www/"${site_to_export_db}"/htdocs/database-"${clone_time_epoch}" ]] && rm -rf /var/www/"${site_to_export_db}"/htdocs/database-"${clone_time_epoch}"
      echo "DB Export of ${site_to_export_db} Failed!" | tee -a /opt/gridpane/gpclone.log
    fi
  fi
}

gpclone::sync::to_remote() {
  if [[ -z $1 ]] || [[ -z $2 ]]; then
    gpclone::end::failsync_pause::exit "We need to know the site to sync and the remote IP to sync to... exiting..."
  fi

  check_ip=$(gridpane::ip_check "$2" "clone")
  echo "${check_ip}" | tee -a /opt/gridpane/gpclone.log

  site_to_sync="$1"
  #  remote_IP="$2"

  echo "Copying User Configs..." | tee -a /opt/gridpane/gpclone.log
  scp /var/www/"${site_to_sync}"/user-configs.php root@"${remote_IP}":/var/www/"${site_to_sync}"/user-configs.php | tee -a /opt/gridpane/gpclone.log

  echo "Copying DB across" | tee -a /opt/gridpane/gpclone.log
  [[ -f /var/www/"${site_to_export_db}"/htdocs/database.sql ]] &&
    scp /var/www/"${site_to_sync}"/htdocs/database.sql root@"${remote_IP}":/var/www/"${site_to_sync}"/htdocs/database.sql | tee -a /opt/gridpane/gpclone.log
  [[ -d /var/www/"${site_to_export_db}"/htdocs/database-${clone_time_epoch} ]] &&
    scp -r /var/www/"${site_to_export_db}"/htdocs/database-"${clone_time_epoch}" root@"${remote_IP}":/var/www/"${site_to_sync}"/htdocs/database-"${clone_time_epoch}"

  [[ -f /var/www/"${site_to_export_db}"/htdocs/database.sql ]] &&
    rm /var/www/"${site_to_sync}"/htdocs/database.sql
  [[ -d /var/www/"${site_to_export_db}"/htdocs/database-${clone_time_epoch} ]] &&
    rm -rf /var/www/"${site_to_export_db}"/htdocs/database-"${clone_time_epoch}"

  echo "Syncing site files for ${site_to_sync} to remote server ${remote_IP} via unison..." | tee -a /opt/gridpane/gpclone.log
  DoUni=$(unison /var/www/"${site_to_sync}"/htdocs ssh://"${remote_IP}"//var/www/"${site_to_sync}"/htdocs -prefer newer -batch)

  echo "${DoUni}" | tee -a /opt/gridpane/gpclone.log
  if [[ ${DoUni} == *"unison: command not found"* ]]; then
    echo "Unison not installed on remote, resolving..." | tee -a /opt/gridpane/gpclone.log
    ssh root@"${remote_IP}" "sleep 1 && apt-get install -y unison" | tee -a /opt/gridpane/gpclone.log
    echo "Reattempting copy..." | tee -a /opt/gridpane/gpclone.log
    DoUni=$(unison /var/www/"${site_to_sync}/"htdocs ssh://"${remote_IP}"//var/www/"${site_to_sync}"/htdocs -prefer newer -batch)
    echo "${DoUni}" | tee -a /opt/gridpane/gpclone.log
  fi

  local cloning_site_cache
  local cloning_site_config
  local remote_cloning_site_config
  local cloning_site_object_caching
  local remote_cloning_site_object_caching
  local remote_certbot_ssl_present
  local remote_acme_ssl_present

  if [[ "$webserver" = nginx  ]]; then
    cloning_site_cache="$(gridpane::check::site::nginx::cache "${site_to_sync}")"
    cloning_site_config="$(gridpane::check::site::nginx::config "${site_to_sync}")"
    cloning_site_object_caching=$(sudo su - "${local_sys_user}" -c "timeout 30 /usr/local/bin/wp redis status --path=/var/www/${site_to_sync}/htdocs 2>&1")
  else
    cloning_site_cache="$(/usr/local/bin/gpols get "$site_to_sync" cache)"
    cloning_site_config="$(/usr/local/bin/gpols get "${site_to_sync}" vhconf)"
    cloning_site_object_caching="$(gridpane::conf_read ols-redis-cache -site.env "${site_to_sync}")"
  fi

  certbot_cert_path="/etc/letsencrypt/live/${site_to_sync}/fullchain.pem"

  if [[ "$webserver_remote" = nginx  ]]; then
    acme_cert_path="/etc/nginx/ssl/${site_to_sync}/cert.pem"
    # shellcheck disable=SC2029
    remote_cloning_site_cache="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx cache-check ${site_to_sync} -q")"
    [[ $remote_cloning_site_cache != "fastcgi" && $remote_cloning_site_cache != "redis"  ]] &&
      remote_cloning_site_cache="off"
    # shellcheck disable=SC2029
    remote_cloning_site_config="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx type-check ${site_to_sync} -q")"
    # shellcheck disable=SC2029
    remote_cloning_site_object_caching=$(ssh root@"$remote_IP" "/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp redis status --path=/var/www/${site_to_sync}/htdocs 2>&1\"")
  else
    acme_cert_path="/usr/local/lsws/conf/cert/${site_to_sync}/cert.pem"
    # shellcheck disable=SC2029
    remote_cloning_site_cache="$(ssh root@"$remote_IP" "/usr/local/bin/gpols get ${site_to_sync} cache")"
    # shellcheck disable=SC2029
    remote_cloning_site_config="$(ssh root@"$remote_IP" "/usr/local/bin/gpols get ${site_to_sync} vhconf")"
    # shellcheck disable=SC2029
    remote_cloning_site_object_caching="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read ols-redis-cache -site.env ${site_to_sync} -q")"
  fi
  # shellcheck disable=SC2029
  remote_acme_ssl_present=$(ssh root@"$remote_IP" "[[ -f ${acme_cert_path} ]] && echo true")
  # shellcheck disable=SC2029
  remote_certbot_ssl_present=$(ssh root@"$remote_IP" "[[ -f ${certbot_cert_path} ]] && echo true")

  if [[ ${cloning_site_config} == *"https" || ${cloning_site_config} == *"vhssl" ]] &&
     [[ ${remote_cloning_site_config} != *"https" && ${remote_cloning_site_config} != *"vhssl" ]]; then

    if [[ ${remote_acme_ssl_present} == "true" ]]; then
      local is_remote_cert_wildcard
       # shellcheck disable=SC2029
      is_remote_cert_wildcard=$(ssh root@"$remote_IP" "openssl x509 -text -noout -in ${acme_cert_path}")
      local https_conf_type
      local is_wildcard
      if [[ "${is_remote_cert_wildcard}" == *"*.${site_to_sync}"* ]]; then
        # shellcheck disable=SC2029
        https_conf_type="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx type-check ${site_to_sync} return-wildcard return-https -q")"
        is_wildcard="true"
      else
        # shellcheck disable=SC2029
        https_conf_type="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx type-check ${site_to_sync} return-https -q")"
        is_wildcard="false"
      fi

      local generate_conf
      if [[ "$webserver_remote" = nginx ]]; then
         # shellcheck disable=SC2029
        generate_conf="$(
          ssh root@"$remote_IP" \
          "/usr/local/bin/gp conf nginx generate ${https_conf_type} ${remote_cloning_site_cache} ${site_to_sync}; \
          /usr/sbin/nginx -t && /usr/local/bin/gp nginx reload"
        )"
      else
        if [[ "$is_wildcard" = true ]]; then
          # shellcheck disable=SC2029
          generate_conf="$(
            ssh root@"$remote_IP" \
            "/usr/local/bin/gpols site ${site_to_sync} --wildcard --ssl --output"
          )"
        else
          # shellcheck disable=SC2029
          generate_conf="$(
            ssh root@"$remote_IP" \
            "/usr/local/bin/gpols site ${site_to_sync} --ssl --output"
          )"
        fi
      fi
    elif [[ ${remote_certbot_ssl_present} == "true" ]]; then

      local generate_conf
      if [[ "$webserver_remote" = nginx ]]; then
         # shellcheck disable=SC2029
        generate_conf="$(
          ssh root@"$remote_IP" \
          "/usr/local/bin/gp conf nginx generate ${https_conf_type} ${remote_cloning_site_cache} ${site_to_sync}; \
          /usr/sbin/nginx -t && /usr/local/bin/gp nginx reload"
        )"
      else
        # shellcheck disable=SC2029
        generate_conf="$(
          ssh root@"$remote_IP" \
          "/usr/local/bin/gpols site ${site_to_sync} --ssl --output"
        )"
      fi
    fi

    {
      echo "${is_remote_cert_wildcard}"
      echo "${generate_conf}"
      # shellcheck disable=SC2029
      ssh root@"$remote_IP" "/usr/local/bin/gp conf wp -update-host ${site_to_sync}"
    } | tee -a /opt/gridpane/gpclone.log

    local check_remote_https_domain
    if [[ "$webserver_remote" = nginx ]]; then
       # shellcheck disable=SC2029
      check_remote_https_domain=$(ssh -n root@"${remote_IP}" "sleep 1 && cat /etc/nginx/sites-enabled/${site_to_sync}")
    else
       # shellcheck disable=SC2029
      check_remote_https_domain=$(ssh -n root@"${remote_IP}" "sleep 1 && cat /usr/local/lsws/conf/vhosts/${site_to_sync}/vhconf.conf")
    fi

    if [[ "$webserver_remote" = nginx && ${check_remote_https_domain} == *"HTTPS"* ||
          "$webserver_remote" = openlitespeed && ${check_remote_https_domain} == *"ssl.conf"* ]]; then

      if [[ "$webserver_remote" = nginx ]]; then
        # shellcheck disable=SC2029
        remote_cloning_site_config_again="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx type-check ${site_to_sync} -q")"
      else
        # shellcheck disable=SC2029
        remote_cloning_site_config_again="$(ssh root@"$remote_IP" "/usr/local/bin/gpols get ${site_to_sync} vhconf")"
      fi

      if [[ $remote_cloning_site_config_again != *"wildcard"* ]]; then
        gridpane::callback::app \
          "callback" \
          "/domain/domain-update" \
          "site_url=${site_to_sync}" \
          "server_ip=${remote_IP}" \
          "domain_url=${site_to_sync}" \
          "ssl@true" \
          "chain_ssl@false" \
          "status=success" \
          "details=success" \
          "silent@false"
      else
        gridpane::callback::app \
          "callback" \
          "/domain/domain-update" \
          "site_url=${site_to_sync}" \
          "server_ip=${remote_IP}" \
          "domain_url=${site_to_sync}" \
          "is_wildcard@true" \
          "ssl@true" \
          "chain_ssl@false" \
          "status=success" \
          "details=success" \
          "silent@false"
      fi

      # shellcheck disable=SC2029
      ssh root@"$remote_IP" "/usr/local/bin/gp conf write ssl true -site.env ${site_to_sync}"
    fi
  fi

  {
    echo "Checking for ${site_to_sync} Add on Alias domains sync"
    echo "--------------------------------------------------------------------"
  } | tee -a /opt/gridpane/gpclone.log
  gpclone::process::sync_domains alias "${site_to_sync}" "$remote_IP"

  {
    echo "Checking for ${site_to_sync} Add on Redirect domains sync"
    echo "--------------------------------------------------------------------"
  } | tee -a /opt/gridpane/gpclone.log
  gpclone::process::sync_domains redirect "${site_to_sync}" "$remote_IP"

  echo "Syncing site $webserver_formatted files for ${site_to_sync} to remote server ${remote_IP} via unison..." | tee -a /opt/gridpane/gpclone.log

  webserver_remote="$(/usr/bin/ssh root@"$remote_IP" "/usr/local/bin/gp conf read webserver -q")"

  if [[ "$webserver" = nginx &&
        "$webserver_remote" = nginx ]]; then
    unison /var/www/"${site_to_sync}"/nginx ssh://"${remote_IP}"//var/www/"${site_to_sync}"/nginx -prefer newer -batch | tee -a /opt/gridpane/gpclone.log
    echo "Syncing site modsec files for ${site_to_sync} to remote server ${remote_IP} via unison..." | tee -a /opt/gridpane/gpclone.log
    unison /var/www/"${site_to_sync}"/modsec ssh://"${remote_IP}"//var/www/"${site_to_sync}"/modsec -prefer newer -batch | tee -a /opt/gridpane/gpclone.log
  elif [[ "$webserver" = openlitespeed &&
        "$webserver_remote" = openlitespeed ]]; then
    unison /var/www/"${site_to_sync}"/ols ssh://"${remote_IP}"//var/www/"${site_to_sync}"/ols -prefer newer -batch | tee -a /opt/gridpane/gpclone.log
  fi

  echo "Unison sync done... " | tee -a /opt/gridpane/gpclone.log

  local source_php_ver
  source_php_ver=$(gridpane::get::site::php "${site_to_sync}")
  local destination_php_ver
  if [[ "$webserver_remote" = nginx ]]; then
    # shellcheck disable=SC2029
    destination_php_ver=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf php -ver-check ${site_to_sync} -q")
  elif [[ "$webserver_remote" = openlitespeed ]]; then
    # shellcheck disable=SC2029
    destination_php_ver=$(ssh root@"$remote_IP" "/usr/local/bin/gpols get ${site_to_sync} php")
  fi

  if [[ ${source_php_ver} != "${destination_php_ver}" ]]; then
    {
      echo "Adjusting PHP for ${site_to_sync} on $remote_IP to ${source_php_ver}"
      # shellcheck disable=SC2029
      ssh root@"$remote_IP" "/usr/local/bin/gp site ${site_to_sync} -switch-php ${source_php_ver}"
    } | tee -a /opt/gridpane/gpclone.log
  fi

  local remote_sys_user
  local local_sys_user
  # shellcheck disable=SC2029
  remote_sys_user="$(ssh root@"$remote_IP" "/bin/grep sys-user: /var/www/${site_to_sync}/logs/${site_to_sync}.env" | /usr/bin/awk -F '[:]' '{print $2}')"
  local_sys_user="$(gridpane::get::set::site::user "${site_to_sync}")"

  echo "remote $webserver_remote" | tee -a /opt/gridpane/gpclone.log
  if [[ "$webserver" = nginx &&
        "$webserver_remote" = nginx ]]; then

    # shellcheck disable=SC2029
    destination_php_ver=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf php -ver-check ${site_to_sync} -q")
    local phpFileVer
    phpFileVer=${destination_php_ver/./}
    local sourcePhpFileVer
    sourcePhpFileVer=${source_php_ver/./}

    local proceed_with_fpm
    case $destination_php_ver in
      7.1|7.2|7.3|7.4) echo "Destination PHP Version valid: $destination_php_ver" | tee -a /opt/gridpane/gpclone.log ;;
      *)
        echo "Destination PHP Version not valid: $destination_php_ver" | tee -a /opt/gridpane/gpclone.log
        proceed_with_fpm="false"
        ;;
    esac

    case $phpFileVer in
      71|72|73|74) echo "Destination PHP Sock Version valid: $phpFileVer" | tee -a /opt/gridpane/gpclone.log ;;
      *)
        echo "Destination PHP Sock Version not valid: $phpFileVer" | tee -a /opt/gridpane/gpclone.log
        proceed_with_fpm="false"
        ;;
    esac

    case $sourcePhpFileVer in
      71|72|73|74) echo "Source PHP Sock Version valid: $sourcePhpFileVer" | tee -a /opt/gridpane/gpclone.log ;;
      *)
        echo "Source PHP Sock Version not valid: $sourcePhpFileVer" | tee -a /opt/gridpane/gpclone.log
        proceed_with_fpm="false"
        ;;
    esac

    if [[ $(cat /etc/passwd) != *"/home/${local_sys_user}:"* ]]; then
      echo "Local System User not Valid: ${local_sys_user}" | tee -a /opt/gridpane/gpclone.log
      proceed_with_fpm="false"
    else
      echo "Local System User Valid: ${local_sys_user}" | tee -a /opt/gridpane/gpclone.log
    fi

    local check_for_remote_user
    # shellcheck disable=SC2029
    check_for_remote_user=$(ssh root@"$remote_IP" "[[ \$(cat /etc/passwd) == *\"/home/${remote_sys_user}:\"* ]] && echo \"true\"")
    if [[ ${check_for_remote_user} != *"true"* ]]; then
      echo "Remote System User not Valid: ${remote_sys_user}" | tee -a /opt/gridpane/gpclone.log
      proceed_with_fpm="false"
    else
      echo "Remote System User Valid: ${remote_sys_user}" | tee -a /opt/gridpane/gpclone.log
    fi

    if [[ ${proceed_with_fpm} != *"false"* ]]; then
      {
        scp /etc/php/"${source_php_ver}"/fpm/pool.d/"${site_to_sync}".conf root@"${remote_IP}":/etc/php/"${destination_php_ver}"/fpm/pool.d/"${site_to_sync}".conf
         # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "sed -i \"s/'${site_to_sync}${sourcePhpFileVer}'/'${site_to_sync}${phpFileVer}'/g\" /etc/php/${destination_php_ver}/fpm/pool.d/${site_to_sync}.conf; \
          sed -i \"s/\[${site_to_sync}${sourcePhpFileVer}\]/\[${site_to_sync}${phpFileVer}\]/g\" /etc/php/${destination_php_ver}/fpm/pool.d/${site_to_sync}.conf; \
          sed -i \"/user = ${local_sys_user}/c\user = ${remote_sys_user}\" /etc/php/${destination_php_ver}/fpm/pool.d/${site_to_sync}.conf; \
          sed -i \"/group = ${local_sys_user}/c\group = ${remote_sys_user}\" /etc/php/${destination_php_ver}/fpm/pool.d/${site_to_sync}.conf; \
          sed -i \"/listen = \/var/c\listen = \/var\/run\/php\/php${phpFileVer}-fpm-${site_to_sync}.sock\" /etc/php/${destination_php_ver}/fpm/pool.d/${site_to_sync}.conf"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    cloning_site_cache="$(gridpane::check::site::nginx::cache "${site_to_sync}")"
    [[ $cloning_site_cache != "fastcgi" && $cloning_site_cache != "redis"  ]] &&
      cloning_site_cache="off"
    # shellcheck disable=SC2029
    remote_cloning_site_cache="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx cache-check ${site_to_sync} -q")"
    [[ $remote_cloning_site_cache != "fastcgi" && $remote_cloning_site_cache != "redis"  ]] &&
      remote_cloning_site_cache="off"
    cloning_site_config="$(gridpane::check::site::nginx::config "${site_to_sync}")"
    # shellcheck disable=SC2029
    remote_cloning_site_config="$(ssh root@"$remote_IP" "/usr/local/bin/gp conf nginx type-check ${site_to_sync} -q")"
    cloning_site_object_caching=$(sudo su - "${local_sys_user}" -c "timeout 30 /usr/local/bin/wp redis status --path=/var/www/${site_to_sync}/htdocs 2>&1")
    # shellcheck disable=SC2029
    remote_cloning_site_object_caching=$(ssh root@"$remote_IP" "/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp redis status --path=/var/www/${site_to_sync}/htdocs 2>&1\"")

    {
      local command_sequence
      if [[ "$cloning_site_config" == *"wildcard"* && "$remote_cloning_site_config" != *"wildcard"* ]]; then
        command_sequence="/usr/local/bin/gp site ${site_to_sync} -create-wildcard;"
      elif [[ "$cloning_site_config" != *"wildcard"* && "$remote_cloning_site_config" == *"wildcard"* ]]; then
        command_sequence="/usr/local/bin/gp site ${site_to_sync} -remove-wildcard;"
      fi

      if [[ "$cloning_site_config" == *"www"* && "$remote_cloning_site_config" != *"www"* ]]; then
         command_sequence="${command_sequence} /usr/local/bin/gp site ${site_to_sync} -route-domain-www;"
      elif [[ "$cloning_site_config" == *"root"* && "$remote_cloning_site_config" != *"root"* ]]; then
        command_sequence="${command_sequence} /usr/local/bin/gp site ${site_to_sync} -route-domain-root;"
      fi

      if [[ "$cloning_site_cache" != "$remote_cloning_site_cache" ]]; then
        command_sequence="${command_sequence} /usr/local/bin/gp site ${site_to_sync} -caching=$cloning_site_cache;"
      fi

      # shellcheck disable=SC2029
      [[ -n ${command_sequence} ]] &&
        ssh root@"$remote_IP" "${command_sequence}"

      # shellcheck disable=SC2029
      if [[ "$cloning_site_object_caching" == *"Status: Connected"* && "$cloning_site_object_caching" != "$remote_cloning_site_object_caching" ]]; then
        ssh root@"$remote_IP" "/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp redis enable --path=/var/www/${site_to_sync}/htdocs\""
      else
        ssh root@"$remote_IP" "/usr/bin/sudo su - $remote_sys_user -c \"timeout 300 /usr/local/bin/wp redis disable --path=/var/www/${site_to_sync}/htdocs\""
      fi

      local source_waf_type
      local destination_waf_type
      source_waf_type=$(gridpane::conf_read waf -site.env "${site_to_sync}")
      # shellcheck disable=SC2029
      destination_waf_type=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read waf -site.env ${site_to_sync} -q")
      command_sequence=""
      if [[ ${source_waf_type} == *"modsec"* &&
            ${destination_waf_type} != *"modsec"* ]]; then
        waf::sync::site_to_app "${site_to_sync}" "modsec"
        if [[ ${destination_waf_type} == *"6"* ]]; then
          command_sequence="/usr/local/bin/gp site ${site_to_sync} -6g-off;"
        elif [[ ${destination_waf_type} == *"7"* ]]; then
          command_sequence="/usr/local/bin/gp site ${site_to_sync} -7g-off;"
        fi
        command_sequence="${command_sequence} /usr/local/bin/gp site ${site_to_sync} -modsec-on;"
      elif [[ ${source_waf_type} == "6G" || ${source_waf_type} == "6g" ]] &&
          [[ ${destination_waf_type} != "6G" && ${destination_waf_type} != "6g" ]]; then
        waf::sync::site_to_app "${site_to_sync}" "6g"
        if [[ ${destination_waf_type} == *"modsec"* ]]; then
          command_sequence="/usr/local/bin/gp site ${site_to_sync} -modsec-off;"
        elif [[ ${destination_waf_type} == *"7"* ]]; then
          command_sequence="/usr/local/bin/gp site ${site_to_sync} -7g-off;"
        fi
        command_sequence="${command_sequence} /usr/local/bin/gp site ${site_to_sync} -6g-on;"
      elif [[ ${source_waf_type} == "7G" || ${source_waf_type} == "7g" ]] &&
          [[ ${destination_waf_type} != "7G" && ${destination_waf_type} != "7g" ]]; then
        waf::sync::site_to_app "${site_to_sync}" "7g"
        if [[ ${destination_waf_type} == *"modsec"* ]]; then
          command_sequence="/usr/local/bin/gp site ${site_to_sync} -modsec-off;"
        elif [[ ${destination_waf_type} == *"6"* ]]; then
          command_sequence="/usr/local/bin/gp site ${site_to_sync} -6g-off;"
        fi
        command_sequence="${command_sequence} /usr/local/bin/gp site ${site_to_sync} -7g-on;"
      fi

       # shellcheck disable=SC2029
      [[ -n ${command_sequence} ]] &&
        ssh root@"$remote_IP" "${command_sequence}"

      source_waf_type=$(gridpane::conf_read waf -site.env "${site_to_clone}")
      # shellcheck disable=SC2029
      destination_waf_type=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read waf -site.env ${site_to_sync} -q")
      if [[ ${source_waf_type} == *"modsec"* &&
            ${destination_waf_type} == *"modsec"* ]]; then
        echo "modsec" >/var/www/"${site_to_sync}"/logs/waf.sync
        scp /var/www/"${site_to_sync}"/modsec/"${site_to_sync}"-crs-anomaly-blocking-threshold.conf \
          root@"${remote_IP}":/var/www/"${site_to_sync}"/modsec/"${site_to_sync}"-crs-anomaly-blocking-threshold.conf
        scp /var/www/"${site_to_sync}"/modsec/"${site_to_sync}"-crs-paranoia-level.conf \
          root@"${remote_IP}":/var/www/"${site_to_sync}"/modsec/"${site_to_sync}"-crs-paranoia-level.conf
        scp /var/www/"${site_to_sync}"/modsec/"${site_to_sync}"-runtime-exceptions.conf \
          root@"${remote_IP}":/var/www/"${site_to_sync}"/modsec/"${site_to_sync}"-runtime-exceptions.conf
        scp /var/www/"${site_to_sync}"/modsec/"${site_to_sync}"-runtime-whitelist.conf \
          root@"${remote_IP}":/var/www/"${site_to_sync}"/modsec/"${site_to_sync}"-runtime-whitelist.conf
        scp /var/www/"${site_to_sync}"/logs/waf.sync root@"${remote_IP}":/var/www/"${site_to_sync}"/logs/waf.sync
      elif [[ ${source_waf_type} == "6G" || ${source_waf_type} == "6g" ]] &&
          [[ ${destination_waf_type} == "6G" || ${destination_waf_type} == "6g" ]]; then
        echo "6g" >/var/www/"${site_to_sync}"/logs/waf.sync
        scp /etc/nginx/common/"${site_to_sync}"-6g.conf root@"${remote_IP}":/etc/nginx/common/"${site_to_sync}"-6g.conf
        scp /var/www/"${site_to_sync}"/logs/waf.sync root@"${remote_IP}":/var/www/"${site_to_sync}"/logs/waf.sync
      elif [[ ${source_waf_type} == "7G" || ${source_waf_type} == "7g" ]] &&
          [[ ${destination_waf_type} == "7G" || ${destination_waf_type} == "7g" ]]; then
        echo "7g" >/var/www/"${site_to_sync}"/logs/waf.sync
        scp /etc/nginx/common/"${site_to_sync}"-7g.conf root@"${remote_IP}":/etc/nginx/common/"${site_to_sync}"-7g.conf
        scp /var/www/"${site_to_sync}"/logs/waf.sync root@"${remote_IP}":/var/www/"${site_to_sync}"/logs/waf.sync
      fi
      [[ -f /var/www/"${site_to_sync}"/logs/waf.sync ]] && rm /var/www/"${site_to_sync}"/logs/waf.sync

    } | tee -a /opt/gridpane/gpclone.log
  fi

  waf::sync::site_to_app "${site_to_sync}" "wpf2b"
  local site_wpfail2ban
  site_wpfail2ban=$(gridpane::conf_read wp-fail2ban -site.env "${site_to_sync}")
  local remote_site_wpfail2ban
  # shellcheck disable=SC2029
  remote_site_wpfail2ban=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read wp-fail2ban -site.env ${site_to_sync} -q")
  if [[ ${site_wpfail2ban} == *"rue"* || ${site_wpfail2ban} == *"on"* ]] &&
    [[ ${remote_site_wpfail2ban} != *"rue"* && ${remote_site_wpfail2ban} != *"on"* ]]; then
    {
      echo "----------------------------------------------------------------------"
      echo "Enabling ${site_to_sync} wpfail2ban configurations on $remote_IP"
      # shellcheck disable=SC2029
      ssh root@"$remote_IP" "/usr/local/bin/gp site ${site_to_sync} -enable-wp-fail2ban"
      sleep 3
    } | tee -a /opt/gridpane/gpclone.log

    local source_fail2ban_user_enumeration
    source_fail2ban_user_enumeration=$(grep "WP_FAIL2BAN_BLOCK_USER_ENUMERATION" /var/www/"${site_to_sync}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_user_enumeration ]]; then
      {
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "/bin/sed -i \"/WP_FAIL2BAN_BLOCK_USER_ENUMERATION/d\" /var/www/${site_to_sync}/wp-fail2ban-configs.php; \
          /bin/echo \"$source_fail2ban_user_enumeration\" | /usr/bin/tee -a /var/www/${site_to_sync}/wp-fail2ban-configs.php"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_blocked_users
    source_fail2ban_blocked_users=$(grep "WP_FAIL2BAN_BLOCKED_USERS" /var/www/"${site_to_sync}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_blocked_users ]]; then
      {
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "/bin/sed -i \"/WP_FAIL2BAN_BLOCKED_USERS/d\" /var/www/${site_to_sync}/wp-fail2ban-configs.php; \
          /bin/echo \"$source_fail2ban_blocked_users\" | /usr/bin/tee -a /var/www/${site_to_sync}/wp-fail2ban-configs.php"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_comments
    source_fail2ban_log_comments=$(grep "WP_FAIL2BAN_LOG_COMMENTS'" /var/www/"${site_to_sync}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_comments ]]; then
      {
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "/bin/sed -i \"/WP_FAIL2BAN_LOG_COMMENTS'/d\" /var/www/${site_to_sync}/wp-fail2ban-configs.php; \
          /bin/echo \"$source_fail2ban_log_comments\" | /usr/bin/tee -a /var/www/${site_to_sync}/wp-fail2ban-configs.php"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_comments_extra
    source_fail2ban_log_comments_extra=$(grep "WP_FAIL2BAN_LOG_COMMENTS_EXTRA" /var/www/"${site_to_sync}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_comments_extra ]]; then
      {
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "/bin/sed -i \"/WP_FAIL2BAN_LOG_COMMENTS_EXTRA/d\" /var/www/${site_to_sync}/wp-fail2ban-configs.php; \
          /bin/echo \"$source_fail2ban_log_comments_extra\" | /usr/bin/tee -a /var/www/${site_to_sync}/wp-fail2ban-configs.php"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_password_request
    source_fail2ban_log_password_request=$(grep "WP_FAIL2BAN_LOG_PASSWORD_REQUEST" /var/www/"${site_to_sync}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_password_request ]]; then
      {
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "/bin/sed -i \"/WP_FAIL2BAN_LOG_PASSWORD_REQUEST/d\" /var/www/${site_to_sync}/wp-fail2ban-configs.php; \
          /bin/echo \"$source_fail2ban_log_password_request\" | /usr/bin/tee -a /var/www/${site_to_sync}/wp-fail2ban-configs.php"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_pingbacks
    source_fail2ban_log_pingbacks=$(grep "WP_FAIL2BAN_LOG_PINGBACKS" /var/www/"${site_to_sync}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_pingbacks ]]; then
      {
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "/bin/sed -i \"/WP_FAIL2BAN_LOG_PINGBACKS/d\" /var/www/${site_to_sync}/wp-fail2ban-configs.php; \
          /bin/echo \"$source_fail2ban_log_pingbacks\" | /usr/bin/tee -a /var/www/${site_to_sync}/wp-fail2ban-configs.php"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_spam
    source_fail2ban_log_spam=$(grep "WP_FAIL2BAN_LOG_SPAM" /var/www/"${site_to_sync}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_spam ]]; then
      {
        # shellcheck disable=SC2029
        ssh root@"$remote_IP" \
          "/bin/sed -i \"/WP_FAIL2BAN_LOG_SPAM/d\" /var/www/${site_to_sync}/wp-fail2ban-configs.php; \
          /bin/echo \"$source_fail2ban_log_spam\" | /usr/bin/tee -a /var/www/${site_to_sync}/wp-fail2ban-configs.php"
      } | tee -a /opt/gridpane/gpclone.log
    fi

  elif [[ ${site_wpfail2ban} == *"false"* || ${site_wpfail2ban} == *"off"* ]] &&
    [[ ${remote_site_wpfail2ban} != *"false"* && ${remote_site_wpfail2ban} != *"off"* ]]; then
    {
      echo "----------------------------------------------------------------------"
      echo "Disabling ${site_to_sync} wpfail2ban configurations on $remote_IP"
      # shellcheck disable=SC2029
      ssh root@"$remote_IP" "/usr/local/bin/gp site ${site_to_sync} -disable-wp-fail2ban"
      sleep 3
    } | tee -a /opt/gridpane/gpclone.log
  fi

  local http_auth
  http_auth=$(gridpane::conf_read http-auth -site.env "${site_to_sync}")
  local remote_http_auth
  # shellcheck disable=SC2029
  remote_http_auth=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read http-auth -site.env ${site_to_sync} -q")
  echo "local http auth: ${http_auth}"
  echo "remote http auth: ${remote_http_auth}"
  if [[ ${http_auth} == "true" || ${http_auth} == "on" ]] &&
    [[ ${remote_http_auth} != "true" && ${remote_http_auth} != "on" ]]; then
    {
      echo "----------------------------------------------------------------------"
      echo "HTTP Auth for ${site_to_sync} on $remote_IP"
      # shellcheck disable=SC2029
      ssh root@"$remote_IP" "/usr/local/bin/gp site ${site_to_sync} -http-auth"
    } | tee -a /opt/gridpane/gpclone.log
  fi

  local smtp
  smtp=$(gridpane::conf_read smtp -site.env "${site_to_sync}")
  local remote_smtp
  # shellcheck disable=SC2029
  remote_smtp=$(ssh root@"$remote_IP" "/usr/local/bin/gp conf read smtp -site.env ${site_to_sync} -q")
  echo "local smtp: ${smtp}"
  echo "remote smtp: ${remote_smtp}"
  if [[ ${smtp} == "true" || ${smtp} == "on" ]] &&
    [[ ${remote_smtp} != "true" && ${remote_smtp} != "on" ]]; then
    if [[ -f /var/www/"${site_to_sync}"/sendgrid-wp-configs.php ]]; then
      {
        echo "----------------------------------------------------------------------"
        echo "Enabling SMTP for ${site_to_sync} on $remote_IP"
        local api_key
        api_key=$(grep "SMTP_PASS" /var/www/"${site_to_sync}"/sendgrid-wp-configs.php)
        api_key=${api_key//\"/}
        api_key=${api_key// /}
        api_key=${api_key#define(SMTP_PASS,}
        api_key=${api_key%');'}

#        ssh root@"$remote_IP" "/usr/local/bin/gp site ${site_to_sync} -mailer enable SendGrid \"${api_key}\" simple"
      } | tee -a /opt/gridpane/gpclone.log
    fi
  fi

  local check_for_local_multisite
  check_for_local_multisite=$(gridpane::check::multisite "${site_to_sync}")
  if [[ ${check_for_multisite} == "true"* ]]; then
    local check_for_remote_multisite
    # shellcheck disable=SC2029
    check_for_remote_multisite=$(ssh root@"$remote_IP" "cat /var/www/${site_to_sync}/wp-config.php")
    if [[ ${check_for_remote_multisite} != *"WP_ALLOW_MULTISITE"* ]]; then
      if [[ ${check_for_multisite} == *"subdomain" ]]; then
        multisite_type="-multisubdom"
      elif [[ ${check_for_multisite} == *"subdirectory" ]]; then
        multisite_type="-multisubdir"
      fi
      {
        echo "Converting ${remote_IP} ${site_to_sync} to multisite ${check_for_multisite}... this is a one way streer..."
        # shellcheck disable=SC2029
        ssh root@"${remote_IP}" "sleep 3 && /usr/local/bin/gp site ${site_to_sync} ${multisite_type}"
      } | tee -a /opt/gridpane/gpclone.log
    fi
  fi

  # shellcheck disable=SC2029
  ssh root@"${remote_IP}" "sleep 1 && cd /var/www/${site_to_sync}/htdocs && /usr/local/bin/gprestore -sync-only" | tee -a /opt/gridpane/gpclone.log

  echo "Site ${site_to_sync} has been synced to remote system ${remote_IP}..." | tee -a /opt/gridpane/gpclone.log
  sleep 1

  gridpane::callback::app \
    "/site/site-update" \
    "site_url=${site_to_sync}" \
    "server_ip=${remote_IP}" \
    "failover_set_at=now" \
    "silent@True" | tee -a /opt/gridpane/gpclone.log
}

gpclone::sync::ssl() {
  echo "Unison sync ssl..." | tee -a /opt/gridpane/gpclone.log

  if [[ -z $1 ]]; then
    gpclone::end::failsync_pause::exit "We need to know the remote IP to sync ssl to... exiting..."
  fi

  check_ip=$(gridpane::ip_check $1 "clone")
  echo "${check_ip}" | tee -a /opt/gridpane/gpclone.log
  #  remote_ip="$1"

  check_remote_rsync=$(ssh root@${remote_IP} "which rsync")
  if [[ -z ${check_remote_rsync} ]]; then
    echo "Installing rsync on destination..." | tee -a /opt/gridpane/gpclone.log
    ssh root@${remote_IP} "apt-get -y install rsync" | tee -a /opt/gridpane/gpclone.log
  fi

  ssh root@${remote_IP} "sleep 1 && mkdir -p /etc/letsencrypt" | tee -a /opt/gridpane/gpclone.log

  echo "Syncing Certbot SSL" | tee -a /opt/gridpane/gpclone.log

  grid_subdom=$(cat /root/grid.subdom)
  if [[ ! -z ${grid_subdom} ]]; then
    grid_subdom_prefix=${grid_subdom%.gridpanevps.com}
    exclude_subdom="--exclude '${grid_subdom_prefix}*'"
  fi

  echo "exclude subdomain: ${exclude_subdom}" | tee -a /opt/gridpane/gpclone.log
  /usr/bin/rsync -avz --exclude 'accounts' --exclude 'renewal' ${exclude_subdom} /etc/letsencrypt/ root@${remote_IP}:/etc/letsencrypt | tee -a /opt/gridpane/gpclone.log

  echo "Syncing ACME SSL Configs" | tee -a /opt/gridpane/gpclone.log
  ssh root@${remote_IP} "sleep 1 && mkdir -p /root/gridenv/acme-wildcard-configs" | tee -a /opt/gridpane/gpclone.log
  /usr/bin/rsync -avz --exclude '*.acme.sh.log' --exclude '*.account.conf' /root/gridenv/acme-wildcard-configs/* root@${remote_IP}:/root/gridenv/acme-wildcard-configs | tee -a /opt/gridpane/gpclone.log

  echo "Syncing ACME SSL" | tee -a /opt/gridpane/gpclone.log

  if [[ "$webserver" = nginx ]]; then
    ssh root@${remote_IP} "sleep 1 && mkdir -p /etc/nginx/ssl" | tee -a /opt/gridpane/gpclone.log
    /usr/bin/rsync -avz /etc/nginx/ssl/* root@${remote_IP}:/etc/nginx/ssl | tee -a /opt/gridpane/gpclone.log
  else
    /usr/bin/rsync -avz /usr/local/lsws/conf/cert/* root@${remote_IP}:/usr/local/lsws/conf/cert | tee -a /opt/gridpane/gpclone.log
  fi

  echo "Syncing ACME CRON" | tee -a /opt/gridpane/gpclone.log
  crontab -l >/tmp/tempcron
  current_cron=$(cat /tmp/tempcron)
  active_sites=$(cat /root/gridenv/active-sites.env)

  for currfolder in $(find /root/gridenv/acme-wildcard-configs -maxdepth 1 -mindepth 1 -type d); do

    entry=$(basename ${currfolder})
    if [[ ${current_cron} == *"${entry}"* ]]; then
      echo "Checking remote ${entry} CRON" | tee -a /opt/gridpane/gpclone.log

      check_cron=$(ssh root@${remote_IP} "sleep 1 && crontab -l")
      if [[ ${check_cron} != *"/root/gridenv/acme-wildcard-configs/${entry}"* ]]; then
        echo "No cronjob for ${entry} SSL adding..." | tee -a /opt/gridpane/gpclone.log
        min=$(echo $((1 + RANDOM % 59)))
        hr=$(echo $((1 + RANDOM % 23)))
        ssh root@${remote_IP} "sleep 1 && crontab -l > /tmp/tempcron && echo \"${min} ${hr} * * * \"/root/.acme.sh\"/acme.sh --cron --home \"/root/.acme.sh\" --config-home /root/gridenv/acme-wildcard-configs/${entry} >>/opt/gridpane/active.acme.monitoring.log 2>&1\" >> /tmp/tempcron && crontab /tmp/tempcron && rm /tmp/tempcron" |
          tee -a /opt/gridpane/gpclone.log
      fi
    fi
  done
  echo "Rsync ssl done..." | tee -a /opt/gridpane/gpclone.log
}

gpclone::ensure::logs() {
  if [[ ! -f /opt/gridpane/gpclone.log ]]; then
    touch /opt/gridpane/gpclone.log
  fi

  if [[ ! -f /etc/logrotate.d/gpclone ]]; then
    cat >/etc/logrotate.d/gpclone <<EOF
/opt/gridpane/gpclone.log {
        create 0644 root root
        daily
        rotate 7
        missingok
        notifempty
        compress
        sharedscripts
}
EOF
  fi

  if [[ ! -f /opt/gridpane/failover ]]; then
    mkdir -p /opt/gridpane/failover
  fi
}

gpclone::process::migrate() {
  site_to_clone="$1"
  destination="$2"
  destination=$(echo "${destination}" | tr '[:upper:]' '[:lower:]')

  local local_sys_user
  local_sys_user="$(gridpane::get::set::site::user "${site_to_clone}")"
  if [[ $local_sys_user == "gridpane" ||
        $local_sys_user == *"_"* ]]; then

    local notification_body
    if [[ $local_sys_user == "gridpane" ]]; then
      notification_body="Cloning Unable to proceed!<br>${site_to_clone} belongs to the gridpane user, this is restricted going forward. Please transfer to a new user and try again."
    else
      notification_body="Cloning Unable to proceed!<br>${site_to_clone} belongs to the $local_sys_user user, this legacy username includes underscores and will fail site build validation. Please transfer to a new user and try again."
    fi

    gridpane::notify::app \
      "Cloning Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

    gpclone::end::failsync_pause::exit "${site_to_clone} can not be cloned/migrated with current restrictions on the gridpane user."
  fi

  gridpane::site_loop::directories
  if [[ "$(cat /root/gridenv/active-sites.env)" != *"${site_to_clone}"* ]]; then
    gridpane::site_loop::directories

     # shellcheck disable=SC2154
    if [[ ${sitesDirectoryArray} == *"${site_to_clone}"* &&
          ${sitesConfigsArray} == *"${site_to_clone}"* ]]; then
      echo "Active site ${site_to_clone} missing from active sites env, healing..." | tee -a /opt/gridpane/gpclone.log
      gridpane::conf_write ":${site_to_clone}:" active -active-sites.env
    else
      notification_body="Cloning Unable to proceed!<br>${site_to_clone} is reported as not being an active WordPress site on the server, please contact support to proceed..."
      gridpane::notify::app \
        "Cloning Fail" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

      gpclone::end::failsync_pause::exit "${site_to_clone} is reported as not being an active WordPress site"
    fi
  fi

  if [[ "$(cat /root/gridenv/active-sites.env)" == *":${destination}:"* ]]; then
    gridpane::site_loop::directories

    if [[ ${sitesDirectoryArray} == *"${destination}"* &&
          ${sitesConfigsArray} == *"${destination}"* ]]; then
      notification_body="Cloning Unable to proceed!<br>${destination} is an existing active WordPress site on the server, please remove the site and try again!"
      gridpane::notify::app \
        "Cloning Fail" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

      gpclone::end::failsync_pause::exit "${site_to_clone} is reported as not being an active WordPress site"
    else
      echo "Non-existing site ${site_to_clone} found in active sites env... healing..." | tee -a /opt/gridpane/gpclone.log
      gridpane::conf_delete "${site_to_clone}" -active-sites.env
    fi
  fi

  check_for_multisite=$(gridpane::check::multisite "${site_to_clone}")
  if [[ ${check_for_multisite} == "true"* ]] && [[ ! -f /opt/gridpane/jeff.says.swap ]]; then
    notification_body="Cloning Unable to proceed!<br>${site_to_clone} is a multisite installation, GridPane cloning does not currently support this, we recommend AIO Migration Multisite Extension currently"
    gridpane::notify::app \
      "Cloning Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

    gpclone::end::failsync_pause::exit "${site_to_clone} is a multisite installation, GridPane cloning does not currently support this"
  fi

  if [[ -d "/var/www/${site_to_clone}" ]]; then
    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Building new ${destination} site directory and db table structure.."
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

    gpclone::add::new_site -same-server "${destination}"

    RETRY=0

    # We can't rely on using cache plugins for nginx to mark site build success...
    # New build options allow sites with no caching == no plugin == stall!
    # Swapping to login mu plugin instead.

    while [[ ! -f /var/www/${destination}/htdocs/wp-content/mu-plugins/wp-cli-login-server.php ]]; do
      echo "Waiting for ${destination} to provision (max 300s)... ${RETRY}s " | tee -a /opt/gridpane/gpclone.log
      sleep 1

      RETRY=$(($RETRY + 1))
      if [[ ${RETRY} -gt 300 ]]; then
        notification_body="Building new '${destination}' site stalled, exiting process..."
        gridpane::notify::app \
          "Cloning Fail" \
          "${notification_body}" \
          "popup_and_center" \
          "fa-exclamation" \
          "infinity" \
          "${site_to_clone}"
        echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

        gpclone::end::failsync_pause::exit "${notification_body}"
      fi
    done

    echo "Created new site ${destination}" | tee -a /opt/gridpane/gpclone.log

    if [[ ${check_for_multisite} == "true"* ]]; then
      if [[ ${check_for_multisite} == *"sudomain" ]]; then
        multisite_type="-multisubdom"
      elif [[ ${check_for_multisite} == *"subdirectory" ]]; then
        multisite_type="-multisubdir"
      fi
      echo "Converting ${destination} to multisite ${check_for_multisite}..." | tee -a /opt/gridpane/gpclone.log
      /usr/local/bin/gp site "${destination}" "${multisite_type}" | tee -a /opt/gridpane/gpclone.log
    fi

    cd /var/www/"${site_to_clone}"/htdocs || return

    gpclone::export::site "${site_to_clone}"

    if [[ -f /tmp/${nonce}.failed.db.export ]]; then
      notification_title="Cloning Fail"
      notification_body="Process Failed<br>Source:${site_to_clone}<br>Destination:${destination}<br>Source ${site_to_clone} DB Failed to export..."
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      rm /tmp/${nonce}.failed.db.export
    else
      notification_title="Cloning Notice"
      notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Source ${site_to_clone} exported.."
      notification_icon="fa-clone"
      notification_duration="default"
    fi

    gridpane::notify::app \
      "${notification_title}" \
      "${notification_body}" \
      "popup_and_center" \
      "${notification_icon}" \
      "${notification_duration}" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

    mv "/var/www/${site_to_clone}/GPBUP-${site_to_clone}-CLONE.gz" "/var/www/${destination}/GPBUP-${site_to_clone}-CLONE.gz" | tee -a /opt/gridpane/gpclone.log
    # shellcheck disable=SC2012
    ls -l "/var/www/${destination}" | tee -a /opt/gridpane/gpclone.log

    gpclone::extract::replace "${site_to_clone}" "${destination}" "${check_for_multisite}"

    if [[ ! -f /tmp/${nonce}.failed.db.import ]]; then
      gpclone::match::state "${site_to_clone}" "${destination}"
    else
      notification_body="Process Update<br>Origin:${site_to_clone}<br>Destination:${destination}<br>Failed to clone site to new URL, database import failed."
      gridpane::notify::app \
        "Cloning Fail" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
    fi
  else
    echo "This is a migration from a different server. Pulling down the most recent full B2 hourly backup but not anymore..." | tee -a /opt/gridpane/gpclone.log
  fi

  gpclone::end::failsync_pause::exit ""
}

gpclone::match::state() {
  local site_to_clone="$1"
  local destination="$2"

  notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Source ${site_to_clone} WordPress core/db cloned to ${destination}, proceeding to match state..."
  gridpane::notify::app \
    "Cloning Notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${site_to_clone}"
  echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

  echo "Site ${site_to_clone} has been cloned to the ${destination} directory..." | tee -a /opt/gridpane/gpclone.log

  echo "Copying ${site_to_clone} user-configs.php to ${destination}" | tee -a /opt/gridpane/gpclone.log
  cp /var/www/"${site_to_clone}"/user-configs.php /var/www/"${destination}"/user-configs.php

  local currentSiteUser
  currentSiteUser=$(gridpane::get::set::site::user "${destination}")

  if [[ ${site_to_clone} == "staging."* &&
        ${destination} != "staging."* ]]; then
    {
      echo "Cloning to Non Staging from Staging... removing force login plugin..."
      sudo su - "${currentSiteUser}" -c "timeout 60 /usr/local/bin/wp plugin deactivate wp-force-login --uninstall --path=/var/www/${destination}/htdocs 2>&1"
    } | tee -a /opt/gridpane/gpclone.log
  fi

  if [[ -f /var/www/"${destination}"/htdocs/grid.user ]]; then
    local matchedSiteUser
    matchedSiteUser=$(awk '{print $1; exit}' /var/www/"${destination}"/htdocs/grid.user)

    local userID
    userID=$(id -u "${matchedSiteUser}" 2>&1)
    if [[ $? -eq 0 &&
          ${matchedSiteUser} != "${currentSiteUser}" ]]; then
      {
        echo "Changing ${destination} site ownership from ${currentSiteUser} to ${matchedSiteUser}..."
        /usr/local/bin/gpswitch "${destination}" "${matchedSiteUser}"
      } | tee -a /opt/gridpane/gpclone.log
    fi
    rm /var/www/"${destination}"/htdocs/grid.user | tee -a /opt/gridpane/gpclone.log
  fi

  local source_cache_type
  local destination_cache_type
  if [[ "$webserver" = nginx ]]; then
    source_cache_type=$(/usr/local/bin/gp conf -ngx -cache-check "${site_to_clone}" -q)
    destination_cache_type=$(/usr/local/bin/gp conf -ngx -cache-check "${destination}" -q)
  else
    source_cache_type="$(/usr/local/bin/gpols get "$site_to_clone" cache)"
    destination_cache_type="$(/usr/local/bin/gpols get "$destination" cache)"
  fi

  local cache_command
  if [[ ${destination_cache_type} != "${source_cache_type}" ]]; then
    if [[ "$webserver" = nginx  ]]; then
      if [[ ${source_cache_type} == "php" ]]; then
        cache_command="-cache-off"
        source_cache_type="none"
      elif [[ ${source_cache_type} == "fastcgi" ]]; then
        cache_command="-fastcgi-cache"
      elif [[ ${source_cache_type} == "redis" ]]; then
        cache_command="-redis-cache"
      fi
    else
      cache_command="-caching=${source_cache_type}"
    fi

    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Adjusting ${destination} caching to ${source_cache_type}"
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

    echo "Adjusting caching for ${destination} to ${source_cache_type}" | tee -a /opt/gridpane/gpclone.log
    if [[ ${destination_cache_type} == *"fastcgi"* ||
          ${destination_cache_type} == *"redis"* ]] &&
      [[ ${source_cache_type} != "php" ]]; then
      /usr/local/bin/gp site "${destination}" -cache-off
    fi
    /usr/local/bin/gp site "${destination}" "${cache_command}" | tee -a /opt/gridpane/gpclone.log
  fi

  local source_conf_type
  local destination_conf_type
  if [[ "$webserver" = nginx ]]; then
    source_conf_type=$(/usr/local/bin/gp conf -nginx -type-check "${site_to_clone}" -q)
    destination_conf_type=$(/usr/local/bin/gp conf -nginx -type-check "${destination}" -q)
  else
    source_conf_type="$(/usr/local/bin/gpols get "$site_to_clone" vhconf)"
    destination_conf_type="$(/usr/local/bin/gpols get "$destination" vhconf)"
  fi

  if [[ "$webserver" = nginx && "$source_conf_type" == *"https"* && "$destination_conf_type" != *"https"* ||
        "$webserver" = openlitespeed && "$source_conf_type" == *"vhssl"* && "$destination_conf_type" != *"vhssl"* ]]; then
    echo "Grabbing SSL for ${destination}" | tee -a /opt/gridpane/gpclone.log
    /usr/local/bin/gp site "${destination}" -primary-ssl-on | tee -a /opt/gridpane/gpclone.log
  fi

  local source_php_ver
  source_php_ver=$(gridpane::get::site::php "${site_to_clone}")
  local destination_php_ver
  destination_php_ver=$(gridpane::get::site::php "${destination}")

  if [[ ${source_php_ver} != "${destination_php_ver}" ]]; then
    echo "Adjusting PHP for ${destination} to ${source_php_ver}" | tee -a /opt/gridpane/gpclone.log
    /usr/local/bin/gp site "${destination}" -switch-php "${source_php_ver}" | tee -a /opt/gridpane/gpclone.log
  fi

  local source_user
  source_user=$(gridpane::get::set::site::user "${site_to_clone}")
  local destination_user
  destination_user=$(gridpane::get::set::site::user "${destination}")
  source_php_ver=$(gridpane::get::site::php "${site_to_clone}")
  destination_php_ver=$(gridpane::get::site::php "${destination}")

  # TODO @Jeff - Cim need this for OLS too
  if [[ "$webserver" = nginx ]]; then
    local phpFileVer
    phpFileVer=${source_php_ver/./}
    local destPhpFileVer
    destPhpFileVer=${destination_php_ver/./}

    local proceed_with_fpm
    case $destination_php_ver in
      7.1|7.2|7.3|7.4) echo "Destination PHP Version valid: $destination_php_ver" | tee -a /opt/gridpane/gpclone.log ;;
      *)
        echo "Destination PHP Version not valid: $destination_php_ver" | tee -a /opt/gridpane/gpclone.log
        proceed_with_fpm="false"
        ;;
    esac

    case $destPhpFileVer in
      71|72|73|74) echo "Destination PHP Sock Version valid: $destPhpFileVer" | tee -a /opt/gridpane/gpclone.log ;;
      *)
        echo "Destination PHP Sock Version not valid: $destPhpFileVer" | tee -a /opt/gridpane/gpclone.log
        proceed_with_fpm="false"
        ;;
    esac

    case $phpFileVer in
      71|72|73|74) echo "Source PHP Sock Version valid: $phpFileVer" | tee -a /opt/gridpane/gpclone.log ;;
      *)
        echo "Source PHP Sock Version not valid: $phpFileVer" | tee -a /opt/gridpane/gpclone.log
        proceed_with_fpm="false"
        ;;
    esac

    if [[ $(cat /etc/passwd) != *"/home/${source_user}:"* ]]; then
      echo "Source System User not Valid: ${source_user}" | tee -a /opt/gridpane/gpclone.log
      proceed_with_fpm="false"
    else
      echo "Source System User Valid: ${source_user}" | tee -a /opt/gridpane/gpclone.log
    fi

    if [[ $(cat /etc/passwd) != *"/home/${destination_user}:"* ]]; then
      echo "Destination System User not Valid: ${destination_user}" | tee -a /opt/gridpane/gpclone.log
      proceed_with_fpm="false"
    else
      echo "Destination System User Valid: ${destination_user}" | tee -a /opt/gridpane/gpclone.log
    fi

    if [[ ${proceed_with_fpm} != "false" ]]; then
      {
        cp /etc/php/"${source_php_ver}"/fpm/pool.d/"${site_to_clone}".conf /etc/php/"${destination_php_ver}"/fpm/pool.d/"${destination}".conf
        sed -i "s/'${site_to_clone}${phpFileVer}'/'${destination}${destPhpFileVer}'/g" /etc/php/"${destination_php_ver}"/fpm/pool.d/"${destination}".conf
        sed -i "s/\[${site_to_clone}${phpFileVer}\]/\[${destination}${destPhpFileVer}\]/g" /etc/php/"${destination_php_ver}"/fpm/pool.d/"${destination}".conf
        sed -i "/user = ${source_user}/c\user = ${destination_user}" /etc/php/"${destination_php_ver}"/fpm/pool.d/"${destination}".conf
        sed -i "/group = ${source_user}/c\group = ${destination_user}" /etc/php/"${destination_php_ver}"/fpm/pool.d/"${destination}".conf
        sed -i "/listen = \/var/c\listen = \/var\/run\/php\/php${destPhpFileVer}-fpm-${destination}.sock" /etc/php/"${destination_php_ver}"/fpm/pool.d/"${destination}".conf
      } | tee -a /opt/gridpane/gpclone.log
    fi
  fi

  php::sync::site_to_app "${destination}"

  if [[ "$webserver" = nginx && "$source_conf_type" == *"www"* && "$destination_conf_type" != *"www"* ||
        "$webserver" = openlitespeed && "$source_conf_type" == *"route-www"* && "$destination_conf_type" != *"route-www"* ||
        "$webserver" = nginx && "$source_conf_type" == *"root"* && "$destination_conf_type" != *"root"* ||
        "$webserver" = openlitespeed && "$source_conf_type" == *"route-root"* && "$destination_conf_type" != *"route-root"* ]]; then

    local route
    if [[ "$webserver" = nginx && "$source_conf_type" == *"www"* ||
          "$webserver" = openlitespeed && "$source_conf_type" == *"route-www"* ]]; then
      route="www"
    else
      route="root"
    fi

    echo "Adjusting Routing for ${destination} to ${route}" | tee -a /opt/gridpane/gpclone.log
    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Adjusting ${destination} routing to ${route}"
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"

    /usr/local/bin/gp site "${destination}" -route-domain-"${route}"
  fi

  local source_waf_type
  local destination_waf_type
  source_waf_type=$(gridpane::conf_read waf -site.env "${site_to_clone}")
  destination_waf_type=$(gridpane::conf_read waf -site.env "${destination}")
  if [[ ${source_waf_type} == *"modsec"* &&
        ${destination_waf_type} != *"modsec"* ]]; then

    waf::sync::site_to_app "${site_to_clone}" "modsec"

    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Enabling ModSec WAF for ${destination}"
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

    if [[ ${destination_waf_type} == *"6"* ]]; then
      /usr/local/bin/gp site "${destination}" -6g-off
    elif [[ ${destination_waf_type} == *"7"* ]]; then
      /usr/local/bin/gp site "${destination}" -7g-off
    fi
    /usr/local/bin/gp site "${destination}" -modsec-on

  elif [[ ${source_waf_type} == "6G" || ${source_waf_type} == "6g" ]] &&
      [[ ${destination_waf_type} != "6G" && ${destination_waf_type} != "6g" ]]; then

     waf::sync::site_to_app "${site_to_clone}" "6g"

    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Enabling 6G WAF for ${destination}"
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

    echo "Enabling 6G for ${destination}" | tee -a /opt/gridpane/gpclone.log
    if [[ ${destination_waf_type} == *"modsec"* ]]; then
      /usr/local/bin/gp site "${destination}" -modsec-off
    elif [[ ${destination_waf_type} == *"7"* ]]; then
      /usr/local/bin/gp site "${destination}" -7g-off
    fi
    /usr/local/bin/gp site "${destination}" -6g-on

  elif [[ ${source_waf_type} == "7G" || ${source_waf_type} == "7g" ]] &&
      [[ ${destination_waf_type} != "7G" && ${destination_waf_type} != "7g" ]]; then

    waf::sync::site_to_app "${site_to_clone}" "7g"

    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Enabling 7G WAF for ${destination}"
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

    echo "Enabling 7G for ${destination}" | tee -a /opt/gridpane/gpclone.log
    if [[ ${destination_waf_type} == *"modsec"* ]]; then
      /usr/local/bin/gp site "${destination}" -modsec-off
    elif [[ ${destination_waf_type} == *"6"* ]]; then
      /usr/local/bin/gp site "${destination}" -6g-off
    fi
    /usr/local/bin/gp site "${destination}" -7g-on
  fi

  source_waf_type=$(gridpane::conf_read waf -site.env "${site_to_clone}")
  destination_waf_type=$(gridpane::conf_read waf -site.env "${destination}")
  if [[ ${source_waf_type} == *"modsec"* &&
        ${destination_waf_type} == *"modsec"* ]]; then
    {
      echo "Enabling Modsec for ${destination}"
      cp /var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-crs-anomaly-blocking-threshold.conf \
        /var/www/"${destination}"/modsec/"${destination}"-crs-anomaly-blocking-threshold.conf
      cp /var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-crs-paranoia-level.conf \
        /var/www/"${destination}"/modsec/"${destination}"-crs-paranoia-level.conf
      cp /var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-runtime-exceptions.conf \
        /var/www/"${destination}"/modsec/"${destination}"-runtime-exceptions.conf
      cp /var/www/"${site_to_clone}"/modsec/"${site_to_clone}"-runtime-whitelist.conf \
        /var/www/"${destination}"/modsec/"${destination}"-runtime-whitelist.conf
    } | tee -a /opt/gridpane/gpclone.log

    waf::sync::site_to_app "${destination}" "modsec"

  elif [[ ${source_waf_type} == "6G" || ${source_waf_type} == "6g" ]] &&
      [[ ${destination_waf_type} == "6G" || ${destination_waf_type} == "6g" ]]; then

    cp /etc/nginx/common/"${site_to_clone}"-6g.conf /etc/nginx/common/"${destination}"-6g.conf
    sed -i "/include \/var\/www\/${site_to_clone}/c\include \/var\/www\/${destination}\/nginx\/*-6g-context.conf;" /etc/nginx/common/"${destination}"-6g.conf
    sed -i "/access_log/c\access_log \/var\/www\/${destination}\/logs\/6g.log 6g_log if=\${drop_log};" /etc/nginx/common/"${destination}"-6g.conf

    waf::sync::site_to_app "${destination}" "6g"

  elif [[ ${source_waf_type} == "7G" || ${source_waf_type} == "7g" ]] &&
      [[ ${destination_waf_type} == "7G" || ${destination_waf_type} == "7g" ]]; then

    cp /etc/nginx/common/"${site_to_clone}"-7g.conf /etc/nginx/common/"${destination}"-7g.conf
    sed -i "/include \/var\/www\/${site_to_clone}/c\include \/var\/www\/${destination}\/nginx\/*-7g-context.conf;" /etc/nginx/common/"${destination}"-7g.conf
    sed -i "/access_log/c\access_log \/var\/www\/${destination}\/logs\/7g.log 7g_log if=\${7g_drop};" /etc/nginx/common/"${destination}"-7g.conf

    waf::sync::site_to_app "${destination}" "7g"
  fi

  waf::sync::site_to_app "${site_to_clone}" "wpf2b"
  local site_wpfail2ban
  site_wpfail2ban=$(gridpane::conf_read wp-fail2ban -site.env "${site_to_clone}")
  if [[ ${site_wpfail2ban} == *"rue"* ||
        ${site_wpfail2ban} == *"on"* ]]; then
    {
      echo "----------------------------------------------------------------------"
      echo "Enabling ${destination} wpfail2ban configurations"
    } | tee -a /opt/gridpane/gpclone.log
    /usr/local/bin/gp site "${destination}" -enable-wp-fail2ban

    local site_wpf2b_config
    site_wpf2b_config=$(cat /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)

    local source_fail2ban_user_enumeration
    source_fail2ban_user_enumeration=$(grep "WP_FAIL2BAN_BLOCK_USER_ENUMERATION" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_user_enumeration ]]; then
      sed -i "/WP_FAIL2BAN_BLOCK_USER_ENUMERATION/d" /var/www/"${destination}"/wp-fail2ban-configs.php
      echo "$source_fail2ban_user_enumeration" | tee -a /var/www/"${destination}"/wp-fail2ban-configs.php /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_blocked_users
    source_fail2ban_blocked_users=$(grep "WP_FAIL2BAN_BLOCKED_USERS" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_blocked_users ]]; then
      sed -i "/WP_FAIL2BAN_BLOCKED_USERS/d" /var/www/"${destination}"/wp-fail2ban-configs.php
      echo "$source_fail2ban_blocked_users" | tee -a /var/www/"${destination}"/wp-fail2ban-configs.php /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_comments
    source_fail2ban_log_comments=$(grep "WP_FAIL2BAN_LOG_COMMENTS'" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_comments ]]; then
      sed -i "/WP_FAIL2BAN_LOG_COMMENTS'/d" /var/www/"${destination}"/wp-fail2ban-configs.php
      echo "$source_fail2ban_log_comments" | tee -a /var/www/"${destination}"/wp-fail2ban-configs.php /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_comments_extra
    source_fail2ban_log_comments_extra=$(grep "WP_FAIL2BAN_LOG_COMMENTS_EXTRA" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_comments_extra ]]; then
      sed -i "/WP_FAIL2BAN_LOG_COMMENTS_EXTRA/d" /var/www/"${destination}"/wp-fail2ban-configs.php
      echo "$source_fail2ban_log_comments_extra" | tee -a /var/www/"${destination}"/wp-fail2ban-configs.php /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_password_request
    source_fail2ban_log_password_request=$(grep "WP_FAIL2BAN_LOG_PASSWORD_REQUEST" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_password_request ]]; then
      sed -i "/WP_FAIL2BAN_LOG_PASSWORD_REQUEST/d" /var/www/"${destination}"/wp-fail2ban-configs.php
      echo "$source_fail2ban_log_password_request" | tee -a /var/www/"${destination}"/wp-fail2ban-configs.php /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_pingbacks
    source_fail2ban_log_pingbacks=$(grep "WP_FAIL2BAN_LOG_PINGBACKS" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_pingbacks ]]; then
      sed -i "/WP_FAIL2BAN_LOG_PINGBACKS/d" /var/www/"${destination}"/wp-fail2ban-configs.php
      echo "$source_fail2ban_log_pingbacks" | tee -a /var/www/"${destination}"/wp-fail2ban-configs.php /opt/gridpane/gpclone.log
    fi

    local source_fail2ban_log_spam
    source_fail2ban_log_spam=$(grep "WP_FAIL2BAN_LOG_SPAM" /var/www/"${site_to_clone}"/wp-fail2ban-configs.php)
    if [[ -n $source_fail2ban_log_spam ]]; then
      sed -i "/WP_FAIL2BAN_LOG_SPAM/d" /var/www/"${destination}"/wp-fail2ban-configs.php
      echo "$source_fail2ban_log_spam" | tee -a /var/www/"${destination}"/wp-fail2ban-configs.php /opt/gridpane/gpclone.log
    fi

    waf::sync::site_to_app "${destination}" "wpf2b"
  fi

  if [[ "$webserver" = nginx ]]; then
    nginx::sync::additional_security "${site_to_clone}"
    {
      echo "----------------------------------------------------------------------"
      echo "Import pulling site Nginx configurations..."
    } | tee -a /opt/gridpane/gpclone.log

    [[ -f /var/www/"${destination}"/nginx/disable-xmlrpc-main-context.conf ]] &&
      rm /var/www/"${destination}"/nginx/disable-xmlrpc-main-context.conf
    [[  -f /var/www/"${destination}"/logs/disable.xmlrpc.env ]] &&
      rm /var/www/"${destination}"/logs/disable.xmlrpc.env

    [[ -f /var/www/"${destination}"/nginx/disable-load-scripts-concatenation-main-context.conf ]] &&
      rm /var/www/"${destination}"/nginx/disable-load-scripts-concatenation-main-context.conf
    [[ -f /var/www/"${destination}"/logs/disable.wpadmin.concatenation.env ]] &&
      rm /var/www/"${destination}"/logs/disable.wpadmin.concatenation.env

    [[ -f /var/www/"${destination}"/nginx/disable-wp-content-php-main-context.conf ]] &&
      rm /var/www/"${destination}"/nginx/disable-wp-content-php-main-context.conf
    [[ -f /var/www/"${destination}"/logs/disable.wpcontent.php.env ]] &&
      rm /var/www/"${destination}"/logs/disable.wpcontent.php.env

    [[ -f /var/www/"${destination}"/nginx/disable-wp-comments-post-main-context.conf ]] &&
      rm /var/www/"${destination}"/nginx/disable-wp-comments-post-main-context.conf
    [[ -f /var/www/"${destination}"/logs/disable.wp.comments.post.env ]] &&
      rm /var/www/"${destination}"/logs/disable.wp.comments.post.env

    [[ -f /var/www/"${destination}"/nginx/disable-wp-links-opml-main-context.conf ]] &&
      rm /var/www/"${destination}"/nginx/disable-wp-links-opml-main-context.conf
    [[ -f /var/www/"${destination}"/logs/disable.wp.links.opml.env ]] &&
      rm /var/www/"${destination}"/logs/disable.wp.links.opml.env

    [[ -f /var/www/"${destination}"/nginx/disable-wp-trackbacks-main-context.conf ]] &&
      rm /var/www/"${destination}"/nginx/disable-wp-trackbacks-main-context.conf
    [[ -f /var/www/"${destination}"/logs/disable.wp.trackbacks.env ]] &&
      rm /var/www/"${destination}"/logs/disable.wp.trackbacks.env

    [[ -f /var/www/"${destination}"/nginx/block-install-file-main-context.conf ]] &&
      rm /var/www/"${destination}"/nginx/block-install-file-main-context.conf
    [[ -f /var/www/"${destination}"/logs/disable.wpadmin.install.env ]] &&
      rm /var/www/"${destination}"/logs/disable.wpadmin.install.env

    [[ -f /var/www/"${destination}"/nginx/block-upgrade-file-main-context.conf ]] &&
      rm /var/www/"${destination}"/nginx/block-upgrade-file-main-context.conf
    [[ -f /var/www/"${destination}"/logs/disable.wpadmin.upgrade.env ]] &&
      rm /var/www/"${destination}"/logs/disable.wpadmin.upgrade.env

    {
      cp /var/www/"${site_to_clone}"/nginx/* /var/www/"${destination}"/nginx/

      if [[ -f /var/www/"${destination}"/nginx/disable-load-scripts-concatenation-main-context.conf ]]; then
        sed -i "/CONCATENATE_SCRIPTS/c\define('CONCATENATE_SCRIPTS', false);" /var/www/"${destination}"/wp-config.php
      fi

      [[ -f /var/www/${destination}/nginx/${site_to_clone}-sockfile.conf ]] &&
        rm "/var/www/${destination}/nginx/${site_to_clone}-sockfile.conf"

      find "/var/www/${destination}/nginx/" -mindepth 1 -maxdepth 1 -name '*.conf' -type f -exec sh -c \
        'i="$1"; sed -i "s|www/'"${site_to_clone}"'/nginx|www/'"${destination}"'/nginx|g" $i' _ {} \;

      mv /var/www/"${destination}"/nginx/"${site_to_clone}"-headers-csp.conf /var/www/"${destination}"/nginx/"${destination}"-headers-csp.conf

      if [[ "${site_to_clone}" == "staging."* && "${destination}" != "staging."* ]]; then
        [[ -f /var/www/${destination}/nginx/x-robots-noindex-main-context.conf ]] &&
          echo "Cloning from Staging to Non Staging... removing noindex conf first..." &&
          rm /var/www/"${destination}"/nginx/x-robots-noindex-main-context.conf
      fi

      # shellcheck disable=SC2012
      ls -l /var/www/"${destination}"/nginx
      nginx::sync::additional_security "${destination}"
      nginx::sync::access_settings "${destination}"
      echo "----------------------------------------------------------------------"
    } | tee -a /opt/gridpane/gpclone.log
  else
    cp /var/www/"${site_to_clone}"/ols/* /var/www/"${destination}"/ols/ | tee -a /opt/gridpane/gpclone.log
  fi

  smtp=$(gridpane::conf_read smtp -site.env "${site_to_clone}")
  if [[ ${smtp} == "true" ]] || [[ ${smtp} == "on" ]]; then
    if [[ -f /var/www/"${site_to_clone}"/sendgrid-wp-configs.php ]]; then
      notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Enabling SendGrid SMTP for ${destination}"
      gridpane::notify::app \
        "Cloning Notice" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-clone" \
        "long" \
        "${site_to_clone}"
      echo "${notification_body}"

      api_key=$(grep "SMTP_PASS" /var/www/"${site_to_clone}"/sendgrid-wp-configs.php)
      api_key=${api_key//\"/}
      api_key=${api_key// /}
      api_key=${api_key#define(SMTP_PASS,}
      api_key=${api_key%');'}

      {
        echo "Enabling SMTP for ${destination}" | tee -a /opt/gridpane/gpclone.log
#        /usr/local/bin/gp site "${destination}" -mailer enable "SendGrid" "${api_key}" "simple"
      } | tee -a /opt/gridpane/gpclone.log
    fi
  fi

  http_auth=$(gridpane::conf_read http-auth -site.env "${site_to_clone}")
  if [[ ${http_auth} == "true" ]] || [[ ${http_auth} == "on" ]]; then
    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Enabling HTTP Auth for ${destination}"
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

    {
      echo "Enabling HTTP Auth for ${destination}"
      /usr/local/bin/gp site "${destination}" -http-auth
    } | tee -a /opt/gridpane/gpclone.log
  fi

  notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Matched PHP INI and Pool.D Configurations for ${destination}"
  gridpane::notify::app \
    "Cloning Notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${site_to_clone}"
  echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

  if [[ "$webserver" = nginx ]]; then
    destination_conf_type_recheck=$(/usr/local/bin/gp conf -nginx -type-check "${destination}" -q)
    if [[ ${destination_conf_type_recheck} != *"proxy"* ]]; then
      site_nginx_fcgi_cache_valid=$(gridpane::conf_read nginx-fcgi-cache-valid -site.env "${site_to_clone}")
      if [[ -n ${site_nginx_fcgi_cache_valid} &&
            ${site_nginx_fcgi_cache_valid} != "1" ]]; then
        nginx_migrations="fcgi-cache-valid, "
        {
          echo "Matching ${destination} nginx fastcgi -cache-valid"
          /usr/local/bin/gp site "${destination}" nginx fastcgi -cache-valid "${site_nginx_fcgi_cache_valid}"
        } | tee -a /opt/gridpane/gpclone.log
      fi
    else
      site_nginx_proxy_cache_valid=$(gridpane::conf_read nginx-proxy-cache-valid -site.env "${site_to_clone}")
      if [[ -n ${site_nginx_proxy_cache_valid} &&
            ${site_nginx_proxy_cache_valid} != "1" ]]; then
        nginx_migrations="proxy-cache-valid, "
        {
          echo "Matching ${destination} nginx proxy -cache-valid"
          /usr/local/bin/gp site "${destination}" nginx proxy -cache-valid "${site_nginx_proxy_cache_valid}"
        } | tee -a /opt/gridpane/gpclone.log
      fi
    fi

    site_nginx_redis_cache_valid=$(gridpane::conf_read nginx-redis-cache-valid -site.env "${site_to_clone}")
    if [[ -n ${site_nginx_redis_cache_valid} &&
          ${site_nginx_redis_cache_valid} != "2592000" ]]; then
      nginx_migrations="redis-cache-valid, "
      {
        echo "Matching ${destination} nginx redis -cache-valid"
        /usr/local/bin/gp site "${destination}" nginx redis -cache-valid "${site_nginx_redis_cache_valid}"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    site_zone_one_burst=$(gridpane::conf_read zone-one-burst -site.env "${site_to_clone}")
    if [[ -n ${site_zone_one_burst} &&
          ${site_zone_one_burst} != "1" ]]; then
      nginx_migrations="${nginx_migrations}zone-one-burst, "
      {
        echo "Matching ${destination} nginx zone-one-burst"
        /usr/local/bin/gp site "${destination}" -nginx-zone-burst one "${site_zone_one_burst}"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    site_zone_wp_burst=$(gridpane::conf_read zone-wp-burst -site.env "${site_to_clone}")
    if [[ -n ${site_zone_wp_burst} &&
          ${site_zone_wp_burst} != "6" ]]; then
      nginx_migrations="${nginx_migrations}zone-wp-burst, "
      {
        echo "Matching ${destination} nginx zone-wp-burst"
        /usr/local/bin/gp site "${destination}" -nginx-zone-burst wp "${site_zone_wp_burst}"
      } | tee -a /opt/gridpane/gpclone.log
    fi

    if [[ -n ${nginx_migrations} ]]; then
      notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Matched Nginx Configurations for ${destination}"
      gridpane::notify::app \
        "Cloning Notice" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-clone" \
        "long" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
    fi

    if [[ ${destination_conf_type_recheck} == *"https"* ]]; then
      site_brotli=$(gridpane::conf_read brotli -site.env "${site_to_clone}")
      if [[ ${site_brotli} == *"rue"* ||
            ${site_brotli} == *"on"* ]]; then
        other_optims="brotli enabled, "
        {
          echo "Enabling ${destination} brotli compression"
          /usr/local/bin/gp site "${destination}" -brotli-on
        } | tee -a /opt/gridpane/gpclone.log
      fi
      site_http2_push=$(gridpane::conf_read http2-push -site.env "${site_to_clone}")
      if [[ ${site_http2_push} == *"rue"* ||
            ${site_http2_push} == *"on"* ]]; then
        other_optims="${other_optims}http2 push preload enabled, "
        {
          echo "Enabling ${destination} http2 push preload"
          /usr/local/bin/gp site "${destination}" -http2-push-preload-on
        } | tee -a /opt/gridpane/gpclone.log
      fi
    fi

    site_csfp_headers=$(gridpane::conf_read csfp-headers -site.env "${site_to_clone}")
    if [[ ${site_csfp_headers} == *"rue"* ||
          ${site_csfp_headers} == *"on"* ]]; then
      other_optims="${other_optims}csp headers enabled, "
      {
        echo "Enabling ${destination} CSP and FP Headers"
        /usr/local/bin/gp site "${destination}" -csp-header-on
      } | tee -a /opt/gridpane/gpclone.log
    fi

    site_clickjacking_protection_off=$(gridpane::conf_read clickjacking-protection -site.env "${site_to_clone}")
    destination_clickjacking_protection_off=$(gridpane::conf_read clickjacking-protection -site.env "${site_to_clone}")
    if [[ ${site_clickjacking_protection_off} == *"alse"* || ${site_clickjacking_protection_off} == *"off"* ]] &&
      [[ ${destination_clickjacking_protection_off} != *"alse"* && ${destination_clickjacking_protection_off} != *"off"* ]]; then
      other_optims="${other_optims}x-frame-options header disabled, "
      {
        echo "Disabling ${destination} x-frame-options clickjacking protection"
        /usr/local/bin/gp site "${destination}" -clickjacking-protection-off
      } | tee -a /opt/gridpane/gpclone.log
    fi
  fi

  site_cron=$(gridpane::conf_read cron -site.env "${site_to_clone}")
  if [[ ${site_cron} == *"rue"* ||
        ${site_cron} == *"on"* ]]; then
    other_optims="${other_optims}gpcron enabled, "
    {
      echo "Enabling ${destination} gpcron"
      /usr/local/bin/gp site "${destination}" -gpcron-on
    } | tee -a /opt/gridpane/gpclone.log
  fi

  # Sync LSAPI
  if [[ "$webserver" = openlitespeed ]]; then
    gp stack ols -site-lsapi "$(gridpane::conf_read lsapi -site.env "$site_to_clone")" "$destination" -no-reload
    gp stack ols -site-lsapi-children "$(gridpane::conf_read lsapi_children -site.env "$site_to_clone")" "$destination" -no-reload
    gp stack ols -site-lsapi-app-instances "$(gridpane::conf_read lsapi_app_instances -site.env "$site_to_clone")" "$destination" -no-reload
    gp stack ols -site-lsapi-app-max-connections "$(gridpane::conf_read lsapi_max_connections -site.env "$site_to_clone")" "$destination" -no-reload
    gp stack ols -site-lsapi-initial-request-timeout "$(gridpane::conf_read lsapi_initial_request_timeout -site.env "$site_to_clone")" "$destination" -no-reload
    gp stack ols -site-lsapi-retry-timeout "$(gridpane::conf_read lsapi_retry_timeout -site.env "$site_to_clone")" "$destination" -no-reload
    gp stack ols -site-lsapi-max-reqs "$(gridpane::conf_read lsapi_max_reqs -site.env "$site_to_clone")" "$destination" -no-reload
    gp stack ols -site-lsapi-max-idle "$(gridpane::conf_read lsapi_max_idle -site.env "$site_to_clone")" "$destination"
  fi

  if [[ -n ${other_optims} ]]; then
    notification_body="Process Complete<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Matched GridPane optimisations and settings for ${destination}"
    gridpane::notify::app \
      "Cloning Complete" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
  else
    notification_body="Process Complete<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Cloning complete, no optimisations to match."
    gridpane::notify::app \
      "Cloning Complete" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
  fi

  # lol
  # Restart OLS one more time for good measure
  [[ "$webserver" = openlitespeed ]] && /usr/local/bin/gpols httpd | tee -a /opt/gridpane/gpclone.log

  rm /var/www/"${destination}"/logs/"${site_to_clone}".env.clone*
}

gpclone::set::remote_ip() {
  check_ip=$(gridpane::ip_check $1 "clone")
  echo "${check_ip}" | tee -a /opt/gridpane/gpclone.log
  readonly remote_IP="$1"
}

gpclone::set::site_to_clone() {
  if [[ -z $1 ]]; then
    gpclone::end::failsync_pause::exit "No site detail passed, exiting!"
  else
    if [[ "$(cat /root/gridenv/active-sites.env)" != *"$1"* ]]; then
      gridpane::site_loop::directories

      if [[ ${sitesDirectoryArray} == *"$1"* ]] && [[ ${sitesConfigsArray} == *"$1"* ]]; then
        echo "$1 missing from active sites env, healing..." | tee -a /opt/gridpane/failsync.log
        gridpane::conf_write ":$1:" active -active-sites.env | tee -a /opt/gridpane/failsync.log
      else
        notification_body="Process Failed<br>Source Server:${serverIP}<br>$1 is not an active site on this server... exiting..."
        gridpane::notify::app \
          "Clone/Migrate Fail" \
          "${notification_body}" \
          "popup_and_center" \
          "fa-clone" \
          "long" \
          "${site_to_clone}"
        echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

        gpclone::end::failsync_pause::exit "$1 is not an active site on this server... exiting..."
      fi
    fi
    site_to_clone=$1
  fi
}

gpclone::clone::single_site::same_url() {
  local local_sys_user
  local_sys_user="$(gridpane::get::set::site::user "${site_to_clone}")"
  if [[ $local_sys_user == "gridpane" ||
        $local_sys_user == *"_"* ]]; then

    local notification_body
    if [[ $local_sys_user == "gridpane" ]]; then
      notification_body="Cloning Unable to proceed!<br>${site_to_clone} belongs to the gridpane user, this is restricted going forward. Please transfer to a new user and try again."
    else
      notification_body="Cloning Unable to proceed!<br>${site_to_clone} belongs to the $local_sys_user user, this legacy username includes underscores and will fail site build validation. Please transfer to a new user and try again."
    fi

    gridpane::notify::app \
      "Cloning Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

    gpclone::end::failsync_pause::exit "${site_to_clone} can not be cloned/migrated with current restrictions on the gridpane user."
  fi

  check_for_multisite=$(gridpane::check::multisite "${site_to_clone}")
  account_type="$(gridpane::get_account_type)"
  if [[ ${check_for_multisite} == "true"* ]] &&
     [[ ${account_type} != *"Developer"* &&
        ${account_type} != *"Agency"* ]]; then
    notification_body="Source:${site_to_clone}<br>Source Server:${serverIP}<br>${site_to_clone} is a multisite installation. Please upgrade to Developer or Agency for Multisite Cloning"
    gridpane::notify::app \
      "Clone/Migrate Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

    gpclone::end::failsync_pause::exit "${site_to_clone} is a multisite installation, GridPane cloning does not currently support this"
  fi

  echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log
  echo "Doing single site clone of ${site_to_clone} to ${remote_IP}..." | tee -a /opt/gridpane/gpclone.log
  echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log

  notification_body="Process Update<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>Building new ${site_to_clone} site directory and app entry."
  gridpane::notify::app \
    "Cloning notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${site_to_clone}"
  echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

  echo "Making ${site_to_clone} on remote..." | tee -a /opt/gridpane/gpclone.log
  gpclone::add::remote_site "${site_to_clone}" "${remote_IP}"

  if [[ $? -ne 0 ]]; then
    notification_body="Process Failed<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>Building new ${site_to_clone} site on ${remote_IP} failed..."
    gridpane::notify::app \
      "Clone/Migrate Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

    echo "Error during previous phase, skipping!" | tee -a /opt/gridpane/gpclone.log
  else
    echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log
    echo "Doing export of ${site_to_clone}..." | tee -a /opt/gridpane/gpclone.log
    echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log

    gpclone::export::site "${site_to_clone}"

    if [[ -f /tmp/${nonce}.failed.db.export ]]; then
      notification_body="Process Failed<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>Failed to export '${site_to_clone}' database..."
      gridpane::notify::app \
        "Clone/Migrate Fail" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
      rm /tmp/"${nonce}".failed.db.export
    else
      notification_body="Process Update<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>${site_to_clone} Clone Package created for export..."
      gridpane::notify::app \
        "Cloning notice" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-clone" \
        "long" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
    fi

    echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log
    echo "Cloning ${site_to_clone} to remote..." | tee -a /opt/gridpane/gpclone.log
    echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log

    gpclone::clone_to_remote "${site_to_clone}" site-clone "${check_for_multisite}"

    echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log
  fi
}

gpclone::clone::single_site::new_url() {
  local local_sys_user
  local_sys_user="$(gridpane::get::set::site::user "${site_to_clone}")"
  if [[ $local_sys_user == "gridpane" ||
        $local_sys_user == *"_"* ]]; then

    local notification_body
    if [[ $local_sys_user == "gridpane" ]]; then
      notification_body="Cloning Unable to proceed!<br>${site_to_clone} belongs to the gridpane user, this is restricted going forward. Please transfer to a new user and try again."
    else
      notification_body="Cloning Unable to proceed!<br>${site_to_clone} belongs to the $local_sys_user user, this legacy username includes underscores and will fail site build validation. Please transfer to a new user and try again."
    fi

    gridpane::notify::app \
      "Cloning Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

    gpclone::end::failsync_pause::exit "${site_to_clone} can not be cloned/migrated with current restrictions on the gridpane user."
  fi

  check_for_multisite=$(gridpane::check::multisite "${site_to_clone}")
  if [[ ${check_for_multisite} == "true"* ]]; then
    notification_body="Process Failed<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>${site_to_clone} is a multisite installation, GridPane cloning does not currently support this, we recommend AIO Migration Multisite Extension currently"
    gridpane::notify::app \
      "Clone/Migrate Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

    gpclone::end::failsync_pause::exit "${site_to_clone} is a multisite installation, GridPane cloning does not currently support this"
  fi

  if [[ -z $1 ]]; then
    gpclone::end::failsync_pause::exit "Missing the new site URL, exiting!"
  elif [[ $1 != *"."* ]]; then
    notification_body="$1 is not a url, Cloning can not proceed..."
    gridpane::notify::app \
      "Clone/Migrate Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
    gpclone::end::failsync_pause::exit "${notification_body}"
  elif [[ $1 == "www."* ]]; then
    notification_body="URL Passed - $1 - contains the www host, Cloning can not proceed..."
    gridpane::notify::app \
      "Clone/Migrate Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
    gpclone::end::failsync_pause::exit "${notification_body}"
  fi

  # Because DOH! thanks Leo
  readonly new_url="$(echo "$1" | tr '[:upper:]' '[:lower:]')"

  notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${new_url}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>Building new ${new_url} site directory and app entry."
  gridpane::notify::app \
    "Cloning notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${site_to_clone}"
  echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

  {
    echo "--------------------------------------------------------------------"
    echo "Doing single site clone of ${site_to_clone} to ${remote_IP} with new url ${new_url}..."
    echo "Now checking/cloning site: ${site_to_clone}"
    echo "--------------------------------------------------------------------"
    echo "Making ${new_url} on remote..."
    echo "--------------------------------------------------------------------"
  } | tee -a /opt/gridpane/gpclone.log
  gpclone::add::new_site -different-server "${new_url}" "${remote_IP}"

  if [[ $? -ne 0 ]]; then
    notification_body="Process Failed<br>Source:${site_to_clone}<br>Destination:${new_url}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>Building new ${destination} site on ${remote_IP} failed..."
    gridpane::notify::app \
      "Clone/Migrate Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    {
      echo "${notification_body}"
      echo "Error during previous phase - making new site..."
    } | tee -a /opt/gridpane/gpclone.log
  else
    {
      echo "--------------------------------------------------------------------"
      echo "Doing export of ${site_to_clone}..."
      echo "--------------------------------------------------------------------"
    } | tee -a /opt/gridpane/gpclone.log
    gpclone::export::site "${site_to_clone}" alternate

    if [[ -f /tmp/${nonce}.failed.db.export ]]; then
      notification_body="Process Failed<br>Source:${site_to_clone}<br>Destination:${new_url}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>Source ${site_to_clone} DB Failed to export..."
      gridpane::notify::app \
        "Clone/Migrate Fail" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
      rm /tmp/"${nonce}".failed.db.export
    else
      notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${new_url}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>${site_to_clone} Clone Package created for export..."
      gridpane::notify::app \
        "Cloning notice" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-clone" \
        "long" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
    fi

    {
      echo "--------------------------------------------------------------------"
      echo "Cloning ${site_to_clone} to remote..."
      echo "--------------------------------------------------------------------"
    } | tee -a /opt/gridpane/gpclone.log
    gpclone::migrate_to_remote "${site_to_clone}" "${new_url}" "${check_for_multisite}"
    echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log
  fi
  echo "Site was not in sync list, added to remote server, exported and updated to remote with new url." | tee -a /opt/gridpane/gpclone.log
}

gpclone::clone::all_sites::same_url() {
  if [[ $1 == "-failover-stop" ]] || [[ $1 == "failover-stop" ]]; then
    echo "Stopping failover, deleting synced-from logs..." | tee -a /opt/gridpane/gpclone.log

    active_sites=$(cat /root/gridenv/active-sites.env)
    gridpane::site_loop::directories

    # shellcheck disable=SC2044
    for currfolder in $(find /var/www -maxdepth 1 -mindepth 1 -type d); do
      entry=$(basename "${currfolder}")

      if [[ ${sitesConfigsArray} != *"${entry}"* ]]; then
        echo "${entry} has a directory, but no enabled Nginx config"
        continue
      fi

      managed_domain="$(gridpane::get_managed_domain)"
      case $entry in
      22222)
        echo "Skipping 22222" | tee -a /opt/gridpane/gpclone.log
        continue
        ;;
      default)
        echo "Skipping default" | tee -a /opt/gridpane/gpclone.log
        continue
        ;;
      *."${managed_domain}")
        {
          if [[ $entry == *"stat"* ]]; then
            echo "Skipping Administrative $entry (Monit Stats) Site..."
          else
            echo "Skipping Administrative $entry (PHPMA) Site..."
          fi
        } | tee -a /opt/gridpane/gpclone.log
        ;;
      *-remote)
        echo "Skipping alternative $entry remote restore Site..." | tee -a /opt/gridpane/gpclone.log
        continue
        ;;
      html)
        echo "Skipping html..." | tee -a /opt/gridpane/gpclone.log
        continue
        ;;
      *)
        if [[ "${active_sites}" != *"${entry}"* ]]; then
          if [[ ${sitesDirectoryArray} == *"${entry}"* &&
                ${sitesConfigsArray} == *"${entry}"* ]]; then
            echo "Active site ${entry} missing from active sites env, healing..."  | tee -a /opt/gridpane/gpclone.log
            gridpane::conf_write ":${entry}:" active -active-sites.env
          else
            echo "Skipping ${entry} - not in active sites list and inactive... probably a remnant..."  | tee -a /opt/gridpane/gpclone.log
            continue
          fi
        fi

        site_to_clone="$entry"
        if [[ -f /var/www/${site_to_clone}/logs/synced-from.log ]]; then
          echo "remove /var/www/${site_to_clone}/logs/synced-from.log" | tee -a /opt/gridpane/gpclone.log
          rm /var/www/"${site_to_clone}"/logs/synced-from.log
        fi

        gpclone::failover_constant unset "${site_to_clone}"

        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_to_clone}" \
          "server_ip=${remote_IP}" \
          "-s" "failover_set_at=NULL" \
          "silent@True" | tee -a /opt/gridpane/gpclone.log
        ;;
      esac
    done

    echo "Updating server conf and resetting gpfailsync..." | tee -a /opt/gridpane/gpclone.log

    gridpane::conf_write gpfailsync-running false
    gridpane::conf_delete gpfailsync-role
    gridpane::conf_delete gpfailsync-server-worker-pause

  else
    gpclone::failsync_pause "Cloning proceeding..."
    ssh root@"${remote_IP}" "sudo /usr/local/bin/gp conf write gpfailsync-server-worker-pause true" | tee -a /opt/gridpane/gpclone.log

    is_failover_sync=$(gridpane::conf_read gpfailsync-role)
    echo "gpfailsync-role: ${is_failover_sync}" | tee -a /opt/gridpane/gpclone.log

    if [[ ${is_failover_sync} == "source" ]]; then

      if [[ -f "/opt/gridpane/failover/syncsites-to-${remote_IP}.log" ]]; then
        echo "Synced sites log for ${remote_IP} ready..." | tee -a /opt/gridpane/gpclone.log
      else
        mkdir -p /opt/gridpane/failover
        touch /opt/gridpane/failover/syncsites-to-${remote_IP}.log
      fi

      failover="true"
      check_destination=$(ssh root@"${remote_IP}" "sudo /usr/local/bin/gp conf read gpfailsync-running" 2>&1)
      echo "Destination gpfailsync-running: ${check_destination}" | tee -a /opt/gridpane/gpclone.log

      if [[ ${check_destination} != "true" ]]; then
        echo "Enabling Destination gpfailsync-running" | tee -a /opt/gridpane/gpclone.log
        ssh root@"${remote_IP}" "sudo /usr/local/bin/gp conf write gpfailsync-running true" | tee -a /opt/gridpane/gpclone.log
      fi

      check_destination=$(ssh root@"${remote_IP}" "sudo /usr/local/bin/gp conf read gpfailsync-role" 2>&1)
      echo "Destination gpfailsync-role: ${check_destination}" | tee -a /opt/gridpane/gpclone.log

      if [[ ${check_destination} != "destination" ]]; then
        echo "Ensuing Destination gpfailsync-role" | tee -a /opt/gridpane/gpclone.log
        ssh root@"${remote_IP}" "sudo /usr/local/bin/gp conf write gpfailsync-role destination" | tee -a /opt/gridpane/gpclone.log
      fi

      echo "Pausing Worker Functions during sync" | tee -a /opt/gridpane/gpclone.log

      # shellcheck disable=SC2154
      if [[ ${prometheus} == "42" ]]; then
        gpclone::sync::ssl "${remote_IP}"
      fi
    fi

    active_sites=$(cat /root/gridenv/active-sites.env)
    gridpane::site_loop::directories
    # shellcheck disable=SC2044,SC2029,SC2029
    for currfolder in $(find /var/www -maxdepth 1 -mindepth 1 -type d); do
      entry=$(basename "${currfolder}")

      managed_domain="$(gridpane::get_managed_domain)"
      case $entry in
      2222)
        echo "Skipping 22222" | tee -a /opt/gridpane/gpclone.log
        continue
        ;;
      default)
        echo "Skipping default" | tee -a /opt/gridpane/gpclone.log
        continue
        ;;
      *."${managed_domain}")
        {
          if [[ $entry == *"stat"* ]]; then
            echo "Skipping Administrative $entry (Monit Stats) Site..."
          else
            echo "Skipping Administrative $entry (PHPMA) Site..."
          fi
        } | tee -a /opt/gridpane/gpclone.log
        continue
        ;;
      *-remote)
        echo "Skipping alternative $entry remote restore Site..." | tee -a /opt/gridpane/gpclone.log
        continue
        ;;
      html)
        echo "Skipping html..." | tee -a /opt/gridpane/gpclone.log
        continue
        ;;
      *canary*)
        echo "$entry is a canary site... skipping!" | tee -a /opt/gridpane/gpclone.log
        continue
        ;;
      *staging*)
        echo "$entry is a staging site... skipping!" | tee -a /opt/gridpane/gpclone.log
        continue
        ;;
      *)
        if [[ "${active_sites}" != *"${entry}"* ]]; then
          if [[ ${sitesDirectoryArray} == *"${entry}"* &&
                ${sitesConfigsArray} == *"${entry}"* ]]; then
            echo "Active site ${entry} missing from active sites env, healing..."  | tee -a /opt/gridpane/gpclone.log
            gridpane::conf_write ":${entry}:" active -active-sites.env
          else
            echo "Skipping ${entry} - not in active sites list and inactive... probably a remnant..."  | tee -a /opt/gridpane/gpclone.log
            continue
          fi
        fi

        local local_sys_user
        local_sys_user="$(gridpane::get::set::site::user "${entry}")"
        if [[ $local_sys_user == "gridpane" ||
              $local_sys_user == *"_"* ]]; then

          local notification_title
          local notification_body
          if [[ $1 != "migration" ]]; then
            notification_title="Failover Clone Fail!"
            if [[ $local_sys_user == "gridpane" ]]; then
              notification_body="Source:${entry}<br>Source Server:${serverIP}<br>${entry} belongs to the restricted gridpane user. Please transfer this site to a new secure user, on next sync it will be cloned."
            else
              notification_body="Source:${entry}<br>Source Server:${serverIP}<br>${entry} belongs to the $local_sys_user user, this legacy username includes underscores and will fail site build validation. Please transfer to a new user, on next sync it will be cloned."
            fi
          else
            notification_title="Cloning Fail!"
            if [[ $local_sys_user == "gridpane" ]]; then
              notification_body="Source:${entry}<br>Source Server:${serverIP}<br>${entry} belongs to the restricted gridpane user. Please transfer this site to a new secure user and re-attempt cloning of this site later."
            else
              notification_body="Source:${entry}<br>Source Server:${serverIP}<br>${entry} belongs to the $local_sys_user user, this legacy username includes underscores and will fail site build validation. Please transfer to a new user and try again."
            fi
          fi
          echo "${entry}" | tee -a /opt/gridpane/gpclone.log

          gridpane::notify::app \
            "${notification_title}" \
            "${notification_body}" \
            "popup_and_center" \
            "fa-exclamation" \
            "infinity" \
            "${entry}"
            continue
        fi

        check_for_multisite=$(gridpane::check::multisite "${entry}")
        account_type="$(gridpane::get_account_type)"
        if [[ ${check_for_multisite} == "true"* ]] &&
           [[ ${account_type} != *"Developer"* &&
              ${account_type} != *"Agency"* ]]; then
          echo "${entry} is a multisite installation, GridPane cloning on your plan does not currently support this, we recommend AIO Migration Multisite Extension currently" | tee -a /opt/gridpane/gpclone.log
          notification_body="Source:${entry}<br>Source Server:${serverIP}<br>${entry} is a multisite installation. Please upgrade to Developer or Agency for Multisite Cloning."
          gridpane::notify::app \
            "Cloning ${entry} Fail" \
            "${notification_body}" \
            "popup_and_center" \
            "fa-exclamation" \
            "long" \
            "${entry}"
          continue
        fi

        site_to_clone="$entry"

        if [[ $1 == "migration" ]]; then
          echo "Ensure site ${site_to_clone} doesn't exist on the destination server already... avoid destructively overwriting anything..." | tee -a /opt/gridpane/gpclone.log
          check_for_extant_site=$(ssh root@"${remote_IP}" "sudo /usr/local/bin/gp site ${site_to_clone} -pre-clone-check" 2>&1)
          echo "${check_for_extant_site}" | tee -a /opt/gridpane/gpclone.log
          if [[ ${check_for_extant_site} != *"roceed"* ]]; then
            continue
          fi
        fi

        echo "Now checking/cloning site: ${site_to_clone}" | tee -a /opt/gridpane/gpclone.log

        sync_done="not done"

        if [[ $1 != "migration" ]]; then
          if [[ -f /var/www/${site_to_clone}/logs/swapped-domain.failover ]]; then
            [[ -f /var/www/${site_to_clone}/logs/synced-to.log ]] && rm /var/www/"${site_to_clone}"/logs/synced-to.log
          fi

          if grep -Fxq "${remote_IP}" /var/www/"${site_to_clone}"/logs/synced-to.log; then
            echo "Doing export of ${site_to_clone}..." | tee -a /opt/gridpane/gpclone.log
            gpclone::export::db_only "${site_to_clone}"
            echo "Cloning ${site_to_clone} to remote..." | tee -a /opt/gridpane/gpclone.log
            gpclone::sync::to_remote "${site_to_clone}" "${remote_IP}"
            echo "Site was already in sync list, exported and updated on remote." | tee -a /opt/gridpane/gpclone.log
            sync_done="done"
          fi
        fi

        if [[ ${sync_done} != "done" ]]; then

          if [[ -f /var/www/${site_to_clone}/logs/swapped-domain.failover ]]; then
            echo "-------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log
            local failover_site_original_domain
            failover_site_original_domain=$(cat /var/www/"${site_to_clone}"/logs/swapped-domain.failover)
            rm /var/www/"${site_to_clone}"/logs/swapped-domain.failover

            notification_body="Failover Site ${site_to_clone} has had primary domain swapped! Cloning new domain site over. Site with previous domain ${failover_site_original_domain} will need manual deletion from ${remote_IP}."
            gridpane::notify::app \
              "Failover Clone Notification" \
              "${notification_body}" \
              "popup_and_center" \
              "fa-exclamation" \
              "infinity" \
              "${site_to_clone}"
            echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
          fi

          echo "-------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log

          notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${new_url}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>Building new ${destination} site directory and app entry."
          gridpane::notify::app \
            "Cloning/Migrate Notice" \
            "${notification_body}" \
            "popup_and_center" \
            "fa-clone" \
            "long" \
            "${site_to_clone}"
          echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

          echo "Making ${site_to_clone} on remote..." | tee -a /opt/gridpane/gpclone.log
          echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log

          if [[ $1 != "migration" ]]; then
            gpclone::add::remote_site "${site_to_clone}" "${remote_IP}" true
          else
            gpclone::add::remote_site "${site_to_clone}" "${remote_IP}"
          fi

          echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log
          echo "Doing export of ${site_to_clone}..." | tee -a /opt/gridpane/gpclone.log
          echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log

          gpclone::export::site "${site_to_clone}"

          echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log

          if [[ -f /tmp/${nonce}.failed.db.export ]]; then
            notification_body="Process Failed<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>Source ${site_to_clone} DB Failed to export..."
            notification_icon="fa-exclamation"
            notification_duration="infinity"
            rm /tmp/"${nonce}".failed.db.export
          else
            notification_body="Process Update<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>Source ${site_to_clone} export package created.."
            notification_icon="fa-clone"
            notification_duration="long"
          fi

          gridpane::notify::app \
            "Cloning Notice" \
            "${notification_body}" \
            "popup_and_center" \
            "${notification_icon}" \
            "${notification_duration}" \
            "${site_to_clone}"
          echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log

          if [[ $1 == "migration" ]]; then
            echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log
            echo "Cloning ${site_to_clone} to remote..." | tee -a /opt/gridpane/gpclone.log
            echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log

            gpclone::clone_to_remote "${site_to_clone}" site-clone "${check_for_multisite}"
            echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log
          else
            echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log
            echo "Cloning Failover clone ${site_to_clone} to remote..." | tee -a /opt/gridpane/gpclone.log
            echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log

            gpclone::clone_to_remote "${site_to_clone}" failover-clone "${check_for_multisite}"
            echo "--------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log
          fi
          echo "Site was not in sync list, added to remote server, exported and updated to remote." | tee -a /opt/gridpane/gpclone.log
        fi
        ;;
      esac
    done

    gridpane::notify::app \
      "Server Clone Process Complete" \
      "All Site Clone Processes have finished, please check your sites for any issues." \
      "popup_and_center" \
      "fa-clone" \
      "infinity" \
      "${entry}"

    echo "Sync complete, unpausing Worker Functions..." | tee -a /opt/gridpane/gpclone.log

    gridpane::conf_delete gpfailsync-server-worker-pause | tee -a /opt/gridpane/gpclone.log
    ssh root@"${remote_IP}" "sudo /usr/local/bin/gp conf delete gpfailsync-server-worker-pause"

    echo "gpfailsync-server-worker-pause deleted, server tasks can resume as normal..." | tee -a /opt/gridpane/gpclone.log
  fi
}

gpclone::failover_constant() {
  local set_or_unset
  set_or_unset="$1"
  local site
  site="$2"
  if [[ ${set_or_unset} == "set" ]]; then
    if ! grep "GRIDPANE_FAILOVER" /var/www/"${site}"/wp-config.php; then
      sed -i "/define('GRIDPANE', true);/a define('GRIDPANE_FAILOVER', true);" /var/www/"${site}"/wp-config.php
    fi
  elif [[ ${set_or_unset} == "unset" ]]; then
    if grep "GRIDPANE_FAILOVER" /var/www/"${site}"/wp-config.php; then
      sed -i "/GRIDPANE_FAILOVER/d" /var/www/"${site}"/wp-config.php
    fi
  fi
}
