#!/bin/bash
#
#

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin

#######################################
# Print currently running function name
#
#  Outputs:
#   Writes parent function name to STDOUT
#######################################
gridpane::setglobals() {
  . /usr/local/bin/lib/webserver.sh

  readonly last_arg="${@: -1}"
  readonly second_last_arg="${@:(-2):1}"
  readonly nonce=$(pwgen 16 1)
  readonly serverIP=$(gridpane::get::serverip)
  readonly gridpanetoken=$(awk '{print $1; exit}' /root/gridpane.token)
  readonly GPURL=$(awk '{print $1; exit}' /root/grid.source)
  readonly prometheus="42"
  readonly script_datetime=$(date)
  readonly file_datetime=$(echo ${script_datetime} | tr ' ' '-')
  readonly host=$(hostname)
  readonly wpadmin=$(awk '{print $1; exit}' /root/gridcreds/wp.user)
  readonly wpemail=$(gridpane::get::email)
  local check_server_build
  check_server_build=$(grep -w "server-build:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  if [[ -z ${check_server_build} &&
      "$(lsb_release -d)" == *"18.04"* ]]; then
    gridpane::conf_write server-build prometheus
    check_server_build="prometheus"
  fi
  readonly server_build="${check_server_build}"
  local check_mysql_version
  check_mysql_version=$(grep -w "mysql-version:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  if [[ -z ${check_mysql_version} ]]; then
    if [[ "$(mysql -V)" == *"Percona"* ]]; then
      check_mysql_version="percona"
    else
      check_mysql_version="mariadb"
    fi
    gridpane::conf_write "mysql-version" "${check_mysql_version}"
  fi
  readonly mysql_version="${check_mysql_version}"
  readonly subdom="$(cat /root/grid.subdom)"
  readonly subdomstat="$(cat /root/grid.subdom | sed "s|.gridpane|stat.gridpane|g")"
}

#######################################
# Outputs the Function run to log with PID and logs
#
#  Outputs:
#   Writes function, PID and args to STDOUT
#   Writes function, PID and args to /var/log/gridpane.log
#######################################
gridpane::logfunction() {
  if [[ ${last_arg} != "-q" ]] && [[ ${last_arg} != "-quiet" ]]; then
    echo "#####################################################################################################"
    echo "The following command was run at ${script_datetime} with PID: $$... $0" "$@"
    echo "#####################################################################################################"
  fi
}

#######################################
# Print currently running function name
#
#  Outputs:
#   Writes parent function name to STDOUT
#######################################
gridpane::displayfunc() {
  echo "Running ${FUNCNAME[1]}..."
}

#######################################
# Find source of SSH request.
#
# Globals:
#   GPSourceIP
#   GPURL
#  Outputs:
#   Writes status to STDOUT
#######################################
gridpane::findsource() {

  GPSourceIP="$(echo $SSH_CLIENT | awk '{ print $1}')"

  # TODO(leo): create list/array of IPs and enumerate (eventually make external)
  if [[ $GPSourceIP == "45.76.231.120" ]]; then
    output="Remote GridPane Server is GridPane Chicago..."
    readonly GPURL=dev.gridpane.com
  elif [[ $GPSourceIP == "35.199.11.215" ]]; then
    output="Remote GridPane Server is GridPane East..."
    readonly GPURL=my.gridpane.com
  elif [[ $GPSourceIP == "130.211.115.235" ]]; then
    output="Remote GridPane Server is GridPane Central..."
    readonly GPURL=my.gridpane.com
  elif [[ $GPSourceIP == "35.202.40.44" ]]; then
    output="Remote GridPane Server is GridPane HC..."
    readonly GPURL=hc.gridpane.com
  elif [[ $GPSourceIP == "144.202.57.136" ]]; then
    output="Remote GridPane Server is GridPane Dev Beta..."
    readonly GPURL=dev-beta.gridpane.com
  elif [[ $GPSourceIP == "178.128.188.34" ]]; then
    output="Remote GridPane Server is GridPane BlackMirror..."
    readonly GPURL=pvg.gridpane.com
  else
    if [[ -z ${prometheus} ]]; then
      output="It looks like these commands are being run manually..."
    fi
  fi

  if [[ -z $GPURL ]]; then
    readonly GPURL=$(awk '{print $1; exit}' /root/grid.source)
    if [[ -z ${prometheus} ]]; then
      extra_output="Command was called locally, we need to pull the GPURL from file..."
    fi
  fi

  if [[ -z $GPURL ]]; then
    output="This server appears to be orphaned, we're going to assume a default build source..."
    readonly GPURL=my.gridpane.com
  fi

  if [[ ${last_arg} != "-q" ]] && [[ ${last_arg} != "-quiet" ]]; then
    echo "${output}"
    if [[ ! -z ${extra_output} ]]; then
      echo "${extra_output}"
    fi
  fi

}

#######################################
# Some verbose description here...
#
# Outputs:
#  Writes status to STDOUT
#######################################
gridpane::conf_read() {
  local do_nothing
  local configuration_file
  local verbose

  if [[ -z $1 ]]; then
    echo "Nothing passed... what is going on???"
    exit 187
  fi

  if [[ $1 == "-v" || $1 == "-verbose" ]]; then
    echo "You can't use the verbose option -v / -verbose here... doing nothing"
    do_nothing="not a thing"
  elif [[ $2 == "-v" || $2 == "-verbose" || $2 == "-q" || $2 == "-quiet" ]] ||
    [[ -z $2 ]]; then
    configuration_file="/root/gridenv/promethean.env"
    [[ $2 == "-v" || $2 == "-verbose" ]] &&
      verbose="true"
  else
    case $2 in
    -site.env|site.env)
      if [[ -z $3 ]]; then
        do_nothing="No site provided... do nothing"
      else
        [[ ! -d /var/www/$3 ]] &&
          do_nothing="Site $3 doesn't exist... do nothing"
      fi
      configuration_file="/var/www/$3/logs/$3.env"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -php.env|php.env)
      if [[ -z $3 ]]; then
        do_nothing="No PHP Version Provided... do nothing"
      else
        if ! [[ $3 =~ 7\.[1-4] ]]; then
          do_nothing="GridPane only supports PHP 7.1/2/3/4, do nothing.."
        else
          [[ ! -d /etc/php/$3 ]] &&
            do_nothing="PHP version $3 hasn't been installed... do nothing"
        fi
      fi
      configuration_file="/root/gridenv/php$3.env"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -php.env.bak|php.env.bak)
      if [[ -z $3 ]]; then
        do_nothing="No PHP Version Provided... do nothing"
      else
        if ! [[ $3 =~ 7\.[1-4] ]]; then
          do_nothing="GridPane only supports PHP 7.1/2/3/4, do nothing.."
        else
          [[ ! -d /etc/php/$3 ]] &&
            do_nothing="PHP version $3 hasn't been installed... do nothing"
        fi
      fi
      configuration_file="/root/gridenv/php$3.env.bak"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -nginx.env|nginx.env)
      configuration_file="/root/gridenv/nginx.env"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -nginx.env.bak|nginx.env.bak)
      configuration_file="/root/gridenv/nginx.env.bak"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -sys-upgrades.env|sys-upgrades.env)
      configuration_file="/root/gridenv/sys-upgrades.env"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -sys-upgrades.env.bak|sys-upgrades.env.bak)
      configuration_file="/root/gridenv/sys-upgrades.env.bak"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -redis.env|redis.env)
      configuration_file="/root/gridenv/redis.env"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -redis.env.bak|redis.env.bak)
      configuration_file="/root/gridenv/redis.env.bak"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -mysql.env|mysql.env)
      configuration_file="/root/gridenv/mysql.env"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -mysql.env.bak|mysql.env.bak)
      configuration_file="/root/gridenv/mysql.env.bak"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -monit.env|monit.env)
      configuration_file="/root/gridenv/monit.env"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -monit.env.bak|monit.env.bak)
      configuration_file="/root/gridenv/monit.env.bak"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    *) do_nothing="Unrecognized GridPane Configuration Env file... do nothing" ;;
    esac
  fi
  if [[ -z ${do_nothing} ]]; then
    if [[ $1 != "all" ]] && [[ $1 != "-all" ]]; then
      val=$(grep -w "^${1}:.*" ${configuration_file} | cut -f 2 -d ':')
      if [[ -z ${verbose} ]]; then
        echo "${val}"
      else
        echo "$1:${val}"
        echo "${configuration_file}"
      fi
    else
      echo "GridPane Env"
      echo "------------------------"
      cat ${configuration_file}
      echo "${configuration_file}"
    fi
  else
    echo "${do_nothing}"
  fi
}

#######################################
# Some verbose description here...
#
# Outputs:
#  Writes status to STDOUT
#######################################
gridpane::conf_write() {
  local do_nothing
  local configuration_file
  local verbose

  if [[ -z $1 ]]; then
    echo "Nothing passed... this could be dangerous... exiting..."
    exit 187
  fi

  if [[ $1 == "-v" || $1 == "-verbose" ]] ||
    [[ $2 == "-v" || $2 == "-verbose" ]]; then
    do_nothing="You can't use the verbose option -v / -verbose here... gonna do nothing"
  elif [[ $3 == "-v" || $3 == "-verbose" ]] ||
    [[ -z $3 ]]; then
    configuration_file="/root/gridenv/promethean.env"
    [[ $3 == "-v" || $3 == "-verbose" ]] &&
      verbose="true"
  else
    case $3 in
    -site.env|site.env)
      if [[ -z $4 ]]; then
        do_nothing="No site provided... do nothing"
      else
        [[ ! -d /var/www/$4 ]] &&
          do_nothing="Site $4 doesn't exist... do nothing"
      fi
      configuration_file="/var/www/$4/logs/$4.env"
      [[ $5 == "-v" || $5 == "-verbose" ]] &&
        verbose="true"
      ;;
    -php.env|php.env)
      if [[ -z $4 ]]; then
        do_nothing="No PHP Version Provided... do nothing"
      else
        if ! [[ $4 =~ 7\.[1-4] ]]; then
          do_nothing="GridPane only supports PHP 7.1/2/3/4, do nothing.."
        else
          [[ ! -d /etc/php/$4 ]] &&
            do_nothing="PHP version $4 hasn't been installed... do nothing"
        fi
      fi
      configuration_file="/root/gridenv/php$4.env"
      [[ $5 == "-v" || $5 == "-verbose" ]] &&
        verbose="true"
      ;;
    -php.env.bak|php.env.bak)
      if [[ -z $4 ]]; then
        do_nothing="No PHP Version Provided... do nothing"
      else
        if ! [[ $4 =~ 7\.[1-4] ]]; then
          do_nothing="GridPane only supports PHP 7.1/2/3/4, do nothing.."
        else
          [[ ! -d /etc/php/$4 ]] &&
            do_nothing="PHP version $4 hasn't been installed... do nothing"
        fi
      fi
      configuration_file="/root/gridenv/php$4.env.bak"
      [[ $5 == "-v" || $5 == "-verbose" ]] &&
        verbose="true"
      ;;
    -nginx.env|nginx.env)
      configuration_file="/root/gridenv/nginx.env"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -nginx.env.bak|nginx.env.bak)
      configuration_file="/root/gridenv/nginx.env.bak"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -sys-upgrades.env|sys-upgrades.env)
      configuration_file="/root/gridenv/sys-upgrades.env"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -sys-upgrades.env.bak|sys-upgrades.env.bak)
      configuration_file="/root/gridenv/sys-upgrades.env.bak"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -redis.env|redis.env)
      configuration_file="/root/gridenv/redis.env"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -redis.env.bak|redis.env.bak)
      configuration_file="/root/gridenv/redis.env.bak"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -mysql.env|mysql.env)
      configuration_file="/root/gridenv/mysql.env"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -mysql.env.bak|mysql.env.bak)
      configuration_file="/root/gridenv/mysql.env.bak"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -monit.env|monit.env)
      configuration_file="/root/gridenv/monit.env"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -monit.env.bak|monit.env.bak)
      configuration_file="/root/gridenv/monit.env.bak"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -active-sites.env|active-sites.env)
      configuration_file="/root/gridenv/active-sites.env"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    *) do_nothing="Unrecognized GridPane Configuration Env file... do nothing" ;;
    esac
  fi
  if [[ -z ${do_nothing} ]]; then
    [[ ! -e ${configuration_file} ]] &&
      sudo touch ${configuration_file}
    chattr -i "${configuration_file}"
    if [[ $3 == "-active-sites-env" || $3 == "active-sites.env" ]]; then
      sed -i "/${1}/d" "${configuration_file}"
      sh -c "echo -n '$1\n' >> ${configuration_file}"
    else
      sed -i "/${1}:/d" ${configuration_file}
      sh -c "echo -n '$1:$2\n' >> ${configuration_file}"
    fi
    chattr +i ${configuration_file}
    if [[ ${verbose} == "true" ]]; then
      echo "Updated GridPane Env"
      if [[ $3 == "-active-sites-env" || $3 == "active-sites.env" ]]; then
        echo "$1"
      else
        echo "$1:$2"
      fi
      echo "${configuration_file}"
    fi
  else
    echo "${do_nothing}"
  fi
}

#######################################
# Some verbose description here...
#
# Outputs:
#  Writes status to STDOUT
#######################################
gridpane::conf_delete() {
  local do_nothing
  local configuration_file
  local verbose

  if [[ -z $1 ]]; then
    echo "Nothing passed... this could be dangerous... exiting..."
    exit 187
  fi

  if [[ $1 == "-v" || $1 == "-verbose" ]]; then
    do_nothing="You can't use the verbose option -v / -verbose here... doing nothing"
  elif [[ $2 == "-v" || $2 == "-verbose" ]] ||
    [[ -z $2 ]]; then
    configuration_file="/root/gridenv/promethean.env"
    [[ $2 == "-v" || $2 == "-verbose" ]] &&
      verbose="true"
  else
    case $2 in
    -site.env|site.env)
      if [[ -z $3 ]]; then
        do_nothing="No site provided... do nothing"
      else
        [[ ! -d /var/www/$3 ]] &&
          do_nothing="Site $3 doesn't exist... do nothing"
      fi
      configuration_file="/var/www/$3/logs/$3.env"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -php.env|php.env)
      if [[ -z $3 ]]; then
        do_nothing="No PHP Version Provided... do nothing"
      else
        if ! [[ $3 =~ 7\.[1-4] ]]; then
          do_nothing="GridPane only supports PHP 7.1/2/3/4, do nothing.."
        else
          [[ ! -d /etc/php/$3 ]] &&
            do_nothing="PHP version $3 hasn't been installed... do nothing"
        fi
      fi
      configuration_file="/root/gridenv/php$3.env"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -php.env.bak|php.env.bak)
      if [[ -z $3 ]]; then
        do_nothing="No PHP Version Provided... do nothing"
      else
        if ! [[ $3 =~ 7\.[1-4] ]]; then
          do_nothing="GridPane only supports PHP 7.1/2/3/4, do nothing.."
        else
          [[ ! -d /etc/php/$3 ]] &&
            do_nothing="PHP version $3 hasn't been installed... do nothing"
        fi
      fi
      configuration_file="/root/gridenv/php$3.env.bak"
      [[ $4 == "-v" || $4 == "-verbose" ]] &&
        verbose="true"
      ;;
    -nginx.env|nginx.env)
      configuration_file="/root/gridenv/nginx.env"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -nginx.env.bak|nginx.env.bak)
      configuration_file="/root/gridenv/nginx.env.bak"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -sys-upgrades.env|sys-upgrades.env)
      configuration_file="/root/gridenv/sys-upgrades.env"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -sys-upgrades.env.bak|sys-upgrades.env.bak)
      configuration_file="/root/gridenv/sys-upgrades.env.bak"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -redis.env|redis.env)
      configuration_file="/root/gridenv/redis.env"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -redis.env.bak|redis.env.bak)
      configuration_file="/root/gridenv/redis.env.bak"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -mysql.env|mysql.env)
      configuration_file="/root/gridenv/mysql.env"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -mysql.env.bak|mysql.env.bak)
      configuration_file="/root/gridenv/mysql.env.bak"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -monit.env|monit.env)
      configuration_file="/root/gridenv/monit.env"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -monit.env.bak|monit.env.bak)
      configuration_file="/root/gridenv/monit.env.bak"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    -active-sites.env|active-sites.env)
      configuration_file="/root/gridenv/active-sites.env"
      [[ $3 == "-v" || $3 == "-verbose" ]] &&
        verbose="true"
      ;;
    *) do_nothing="Unrecognized GridPane Configuration Env file... do nothing" ;;
    esac
  fi
  if [[ -z ${do_nothing} ]]; then
    chattr -i "${configuration_file}"
    sed -i "/^${1}/d" "${configuration_file}"
    chattr +i "${configuration_file}"
    if [[ ${verbose} == "true" ]]; then
      echo "Updated GridPane Env"
      echo "--------------------------"
      echo "$1 deleted"
      echo "${configuration_file}"
    fi
  else
    echo "${do_nothing}"
  fi
}

gridpane::get_account_type() {
  local accounttype
  accounttype=$(/usr/bin/curl -s -X POST https://$GPURL/api/check-account-type?api_token=${gridpanetoken} 2>&1)
  accounttype=$(echo "${accounttype}" | awk -F'"name":"|","' '{print $2}')
  accounttype="${accounttype//name/}" # Drop name
  accounttype="${accounttype//:/}"   # Drop extra chars
  accounttype="${accounttype//\"/}"
  echo "${accounttype}"
}

#######################################
# Some verbose description here...
#
# Outputs:
#  Writes status to STDOUT
#######################################
gridpane::validate() {

  local accounttype
  local verify_account
  if [[ $1 != "verify" && ! -f /opt/gridpane/verify.account ]]; then
    accounttype=$(grep -w "gridpane-account:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  else
    verify_account="verify"
    [[ -f /opt/gridpane/verify.account ]] &&
      rm /opt/gridpane/verify.account
  fi

  if [[ -z "${accounttype}" ]] || [[ "${accounttype}" == "unverified" ]]; then
    if [[ ${verify_account} != "verify" ]]; then
      [[ ${last_arg} != "-q" && ${last_arg} != "-quiet" ]] &&
        echo "Initial GridPane validation failed.... calling home to double check..."
    else
      echo "verifying account..."
    fi
    accounttype="$(gridpane::get_account_type)"
    if [[ -n "${accounttype}" ]]; then
      gridpane::conf_write gridpane-account "${accounttype}"
    else
      gridpane::conf_write gridpane-account unverified
    fi
  fi
  case ${accounttype} in
  "unverified") output="Mothership confirms an invalid account... please speak to support..." ;;
  *"Free"*)
    output="GridPane Free Forever"
    account="free"
    ;;
  *"Developer"*)
    output="GridPane Developer"
    account="paid"
    ;;
  *"Agency"*)
    output="GridPane Agency - Version 1.2.0 ... "
    account="paid"
    ;;
  *"Pro"*)
    output="GridPane Pro"
    account="paid"
    ;;
  *"Solo"*)
    output="GridPane Solo"
    account="paid"
    ;;
  *) output="Your GridPane account is invalid, your token has been changed or lost, or this function is changing your account type..." ;;
  esac
  if [[ ${last_arg} != "-q" ]] && [[ ${last_arg} != "-quiet" ]]; then
    echo "${output}"
    if [[ "${account}" == "paid" ]] || [[ "${account}" == "free" ]]; then
      script="${1}"
      script="${script%%/usr/local/bin/}"
      if [[ $1 != "verify" ]]; then
        echo "Validated... running ${1} script..."
      else
        echo "${account} Account Validated..."
      fi
    else
      echo "This feature is only available to valid GridPane plans, please visit GridPane.com to signup!"
    fi
  fi
}

#######################################
# Check for apt-lock before install, wait upto 90 seconds before attempting to break
#
# Outputs:
#  Writes status to STDOUT
#######################################
gridpane::preinstallaptcheck() {
  RETRY=0
  updateprogress=$(sudo apt-get update)

  while [[ ${updateprogress} == *"Unable to lock director"* ]] ||
    [[ ${updateprogress} == *"Could not get lock"* ]] ||
    [[ ${updateprogress} == *"Resource temporarily unavailable"* ]]; do

    echo "Waiting 30 seconds for lock file... Retry ${RETRY}"
    sleep 1

    # Timeout and fail xxit after 5 mins
    if [[ ${RETRY} -gt 90 ]]; then
      gridpane::breakaptlock
      break
    fi

    updateprogress=$(sudo apt-get update)
    RETRY=$(($RETRY + 1))
  done

  echo "apt-get update succeeded, check completed, proceeding... "

}

#######################################
# Find processes hogging the apt-lock, kill them and free the lock
#
# Outputs:
#  Writes status to STDOUT
#######################################
gridpane::breakaptlock() {

  local dpkg_lock
  local list_lock
  local archive_lock

  if [[ -f /var/lib/dpkg/lock ]]; then
    dpkg_lock=$(lsof /var/lib/dpkg/lock | awk '{print $2}')
  fi

  if [[ ! -z ${dpkg_lock} ]]; then
    echo "Apt-lock found: /var/lib/dpkg/lock"
    for record in ${dpkg_lock}; do
      if [[ "${record}" != "PID" ]]; then
        kill -9 ${record}
      fi
    done
    #remove lock
    rm /var/lib/dpkg/lock
  fi

  if [[ -f /var/lib/apt/lists/lock ]]; then
    list_lock=$(lsof /var/lib/apt/lists/lock | awk '{print $2}')
  fi

  if [[ ! -z ${list_lock} ]]; then
    echo "Apt-lock found: /var/lib/apt/lists/lock"
    for record in ${list_lock}; do
      if [[ "${record}" != "PID" ]]; then
        kill -9 ${record}
      fi
    done
    #remove lock
    rm /var/lib/apt/lists/lock
  fi

  if [[ -f /var/cache/apt/archives/lock ]]; then
    archive_lock=$(lsof /var/cache/apt/archives/lock | awk '{print $2}')
  fi

  if [[ ! -z ${archive_lock} ]]; then
    echo "Apt-lock found: /var/cache/apt/archives/lock"
    for record in ${archive_lock}; do
      if [[ "${record}" != "PID" ]]; then
        kill -9 ${record}
      fi
    done
    #remove lock
    rm /var/cache/apt/archives/lock
  fi
  # reconfigure the packages
  dpkg --configure -a
}

#######################################
# Send Notifications directly to the
# dashboard via notifications API callback
#
# Accepted Args:
#  $1 title: string
#  $2 body: string (accepts html tags)
#  $3 channel: popup_only|center_only|popup_and_center
#  $4 icon: FontAwesome4 slug
#  $5 duration: short|default|long|infinity
#  $6 site_url: site primary domain (optional)
# Outputs:
#  api response on failure
#######################################
gridpane::notify::app() {
  local server_ip
  if [[ -z $7 ]]; then
    server_ip="${serverIP}"
  else
    server_ip="$7"
  fi
  local date
  date="$(date)"
  /usr/bin/curl \
    -d '{ "title":"'"${1}"'", "body":"'"${2}"'", "channel":"'"${3}"'", "icon":"'"${4}"'", "duration":"'"${5}"'", "site_url":"'"${6}"'", "server_ip":"'"${server_ip}"'", "date":"'"${date}"'" }' \
    -H "Content-Type: application/json" \
    -X POST "https://${GPURL}/api/notification?api_token=${gridpanetoken}"
}

#######################################
# Generates JSON format payload
# and callbacks back to the app
#
# Accepted Args:
#  $1 API endpoint (string)
#   - /site/site-update
#   - /notification
#   - /wsod
#   etc
#  $n jo formatted
#   - key=value (string)
#   -  key@value (boolean)
# Outputs:
#  api response
#######################################
gridpane::callback::app() {

  if ! type jo >/dev/null; then
    gridpane::preinstallaptcheck
    apt-get -y install jo
  fi

  local cli_callback
  if [[ $1 == "callback" || $1 == "-callback" ]]; then
    cli_callback="true"
    shift
  fi

  local api_endpoint="$1"
  shift

  local payload=()

  for i in "$@"; do
    payload+=("${i}")
  done

  payload=$(/usr/bin/jo "${payload[@]}")

  if [[ ! -z ${cli_callback} ]]; then
    echo "Endpoint:"
    echo "https://${GPURL}/api${api_endpoint}"
    echo "Payload:"
    echo "${payload}"
  fi

  /usr/bin/curl \
    -d "${payload}" \
    -H "Content-Type: application/json" \
    -X POST https://${GPURL}/api${api_endpoint}?api_token=${gridpanetoken}

}

#######################################
# Callback to app site build status
#
#######################################
gridpane::callback::sitebuild() {
  /usr/bin/curl \
    -d '{ "site_url":"'"${1}"'", "status":"'"${2}"'", "server_ip":"'"${serverIP}"'" }' \
    -H "Content-Type: application/json" \
    -X POST https://${GPURL}/api/sites/build-status?api_token=${gridpanetoken}
}

#######################################
# Send Notification to the slack
# For not uses stupid endpoint
#
# Accepted Args:
#  type: warning|error|info
#  details: no html - but returns okay
# Outputs:
#  Callback to /api/wsod/completed
#######################################
gridpane::notify::slack() {
  local preemptive_support="$4"
  [[ -z $preemptive_support ]] &&
    preemptive_support="false"
  /usr/bin/curl \
    -d '{"server":"'"${serverIP}"'", "type":"'"${1}"'", "resolution":"'"${2}"'", "details":"'"${3}"'", "preemptive_support":'${preemptive_support}'}' \
    -H "Content-Type: application/json" \
    -X POST https://${GPURL}/api/wsod/completed?api_token=${gridpanetoken}
}

gridpane::check:bashcompletion() {
  if [[ ! -f /usr/local/bin/wp-completion.bash ]]; then
    wget https://raw.githubusercontent.com/wp-cli/wp-cli/master/utils/wp-completion.bash
    mv wp-completion.bash /usr/local/bin/wp-completion.bash
  fi
}

gridpane::get::email() {

  local user_email=$(grep -w "^mail:.*" /root/gridenv/promethean.env | cut -f 2 -d ':')
  local creds_email=$(cat /root/gridcreds/wp.email)

  if [[ -z ${user_email} ]]; then

    creds_email=$(cat /root/gridcreds/wp.email)

    if [[ ! -z ${creds_email} ]] && [[ ${creds_email} == *"@"* ]] && [[ ${creds_email} == *"."* ]]; then

      user_email="${creds_email}"

    else

      user_email=$(grep -w "^mail:.*" /opt/gridpane/env-baks/promethean.env.og.bak | cut -f 2 -d ':')

    fi

    store_email=$(gridpane::conf_write mail ${user_email})

  else

    if [[ ${user_email} != "${creds_email}" ]] && [[ ! -z ${creds_email} ]] &&
      [[ ${creds_email} == *"."* ]] && [[ ${creds_email} == *"@"* ]]; then

      user_email="${creds_email}"

      store_email=$(gridpane::conf_write mail ${user_email})

    fi

  fi

  echo "${user_email}"

}

gridpane::get::serverip() {
  local server_IP
  server_IP=$(cat /root/server.ip)
  local incorrect_initial_server_ip
  if [[ ! $server_IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    incorrect_initial_server_ip=1
    server_IP=$(/usr/bin/curl --max-time 1 -4 http://ifconfig.me 2>/dev/null | tr -d '\n')
    if [[ ! $server_IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
      server_IP=$(/usr/bin/curl --max-time 5 -4 http://checkip.amazonaws.com 2>/dev/null | tr -d '\n')
      if [[ ! $server_IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
        server_IP=$(/usr/bin/curl --max-time 10 -4 http://ip4.ident.me 2>/dev/null | tr -d '\n')
      fi
    fi
  fi

  if [[ $server_IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    [[ -n $incorrect_initial_server_ip ]] &&
      echo "${server_IP}" >/root/server.ip
    echo "${server_IP}"
  else
    echo "ServerIP-Error:$server_IP"
  fi
}

gridpane::check::argnum() {

  NUMARGS=$#
  if [[ $NUMARGS -eq 0 ]]; then

    echo "No arguments passed, like... none.... failing!"
    exit 1

  fi

}

gridpane::check::activesite() {

  local site_to_check_for="$1"

  local active_sites=$(cat /root/gridenv/active-sites.env)

  if [[ ${active_sites} != *"${site_to_check_for}"* ]]; then

    # need to swap this out all over...
    sitesDirectoryArray=$(find /var/www -maxdepth 1 -mindepth 1 -type d)

    if [[ "$webserver" = nginx ]]; then
      sitesConfigsArray=$(dir /etc/nginx/sites-enabled)
    elif [[ "$webserver" = openlitespeed ]]; then
      sitesConfigsArray=$(dir /usr/local/lsws/conf/vhosts)
    fi

    if [[ ${sitesDirectoryArray} == *"${site_to_check_for}"* ]] && [[ ${sitesConfigsArray} == *"${site_to_check_for}"* ]]; then

      echo "Active site ${site_to_check_for} missing from active sites env, healing..."

      gridpane::conf_write ":${site_to_check_for}:" active -active-sites.env

    else

      echo "Site passed doesn't exist and we aren't creating it... exiting..."

      exit 187

    fi

  fi

}

gridpane::get::site::php() {
  local currentphpver
  if [[ "$webserver" = nginx ]]; then
    local phpsockfile
    phpsockfile=$(find /var/run/php/ -maxdepth 1 -name "*-fpm-$1.sock" -print)
    RETRY=0
    #Add guard here
    if [[ ! -f "/var/www/$1/wp-config.php" ]]; then
      echo "This is not a wordpress site. Function get php version"
      RETRY=6
    fi
    while [[ -z ${phpsockfile} ]]; do
      if [[ ${RETRY} -gt 5 ]]; then
        break
      fi
      sleep 1
      local phpsockfile
      phpsockfile=$(find /var/run/php/ -maxdepth 1 -name "*-fpm-$1.sock" -print)
      RETRY=$((RETRY + 1))
    done

    phpsockfile=${phpsockfile#/var/run/php/}
    phpsockfile=${phpsockfile%-fpm-$1.sock}
    currentphpver=${phpsockfile#php}
    currentphpver="${currentphpver:0:1}.${currentphpver:1:1}"

    if [[ ${currentphpver} != "7.2" &&
      ${currentphpver} != "7.3" &&
      ${currentphpver} != "7.1" &&
      ${currentphpver} != "7.4" ]]; then

      worker_restart_output="Missing $1 PHP unix sock requires php7.2-fpm restart"

      if [[ -f /etc/php/7.2/fpm/pool.d/$1.conf ]]; then
        currentphpver="7.2"
        require_worker_72_restart="true"
      elif [[ -f /etc/php/7.3/fpm/pool.d/$1.conf ]]; then
        currentphpver="7.3"
        require_worker_73_restart="true"
      elif [[ -f /etc/php/7.1/fpm/pool.d/$1.conf ]]; then
        currentphpver="7.1"
        require_worker_71_restart="true"
      elif [[ -f /etc/php/7.4/fpm/pool.d/$1.conf ]]; then
        currentphpver="7.4"
        require_worker_74_restart="true"
      fi
    fi
  else
    currentphpver="$(/usr/local/bin/gpols get "$1" php-ver)"
  fi

  local current_env_says
  current_env_says=$(gridpane::conf_read php-ver -site.env "$1")
  if [[ ${current_env_says} != *"${currentphpver}"* &&
    -n ${current_env_says} ]]; then
    write_back_to_env=$(gridpane::conf_write php-ver "${currentphpver}" -site.env "$1")
  fi

  echo "${currentphpver}"
}

gridpane::get::set::site::user() {
  local users_site="$1"
  local just_get="$2"

  if [[ "$webserver" = nginx ]]; then
    local php_ver
    php_ver=$(gridpane::get::site::php "${users_site}") || true
    local app_php_user
    app_php_user=$(grep -m 1 "user =" "/etc/php/${php_ver}/fpm/pool.d/${users_site}.conf" | cut -d " " -f 3)
  else
    local app_php_user
    # TODO (Jeff) fallback incase of corrupted local env
    # Cim: Not sure what happened here but the value of app_php_user is nil
    # app_php_user=$(grep -w "sys-user:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
    # gpols get command below already has a fallback
    app_php_user="$(/usr/local/bin/gpols get "$users_site" user)"
  fi

  echo "${app_php_user}"

  if [[ $just_get != "-just-get" ]]; then
    local multitenancy_status
    multitenancy_status=$(grep -w "multitenancy:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
    local current_env_says
    current_env_says=$(grep -w "sys-user:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
    if [[ ${current_env_says} != *"${app_php_user}"* ]] &&
      [[ -n ${current_env_says} ]]; then
      write_to_env=$(gridpane::conf_write sys-user "${app_php_user}" -site.env "${users_site}")
    fi

    local existing_server_system_users
    existing_server_system_users=$(cut -d: -f1 /etc/passwd)
    if [[ ${existing_server_system_users} == *"${app_php_user}"* ]] &&
      [[ -n ${current_env_says} && -z ${multitenancy_status} ]]; then
      local set_site_owner
      set_site_owner=$(chown -R "${app_php_user}":"${app_php_user}" "/var/www/${users_site}/htdocs")
      if [[ -d /var/www/${users_site}/modsec/audit-logs/audit ]]; then
        local quiet_chown
        if [[ "$webserver" = nginx ]]; then
          quiet_chown=$(/bin/chown -R www-data:www-data "/var/www/${users_site}/modsec/audit-logs/audit")
        else
          quiet_chown=$(/bin/chown -R nobody:nogroup /var/www/"$users_site"/modsec/audit-logs/audit)
        fi
      fi
    fi
  fi
}

gridpane::get::site::sockfile() {
  local site_to_get_sockfile_for="$1"
  local unescaped_or_not="$2"
  local php_ver
  local sockfile
  if [[ "$webserver" = nginx ]]; then
    php_ver=$(gridpane::get::site::php "${site_to_get_sockfile_for}")
    php_ver=${php_ver//./}
    if [[ -S /var/run/php/php${php_ver}-fpm-${site_to_get_sockfile_for}.sock ]]; then
      sockfile="unix:/var/run/php/php${php_ver}-fpm-${site_to_get_sockfile_for}.sock"
    else
      if [[ -S /var/run/php/php72-fpm-${site_to_get_sockfile_for}.sock ]]; then
        sockfile="unix:/var/run/php/php72-fpm-${site_to_get_sockfile_for}.sock"
      else
        if [[ -S /var/run/php/php71-fpm-${site_to_get_sockfile_for}.sock ]]; then
          sockfile="unix:/var/run/php/php71-fpm-${site_to_get_sockfile_for}.sock"
        else
          if [[ -S /var/run/php/php73-fpm-${site_to_get_sockfile_for}.sock ]]; then
            sockfile="unix:/var/run/php/php73-fpm-${site_to_get_sockfile_for}.sock"
          fi
        fi
      fi
    fi
  else
    local lsphp
    lsphp="$(/usr/local/bin/gpols get "$site_to_get_sockfile_for" lsphp)"
    [[ -S /tmp/lshttpd/"$site_to_get_sockfile_for"-"$lsphp".sock  ]] &&
      sockfile=/tmp/lshttpd/"$site_to_get_sockfile_for"-"$lsphp".sock
  fi

  if [[ $unescaped_or_not != *"unescaped" ]]; then
    # shellcheck disable=SC2001
    sockfile=$(echo "${sockfile}" | sed 's;/;\\/;g')
  fi

  echo "${sockfile}"
}

gridpane::check::site::nginx::config() {
  local site_to_check="$1"

  if [[ ! -f /etc/nginx/sites-available/${site_to_check} ]]; then
    conf_type="http"
  else
    conf_type=$(sed -n '/##GridPane_Prometheus_vHost_Begin##/{n;p}' "/etc/nginx/sites-available/${site_to_check}")
    conf_type=${conf_type#"#"}
    conf_type=${conf_type%"#"}
    conf_type=$(echo "${conf_type}" | tr '[:upper:]' '[:lower:]')
  fi

  shift

  local scheme
  local proxied
  local wildcard

  while true; do
    case $1 in
    *return-http*)
      scheme="http"
      [[ $1 == *"https"* ]] &&
        scheme="https"
      ;;
    *proxy*)
      proxied="false"
      [[ $1 == *"return-proxy"* ]] &&
        proxied="true" ;;
    *wildcard*)
      wildcard="false"
      [[ $1 == *"return-wildcard"* ]] &&
        wildcard="true"
      ;;
    esac
    shift 1 || true
    [[ -z "$1" ]] && break
  done

  if [[ -n $scheme ]]; then
    conf_type=$(echo "${conf_type}" | tr -d 's')
    [[ $scheme == "https" ]] &&
      conf_type=${conf_type//http/https}
  fi

  if [[ -n $proxied ]]; then
    conf_type=${conf_type//proxy-/}
    [[ $proxied == "true" ]] &&
      conf_type="proxy-${conf_type}"
  fi

  if [[ -n $wildcard ]]; then
    conf_type=${conf_type%-wildcard}
    [[ $wildcard == "true" ]] &&
      conf_type="${conf_type}-wildcard"
  fi

  echo "${conf_type}"
}

gridpane::check::site::nginx::cache() {
  local cache_check
  cache_check=$(cat /etc/nginx/sites-available/"${1}")
  case $cache_check in
  *"proxycache.conf"*|*"-wpfc.conf"*) echo "fastcgi" ;;
  *"-redis"*) echo "redis" ;;
  *) echo "php" ;;
  esac
}

gridpane::check::site::openlitespeed::config() {

  local GP_SITE="$1"

  /usr/local/bin/gpols get "$GP_SITE" proto

}

gridpane::check::site::openlitespeed::cache() {

  local GP_SITE="$1"

  /usr/local/bin/gpols get "$GP_SITE" cache

}

gridpane::display::site::nginx::config() {

  site_config_to_show="$1"

  if [[ ! -f /etc/nginx/sites-available/${site_config_to_show} ]] &&
    [[ ! -f /etc/nginx/sites-enabled/${site_config_to_show} ]]; then

    echo "Error... Site $site_config_to_show doesn't have a configs"
    echo "In /etc/nginx/sites-available or symlinked to /etc/nginx/sites-enabled"
    exit 187

  elif [[ -f /etc/nginx/sites-available/${site_config_to_show} ]] &&
    [[ ! -f /etc/nginx/sites-enabled/${site_config_to_show} ]]; then

    echo "Error... Site $site_config_to_show exists in /etc/nginx/sites-available"
    echo "But is not symlinked to /etc/nginx/sites-enabled"
    exit 187

  elif [[ ! -f /etc/nginx/sites-available/${site_config_to_show} ]]; then

    echo "Error... Site $site_config_to_show doesn't have a config in /etc/nginx/sites-available"
    exit 187

  fi

  if [[ ${last_arg} != "-q" ]] && [[ ${last_arg} != "-quiet" ]]; then

    echo "--------------------------------------------------"

  fi

  cat /etc/nginx/sites-available/${site_config_to_show}

  if [[ ${last_arg} != "-q" ]] && [[ ${last_arg} != "-quiet" ]]; then

    echo "--------------------------------------------------"

  fi

}

gridpane::site::lock::function() {

  local function_to_lock
  local site_to_lock
  function_to_lock="$1"
  site_to_lock="$2"

  if [[ -f /tmp/gridpane.${site_to_lock}.${function_to_lock}.lock ]]; then
    echo "-------------------------------------------------------------------"
    echo "Unable to grab lock exiting this process..."
    echo "-------------------------------------------------------------------"
    exit 187
  else
    touch /tmp/gridpane.${site_to_lock}.${function_to_lock}.lock
    echo "-------------------------------------------------------------------"
    echo "Creating lock file @ /tmp/gridpane.${site_to_lock}.${function_to_lock}.lock"
    echo "-------------------------------------------------------------------"
  fi

  #TODO (Jeff) trap rm this with unlock function when setting lock...
}

gridpane::site::unlock::function() {

  local function_to_lock
  local site_to_lock
  local log_to_use
  function_to_lock="$1"
  site_to_lock="$2"
  log_to_use="$3"

  echo "--------------------------------------------------------------------" | tee -a /var/www/${site_to_lock}/logs/${log_to_use}.log
  echo "Removing lockfile @ /tmp/gridpane.${site_to_lock}.${function_to_lock}.lock" | tee -a /var/www/${site_to_lock}/logs/${log_to_use}.log
  rm /tmp/gridpane.${site_to_lock}.${function_to_lock}.lock | tee -a /var/www/${site_to_lock}/logs/${log_to_use}.log
  echo "--------------------------------------------------------------------" | tee -a /var/www/${site_to_lock}/logs/${log_to_use}.log
}

gridpane::check::config::fastcgi::sockpass() {

  local site_to_check
  site_to_check="$1"
  sockfile=$(gridpane::get::site::sockfile ${site_to_check})

  find /etc/nginx/common -name "${site_to_check}*.conf" -exec sed -i "s/fastcgi_pass php;/fastcgi_pass ${sockfile};/g" {} \;
}

gridpane::cache::nginx::check::restore() {

  local site_to_check
  local new_cache_conf
  local orig_cache_conf
  local new_proxy_cache_conf
  local orig_proxy_cache_conf
  site_to_check="$1"
  new_cache_conf="$2"
  orig_cache_conf="$3"
  new_proxy_cache_conf="$4"
  orig_proxy_cache_conf="$5"

  check_nginx=$(nginx -t 2>&1)
  echo "${check_nginx}" | tee -a /var/www/${site_to_check}/logs/gridpane-general.log
  if [[ ${check_nginx} == *"${site_to_check}"* ]] &&
    [[ ${check_nginx} != *"syntax is ok"* ]] &&
    [[ ${check_nginx} != *"test is successful"* ]]; then
    echo "New ${site_to_check} Nginx Configs are failing a Syntax check and will crash Nginx..." | tee -a /var/www/${site_to_check}/logs/gridpane-general.log
    echo "Removing them and restoring original configurations" | tee -a /var/www/${site_to_check}/logs/gridpane-general.log
    echo "Moving problem configs to /etc/nginx/temp-backup/${nonce}-..." | tee -a /var/www/${site_to_check}/logs/gridpane-general.log

    mv /etc/nginx/sites-available/${site_to_check} /etc/nginx/temp-backup/error-${nonce}-${site_to_check}-${file_datetime} |
      tee -a /var/www/${site_to_check}/logs/gridpane-general.log

    mv /etc/nginx/temp-backup/${nonce}-${site_to_check} /etc/nginx/sites-available/${site_to_check} |
      tee -a /var/www/${site_to_check}/logs/gridpane-general.log

    mv /etc/nginx/common/${site_to_check}-${new_cache_conf} \
      /etc/nginx/temp-backup/error-${nonce}-${site_to_check}-${new_cache_conf}-${file_datetime} |
      tee -a /var/www/${site_to_check}/logs/gridpane-general.log

    mv /etc/nginx/temp-backup/${nonce}-${site_to_check}-${orig_cache_conf} /etc/nginx/common/${site_to_check}-${orig_cache_conf} |
      tee -a /var/www/${site_to_check}/logs/gridpane-general.log

    if [[ -f /etc/nginx/temp-backup/${nonce}-${site_to_check}-${orig_proxy_cache_conf} ]]; then
      mv /etc/nginx/common/${site_to_check}-${new_proxy_cache_conf} \
        /etc/nginx/temp-backup/error-${nonce}-${site_to_check}-${new_proxy_cache_conf}-${file_datetime} |
        tee -a /var/www/${site_to_check}/logs/gridpane-general.log

      mv /etc/nginx/temp-backup/${nonce}-${site_to_check}-${orig_proxy_cache_conf} \
        /etc/nginx/common/${site_to_check}-${orig_proxy_cache_conf} |
        tee -a /var/www/${site_to_check}/logs/gridpane-general.log
    fi

    if [[ -f /var/www/${site_to_check}/logs/${site_to_check}-additional-domains.env ]]; then
      while read add_on_domain; do
        add_on_domain=${add_on_domain#:}
        add_on_domain=${add_on_domain%:}

        # Now restore all the add on domains
        mv /etc/nginx/temp-backup/${nonce}-${add_on_domain} /etc/nginx/sites-available/${add_on_domain} |
          tee -a /var/www/${site_to_check}/logs/gridpane-general.log

        if [[ -f /etc/nginx/temp-backup/${nonce}-${add_on_domain}-${orig_proxy_cache_conf} ]]; then
          mv /etc/nginx/temp-backup/${nonce}-${add_on_domain}-${orig_proxy_cache_conf} \
            /etc/nginx/temp-backup/${nonce}-${add_on_domain}-${orig_proxy_cache_conf} |
            tee -a /var/www/${site_to_check}/logs/gridpane-general.log
        fi
      done </var/www/${site_to_check}/logs/${site_to_check}-additional-domains.env
    fi

    cache_type=$(gridpane::check::site::nginx::cache ${site_to_check})
    if [[ ${cache_type} == "php" ]]; then
      cache_type="off"
    fi

    details="New Nginx FastCGI Caching Configs failed a Syntax Check, restoring original configuration and exiting - please check logs, and contact support"
    failCURL=$(
      /usr/bin/curl - d '{ "site_url":"'${site_to_check}'", "server_ip":"'${serverIP}'", "caching":"'${cache_type}'", "details":"'${details}'" }' \
        -H "Content-Type: application/json" \
        -X POST https://${GPURL}/api/site/site-update?api_token=${gridpanetoken} \
        2>&1
    )

    gridpane::site::unlock::function "cache" "${site_to_check}" "gridpane-general"
    logIt gridpane-general gridpane-caching End ${site_to_check} "${failCURL}"

  elif [[ ${check_nginx} != *"syntax is ok"* ]] &&
    [[ ${check_nginx} != *"test is successful"* ]]; then
    if [[ -f /var/www/${site_to_check}/logs/${site_to_check}-additional-domains.env ]]; then
      local add_on_domain_error

      while read add_on_domain; do
        add_on_domain=${add_on_domain#:}
        add_on_domain=${add_on_domain%:}

        if [[ ${check_nginx} == *"${add_on_domain}"* ]] &&
          [[ ${check_nginx} != *"syntax is ok"* ]] &&
          [[ ${check_nginx} != *"test is successful"* ]]; then
          add_on_domain_error="true"
        fi
      done </var/www/${site_to_check}/logs/${site_to_check}-additional-domains.env

      if [[ ${add_on_domain_error} != "true" ]]; then
        echo "New ${site_to_check} Nginx Configs are failing a Syntax check from add on domains and will crash Nginx..." |
          tee -a /var/www/${site_to_check}/logs/gridpane-general.log
        echo "Removing them and restoring original configurations" | tee -a /var/www/${site_to_check}/logs/gridpane-general.log
        echo "Moving problem configs to /etc/nginx/temp-backup/${nonce}-..." | tee -a /var/www/${site_to_check}/logs/gridpane-general.log

        mv /etc/nginx/sites-available/${site_to_check} /etc/nginx/temp-backup/error-${nonce}-${site_to_check}-${file_datetime} |
          tee -a /var/www/${site_to_check}/logs/gridpane-general.log

        mv /etc/nginx/temp-backup/${nonce}-${site_to_check} /etc/nginx/sites-available/${site_to_check} |
          tee -a /var/www/${site_to_check}/logs/gridpane-general.log

        mv /etc/nginx/common/${site_to_check}-${new_cache_conf} \
          /etc/nginx/temp-backup/error-${nonce}-${site_to_check}-${new_cache_conf}-${file_datetime} |
          tee -a /var/www/${site_to_check}/logs/gridpane-general.log

        mv /etc/nginx/temp-backup/${nonce}-${site_to_check}-${orig_cache_conf} \
          /etc/nginx/common/${site_to_check}-${orig_cache_conf} |
          tee -a /var/www/${site_to_check}/logs/gridpane-general.log

        if [[ -f /etc/nginx/temp-backup/${nonce}-${site_to_check}-${orig_proxy_cache_conf} ]]; then
          mv /etc/nginx/common/${site_to_check}-${new_proxy_cache_conf} \
            /etc/nginx/temp-backup/error-${nonce}-${site_to_check}-${new_proxy_cache_conf}-${file_datetime} |
            tee -a /var/www/${site_to_check}/logs/gridpane-general.log

          mv /etc/nginx/temp-backup/${nonce}-${site_to_check}-${orig_proxy_cache_conf} \
            /etc/nginx/common/${site_to_check}-${orig_proxy_cache_conf} |
            tee -a /var/www/${site_to_check}/logs/gridpane-general.log
        fi

        while read add_on_domain; do
          add_on_domain=${add_on_domain#:}
          add_on_domain=${add_on_domain%:}

          # Now restore all the add on domains
          mv /etc/nginx/temp-backup/${nonce}-${add_on_domain} /etc/nginx/sites-available/${add_on_domain} |
            tee -a /var/www/${site_to_check}/logs/gridpane-general.log

          if [[ -f /etc/nginx/temp-backup/${nonce}-${add_on_domain}-${orig_proxy_cache_conf} ]]; then
            mv /etc/nginx/temp-backup/${nonce}-${add_on_domain}-${orig_proxy_cache_conf} \
              /etc/nginx/temp-backup/${nonce}-${add_on_domain}-${orig_proxy_cache_conf} |
              tee -a /var/www/${site_to_check}/logs/gridpane-general.log
          fi
        done </var/www/${site_to_check}/logs/${site_to_check}-additional-domains.env

        cache_type=$(gridpane::check::site::nginx::cache ${site_to_check})
        if [[ ${cache_type} == "php" ]]; then
          cache_type="off"
        fi

        details="New Nginx FastCGI Caching Configs failed a Syntax Check, restoring original configuration and exiting - please check logs, and contact support"
        failCURL=$(
          /usr/bin/curl \
            -d '{ "site_url":"'${site_to_check}'", "server_ip":"'${serverIP}'", "caching":"'${cache_type}'", "details":"'${details}'" }' \
            -H "Content-Type: application/json" \
            -X POST https://${GPURL}/api/site/site-update?api_token=${gridpanetoken}
          2>&1
        )

        gridpane::site::unlock::function "cache" "${site_to_check}" "gridpane-general"
        logIt gridpane-general gridpane-caching End ${site_to_check} "${failCURL}"
      else
        echo "Nginx is failing syntax checks... but not from ${site_to_check} or add on domains, but reload will fail." | tee -a /var/www/${site_to_check}/logs/gridpane-general.log
      fi
    else
      echo "Nginx is failing syntax checks... but seems to be caused by external process... reload will fail." | tee -a /var/www/${site_to_check}/logs/gridpane-general.log
    fi
  fi

}

gridpane::cache::nginx::process::aliasdomains() {

  local site_to_process="$1"
  local cache_type="$2"
  local orig_proxy_cache_conf="$3"

  if [[ -f /var/www/${site_to_process}/logs/${site_to_process}-additional-domains.env ]]; then
    while read add_on_domain; do
      add_on_domain=${add_on_domain#:}
      add_on_domain=${add_on_domain%:}

      echo "Add on domains found - ${add_on_domain}"
      echo "Backing up /etc/nginx/temp-backup/${nonce}-${add_on_domain}"

      local add_ons_conf_type=$(gridpane::check::site::nginx::config ${add_on_domain})
      echo "add on conf type: ${add_ons_conf_type}"

      mv /etc/nginx/sites-available/${add_on_domain} \
        /etc/nginx/temp-backup/${nonce}-${add_on_domain}

      if [[ -f /etc/nginx/common/${add_on_domain}-${orig_proxy_cache_conf} ]]; then
        echo "Backing up /etc/nginx/temp-backup/${nonce}-${add_on_domain}-${orig_proxy_cache_conf}" |
          tee -a /var/www/${site_to_process}/logs/gridpane-general.log

        mv /etc/nginx/common/${add_on_domain}-${orig_proxy_cache_conf} \
          /etc/nginx/temp-backup/${nonce}-${add_on_domain}-${orig_proxy_cache_conf} |
          tee -a /var/www/${site_to_process}/logs/gridpane-general.log
      fi

      local generatedNgxConfig=$(generateNgxConfig ${add_ons_conf_type} ${cache_type} ${site_to_process} ${add_on_domain})
      echo "$generatedNgxConfig"
    done </var/www/${site_to_process}/logs/${site_to_process}-additional-domains.env
  else
    echo "No additional domains found for ${site_to_process}..."
  fi

}

gridpane::cache::openlitespeed::process::aliasdomains() {

  local site_to_process="$1"
  local add_ons_conf_type
  local generatedConfig

  if [[ -f /var/www/${site_to_process}/logs/${site_to_process}-additional-domains.env ]]; then
    while read add_on_domain; do
      add_on_domain=${add_on_domain#:}
      add_on_domain=${add_on_domain%:}

      /bin/echo "Add on domains found - ${add_on_domain}"
      /bin/echo "Backing up /usr/local/lsws/conf/backup/${nonce}-${add_on_domain}"

      add_ons_conf_type="$(/usr/local/bin/gpols get "$add_ons_conf_type" proto)"
      /bin/echo "add on conf type: ${add_ons_conf_type}"

      /bin/mv /usr/local/lsws/conf/vhosts/"$add_on_domain"/vhconf.conf \
        /usr/local/lsws/conf/backup/"$nonce"-"$add_on_domain"

      generatedConfig="$(/usr/local/bin/gpols addon "$add_on_domain" --output --no-restart)"
      /bin/echo "$generatedConfig"
    done </var/www/${site_to_process}/logs/${site_to_process}-additional-domains.env
  else
    /bin/echo "No additional domains found for ${site_to_process}..."
  fi

}

gridpane::redis::key_clear() {
  local pattern="$1"
  local keys
  local unlink
  # shellcheck disable=SC2035,SC2086
  keys=$(redis-cli --scan --pattern *${pattern}*)
  # shellcheck disable=SC2086
  unlink=$(echo ${keys} | xargs -n 1 redis-cli unlink)
  # shellcheck disable=SC2035,SC2086
  keys=$(redis-cli --scan --pattern *${pattern}*)
  # shellcheck disable=SC2086
  if [[ -n $keys ]]; then
    while read -r key; do
      redis-cli unlink "${key}"
    done <<<${keys}
  fi
}

gridpane::cache::clear() {
  if [[ -z $1 ]]; then
    redis-cli flushall
    local sitesDirectoryArray
    if [[ "$webserver" = nginx ]]; then
      /usr/sbin/service php7.1-fpm reload
      /usr/sbin/service php7.2-fpm reload
      /usr/sbin/service php7.3-fpm reload
      /usr/sbin/service php7.4-fpm reload
      sitesDirectoryArray=$(dir /etc/nginx/sites-enabled)
    else
      sitesDirectoryArray=$(dir /usr/local/lsws/conf/vhosts)
    fi

    local sitesVarArray
    sitesVarArray=$(ls /var/www)

    [[ -f /tmp/${nonce}.cache-clear.fail ]] && rm "/tmp/${nonce}.cache-clear.fail"

    for site_to_clear in ${sitesDirectoryArray}; do
      echo "${site_to_clear}"
      managed_domain="$(gridpane::get_managed_domain)"
      case ${site_to_clear} in
        22222|*-remote|html|default|canary.*|*."${managed_domain}") continue ;;
        *) [[ ${sitesVarArray} != *"${site_to_clear}"* ]] && continue ;;
      esac

      local site_to_clear_config
      [[ "$webserver" = nginx ]] &&
        site_to_clear_config=$(cat "/etc/nginx/sites-enabled/${site_to_clear}")

      if [[ "$webserver" = nginx && ${site_to_clear_config} == *"wpfc"* ||
              "$webserver" = nginx && ${site_to_clear_config} == *"proxycache"* ]]; then
        local scheme="http"
        [[ ${site_to_clear_config} == *"HTTPS"* ]] &&
          scheme="https"
        local purge
        purge=$(/usr/bin/curl -L -X PURGE -H "Host:${site_to_clear}" "${scheme}://127.0.0.1/purgeall" -k)
        [[ ${purge} == *"orbidden"* ]] &&
          touch "/tmp/${nonce}.cache-clear.fail"

        if [[ -f /var/www/${site_to_clear}/logs/${site_to_clear}-additional-domains.env ]]; then
          while read domain_to_clear; do
            domain_to_clear=${domain_to_clear#:}
            domain_to_clear=${domain_to_clear%:}
            echo "Flushing ${domain_to_clear}"
            local domain_to_clear_config
            domain_to_clear_config=$(cat "/etc/nginx/sites-enabled/${domain_to_clear}")
            local domain_scheme
            domain_scheme="http"
            [[ ${domain_to_clear_config} == *"HTTPS"* ]] &&
              domain_scheme="https"
            local domain_to_purge
            domain_to_purge="${site_to_clear}"
            [[ ${site_to_clear_config} == *"WWW"* ]] &&
              domain_to_purge="www.${domain_to_purge}"
            local domain_purge
            domain_purge=$(/usr/bin/curl -L -X PURGE -H "Host:${domain_to_purge}" "${domain_scheme}://127.0.0.1/purgeall" -k)
            [[ ${domain_purge} == *"orbidden"* ]] &&
              touch "/tmp/${nonce}.cache-clear.fail"
          done <"/var/www/${site_to_clear}/logs/${site_to_clear}-additional-domains.env"
        fi
      fi

      local siteowner
      siteowner=$(gridpane::get::set::site::user "${site_to_clear}")
      echo "Flushing ${site_to_clear}"
      [[ "$webserver" = openlitespeed ]] &&
        sudo su - "$siteowner" -c "timeout 30 /usr/local/bin/wp litespeed-purge all --path=/var/www/${site_to_clear}/htdocs"
      sudo su - "${siteowner}" -c "timeout 30 /usr/local/bin/wp cache flush --path=/var/www/${site_to_clear}/htdocs 2>&1"
    done

    if [[ "$webserver" = nginx && -f /tmp/${nonce}.cache-clear.fail ]]; then
      [[ ${site_to_clear_config} == *"wpfc"* ||
        ${site_to_clear_config} == *"proxycache"* ]] &&
        [[ -d /var/run/nginx-cache ]] && rm -rf /var/run/nginx-cache/* &&
        [[ -d /var/run/nginx-proxy-cache/ ]] && rm -rf /var/run/nginx-proxy-cache/*
      local clearNginx
      clearNginx=$(nginxServices reload)
      rm "/tmp/${nonce}.cache-clear.fail"
    fi

  else
    local site_to_clear
    site_to_clear="$1"
    local siteowner
    siteowner=$(gridpane::get::set::site::user "${site_to_clear}")
    echo "$siteowner"
    local php_ver
    php_ver=$(gridpane::get::site::php "${site_to_clear}")
    echo "$php_ver"
    echo "Flushing ${site_to_clear}"
    local site_to_clear_config
    [[ "$webserver" = nginx ]] &&
      site_to_clear_config=$(cat "/etc/nginx/sites-enabled/${site_to_clear}")

    # TODO (Jeff) use wp-cli to purge lscache page caching... if possible for alias domains too?
    if [[ "$webserver" = nginx && ${site_to_clear_config} == *"wp-redis"* ||
          "$webserver" = nginx && ${site_to_clear_config} == *"rediscache"* ||
          "$webserver" = openlitespeed && "$(/usr/local/bin/gpols get "$site_to_clear" cache)" = redis ]]; then
      gridpane::redis::key_clear "${site_to_clear}"
      if [[ -f /var/www/${site_to_clear}/logs/${site_to_clear}-additional-domains.env ]]; then
        while read domain_to_clear; do
          domain_to_clear=${domain_to_clear#:}
          domain_to_clear=${domain_to_clear%:}
          echo "Flushing ${domain_to_clear}"
          gridpane::redis::key_clear "${domain_to_clear}"
        done <"/var/www/${site_to_clear}/logs/${site_to_clear}-additional-domains.env"
      fi
    elif [[ "$webserver" = nginx && ${site_to_clear_config} == *"wpfc"* ]] ||
        [[ "$webserver" = nginx && ${site_to_clear_config} == *"proxycache"* ]]; then
      local scheme
      scheme="http"
      [[ ${site_to_clear_config} == *"HTTPS"* ]] &&
        scheme="https"
      local site_to_purge
      site_to_purge="${site_to_clear}"
      [[ ${site_to_clear_config} == *"WWW"* ]] &&
        site_to_purge="www.${site_to_purge}"
      local purge
      purge=$(/usr/bin/curl -L -X PURGE -H "Host:${site_to_purge}" "${scheme}://127.0.0.1/purgeall" -k)
      [[ ${purge} == *"orbidden"* ]] &&
        touch "/tmp/${nonce}.cache-clear.fail"

      if [[ -f /var/www/${site_to_clear}/logs/${site_to_clear}-additional-domains.env ]]; then
        while read domain_to_clear; do
          domain_to_clear=${domain_to_clear#:}
          domain_to_clear=${domain_to_clear%:}
          echo "Flushing ${domain_to_clear}"
          local domain_to_clear_config
          domain_to_clear_config=$(cat "/etc/nginx/sites-enabled/${domain_to_clear}")
          local domain_scheme
          domain_scheme="http"
          [[ ${domain_to_clear_config} == *"HTTPS"* ]] &&
            domain_scheme="https"
          local domain_to_purge
          domain_to_purge="${site_to_clear}"
          [[ ${site_to_clear_config} == *"WWW"* ]] &&
            domain_to_purge="www.${domain_to_purge}"
          local domain_purge
          domain_purge=$(/usr/bin/curl -L -X PURGE -H "Host:${domain_to_purge}" "${domain_scheme}://127.0.0.1/purgeall" -k)
          [[ ${domain_purge} == *"orbidden"* ]] &&
            touch "/tmp/${nonce}.cache-clear.fail"
        done <"/var/www/${site_to_clear}/logs/${site_to_clear}-additional-domains.env"
      fi

      if [[ -f /tmp/${nonce}.cache-clear.fail ]]; then
        [[ ${site_to_clear_config} == *"wpfc"* ]] && rm -rf /var/run/nginx-cache/*
        [[ ${site_to_clear_config} == *"proxycache"* ]] && rm -rf /var/run/nginx-proxy-cache/*
        local clearNginx
        clearNginx=$(nginxServices reload)
        rm "/tmp/${nonce}.cache-clear.fail"
      fi
    fi

    sudo su - "${siteowner}" -c "timeout 30 /usr/local/bin/wp cache flush --path=/var/www/${site_to_clear}/htdocs 2>&1"
    ### Should be cleared by the wp cache flush
    ### If redis page caching is enabled, this is redundant as will be caught by it's wildcard
    gridpane::redis::key_clear "${site_to_clear}"

    if [[ "$webserver" = nginx ]]; then
      /usr/sbin/service php"${php_ver}"-fpm reload
    else
      local ols_cache_check
      ols_cache_check="$(sudo su - "$siteowner" -c "timeout 30 /usr/local/bin/wp option get litespeed.conf.cache --path=/var/www/${site_to_clear}/htdocs")"
      if [[ "$ols_cache_check" = 1 ]]; then
          sudo su - "$siteowner" -c "timeout 30 /usr/local/bin/wp litespeed-purge all --path=/var/www/${site_to_clear}/htdocs"
      fi
    fi
  fi

  echo "Redis Cache has been successfully cleared."
  if [[ "$webserver" = nginx ]]; then
    echo "PHP OpCache has been successfully cleared."
    echo "Nginx FastCGI/Proxy Cache has been successfully cleared."
  else
    echo "OpenLiteSpeed Cache has been purged."
  fi
}

gridpane::cache::clear_page() {
  local site="$1"
  siteowner=$(gridpane::get::set::site::user "${site}")
  echo "$siteowner"
  php_ver=$(gridpane::get::site::php "${site}")
  echo "$php_ver"
  echo "Flushing ${site} page cache"

  local site_config
  if [[ "$webserver" = nginx ]]; then
    site_config=$(cat /etc/nginx/sites-enabled/"${site}")
  elif [[ "$webserver" = openlitespeed ]]; then
    # TODO (JEFF) @cim not sure this is even needed?
    cim=openlitespeed
  fi

  # TODO (Jeff) use wp-cli to purge lscache page caching... if possible for alias domains too?
  if [[ "$webserver" = nginx && ${site_config} == *"wp-redis"* ||
        "$webserver" = nginx && ${site_config} == *"rediscache"* ]]; then
    echo "Flushing $site Using redis-cli"
    gridpane::redis::key_clear "nginx-cache:*GET${site}"
    gridpane::redis::key_clear "nginx-cache:*GETwww${site}"
    gridpane::redis::key_clear "nginx-cache:*HEAD${site}"
    gridpane::redis::key_clear "nginx-cache:*HEADwww${site}"
    gridpane::redis::key_clear "nginx-cache:*PUT${site}"
    gridpane::redis::key_clear "nginx-cache:*PUTwww${site}"
    gridpane::redis::key_clear "nginx-cache:*PATCH${site}"
    gridpane::redis::key_clear "nginx-cache:*PATCHwww${site}"
    gridpane::redis::key_clear "nginx-cache:*DELETE${site}"
    gridpane::redis::key_clear "nginx-cache:*DELETEwww${site}"
    gridpane::redis::key_clear "nginx-cache:*POST${site}"
    gridpane::redis::key_clear "nginx-cache:*POSTwww${site}"
    gridpane::redis::key_clear "nginx-cache:*OPTIONS${site}"
    gridpane::redis::key_clear "nginx-cache:*OPTIONSwww${site}"
    if [[ -f /var/www/${site}/logs/${site}-additional-domains.env ]]; then
      # shellcheck disable=SC2162
      while read domain; do
        domain=${domain#:}
        domain=${domain%:}
        echo "Flushing ${domain} using redis-cli"
        gridpane::redis::key_clear "nginx-cache:*GET${domain}"
        gridpane::redis::key_clear "nginx-cache:*GETwww${domain}"
        gridpane::redis::key_clear "nginx-cache:*HEAD${domain}"
        gridpane::redis::key_clear "nginx-cache:*HEADwww${domain}"
        gridpane::redis::key_clear "nginx-cache:*PUT${domain}"
        gridpane::redis::key_clear "nginx-cache:*PUTwww${domain}"
        gridpane::redis::key_clear "nginx-cache:*PATCH${domain}"
        gridpane::redis::key_clear "nginx-cache:*PATCHwww${domain}"
        gridpane::redis::key_clear "nginx-cache:*DELETE${domain}"
        gridpane::redis::key_clear "nginx-cache:*DELETEwww${domain}"
        gridpane::redis::key_clear "nginx-cache:*POST${domain}"
        gridpane::redis::key_clear "nginx-cache:*POSTwww${domain}"
        gridpane::redis::key_clear "nginx-cache:*OPTIONS${domain}"
        gridpane::redis::key_clear "nginx-cache:*OPTIONSwww${domain}"
      done </var/www/"${site}"/logs/"${site}"-additional-domains.env
    fi
    /usr/sbin/service php"${php_ver}"-fpm reload
  elif [[ "$webserver" = nginx && ${site_config} == *"wpfc"* ||
          "$webserver" = nginx && ${site_config} == *"proxycache"* ]]; then
    local scheme
    scheme="http"
    [[ ${site_config} == *"HTTPS"* ]] &&
      scheme="https"
    local site_to_purge
    site_to_purge="${site}"
    [[ ${site_config} == *"WWW"* ]] &&
      site_to_purge="www.${site_to_purge}"
    local purge
    purge=$(/usr/bin/curl -L -X PURGE -H "Host:${site_to_purge}" "${scheme}://127.0.0.1/purgeall" -k)
    [[ ${purge} == *"orbidden"* ]] &&
      touch /tmp/"${nonce}".cache-clear.fail

    if [[ -f /var/www/${site}/logs/${site}-additional-domains.env ]]; then
      while read domain; do
        domain=${domain#:}
        domain=${domain%:}
        echo "Flushing ${domain}"
        local domain_config
        domain_config=$(cat /etc/nginx/sites-enabled/"${domain}")
        local domain_scheme
        domain_scheme="http"
        [[ ${domain_config} == *"HTTPS"* ]] &&
          domain_scheme="https"
        local domain_to_purge
        domain_to_purge="${domain}"
        [[ ${domain_config} == *"WWW"* ]] &&
          domain_to_purge="www.${domain_to_purge}"
        local domain_purge
        domain_purge=$(/usr/bin/curl -L -X PURGE -H "Host:${domain_to_purge}" "${domain_scheme}://127.0.0.1/purgeall" -k)
        [[ ${domain_purge} == *"orbidden"* ]] &&
          touch /tmp/"${nonce}".cache-clear.fail
      done </var/www/"${site}"/logs/"${site}"-additional-domains.env
    fi

    if [[ -f /tmp/${nonce}.cache-clear.fail ]]; then
      [[ ${site_config} == *"wpfc"* ]] &&
        rm -rf /var/run/nginx-cache/*
      [[ ${site_config} == *"proxycache"* ]] &&
        rm -rf /var/run/nginx-proxy-cache/*
      local clearNginx
      clearNginx=$(nginxServices reload)
      rm /tmp/"${nonce}".cache-clear.fail
    fi

    /usr/sbin/service php"${php_ver}"-fpm reload
  elif [[ "$webserver" = openlitespeed ]]; then
    # TODO (JEFF) @cim needs just a page cache purge here
    cim=openlitespeed_purge_page_cache
  fi
}

gridpane::cache::clear_object() {
  local site="$1"
  siteowner=$(gridpane::get::set::site::user "${site}")
  echo "$siteowner"
  php_ver=$(gridpane::get::site::php "${site}")
  echo "$php_ver"
  echo "Flushing ${site} object cache"
  # TODO (JEFF) @cim using redis and wp-cli directly - should be good with ols?
  sudo su - "${siteowner}" -c "timeout 30 /usr/local/bin/wp cache flush --path=/var/www/${site}/htdocs 2>&1"
  gridpane::redis::key_clear "object_cache_${site}"
  if [[ "$webserver" = nginx ]]; then
    /usr/sbin/service php"${php_ver}"-fpm reload
  fi
}

#######################################
# Moves any incorrectly named LE 0000 cert
# directories to the expected location
#
# Accepted Args:
#  ssl_domain
#######################################
gridpane::letsencrypt::0000::fix() {
  local ssl_domain="$1"
  if [[ ! -d /etc/letsencrypt/live/${ssl_domain} ]]; then
    [[ -d /etc/letsencrypt/live/${ssl_domain}-0001 ]] && mv /etc/letsencrypt/live/${ssl_domain}-0001 /etc/letsencrypt/live/${ssl_domain}
    [[ -d /etc/letsencrypt/live/${ssl_domain}-0002 ]] && mv /etc/letsencrypt/live/${ssl_domain}-0002 /etc/letsencrypt/live/${ssl_domain}
    [[ -d /etc/letsencrypt/live/${ssl_domain}-0003 ]] && mv /etc/letsencrypt/live/${ssl_domain}-0003 /etc/letsencrypt/live/${ssl_domain}
    [[ -d /etc/letsencrypt/live/${ssl_domain}-0004 ]] && mv /etc/letsencrypt/live/${ssl_domain}-0004 /etc/letsencrypt/live/${ssl_domain}
    [[ -d /etc/letsencrypt/live/${ssl_domain}-0005 ]] && mv /etc/letsencrypt/live/${ssl_domain}-0005 /etc/letsencrypt/live/${ssl_domain}
  fi
}

#######################################
# Grabs and formats the public suffix database
# for second level domains.
# e.g .co.uk
#
#######################################
gridpane::get::publicsuffix::dat() {
  if [[ ! -f /opt/gridpane/second-level-domains.dat ]]; then
    wget --timeout=15 -O /opt/gridpane/public_suffix_list.dat https://publicsuffix.org/list/public_suffix_list.dat
    sed -i '/\/\//d' /opt/gridpane/public_suffix_list.dat
    mv /opt/gridpane/public_suffix_list.dat /opt/gridpane/second-level-domains.dat
    [[ -f /opt/gridpane/public_suffix_list.dat ]] && rm /opt/gridpane/public_suffix_list.dat
  fi
}

#######################################
# Validate DNS creds files
# Needs testing extensively
# - all values accessible
# - all values should be readonly
#######################################
gridpane::domain::validate::dns::creds() {

  local primary_site
  local domain_for_creds

  primary_site="$1"
  if [[ -z $2 ]]; then
    domain_for_creds="$1"
  else
    domain_for_creds="$2"
  fi

  if [[ -f /var/www/${primary_site}/dns/${domain_for_creds}.creds ]]; then
    /usr/bin/dos2unix /var/www/${primary_site}/dns/${domain_for_creds}.creds
    sed -i '/^[[:space:]]*$/d' /var/www/${primary_site}/dns/${domain_for_creds}.creds
    provider=$(grep -w "^provider:.*" /var/www/${primary_site}/dns/${domain_for_creds}.creds | cut -f 2 -d ':')
    api_key=$(grep -w "^api-key:.*" /var/www/${primary_site}/dns/${domain_for_creds}.creds | cut -f 2 -d ':')
    if [[ -z ${provider} ]] || [[ -z ${api_key} ]]; then
      proceed_with_dns_api="false"
    else
      check_creds=$(cat /var/www/${primary_site}/dns/${domain_for_creds}.creds)
      case ${check_creds} in
      *"api-secret"*) secret_or_email=$(grep -w "^api-secret:.*" /var/www/${primary_site}/dns/${domain_for_creds}.creds | cut -f 2 -d ':') ;;
      *"api-email"*) secret_or_email=$(grep -w "^api-secret:.*" /var/www/${primary_site}/dns/${domain_for_creds}.creds | cut -f 2 -d ':') ;;
      *)
        secret_or_email="NULL"
        proceed_with_dns_api="false"
        ;;
      esac
    fi
    if [[ ${proceed_with_dns_api} != "false" ]]; then
      if [[ ${check_creds} == *"nameserver-domain"* ]]; then
        expected_nameserver_domain=$(grep -w "^nameserver-domain:.*" /var/www/${primary_site}/dns/${domain_for_creds}.creds | cut -f 2 -d ':')
      else
        case ${check_creds} in
        *"dnsme"*) expected_nameserver_domain="dnsmadeeasy.com" ;;
        *"cloudflare"*) expected_nameserver_domain="cloudflare.com" ;;
        *)
          proceed_with_dns_api="false"
          expected_nameserver_domain="NULL"
          ;;
        esac
      fi
      [[ ${check_creds} == *"challenge-domain"* ]] &&
        challenge_alias=$(grep -w "^challenge-domain:.*" /var/www/${primary_site}/dns/${domain_for_creds}.creds | cut -f 2 -d ':')
      gridpane::get::publicsuffix::dat
      second_level_domains_list=$(cat /opt/gridpane/second-level-domains.dat)
      domain_for_ssl_tld=$(grep -o '[^.]*\.[^.]*$' <<<"${domain_for_creds}")
      [[ ${second_level_domains_list} == *"${domain_for_ssl_tld}"* ]] &&
        domain_for_ssl_tld=$(grep -o '[^.]*\.[^.]*\.[^.]*$' <<<"${domain_for_creds}")
      domain_name_servers=$(dig @8.8.8.8 +short NS "${domain_for_ssl_tld}")
      [[ -z ${domain_name_servers} ]] && domain_name_servers="Empty-Response"
      if [[ ${domain_name_servers} != *"${expected_nameserver_domain}"* ]]; then
        domain_for_ssl_tld_resolving="false"
        if [[ -z ${challenge_alias} ]]; then
          challenge_alias_tld_resolving="false"
          proceed_with_dns_api="false"
        else
          challenge_alias_tld=$(grep -o '[^.]*\.[^.]*$' <<<"${challenge_alias}")
          [[ ${second_level_domains_list} == *"${challenge_alias_tld}"* ]] &&
            challenge_alias_tld=$(grep -o '[^.]*\.[^.]*\.[^.]*$' <<<"${challenge_alias}")
          challenge_name_servers=$(dig @8.8.8.8 +short NS "${challenge_alias_tld}")
          [[ -z ${challenge_name_servers} ]] && challenge_name_servers="Empty-Response"
          if [[ ${challenge_name_servers} != *"${expected_nameserver_domain}"* ]]; then
            proceed_with_dns_api="false"
            challenge_alias_tld_resolving="false"
          else
            challenge_alias_tld_resolving="true"
            proceed_with_dns_api="true"
          fi
        fi
      else
        domain_for_ssl_tld_resolving="true"
        proceed_with_dns_api="true"
        if [[ -z ${challenge_alias} ]]; then
          challenge_alias_tld_resolving="false"
        else
          challenge_alias_tld=$(grep -o '[^.]*\.[^.]*$' <<<"${challenge_alias}")
          [[ ${second_level_domains_list} == *"${challenge_alias_tld}"* ]] &&
            challenge_alias_tld=$(grep -o '[^.]*\.[^.]*\.[^.]*$' <<<"${challenge_alias}")
          challenge_name_servers=$(dig @8.8.8.8 +short NS "${challenge_alias_tld}")
          [[ -z ${challenge_name_servers} ]] && challenge_name_servers="Empty-Response"
          if [[ ${challenge_name_servers} == *"${expected_nameserver_domain}"* ]]; then
            challenge_alias_tld_resolving="true"
          else
            challenge_alias_tld_resolving="false"
          fi
        fi
      fi
    else
      echo "-------------------------------------------------------------------"
      echo "Invalid Api creds... can't proceed with API SSL for ${domain_for_creds}"
      echo "-------------------------------------------------------------------"
    fi
  fi
}

gridpane::ip_check() {
  if [[ $1 =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    echo "IP passed format validation..."
  else
    echo "Remote IP provided $1 is not a valid IP... exiting..."
    case $2 in
    clone) gpclone::end::failsync_pause::exit "Remote IP provided $1 is not a valid IP... exiting...";;
    *) exit 187 ;;
    esac
  fi
}

#######################################
# Generate array of sites
# ${sitesDirectoryArray}
#######################################
gridpane::site_loop::directories() {
  readonly sitesDirectoryArray=$(ls -l /var/www | egrep '^d' | awk '{print $9}')
  if [[ "$webserver" = nginx ]]; then
    readonly sitesConfigsArray=$(dir /etc/nginx/sites-enabled)
  elif [[ "$webserver" = openlitespeed ]]; then
    readonly sitesConfigsArray=$(dir /usr/local/lsws/conf/vhosts)
  fi
}

gridpane::get_managed_domain() {
  local managed_domain
  managed_domain=$(grep -w "managed-domain:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  if [[ -z ${managed_domain} ]]; then
    managed_domain="$(cat /root/grid.subdom)"
    managed_domain="${managed_domain#*.}"
    [[ -n ${managed_domain} ]] &&
      local write_it_back &&
      write_it_back=$(gridpane::conf_write managed-domain "${managed_domain}")
  fi
  ## This fallback should be replaced with an api call to be able to get this value and heal env
  [[ -z ${managed_domain} ]] &&
    managed_domain="gridpanevps.com"
  echo "${managed_domain}"
}

gridpane::check::multisite() {
  local site="$1"
  grep_multisite=$(grep "WP_ALLOW_MULTISITE" "/var/www/${site}/wp-config.php")
  if [[ ${grep_multisite} == *"true"* || ${grep_multisite} == *"TRUE"* ]]; then
    local type=$(grep "SUBDOMAIN_INSTALL" "/var/www/${site}/wp-config.php")
    if [[ ${type} == *"true"* || ${type} == *"TRUE"* ]]; then
      type="-subdomain"
    else
      type="-subdirectory"
    fi
    echo "true${type}"
  else
    echo "false"
  fi
}

gridpane::generate::username() {
  username=$(echo ${1} | sed "s/[^0-9A-Za-z]//g")
  username=${username%_*} \
    username=${username:0:11} \
    random1=$(($RANDOM % 10)) \
    random2=$(($RANDOM % 10)) \
    random3=$(($RANDOM % 10)) \
    random4=$(($RANDOM % 10)) \
    random5=$(($RANDOM % 10)) \
    username="${username}${random1}${random2}${random3}${random4}${random5}"
  echo "${username}"
}

gridpane::check::UUID() {
  if [[ -f /root/gridcreds/gridpane.uuid ]]; then
    local uuid="$(cat /root/gridcreds/gridpane.uuid)"
    gridpane::callback::app \
      "callback" \
      "/server/server-update" \
      "server_ip=${serverIP}" \
      "UUID=${uuid}" | tee -a /var/log/gridpane.log
  else
    echo "This doesn't seem to be a backups V2 enabled server..." | tee -a /var/log/gridpane.log
  fi
}

gridpane::check::search_replace_db() {
  if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
    [[ ! -f /opt/gridpane/Search-Replace-DB/srdb.cli.php ]] &&
      [[ -d /opt/gridpane/Search-Replace-DB ]] && rm -rf /opt/gridpane/Search-Replace-DB/
    /usr/bin/git clone https://github.com/interconnectit/Search-Replace-DB.git /opt/gridpane/Search-Replace-DB
    [[ $? -eq 0 && -f /opt/gridpane/Search-Replace-DB/srdb.cli.php ]] &&
    /bin/cp -rf /opt/gridpane/Search-Replace-DB /usr/local/bin/Search-Replace-DB
  fi
}

gridpane::grep_output::search_replace_db() {
  /bin/grep "Replacing .* with .* on .* tables with .* rows" <<<"$1"
  /bin/grep "changes were made" <<<"$1"
  /bin/grep "updates were actually made" <<<"$1"
}

gridpane::check::mu_symlink() {
  local site="$1"
  local mu_symlinked_file="$2"
  local old_file_symlinked_to="$3"
  local new_file_to_symlink_to="$4"
  local mu_plugins_list

  mu_plugins_list="$(ls -la /var/www/"${site}"/htdocs/wp-content/mu-plugins)"
  if [[ "${mu_plugins_list}" == *"${mu_symlinked_file}"* &&
        "${mu_plugins_list}" == *"${old_file_symlinked_to}"* ]]; then
    {
      echo "------------------------------------------------------------------"
      echo "Updating $site ${mu_symlinked_file} mu plugin symlink..."
      rm /var/www/"${site}"/htdocs/wp-content/mu-plugins/"${mu_symlinked_file}"
      [[ -f "${new_file_to_symlink_to}" ]] &&
        ln -s "${new_file_to_symlink_to}" /var/www/"${site}"/htdocs/wp-content/mu-plugins/"${mu_symlinked_file}"

      mu_plugins_list="$(ls -la /var/www/"${site}"/htdocs/wp-content/mu-plugins)"
      if [[ "${mu_plugins_list}" == *"${mu_symlinked_file}"* &&
            "${mu_plugins_list}" == *"${new_file_to_symlink_to}"* ]]; then
        ls -la /var/www/"${site}"/htdocs/wp-content/mu-plugins/"${mu_symlinked_file}"
      else
        echo "failed to create $site ${mu_symlinked_file} mu plugin symlink..."
      fi
    }
  fi
}

gridpane::check::wordfence_bork() {
  local new_domain="$1"
  local original_domain="$2"
  if [[ -d /var/www/"${new_domain}"/htdocs/wp-content/plugins/wordfence &&
        -f /var/www/"${new_domain}"/htdocs/wordfence-waf.php ]]; then
    {
      echo "------------------------------------------------------------------"
      echo "WordFence is installed ... reconfiguring things to stop any borking"
      local check_wf_ini
      check_wf_ini="$(/bin/grep wordfence-waf /var/www/"${new_domain}"/htdocs/.user.ini || true)"
      if [[ -n "$check_wf_ini" ]]; then
        /bin/echo "Updating .user.ini ..."
        /bin/sed -i "s|/var/www/${original_domain}|/var/www/${new_domain}|g" /var/www/"${new_domain}"/htdocs/.user.ini
      fi

      /bin/echo "Updating wordfence-waf.php ..."
      /bin/sed -i "s|/var/www/${original_domain}|/var/www/${new_domain}|g" /var/www/"${new_domain}"/htdocs/wordfence-waf.php
    }
  fi
}

gridpane::configure_site::redis_object() {
  local site="$1"
  local on_or_off_or_sync="$2"
  local siteowner
  siteowner=$(gridpane::get::set::site::user "${site}")

  local object_caching
  if [[ ${on_or_off_or_sync} != "sync"  ]]; then

    local object_cache_keys
    object_cache_keys=$(redis-cli --scan --pattern *"object_cache_${site}"*)
    local unlink
    # shellcheck disable=SC2086
    [[ -n ${object_cache_keys} ]] &&
      unlink=$(echo ${object_cache_keys} | xargs redis-cli unlink)

    if [[ ${on_or_off_or_sync} == "On" ||
          ${on_or_off_or_sync} == "on"  ]]; then
      sudo su - "${siteowner}" -c \
        "timeout 30 /usr/local/bin/wp cache flush --path=/var/www/${site}/htdocs 2>&1; \
        timeout 300 /usr/local/bin/wp plugin install https://github.com/gridpane/gridpane-redis-object-cache/archive/master.zip --activate --path=/var/www/${site}/htdocs 2>&1; \
        timeout 30 /usr/local/bin/wp redis update-dropin --path=/var/www/${site}/htdocs 2>&1; \
        timeout 30 /usr/local/bin/wp cache flush --path=/var/www/${site}/htdocs 2>&1 \
        timeout 30 /usr/local/bin/wp redis enable --path=/var/www/${site}/htdocs 2>&1; \
        timeout 30 /usr/local/bin/wp cache flush --path=/var/www/${site}/htdocs 2>&1"
    else
      sudo su - "${siteowner}" -c \
        "timeout 30 /usr/local/bin/wp cache flush --path=/var/www/${site}/htdocs 2>&1; \
        timeout 30 /usr/local/bin/wp redis disable --path=/var/www/${site}/htdocs 2>&1; \
        timeout 30 /usr/local/bin/wp cache flush --path=/var/www/${site}/htdocs 2>&1"
    fi
  fi

  check_object_caching_status=$(
    sudo su - "${siteowner}" -c "timeout 30 /usr/local/bin/wp redis status --path=/var/www/${site}/htdocs 2>&1;"
  )
  if [[ ${check_object_caching_status} == *"Status: Connected"* ]]; then
    object_caching="true"
  else
    object_caching="false"
  fi

  gridpane::conf_write redis-object-cache "${object_caching}" -site.env "${site}"

  gridpane::callback::app \
    "callback" \
    "/site/site-update" \
    "--" \
    "site_url=${site}" \
    "server_ip=${serverIP}" \
    "object_caching@${object_caching}" \
    "object_cache_settings_synced@true"
}

gridpane::sync::site_cron_settings() {
  local site="$1"
  local siteowner
  siteowner=$(gridpane::get::set::site::user "${site}")

  # shellcheck disable=SC2064
  trap "[[ -f /home/${siteowner}/sites/${nonce}.tempcron ]] && rm /home/${siteowner}/sites/${nonce}.tempcron;" EXIT

  sudo su - "${siteowner}" -c "crontab -l >/home/${siteowner}/sites/${nonce}.tempcron"
  current_site_owner_crontab="$(cat "/home/${siteowner}/sites/${nonce}.tempcron")"

  local cron
  local current_site_cron_freq

  if [[ ${current_site_owner_crontab} == *"//${site}/wp-cron.php"* ]]; then
    if [[ -f /var/www/${site}/logs/gp-sitecron.on ]]; then
      stored_site_cron_freq="$(cat "/var/www/${site}/logs/gp-sitecron.on")"
    fi
    current_site_cron=$(grep "//${site}/wp-cron.php" "/home/${siteowner}/sites/${nonce}.tempcron")

    if [[ ${stored_site_cron_freq} =~ ^[1-9]$ ]] ||
      [[ ${stored_site_cron_freq} =~ ^[1-5][0-9]$ ]] ||
      [[ ${stored_site_cron_freq} = "60" ]]; then
      if [[ ${stored_site_cron_freq} =~ ^[1-9]$ ]] ||
        [[ ${stored_site_cron_freq} =~ ^[1-5][0-9]$ ]]; then
          [[ $current_site_cron == "*/${stored_site_cron_freq} * * * * "* ]] &&
            current_site_cron_freq="${stored_site_cron_freq}"
      elif [[ ${stored_site_cron_freq} = "60" ]]; then
        [[ $current_site_cron == "${stored_site_cron_freq} * * * * "* ]] &&
            current_site_cron_freq="${stored_site_cron_freq}"
      fi
    fi

    if [[ -z ${current_site_cron_freq} ]]; then
      current_site_cron=${current_site_cron% * * * * wget -q -O - http:\/\/$site\/wp-cron.php?doing_wp_cron >\/dev\/null 2>&1}
      current_site_cron=${current_site_cron#\*}
      current_site_cron_freq=${current_site_cron#/}
    fi
    cron="true"
  else
    cron="false"
    current_site_cron_freq="0"
  fi

  gridpane::conf_write cron ${cron} -site.env "${site}"
  gridpane::conf_write cron-freq "${current_site_cron_freq}" -site.env "${site}"

  gridpane::callback::app \
    "callback" \
    "/site/site-update" \
    "--" \
    "site_url=${site}" \
    "server_ip=${serverIP}" \
    "server_cron_enabled@${cron}" \
    "server_cron_freq=${current_site_cron_freq}" \
    "server_cron_settings_synced@true"
}

gridpane::gettimedifference() {

  local time_1="$1"
  local time_2="$2"
  local time_increment="$3"
  local time_1_epoch
  local time_2_epoch
  local epoch_time_result
  local calculated_time_difference

  if [[ ${time_1} =~ [0-9]{10} ]]; then
    time_1_epoch="${time_1}"
  else
    time_1_epoch="$(date -d "${time_1}" +%s)"
  fi

  if [[ ${time_2} =~ [0-9]{10} ]]; then
    time_2_epoch="${time_2}"
  else
    time_2_epoch="$(date -d "${time_2}" +%s)"
  fi

  epoch_time_result=$((time_1_epoch - time_2_epoch))

  case ${time_increment} in
    months|monthly) calculated_time_difference=$(awk "BEGIN {print ((($epoch_time_result/3600)/24)/30)}") ;;
    weeks|weekly) calculated_time_difference=$(awk "BEGIN {print ((($epoch_time_result/3600)/24)/7)}") ;;
    days|daily) calculated_time_difference=$(awk "BEGIN {print (($epoch_time_result/3600)/24)}") ;;
    hours|hourly) calculated_time_difference=$(awk "BEGIN {print ($epoch_time_result/3600)}") ;;
    minutes) calculated_time_difference=$((epoch_time_result/60)) ;;
    seconds) calculated_time_difference=$((epoch_time_result)) ;;
  esac

  echo $calculated_time_difference
}
