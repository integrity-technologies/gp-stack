#!/bin/bash

#. gridpane.sh

#######################################
# Check if package is installed.
# Installs if not found.
#
# Accepted Args:
#  $1 deb apt package: string
# Outputs:
#  stdout
#######################################
workers::checkinstall::dep() {
  local current_time=$(date +%s)
  local last_apt_update=$(stat -c %Y /var/lib/apt/periodic/update-success-stamp)
  [[ -z ${last_apt_update} ]] && last_apt_update=0
  local time_since_last_apt_update=$((${current_time} - ${last_apt_update}))
  # Don't waste time running if it's ran in the last 5 days...
  if [[ ${time_since_last_apt_update} -gt 432000 ]]; then
    apt-get update
  fi

  if ! type $1 >/dev/null; then
    gridpane::preinstallaptcheck
    echo "Installing $1..."
    apt-get -y install "$1"
  fi
}

#######################################
# Runs a GPupdate at a random future point
#
# Accepted Args:
#  $1 Minutes to randomise from: int (required)
#  $2 force arg: force-scripts|force-configs|force-scripts-configs (optional)
# Outputs:
#  stdout
#######################################
workers::random::update() {
  if ! grep "gridpane-update.service" <<<"$(systemctl list-timers --all)"; then
    if [[ -f /usr/local/bin/gridpane-update.timer ]]; then
      cp /usr/local/bin/gridpane-update.timer /etc/systemd/system/gridpane-update.timer
      rm /usr/local/bin/gridpane-update.timer
      chmod 644 /etc/systemd/system/gridpane-update.timer
    fi
    if [[ -f /usr/local/bin/gridpane-update.service ]]; then
      cp /usr/local/bin/gridpane-update.service /etc/systemd/system/gridpane-update.service
      rm /usr/local/bin/gridpane-update.service
      chmod 644 /etc/systemd/system/gridpane-update.service
    fi
    if [[ -f /usr/local/bin/gridpane-daily-update.timer ]]; then
      cp /usr/local/bin/gridpane-daily-update.timer /etc/systemd/system/gridpane-daily-update.timer
      rm /usr/local/bin/gridpane-daily-update.timer
      chmod 644 /etc/systemd/system/gridpane-daily-update.timer
    fi
    if [[ -f /usr/local/bin/gridpane-daily-update.service ]]; then
      cp /usr/local/bin/gridpane-daily-update.service /etc/systemd/system/gridpane-daily-update.service
      rm /usr/local/bin/gridpane-daily-update.service
      chmod 644 /etc/systemd/system/gridpane-daily-update.service
    fi
    if [[ -f /etc/systemd/system/gridpane-update.timer &&
      -f /etc/systemd/system/gridpane-update.service ]] &&
    [[ -f /etc/systemd/system/gridpane-daily-update.timer &&
      -f /etc/systemd/system/gridpane-daily-update.service ]]; then
      systemctl start gridpane-update.timer
      systemctl start gridpane-daily-update.timer
      /bin/systemctl daemon-reload
    else
      update_minute=$((RANDOM % $1))
      echo "/usr/local/bin/gpupdate $2" | at now + ${update_minute} minute
    fi
  fi

}

#######################################
# Exports Path
# /usr/local/bin:/usr/bin:/bin
#
# Accepted Args:
#  $1 Path to append: :${string} (optional)
#######################################
workers::exportpath() {
  [[ ! -z $1 ]] && additional_path=":$1"
  export PATH="/usr/local/bin:/usr/bin:/bin:/snap/bin${additional_path}"
}

workers::check::facl() {
  setfacl -bn /tmp
  setfacl -m g:GridPane-System-Users:x /var/.gridpane
  setfacl -m g:GridPane-System-Users:x /var/log
  setfacl -x group:GridPane-System-Users /bin
  setfacl -x group:GridPane-System-Users /dev
}

workers::cleanup:varwww() {
  [[ -f '--timeout=30' ]] && rm -v -- '--timeout=30'
  [[ -d /var/www/logs ]] && rm -rf /var/www/logs
  [[ -f /var/www/database.gz ]] && rm /var/www/database.gz
}

workers::failsync_pause() {
  local current_worker_pause
  current_worker_pause=$(grep -w "gpfailsync-server-worker-pause:.*" /root/gridenv/promethean.env)
  # shellcheck disable=SC2181
  if [[ $? == 0 ]]; then

    current_worker_pause=$(echo "$current_worker_pause" | cut -f 2 -d ':')

    if [[ "$current_worker_pause" == "true" ]]; then
      current_worker_pause=$(date +%s)
      echo "Old worker pause true value found, updating to epoch ${current_worker_pause}" | tee -a /var/log/gridpane.log
      gridpane::conf_write gpfailsync-server-worker-pause "${current_worker_pause}"
    fi

    local time_diff_unit
    time_diff_unit="days"
    [[ -f /root/worker.failsync.pause.test.remote ]] &&
      time_diff_unit=$(cat /root/worker.failsync.pause.test.remote)

    local refresh_time_diff
    refresh_time_diff=$(gridpane::gettimedifference "now" "${current_worker_pause}" "${time_diff_unit}")

    if (($(awk "BEGIN {print ("${refresh_time_diff}" > "0.99" )}"))); then
      echo "Server to server sync pause detected... but it's over a ${time_diff_unit} old... proceeding with gpworker..." | tee -a /var/log/gridpane.log
      gridpane::conf_delete gpfailsync-server-worker-pause
      gridpane::conf_write gpfailsync-running false
    else
      echo "Server to server sync pause detected... skipping this gpworker cycle..." | tee -a /var/log/gridpane.log
      exit 187
    fi
  fi
}

workers::check::wpconfig_bups() {
  local entry="$1"
  if [[ ! -f /opt/gridpane/site-configs/${entry}-wp-config-immutable.BUP ]]; then
    [[ ! -d /opt/gridpane/site-configs ]] &&
      mkdir /opt/gridpane/site-configs
    cp "/var/www/${entry}/wp-config.php" "/opt/gridpane/site-configs/${entry}-wp-config-immutable.BUP"
    chattr +i "/opt/gridpane/site-configs/${entry}-wp-config-immutable.BUP" # This is now protected by magic.
    echo "Missing Immutable ${entry} WP Config Backup File ADDED..."
  fi
}

workers::check::site_perms() {
  local entry="$1"
  if [[ -n ${siteowner} &&
      -z ${multitenancy_status} ]]; then
    local dirowner
    dirowner=$(stat -c '%U' /var/www/"${entry}")
    if [[ ${siteowner} != "${dirowner}" ]]; then
      echo "Site /var/www/${entry} belonged to incorrect ${dirowner} owner - correcting with correct PHP owner ${siteowner}..."
      chattr -R -i "/var/www/${entry}"
      chown -R "${siteowner}":"${siteowner}" "/var/www/${entry}"
      [[ -d /var/www/${entry}/modsec/audit-logs/audit ]] &&
        chown -R www-data:www-data "/var/www/${entry}/modsec/audit-logs/audit"
      [[ -f /var/www/${entry}/logs/${entry}.env ]] &&
        chattr +i "/var/www/${entry}/logs/${entry}.env"
      [[ -f /var/www/${entry}/logs/${entry}-additional-domains.env ]] &&
        chattr +i "/var/www/${entry}/logs/${entry}-additional-domains.env"
      [[ -f /var/www/${entry}/logs/${entry}-301-domains.env ]] &&
        chattr +i "/var/www/${entry}/logs/${entry}-301-domains.env"
    fi

    local htdocsowner
    htdocsowner=$(stat -c '%U' /var/www/"${entry}"/htdocs)
    if [[ ${siteowner} != "${htdocsowner}" ]]; then
      echo "Site /var/www/${entry}/htdocs belonged to incorrect ${htdocsowner} owner - correcting with correct PHP owner ${siteowner}..."
      find "/var/www/${entry}/htdocs" -not -user "${siteowner}" -exec chown "${siteowner}": {} \;
    fi

    if [[ -d /var/www/${entry}/modsec/audit-logs/audit ]]; then
      log_dirowner=$(stat -c '%U' "/var/www/${entry}/modsec/audit-logs/audit")
      [[ ${log_dirowner} != *"www-data"* ]] &&
        find "/var/www/${entry}/modsec/audit-logs/audit" -not -user www-data -exec chown www-data: {} \;
    fi

    if [[ -d /home/${siteowner} &&
          ! -d /home/${siteowner}/.gnupg ]]; then
      mkdir -p "/home/${siteowner}/.gnupg"
      chown -R "${siteowner}":"${siteowner}" "/home/${siteowner}/.gnupg"
    fi
  fi

  find "/var/www/${entry}/htdocs" -not -perm 755 -type d -exec chmod 755 {} \;
  find "/var/www/${entry}/htdocs" -not -perm 644 -type f -exec chmod 644 {} \;
  find "/var/www/${entry}/dns" -mindepth 1 -not -user root -exec chown root: {} \;
  find "/var/www/${entry}/dns" -mindepth 1 -not -perm 640 -type f -exec chmod 640 {} \;
  if [[ "$webserver" = nginx ]]; then
    find "/var/www/${entry}/nginx" -mindepth 1 -not -user root -exec chown root: {} \;
    find "/var/www/${entry}/nginx" -mindepth 1 -not -perm 644 -type f -exec chmod 644 {} \;
  fi
  if [[ "$webserver" = openlitespeed ]]; then
    find "/var/www/${entry}/ols" -mindepth 1 -not -user root -exec chown root: {} \;
    find "/var/www/${entry}/ols" -mindepth 1 -not -perm 644 -type f -exec chmod 644 {} \;
  fi
  find "/var/www/${entry}/modsec" -mindepth 1 -not -user root -exec chown root: {} \;
  find "/var/www/${entry}/modsec" -mindepth 1 -not -perm 644 -type f -exec chmod 644 {} \;
}

workers::check:wp_mail_user() {
  local entry="$1"
  if [[ -z $site_email && -n $site_user ]]; then
    local user_email
    user_email=$(
      sudo su - "${siteowner}" -c "timeout 60 /usr/local/bin/wp user get ${site_user} --field=email --path=/var/www/${entry}/htdocs 2>&1"
    )
    user_email=$(sed -n -e '$p' <<<"${user_email}")
    echo "${entry} ${site_user} ${user_email} | ${entry}.env"
    gridpane::conf_write wp-email "${user_email}" -site.env "${entry}"
  fi
}

workers::check:site_crons() {
  local entry="$1"
  if [[ -f /var/www/${entry}/logs/gp-sitecron.on ]]; then
    local current_siteowner_cron
    current_siteowner_cron=$(sudo su - "${siteowner}" -c "crontab -l")
    if [[ ${current_siteowner_cron} != *"//${entry}/wp-cron.php"* ]]; then
      local token_freq
      token_freq=$(cat "/var/www/${entry}/logs/gp-sitecron.on")
      if [[ ${token_freq} =~ ^[1-9]$ ]] ||
        [[ ${token_freq} =~ ^[1-5][0-9]$ ]] ||
        [[ ${token_freq} = "60" ]]; then
        token_freq="*/${token_freq}"
      else
        token_freq="5"
        echo "${token_freq}" >"/var/www/${entry}/logs/gp-sitecron.on"
        gridpane::conf_write cron-freq "${token_freq}" -site.env "${entry}"
        token_freq="*/5"
      fi
      echo "Repair | Adding to server cron:"
      # shellcheck disable=SC2154
      echo "${token_freq} * * * * wget -q -O - http://${entry}/wp-cron.php?doing_wp_cron >/dev/null 2>&1 >> /home/${siteowner}/sites/${nonce}.tempcron"
      sudo su - "${siteowner}" -c "crontab -l > /home/${siteowner}/sites/${nonce}.tempcron"
      echo "${token_freq} * * * * wget -q -O - http://${entry}/wp-cron.php?doing_wp_cron >/dev/null 2>&1" >>"/home/${siteowner}/sites/${nonce}.tempcron"
      sudo su - "${siteowner}" -c "crontab /home/${siteowner}/sites/${nonce}.tempcron"
      # TODO trap remove any files created
      rm "/home/${siteowner}/sites/${nonce}.tempcron"
    fi
  fi
}

workers::check::domains_and_creds() {
  local site="$1"
  [[ -f /var/www/${site}/dns/${site}.creds ]] &&
    chown root:root "/var/www/${site}/dns/${site}.creds" &&
    chmod 640 "/var/www/${site}/dns/${site}.creds"
}

workers::check::site_user_configs() {
  local site_to_check_user_configs="$1"
  if [[ ! -f /var/www/${site_to_check_user_configs}/user-configs.php ]]; then
    echo "No user configs detected for ${site_to_check_user_configs}, adding..."
    cat >"/var/www/${site_to_check_user_configs}/user-configs.php" <<EOF
<?php
/**
 * This file is included in the wp-config.php file.
 * Users can add their own WordPress configurations here
 */
EOF
  fi
  if [[ -f /var/www/${site_to_check_user_configs}/user-configs.php ]]; then
    if [[ "$(cat "/var/www/${site_to_check_user_configs}/wp-config.php")" != *"user-configs.php';"* ]]; then
      echo "No user configs include detected for ${site_to_check_user_configs} in wp-config.php, adding..."
      sed -i "/wp-fail2ban-configs.php/a include __DIR__ . '/user-configs.php';" "/var/www/${site_to_check_user_configs}/wp-config.php"
      sed -i "/wp-fail2ban-configs.php/a /* GridPane WP-config User includes */" "/var/www/${site_to_check_user_configs}/wp-config.php"
      sed -i "/wp-fail2ban-configs.php/G" "/var/www/${site_to_check_user_configs}/wp-config.php"
    fi
  fi
}

workers::regenerate::site_virtual_servers() {
  local site="$1"
  local conf_type
  local cache_type
  local generate_conf

  # shellcheck disable=SC2154
  echo "Regenerating ${site} $webserver_formatted virtual server"

  if [[ "$webserver" = nginx ]]; then
    conf_type=$(/usr/local/bin/gp conf nginx type-check "${site}" -q)
    cache_type=$(/usr/local/bin/gp conf nginx cache-check "${site}" -q)
    generate_conf=$(/usr/local/bin/gp conf nginx generate "${conf_type}" "${cache_type}" "${site}")
  else
    generate_conf="$(/usr/local/bin/gpols site "$site" --no-restart)"
  fi

  if [[ -f /var/www/${site}/logs/${site}-additional-domains.env ]]; then
    while read alias_domain; do
      alias_domain=${alias_domain#:}
      alias_domain=${alias_domain%:}
      echo "Regenerating ${alias_domain} $webserver_formatted virtual server"

      if [[ "$webserver" = nginx ]]; then
        alias_conf_type=$(/usr/local/bin/gp conf nginx type-check "${alias_domain}" -q)
        alias_cache_type=$(/usr/local/bin/gp conf nginx cache-check "${alias_domain}" -q)
        alias_generate_conf=$(/usr/local/bin/gp conf nginx generate "${alias_conf_type}" "${alias_cache_type}" "${site}" "${alias_domain}")
      else
        alias_generate_conf="$(/usr/local/bin/gpols addon "$alias_domain" --no-restart)"
      fi
    done <"/var/www/${site}/logs/${site}-additional-domains.env"
  fi

  if [[ "$webserver" = openlitespeed ]]; then
    /usr/local/bin/gpols httpd
  fi
}

workers::check::swapped_sites::ssl_renewals() {
  local site_to_check_renewals="$1"
  if [[ -n ${original_domain} ]]; then
    if [[ -f /etc/letsencrypt/renewal/${site_to_check_renewals}.conf ]]; then
      local conf_content
      conf_content=$(cat "/etc/letsencrypt/renewal/${site_to_check_renewals}.conf")
      if [[ ${conf_content} == *"= /var/www/${original_domain}/htdocs"* ]]; then
        echo "------------------------------------------------------------------"
        echo "original domain: ${original_domain}"
        echo "------------------------------------------------------------------"
        echo "Updating Cerbot webroot /etc/letsencrypt/renewal/${site_to_check_renewals}.conf:"
        echo "= /var/www/${site_to_check_renewals}/htdocs"
        sed -i "s/= \/var\/www\/${original_domain}\/htdocs/= \/var\/www\/${site_to_check_renewals}\/htdocs/g" \
          "/etc/letsencrypt/renewal/${site_to_check_renewals}.conf"
        echo "------------------------------------------------------------------"
      fi
    fi

    if [[ -f /root/gridenv/acme-wildcard-configs/${site_to_check_renewals}/${site_to_check_renewals}/${site_to_check_renewals}.conf ]]; then
      local acme_conf_content
      acme_conf_content=$(cat "/root/gridenv/acme-wildcard-configs/${site_to_check_renewals}/${site_to_check_renewals}/${site_to_check_renewals}.conf")
      if [[ ${acme_conf_content} == *"Le_Webroot='/var/www/${original_domain}/htdocs'"* ]]; then
        echo "------------------------------------------------------------------"
        echo "original domain: ${original_domain}"
        echo "------------------------------------------------------------------"
        echo "Updating Acme Primary webroot"
        echo "/root/gridenv/acme-wildcard-configs/${site_to_check_renewals}/${site_to_check_renewals}/${site_to_check_renewals}.conf:"
        echo "Le_Webroot='/var/www/${site_to_check_renewals}/htdocs'"
        sed -i "s/Le_Webroot='\/var\/www\/${original_domain}\/htdocs'/Le_Webroot='\/var\/www\/${site_to_check_renewals}\/htdocs'/g" \
          "/root/gridenv/acme-wildcard-configs/${site_to_check_renewals}/${site_to_check_renewals}/${site_to_check_renewals}.conf"
        echo "------------------------------------------------------------------"
      fi
    fi

    if [[ -f /var/www/${site_to_check_renewals}/logs/${site_to_check_renewals}-additional-domains.env ]]; then
      while read add_on_domain; do
        add_on_domain=${add_on_domain#:}
        add_on_domain=${add_on_domain%:}
        if [[ -f /etc/letsencrypt/renewal/${add_on_domain}.conf ]]; then
          local conf_content
          conf_content=$(cat "/etc/letsencrypt/renewal/${add_on_domain}.conf")
          if [[ ${conf_content} == *"= /var/www/${original_domain}/htdocs"* ]]; then
            echo "------------------------------------------------------------------"
            echo "Updating Cerbot webroot /etc/letsencrypt/renewal/${add_on_domain}.conf:"
            echo "= /var/www/${site_to_check_renewals}/htdocs"
            sed -i "s/= \/var\/www\/${original_domain}\/htdocs/= \/var\/www\/${site_to_check_renewals}\/htdocs/g" \
              "/etc/letsencrypt/renewal/${add_on_domain}.conf"
            echo "------------------------------------------------------------------"
          fi
        fi

        [[ ! -f /root/gridenv/acme-wildcard-configs/${add_on_domain}/${add_on_domain}/${add_on_domain}.conf ]] &&
          continue

        local acme_add_on_conf_content
        acme_add_on_conf_content=$(cat "/root/gridenv/acme-wildcard-configs/${add_on_domain}/${add_on_domain}/${add_on_domain}.conf")
        if [[ ${acme_add_on_conf_content} == *"Le_Webroot='/var/www/${original_domain}/htdocs'"* ]]; then
          echo "------------------------------------------------------------------"
          echo "Updating Acme Alias webroot"
          echo "/root/gridenv/acme-wildcard-configs/${add_on_domain}/${add_on_domain}/${add_on_domain}.conf:"
          echo "Le_Webroot='/var/www/${site_to_check_renewals}/htdocs'"
          sed -i "s/Le_Webroot='\/var\/www\/${original_domain}\/htdocs'/Le_Webroot='\/var\/www\/${site_to_check_renewals}\/htdocs'/g" \
            "/root/gridenv/acme-wildcard-configs/${add_on_domain}/${add_on_domain}/${add_on_domain}.conf"
          echo "------------------------------------------------------------------"
        fi

      done <"/var/www/${site_to_check_renewals}/logs/${site_to_check_renewals}-additional-domains.env"
    fi

    if [[ -f /var/www/${site_to_check_renewals}/logs/${site_to_check_renewals}-301-domains.env ]]; then
      while read redirect_domain; do
        redirect_domain=${redirect_domain#:}
        redirect_domain=${redirect_domain%:}
        if [[ -f /etc/letsencrypt/renewal/${redirect_domain}.conf ]]; then
          local conf_content
          conf_content=$(cat "/etc/letsencrypt/renewal/${redirect_domain}.conf")
          if [[ ${conf_content} == *"= /var/www/${original_domain}/htdocs"* ]]; then
            echo "------------------------------------------------------------------"
            echo "Updating Cerbot webroot /etc/letsencrypt/renewal/${redirect_domain}.conf:"
            echo "= /var/www/${site_to_check_renewals}/htdocs"
            sed -i "s/= \/var\/www\/${original_domain}\/htdocs/= \/var\/www\/${site_to_check_renewals}\/htdocs/g" \
              "/etc/letsencrypt/renewal/${redirect_domain}.conf"
            echo "------------------------------------------------------------------"
          fi
        fi

        [[ ! -f /root/gridenv/acme-wildcard-configs/${redirect_domain}/${redirect_domain}/${redirect_domain}.conf ]] &&
          continue

        local acme_add_on_conf_content
        acme_add_on_conf_content=$(cat "/root/gridenv/acme-wildcard-configs/${redirect_domain}/${redirect_domain}/${redirect_domain}.conf")
        if [[ ${acme_add_on_conf_content} == *"Le_Webroot='/var/www/${original_domain}/htdocs'"* ]]; then
          echo "------------------------------------------------------------------"
          echo "Updating Acme Redirect webroot"
          echo "/root/gridenv/acme-wildcard-configs/${redirect_domain}/${redirect_domain}/${redirect_domain}.conf:"
          echo "Le_Webroot='\/var\/www\/${site_to_check_renewals}\/htdocs'"
          sed -i "s/Le_Webroot='\/var\/www\/${original_domain}\/htdocs'/Le_Webroot='\/var\/www\/${site_to_check_renewals}\/htdocs'/g" \
            "/root/gridenv/acme-wildcard-configs/${redirect_domain}/${redirect_domain}/${redirect_domain}.conf"
          echo "------------------------------------------------------------------"
        fi
      done <"/var/www/${site_to_check_renewals}/logs/${site_to_check_renewals}-301-domains.env"
    fi
  fi
}

workers::check::site_tokens() {
  local site="$1"
  local site_server_conf
  if [[ ${site_virtual_server} == *"PROXY"* ||
        "$site_virtual_server" == *"module mod_security"* ]]; then
    echo "modsec" >"/var/www/${site}/logs/waf.env"
  elif [[ "$webserver" = nginx && ${site_virtual_server} != *"#include /etc/nginx/common/${site}-7g.conf"* ||
          "$webserver" = nginx && ${site_virtual_server} != *"#include /etc/nginx/common/${site}-6g.conf"* ]]; then
    if [[ ${site_virtual_server} != *"#include /etc/nginx/common/${site}-6g.conf"* ]] &&
      [[ ${site_virtual_server} == *"include /etc/nginx/common/${site}-6g.conf"* ]]; then
      echo "6G" >"/var/www/${site}/logs/waf.env"
    elif [[ ${site_virtual_server} != *"#include /etc/nginx/common/${site}-7g.conf"* ]] &&
      [[ ${site_virtual_server} == *"include /etc/nginx/common/${site}-7g.conf"* ]]; then
      echo "7G" >"/var/www/${site}/logs/waf.env"
    else
      if [[ ${site_virtual_server} != *"#include /etc/nginx/common/7g.conf"* ]] ||
        [[ ${site_virtual_server} != *"#include /etc/nginx/common/6g.conf"* ]]; then
        if [[ ${site_virtual_server} != *"#include /etc/nginx/common/6g.conf"* ]] &&
          [[ ${site_virtual_server} == *"include /etc/nginx/common/6g.conf"* ]]; then
          echo "6G" >"/var/www/${site}/logs/waf.env"
        elif [[ ${site_virtual_server} != *"#include /etc/nginx/common/7g.conf"* ]] &&
          [[ ${site_virtual_server} == *"include /etc/nginx/common/7g.conf"* ]]; then
          echo "7G" >"/var/www/${site}/logs/waf.env"
        else
          [[ -f /var/www/${site}/logs/waf.env ]] &&
            rm "/var/www/${site}/logs/waf.env"
        fi
      fi
    fi
  elif [[ "$webserver" = openlitespeed && "$site_virtual_server" == *"# 7G:[CORE]"* ]]; then
    /bin/echo "7G" > "/var/www/${site}/logs/waf.env"
  else
    [[ -f /var/www/${site}/logs/waf.env ]] &&
      rm "/var/www/${site}/logs/waf.env"
  fi

  if [[ "$webserver" = nginx && ${site_virtual_server} == *"#include /etc/nginx/common/acl.conf"* ||
        "$webserver" = openlitespeed && "$site_virtual_server" != *"realm Basic Auth"* ]]; then
    [[ -f /var/www/${site}/logs/http.auth.env ]] &&
      rm "/var/www/${site}/logs/http.auth.env"
  else
    echo "true" >"/var/www/${site}/logs/http.auth.env"
  fi

  if [[ "$webserver" = nginx ]]; then
    if [[ ${site_virtual_server} == *"#include /var/www/${site}/nginx/${site}-headers-csp.conf;"* ]]; then
      [[ -f /var/www/${site}/logs/csp.headers.env ]] &&
        rm "/var/www/${site}/logs/csp.headers.env"
    else
      echo "true" >"/var/www/${site}/logs/csp.headers.env"
    fi

    site_wpcommon="$(cat /etc/nginx/common/${site}-wpcommon.conf)"
    site_server_conf=nginx
  else
    site_server_conf=ols
  fi

  if [[ "$webserver" = nginx && ${site_wpcommon} == *"include /etc/nginx/common/disable-xmlrpc.conf;"* ]]; then
    if [[ ${site_wpcommon} != *"#include /etc/nginx/common/disable-xmlrpc.conf;"* ]]; then
      echo "true" >"/var/www/${site}/logs/disable.xmlrpc.env"
    fi
  elif [[ -f /var/www/${site}/"$site_server_conf"/disable-xmlrpc-main-context.conf ]]; then
    echo "true" >"/var/www/${site}/logs/disable.xmlrpc.env"
  else
    [[ -f /var/www/${site}/logs/disable.xmlrpc.env ]] &&
      rm "/var/www/${site}/logs/disable.xmlrpc.env"
  fi

  if [[ "$webserver" = nginx && ${site_wpcommon} == *"include /etc/nginx/common/disable-concat.conf;"* ]]; then
    if [[ ${site_wpcommon} != *"#include /etc/nginx/common/disable-concat.conf;"* ]]; then
      echo "true" >"/var/www/${site}/logs/disable.wpadmin.concatenation.env"
    fi
  elif [[ -f /var/www/${site}/"$site_server_conf"/disable-load-scripts-concatenation-main-context.conf ]]; then
    echo "true" >"/var/www/${site}/logs/disable.wpadmin.concatenation.env"
  else
    [[ -f /var/www/${site}/logs/disable.wpadmin.concatenation.env ]] &&
      rm "/var/www/${site}/logs/disable.wpadmin.concatenation.env"
  fi

  if [[ "$webserver" = nginx && ${site_wpcommon} == *"include /etc/nginx/common/disable-content-php.conf;"* ]]; then
    if [[ ${site_wpcommon} != *"#include /etc/nginx/common/disable-content-php.conf;"* ]]; then
      echo "true" >"/var/www/${site}/logs/disable.wpcontent.php.env"
    fi
  elif [[ -f /var/www/${site}/"$site_server_conf"/disable-wp-content-php-main-context.conf ]]; then
    echo "true" >"/var/www/${site}/logs/disable.wpcontent.php.env"
  else
    [[ -f /var/www/${site}/logs/disable.wpcontent.php.env ]] &&
      rm "/var/www/${site}/logs/disable.wpcontent.php.env"
  fi

  if [[ "$webserver" = nginx && ${site_wpcommon} == *"include /etc/nginx/common/disable-comments.conf;"* ]]; then
    if [[ ${site_wpcommon} != *"#include /etc/nginx/common/disable-comments.conf;"* ]]; then
      echo "true" >"/var/www/${site}/logs/disable.wp.comments.post.env"
    fi
  elif [[ -f /var/www/${site}/"$site_server_conf"/disable-wp-comments-post-main-context.conf ]]; then
    echo "true" >"/var/www/${site}/logs/disable.wp.comments.post.env"
  else
    [[ -f /var/www/${site}/logs/disable.wp.comments.post.env ]] &&
      rm "/var/www/${site}/logs/disable.wp.comments.post.env"
  fi

  if [[ "$webserver" = nginx && ${site_wpcommon} == *"include /etc/nginx/common/disable-links-opml.conf;"* ]]; then
    if [[ ${site_wpcommon} != *"#include /etc/nginx/common/disable-links-opml.conf;"* ]]; then
      echo "true" >"/var/www/${site}/logs/disable.wp.links.opml.env"
    fi
  elif [[ -f /var/www/${site}/"$site_server_conf"/disable-wp-links-opml-main-context.conf ]]; then
    echo "true" >"/var/www/${site}/logs/disable.wp.links.opml.env"
  else
    [[ -f /var/www/${site}/logs/disable.wp.links.opml.env ]] &&
      rm "/var/www/${site}/logs/disable.wp.links.opml.env"
  fi

  if [[ "$webserver" = nginx && ${site_wpcommon} == *"include /etc/nginx/common/disable-trackbacks.conf;"* ]]; then
    if [[ ${site_wpcommon} != *"#include /etc/nginx/common/disable-trackbacks.conf;"* ]]; then
      echo "true" >"/var/www/${site}/logs/disable.wp.trackbacks.env"
    fi
  elif [[ -f /var/www/${site}/"$site_server_conf"/disable-wp-trackbacks-main-context.conf ]]; then
    echo "true" >"/var/www/${site}/logs/disable.wp.trackbacks.env"
  else
    [[ -f /var/www/${site}/logs/disable.wp.trackbacks.env ]] &&
      rm "/var/www/${site}/logs/disable.wp.trackbacks.env"
  fi

  if [[ -f /var/www/${site}/"$site_server_conf"/block-install-file-main-context.conf ]]; then
    echo "true" >"/var/www/${site}/logs/disable.wpadmin.install.env"
  else
    [[ -f /var/www/${site}/logs/disable.wpadmin.install.env ]] &&
      rm "/var/www/${site}/logs/disable.wpadmin.install.env"
  fi

  if [[ -f /var/www/${site}/"$site_server_conf"/block-upgrade-file-main-context.conf ]]; then
    echo "true" >"/var/www/${site}/logs/disable.wpadmin.upgrade.env"
  else
    [[ -f /var/www/${site}/logs/disable.wpadmin.upgrade.env ]] &&
      rm "/var/www/${site}/logs/disable.wpadmin.upgrade.env"
  fi
}

workers::cleanup::site_envs() {
  for env in /var/www/"${1}"/htdocs/*.env.*
  do
    [ -e "$env" ] && rm "$env"
  done
}

workers::check::site_failsync() {
  local site="$1"
  if [[ ${gpfailsync_role} == "source" &&
        -z ${gpfailsync_destination} &&
        -f /var/www/"${site}"/logs/synced-to.log ]]; then
    local remote_ip
    remote_ip=$(cat /var/www/"${site}"/logs/synced-to.log)
    if [[ ${remote_ip} =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ &&
          -n ${remote_ip} ]]; then
       gridpane::conf_write gpfailsync-destination "${remote_ip}"
    fi
  fi
}

workers::check::nginx_modsec_rules() {
  if [[ "$webserver" = nginx ]]; then
    local site="$1"
    if [[ $site_virtual_server == *"PROXY"* ]]; then
      if [[ ! -f /etc/nginx/modsec/"${site}"-main.conf ]]; then
        echo "---------------------------------------------------------------------------"
        echo "Missing /etc/nginx/modsec/${site}-main.conf"
        echo "Generating..."

        cp /etc/nginx/modsec/main.conf /etc/nginx/modsec/"${site}"-main.conf
        sed -i "s/Include \/etc\/nginx\/modsec\/owasp\/crs-setup.conf/Include \/var\/www\/${site}\/modsec\/${site}-crs-setup.conf/g" \
          /etc/nginx/modsec/"${site}"-main.conf
        sed -i "s/Include \/etc\/nginx\/modsec\/owasp\/crs-paranoia-level.conf/Include \/var\/www\/${site}\/modsec\/${site}-crs-paranoia-level.conf/g" \
          /etc/nginx/modsec/"${site}"-main.conf
        sed -i "s/Include \/etc\/nginx\/modsec\/owasp\/crs-anomaly-blocking-threshold.conf/Include \/var\/www\/${site}\/modsec\/${site}-crs-anomaly-blocking-threshold.conf/g" \
          /etc/nginx/modsec/"${site}"-main.conf
        sed -i "s/SecAuditLog \/var\/log\/modsec_audit.log/SecAuditLog \/var\/www\/${site}\/modsec\/modsec_audit.log/g" \
          /etc/nginx/modsec/"${site}"-main.conf
        sed -i "/#SecAuditLogStorageDir/d" /etc/nginx/modsec/"${site}"-main.conf
        sed -i "/modsec_audit.log/a SecAuditLogStorageDir \/var\/www\/${site}\/modsec\/audit-logs\/audit" /etc/nginx/modsec/"${site}"-main.conf
        [[ ! -f /var/www/"${site}"/modsec/"${site}"-runtime-whitelist.conf ]] &&
          cp /etc/nginx/modsec/runtime-whitelist.conf /var/www/"${site}"/modsec/"${site}"-runtime-whitelist.conf
        sed -i "s/Include \/etc\/nginx\/modsec\/owasp\/runtime-whitelist.conf/Include \/var\/www\/${site}\/modsec\/${site}-runtime-whitelist.conf/g" \
          /etc/nginx/modsec/"${site}"-main.conf
        [[ ! -f /var/www/"${site}"/modsec/"${site}"-runtime-exceptions.conf ]] &&
          cp /etc/nginx/modsec/runtime-exceptions.conf /var/www/"${site}"/modsec/"${site}"-runtime-exceptions.conf
        sed -i "s/Include \/etc\/nginx\/modsec\/owasp\/runtime-exceptions.conf/Include \/var\/www\/${site}\/modsec\/${site}-runtime-exceptions.conf/g" \
          /etc/nginx/modsec/"${site}"-main.conf

        ls -l /etc/nginx/modsec/"${site}"-main.conf
        grep "${site}-crs-setup.conf" /etc/nginx/modsec/"${site}"-main.conf
        grep "${site}-crs-paranoia-level.conf" /etc/nginx/modsec/"${site}"-main.conf
        grep "${site}-crs-anomaly-blocking-threshold.conf" /etc/nginx/modsec/"${site}"-main.conf
        grep "modsec_audit.log" /etc/nginx/modsec/"${site}"-main.conf
        grep "SecAuditLogStorageDir" /etc/nginx/modsec/"${site}"-main.conf
        grep "${site}-runtime-whitelist.conf" /etc/nginx/modsec/"${site}"-main.conf
        grep "${site}-runtime-exceptions.conf" /etc/nginx/modsec/"${site}"-main.conf
        echo "---------------------------------------------------------------------------"
      fi

      if [[ "$(cat /etc/nginx/common/"${site}"-php.conf)" == *"#modsecurity_rules_file /etc/nginx/modsec/main.conf;"* &&
            -f /etc/nginx/modsec/"${site}"-main.conf ]]; then
        echo "---------------------------------------------------------------------------"
        echo "Misconfigured /etc/nginx/common/"${site}"-php.conf detected"
        echo "reconfiguring:"
        sed -i "s/#modsecurity_rules_file \/etc\/nginx\/modsec\/main.conf;/modsecurity_rules_file \/etc\/nginx\/modsec\/${site}-main.conf;/g" \
          /etc/nginx/common/"${site}"-php.conf
        cat /etc/nginx/common/"${site}"-php.conf
        echo "---------------------------------------------------------------------------"
      fi
    fi
  fi
}

workers::set_globals() {
  monit_url=$(grep -w "monit-url:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  phpma_url=$(grep -w "phpma-url:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  gpfailsync_role=$(grep -w "gpfailsync-role:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  gpfailsync_destination=$(grep -w "gpfailsync-destination:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  multitenancy_status=$(grep -w "multitenancy:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  backup_version=$(grep -w "backups-version:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
}

workers::check::site_ultimo() {
  local site="$1"
  local check_application_token
  check_application_token=$(grep -w "application-token:.*" "/var/www/${site}/logs/${site}.env" | cut -f 2 -d ':') || true
  if [[ -z ${check_application_token} &&
    -f /var/www/${site}/gp-ultimo-configs.php ]]; then
    update_minute=$((RANDOM % 15))
    wait_seconds=$((RANDOM % 60))
    # shellcheck disable=SC2154
    echo "${update_minute} and $wait_seconds - ${site}"
    echo "sleep $wait_seconds; /usr/local/bin/gp callback /site/check-ultimo site_url=${site} server_ip=${serverIP}" | at now + ${update_minute} minute
  fi

  if [[ -n ${check_application_token} &&
    -f /var/www/${site}/gp-ultimo-configs.php ]]; then
    if ! grep "define('WU_GRIDPANE_API_KEY', '${check_application_token}" "/var/www/${site}/gp-ultimo-configs.php" >/dev/null; then
      /usr/local/bin/gp "${site}" -gp-ultimo-on "${check_application_token}"
    fi
  fi
}

workers::check::site_additional_includes() {
  local site="$1"

  if [[ -f /var/www/${site}/nginx/disable-xmlrpc-main-context.conf ]]; then
    if ! grep "-before-disable-xmlrpc-main-context.conf" "/var/www/${site}/nginx/disable-xmlrpc-main-context.conf" >/dev/null; then
      cat >"/var/www/${site}/nginx/disable-xmlrpc-main-context.conf" <<EOF
# #
# GridPane Disable XLMRPC
# Disable xmlrpc to avoid DDoS attacks
# Version 1.2.0
# #
include /var/www/${site}/nginx/*-before-disable-xmlrpc-main-context.conf;

fastcgi_hide_header X-Pingback;
proxy_hide_header X-Pingback;

location = /xmlrpc.php {
    deny all;
}
EOF
    fi
  fi

  if [[ -f /var/www/${site}/nginx/disable-load-scripts-concatenation-main-context.conf ]]; then
    if ! grep "-before-disable-load-scripts-concatenation-main-context.conf" "/var/www/${site}/nginx/disable-load-scripts-concatenation-main-context.conf" >/dev/null; then
      cat >"/var/www/${site}/nginx/disable-load-scripts-concatenation-main-context.conf" <<EOF
# #
# GridPane Disable WordPress Concatenation files
# - load-scripts.php
# - load-styles.php
# Can enable when site has SSL/HTTP2 enabled.
# Mitigates CVE-2018-6389 – the load-scripts.php DoS
# Please see:
# https://baraktawily.blogspot.com/2018/02/how-to-dos-29-of-world-wide-websites.html
# https://bjornjohansen.no/load-scripts-php
# Really no need to concatenate anyway when using HTTP2 multiplexing
# This is our opinion and we are opinionated.
# Version 1.2.0
# #
include /var/www/${site}/nginx/*-before-disable-load-scripts-concatenation-main-context.conf;

location = /wp-admin/load-scripts.php {
    deny all;
}

location = /wp-admin/load-styles.php {
    deny all;
}
EOF
    fi
  fi

  if [[ -f /var/www/${site}/nginx/disable-wp-content-php-main-context.conf ]]; then
    if ! grep "-before-disable-wp-content-php-main-context.conf" "/var/www/${site}/nginx/disable-wp-content-php-main-context.conf" >/dev/null; then
      cat >"/var/www/${site}/nginx/disable-wp-content-php-main-context.conf" <<EOF
# #
# GridPane Disable web access to PHP scripts in wp-content|wp-include dirs/subdirs
# When disabled may need to add specific rules for lazy plugin devs
# Version 1.2.0
# #
include /var/www/${site}/nginx/*-before-disable-wp-content-php-main-context.conf;

location ~* ^/wp-content/.*\.php$ {
    deny all;
}
EOF
    fi
  fi

  if [[ -f /var/www/${site}/nginx/disable-wp-comments-post-main-context.conf ]]; then
    if ! grep "-before-disable-wp-comments-post-main-context.conf" "/var/www/${site}/nginx/disable-wp-comments-post-main-context.conf" >/dev/null; then
      cat >"/var/www/${site}/nginx/disable-wp-comments-post-main-context.conf" <<EOF
# #
# GridPane Disable Comments
# Disable when the site is NOT using Comments.
# Version 1.2.0
# #
include /var/www/${site}/nginx/*-before-disable-wp-comments-post-main-context.conf;

location = /wp-comments-post.php {
    deny all;
}
EOF
    fi
  fi

  if [[ -f /var/www/${site}/nginx/disable-wp-links-opml-main-context.conf ]]; then
    if ! grep "-before-disable-wp-links-opml-main-context.conf" "/var/www/${site}/nginx/disable-wp-links-opml-main-context.conf" >/dev/null; then
      cat >"/var/www/${site}/nginx/disable-wp-links-opml-main-context.conf" <<EOF
# #
# GridPane Disable Links OPML
# Disable when the site is NOT using the Links feature in the backend.
# Version 1.2.0
# #
include /var/www/${site}/nginx/*-before-disable-wp-links-opml-main-context.conf;

location = /wp-links-opml.php {
    deny all;
}
EOF
    fi
  fi

  if [[ -f /var/www/${site}/nginx/disable-wp-trackbacks-main-context.conf ]]; then
    if ! grep "-before-disable-wp-trackbacks-main-context.conf" "/var/www/${site}/nginx/disable-wp-trackbacks-main-context.conf" >/dev/null; then
      cat >"/var/www/${site}/nginx/disable-wp-trackbacks-main-context.conf" <<EOF
# #
# GridPane Disable Trackbacks
# Disable when the site is NOT using trackbacks.
# Version 1.2.0
# #
include /var/www/${site}/nginx/*-before-disable-wp-trackbacks-main-context.conf;

location = /wp-trackback.php {
    deny all;
}
EOF
    fi
  fi

  if [[ -f /var/www/${site}/nginx/block-install-file-main-context.conf ]]; then
    if ! grep "-before-block-install-file-main-context.conf" "/var/www/${site}/nginx/block-install-file-main-context.conf" >/dev/null; then
      cat >"/var/www/${site}/nginx/block-install-file-main-context.conf" <<EOF
# #
# GridPane Install.php
# Version 1.2.0
# #
include /var/www/${site}/nginx/*-before-block-install-file-main-context.conf;

location ^~ /wp-admin/install.php {
    deny all;
    error_page 403 =404 / ;
}
EOF
    fi
  fi

  if [[ -f /var/www/${site}/nginx/block-upgrade-file-main-context.conf ]]; then
    if ! grep "-before-block-upgrade-file-main-context.conf" "/var/www/${site}/nginx/block-upgrade-file-main-context.conf" >/dev/null; then
      cat >"/var/www/${site}/nginx/block-upgrade-file-main-context.conf" <<EOF
# #
# GridPane Upgrade.php
# Version 1.2.0
# #
include /var/www/${site}/nginx/*-before-block-upgrade-file-main-context.conf;

location ^~ /wp-admin/upgrade.php {
    deny all;
    error_page 403 =404 / ;
}
EOF
    fi
  fi

}

workers::regen::site_nginx_conf() {
  local site="$1"
  if ! grep "sockfile.conf" "/etc/nginx/sites-available/$site" >/dev/null; then
    local site_cache
    site_cache="$(gridpane::check::site::nginx::cache "$site")"
    case $site_cache in
    php|fastcgi|redis)
      local site_config
      site_conf_type="$(gridpane::check::site::nginx::config "$site")"
      case $site_conf_type in
        *http*)
          /usr/local/bin/gp conf nginx generate "${site_conf_type}" "${site_cache}" "${site}"
          reload_nginx="true"
        ;;
      esac
    ;;
    esac
  fi
}

workers::check::site_domains_env() {
  local site="$1"
  while read -r conf; do
    [[ $conf == "/etc/nginx/sites-available/$site" ]] && continue

    if grep "ADD-ON" "$conf" >/dev/null; then
      if grep "301" "$conf" >/dev/null; then
        domains_env="/var/www/$site/logs/$site-301-domains.env"
      else
        domains_env="/var/www/$site/logs/$site-additional-domains.env"
      fi

      [[ ! -f "$domains_env" ]] &&
        touch "$domains_env"

      domain=${conf#/etc/nginx/sites-available/}
      if ! grep ":${domain}:" "$domains_env" >/dev/null; then
        chattr -i "$domains_env"
        chmod 0644 "$domains_env"
        echo ":${domain}:" >>"$domains_env"
        chmod 0444 "$domains_env"
        chattr +i "$domains_env"
      fi
    fi
  done <<<"$(grep -Ril "/$site/" "/etc/nginx/sites-available/")"
  touch /opt/gridpane/domains.env.updated
}


#######################################
# Run passed command against each site in array
#
# Needs to run after
# gridpane::site_loop::directories
#
# requires
# ${sitesDirectoryArray}
#######################################
workers::site_loop::process() {
  # shellcheck disable=SC2154
  for entry in ${sitesDirectoryArray}; do
    case $entry in
      22222|*-remote|html|default|*.gridpanevps.com|"${monit_url}"|"${phpma_url}") continue ;;
    esac

    [[ ${sitesConfigsArray} != *"${entry}"* ]] &&
      echo "${entry} has a directory, but no enabled $webserver_formatted config - inactive site to investigate?" &&
      continue

    # Not needed currently
    #if [[ "$webserver" = nginx ]]; then
    #  site_virtual_server=$(cat /etc/nginx/sites-available/"${entry}")
    #else
    #  site_virtual_server="$(/usr/local/bin/gpols get "$entry" vhconf)"
    #fi

    siteowner=$(gridpane::get::set::site::user "${entry}" -just-get)
    original_domain=$(grep -w "original-domain:.*" "/var/www/${entry}/logs/${entry}.env" | cut -f 2 -d ':') || true
    site_user=$(grep -w "wp-user:.*" "/var/www/${entry}/logs/${entry}.env" | cut -f 2 -d ':') || true
    site_email=$(grep -w "wp-email:.*" "/var/www/${entry}/logs/${entry}.env" | cut -f 2 -d ':') || true

    "$1" "${entry}"

#    site_virtual_server=""
    original_domain=""
    siteowner=""
    site_user=""
    site_email=""
  done
}

workers::php_restarts::process() {
  if [[ "$webserver" = nginx ]]; then
    if [[ ${require_worker_71_restart} == "true" ]]; then
      echo "Restarting php7.1-fpm"
      /usr/sbin/service php7.1-fpm stop
      /usr/sbin/service php7.1-fpm start
    fi
    if [[ ${require_worker_72_restart} == "true" ]]; then
      echo "Restarting php7.2-fpm"
      /usr/sbin/service php7.2-fpm stop
      /usr/sbin/service php7.2-fpm start
    fi
    if [[ ${require_worker_73_restart} == "true" ]]; then
      echo "Restarting php7.3-fpm"
      /usr/sbin/service php7.3-fpm stop
      /usr/sbin/service php7.3-fpm start
    fi
    if [[ ${require_worker_74_restart} == "true" ]]; then
      echo "Restarting php7.4-fpm"
      /usr/sbin/service php7.4-fpm stop
      /usr/sbin/service php7.4-fpm start
    fi
  else
    /usr/local/bin/gpols httpd
  fi
}

workers:borg_checks() {
  borgStatus=$(which borg)
  if [[ -z "${borgStatus}" ]]; then
    echo "Installing Borg..." | tee -a /var/log/gridpane.log
    add-apt-repository ppa:costamagnagianfranco/borgbackup -y
    gridpane::preinstallaptcheck
    apt-get update
    apt-get install -y borgbackup
  fi
  if [[ ! -d /opt/gridpane/backups/snapshots ]]; then
    mkdir -p /opt/gridpane/backups/snapshots
    export BORG_PASSPHRASE=$(awk '{print $1; exit}' /root/gridpane.token)
    drivesize=$(df -h / | awk '{print $2}')
    drivesize=${drivesize//[!0-9]/}
    bupquota=$((drivesize / 5))
    /usr/bin/borg init --encryption=repokey /opt/gridpane/backups/snapshots --storage-quota "$bupquota"G
    echo "Initialized Backup Directory /opt/gridpane/backups/snapshots..." | tee -a /var/log/gridpane.log
    gridpane::conf_write borg-storage-quota-gb ${bupquota}
  fi
  stored_borg_quota=$(grep -w "borg-storage-quota-gb:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  if [[ -z ${stored_borg_quota} ]]; then
    stored_borg_quota=$(grep -m 1 "storage_quota =" /opt/gridpane/backups/snapshots/config | cut -d " " -f 3)
    stored_borg_quota=$((stored_borg_quota / 1000000000))
    gridpane::conf_write borg-storage-quota-gb ${stored_borg_quota}
  fi
}

workers::check::server_resources() {
  local monit_notifications_update
  monit_notifications_update=$(grep -w "monit-notifications:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  local storedProcNo
  storedProcNo=$(grep -w "cpu-cores:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  local storedSysRam
  storedSysRam=$(grep -w "system-ram-mb:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  local ram_KB
  ram_KB=$(grep MemTotal /proc/meminfo | awk '{print $2}')
  local ram_MB
  ram_MB=$(echo "scale=0; ${ram_KB} / 1000" | bc)
  local cores
  cores=$(nproc --all)
  local disk_usage
  disk_usage=$(df)
  local disk_size
  disk_size=$(echo "${disk_usage}" | awk '$NF=="/"{printf "%d", $2}')
  local disk_GB
  disk_GB=$(echo "scale=0; ${disk_size} / 1000000" | bc)

  # Remove after been running a day, will update all monit redis configs to have at least a
  # 20MB margin above their set maxmemory, this is all repeated in the function as part of resizing.
  local update_monit_one_time_deal
  local redisRAM
  local currentMonitRedisRam
  redisRAM=$(/bin/grep "maxmemory .*mb" /etc/redis/redis.conf | xargs)
	redisRAM=${redisRAM#maxmemory }
  redisRAM=${redisRAM%mb}
  redisRAM=$((redisRAM + 50))
  currentMonitRedisRam=$(/bin/grep "if totalmem > .* Mb" /etc/monit/conf.d/redis | xargs)
  currentMonitRedisRam=${currentMonitRedisRam#if totalmem > }
  currentMonitRedisRam=${currentMonitRedisRam% Mb then alert}
  currentMonitRedisRam=${currentMonitRedisRam% Mb}
  [[ ${redisRAM} -lt ${currentMonitRedisRam} ]] &&
    update_monit_one_time_deal="true"

  if [[ ${monit_notifications_update} != "true" ]] ||
    [[ ${update_monit_one_time_deal} == "true" ]] ||
    [[ ${storedProcNo} != "${cores}" ]] ||
    [[ ${storedSysRam} != "${ram_MB}" ]]; then
    {
      echo "Updating Monit for new server resources..."
      /usr/local/bin/gp server update-monit
      [[ ${monit_notifications_update} != "true" ]] &&
        gridpane::conf_write monit-notifications true
      [[ ${storedSysRam} != "${ram_MB}" ]] &&
        echo "Updating - system-ram-mb:${ram_MB}"
      [[ ${storedProcNo} != "${cores}" ]] &&
        echo "Updating - cpu-cores:${cores}"
      } | tee -a /var/log/gridpane.log
    gridpane::conf_write system-ram-mb "${ram_MB}"
    gridpane::conf_write cpu-cores "${cores}"
  fi
  local stored_disk_GB
  stored_disk_GB=$(grep -w "system-disk-gb:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  if [[ ${stored_disk_GB} != "${disk_GB}" ]]; then
    echo "Updating - system-disk-gb:${disk_GB}" | tee -a /var/log/gridpane.log
    gridpane::conf_write system-disk-gb "${disk_GB}"
    if [[ ${backup_version} != "2" ]]; then
      local drivesize
      drivesize=$(df -h / | awk '{print $2}')
      local drivesize_gb
      drivesize_gb=${drivesize//[!0-9]/}
      generated_bupquota_gb_for_borg=$((drivesize_gb / 5))
      stored_borg_quota=$(grep -w "borg-storage-quota-gb:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
      if [[ ${generated_bupquota_gb_for_borg} -gt "${stored_borg_quota}" ]]; then
        {
          echo "Backups v1 detected - updating quota: {generated_bupquota_gb_for_borg}"
          /usr/local/bin/gp server update-borg "${generated_bupquota_gb_for_borg}" -q
        } | tee -a /var/log/gridpane.log
      fi
    fi
  fi
}

workers::check::unattended_upgrades::config() {
  local allow_change_update
  local unatupconf
  unatupconf=$(cat /etc/apt/apt.conf.d/50unattended-upgrades)
  if [[ ${unatupconf} != *"Dpkg::Options"* ]]; then
    cat >>/etc/apt/apt.conf.d/50unattended-upgrades <<EOF

// Force non interactive updating of repo conf
Dpkg::Options {
   "--force-confdef";
   "--force-confold";
};
EOF
    allow_change_update="true"
  fi

  [[ ${unatupconf} != *"GridPane:bionic"* ]] &&
    sed -i "/\${distro_id}ESM:\${distro_codename}/a\	\"GridPane:bionic\";" /etc/apt/apt.conf.d/50unattended-upgrades &&
    allow_change_update="true"

  [[ $allow_change_update == "true" ]] &&
    apt-get --allow-releaseinfo-change update
}

workers::check::crontab() {
  crontab -l >"/opt/gridpane/${nonce}.tempcron"
  local update_crontab
  update_crontab="false"

  if ! /bin/grep "gpworker" "/opt/gridpane/${nonce}.tempcron" >/dev/null; then
    echo "*/15 * * * * /usr/local/bin/gpworker >>/opt/gridpane/gpworker.log" >>"/opt/gridpane/${nonce}.tempcron"
    update_crontab="true"
  fi

  if ! /bin/grep "gphourlyworker" "/opt/gridpane/${nonce}.tempcron" >/dev/null; then
    echo "50 * * * * /usr/local/bin/gphourlyworker >>/opt/gridpane/gphourlyworker.log" >>"/opt/gridpane/${nonce}.tempcron"
    update_crontab="true"
  fi

  if ! /bin/grep "gpdailyworker" "/opt/gridpane/${nonce}.tempcron" >/dev/null; then
    local dailyworker_hour
    dailyworker_hour=$((RANDOM % 4 + 2))
    local dailyworker_min
    dailyworker_min=$((RANDOM % 58 + 1))
    echo "${dailyworker_min} ${dailyworker_hour} * * * /usr/local/bin/gpdailyworker >>/opt/gridpane/gpdailyworker.log" >>"/opt/gridpane/${nonce}.tempcron"
    update_crontab="true"
  fi

  if ! /bin/grep "gpweeklyworker" "/opt/gridpane/${nonce}.tempcron" >/dev/null; then
    echo "0 5 * * 0 /usr/local/bin/gpweeklyworker >>/opt/gridpane/gpweeklyworker.log" >>"/opt/gridpane/${nonce}.tempcron"
    update_crontab="true"
  fi

  if ! /bin/grep "monit -ssl-renewal" "/opt/gridpane/${nonce}.tempcron" >/dev/null; then
    local renewal_day
    renewal_day=$((RANDOM % 26 + 1))
    local renewal_hou
    renewal_hour=$((RANDOM % 22 + 1))
    local renewal_min
    renewal_min=$((RANDOM % 58 + 1))
    echo "${renewal_min} ${renewal_hour} ${renewal_day} * * /usr/local/bin/gp site monit -ssl-renewal >>/opt/gridpane/stats-ssl.log" >>"/opt/gridpane/${nonce}.tempcron"
    update_crontab="true"
  else
    if /bin/grep "0 5 1 \* \* /usr/local/bin/gp site monit -ssl-renewal" "/opt/gridpane/${nonce}.tempcron" >/dev/null; then
      echo "swap it"
      local renewal_day
      renewal_day=$((RANDOM % 26 + 1))
      local renewal_hour
      renewal_hour=$((RANDOM % 22 + 1))
      local renewal_min
      renewal_min=$((RANDOM % 58 + 1))
      /bin/sed -i "s|0 5 1 \* \* /usr/local/bin/gp site monit -ssl-renewal|${renewal_min} ${renewal_hour} ${renewal_day} * * /usr/local/bin/gp site monit -ssl-renewal|g" "/opt/gridpane/${nonce}.tempcron"
      update_crontab="true"
    fi
  fi

  [[ ${update_crontab} == "true" ]] &&
    echo "updating crontab" &&
    crontab "/opt/gridpane/${nonce}.tempcron"
  rm "/opt/gridpane/${nonce}.tempcron"
}

workers::check::certbot_auto() {
  if [[ -z "$(which certbot-auto)" ]]; then
    wget https://dl.eff.org/certbot-auto
    mv certbot-auto /usr/local/bin/certbot-auto
    chown root /usr/local/bin/certbot-auto
    chmod 0755 /usr/local/bin/certbot-auto
    sed -i 's/virtualenv --no-site-packages/virtualenv --no-download --no-site-packages/' /usr/local/bin/certbot-auto
    export PATH=/sbin:/bin:/usr/bin:/usr/sbin:/usr/local/sbin:/usr/local/bin
    yes | certbot-auto --version
  fi
}

workers::check::server_email() {
  currentwpemail=$(grep -w "mail:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  if [[ -z ${currentwpemail} ]]; then
    currentwpemail=$(cat /root/gridcreds/wp.email)
    gridpane::conf_write mail "${currentwpemail}"
  fi
}

workers::check::active_sites_env_demarkers() {
  [[ -f /root/gridenv/active-sites.env ]] &&
    while read site_to_update_in_active_sites; do
      if [[ ${site_to_update_in_active_sites} != ":"* &&
            ${site_to_update_in_active_sites} != *":" ]]; then
        chattr -i "/root/gridenv/active-sites.env"
        sed -i "s/${site_to_update_in_active_sites}/:${site_to_update_in_active_sites}:/g" "/root/gridenv/active-sites.env"
        chattr +i "/root/gridenv/active-sites.env"
      fi
    done </root/gridenv/active-sites.env
}

workers::le_renewals::last_line() {
  renewal_confs=$(dir /etc/letsencrypt/renewal)
  for conf in ${renewal_confs}; do
    last_empty_line_of_conf=$(awk '/./{line=$0} END{print line}' "/etc/letsencrypt/renewal/${conf}")
    if [[ ${last_empty_line_of_conf} == *"webroot_path"* ]]; then
      echo "${last_empty_line_of_conf}"
      output=$(awk 'NR>1 && /./ { print buf; buf=rs=""} { buf=(buf rs $0); rs=RS }' "/etc/letsencrypt/renewal/${conf}")
      echo "${output}" >"/etc/letsencrypt/renewal/${conf}"
    fi
  done
}

workers::check::auditd() {
  if [[ $(dpkg-query -W -f='${Status}' auditd 2>/dev/null | grep -c "ok installed") -eq 0 ]]; then
    echo "Installing AuditD"
    export PATH=/sbin:/bin:/usr/bin:/usr/sbin:/usr/local/sbin:/usr/local/bin
    apt install -y auditd
    rm /etc/audit/auditd.conf
    touch /etc/audit/auditd.conf
    cat >/etc/audit/auditd.conf <<EOF
# GridPane Core System Audit Log
# This file controls the configuration of the audit daemon

local_events = yes
write_logs = yes
log_file = /var/log/audit/audit.log
log_group = adm
log_format = RAW
flush = INCREMENTAL_ASYNC
freq = 50
max_log_file = 100
num_logs = 10
priority_boost = 4
disp_qos = lossy
dispatcher = /sbin/audispd
name_format = NONE
##name = mydomain
max_log_file_action = ROTATE
space_left = 75
space_left_action = SYSLOG
verify_email = yes
action_mail_acct = root
admin_space_left = 50
admin_space_left_action = SUSPEND
disk_full_action = SUSPEND
disk_error_action = SUSPEND
use_libwrap = yes
##tcp_listen_port = 60
tcp_listen_queue = 5
tcp_max_per_addr = 1
##tcp_client_ports = 1024-65535
tcp_client_max_idle = 0
enable_krb5 = no
krb5_principal = auditd
##krb5_key_file = /etc/audit/audit.key
distribute_network = no
EOF
    systemctl restart auditd.service
  else
    echo "AuditD already installed"
  fi
}

workers::check::systemd::monit() {
  if [[ ! -f /lib/systemd/system/monit.service ]]; then
    monit_path=$(which monit)
    cat >/lib/systemd/system/monit.service <<EOF
[Unit]
Description=Pro-active monitoring utility for unix systems
After=network-online.target
Documentation=man:monit(1) https://mmonit.com/wiki/Monit/HowTo

[Service]
Type=simple
KillMode=process
ExecStart=${monit_path} -I
ExecStop=${monit_path} quit
ExecReload=${monit_path} reload
Restart = on-abnormal
StandardOutput=null

[Install]
WantedBy=multi-user.target
EOF
    systemctl enable monit.service
    /usr/sbin/service monit start
    monit
  fi
}

workers::check::wpcli_skel() {
  if [[ ! -f /etc/default/useradd ]]; then
    cat >/etc/default/useradd <<EOF
# Default values for useradd(8)
#
# The SHELL variable specifies the default login shell on your
# system.
# Similar to DHSELL in adduser. However, we use "sh" here because
# useradd is a low level utility and should be as general
# as possible
SHELL=/bin/sh
#
# The default group for users
# 100=users on Debian systems
# Same as USERS_GID in adduser
# This argument is used when the -n flag is specified.
# The default behavior (when -n and -g are not specified) is to create a
# primary user group with the same name as the user being added to the
# system.
# GROUP=100
#
# The default home directory. Same as DHOME for adduser
# HOME=/home
#
# The number of days after a password expires until the account
# is permanently disabled
# INACTIVE=-1
#
# The default expire date
# EXPIRE=
#
# The SKEL variable specifies the directory containing "skeletal" user
# files; in other words, files such as a sample .profile that will be
# copied to the new user's home directory when it is created.
SKEL=/etc/skel
#
# Defines whether the mail spool should be created while
# creating the account
# CREATE_MAIL_SPOOL=yes
EOF
  else
    check_useradd_skel=$(cat /etc/default/useradd)
    if [[ ${check_useradd_skel} == *"# SKEL="* ]] ||
      [[ ${check_useradd_skel} == *"#SKEL="* ]]; then
      sed -i "/SKEL=/c\SKEL=/etc/skel" /etc/default/useradd
    fi
  fi
  if [[ ! -d /etc/skel/.wp-cli ]]; then
    if [[ ! -d /etc/skel ]]; then
      mkdir -p /etc/skel
    fi
    cp -rf /root/.wp-cli /etc/skel/.wp-cli
  fi
}

workers::check::systemd::nginx() {
  if [[ "$webserver" = nginx ]]; then
    check_it=$(cat /etc/systemd/system/nginx.service)
    if [[ ${check_it} != *"gpocsp"* &&
        -f /usr/local/bin/gpocsp ]]; then
      cat >/etc/systemd/system/nginx.service <<EOF
[Unit]
Description=A high performance web server and a reverse proxy server
After=network.target

[Service]
Type=forking
PIDFile=/run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t -q -g 'daemon on; master_process on;'
ExecStartPre=-/usr/local/bin/gpocsp
ExecStart=/usr/sbin/nginx -g 'daemon on; master_process on;'
ExecReload=-/usr/local/bin/gpocsp
ExecReload=/usr/sbin/nginx -g 'daemon on; master_process on;' -s reload
ExecStop=-/sbin/start-stop-daemon --quiet --stop --retry QUIT/5 --pidfile /run/nginx.pid
TimeoutStopSec=5
KillMode=mixed

[Install]
WantedBy=multi-user.target
EOF
      systemctl daemon-reload
    fi
  fi
}

workers::check::backups_token() {
  if [[ ! -f /root/gridenv/backups.token ]]; then
    [[ -f /opt/gridpane/promethean.env.bak ]] &&
      backups_token=$(grep -w "gridpane-token:.*" /opt/gridpane/promethean.env.bak | cut -f 2 -d ':')
    [[ -z ${backups_token} && -f /opt/gridpane/env-baks/promethean.env.og.bak ]] &&
      backups_token=$(grep -w "gridpane-token:.*" /opt/gridpane/env-baks/promethean.env.og.bak | cut -f 2 -d ':')
    [[ -n ${backups_token} ]] &&
      echo "${backups_token}" >/root/gridenv/backups.token &&
      chattr +i /root/gridenv/backups.token &&
      gridpane::conf_write backups-token "${backups_token}"
  fi
}

workers::check::site_routing() {
  local site="$1"
  local check_route_config

  if [[ "$webserver" = nginx ]]; then
    check_route_config="$(gridpane::check::site::nginx::config "$site")"
  else
    check_route_config="$(/usr/local/bin/gpols get "$site" route)"
  fi

  if [[ "$check_route_config" != *"www"* &&
    "$check_route_config" != *"root"* ]]; then
    local scheme="http"
    [[ "$check_route_config" == *"https"* ]] &&
      scheme="https"

    if ! grep "//define('WP_HOME" "/var/www/${site}/wp-config.php" >/dev/null; then
      local wp_options_home
      local wp_home
      wp_options_home=$(sudo su - "${siteowner}" -c "timeout 300 /usr/local/bin/wp option get home --path=/var/www/${site}/htdocs 2>&1") || true
      case $wp_options_home in
      *//www."${site}"*) wp_home="${scheme}://www.${site}" ;;
      *) wp_home="${scheme}://${site}" ;;
      esac
      sed -i "/define('WP_HOME'/c \\/\/define('WP_HOME', '${wp_home}');" "/var/www/${site}/wp-config.php"
    fi

    if ! grep "//define('WP_SITEURL" "/var/www/${site}/wp-config.php" >/dev/null; then
      local wp_options_siteurl
      local wp_siteurl
      wp_options_siteurl=$(sudo su - "${siteowner}" -c "timeout 300 /usr/local/bin/wp option get siteurl --path=/var/www/${site}/htdocs 2>&1") || true
      case $wp_options_siteurl in
      *//www."${site}"*) wp_siteurl="${scheme}://www.${site}" ;;
      *) wp_siteurl="${scheme}://${site}" ;;
      esac
      sed -i "/define('WP_SITEURL'/c \\/\/define('WP_SITEURL', '${wp_siteurl}');" "/var/www/${site}/wp-config.php"
    fi
  fi
}

workers::check::site_suspend() {
  local site="$1"
  if [[ ! -f /var/www/"${site}"/htdocs/index.php &&
        -f /var/www/"${site}"/htdocs/index.php.suspend &&
        -f /var/www/"${site}"/htdocs/index.html ]]; then

    [[ ! -f /var/www/"${site}"/nginx/gp-suspend-root-context.conf ]] &&
      cat >/var/www/"${site}"/nginx/gp-suspend-root-context.conf <<EOF
# GridPane Suspend response unavailable
error_page 503 /index.html;
return 503;
EOF

    [[ ! -f /var/www/"${site}"/nginx/gp-suspend-wp-admin-context.conf ]] &&
      cat >/var/www/"${site}"/nginx/gp-suspend-wp-admin-context.conf <<EOF
# GridPane Suspend response unavailable
error_page 503 /index.html;
return 503;
EOF

  fi
}

workers::check::gridpane_constant() {
  local site="$1"
  local check_wp_config=$(cat /var/www/${site}/wp-config.php)
  if [[ ${check_wp_config} != *"define('GRIDPANE', true);"* ]]; then
    sed -i "/GridPane site routing/i \/\* GridPane \*\/" /var/www/${site}/wp-config.php
    sed -i "/GridPane site routing/i define('GRIDPANE', true);" /var/www/${site}/wp-config.php
    sed -i "/GridPane site routing/i\    " /var/www/${site}/wp-config.php
  fi
}

workers::check::gp_ultimo() {
  local site="$1"

  check_site_ultimo=$(gridpane::conf_read gp-ultimo -site.env ${site})
  check_wp_config=$(cat /var/www/${site}/wp-config.php)
  if [[ -z ${check_site_ultimo} ]]; then

    if [[ -f /var/www/${site}/gp-ultimo-configs.php ]] &&
      [[ ${check_wp_config} != *"//include __DIR__ . '/gp-ultimo-configs.php'"* ]]; then
      gp_ultimo_bool="true"
      gridpane::callback::app \
        "/site/site-update" \
        "site_url=${site}" \
        "server_ip=${serverIP}" \
        "gp_ultimo@${gp_ultimo_bool}" \
        "silent@true"
    else
      gp_ultimo_bool="false"
    fi

    gridpane::conf_write gp-ultimo ${gp_ultimo_bool} -site.env ${site}
    echo ""
    echo "Adjusting ${site} gp-ultimo ${gp_ultimo_bool}"
  fi
}

workers::check::wp_environment_type() {
  local site="$1"

  local wp_environment_type
  if [[ ${site} == "staging."* ]]; then
    wp_environment_type="staging"
  else
    wp_environment_type="production"
  fi

  local check_wp_config
  check_wp_config=$(cat /var/www/"${site}"/wp-config.php)
  if [[ ${check_wp_config} != *"WP_ENVIRONMENT_TYPE"* ]]; then
    sed -i "/define('GRIDPANE'/a\define('WP_ENVIRONMENT_TYPE', '${wp_environment_type}');" /var/www/"${site}"/wp-config.php
  fi
}

workers::check::webserver() {
  local webserver_set_to
  webserver_set_to=$(grep -w "webserver:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
  if [[ ${webserver_set_to} != "nginx" ]]; then
    # Lets make sure this is NOT an Nginx Server and if it is, fix the env
    export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    local check_nginx
    check_nginx="$(which nginx)"
    [[ -n ${check_nginx} ]] &&
      gridpane::conf_write "webserver" "nginx" -v
  fi
}

workers::check::ols_jq() {
  if [[ $webserver == "openlitespeed" ]]; then
    local check_jq
    check_jq=$(/usr/bin/which jq)
    if [[ -z ${check_jq} ]]; then
      gridpane::preinstallaptcheck >/dev/null
      apt-get -y install jq
    fi
  fi
}

workers::check::mysql_error_log() {
  if [[ "$(mysql -V)" == *"Percona"* ]]; then
    local cnf
    cnf="$(cat /etc/mysql/mysql.conf.d/mysqld.cnf)"
    if [[ ${cnf} == *"log_error = /var/log/mysql/error.log"* ]]; then
      sed -i "s/log_error = \/var\/log\/mysql\/error.log//g" /etc/mysql/mysql.conf.d/mysqld.cnf
    fi
  fi
}

workers::check::php_slow_log() {
  local php_71_pool_confs
  php_71_pool_confs="$(ls /etc/php/7.1/fpm/pool.d/)"
  for entry in ${php_71_pool_confs}; do
    local php_71_pool_conf_content
    php_71_pool_conf_content="$(cat /etc/php/7.1/fpm/pool.d/"${entry}")"
    if [[ ${php_71_pool_conf_content} == *";slowlog ="* ]]; then
      if [[ ${php_71_pool_conf_content} == *"request_slowlog_timeout ="* &&
            ${php_71_pool_conf_content} != *";request_slowlog_timeout ="* ]]; then
        sed -i "/request_slowlog_timeout =/c\;request_slowlog_timeout = 0" "/etc/php/7.1/fpm/pool.d/${entry}"
      fi
      if [[ ${php_71_pool_conf_content} == *"request_slowlog_trace_depth ="* &&
            ${php_71_pool_conf_content} != *";request_slowlog_trace_depth ="* ]]; then
        sed -i "/request_slowlog_trace_depth =/c\;request_slowlog_trace_depth = 20" "/etc/php/7.1/fpm/pool.d/${entry}"
      fi
    fi
  done

  local php_72_pool_confs
  php_72_pool_confs="$(ls /etc/php/7.2/fpm/pool.d/)"
  for entry in ${php_72_pool_confs}; do
    local php_72_pool_conf_content
    php_72_pool_conf_content="$(cat "/etc/php/7.2/fpm/pool.d/${entry}")"
    if [[ ${php_72_pool_conf_content} == *";slowlog ="* ]]; then
      if [[ ${php_72_pool_conf_content} == *"request_slowlog_timeout ="* &&
            ${php_72_pool_conf_content} != *";request_slowlog_timeout ="* ]]; then
        sed -i "/request_slowlog_timeout =/c\;request_slowlog_timeout = 0" "/etc/php/7.2/fpm/pool.d/${entry}"
      fi
      if [[ ${php_72_pool_conf_content} == *"request_slowlog_trace_depth ="* &&
            ${php_72_pool_conf_content} != *";request_slowlog_trace_depth ="* ]]; then
        sed -i "/request_slowlog_trace_depth =/c\;request_slowlog_trace_depth = 20" "/etc/php/7.2/fpm/pool.d/${entry}"
      fi
    fi
  done

  local php_73_pool_confs
  php_73_pool_confs="$(ls /etc/php/7.3/fpm/pool.d/)"
  for entry in ${php_73_pool_confs}; do
    local php_73_pool_conf_content
    php_73_pool_conf_content="$(cat "/etc/php/7.3/fpm/pool.d/${entry}")"
    if [[ ${php_73_pool_conf_content} == *";slowlog ="* ]]; then
      if [[ ${php_73_pool_conf_content} == *"request_slowlog_timeout ="* &&
            ${php_73_pool_conf_content} != *";request_slowlog_timeout ="* ]]; then
        sed -i "/request_slowlog_timeout =/c\;request_slowlog_timeout = 0" "/etc/php/7.3/fpm/pool.d/${entry}"
      fi
      if [[ ${php_73_pool_conf_content} == *"request_slowlog_trace_depth ="* &&
            ${php_73_pool_conf_content} != *";request_slowlog_trace_depth ="* ]]; then
        sed -i "/request_slowlog_trace_depth =/c\;request_slowlog_trace_depth = 20" "/etc/php/7.3/fpm/pool.d/${entry}"
      fi
    fi
  done

  local php_74_pool_confs
  php_74_pool_confs="$(ls /etc/php/7.4/fpm/pool.d/)"
  for entry in ${php_74_pool_confs}; do
    local php_74_pool_conf_content
    php_74_pool_conf_content="$(cat "/etc/php/7.4/fpm/pool.d/${entry}")"
    if [[ ${php_74_pool_conf_content} == *";slowlog ="* ]]; then
      if [[ ${php_74_pool_conf_content} == *"request_slowlog_timeout ="* &&
            ${php_74_pool_conf_content} != *";request_slowlog_timeout ="* ]]; then
        sed -i "/request_slowlog_timeout =/c\;request_slowlog_timeout = 0" "/etc/php/7.4/fpm/pool.d/${entry}"
      fi
      if [[ ${php_74_pool_conf_content} == *"request_slowlog_trace_depth ="* &&
            ${php_74_pool_conf_content} != *";request_slowlog_trace_depth ="* ]]; then
        sed -i "/request_slowlog_trace_depth =/c\;request_slowlog_trace_depth = 20" "/etc/php/7.4/fpm/pool.d/${entry}"
      fi
    fi
  done
}

workers::check::monit() {
  local monit_reload
  local procNo=$(nproc --all)
  if ! grep "/diskus --threads" /usr/local/bin/monit-scripts/monit_dir_sizecheck >/dev/null; then
    if [[ -f opt/gridpane/server-scripts/monit-scripts/monit_dir_sizecheck ]]; then
      local diskus_theads
      case $procNo in
        1) diskus_threads=2 ;;
        2|3|4) diskus_threads=4 ;;
        *) diskus_threads=6 ;;
      esac
      cp /opt/gridpane/server-scripts/monit-scripts/monit_dir_sizecheck /usr/local/bin/monit-scripts/monit_dir_sizecheck
      sed -i "s/XXXDISKUSTHREADSXXX/${diskus_threads}/g" /usr/local/bin/monit-scripts/monit_dir_sizecheck
      monit_reload="true"
    fi
  else
    if grep "/--threads XXXDISKUSTHREADSXXX" /usr/local/bin/monit-scripts/monit_dir_sizecheck >/dev/null; then
      local diskus_theads
      case $procNo in
        1) diskus_threads=2 ;;
        2|3|4) diskus_threads=4 ;;
        *) diskus_threads=6 ;;
      esac
      sed -i "s/XXXDISKUSTHREADSXXX/${diskus_threads}/g" /usr/local/bin/monit-scripts/monit_dir_sizecheck
      monit_reload="true"
    fi
  fi

  if [[ ! -f /usr/local/bin/monit-scripts/monit_run_ramdisk_usage ]]; then
    [[ ! -f /opt/gridpane/server-scripts/monit-scripts/monit_run_ramdisk_usage ]] &&
      touch /opt/gridpane/server-scripts/monit-scripts/monit_run_ramdisk_usage
    if [[ -f /opt/gridpane/server-scripts/monit-scripts/monit_run_ramdisk_usage ]]; then
      cp /opt/gridpane/server-scripts/monit-scripts/monit_run_ramdisk_usage /usr/local/bin/monit-scripts/monit_run_ramdisk_usage
      chmod 755 /usr/local/bin/monit-scripts/monit_run_ramdisk_usage
      monit_reload="true"
    fi
  fi

  if [[ "${webserver}" = nginx &&
    ! -f /usr/local/bin/monit-scripts/php_fpm_test ]]; then
    [[ ! -f /opt/gridpane/server-scripts/monit-scripts/php_fpm_test ]] &&
      touch /opt/gridpane/server-scripts/monit-scripts/php_fpm_test
    if [[ -f /opt/gridpane/server-scripts/monit-scripts/php_fpm_test ]]; then
      cp /opt/gridpane/server-scripts/monit-scripts/php_fpm_test /usr/local/bin/monit-scripts/php_fpm_test
      chmod 755 /usr/local/bin/monit-scripts/php_fpm_test
      monit_reload="true"
    fi
  fi

  if [[ "${webserver}" = openlitespeed &&
    ! -f /usr/local/bin/monit-scripts/openlitespeed_conf_test ]]; then
    [[ ! -f /opt/gridpane/server-scripts/monit-scripts/openlitespeed_conf_test ]] &&
      touch /opt/gridpane/server-scripts/monit-scripts/openlitespeed_conf_test
    if [[ -f /opt/gridpane/server-scripts/monit-scripts/openlitespeed_conf_test ]]; then
      cp /opt/gridpane/server-scripts/monit-scripts/openlitespeed_conf_test /usr/local/bin/monit-scripts/openlitespeed_conf_test
      chmod 755 /usr/local/bin/monit-scripts/openlitespeed_conf_test
      monit_reload="true"
    fi
  fi

  # Check if server log is symlinked in /var/log/ols
  if [[ "$webserver" = openlitespeed && ! -f /var/log/ols/ols.access.log ||
        "$webserver" = openlitespeed && ! -f /var/log/ols/ols.error.log ]]; then
    /usr/local/bin/gpols init
  fi

  # TEMPORARY @CIM
  # Need to replace old logpath for fail2ban
  if [[ "$webserver" = openlitespeed ]]; then
    local check_old_logpath
    [[ -f /etc/fail2ban/jail.local ]] && check_old_logpath="$(/bin/cat < /etc/fail2ban/jail.local)"
    [[ "$check_old_logpath" == *"/var/www/*/logs/*access.log"* ]] && /bin/sed -i "s|/var/www/\*/logs/\*access.log|/var/log/ols/\*access.log|g" /etc/fail2ban/jail.local
  fi

  if ! grep "programs v2" "/etc/monit/conf.d/programs" >/dev/null; then
    # shellcheck disable=SC2154
    if [[ "${webserver}" = nginx &&
           "${mysql_version}" = percona ]]; then
      cp /opt/gridpane/monit-settings/conf.d/programs_lepp /etc/monit/conf.d/programs
      monit_reload="true"
    elif [[ "${webserver}" = nginx &&
            "${mysql_version}" = mariadb ]]; then
      cp /opt/gridpane/monit-settings/conf.d/programs_lemp /etc/monit/conf.d/programs
      monit_reload="true"
    elif [[ "${webserver}" = openlitespeed &&
          "${mysql_version}" = percona ]]; then
      cp /opt/gridpane/monit-settings/conf.d/programs_lomp /etc/monit/conf.d/programs
      monit_reload="true"
    elif [[ "${webserver}" = openlitespeed &&
          "${mysql_version}" = mariadb ]]; then
      cp /opt/gridpane/monit-settings/conf.d/programs_lopp /etc/monit/conf.d/programs
      monit_reload="true"
    fi
  fi

  if ! grep "fail2ban v2" "/etc/monit/conf.d/fail2ban" >/dev/null; then
    if [[ -f /opt/gridpane/server-scripts/monit-settings/conf.d/fail2ban ]]; then
      cp /opt/gridpane/server-scripts/monit-settings/conf.d/fail2ban /etc/monit/conf.d/fail2ban
      monit_reload="true"
    fi
  fi

  if ! grep "filesystem v2" "/etc/monit/conf.d/filesystem" >/dev/null; then
    if [[ -f /opt/gridpane/server-scripts/monit-settings/conf.d/filesystem ]]; then
      cp /opt/gridpane/server-scripts/monit-settings/conf.d/filesystem /etc/monit/conf.d/filesystem
      monit_reload="true"
    fi
  fi

  if ! grep "system v2" "/etc/monit/conf.d/system" >/dev/null; then
    if [[ -f /opt/gridpane/server-scripts/monit-settings/conf.d/system ]]; then
      cp /opt/gridpane/server-scripts/monit-settings/conf.d/system /etc/monit/conf.d/system
      monit_reload="true"
    fi
  fi

  if ! grep "mysql v2" "/etc/monit/conf.d/mysql" >/dev/null; then
    /usr/local/bin/gpmonit mysql --no-reload
    monit_reload="true"
  fi

  if [[ "${webserver}" = openlitespeed ]]; then
    if ! grep "openlitespeed v2" "/etc/monit/conf.d/openlitespeed" >/dev/null; then
      /usr/local/bin/gpmonit openlitespeed --no-reload
      monit_reload="true"
    fi
  fi

  if [[ "${webserver}" = nginx ]]; then
    if ! grep "nginx v2" "/etc/monit/conf.d/nginx" >/dev/null; then
      /usr/local/bin/gpmonit nginx --no-reload
      monit_reload="true"
    fi
    if ! grep "php71 v2" "/etc/monit/conf.d/php71" >/dev/null; then
      /usr/local/bin/gpmonit php71 --no-reload
      monit_reload="true"
    fi
    if ! grep "php72 v2" "/etc/monit/conf.d/php72" >/dev/null; then
      /usr/local/bin/gpmonit php72 --no-reload
      monit_reload="true"
    fi
    if ! grep "php73 v2" "/etc/monit/conf.d/php73" >/dev/null; then
      /usr/local/bin/gpmonit php73 --no-reload
      monit_reload="true"
    fi
    if ! grep "php74 v2" "/etc/monit/conf.d/php74" >/dev/null; then
      /usr/local/bin/gpmonit php74 --no-reload
      monit_reload="true"
    fi
  fi

  if ! grep "redis v2" "/etc/monit/conf.d/redis" >/dev/null; then
    /usr/local/bin/gpmonit redis --no-reload
    monit_reload="true"
  fi

  if [[ -n $monit_reload ]]; then
    if $(which monit) -t; then
      $(which monit) -v
      $(which monit) reload
    fi
  fi
}

workers::check::g_waf_sed() {
  local site="$1"
  if [[ -f /etc/nginx/common/${site}-6g.conf ]]; then
    [[ $(cat /etc/nginx/common/"${site}"-6g.conf) == *"*-6g-context.conf;/g"* ]] &&
      sed -i "/*-6g-context.conf/c \include \/var\/www\/${site}\/nginx\/\*-6g-context.conf;" /etc/nginx/common/"${site}"-6g.conf &&
      reload_nginx="true"
  fi

  if [[ -f /etc/nginx/common/${site}-7g.conf ]]; then
    [[ $(cat /etc/nginx/common/"${site}"-7g.conf) == *"*-7g-context.conf;/g"* ]] &&
      sed -i "/*-7g-context.conf/c \include \/var\/www\/${site}\/nginx\/\*-7g-context.conf;" /etc/nginx/common/"${site}"-7g.conf &&
      reload_nginx="true"
  fi
}

workers::check::reboot_required() {
  local running_as_worker="$1"
  echo "Checking if security reboot required..."
  if [[ ! -f /usr/local/bin/gprebooted ]]; then
    echo "Creating reboot script..."
    cat >/usr/local/bin/gprebooted<<EOF
# Script to run GridPane functions on a server reboot
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
# Sync system timezones and stuff - internally checks if reboot is a security auto reboot
/usr/local/bin/gp server -sync-system-settings reboot
# easily store the last reboot time somewhere accessible
/bin/date >/opt/gridpane/last.restart
EOF
    chmod u+x /usr/local/bin/gprebooted
  fi

  if [[ ! -f /etc/systemd/system/gridpane-startup.service &&
        -f /usr/local/bin/gprebooted ]]; then
    echo "Creating reboot service..."
    cat >/etc/systemd/system/gridpane-startup.service<<EOF
[Unit]
Description=Run After Rebooting A GridPane Managed Server
After=default.target

[Service]
ExecStart=/usr/local/bin/gprebooted

[Install]
WantedBy=default.target
EOF

    systemctl daemon-reload
    systemctl enable gridpane-startup.service
  fi

  local reboot
  reboot=$(grep "Unattended-Upgrade::Automatic-Reboot " /etc/apt/apt.conf.d/50unattended-upgrades)
  reboot=${reboot#Unattended-Upgrade::Automatic-Reboot \"}
  reboot=${reboot%\";}
  local reboot_time
  reboot_time=$(grep "Unattended-Upgrade::Automatic-Reboot-Time " /etc/apt/apt.conf.d/50unattended-upgrades)
  reboot_time=${reboot_time#Unattended-Upgrade::Automatic-Reboot-Time \"}
  reboot_time=${reboot_time%\";}

  if [[ ! -f /opt/gridpane/initial.random.upgrade.time.set &&
        $reboot_time == "02:30" ]]; then
    # Assume this is the og default GP of 02.30 since new builds randomise between 2am to 4.59am
    # So lets randomise that shit before we sync back, as if it's a new server.
    # Since the UI has no previous state anyway
    local reboot_hour
    reboot_hour=$((RANDOM % 3 + 2))
    local reboot_min
    reboot_min=$((RANDOM % 58 + 1))
    [[ $reboot_min -lt 10 ]] &&
      reboot_min="0$reboot_min"
    reboot_time="0${reboot_hour}:${reboot_min}"
    /bin/sed -i "/Unattended-Upgrade::Automatic-Reboot-Time \"/c \Unattended-Upgrade::Automatic-Reboot-Time \"${reboot_time}\";" \
      /etc/apt/apt.conf.d/50unattended-upgrades
    # And create the token this is a one time only deal...
    touch /opt/gridpane/initial.random.upgrade.time.set
    # Now the initial stqte for the fleet should be that once this feature becomes active
  fi

  local security_updates_reboot_required
  security_updates_reboot_required="false"
  [[ -f /var/run/reboot-required ]] &&
    security_updates_reboot_required="true"

  # This will be enabled once the system settings have been in production
  # for a while and people have had a chance to set their servers to their timezones
#  if [[ $running_as_worker == "worker" &&
#        $security_updates_reboot_required == "true" &&
#        ! -f /var/run/reboot-required-callback.complete ]]; then
#    local minutes
#    minutes="0"
#    if [[ -f /opt/gridpane/first.reboot.required.callback ]]; then
#      # First ever run, maybe there is a platform wide reboot at this time... best randomise a bit more
#      minutes=$((RANDOM % 120))
#    fi
#    local seconds_to_sleep
#    seconds_to_sleep=$((RANDOM % 900))
#    echo "Reboot required, running as automated worker, callback in... ${seconds_to_sleep}s"
#    echo "sleep ${seconds_to_sleep}; \
#      /usr/local/bin/gp callback \
#        /server/server-update \
#        -- server_ip=${serverIP} \
#        security_updates_reboot_required@${security_updates_reboot_required} >/var/run/reboot-required-callback.complete" | at now + $minutes minute
#  elif [[ $running_as_worker != "worker" &&
#        $security_updates_reboot_required == "true" ]]; then
#    echo "Detected a reboot is required, this is a manual run, calling back..."
#    gridpane::callback::app \
#      "/server/server-update" \
#      "--" \
#      "server_ip=${serverIP}" \
#      "security_updates_reboot_required@${security_updates_reboot_required}"
#  else
#    echo "No reboot required..."
#  fi
}

workers::check_imagick() {
  local check_for_imagick_versions
  check_for_imagick_versions=$(/bin/grep -r imagick /etc/php)
  local install_string
  [[ $check_for_imagick_versions != *"7.1"* ]] &&
    install_string="php7.1-imagick"
  [[ $check_for_imagick_versions != *"7.2"* ]] &&
    install_string="$install_string php7.2-imagick"
  [[ $check_for_imagick_versions != *"7.3"* ]] &&
    install_string="$install_string php7.3-imagick"
  [[ $check_for_imagick_versions != *"7.4"* ]] &&
    install_string="$install_string php7.4-imagick"
  export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
  # shellcheck disable=SC2086
 [[ -n $install_string ]] &&
    gridpane::preinstallaptcheck &&
    apt-get install -y ${install_string}
}

workers::check::7g_hotfix() {
  local check_7g
  local i

  cd /var/www || exit

  for i in *; do
    
    [[ ! -f /var/www/"$i"/wp-config.php ]] && continue
      
    check_7g="$(/usr/local/bin/gpols get "$i" vhconf)"

    if [[ "$check_7g" == *"# 7G:[CORE]"* && "$check_7g" != *"# END WordPress
  }"* ]]; then

      /usr/local/bin/gpols site "$i"

    fi

  done

}

workers::prep_updated_gp_access() {
  if ! grep "gridpane-production-1" "/root/.ssh/authorized_keys" >/dev/null; then
    {
      echo ""
      echo "## GridPane Production 1"
      echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDUa8JdSOehH6s3rOXg7i7pz6pZ30QRINn0ccPFE7F4NhXLCjx2FtWCaZC9ehUp/B/KaKyq/vSqkNyqmOYCJ7RE+OTvpApUyR9Gws0+5yHfGyJt2vyj0qEIn4CFwFdZSLxARSwo6FCc6f5qEf+h4GTUU2GN/Ch7ne7x/DzMHcJGLF67buO1dJxz4Tl1ePxN/5UEh7iVhTyKNdnWYwVgnrHS/KclpLmYIiSBpGjnhBvA2aSc9mcLVH1X+UismsZTa4p9AWY77VWCT/848DD4kWpyGLBfm/CnlLE0mpwFfPm8yxX/Mwss2evS29ciEPnaPhjMMxsgiLi1df+1himuvdM0yYejlpTYtk+VL7Kr7rqLs6cW3599JDuj515dtwu3V9JxulF+TdP0aS1Msa9fuhSTlvPBmqa8WIPLVjP5IYMohC7O2OTmJdwjd/HLCA++PFT0UiiPGv6VwIBXja4mcUqLqKX/5aeOVi9CttzAcLhXHw7bYPc8D24hVUTcCPnypwc= root@gridpane-production-1"
      echo ""
    } >>/root/.ssh/authorized_keys
  fi
}
