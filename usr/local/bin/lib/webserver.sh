#!/bin/bash

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
gp_webserver() {
    local check_webserver
    check_webserver="$(/usr/bin/which nginx || true)"
    local gp_cat
    local gp_env
    gp_env=/root/gridenv/promethean.env

    if [[ -n "$check_webserver" ]]; then
        [[ -z "$webserver" ]] && readonly webserver=nginx
        [[ -z "$webserver_formatted" ]] && readonly webserver_formatted="Nginx"
    else
        [[ -z "$webserver" ]] && readonly webserver=openlitespeed
        [[ -z "$webserver_formatted" ]] && readonly webserver_formatted="OpenLiteSpeed"
    fi

    check_webserver="$(/bin/grep webserver: "$gp_env" || true)"

    if [[ -z "$check_webserver" ]]; then
        # Checks if function exists.
        if declare -F gridpane::conf_write &> /dev/null; then
            gridpane::conf_write "webserver" "$webserver"
        else
            /usr/bin/chattr -i "$gp_env"
            /bin/echo "webserver:${webserver}" >> "$gp_env"
            gp_cat="$(/bin/cat < "$gp_env" | /usr/bin/sort | /usr/bin/uniq)"
            /bin/echo "$gp_cat" > "$gp_env"
            /usr/bin/chattr +i "$gp_env"
        fi
    fi
}
gp_webserver