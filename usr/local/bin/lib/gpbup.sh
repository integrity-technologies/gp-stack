#!/bin/bash
#
#

. /usr/local/bin/lib/webserver.sh

#################################################
# Lazy split just to get it out of the way
# OG functions
#################################################

conf_read() {

    local do_nothing
    local configuration_file
    local verbose

    if [[ $1 == "v" ]] || [[ $1 == "-v" ]] \
    || [[ $1 == "verbose" ]] || [[ $1 == "-verbose" ]]
    then

        echo "You can't use the verbose option -v / -verbose here... doing nothing"
        do_nothing="not a thing"

    elif [[ $2 == "v" ]] || [[ $2 == "-v" ]] \
    || [[ $2 == "verbose" ]] || [[ $2 == "-verbose" ]] \
    || [[ -z $2 ]] || [[ $2 == "-q" ]] || [[ $2 == "-quiet" ]]
    then

        configuration_file="/root/gridenv/promethean.env"

        if [[ $2 == "v" ]] || [[ $2 == "-v" ]] \
        || [[ $2 == "verbose" ]] || [[ $2 == "-verbose" ]]
        then

            verbose="true"

        fi

    else

        if [[ $2 == "-site.env" ]] || [[ $2 == "site.env" ]]
        then

             # gp conf read $1 -site.env $3

            if [[ -z $3 ]]
            then

                echo "No site provided... do nothing"
                do_nothing="not a thing"

            else

                if [[ ! -d /var/www/$3 ]]
                then

                    echo "Site $3 doesn't exist... do nothing"
                    do_nothing="not a thing"

                fi

                configuration_file="/var/www/$3/logs/$3.env"

            fi

            if [[ $4 == "-v" ]] || [[ $4 == "v" ]] \
            || [[ $4 == "-verbose" ]] || [[ $4 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-php.env" ]] || [[ $2 == "php.env" ]]
        then

            if [[ -z $3 ]]
            then

                echo "No PHP Version Provided... do nothing"
                do_nothing="not a thing"

            else

                if  [[ $3 != *"7.1"* ]] && [[ $3 != *"7.2"* ]] && [[ $3 != *"7.3"* ]] && [[ $3 != *"7.4"* ]]
                then

                    echo "GridPane only supports PHP 7.1/2/3/4, do nothing.."
                    do_nothing="not a thing"

                fi

                if [[ ! -d /etc/php/$3 ]]
                then

                    echo "PHP version $3 hasn't been installed... do nothing"
                    do_nothing="not a thing"

                fi

                configuration_file="/root/gridenv/php$3.env"

            fi

            if [[ $4 == "-v" ]] || [[ $4 == "v" ]] \
            || [[ $4 == "-verbose" ]] || [[ $4 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-php.env.bak" ]] || [[ $2 == "php.env.bak" ]]
        then

            if [[ -z $3 ]]
            then

                echo "No PHP Version Provided... do nothing..."
                do_nothing="not a thing"

            else

                if  [[ $3 != *"7.1"* ]] && [[ $3 != *"7.2"* ]] && [[ $3 != *"7.3"* ]] && [[ $3 != *"7.4"* ]]
                then

                    echo "GridPane only supports PHP 7.1/2/3/4, do nothing"
                    do_nothing="not a thing"

                fi

                if [[ ! -d /etc/php/$3 ]]
                then

                    echo "PHP version $3 hasn't been installed... do nothing"
                    do_nothing="not a thing"

                fi

                configuration_file="/opt/gridpane/php$3.env.bak"

            fi

            if [[ $4 == "-v" ]] || [[ $4 == "v" ]] \
            || [[ $4 == "-verbose" ]] || [[ $4 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-nginx.env" ]] || [[ $2 == "nginx.env" ]]
        then

            configuration_file="/root/gridenv/nginx.env"

            if [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
            || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-nginx-bak.env" ]] || [[ $2 == "nginx-bak.env" ]]
        then

            configuration_file="/opt/gridpane/nginx.env.bak"

            if [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
            || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-sys-upgrades.env" ]] || [[ $2 == "sys-upgrades.env" ]]
        then

            configuration_file="/root/gridenv/sys-upgrades.env"

            if [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
            || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-sys-upgrades.env.bak" ]] || [[ $2 == "sys-upgrades.env.bak" ]]
        then

            configuration_file="/opt/gridpane/sys-upgrades.env.bak"

            if [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
            || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-redis.env" ]] || [[ $2 == "redis.env" ]]
        then

            configuration_file="/root/gridenv/redis.env"

            if [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
            || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-redis.env.bak" ]] || [[ $2 == "redis.env.bak" ]]
        then

            configuration_file="/opt/gridpane/redis.env.bak"

            if [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
            || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-mysql.env" ]] || [[ $2 == "mysql.env" ]]
        then

            configuration_file="/root/gridenv/mysql.env"

            if [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
            || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-mysql.env.bak" ]] || [[ $2 == "mysql.env.bak" ]]
        then

            configuration_file="/opt/gridpane/mysql.env.bak"

            if [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
            || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
            then

                verbose="true"

            fi

        else

            echo "Unrecognized GridPane Configuration Env file... do nothing"
            do_nothing="not a thing"

        fi

    fi

    if [[ -z ${do_nothing} ]]
    then

        if [[ $1 != "all" ]] && [[ $1 != "-all" ]]
        then

            val=$(grep -w "^${1}:.*" ${configuration_file} | cut -f 2 -d ':') || true

            if [[ -z ${verbose} ]]
            then

                echo "${val}"

            else

                echo "$1:${val}"
                echo "${configuration_file}"

            fi

        else

            echo "GridPane Env"
            echo "------------------------"
            cat ${configuration_file}
            echo "${configuration_file}"

        fi

    fi

}

conf_write() {

    local do_nothing
    local configuration_file
    local verbose

    if [[ -z $1 ]]
    then

        echo "Nothing passed... this could be dangerous... exiting..."
        exit 187

    fi

    if [[ $1 == "v" ]] || [[ $1 == "-v" ]] \
    || [[ $1 == "verbose" ]] || [[ $1 == "-verbose" ]] \
    || [[ $2 == "v" ]] || [[ $2 == "-v" ]] \
    || [[ $2 == "verbose" ]] || [[ $2 == "-verbose" ]]
    then

        echo "You can't use the verbose option -v / -verbose here... gonna do nothing"

        do_nothing="not a thing"

    elif [[ $3 == "v" ]] || [[ $3 == "-v" ]] \
    || [[ $3 == "verbose" ]] || [[ $3 == "-verbose" ]] \
    || [[ -z $3 ]]
    then

        configuration_file="/root/gridenv/promethean.env"

        if [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
        || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
        then

            verbose="true"

        fi

    else

        if [[ $3 == "-site.env" ]] || [[ $3 == "site.env" ]]
        then

            if [[ -z $4 ]]
            then

                echo "No site provided... gonna do nothing"
                do_nothing="not a thing"

            else

                if [[ ! -d /var/www/$4 ]]
                then

                    echo "Site $4 doesn't exist... gonna do nothing..."
                    do_nothing="not a thing"

                fi

                configuration_file="/var/www/$4/logs/$4.env"

            fi

            if [[ $5 == "-v" ]] || [[ $5 == "v" ]] \
            || [[ $5 == "-verbose" ]] || [[ $5 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $3 == "-php.env" ]] || [[ $3 == "php.env" ]]
        then

            if [[ -z $4 ]]
            then

                echo "No PHP Version Provided... gonna do nothing"
                do_nothing="not a thing"

            else

                if  [[ $4 != *"7.1"* ]] && [[ $4 != *"7.2"* ]] && [[ $4 != *"7.3"* ]] && [[ $4 != *"7.4"* ]]
                then

                    echo "GridPane only supports PHP 7.1/2/3/4, gonna do nothing"
                    do_nothing="not a thing"

                fi

                if [[ ! -d /etc/php/$4 ]]
                then

                    echo "PHP version $4 hasn't been installed... gonna do nothing"
                    do_nothing="not a thing"

                fi

                configuration_file="/root/gridenv/php$4.env"

                if [[ $5 == "-v" ]] || [[ $5 == "v" ]] \
                || [[ $5 == "-verbose" ]] || [[ $5 == "verbose" ]]
                then

                    verbose="true"

                fi

            fi

        elif [[ $3 == "-nginx.env" ]] || [[ $3 == "nginx.env" ]]
        then

            configuration_file="/root/gridenv/nginx.env"

            if [[ $4 == "-v" ]] || [[ $4 == "v" ]] \
            || [[ $4 == "-verbose" ]] || [[ $4 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $3 == "-sys-upgrades.env" ]] || [[ $3 == "sys-upgrades.env" ]]
        then

            configuration_file="/root/gridenv/sys-upgrades.env"

            if [[ $4 == "-v" ]] || [[ $4 == "v" ]] \
            || [[ $4 == "-verbose" ]] || [[ $4 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $3 == "-active-sites-env" ]] || [[ $3 == "active-sites-env" ]] \
        || [[ $3 == "-active-sites.env" ]] || [[ $3 == "active-sites.env" ]]
        then

            # gp conf write $1setting $2parameter $3

            configuration_file="/root/gridenv/active-sites.env"

            if [[ $4 == "-v" ]] || [[ $4 == "v" ]] \
            || [[ $4 == "-verbose" ]] || [[ $4 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $3 == "-redis.env" ]] || [[ $3 == "redis.env" ]]
        then

            configuration_file="/root/gridenv/redis.env"

            if [[ $4 == "-v" ]] || [[ $4 == "v" ]] \
            || [[ $4 == "-verbose" ]] || [[ $4 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $3 == "-mysql.env" ]] || [[ $3 == "mysql.env" ]]
        then

            configuration_file="/root/gridenv/mysql.env"

            if [[ $4 == "-v" ]] || [[ $4 == "v" ]] \
            || [[ $4 == "-verbose" ]] || [[ $4 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
        || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
        then

            verbose="true"

            configuration_file="/root/gridenv/promethean.env"

        else

            echo "Unrecognized GridPane Configuration Env file... gonna do nothing"
            do_nothing="not a thing"

        fi

    fi

    if [[ -z ${do_nothing} ]]
    then

        [[ ! -a ${configuration_file} ]] && sudo touch ${configuration_file}

        chattr -i ${configuration_file}

        if [[ $3 == "-active-sites-env" ]] || [[ $3 == "active-sites-env" ]] \
        || [[ $3 == "-active-sites.env" ]] || [[ $3 == "active-sites.env" ]]
        then

            sed -i "/${1}/d" ${configuration_file}
            sh -c "echo -n '$1\n' >> ${configuration_file}"

        else

            sed -i "/${1}:/d" ${configuration_file}
            sh -c "echo -n '$1:$2\n' >> ${configuration_file}"

        fi

        cat ${configuration_file} | sort > /tmp/${nonce}-sorter
        cat /tmp/${nonce}-sorter > ${configuration_file}
        rm /tmp/${nonce}-sorter

        chattr +i ${configuration_file}

        if [[ ${verbose} == "true" ]]
        then

            echo "Updated GridPane Env"

            if [[ $3 == "-active-sites-env" ]] || [[ $3 == "active-sites-env" ]] \
            || [[ $3 == "-active-sites.env" ]] || [[ $3 == "active-sites.env" ]]
            then

                echo "$1"

            else

                echo "$1:$2"

            fi

            echo "${configuration_file}"

        fi

    fi

}

conf_delete() {

    local do_nothing
    local configuration_file
    local verbose

    if [[ -z $1 ]]
    then

        echo "Nothing passed... this could be dangerous... exiting..."
        exit 187

    fi

    # post_max_size -site.env deft.one

    if [[ $1 == "v" ]] || [[ $1 == "-v" ]] \
    || [[ $1 == "verbose" ]] || [[ $1 == "-verbose" ]]
    then

        echo "You can't use the verbose option -v / -verbose here... doing nothing"
        do_nothing="not a thing"

    elif [[ $2 == "v" ]] || [[ $2 == "-v" ]] \
    || [[ $2 == "verbose" ]] || [[ $2 == "-verbose" ]] \
    || [[ -z $2 ]]
    then

         configuration_file="/root/gridenv/promethean.env"

        if [[ $2 == "-v" ]] || [[ $2 == "v" ]] \
        || [[ $2 == "-verbose" ]] || [[ $2 == "verbose" ]]
        then

            verbose="true"

        fi

    else

        if [[ $2 == "-site.env" ]] || [[ $2 == "site.env" ]]
        then

            # post_max_size -site.env deft.one

            if [[ -z $3 ]]
            then

                echo "No site provided... doing nothing"
                do_nothing="not a thing"

            else

                if [[ ! -d /var/www/$3 ]]
                then

                    echo "Site $3 doesn't exist... doing nothing"
                    do_nothing="not a thing"

                fi

                configuration_file="/var/www/$3/logs/$3.env"

            fi

            if [[ $4 == "-v" ]] || [[ $4 == "v" ]] \
            || [[ $4 == "-verbose" ]] || [[ $4 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-php.env" ]] || [[ $2 == "php.env" ]]
        then

            if [[ -z $3 ]]
            then

                echo "No PHP Version Provided... doing nothing"
                do_nothing="not a thing"

            else

                if  [[ $3 != *"7.1"* ]] && [[ $3 != *"7.2"* ]] && [[ $3 != *"7.3"* ]] && [[ $3 != *"7.4"* ]]
                then

                    echo "GridPane only supports PHP 7.1/2/3/4, doing nothing"
                    do_nothing="not a thing"

                fi

                if [[ ! -d /etc/php/$3 ]]
                then

                    echo "PHP version $3 hasn't been installed... doing nothing"
                    do_nothing="not a thing"

                fi

                configuration_file="/root/gridenv/php$3.env"

            fi

            if [[ $4 == "-v" ]] || [[ $4 == "v" ]] \
            || [[ $4 == "-verbose" ]] || [[ $4 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-nginx.env" ]] || [[ $2 == "nginx.env" ]]
        then

            configuration_file="/root/gridenv/nginx.env"

            if [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
            || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-sys-upgrades.env" ]] || [[ $2 == "sys-upgrades.env" ]]
        then

            configuration_file="/root/gridenv/sys-upgrades.env"

            if [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
            || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-active-sites-env" ]] || [[ $2 == "active-sites-env" ]] \
        || [[ $2 == "-active-sites.env" ]] || [[ $2 == "active-sites.env" ]]
        then

            configuration_file="/root/gridenv/active-sites.env"

            if [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
            || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-redis.env" ]] || [[ $2 == "redis.env" ]]
        then

            configuration_file="/root/gridenv/redis.env"

            if [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
            || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-mysql.env" ]] || [[ $2 == "mysql.env" ]]
        then

            configuration_file="/root/gridenv/mysql.env"

            if [[ $3 == "-v" ]] || [[ $3 == "v" ]] \
            || [[ $3 == "-verbose" ]] || [[ $3 == "verbose" ]]
            then

                verbose="true"

            fi

        elif [[ $2 == "-v" ]] || [[ $2 == "v" ]] \
        || [[ $2 == "-verbose" ]] || [[ $2 == "verbose" ]]
        then

            verbose="true"

            configuration_file="/root/gridenv/promethean.env"

        else

            echo "Unrecognized GridPane Configuration Env file... do nothing"
            do_nothing="not a thing"

        fi

    fi

    if [[ -z $do_nothing ]]
    then

        chattr -i "${configuration_file}"

        sed -i "/^${1}/d" "${configuration_file}"

        chattr +i "${configuration_file}"

        if [[ ${verbose} == "true" ]]
        then

            echo "Updated GridPane Env"
            echo "--------------------------"
            echo "$1 deleted"
            echo "${configuration_file}"

        fi

    fi

}

gpbup::thepurge() {

    local gpfailsync_pause
    local gpstaging_pause
    local current_cron
    local env_file

    gpfailsync_pause=$(conf_read gpfailsync-server-worker-pause)

    if [[ ${gpfailsync_pause} == "true" ]]; then

        echo "gpfailsync-server-worker-pause:true" >> /var/log/gridpane.log
        echo "Server to server sync ongoing... unable to update backups until its complete" >> /var/log/gridpane.log
        echo "server ip: ${serverIP}" >> /var/log/gridpane.log

        curl -d '{ "status":"failure", "server_ip":"'${serverIP}'" }'  -H "Content-Type: application/json" -X POST https://${GPURL}/api/server/update-backups-system?api_token=${gridpanetoken} >> /var/log/gridpane.log
        curl -d '{ "title":"Backup System Update", "body":"Update Failed!<br>GridPane Sync worker task is ongoing, please try again after it completes.", "channel":"popup_and_center", "icon":"fa-clone", "duration":"short" }'  -H "Content-Type: application/json" -X POST https://${GPURL}/api/notification?api_token=${gridpanetoken}

        exit 187

    fi

    gpstaging_pause=$(conf_read gpstaging-worker-pause)

    if [[ ${gpstaging_pause} == "true" ]]; then

        echo "gpstaging-worker-pause:true" >> /var/log/gridpane.log
        echo "Staging push ongoing... unable to update backups until its complete..." >> /var/log/gridpane.log

        curl -d '{ "status":"failure", "server_ip":"'${serverIP}'" }'  -H "Content-Type: application/json" -X POST https://${GPURL}/api/server/update-backups-system?api_token=${gridpanetoken} >> /var/log/gridpane.log
        curl -d '{ "title":"Backup System Update", "body":"Update Failed!<br>GridPane staging worker task is ongoing, please try again after it completes.", "channel":"popup_and_center", "icon":"fa-clone", "duration":"short" }'  -H "Content-Type: application/json" -X POST https://${GPURL}/api/notification?api_token=${gridpanetoken}

        exit 187

    fi

    echo "Removing old gpbup cron jobs..."
    crontab -l | grep -v "/usr/local/bin/gpbup"  | crontab  -

    echo "Removing Borg Snapshots Directory..."
    rm -rf /opt/gridpane/backups/snapshots

    echo "Setting backup system to version 2..."
    conf_write backups-version "2"
    echo "duplicacy" > /root/gridenv/backups.system

    echo "Creating new gpbup log file..."
    if [[ ! -f /opt/gridpane/gpbup.log ]]; then
        touch /opt/gridpane/gpbup.log
    fi

    echo "Adding new gpbup cron jobs..."
    current_cron="$(crontab -l)"
    current_cron+="$(echo -e "\n0 * * * * /usr/local/bin/gpbup >> /opt/gridpane/gpbup.log")"
    echo "${current_cron}" | crontab -

    echo "Adding new logrotate config for gpbup..."
    cat > /etc/logrotate.d/gpbup << EOF
/opt/gridpane/gpbup.log {
        create 0644 root root
        daily
        rotate 7
        missingok
        notifempty
        compress
        sharedscripts
}
EOF

    echo "Removing any existing backups.env files..."
    for env_file in $(find /var/www/ -name backups.env); do
     chattr -i "${env_file}"
     rm "${env_file}"
    done

    gpbup::installduplicacy || return 1

    #curl -d '{ "title":"Backup System Update", "body":"Updated Backups - The Borg have been banished to the Delta Quadrant, long live our new Robot Overlords!", "channel":"popup_and_center", "icon":"fa-clone", "duration":"short" }'  -H "Content-Type: application/json" -X POST https://${GPURL}/api/notification?api_token=${gridpanetoken}
    #curl -d '{ "status":"success", "server_ip":"'${serverIP}'" }'  -H "Content-Type: application/json" -X POST https://${GPURL}/api/server/update-backups-system?api_token=${gridpanetoken}

}

gpbup::installduplicacy() {

    echo "Installing duplicacy..." | updatesafely::showoutput
    wget --no-check-certificate -O /usr/local/bin/duplicacy https://github.com/gilbertchen/duplicacy/releases/download/v2.3.0/duplicacy_linux_x64_2.3.0 || return 1
    chmod +x /usr/local/bin/duplicacy || return 1

    if [ ! -f /usr/bin/expect ]; then
        echo "Installing expect..." | updatesafely::showoutput
        apt-get install expect -y || return 1
    fi

    mkdir -p  /opt/gridpane/backups/duplications

    return 0

}

gpbup::gettimedifference() {

    local time_1="$1"
    local time_2="$2"
    local time_increment="$3"
    local time_1_epoch
    local time_2_epoch
    local epoch_time_result
    local calculated_time_difference

    if [[ ${time_1} =~ [0-9]{10} ]]; then
        time_1_epoch="${time_1}"
    else
        time_1_epoch="$(date -d "${time_1}" +%s)"
    fi

    if [[ ${time_2} =~ [0-9]{10} ]]; then
        time_2_epoch="${time_2}"
    else
        time_2_epoch="$(date -d "${time_2}" +%s)"
    fi

    epoch_time_result=$(($time_1_epoch - $time_2_epoch))

    case ${time_increment} in
        months|monthly) calculated_time_difference=$(awk "BEGIN {print ((($epoch_time_result/3600)/24)/30)}") ;;
        weeks|weekly) calculated_time_difference=$(awk "BEGIN {print ((($epoch_time_result/3600)/24)/7)}") ;;
        days|daily) calculated_time_difference=$(awk "BEGIN {print (($epoch_time_result/3600)/24)}") ;;
        hours|hourly) calculated_time_difference=$(awk "BEGIN {print ($epoch_time_result/3600)}") ;;
        minutes) calculated_time_difference=$(($epoch_time_result/60)) ;;
        seconds) calculated_time_difference=$(($epoch_time_result)) ;;
    esac

    echo $calculated_time_difference

}

gpbup::checktimetorefresh() {

    local site_to_backup="$1"
    local service_to_check="$2"
    local last_refresh_time=$(conf_read last-backup-refresh-${service_to_check} -site.env ${site_to_backup}) || true

    # If nothing is set then make last_refresh_time 24 hours ago so we return 0.
    if [[ -z ${last_refresh_time} ]]; then
        last_refresh_time=$(date -d "24 hours ago" +%s)
    fi

    refresh_time_diff=$(gpbup::gettimedifference "now" "${last_refresh_time}" "days")

    #if [[ ${refresh_time_diff} -gt 0 ]]; then
    # set to .917 instead of 1 to allow some headroom
    if (($(awk "BEGIN {print ("${refresh_time_diff}" > "0.99" )}"))); then
        return 0
    else
        return 1
    fi

}

gpbup::updaterefreshtime() {

    local site_to_backup="$1"
    local storage_service="$2"
    local current_epoch_time=$(date +%s)

    conf_write last-backup-refresh-${storage_service} ${current_epoch_time} -site.env ${site_to_backup}

}

gpbup::updatebackuptimestamp() {

    local site_to_backup="$1"
    local storage_service="$2"
    local current_epoch_time=$(date +%s)

    conf_write backup-schedule-timestamp-${storage_service} ${START_TIME_EPOCH} -site.env ${site_to_backup}

}

gpbup::exportbackup() {

    local site_to_export="$1"
    local backup_identifier_string="$2"
    local storage_service="$3"
    local revision_to_restore
    local siteowner

    backup_identifier_string="${backup_identifier_string/\ /-}"

    if [[ -z ${site_to_export} ]]; then
        echo "No domain was provided!"
        exit 1
    fi

    if [[ ! -d "/var/www/${site_to_export}" ]]; then
        echo "Folder for ${site_to_export} does not exist!"
        exit 1
    fi

    if [[ -d "/var/www/${site_to_export}-export" ]]; then
        rm -fr "/var/www/${site_to_export}-export"
    fi

    if [[ -f "/var/www/${site_to_export}/${site_to_export}-SFTP-export.tar.gz" ]]; then
        rm "/var/www/${site_to_export}/${site_to_export}-SFTP-export.tar.gz"
    fi

    export DUPLICACY_PASSWORD="${UUID}"

    mkdir -p /var/www/${site_to_export}-export/.duplicacy
    cd /var/www/${site_to_export}-export
    rsync -avzh /var/www/${site_to_export}/.duplicacy/ /var/www/${site_to_export}-export/.duplicacy/

    sed -i "s|/var/www/${site_to_export}|/var/www/${site_to_export}-export|g" /var/www/${site_to_export}-export/.duplicacy/preferences

    if [[ "${backup_identifier_string}" != "GP-"* ]]; then
      revision_to_restore="$(gprestore::getidentifierrevision ${site_to_export} ${backup_identifier_string} ${storage_service})"
    else
      revision_to_restore="${backup_identifier_string#GP-}"
    fi    

    if [[ -z ${storage_service} || ${storage_service} == "local" ]]; then
        duplicacy restore -r ${revision_to_restore} htdocs\*
    else
        duplicacy restore -r ${revision_to_restore} -storage ${storage_service} htdocs\*
    fi

    cat << 'EOF' > htdocs/README
The database dump contained in this archive has been created using 'mydumper' and can be imported with 'myloader'. You can install 'myloader' on most linux operating systems using their respective package manager. You can also import all SQL files at once using 'mysqlimport' by specifying a wildcard for the table name. For example, `mysqlimport *.sql`.
EOF

    if [[ ${site_to_export} == *"-remote" ]]; then
        tar -zcvf "/root/${site_to_export}-SFTP-export.tar.gz" htdocs
    else
        tar -zcvf "/var/www/${site_to_export}/${site_to_export}-SFTP-export.tar.gz" htdocs
        siteowner="$(gridpane::get::set::site::user ${site_to_export})" || true
        chown "${siteowner}". "/var/www/${site_to_export}/${site_to_export}-SFTP-export.tar.gz"
    fi

    rm -fr "/var/www/${site_to_export}-export"
    
}

gpbup::getavailablebackups() {

    local site_to_backup="$1"
    local storage_service="$2"
    local backup_refresh="$3"
    local origin_server_remote_uuid="$4"
    local duplicacy_list
    local backup
    local backup_list_auto=()
    local backup_list_manual=()
    local auto_bups
    local manual_bups
    local source="original"

    if ! gpbup::checksiterunnable ${site_to_backup}; then
        exit 1
    fi

    gpbup::checkrepoinit ${site_to_backup}

    echo "Getting available backups for site ${site_to_backup}..."
    cd /var/www/${site_to_backup}

    if gpbup::checktimetorefresh ${site_to_backup} ${storage_service} || [[ -n ${backup_refresh} ]]; then

        if [[ -n ${storage_service} && ${storage_service} != "local" ]]; then
            if ! gpbup::checkforexistingremotestorage ${site_to_backup} ${storage_service}; then
                duplicacy_list="$(/usr/local/bin/duplicacy list -storage ${storage_service} | sed -e 1d)"
            else
                echo "Service ${storage_service} is not configured for ${site_to_backup}..."
                return 0
            fi
        else
            duplicacy_list="$(/usr/local/bin/duplicacy list | sed -e 1,2d || true)"
        fi

        while read -r backup; do
            # strip everything from line except last 3 fields
            backup=$(echo ${backup} | awk '{$1=$2=$3=$4=$5=$6=""; print $0}' | sed -e s/^\ *//g)
            if [[ ${backup} == *"manual"* ]]; then
                # strip 3rd field from string, we don't need it at this point
                backup=$(echo ${backup} | awk '{print $1" "$2}')
                backup_list_manual+="${backup},"
            else
                backup=$(echo ${backup} | awk '{print $1" "$2}')
                backup_list_auto+="${backup},"
            fi
        done <<< "${duplicacy_list}"

        # strip out trailing comma on CSV lists
        backup_list_manual="${backup_list_manual%?}"
        backup_list_auto="${backup_list_auto%?}"

        if [[ ! -f /var/www/${site_to_backup}/logs/backups.env ]]; then
            mkdir -p /var/www/${site_to_backup}/logs
            touch /var/www/${site_to_backup}/logs/backups.env
            echo "Backups ENVironment file for site ${site_to_backup} created..."
        fi

        sed -i "/^MANUAL-${storage_service}/ d" /var/www/${site_to_backup}/logs/backups.env
        sed -i "/^AUTO-${storage_service}/ d" /var/www/${site_to_backup}/logs/backups.env

        echo "MANUAL-${storage_service}:${backup_list_manual}" >> /var/www/${site_to_backup}/logs/backups.env
        echo "AUTO-${storage_service}:${backup_list_auto}" >> /var/www/${site_to_backup}/logs/backups.env

        gpbup::updaterefreshtime ${site_to_backup} ${storage_service}

    fi

    auto_bups="$(cat /var/www/${site_to_backup}/logs/backups.env | grep "AUTO-${storage_service}:" | sed -e "s/^AUTO-${storage_service}://")"
    manual_bups="$(cat /var/www/${site_to_backup}/logs/backups.env | grep "MANUAL-${storage_service}:" | sed -e "s/^MANUAL-${storage_service}://")"

    if [[ "${site_to_backup}" == *"-remote" ]]; then
        site_to_backup=$(echo ${site_to_backup/\-remote/})
        source="alternative"
    fi

    gridpane::callback::app \
    "/backups/available-backups" \
    "site_url=${site_to_backup}" \
    "server_ip=${serverIP}" \
    "source=${source}" \
    "origin_server_remote_uuid=${origin_server_remote_uuid}" \
    "${storage_service}={\"automatic\":\"${auto_bups}\",\"manual\":\"${manual_bups}\"}"

}

#######################################
#
#  Toggles local backups on/off for site.
#
#  Inputs:
#   1: site_to_backup (string)
#   2: backup_type (Local/Remote)
#   3: backup_toggle_value (ON/OFF)
#
#  Outputs:
#   Writes status to STDOUT
#
#  Returns:
#   0: Success
#   1: Failure
#
#  Exits:
#   1: If gpbup::checksiterunnable fails
#
#######################################
gpbup::backuptoggle() {

    local site_to_backup="$1"
    local backup_type="$2"
    local backup_toggle_value="$3"
    local platform_return
    local api_call
    local site_backup_config="/var/www/${site_to_backup}/logs/backups.env"

    if ! gpbup::checksiterunnable ${site_to_backup}; then
        exit 1
    fi

    if [[ ! -f /var/www/${site_to_backup}/logs/backups.env ]]; then
        touch /var/www/${site_to_backup}/logs/backups.env
    fi

    if [[ ! -f /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env ]]; then
        touch /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
    fi

    if [[ "${backup_type}" == "Remote" && "${backup_toggle_value}" == "ON" ]] && ! gpbup::getremoteservices ${site_to_backup} >/dev/null; then
        >&2 echo "Failed!"
        >&2 echo "There are no remote services configured for ${site_to_backup}!"
        return 1
    fi

    sed -i "s/\\n//" /var/www/${site_to_backup}/logs/backups.env
    sed -i "/^${backup_type}-Backups:/d" /var/www/${site_to_backup}/logs/backups.env
    sh -c "echo -n '${backup_type}-Backups:${backup_toggle_value}\n' >> ${site_backup_config}"
    printf "${backup_type} backups have been turned ${backup_toggle_value} at ${site_backup_config}... "

    case ${backup_type} in
        Local) platform_return="\"local_backups\":" ;;
        Remote) platform_return="\"remote_backups\":" ;;
    esac

    case ${backup_toggle_value} in
        ON) platform_return+="true" ;;
        OFF) platform_return+="false" ;;
    esac

    # TODO: (leo) remove extra api_call, compensating for the need to get a return value
    api_call="$(curl -d '{ "site_url":"'${site_to_backup}'", "server_ip":"'${serverIP}'", '${platform_return}' }'  -H "Content-Type: application/json" -X POST https://${GPURL}/api/site/site-update?api_token=${gridpanetoken} 2>&1)"

    #gridpane::callback::app \
      #"/site/site-update" \
      #"--" \
      #"site_url=${site_to_backup}" \
      #"server_ip=${serverIP}" \
      #"status=success" \
      #"${platform_return}"

    if [[ $api_call == *"${platform_return}"* ]]; then
        echo "Success!"
        gpbup::checkrepoinit ${site_to_backup} >/dev/null
        return 0
    else
        echo "Failed!"
        return 1
    fi

}

gpbup::prune() {

    local site_to_prune="$1"
    local storage_service="$2"
    local service
    local prune_retain_days
    local prune_schedule_id
    local prune_command=(duplicacy prune -t "gp-automated")

    cd /var/www/${site_to_prune}

    prune_schedule_id=$(conf_read prune-schedule-id-${storage_service} -site.env ${site_to_prune}) || true
    prune_retain_days=$(conf_read prune-schedule-retain-${storage_service} -site.env ${site_to_prune}) || true

    if [[ ${prune_retain_days} -ne 0 && -n ${prune_retain_days} ]]; then
        prune_command+=(-keep 0:${prune_retain_days})
    fi

    case ${prune_schedule_id} in
        0) prune_command+=(-keep 1:1) ;;
        1) prune_command+=(-keep 7:7 -keep 1:1) ;;
        2) prune_command+=(-keep 30:30 -keep 7:7 -keep 1:1) ;;
        3) prune_command+=(-keep 30:30 -keep 1:1) ;;
        *) prune_command+=(-keep 0:360 -keep 30:180 -keep 7:30 -keep 1:7) ;;
    esac

    if [[ -z ${storage_service} ]]; then
        for service in $(gpbup::getremoteservices ${site_to_prune} || true); do
            prune_command+=(-storage ${service})
            "${prune_command[@]}" || true
        done
    elif [[ "${storage_service}" == "local" ]]; then
        "${prune_command[@]}" || true
    else
        prune_command+=(-storage ${storage_service})
        "${prune_command[@]}" || true
    fi

}

gpbup::pruneall() {

    local site_to_prune
    local storage_service

    while read -r site_to_prune; do

        if ! gpbup::checkbackupsenabled ${site_to_prune}; then
            echo "Backups aren't enabled for ${site_to_prune}... "
        else
            check_new_scheduler="$(gridpane::conf_read v2scheduler-local -site.env ${site_to_prune}||true)"
            if [[ -z ${check_new_scheduler} ]]; then
                echo "Site doesn't have v2scheduler enabled, skipping..."
            else
                echo "Attempting to prune ${site_to_prune}... "
                gpbup::prune ${site_to_prune} local
            fi
        fi

        for storage_service in $(gpbup::getremoteservices ${site_to_prune} || true); do
            check_new_scheduler="$(gridpane::conf_read v2scheduler-${storage_service} -site.env ${site_to_prune}||true)"

            if [[ -z ${check_new_scheduler} ]]; then
                echo "Site doesn't have v2scheduler enabled, skipping..."
                continue
            fi
            gpbup::prune ${site_to_prune} ${storage_service}
        done
    done <<< "$(gpbup::getallrunnablesites)"
    
}

#######################################
#
#  Add $remote_storage_type backup service to
#  $site_to_backup.
#
#  Inputs:
#   1: site_to_backup (string)
#   2: remote_storage_type (backblaze/wasabi/aws-s3/dropbox)
#   3: remote_api (api key; string)
#   4: remote_secret (api secret key; string)
#   5: remote_region (aws region; us-east-1 default)
#
#  Outputs:
#   Writes status to STDOUT
#
#  Exits:
#   1: If gpbup::checksiterunnable or
#      gpbup::checkforexistingremotestorage fails.
#
#######################################
gpbup::addsiteremotestorage() {

    local site_to_backup="$1"
    local remote_storage="$2"
    local remote_api="$3"
    local remote_secret="$4"
    local remote_region="$5"
    local remote_uuid="$6"
    local api_call
    local site_to_backup_stripped

    if [[ ! -z "${remote_uuid}" ]]; then

        site_to_backup="${site_to_backup}-remote"
        if [[ -d "/var/www/${site_to_backup}" ]]; then
            chattr -i /var/www/${site_to_backup}/logs/${site_to_backup}.env
            rm -r "/var/www/${site_to_backup}"
        fi
        mkdir -p "/var/www/${site_to_backup}/.duplicacy"
        mkdir -p "/var/www/${site_to_backup}/logs"
        mkdir -p "/var/www/${site_to_backup}/htdocs"
        touch "/etc/nginx/sites-enabled/${site_to_backup}"
        # TODO: (leo) save this for when we're attaching remote backups vs restoring remote backups
        # echo "${remote_uuid}" | sudo tee "/var/www/${site_to_backup}/gridpane_remote.uuid"
        UUID="${remote_uuid}"

        if [[ -d /opt/gridpane/backups/${site_to_backup}-duplications ]]; then
            rm -r /opt/gridpane/backups/${site_to_backup}-duplications
        fi

    fi

    if ! gpbup::checksiterunnable ${site_to_backup}; then
        exit 1
    fi

    if ! gpbup::checkforexistingremotestorage ${site_to_backup} ${remote_storage}; then
        if [[ "${site_to_backup}" != *"-remote" ]]; then
        
            gridpane::callback::app \
            "/backups/add-remote-storage" \
            "--" \
            "site_url=${site_to_backup}" \
            "server_ip=${serverIP}" \
            "status=success" \
            "service=${remote_storage}" \
            "-s" "details=\"Finished: ${remote_storage} has been successfully added to ${site_to_backup}\""

            gridpane::notify::app \
            "Backup Add Remote Notice" \
            "Adding Remote Storage: ${remote_storage}<br>Storage was already configured on server, anomaly has been rectified with App UI." \
            "popup_and_center" \
            "fa-exclamation" \
            "long"
            exit 1
        fi
    fi

    gpbup::checkrepoinit ${site_to_backup}

    case ${remote_storage} in
        backblaze)
            gpbup::addb2remote $site_to_backup $remote_storage $remote_api $remote_secret
            echo "provider:${remote_storage}" | tee -a /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            echo "b2_id:$3" | tee -a /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            echo "b2_key:$4" | tee -a /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            ;;
        wasabi)
            gpbup::addwasabiremote $site_to_backup $remote_storage $remote_api $remote_secret $remote_region
            echo "provider:${remote_storage}" | tee -a /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            echo "wasabi_key:$3" | tee -a /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            echo "wasabi_secret:$4" | tee -a /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            ;;
        aws-s3)
            gpbup::adds3remote $site_to_backup $remote_storage $remote_api $remote_secret $remote_region
            echo "provider:${remote_storage}" | tee -a /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            echo "s3_key:$3" | tee -a /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            echo "s3_secret:$4" | tee -a /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            ;;
        dropbox)
            gpbup::adddropboxremote $site_to_backup $remote_storage $remote_api
            echo "provider:${remote_storage}" | tee -a /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            echo "drop_key:$3" | tee -a /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            ;;
        *)
            echo "We are sorry, we currently don't support the remote storage provider $remote_storage"
            exit 1
            ;;
    esac

    if [[ "${site_to_backup}" == *"-remote" ]]; then
        site_to_backup_stripped=$(echo ${site_to_backup/\-remote/})

        sed -i "s/\"id\"\: \"${site_to_backup}\"/\"id\"\: \"${site_to_backup_stripped}\"/" /var/www/${site_to_backup}/.duplicacy/preferences

# TODO (leo): removed during discussion with dimitry, remove if unecessary moving forward
#        gridpane::callback::app \
#           "/backups/add-remote-storage" \
#            "--" \
#            "site_url=${site_to_backup_stripped}" \
#            "server_ip=${serverIP}" \
#            "status=success" \
#            "service=${remote_storage}" \
#            "-s" "details=\"Finished: remote storage ${remote_storage} has been successfully added to ${site_to_backup}\""
    else
        gridpane::callback::app \
            "/backups/add-remote-storage" \
            "--" \
            "site_url=${site_to_backup}" \
            "server_ip=${serverIP}" \
            "status=success" \
            "service=${remote_storage}" \
            "-s" "details=\"Finished: ${remote_storage} has been successfully added to ${site_to_backup}\""
    fi

}

gpbup::checklocaldiskusage() {

    local site_to_backup="$1"
    local current_disk_space="$(df -h /opt/gridpane/backups/duplications | sed 1d | awk '{printf "%.0f", $5}' | sed 's/G//' || echo 100)" # default to 100% if nothing expected is returned
    local current_disk_space_remaining="$(expr 100 - ${current_disk_space} || echo 0)" # default to 0 if nothing expected is returned
    local disk_space_allowed="$(conf_read backup-max-disk-usage -site.env ${site_to_backup} || echo 10)" # default to 10% if nothing set

    if [[ ${current_disk_space_remaining} -lt ${disk_space_allowed} ]]; then
        echo "Not enough disk space left to run local backups for ${site_to_backup}..."
        gridpane::notify::app \
            "Local Backup Failed" \
            "Local backup failed for ${site_to_backup}<br>Reason: Disk has less than ${disk_space_allowed}% capacity available (${current_disk_space}%)." \
            "popup_and_center" \
            "fa-exclamation" \
            "long"
        return 1
    fi

    return 0

}

gpbup::setlocaldiskquota() {

    local site_to_backup="$1"
    local site_quota="$2"

    conf_write backup-max-disk-usage ${site_quota} -site.env ${site_to_backup}

    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=${site_to_backup}" \
      "server_ip=${serverIP}" \
      "-s" "local_backups_threshold=${site_quota}"

}

gpbup::checkbackupscheduled() {

    local site_to_backup="$1"
    local storage_service="$2"

    # temporary until we support changing the schedule on individual remote services
    if [[ ${storage_service} =~ ^(aws-s3|backblaze|dropbox|wasabi|remote)$ ]]; then
        pseudo_storage_service="remote"
    else
        pseudo_storage_service="local"
    fi

    local backup_increment_last_timestamp="$(conf_read backup-schedule-timestamp-${storage_service} -site.env ${site_to_backup} || true)"
    local backup_increment="$(conf_read backup-schedule-${pseudo_storage_service} -site.env ${site_to_backup} || true)"
    local current_time_epoch="$(date +%s)"
    local time_since_last_backup

    # default to 'hourly' if nothing is set
    if [[ -z "${backup_increment}" ]]; then
        conf_write backup-schedule-${pseudo_storage_service} hourly -site.env ${site_to_backup}
        backup_increment="hourly"
    fi

    # default to current epoch time if nothing is set
    if [[ -z "${backup_increment_last_timestamp}" ]]; then
        conf_write backup-schedule-timestamp-${storage_service} ${START_TIME_EPOCH} -site.env ${site_to_backup}
        backup_increment_last_timestamp="${START_TIME_EPOCH}"
    fi

    time_since_last_backup=$(gpbup::gettimedifference "now" "${backup_increment_last_timestamp}" "${backup_increment}")

    #if [[ ${time_since_last_backup} -gt 0 ]]; then
    # set to .99 instead of 1 to allow some headroom
    if (($(awk "BEGIN {print ("${time_since_last_backup}" > "0.99" )}"))); then
        return 0
    else
        echo "Backups for ${site_to_backup} using ${storage_service} are not yet set to run..."
        echo "backup_increment_last_timestamp: ${backup_increment_last_timestamp}"
        return 1
    fi

}

gpbup::setbackupschedule() {

    local site_to_backup="$1"
    local storage_service="$2"
    local backup_increment="$3"
    local current_time_epoch="$(date +%s)"

    if ! [[ "${storage_service}" =~ ^(remote|local|aws-s3|backblaze|dropbox|wasabi)$ ]]; then
        echo "Incorrect value specified for the storage service (${storage_service})..."
        return 1
    fi

    if ! [[ "${backup_increment}" =~ ^(hourly|daily|weekly|monthly)$ ]]; then
        echo "Incorrect increment specified (${backup_increment})..."
        return 1
    fi

    conf_write backup-schedule-${storage_service} ${backup_increment} -site.env ${site_to_backup}
    conf_write backup-schedule-timestamp-${storage_service} ${START_TIME_EPOCH} -site.env ${site_to_backup}

    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=${site_to_backup}" \
      "server_ip=${serverIP}" \
      "${storage_service}_backups_schedule=${backup_increment}"

}

gpbup::setpruneschedule() {

    local site_to_backup="$1"
    local storage_service="$2"
    local schedule_id="$3"
    local retain_days="$4"

    if ! [[ "${storage_service}" =~ ^(remote|local|aws-s3|backblaze|dropbox|wasabi)$ ]]; then
        echo "Incorrect value specified for the storage service (${storage_service})..."
        return 1
    fi

    if ! [[ "${schedule_id}" =~ ^[0-9]$ ]]; then
        echo "Incorrect value specified for the schedule id (${schedule_id})..."
        return 1
    fi

    if ! [[ ${retain_days} =~ ^[0-9]+$ ]]; then
        echo "Incorrect value specified for number of retained days (${retain_days})..."
        return 1
    fi

    #TODO (leo): modify this later when we do pruning schedules for specific remote services
    if [[ "${storage_service}" =~ ^(aws-s3|backblaze|dropbox|wasabi|remote)$ ]]; then
        storage_service="remote"
    fi

    conf_write prune-schedule-id-${storage_service} ${schedule_id} -site.env ${site_to_backup}
    conf_write prune-schedule-retain-${storage_service} ${retain_days} -site.env ${site_to_backup}

    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=${site_to_backup}" \
      "server_ip=${serverIP}" \
      "${storage_service}_prune_schedule=${schedule_id}" \
      "${storage_service}_retain_days=${retain_days}"

}

gpbup::lockprocess() {

    local lock_file="/opt/gridpane/backups/${UUID}.pid"
    local ps_list
    local pid

    if [[ -f ${lock_file} ]]; then
        printf "We can't run because there's a lock file @ ${lock_file}"
        pid=$(cat ${lock_file})
        kill_status=$(kill -0 ${pid} >/dev/null 2>&1; echo $?) || true

        if [[ ${kill_status} -eq 0 ]]; then
            printf " and process ${pid} is still running, exiting...\n"
            exit 1
        else
            ps_list="$(ps auxwww)"
            if [[ ${ps_list} != *"duplicacy"* ]]; then
            printf ". But it appears process ${pid} and duplicacy are no longer running. We're going to delete ${lock_file}...\n"
            rm ${lock_file}
            else
            printf ". Process ${pid} is not running, but a duplicacy process appears to be running. Please try again later...\n"
            exit 1
            fi
        fi
    fi

    if [[ ! -f ${lock_file} ]]; then
        pid=$$
        echo "Creating lockfile for PID ${pid}..."
        echo ${pid} > ${lock_file}
    fi

    "echo \"Deleting lockfile...\"; rm /opt/gridpane/backups/${UUID}.pid" EXIT

}

gpbup::trapadd() {
    trap_add_cmd=$1; shift
    for trap_add_name in "$@"; do
        trap -- "$(
            extract_trap_cmd() { printf '%s\n' "$3"; }
            eval "extract_trap_cmd $(trap -p "${trap_add_name}")"
            printf '%s\n' "${trap_add_cmd}"
        )" "${trap_add_name}"
    done
} 

gpbup::lockprocesssingle() {

    local site_to_lock="$1"
    local storage_service="$2"
    local ps_list
    local lock_file="/opt/gridpane/backups/${site_to_lock}-${storage_service}-${UUID}.pid"
    local pid
    local trap_array

    if [[ -f ${lock_file} ]]; then
        printf "We can't run because there's a lock file @ ${lock_file}"
        pid=$(cat ${lock_file})
        kill_status=$(kill -0 ${pid} >/dev/null 2>&1; echo $?) || true

        if [[ ${kill_status} -eq 0 ]]; then
            printf " and process ${pid} is still running, exiting...\n"
            exit 1
        else
            ps_list="$(ps auxwww)"
            if [[ ${ps_list} != *"${site_to_lock}"* ]]; then
            printf ". But it appears process ${pid} and duplicacy are no longer running. We're going to delete ${lock_file}...\n"
            rm ${lock_file}
            else
            printf ". Process ${pid} is not running, but a duplicacy process appears to be running. Please try again later...\n"
            exit 1
            fi
        fi
    fi

    if [[ ! -f ${lock_file} ]]; then
        pid=$$
        echo "Creating lockfile for PID ${pid}..."
        echo ${pid} > ${lock_file}
    fi

    if [[ -z $(trap -p EXIT) ]]; then
        trap "echo \"Deleting lockfile ${lock_file}...\"; rm ${lock_file};" EXIT
    else
        gpbup::trapadd "echo \"Deleting lockfile ${lock_file}...\"; rm ${lock_file};" EXIT
    fi

}


#######################################
#
#  Create Unique ID for backups.
#
#  Outputs:
#   Writes status to STDOUT
#   Write UUID to /root/gridcreds/gridpane.uuid
#
#######################################
gpbup::uuidgen() {

    local uuid

    if ! type uuidgen >/dev/null; then
        apt-get install -y uuid-runtime
    fi

    uuid="$(uuidgen)"

    if [[ ! -f /root/gridcreds/gridpane.uuid ]]; then
        printf "Generating UUID... " && \
            echo "${uuid}" > /root/gridcreds/gridpane.uuid && \
            gridpane::callback::app \
                "/server/server-update" \
                "server_ip=${serverIP}" \
                "UUID=${uuid}" && \
        printf "Success\n"
    fi

}

gpbup::exit_if_remote_fail() {
  local kill_status="$1"
  local remote_storage="$2"
  local site_to_backup="$3"
  local kill_message="${4:-could not authorize account}"

  if [[ ${kill_status} -ne 0 ]]; then
    echo "Unable to add ${remote_storage} to ${site_to_backup} - ${kill_message}"

    notification_body="unable to add ${remote_storage} to ${site_to_backup}.<br>"${kill_message}""

    gridpane::callback::app \
    "/backups/add-remote-storage" \
    "--" \
    "site_url=${site_to_backup}" \
    "server_ip=${serverIP}" \
    "status=failed" \
    "service=${remote_storage}" \
    "-s" "details=\"Finished: ${notification_body}\""

    gridpane::notify::app \
      "Backup Remote Error" \
      "Add Remote Storage Fail:<br>${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "long"

     exit 1
  fi

}

gpbup::adds3remote() {

    local site_to_backup="$1"
    local remote_storage="aws-s3"
    local s3_accesskey="$3"
    local s3_secretkey="$4"
    local s3_bucket_region="${5:-us-east-1}"
    local curr_time="$(date +%Y-%M-%d-%H-%M-%S)"
    local s3_bucket_name="gridpane-backups-${UUID}"
    local s3_existing_buckets
    local s3_create_bucket
    local kill_status=0

    if ! type aws >/dev/null; then
        printf "Installing AWS CLI V2... "
            curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "/opt/gridpane/awscliv2.zip" >/dev/null
            unzip /opt/gridpane/awscliv2.zip -d /opt/gridpane >/dev/null
            sudo /opt/gridpane/aws/install >/dev/null && \
        printf "Success\n"
    fi

    echo "Configuring $remote_storage remote backups for $site_to_backup..."

    cd /var/www/$site_to_backup

    printf "Backing up /var/www/$site_to_backup/.duplicacy/preferences... "
        cp /var/www/$site_to_backup/.duplicacy/preferences /var/www/$site_to_backup/.duplicacy/preferences.$curr_time && \
    printf "Success\n"

    printf "Setting up local aws profile for S3 access... "
        aws configure set aws_access_key_id $s3_accesskey --profile ${UUID} && \
        aws configure set aws_secret_access_key $s3_secretkey --profile ${UUID}
    printf "Success\n"

    #s3_existing_buckets="$(aws s3 ls --region ${s3_bucket_region} --profile ${UUID} 2>&1)" || kill_status=1
    #gpbup::exit_if_remote_fail "${kill_status}" "${remote_storage}" "${site_to_backup}" "${s3_existing_buckets}"

    #if [[ "${s3_existing_buckets}" == *"${s3_bucket_name}"* ]]; then
    #    echo "GridPane backups bucket is already in place!"
    #else
    echo "Creating GridPane backup bucket on remote..."
    s3_create_bucket="$(aws s3 mb s3://${s3_bucket_name} --region ${s3_bucket_region} --profile ${UUID} 2>&1)" || kill_status=1
    if [[ "${s3_create_bucket}" == *"BucketAlreadyOwnedByYou"* ]]; then
        echo "Bucket already exists! Skipping creation..."
        kill_status=0
    fi
    gpbup::exit_if_remote_fail "${kill_status}" "${remote_storage}" "${site_to_backup}"
    #fi

    echo "Configuring duplicacy remote backup to ${remote_storage} for site ${site_to_backup}... "
    /usr/local/bin/duplicacy-s3-expect ${remote_storage} ${site_to_backup} ${s3_bucket_region} ${s3_bucket_name} ${s3_accesskey} ${s3_secretkey} ${UUID}
    #/usr/local/bin/duplicacy add -e ${remote-storage} ${site_to_backup} s3://${s3_bucket_region}\@${s3_bucket_name} ${DUPLICACY_ENCRYPTION_KEY}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -key s3_id -value ${s3_accesskey}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -key s3_secret -value ${s3_secretkey}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -key password -value ${UUID}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -filters /var/www/${site_to_backup}/.duplicacy/filters

    echo "Happy hunting."

}

gpbup::addwasabiremote() {

    local site_to_backup="$1"
    local remote_storage="wasabi"
    local s3_accesskey="$3"
    local s3_secretkey="$4"
    local s3_bucket_region="${5:-us-east-1}"
    local curr_time="$(date +%Y-%M-%d-%H-%M-%S)"
    local s3_bucket_name="gridpane-backups-${UUID}"
    local s3_existing_buckets
    local s3_create_bucket
    local profile_name="${UUID}-${remote_storage}"
    local kill_status=0
    local expect_response

    if ! type aws >/dev/null; then
        printf "Installing AWS CLI V2... "
            curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "/opt/gridpane/awscliv2.zip" >/dev/null
            unzip /opt/gridpane/awscliv2.zip -d /opt/gridpane >/dev/null
            sudo /opt/gridpane/aws/install >/dev/null && \
        printf "Success\n"
    fi

    echo "Configuring $remote_storage remote backups for $site_to_backup..."

    cd /var/www/$site_to_backup

    printf "Backing up /var/www/$site_to_backup/.duplicacy/preferences... "
        cp /var/www/$site_to_backup/.duplicacy/preferences /var/www/$site_to_backup/.duplicacy/preferences.$curr_time && \
    printf "Success\n"

    printf "Setting up local aws profile for ${remote_storage} access... "
        aws configure set aws_access_key_id $s3_accesskey --profile ${profile_name} && \
        aws configure set aws_secret_access_key $s3_secretkey --profile ${profile_name}
    printf "Success\n"

    s3_existing_buckets="$(aws s3 ls --endpoint-url=https://s3.${s3_bucket_region}.wasabisys.com --region "${s3_bucket_region}" --profile "${profile_name}" 2>&1)" || kill_status=1
    gpbup::exit_if_remote_fail "${kill_status}" "${remote_storage}" "${site_to_backup}" "${s3_existing_buckets}"

    if [[ "${s3_existing_buckets}" == *"${s3_bucket_name}"* ]]; then
        echo "GridPane backups bucket is already in place!"
    else
        echo "Creating GridPane backup bucket on remote..."
        s3_create_bucket=$(aws s3 mb s3://${s3_bucket_name} --endpoint-url=https://s3.${s3_bucket_region}.wasabisys.com --region "${s3_bucket_region}" --profile "${profile_name}" 2>&1) || kill_status=1
        gpbup::exit_if_remote_fail "${kill_status}" "${remote_storage}" "${site_to_backup}" "${s3_create_bucket}"
    fi

    echo "Configuring duplicacy remote backup to ${remote_storage} for site ${site_to_backup}... "
    expect_response="$(/usr/local/bin/duplicacy-wasabi-expect ${remote_storage} ${site_to_backup} ${s3_bucket_region} ${s3_bucket_name} ${s3_accesskey} ${s3_secretkey} ${UUID} 2>&1)"
    if [[ ${expect_response} == *"Failed to configure the storage"* ]]; then
        expect_response=$(echo "${expect_response}" | grep "Failed to configure the storage" | tr -d "\r")
        gpbup::exit_if_remote_fail 1 "${remote_storage}" "${site_to_backup}" "${expect_response}"
    fi
    #/usr/local/bin/duplicacy add -e ${remote-storage} ${site_to_backup} s3://${s3_bucket_region}\@${s3_bucket_name} ${DUPLICACY_ENCRYPTION_KEY}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -key wasabi_key -value ${s3_accesskey}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -key wasabi_secret -value ${s3_secretkey}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -key password -value ${UUID}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -filters /var/www/${site_to_backup}/.duplicacy/filters

    echo "Happy hunting."

}

gpbup::addb2remote() {

    local site_to_backup="$1"
    local remote_storage="backblaze"
    local s3_accesskey="$3"
    local s3_secretkey="$4"
    local curr_time="$(date +%Y-%M-%d-%H-%M-%S)"
    local s3_bucket_name="gridpane-backups-${UUID}"
    local s3_bucket_name_truncated="${s3_bucket_name:0:50}" # truncated to 50 characters due to backblaze limit
    local s3_existing_buckets
    local profile_name="${UUID}-${remote_storage}"
    local kill_status=0
    local b2_authorize_account
    local b2_create_bucket

    # TODO (leo): update b2 package at a later date... today is 11/11/20..
    printf "Installing B2 CLI... "
        sudo apt-get -y install python-setuptools
        sudo pip install --upgrade pip >/dev/null && \
        sudo pip install --upgrade b2==2.0.2 >/dev/null && \
    printf "Success\n"

    echo "Configuring $remote_storage remote backups for $site_to_backup..."

    cd /var/www/$site_to_backup

    printf "Backing up /var/www/$site_to_backup/.duplicacy/preferences... "
        cp /var/www/$site_to_backup/.duplicacy/preferences /var/www/$site_to_backup/.duplicacy/preferences.$curr_time && \
    printf "Success\n"

    printf "Setting up local b2 profile for ${remote_storage} access... "
        b2_authorize_account="$(b2 authorize-account $s3_accesskey $s3_secretkey 2>&1)" || kill_status=1
        gpbup::exit_if_remote_fail "${kill_status}" "${remote_storage}" "${site_to_backup}" "${b2_authorize_account}"
    printf "Success\n"

    echo "Creating GridPane backup bucket on remote..."
    b2_create_bucket="$(b2 create-bucket ${s3_bucket_name_truncated} allPrivate 2>&1)" || kill_status=1
        
    if [[ ${kill_status} -ne 0 ]]; then
        if [[ "${b2_create_bucket}" != *"ERROR: Bucket name is already in use:"* ]]; then
            gpbup::exit_if_remote_fail "${kill_status}" "${remote_storage}" "${site_to_backup}" "${b2_create_bucket}"
        fi
    fi

    echo "Configuring duplicacy remote backup to ${remote_storage} for site ${site_to_backup}... "
    /usr/local/bin/duplicacy-b2-expect ${remote_storage} ${site_to_backup} ${s3_bucket_name_truncated} ${s3_accesskey} ${s3_secretkey} ${UUID}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -key b2_id -value ${s3_accesskey}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -key b2_key -value ${s3_secretkey}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -key password -value ${UUID}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -filters /var/www/${site_to_backup}/.duplicacy/filters

    echo "Happy hunting."

}

gpbup::adddropboxremote() {

    local site_to_backup="$1"
    local remote_storage="dropbox"
    local s3_accesskey="$3"
    local curr_time="$(date +%Y-%M-%d-%H-%M-%S)"
    local s3_bucket_name="gridpane-backups-${UUID}"
    local s3_existing_buckets
    local profile_name="${UUID}-${remote_storage}"
    local dropbox_authorize_account

    if ! type dbxcli >/dev/null; then
        printf "Installing Dropbox CLI... "
            wget --no-check-certificate -O /usr/local/bin/dbxcli https://github.com/dropbox/dbxcli/releases/download/v3.0.0/dbxcli-linux-amd64 && \
            chmod +x /usr/local/bin/dbxcli
        printf "Success\n"
    fi

    echo "Configuring $remote_storage remote backups for $site_to_backup..."

    cd /var/www/$site_to_backup

    printf "Backing up /var/www/$site_to_backup/.duplicacy/preferences... "
        cp /var/www/$site_to_backup/.duplicacy/preferences /var/www/$site_to_backup/.duplicacy/preferences.$curr_time && \
    printf "Success\n"

    echo "Configuring duplicacy remote backup to ${remote_storage} for site ${site_to_backup}... "

    dropbox_authorize_account="$(
      /usr/local/bin/duplicacy-dropbox-expect ${remote_storage} ${site_to_backup} ${s3_bucket_name} ${s3_accesskey} ${UUID}
    )" || kill_status=1
    gpbup::exit_if_remote_fail "${kill_status}" "${remote_storage}" "${site_to_backup}" "${dropbox_authorize_account}"

    #/usr/local/bin/duplicacy add -e ${remote-storage} ${site_to_backup} s3://${s3_bucket_region}\@${s3_bucket_name} ${DUPLICACY_ENCRYPTION_KEY}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -key dropbox_token -value ${s3_accesskey}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -key password -value ${UUID}
    /usr/local/bin/duplicacy set -storage ${remote_storage} -filters /var/www/${site_to_backup}/.duplicacy/filters

    echo "Happy hunting."

}

#######################################
#
#  Check if $site_to_backups is a site we
#  can run backups against.
#
#  Inputs:
#   1: site_to_backup (string)
#
#  Returns:
#   0: Runnable
#   1: Not Runnable
#
#  Outputs:
#   Write status to STDOUT
#
#######################################
gpbup::checksiterunnable() {

    local site_to_backup="$1"

    if [[ -z ${site_to_backup} || ${site_to_backup} == "-"* ]]; then
        echo "Please supply a valid site... "
        return 1
    fi

    #TODO (Jeff) @cim should the directory check for the site vhost directory not check internally for files?
    if [[   "${webserver}" == "nginx" &&
            -d "/var/www/${site_to_backup}" &&
            -f "/etc/nginx/sites-enabled/${site_to_backup}" &&
            "${site_to_backup}" != "canary."* &&
            "${site_to_backup}" != *".gridpanevps.com" ||
            "${webserver}" == "openlitespeed" &&
            -d "/var/www/${site_to_backup}" &&
            -d "/usr/local/lsws/conf/vhosts/${site_to_backup}" &&
            "${site_to_backup}" != "canary."* 
            && "${site_to_backup}" != *".gridpanevps.com" ]]; then
        return 0
    else
        echo "The site ${site_to_backup} doesn't exist or is a staging/canary site..."
        return 1
    fi

}

#######################################
#
#  Check if $remote_type backups are enabled
#  for $site_to_backups. If $remote_type is
#  omitted then the return code will be based on
#  whether ANY backup types are enabled.
#
#  Inputs:
#   1: site_to_backup (string)
#   2: backup_type (optional; Local/Remote)
#
#  Returns:
#   0: Backups enabled
#   1: Backups not enabled
#
#  Outputs:
#   Write status to STDOUT
#
#######################################
gpbup::checkbackupsenabled() {

    local site_to_backup="$1"
    local backup_type="$2"
    local site_backup_config="$(cat /var/www/${site_to_backup}/logs/backups.env 2>/dev/null)"

    ### TDOO: (leo) Check if Remote-Backups or Local-Backups exist in $site_backup_config. If not, then pull values from API endpoint
    #

    if [[ -z $backup_type ]]; then
        if [[ "${site_backup_config}" == *"Remote-Backups:ON"* || "${site_backup_config}" == *"Local-Backups:ON"*  ]]; then
            echo "Backups are enabled..."
            return 0
        else
            echo "No backups are enabled..."
            return 1
        fi
    else
        if [[ "${site_backup_config}" == *"${backup_type}-Backups:ON"* ]]; then
            echo "${backup_type} backups are enabled for ${site_to_backup}..."
            return 0
        else
            echo "${backup_type} backups are NOT enabled for ${site_to_backup}..."
            return 1
        fi
    fi

}

#######################################
#
#  Get list of all sites and echo ones
#  which we can run backups against.
#
#  Outputs:
#   Writes all runnable sites to STDOUT
#
#######################################
gpbup::getallrunnablesites() {

    local site
    local site_dir
    
    if [[ "$webserver" = nginx ]]; then
        site_dir="$(/bin/dir /etc/nginx/sites-enabled)"
    else
        site_dir="$(/bin/dir /usr/local/lsws/conf/vhosts)"
    fi

    for site in $site_dir; do
        # @jeff I am disabling below because awk is filtering the dir output using / which the sites don't have? 
        # Nothing is outputted. Please verify.
        #site="$(echo "$site" | awk -F'/' '{print $5}')"
        if gpbup::checksiterunnable $site >/dev/null; then
            echo $site
        fi
    done

}

#######################################
#
#  Run $remote_storage_type backup service for
#  $site_to_backup.
#
#  Inputs:
#   1: site_to_backup (string)
#   2: remote_storage_type (backblaze/wasabi/aws-s3/dropbox)
#
#  Outputs:
#   Writes status to STDOUT
#
#  Exits:
#   1: If gpbup::checksiterunnable or
#      gpbup::checkforexistingremotestorage fails.
#
#######################################
gpbup::runsiteremotestorage() {

    local site_to_backup="$1"
    local remote_storage="$2"

    if ! gpbup::checksiterunnable ${site_to_backup}; then
        exit 1
    fi

    if gpbup::checkforexistingremotestorage ${site_to_backup} ${remote_storage}; then
        exit 1
    fi

    gpbup::checkrepoinit ${site_to_backup}

    printf "Attempting to run ${remote_storage} backup for ${site_to_backup}... "

    case ${remote_storage} in
        backblaze)
            duplicacy backup -storage ${remote_storage} && \
            printf "Success!" ;;
        wasabi)
            duplicacy backup -storage ${remote_storage} && \
            printf "Success!" ;;
        aws-s3)
            duplicacy backup -storage ${remote_storage} && \
            printf "Success!" ;;
        dropbox)
            duplicacy backup -storage ${remote_storage} && \
            printf "Success!\n" ;;
        *)
            printf "Failure!\n"
            exit 1
            ;;
    esac

}

gpbup::getavailableservices() {

    local site_to_backup="$1"
    local available_services=()
    local available_services_csv

    while read -r service; do
        available_services+=("${service}")
    done <<< "$(gpbup::getremoteservices ${site_to_backup})"

    # shellcheck disable=SC2145
    echo "The following services are available for ${site_to_backup}: ${available_services[@]}"

    available_services_csv=$(printf "%s\n" "${available_services[@]}" | paste -sd ',')

    # TODO: (leo) send payload to api with csv list
}

#######################################
#
#  Get list of all configured remote services
#  for $site_to_backup. If no services exist
#  then display a message notating such.
#
#  Outputs:
#   Writes all services for $site_to_backup to STDOUT or
#   output error if none exist.
#
#######################################
gpbup::getremoteservices() {

    local service
    local site_to_backup="$1"
    local site_remote_config

    if [[ ! -f /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env ]]; then
        return 1
    fi

    site_remote_config="$(cat /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env | grep ^provider | awk -F':' '{print $2}' || true)"

    if [[ -z ${site_remote_config} ]]; then
        >&2 echo "There are no remote services setup for ${site_to_backup}..."
        return 1
    fi

    for service in ${site_remote_config}; do
        echo $service
    done

    return 0

}

#######################################
#
#  Check to see if $remote_storage_type is
#  already being used for $site_to_backup.
#
#  Inputs:
#   1: site_to_backup (string)
#   2: remote_storage_type (backblaze/wasabi/aws-s3/dropbox)
#
#  Returns:
#   0: Service not in use
#   1: Service already in use
#
#  Outputs:
#   Writes status to STDOUT
#
#######################################
gpbup::checkforexistingremotestorage() {

    local site_to_backup="$1"
    local remote_storage_type="$2"
    local existing_storage_type
    local return_code=0
    local site_remote_backup_config="/var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env"

    if [[ -f ${site_remote_backup_config} ]]; then
        for existing_storage_type in $(cat ${site_remote_backup_config} | grep provider | awk -F':' '{print $2}' || true); do
            if [[ "${remote_storage_type}" == "${existing_storage_type}" ]]; then
                >&2 echo "Remote storage service ${existing_storage_type} already exists for ${site_to_backup}... "
                ((return_code++))
            fi
        done
        return ${return_code}
    else
        return ${return_code}
    fi

}

#######################################
#
#  Delete stuff?
#
#  Outputs:
#   Writes status to STDOUT
#
#  TODO:
#   Make this do something...
#
#######################################
gpbup::deletesiteremotestorage() {


    local site_to_backup="$1"
    local remote_storage_type

    if [[ -f /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env ]]; then
        echo "Removing ${site_to_backup} remote backup config:"
        echo "/var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env"

        mv /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env-$(date +%S)
    else
        echo "No ${site_to_backup} remote backup config to remove..."
    fi

}

gpbup::deletesiteremotestoragenew() {

    local site_to_backup="$1"
    local remote_storage="$2"

    if ! gpbup::checksiterunnable ${site_to_backup}; then
        exit 1
    fi

    if gpbup::checkforexistingremotestorage ${site_to_backup} ${remote_storage}; then
        echo "${remote_storage} does not exist for ${site_to_backup}... "

        gridpane::callback::app \
          "/backups/delete-remote-storage" \
          "--" \
          "site_url=${site_to_backup}" \
          "server_ip=${serverIP}" \
          "status=success" \
          "service=${remote_storage}" \
          "-s" "details=\"Finished: ${remote_storage} has been successfully deleted from ${site_to_backup}\""

        gridpane::notify::app \
          "Backup Remove Remote Notice" \
          "Removing Remote Storage: ${remote_storage}<br>Storage was not configured on server, anomaly has been rectified with App UI>" \
          "popup_and_center" \
          "fa-exclamation" \
          "long"

        exit 1
    fi

    case ${remote_storage} in
        backblaze)
            sed -i "/b2_id\|b2_key\|provider:${remote_storage}/d" /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            echo "Deleted ${remote_storage} for ${site_to_backup}... "
            ;;
        wasabi)
            sed -i "/wasabi_key\|wasabi_secret\|provider:${remote_storage}/d" /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            echo "Deleted ${remote_storage} for ${site_to_backup}... "
            ;;
        aws-s3)
            sed -i "/s3_key\|s3_secret\|provider:${remote_storage}/d" /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            echo "Deleted ${remote_storage} for ${site_to_backup}... "
            ;;
        dropbox)
            sed -i "/drop_key\|provider:${remote_storage}/d" /var/www/${site_to_backup}/logs/${site_to_backup}-remote.backup.env
            echo "Deleted ${remote_storage} for ${site_to_backup}... "
            ;;
    esac

    gridpane::callback::app \
      "/backups/delete-remote-storage" \
      "--" \
      "site_url=${site_to_backup}" \
      "server_ip=${serverIP}" \
      "status=success" \
      "service=${remote_storage}" \
      "-s" "details=\"Finished: ${remote_storage} has been successfully deleted from ${site_to_backup}\""

}

gpbup::runandreturn() {

    local return

    $@

    return=$?

    if [[ ${return} -eq 0 ]]; then
        printf "Success!\n"
        return ${return}
    else
        printf "Failure!\n"
        return ${return}
    fi
}

gpbup::backupallsites() {

    local backup_type="$1"
    local backup_tag="$2"
    local site_to_backup
    local check_new_scheduler

    while read -r site_to_backup; do
        if ! gpbup::checkbackupsenabled ${site_to_backup}; then
            echo "Backups aren't enabled for ${site_to_backup}... "
            continue
        fi
        echo "Attempting to backup ${site_to_backup}... "
        gpbup::backup ${site_to_backup} ${backup_type} ${backup_tag}
    done <<< "$(gpbup::getallrunnablesites)"

}

gpbup::localrepohealthcheck() {

    local site_to_backup="$1"
    local broken_revision
    local site_snapshot_folder
    local duplicacy_exit_code

    cd /var/www/${site_to_backup}

    IFS=$'\n'
    local duplicacy_check_array=($(/usr/local/bin/duplicacy check; duplicacy_exit_code=$?)) || true
    unset IFS

    echo "Checking repo health..."

    if [[ ${duplicacy_exit_code} -ne 0 ]]; then
        echo "Duplicacy exited with error code ${duplicacy_exit_code}..."
    fi

    for line in "${duplicacy_check_array[@]}"; do
        if [[ "$line" == "Some chunks"* && "$line" == *"are missing" ]]; then
            broken_revision=$(echo "${line}" | grep -o "revision\ [0-9]\+" | awk '{print $2}')
            site_snapshot_folder="$(echo "${line}" | grep -o "gridpane-[-a-z0-9.]\+")"
            echo "Found an issue with revision ${broken_revision} in ${site_snapshot_folder}..."
            if [[ -f /opt/gridpane/backups/duplications/snapshots/${site_snapshot_folder}/${broken_revision} ]]; then
                echo "Deleting revision ${broken_revision} in ${site_snapshot_folder} repo..."
                rm /opt/gridpane/backups/duplications/snapshots/${site_snapshot_folder}/${broken_revision}
            fi
        fi
    done

}

gpbup::setprunetime() {
    local prune_time=($1)
    local cron_array=()
    local check_new_scheduler

    if [[ -z ${prune_time[@]} ]]; then
        echo "Please specify a prune time."
    fi

    cron_array=("${prune_time[1]}" "${prune_time[0]}" "*" "*" "*" root /usr/local/bin/gpbup -run-prune-all)

    find /etc/cron.d -type f -name "backup-prune-job" -delete

    echo "${cron_array[@]}" | tee -a /etc/cron.d/backup-prune-job

}

gpbup::setbackuptime() {
    local site_to_backup="$1"
    local site_to_backup_formatted="${site_to_backup//./-}"
    local storage_service="$2"
    local backup_interval="$3"
    local backup_time=($4)
    local cron_array=()
    local check_new_scheduler

    if [[ -z ${backup_time[@]} ]]; then
        echo "Please specify a backup time."
    fi

    case ${backup_interval} in
        hourly) cron_array=("${backup_time[0]}" "*" "*" "*" "*") ;;
        daily) cron_array=("${backup_time[0]}" "${backup_time[1]}" "*" "*" "*") ;;
        weekly) cron_array=("${backup_time[0]}" "${backup_time[1]}" "*" "*" ${backup_time[2]}) ;;
        monthly) cron_array=("${backup_time[0]}" "${backup_time[1]}" "${backup_time[2]}" "*" "*" "*") ;;
    esac

    cron_array+=(root /usr/local/bin/gpbup ${site_to_backup} -run-single-site ${storage_service})

    find /etc/cron.d -regex "/etc/cron.d/backup-.*-${storage_service}-${site_to_backup_formatted}" -delete

    echo "${cron_array[@]}" | tee -a /etc/cron.d/backup-${backup_interval}-${storage_service}-${site_to_backup_formatted}

    check_new_scheduler="$(gridpane::conf_read v2scheduler-${storage_service} -site.env ${site_to_backup}||true)"

    if [[ -z ${check_new_scheduler} ]]; then
        gridpane::conf_write v2scheduler-${storage_service} true -site.env ${site_to_backup}
    fi

}

gpbup::setconcurrentjobs() {
    local num_jobs="$1"

    gridpane::conf_write v2concurrency ${num_jobs}
}

gpbup::purge() {

    local site_to_purge="$1"
    local storage_service="$2"
    local backup_identifier_string="$3"
    local backup_identifier_string_2="$4"
    local revision_to_purge
    local revision_to_purge_2

    export DUPLICACY_PASSWORD="${UUID}"

    cd /var/www/${site_to_purge}/htdocs

    revision_to_purge="$(gprestore::getidentifierrevision ${site_to_purge} ${backup_identifier_string} ${storage_service})"
    
    if [[ ! -z ${backup_identifier_string_2} ]]; then
        revision_to_purge_2="$(gprestore::getidentifierrevision ${site_to_purge} ${backup_identifier_string_2} ${storage_service})"

        echo "Pruning revisions between, and including, ${revision_to_purge} and ${revision_to_purge_2} from ${site_to_purge}." | updatesafely::showandlogoutputindented ${LOG_FILE}
        if [[ -z ${storage_service} || "${storage_service}" == "local" ]]; then
            duplicacy prune -r ${revision_to_purge}-${revision_to_purge_2} -exclusive -exhaustive | updatesafely::showandlogoutputindentednocolor ${LOG_FILE}
        else
            duplicacy prune -r ${revision_to_purge}-${revision_to_purge_2} -storage ${storage_service} -exclusive -exhaustive | updatesafely::showandlogoutputindentednocolor ${LOG_FILE}
        fi
    else
        echo "Pruning revision ${revision_to_purge} from ${site_to_purge}." | updatesafely::showandlogoutputindented ${LOG_FILE}
        if [[ -z ${storage_service} || "${storage_service}" == "local" ]]; then
            duplicacy prune -r ${revision_to_purge} -exclusive -exhaustive | updatesafely::showandlogoutputindentednocolor ${LOG_FILE}
        else
            duplicacy prune -r ${revision_to_purge} -storage ${storage_service} -exclusive -exhaustive | updatesafely::showandlogoutputindentednocolor ${LOG_FILE}
        fi
    fi

    gpbup::getavailablebackups "${site_to_purge}" "${storage_service}" refresh

    gridpane::callback::app \
      "/backups/purge" \
      "--" \
      "site_url=${site_to_backup}" \
      "server_ip=${serverIP}" \
      "status=success"

}

#######################################
#
#  Callback Success or Failure of manual backup to app
#  $backup_type with tag set to $backup_tag (if specified).
#
#  Inputs:
#   1: return (int)
#   2: site (site.url)
#   3: backup_type (local|aws-s3|backblaze|wasabi|dropbox)
#
#  Outputs:
#   Curl to /backups/backup to reenable button functionality
#   Curl to notification endpoint to display in app
#
#######################################
gpbup::callback::manual_backup_status() {
  local return="$1"
  local site="$2"
  local backup_type="$3"
  local failure_reason="$4"
  local status
  local notification_icon
  local notification_duration
  local notification_body

  if [[ ${return} -eq 0 ]]; then
    status="success"
    notification_icon="fa-check"
    notification_duration="long"
  else
    status="failed"
    notification_icon="fa-exclamation"
    notification_duration="infinity"
    [[ -z ${failure_reason} ]] && failure_reason="| Duplicacy Backup failed, please check logs..."
  fi
  notification_body="Finished: ${site_to_backup} - ${backup_type} backup ${status} ${failure_reason}"

  gridpane::callback::app \
    "/backups/backup" \
    "--" \
    "site_url=${site}" \
    "server_ip=${serverIP}" \
    "status=${status}" \
    "service=${backup_type}" \
    "-s" "details=\"${notification_body}\""

  gridpane::notify::app \
    "Backup ${status}" \
    "${notification_body}" \
    "popup_and_center" \
    "${notification_icon}" \
    "${notification_duration}"

}

gpbup::duplicacy_logging() {
  local duplicacy_backup_status="$1"
  local duplicacy_backup_output="$2"
  local automated_or_manual="$3"
  local backup_datetime
  backup_datetime=$(date)
  backup_datetime=$(echo "${backup_datetime}" | tr ' ' '-')
  {
    echo "------------------------------------------------------------------------------"
    if [[ $duplicacy_backup_status != 0 ]]; then
      echo "${backup_datetime} | $site_to_backup | Duplicacy Fail {${duplicacy_backup_status}}"
      echo "${duplicacy_backup_output}"
      echo "$automated_or_manual Backup Failed!"
    else
      echo "${backup_datetime} | $site_to_backup | Duplicacy Success {${duplicacy_backup_status}}"
      grep "Backup .* revision .* completed" <<<"${duplicacy_backup_output}" || true
      grep "Total running time" <<<"${duplicacy_backup_output}" || true
    fi
  } | tee -a /opt/gridpane/active.backups.monitoring.log
}

#######################################
#
#  Run backup for $site_to_backup using
#  $backup_type with tag set to $backup_tag (if specified).
#
#  Inputs:
#   1: site_to_backup (string)
#   2: backup_type (Local/Remote)
#   3: backup_tag (optional; limit to 6 characters)
#
#  Outputs:
#   Writes status to STDOUT
#
#  Exits:
#   1: If gpbup::checksiterunnable or
#      gpbup::checkbackupsenabled fails.
#
#######################################
gpbup::backup() {

    local site_to_backup="$1"
    local backup_type="$2"
    local backup_tag="$3"
    local remote_service
    local user_defined_tag
    local wp_db_name
    local wp_db_user
    local wp_db_pass
    local log_backup_type
    local duplicacy_backup_output
    local duplicacy_backup_return=0
    local check_new_scheduler
    if [[ ${backup_type} == "Local" ]]; then
        user_defined_tag="$4"
    elif [[ ${backup_type} == "Remote" ]]; then
        remote_service="$4"
        user_defined_tag="$5"
    fi

    if ! gpbup::checksiterunnable "${site_to_backup}"; then
        exit 1
    fi
    gpbup::checkrepoinit "${site_to_backup}"
    gpbup::localrepohealthcheck "${site_to_backup}"

    export DUPLICACY_PASSWORD="${UUID}"

    cd "/var/www/${site_to_backup}" || return

    if [[ -z ${backup_type} ]]; then

        if gpbup::checkbackupsenabled "${site_to_backup}" Remote; then
            local type_available
            # shellcheck disable=SC2162
            local log_backup_type="Automated Remote"
                if gpbup::getremoteservices "${site_to_backup}"; then
                    while read type_available; do
                        check_new_scheduler="$(gridpane::conf_read v2scheduler-${type_available} -site.env ${site_to_backup}||true)"
                        if [[ ${check_new_scheduler} == "true" ]]; then
                            echo "Site has new scheduler enabled for ${site_to_backup} and remote service ${type_available}, skipping..."
                            continue
                        fi
                        if gpbup::checkbackupscheduled "${site_to_backup}" "${type_available}"; then
                            gpbup::lockprocesssingle ${site_to_backup} ${type_available}
                            echo "Initiating full automated backup to ${type_available}" | tee -a /opt/gridpane/active.backups.monitoring.log
                            if gpbup::dobackup "${site_to_backup}" "${log_backup_type}"; then
                            duplicacy_backup_output=$(/usr/local/bin/duplicacy backup -storage "${type_available}" -t "gp-automated" -stats 2>&1 || duplicacy_backup_return=$? )
                            gpbup::duplicacy_logging "${duplicacy_backup_return}" "${duplicacy_backup_output}" "${log_backup_type}"
                            gpbup::dobackup::post "${site_to_backup}"
                            gpbup::updatebackuptimestamp "${site_to_backup}" "${type_available}"
                            gpbup::prune ${site_to_backup} ${type_available}
                            fi
                        fi
                    done <<< "$(gpbup::getremoteservices "${site_to_backup}")"
                fi
        fi

        if gpbup::checkbackupsenabled "${site_to_backup}" Local; then
            check_new_scheduler="$(gridpane::conf_read v2scheduler-local -site.env ${site_to_backup}||true)"
            if [[ ${check_new_scheduler} == "true" ]]; then
                echo "Site has new scheduler enabled for ${site_to_backup}, skipping..."
            else
                if gpbup::checkbackupscheduled "${site_to_backup}" local; then
                    local log_backup_type="Automated Local"
                    if gpbup::checklocaldiskusage "${site_to_backup}"; then
                        gpbup::lockprocesssingle ${site_to_backup} local
                        echo "Initiating full automated backup to Local..." | tee -a /opt/gridpane/active.backups.monitoring.log
                        if gpbup::dobackup "${site_to_backup}" "${log_backup_type}"; then
                            duplicacy_backup_output=$(/usr/local/bin/duplicacy backup -t "gp-automated" -stats 2>&1 || duplicacy_backup_return=$? )
                            gpbup::duplicacy_logging "${duplicacy_backup_return}" "${duplicacy_backup_output}" "${log_backup_type}"
                            gpbup::dobackup::post "${site_to_backup}"
                            gpbup::updatebackuptimestamp "${site_to_backup}" local
                            gpbup::prune ${site_to_backup} local
                        fi
                    else
                        local failure_reason="Unable to push full manual backup to Local... storage threshold exceeded..."
                        gpbup::duplicacy_logging 1 "${failure_reason}" "${log_backup_type}"
                    fi
                fi
            fi
        fi

    elif [[ ${backup_type} =~ ^(aws-s3|backblaze|dropbox|wasabi)$ ]]; then
        if gpbup::checkbackupsenabled "${site_to_backup}" Remote; then
            gpbup::lockprocesssingle ${site_to_backup} ${backup_type}
            local type_available
            # shellcheck disable=SC2162
            local log_backup_type="Automated ${backup_type}"
            echo "Initiating full automated backup to ${backup_type}" | tee -a /opt/gridpane/active.backups.monitoring.log
            if gpbup::dobackup "${site_to_backup}" "${log_backup_type}"; then
                duplicacy_backup_output=$(/usr/local/bin/duplicacy backup -storage "${backup_type}" -t "gp-automated" -stats 2>&1 || duplicacy_backup_return=$? )
                gpbup::duplicacy_logging "${duplicacy_backup_return}" "${duplicacy_backup_output}" "${log_backup_type}"
                gpbup::dobackup::post "${site_to_backup}"
            fi
        fi

    elif [[ ${backup_type} == "local" ]]; then
        if gpbup::checkbackupsenabled "${site_to_backup}" Local; then
            gpbup::lockprocesssingle ${site_to_backup} local
            local log_backup_type="Automated Local"
            if gpbup::checklocaldiskusage "${site_to_backup}"; then
                echo "Initiating full automated backup to Local..." | tee -a /opt/gridpane/active.backups.monitoring.log
                if gpbup::dobackup "${site_to_backup}" "${log_backup_type}"; then
                    duplicacy_backup_output=$(/usr/local/bin/duplicacy backup -t "gp-automated" -stats 2>&1 || duplicacy_backup_return=$? )
                    gpbup::duplicacy_logging "${duplicacy_backup_return}" "${duplicacy_backup_output}" "${log_backup_type}"
                    gpbup::dobackup::post "${site_to_backup}"
                    gpbup::updatebackuptimestamp "${site_to_backup}" local
                fi
            else
                local failure_reason="Unable to push full manual backup to Local... storage threshold exceeded..."
                gpbup::duplicacy_logging 1 "${failure_reason}" "${log_backup_type}"
            fi
        fi

    elif [[ ${backup_type} == "Remote" ]]; then
        local log_backup_type="Manual Remote"
        gpbup::lockprocesssingle ${site_to_backup} remote
        if gpbup::dobackup "${site_to_backup}" "${log_backup_type}"; then
            if [[ -z "${remote_service}" ]]; then
                local type_available
                local return_status
                # shellcheck disable=SC2162

                if gpbup::getremoteservices "${site_to_backup}"; then
                    while read type_available; do
                        echo "Pushing full manual backup to ${type_available} alongside all remote storage options..." | tee -a /opt/gridpane/active.backups.monitoring.log
                        duplicacy_backup_output=$(/usr/local/bin/duplicacy backup -storage "${type_available}" -stats 2>&1 || duplicacy_backup_return=$? )
                        gpbup::duplicacy_logging "${duplicacy_backup_return}" "${duplicacy_backup_output}" "${log_backup_type}"
                        gpbup::callback::manual_backup_status "${duplicacy_backup_return}" "${site_to_backup}" "${type_available}"
                        gpbup::updatebackuptimestamp "${site_to_backup}" "${type_available}"
                        gpbup::getavailablebackups "${site_to_backup}" "${type_available}" refresh
                    done <<< "$(gpbup::getremoteservices "${site_to_backup}")"
                fi
            else
                echo "Pushing full manual backup to ${remote_service}..." | tee -a /opt/gridpane/active.backups.monitoring.log
                local return_status
                if [[ -z ${backup_tag} ]]; then
                    duplicacy_backup_output=$(/usr/local/bin/duplicacy backup -storage "${remote_service}" -t "$(date +%s)" -stats 2>&1 || duplicacy_backup_return=$? )
                else
                    if [[ -z ${user_defined_tag} ]]; then
                        duplicacy_backup_output=$(/usr/local/bin/duplicacy backup -storage "${remote_service}" -t "$(date +%s)-${backup_tag}" -stats 2>&1 || duplicacy_backup_return=$? )
                    else
                        duplicacy_backup_output=$(/usr/local/bin/duplicacy backup -storage "${remote_service}" -t "$(date +%s)-${backup_tag}-${user_defined_tag}" -stats 2>&1 || duplicacy_backup_return=$? )
                    fi
                fi
                gpbup::duplicacy_logging "${duplicacy_backup_return}" "${duplicacy_backup_output}" "${log_backup_type}"
                gpbup::callback::manual_backup_status "${duplicacy_backup_return}" "${site_to_backup}" "${remote_service}"
                gpbup::updatebackuptimestamp "${site_to_backup}" "${remote_service}"
            fi

            gpbup::dobackup::post "${site_to_backup}"

        else
            if [[ -z "${remote_service}" ]]; then
                gpbup::callback::manual_backup_status 1 "${site_to_backup}" "${type_available}" \
                  "Unable to push full manual backup to ${remote_service} - MySQL Dump failed, please check logs..."
            else
                gpbup::callback::manual_backup_status 1 "${site_to_backup}" "${remote_service}" \
                  "Unable to push full manual backup to All Remotes - MySQL Dump failed, please check logs..."
            fi
        fi

    elif [[ ${backup_type} == "Local" ]]; then
        local log_backup_type="Manual Local"
        gpbup::lockprocesssingle ${site_to_backup} local
        if gpbup::dobackup "${site_to_backup}" "${log_backup_type}"; then

            if gpbup::checklocaldiskusage "${site_to_backup}"; then
                echo "Pushing full manual backup to ${backup_type}..." | tee -a /opt/gridpane/active.backups.monitoring.log
                local return_status
                if [[ -z ${backup_tag} ]]; then
                    duplicacy_backup=$(/usr/local/bin/duplicacy backup -t "$(date +%s)" -stats 2>&1)
                    return_status="$?"
                else
                    if [[ -z ${user_defined_tag} ]]; then
                        duplicacy_backup=$(/usr/local/bin/duplicacy backup -t "$(date +%s)-${backup_tag}" -stats 2>&1)
                        return_status="$?"
                    else
                        duplicacy_backup=$(/usr/local/bin/duplicacy backup -t "$(date +%s)-${backup_tag}-${user_defined_tag}" -stats 2>&1)
                        return_status="$?"
                    fi
                fi
                gpbup::duplicacy_logging "${return_status}" "${duplicacy_backup}" "${log_backup_type}"
                gpbup::callback::manual_backup_status ${return_status} "${site_to_backup}" "local"
                gpbup::updatebackuptimestamp "${site_to_backup}" local
                gpbup::getavailablebackups "${site_to_backup}" local refresh
            else
                local failure_reason="Unable to push full manual backup to ${backup_type}... storage threshold exceeded..."
                gpbup::duplicacy_logging 1 "${failure_reason}" "${log_backup_type}"
                gpbup::callback::manual_backup_status 1 "${site_to_backup}" "local" "${failure_reason}"
            fi

            gpbup::dobackup::post "${site_to_backup}"

        else
            gpbup::callback::manual_backup_status 1 "${site_to_backup}" "local" \
              "Unable to push full manual backup to ${backup_type} - MySQL Dump failed, please check logs..."
        fi
    fi

}

gpbup::dobackup() {
    local site_to_backup="$1"
    local backup_type="$2"
    local wp_db_user
    local wp_db_pass
    local wp_db_name
    local wp_db_host
    local wp_db_port
    local tableprefix
    local return_code=0

    if ! type mydumper >/dev/null; then
        apt-get -y install mydumper
    fi
    
    cd "/var/www/${site_to_backup}/htdocs" || return

    ## TODO: (leo) Find a way to verify a valid wordpress install, that's functional, exists.

    # shellcheck disable=SC2091
    if [[ ! -f "/var/www/${site_to_backup}/wp-config.php" ]]; then
        echo "This is not a valid WordPress install, skipping!!!" | tee -a /var/log/gridpane.log
        return 0
    else
        echo "It IS a Valid WP Site..."
        echo "Exporting Database..."

        [[ ! -f /opt/gridpane/active.backups.monitoring.log ]] &&
          touch /opt/gridpane/active.backups.monitoring.log

        local db_folder_name
        db_folder_name="/var/www/${site_to_backup}/htdocs/database-${START_TIME_EPOCH}"

        # shellcheck disable=SC2002
        wp_db_name=$(cat "/var/www/${site_to_backup}/wp-config.php" | grep DB_NAME | tr \" \' | cut -d \' -f 4)
        # shellcheck disable=SC2002
        wp_db_user=$(cat "/var/www/${site_to_backup}/wp-config.php" | grep DB_USER | tr \" \' | cut -d \' -f 4)
        # shellcheck disable=SC2002
        wp_db_pass=$(cat "/var/www/${site_to_backup}/wp-config.php" | grep DB_PASSWORD | tr \" \' | cut -d \' -f 4)
        # shellcheck disable=SC2002
        wp_db_host=$(cat "/var/www/${site_to_backup}/wp-config.php" | grep DB_HOST | tr \" \' | cut -d \' -f 4)
        wp_db_port=$(echo ${wp_db_host} | awk -F':' '{print $2}')
        wp_db_port=${wp_db_port:-3306}

        mydumper_dump=$(
            mydumper -v 3 -h "${wp_db_host}" -P "${wp_db_port}" -B "${wp_db_name}" -u "${wp_db_user}" -p "${wp_db_pass}" --lock-all-tables \
            -o  "${db_folder_name}" \
            2>&1
        )
        mydumper_dump_status=$?

        local backup_datetime
        backup_datetime=$(date)
        backup_datetime=$(echo "${backup_datetime}" | tr ' ' '-')

        if [[ ${mydumper_dump_status} != 0 ]]; then
            {
                echo "------------------------------------------------------------------------------"
                echo "${backup_datetime} | $site_to_backup | MyDumper Fail {${mydumper_dump_status}}"
                echo "${mydumper_dump}"
                echo "$backup_type Backup Failed!"
            } | tee -a /opt/gridpane/active.backups.monitoring.log
            return 1
        else
            {
                echo "------------------------------------------------------------------------------"
                echo "${backup_datetime} | $site_to_backup | MyDumper Success {${mydumper_dump_status}}"
                ls -d "${db_folder_name}"
                ls -lh "${db_folder_name}"
            } | tee -a /opt/gridpane/active.backups.monitoring.log

            echo "DB Exported..."

            # Need to get the DB prefix from wp-config...
            tableprefix=$(sed -n -e "/\$table_prefix/p" "/var/www/${site_to_backup}/wp-config.php")
            echo "${tableprefix}" > "/var/www/${site_to_backup}/htdocs/table.prefix"
            chmod 400 "/var/www/${site_to_backup}/htdocs/table.prefix"
            echo "Database Table Prefix Exported..."

            cp "/var/www/${site_to_backup}/wp-config.php" "/var/www/${site_to_backup}/wp-config.last.config"
            chmod 400 "/var/www/${site_to_backup}/wp-config.last.config"

            echo " "
            echo "Backup of ${site_to_backup} Done!"

            return 0
        fi
    fi

}

gpbup::dobackup::post() {

    local site_to_backup="$1"
    
    echo "Cleaning up local directory..."
    rm -r /var/www/${site_to_backup}/htdocs/database-${START_TIME_EPOCH} || true
    rm /var/www/${site_to_backup}/htdocs/table.prefix || true
    rm /var/www/${site_to_backup}/wp-config.last.config || true

    echo " "
    echo "Backup of ${site_to_backup} Done!"

}

gpbup::appcallback() {

    local site_domain="$1"
    local api_route="$2"
    local arg1="$3"
    local arg2="$4"
    local arg3="$5"
    local payload

    case $api_route in
        backups/backup)
            payload="\"site_url\":\"${site_domain}\", \"server_ip\":\"${serverIP}\", \"status\":\"${arg1}\", \"details\":\"${arg2}\", \"percentage\":\"\""
            ;;
        backups/add-remote-storage|backups/delete-remote-storage|backups/restore)
            payload="\"site_url\":\"${site_domain}\", \"server_ip\":\"${serverIP}\", \"status\":\"${arg1}\", \"details\":\"${arg2}\", \"service\":\"${arg3}\""
            ;;
        site/site-update)
            payload="\"site_url\":\"'${site_domain}'\", \"server_ip\":\"'${serverIP}'\", \"${arg1}_bup\":${arg2}\""
            ;;
    esac

    curl -d "{${payload}}" -H "Content-Type: application/json" -X POST https://$GPURL/api/${api_route}?api_token=$gridpanetoken

}

gpbup::checkrepoinit() {

    local site_to_backup="$1"
    export DUPLICACY_PASSWORD="${UUID}"

    echo "Checking if site $site_to_backup is Initialized..."

    if [ ! -f "/var/www/$site_to_backup/.duplicacy/preferences" ]; then
        echo "Initializing $site_to_backup for Duplicacy..."
        cd /var/www/$site_to_backup/
        if [[ "${site_to_backup}" == *"-remote" ]]; then
            /usr/local/bin/duplicacy init -e -repository /var/www/${site_to_backup} gridpane-${UUID}-${site_to_backup} /opt/gridpane/backups/${site_to_backup}-duplications
        else
            /usr/local/bin/duplicacy init -e -repository /var/www/${site_to_backup} gridpane-${UUID}-${site_to_backup} /opt/gridpane/backups/duplications
        fi
        duplicacy set -key password -value "${UUID}"

        # Need to add the filters here...
        sed -i "s/\"filters\": \"\"/\"filters\": \"\/var\/www\/${site_to_backup}\/.duplicacy\/filters\"/g" /var/www/${site_to_backup}/.duplicacy/preferences
        cat > /var/www/${site_to_backup}/.duplicacy/filters <<EOF
-logs/*
wp-config.php
*.wpress
*.tar
*.zip
*.img
*.iso
*.gz
EOF

    fi

    if [[ ! -f "/var/www/${site_to_backup}/logs/backups.env" ]]; then
        echo "Creating backups.env file..."
        touch "/var/www/${site_to_backup}/logs/backups.env"
        echo "Local-Backups:OFF" >> "/var/www/${site_to_backup}/logs/backups.env"
        echo "Remote-Backups:OFF" >> "/var/www/${site_to_backup}/logs/backups.env"
    fi

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#######################################
gpbup::getargsetvar() {

  ((DEBUG)) && gridpane::displayfunc

  if [[ $# -gt 0 ]]; then
    while true; do
      local var="$1"
      case "${var}" in
      -debug) DEBUG=1 ;;
      esac

      shift 1 || true

      if [[ -z "${var}" ]]; then
        break
      fi

    done
  fi

}

