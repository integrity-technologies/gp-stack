#!/bin/bash

#
#
#. gridpane.sh
#. workers.sh

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

#######################################
# Set Health Test Variables
#
#######################################
health_test::set_variables() {
  readonly pgrep='/usr/bin/pgrep'
  readonly fail_msg="is down!"
  readonly success_msg="is running!"
  readonly slack_1=error
  readonly Date=$(date "+%Y-%m-%d %H:%M")

  if [[ "$webserver" = nginx ]]; then
    readonly services_array=("monit" "mysql" "nginx" "php7.1" "php7.2" "php7.3" "php7.4" "redis" "fail2ban")
  else
    readonly services_array=("monit" "mysql" "openlitespeed" "redis" "fail2ban")
  fi

  readonly resources_array=("disk_usage" "ram_usage" "cpu_loadavg" "process_mem")
  readonly monit_url=$(gridpane::conf_read monit-url -q)
  count=100

}

#######################################
# Create the report log for the health
# check and date it
#
# Outputs stdout
# - /opt/gridpane/health_report/health_report_log.txt
#######################################
health_test::init_report_log() {
  mkdir -p /opt/gridpane/health_report
  [[ ! -f /opt/gridpane/health_report/Health.txt ]] && touch /opt/gridpane/health_report/Health.txt
  echo "" >/opt/gridpane/health_report/Health.txt
  echo -e "#####################################################################################"
  echo -e "GridPane ${host} Server Health Test ${Date}"
  echo -e "#####################################################################################"
}

health_test::section_title() {
  echo "************************************************************************************" | tee -a ${2}
  echo "$1" | tee -a /opt/gridpane/health_report/health_report_log.txt
  echo "************************************************************************************" | tee -a ${2}
}

health_test::monit_status() {
  monit status >/tmp/${nonce}-monit.status
  local monit_status=$(cat /tmp/${nonce}-monit.status)
  touch /opt/gridpane/health_report/monit.txt

  if [[ ${monit_status} == *"Resource limit matched"* ]] ||
    [[ ${monit_status} == *"Execution failed"* ]] ||
    [[ ${monit_status} == *"Does not exist"* ]] ||
    [[ ${monit_status} == *"Timeout"* ]] ||
    [[ ${monit_status} == *"Nginx Syntax Test failed"* ]] ||
    [[ ${monit_status} == *"Status failed"* ]]; then
    ((count = count - 10))
    output="Monit Check failed, issues found."
  else
    output="Monit Check passed, no issues found."
  fi

#  echo -e "-------------------------------------------------------------------------------"
#  echo -e "Monit Check"
#  cat /tmp/${nonce}-monit.status
#  rm /tmp/${nonce}-monit.status
#  echo -e "${output}\t\t\t| Current Grade: ${count}%"
  echo ${output}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
}
#######################################
# Set variables for:
# - Service Running Check
# - Test parameters
#
# Outputs
# - Command to run the running verification
#  health::test::verify::service_running \
#    "${service}" \
#    "${verify_test}" \
#    "${check_service_daemon}"
#######################################
health::test::process::service_running() {
  local service="$1"
  local check_service_daemon
  local verify_test

  if [[ "$webserver" = nginx ]]; then
    case ${service} in
    monit)
      check_service_daemon="${pgrep} monit"
      verify_test="\$? -eq 0"
      ;;
    mysql)
      check_service_daemon="${pgrep} mysqld"
      verify_test="\$? -eq 0"
      ;;
    nginx)
      check_service_daemon="${pgrep} nginx"
      verify_test="\$? -eq 0"
      ;;
    php7.1)
      check_service_daemon="${pgrep} php-fpm7.1"
      verify_test="\$? -eq 0"
      ;;
    php7.2)
      check_service_daemon="${pgrep} php-fpm7.2"
      verify_test="\$? -eq 0"
      ;;
    php7.3)
      check_service_daemon="${pgrep} php-fpm7.3"
      verify_test="\$? -eq 0"
      ;;
    php7.4)
      check_service_daemon="${pgrep} php-fpm7.4"
      verify_test="\$? -eq 0"
      ;;
    fail2ban)
      check_service_daemon="${pgrep} fail2ban-server"
      verify_test="\$? -eq 0"
      ;;
    redis)
      check_service_daemon=""
      verify_test="redis-cli ping | grep -q 'PONG'"
      ;;
    esac
  else
    case ${service} in
    monit)
      check_service_daemon="${pgrep} monit"
      verify_test="\$? -eq 0"
      ;;
    mysql)
      check_service_daemon="${pgrep} mysqld"
      verify_test="\$? -eq 0"
      ;;
    openlitespeed)
      check_service_daemon="${pgrep} openlitespeed"
      verify_test="\$? -eq 0"
      ;;
    fail2ban)
      check_service_daemon="${pgrep} fail2ban-server"
      verify_test="\$? -eq 0"
      ;;
    redis)
      check_service_daemon=""
      verify_test="redis-cli ping | grep -q 'PONG'"
      ;;
    esac
  fi

  health::test::verify::service_running \
    "${service}" \
    "${verify_test}" \
    "${check_service_daemon}"
}

#######################################
# Test to see if service is running
#
# Outputs stdout
# - /opt/gridpane/health_report/Health.txt
# - /opt/gridpane/health_report/health_report_log.txt
# - gridpane::notify::app (optional)
# - gridpane::notify::slack (optional)
#######################################
health::test::verify::service_running() {
  local service="$1"
  local verify_test="$2"
  local check_service_daemon="$3"
  local notification_body

  touch /opt/gridpane/health_report/service_running.txt

  check=$(${check_service_daemon})
  if [[ ${verify_test} ]]; then
    ((count = count))
#    echo -e "Stack Service ${service} ${success_msg} | Current Grade: ${count}%"
    echo ${service} ${success_msg}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
    if [[ ${last_arg} == "-notify-user" ]]; then
      notification_body="${service} is running on server hostname: ${host}!"
      gridpane::notify::app \
        "Health Check Notification" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-cogs" \
        "short" \
        "NULL" >/dev/null 2>&1
    fi
  else
    ((count = count - 20))
#    echo -e "Stack Service ${service} ${fail_msg} | Current Grade: ${count}%"
    echo ${service} ${fail_msg}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
    if [[ ${last_arg} == "-notify-user" ]]; then
      notification_body="${service} is down on server hostname: ${host}!"
      gridpane::notify::app \
        "Health Check Warning" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "NULL" >/dev/null 2>&1
      gridpane::notify::slack \
        "${slack_1}" \
        "${service} Service Failure" \
        "${notification_body}" >/dev/null 2>&1
    fi

  fi
}

#######################################
# Loop through services array
# run command to process each array item
#
#######################################
health_test::service_check() {
#  echo -e "-------------------------------------------------------------------------------"
#  echo -e "Services Check"
  for service in ${services_array[@]}; do
    health::test::process::service_running ${service}
  done
}

health::test::pre_process::resource_usage::disk_usage() {
  local mem=$(df -h | awk '$6 == "/" {print $5}')
  local usep=$(echo ${mem} | awk '{ print $1}' | cut -d'%' -f1)
  verify_test="\${usep} -lt 90"
  success_output="Disk is OK. Current Disk Usage: ${usep}%"
  failure_output="Disk is not OK. Current Disk Usage: ${usep}%"
}

health::test::pre_process::resource_usage::ram_usage() {
  local ram=$(free -m | awk 'NR==2{printf "Memory Usage: %s/%sMB (%.2f%%)\n", $3,$2,$3*100/$2 }')
  local ram2=$(free -m | awk 'NR==2{printf "%.2f\n", $3*100/$2 }')
  local ram_high="90"
  verify_test="\$(bc <<<\"\${ram2} <= \${ram_high}\") -eq 1"
  success_output="RAM is OK. Current ${ram}"
  failure_output="RAM is not OK. Current ${ram}"
}

health::test::pre_process::resource_usage::cpu_loadavg() {
  local cpu=$(cat /proc/loadavg | awk '{print $1}')
  local cpu_high="90"
  verify_test="\$(bc <<<\"\${cpu} <= \${cpu_high}\") -ne 1"
  success_output="CPU is OK. Current LoadAvg ${cpu}%"
  failure_output="CPU is not OK. Current LoadAvg ${cpu}%"
}

health::test::pre_process::resource_usage::process_mem() {
  local proc_mem=$(ps -eo %mem,cmd --sort=-%mem | head | awk '$1>2.0')
  local mem_use=$(echo ${proc_mem} | awk '{ print $1}' | cut -d'.' -f1)
  local process=$(echo ${proc_mem} | awk '{$1=""; print $0}')
  verify_test="\${mem_use} -lt 90"
  success_output="No processes are using a lot of resources."
  failure_output="Process: ${process} using ${mem_use}%"
}

health::test::process::resources_usage() {

  health::test::pre_process::resource_usage::$1

  if [[ ${verify_test} ]]; then
    ((count = count))

    echo ${success_output}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
    success_output="${success_output} | Current Grade: ${count}%"
#    echo -e "${success_output}"

    if [[ ${last_arg} == "-notify-user" ]]; then
      gridpane::notify::app \
        "Health Check Notification" \
        "${success_output}" \
        "popup_and_center" \
        "fa-cogs" \
        "short" \
        "NULL" >/dev/null 2>&1
    fi
  else
    ((count = count - 10))

    echo ${failure_output}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
    failure_output="${failure_output}\t| Current Grade: ${count}%"
#    echo -e "${failure_output}"

    if [[ ${last_arg} == "-notify-user" ]]; then
      gridpane::notify::app \
        "Health Check Warning" \
        "${failure_output}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "NULL" >/dev/null 2>&1
      gridpane::notify::slack \
        "${slack_1}" \
        "${1} Resource Usage Warning" \
        "${failure_output}" >/dev/null 2>&1
    fi

  fi

  verify_test=""
  success_output=""
  failure_output=""
}

health_test::resources_check() {
  for resource_use in ${resources_array[@]}; do
    health::test::process::resources_usage ${resource_use}
  done
}

health_test::process::active_sysusers() {
  local memory_or_cpu
  local user_resource
  local user_resource1
  local user_resource2

  if [[ ${1} == "cpu" ]]; then
    memory_or_cpu="CPU"
  else
    memory_or_cpu="Memory"
  fi
  user_resource="user_${1}"
  user_resource1="user_${1}1"
  user_resource2="user_${1}2"

  echo -e "-------------------------------------------------------------------------------"
  echo "${memory_or_cpu} use"
  echo "%     user"
  echo -e " "
  echo -e ${2} | grep -v ^$ | sort -rn | head | column -t | awk '$1>0.00'

  user_resource=$(echo -e ${2} | grep -v ^$ | sort -rn | head | column -t | awk '$1>0.0')
  user_resource1=$(echo ${user_resource} | awk '{ print $1}')
  user_resource2=$(echo ${user_resource} | awk '{ print $2}')
  local user_resource_high="90"

#  echo "-------------------------------------------------------------------------------"

  if [[ $(bc <<<"${user_resource1} <= ${user_resource_high}") -eq 1 ]]; then

    ((count = count))
    local success_output="No user is consuming a concerning amount of ${memory_or_cpu}"
    echo ${success_output}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
#    echo -e "${success_output} | Current Grade: ${count}%"

    [[ ${last_arg} == "-notify-user" ]] &&
      gridpane::notify::app \
        "Health Check Notification" \
        "${success_output}" \
        "popup_and_center" \
        "fa-cogs" \
        "short" \
        "NULL" >/dev/null 2>&1

  else

    ((count = count - 10))
    local failure_output="Greedy User: ${user_resource2} is using (${user_resource1}%) ${memory_or_cpu}"
    echo ${failure_output}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
#    echo -e "${failure_output} | Current Grade: ${count}%"

    if [[ ${last_arg} == "-notify-user" ]]; then
      gridpane::notify::app \
        "Health Check Warning" \
        "${failure_output}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "NULL" >/dev/null 2>&1
      gridpane::notify::slack \
        "${slack_1}" \
        "${1} Resource Usage Warning" \
        "${failure_output}" >/dev/null 2>&1
    fi
  fi

}

health_test::check::active_sysusers() {
  for user in $(ps aux | grep -v COMMAND | awk '{print $1}' | sort -u); do
    mem_stats="${mem_stats}\n$(ps aux | egrep ^${user} | awk 'BEGIN{total=0}; {total += $4};END{print total,$1}')"
    cpu_stats="${cpu_stats}\n$(ps aux | egrep ^${user} | awk 'BEGIN{total=0}; {total += $3};END{print total,$1}')"
  done
  health_test::process::active_sysusers "mem" "${mem_stats}"
  health_test::process::active_sysusers "cpu" "${cpu_stats}"
}

#######################################
# Process site access logs
#######################################
health_test::sites::process::access_logs() {
  local site="$1"
  local notification_body

  if [[ "$webserver" = nginx && -s /var/log/nginx/${site}.access.log ||
        "$webserver" = openlitespeed && -s /var/www/"$site"/logs/"$site".access.log ]]; then

    RAND=$(tr </dev/urandom -dc _A-Z-a-z-0-9 | head -c8)
    curl ${site}?${RAND} --max-time 5 >/tmp/tempcurl &>/dev/null

    if [[ $? == "28" ]]; then

      notification_body="Site timeout - likely DNS issue. Site: ${site} on server hostname: ${host}"
#      echo -e "${notification_body}"
      authepoch="1"

      if [[ ${last_arg} == "-notify-user" ]]; then
        gridpane::notify::app \
          "Health Check Warning" \
          "${notification_body}" \
          "popup_and_center" \
          "fa-exclamation" \
          "short" \
          "NULL" >/dev/null 2>&1
        gridpane::notify::slack \
          "${slack_1}" \
          "Health Check Warning" \
          "${notification_body}" >/dev/null 2>&1
      fi
    else
      if [[ "$webserver" = nginx ]]; then
        local authline=$(tail -1 /var/log/nginx/${site}.access.log) #whatever log file
        local logdate=$(echo ${authline} | sed 's/[][]//g')         #we remove the brackets
        local logdate=$(echo ${logdate} | awk '{print $1;}')        # we grab the first word
        local logdate=$(echo ${logdate} | sed 's/:/ /')             #we drop the first colon
        local logdate=$(echo ${logdate} | sed 's#/#\-#g')           #we replace slashes with dashes
      else
        local authline="$(tail -1 /var/www/"$site"/logs/"$site".access.log)" #whatever log file
        local logdate=$(echo ${authline} | sed 's/[][]//g')         #we remove the brackets
        local logdate=$(echo ${logdate} | awk '{print $5;}')        # we grab the first word
        local logdate=$(echo ${logdate} | sed 's/:/ /')             #we drop the first colon
        local logdate=$(echo ${logdate} | sed 's#/#\-#g')           #we replace slashes with dashes
      fi

      authepoch=$(date --date="${logdate}" +%s)
#      echo -e "The Access Log for ${site} is ${authepoch}"
    fi
  else
#    echo -e "${site} Access log is empty..."
    authepoch="1"
  fi
}

#######################################
# Process site error logs
#######################################
health_test::sites::process::error_logs() {
  local site="$1"
  if [[ "$webserver" = nginx && -s /var/log/nginx/${site}.error.log ]]; then
#    echo Errors in ${site}.error.log
    local year=$(date +%Y)
    local errorline=$(grep ${year} /var/log/nginx/${site}.error.log | tail -1) #whatever log file
    local errorarray=(${errorline})
    local logdate="${errorarray[@]:0:2}"
    errorepoch=$(date --date="${logdate}" +%s)
#    echo -e "The Error Log for ${site} is ${errorepoch}"
  elif [[ "$webserver" = openlitespeed && -s /var/www/"$site"/logs/"$site".error.log ]]; then
#    echo Errors in ${site}.error.log
    local year=$(date +%Y)
    local errorline=$(grep ${year} /var/www/"$site"/logs/"$site".error.log | tail -1) #whatever log file
    local errorarray=(${errorline})
    local logdate="${errorarray[@]:0:2}"
    errorepoch=$(date --date="${logdate}" +%s)
#    echo -e "The Error Log for ${site} is ${errorepoch}"
  else
#    echo -e "${site} Error log is empty..."
    errorepoch="0"
  fi
}

#######################################
# Process WSOD error
#######################################
health_test::sites::process::wsod_errors() {
  local site="$1"
  local slack_type
  local notification_title
  local notification_body

  if [[ "${errorepoch}" -ge "${authepoch}" ]]; then
#    echo -e "${site} may have an error, inspecting..."

    local errorline=$(echo ${errorline} | sed 's/.* in \(.*\) on .*/\1/')
    if [[ ${errorline} == *"wp-config.php"* || ${errorline} == *"Call to undefined function wp()"* ]]; then
      ((count = count - 10))

      echo -e "Error found on ${site}... \n" >>/opt/gridpane/health_report/check_wsods.txt
#      echo -e "It looks like a configuration problem with wp-config.php - recommend backing up and reverting to backup wp-config.php file..."

      slack_type="error"
      notification_title="Health Check Warning"
      notification_body="${site} wp-config.php issue detected"
      echo ${notification_body}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
      notification_body="${notification_body}\t\t| Current Grade: ${count}%"

    elif [[ ${errorline} == *"wp-content/themes"* ]]; then
      ((count = count - 10))
      themepath=$(dirname ${errorline})
      themename=$(basename ${themepath})

#      echo -e "Error found on ${site}..."
#      echo -e "It's a problem with your active theme ${themename}"

      slack_type="error"
      notification_title="Health Check Warning"
      notification_body="${site} theme issue detected"
      echo ${notification_body}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
      notification_body="${notification_body}\t\t| Current Grade: ${count}%"

    elif [[ ${errorline} == *"wp-content/plugins"* ]]; then
      ((count = count - 10))
#      echo -e "Error found on ${site}..."

      local pluginpath=$(dirname ${errorline})
      local quickcheck=$(basename ${pluginpath})
      local pluginname=$(basename ${errorline})

      if [[ "${quickcheck}" == "plugins" ]]; then
        pluginname=${pluginname%.php}
      fi

#      echo -e "It's a problem with the ${pluginname} plugin"

      slack_type="error"
      notification_title="Health Check Warning"
      notification_body="${site} plugin issue detected"
      echo ${notification_body}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
      notification_body="${notification_body}\t\t| Current Grade: ${count}%"

    else
#      echo -e "${site} Error is not in configuration, themes, or plugins, checking Core WP files..."
      cd /var/www/${site}/htdocs

      local checksum=$(wp core verify-checksums --allow-root 2>&1)
      if [[ ${checksum} == *"Error: WordPress install"* ]]; then
        ((count = count - 10))
#        echo -e "${site} Core WordPress files don't match original files..."
#        echo -e "Recommend Backing up entire site and reinstalling core WP from source..."

        slack_type="error"
        notification_title="Health Check Warning"
        notification_body="${site} Core WP files issue detected"
        echo ${notification_body}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
        notification_body="${notification_body}\t\t| Current Grade: ${count}%"
      else
#        echo -e "${site} error doesn't appear to be causing an outage, visually checking..."

        slack_type="error"
        notification_title="Health Check Notification"
        notification_body="${site} Core WP files OK - unrelated to error"
        echo ${notification_body}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
        notification_body="${notification_body}\t\t| Current Grade: ${count}%"
      fi
    fi

  else
#    echo -e "${site} seems to be running without error, double checking..."

    if [[ "$webserver" = nginx ]]; then
      local authline=$(tail -1 /var/log/nginx/${site}.access.log)
    else
      local authline="$(tail -1 /var/www/"$site"/logs/"$site".access.log)"
    fi

    if [[ ${authline} == *" 200 5 "* ]]; then
      ((count = count - 10))
#      echo -e "${site} Pure WSOD error found, no clear cause indicated!!!"
#      echo -e "Recommend Performing full backup of site..."

      slack_type="error"
      notification_title="Health Check Warning"
      notification_body="${site} Pure WSOD issue - unrelated to error"
      echo ${notification_body}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
      notification_body="${notification_body}\t\t| Current Grade: ${count}%"
    else
#      echo -e "${site} appears to be fully functional."

      notification_title="Health Check Notification"
      notification_body="${site} is functional, no errors"
      echo ${notification_body}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
      notification_body="${notification_body}\t\t| Current Grade: ${count}%"
      send_to_slack="no"
    fi
  fi

  if [[ ${last_arg} == "-notify-user" ]]; then
    gridpane::notify::app \
      "${notification_title}" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "short" \
      "NULL" >/dev/null 2>&1
    if [[ -z ${send_to_slack} ]]; then
      gridpane::notify::slack \
        "${slack_type}" \
        "${notification_title}" \
        "${notification_body}" >/dev/null 2>&1
    fi
  fi
}

#######################################
# Check sites for WSOD issues
#######################################
health_check::check_wsods() {
  local site="$1"
#  echo -e "-------------------------------------------------------------------------------"
#  echo "Checking ${site}"
  health_test::sites::process::access_logs "${site}"
  health_test::sites::process::error_logs "${site}"
  health_test::sites::process::wsod_errors "${site}"
}

health_test::oom_errors() {

#  echo -e "-------------------------------------------------------------------------------"
#  echo "Out of Memory Error check"

  KILLED_PROCS=$(grep -i -r --perl-regexp '(kernel: Killed process)|(oom-killer:)' /var/log/)
  if [[ $? -ne 0 ]]; then
    output="Did not find killed procs. No Out of Memory errors"
  else
    ((count = count - 10))
    output="Out of Memory conditions detected!"
  fi

#  echo "-------------------------------------------------------------------------------"
#  echo -e "${output}\t\t\t| Current Grade: ${count}%"
#  echo "-------------------------------------------------------------------------------"
  echo ${output}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt

}

health_test::nginx_syntax() {

  if [[ "$webserver" = nginx ]]; then
    /usr/sbin/nginx -t >/tmp/${nonce}-nginx.status 2>&1
    local nginx_syntax_check=$(cat /tmp/${nonce}-nginx.status)

#    echo -e "######################################"
#    echo "Nginx Syntax Check" >>/opt/gridpane/health_report/nginx_syntax.txt
#    echo -e "######################################"
#    cat /tmp/${nonce}-nginx.status | tee -a /opt/gridpane/health_report/nginx_syntax.txt
    rm /tmp/${nonce}-nginx.status
    if [[ ${nginx_syntax_check} == *"test failed"* ]]; then
      ((count = count - 10))
      output="Nginx Syntax Check failed, issues found."
    else
      output="Nginx Syntax Check passed, no issues found."
    fi

#    echo "-------------------------------------------------------------------------------"
#    echo -e "${output}\t\t\t| Current Grade: ${count}%"
#    echo "-------------------------------------------------------------------------------"
    echo ${output}@Current Grade: ${count}% >>/opt/gridpane/health_report/Health.txt
  fi

}
#######################################
# Tally Health Check Count up
# If grade is > 80% pass if not fail.
# Post the final grade.
# Send notifications to homepage.
#
# Outputs
# Curls
# - App notifications
# - Slack webhook
# Stdout
# - /opt/gridpane/health_report/Health.txt
# - /opt/gridpane/health_report/health_report_log.txt
#######################################
health_test::tally_up() {

  # Verify count is not less than 0.  If so set to 0
  if ((count < 0)); then
    ((count = 0))
    echo ${count}
  fi

  output="Final Grade: ${count}%"

  # If grade is > 80% pass if not fail.  Post the final grade. Send notifications to homepage.
  if ((count <= 80)); then
    output="Failed! ${output}"
    notification_icon="fa-exclamation"
  else
    output="Passed! ${output}"
    notification_icon="fa-thumbs-o-up"
  fi

  echo --------------------------------------------@--------------------------- >>/opt/gridpane/health_report/Health.txt
  echo Summary:@${output} >>/opt/gridpane/health_report/Health.txt
  echo --------------------------------------------@--------------------------- >>/opt/gridpane/health_report/Health.txt

  if [[ ${last_arg} == "-notify-user" ]]; then
    output="${hostname} ${output}"
    gridpane::notify::app \
      "Health Check Report" \
      "${output}" \
      "popup_and_center" \
      "${notification_icon}" \
      "infinity" \
      "NULL" >/dev/null 2>&1
    gridpane::notify::slack \
      "warning" \
      "Health Check Report" \
      "${output}" >/dev/null 2>&1
  fi

}

#######################################
# Format Health Report to a PDF
#
# Outputs
# opt/gridpane/health_report/health_report_$now.pdf
#######################################
health_test::format_report() {
  # Add header, line under header, and parse output to a table on character @ .
  sed -i $'1 i\\\Service@Score' /opt/gridpane/health_report/Health.txt
  sed -i $'2 i\\\--------------------------------------------@---------------------------' /opt/gridpane/health_report/Health.txt
  column -t -s "@" /opt/gridpane/health_report/Health.txt >/opt/gridpane/health_report/Health1.txt
  echo -e "-------------------------------------------------------------------------------"
  cat /opt/gridpane/health_report/Health1.txt

#  cat /opt/gridpane/health_report/Health1.txt | tee -a /opt/gridpane/health_report/health_report_log.txt
#  cat /opt/gridpane/health_report/resource_usage.txt >>/opt/gridpane/health_report/health_report_log.txt
#  [[ "$webserver" = nginx ]] && cat /opt/gridpane/health_report/nginx_syntax.txt >>/opt/gridpane/health_report/health_report_log.txt
#  cat /opt/gridpane/health_report/oom_error.txt >>/opt/gridpane/health_report/health_report_log.txt
#  cat /opt/gridpane/health_report/check_wsods.txt >>/opt/gridpane/health_report/health_report_log.txt
#  cat /opt/gridpane/health_report/monit.txt >>/opt/gridpane/health_report/health_report_log.txt
#  cp /opt/gridpane/health_report/health_report_log.txt /opt/gridpane/health_report/health_report_log_for_html.txt
#  cat /opt/gridpane/health_report/health_report_log.txt
}

#######################################
# Find and Remove all health_check files
#
#######################################
health_test::clean_up() {
  # Remove temp .txt files
  [[ -f /opt/gridpane/health_report/health_report_log_for_html.txt ]] &&
    rm /opt/gridpane/health_report/health_report_log_for_html.txt
  [[ -f /opt/gridpane/health_report/service_running.txt ]] &&
    rm /opt/gridpane/health_report/service_running.txt
  [[ -f /opt/gridpane/health_report/resource_usage.txt ]] &&
    rm /opt/gridpane/health_report/resource_usage.txt
  [[ "$webserver" = nginx ]] &&
    [[ -f /opt/gridpane/health_report/nginx_syntax.txt ]] &&
    rm /opt/gridpane/health_report/nginx_syntax.txt
  [[ -f /opt/gridpane/health_report/oom_error.txt ]] &&
    rm /opt/gridpane/health_report/oom_error.txt
  [[ -f /opt/gridpane/health_report/check_wsods.txt ]] &&
    rm /opt/gridpane/health_report/check_wsods.txt
  [[ -f /opt/gridpane/health_report/monit.txt ]] &&
    rm /opt/gridpane/health_report/monit.txt

  # Remove files after 30 days
  /usr/bin/find /opt/gridpane/health_report -name "health_re*" -type f -mtime +30 -delete
}
