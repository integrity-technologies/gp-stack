#!/bin/bash
#

#######################################
# Clears any remnants of previous gzip fallbacks
#
#######################################
gpupdate::clearold() {
  find /opt/gridpane/ -name 'scripts-version.txt*' -exec rm {} \;
  find /opt/gridpane/ -name 'gridpane.gz*' -exec rm {} \;
  find /opt/gridpane/ -name 'configs-version.txt*' -exec rm {} \;
  find /opt/gridpane/ -name 'nginx-configs.gz*' -exec rm {} \;
}

#######################################
# returns the branch to use to update scripts
#
# Sets Globals:
#  scripts_branch
#  scripts_path_part
#  is_dev_scripts
# Outputs:
#  Writes to STDOUT
#######################################
gpupdate::branch() {
  if [[ -f /root/$1.development.test.branch ]]; then
    echo "$1 development test branch token found:"
    if [[ $1 == "scripts" ]]; then
      scripts_branch="$(cat "/root/$1.development.test.branch" | xargs)"
    else
      configs_branch="$(cat "/root/$1.development.test.branch" | xargs)"
    fi
    return 0
  else
    if [[ $1 == "scripts" ]]; then
      scripts_branch="master"
    else
      configs_branch="master"
    fi
    # shellcheck disable=SC2154
    if [[ $2 == "daily" || $2 == *"force"* ]]; then
      echo "$2 $1 Update..."
      return 0
    else
      if [[ $1 == "scripts" ]]; then
        version_file="scripts-version.txt"
        version_key="scripts-version"
      else
        version_file="configs-version.txt"
        version_key="configs-version"
      fi
      cd /opt/gridpane || exit
      wget --timeout=30 "https://gridpane.org/prometheus/$version_file"
      if [[ ! -f /opt/gridpane/$version_file ]]; then
        wget --timeout=30 "https://gridpane.net/prometheus/$version_file"
      fi
      cd - || exit
      current_version=$(awk '{print $1; exit}' "/opt/gridpane/$version_file")
      [[ -f /opt/gridpane/$version_file ]] &&
        rm /opt/gridpane/$version_file
      local_version=$(grep -w "$version_key:.*" /root/gridenv/promethean.env | cut -f 2 -d ':') || true
      echo "Current production $1: $current_version"
      echo "Current local $1: $local_version"
      if [[ $current_version != "$local_version" ]]; then
        return 0
      else
        return 1
      fi
    fi
  fi
}

#######################################
# Update the GridPane Scripts
#
#######################################
gpupdate::scripts() {

  cd /opt/gridpane || exit

  [[ -f /opt/gridpane/gpupdate-postprocess ]] &&
    rm /opt/gridpane/gpupdate-postprocess
  [[ ! -d /opt/gridpane/repos ]] &&
    mkdir -p /opt/gridpane/repos

  cd /opt/gridpane/repos || exit

  local force_update
  if [[ ! -d /opt/gridpane/repos/server-scripts ]] ||
    [[ $1 == "force" || $1 == *"force"* && $1 == *"scripts"*  ]]; then
    [[ -d /opt/gridpane/repos/server-scripts ]] &&
      rm -rf /opt/gridpane/repos/server-scripts
    git clone --depth 1 --single-branch -b "${scripts_branch}" \
      https://gitlab+deploy-token-16:FgMHPGqfyVDLw_ixsB-Y@gitlab.gridpane.net/gp-public/server-scripts
    force_update="true"
  fi

  local update_scripts_failed
  if [[ -d /opt/gridpane/repos/server-scripts ]]; then

    cd /opt/gridpane/repos/server-scripts || exit

    git checkout "${scripts_branch}"

    if [ $? != 0 ]; then
      cd /opt/gridpane/repos || exit
      rm -rf /opt/gridpane/repos/server-scripts
      git clone --depth 1 --single-branch -b "${scripts_branch}" \
        https://gitlab+deploy-token-16:FgMHPGqfyVDLw_ixsB-Y@gitlab.gridpane.net/gp-public/server-scripts
      cd /opt/gridpane/repos/server-scripts || exit
      force_update="true"
    fi

    local pull_origin
    local pull_origin_response
    pull_origin=$(git pull origin "${scripts_branch}")
    pull_origin_response="$?"
    echo "$pull_origin"
    if [[ $pull_origin_response != 0 ]]; then
      cd /opt/gridpane/repos || exit
      rm -rf /opt/gridpane/repos/server-scripts
      git clone --depth 1 --single-branch -b "${scripts_branch}" \
        https://gitlab+deploy-token-16:FgMHPGqfyVDLw_ixsB-Y@gitlab.gridpane.net/gp-public/server-scripts;
      cd /opt/gridpane/repos/server-scripts || exit
      pull_origin=$(git pull origin "${scripts_branch}")
      pull_origin_response="$?"
      force_update="true"
      echo "$pull_origin"
      [[ "$pull_origin_response" != 0 ]] &&
        update_scripts_failed="true"
    fi

    [[ -f /usr/local/bin/.editorconfig ]] &&
      rm /usr/local/bin/.editorconfig
    [[ -f /usr/local/bin/.gitignore ]] &&
      rm /usr/local/bin/.gitignore
    [[ -f /usr/local/bin/export-gl-issues.pl ]] &&
      rm /usr/local/bin/export-gl-issues.pl
    [[ -f /usr/local/bin/php-timezones.env ]] &&
      rm /usr/local/bin/php-timezones.env
    [[ -f /usr/local/bin/gridpane-startup.service ]] &&
      rm /usr/local/bin/gridpane-startup.service
    [[ -f /usr/local/bin/gridpane-update.service ]] &&
      rm /usr/local/bin/gridpane-update.service
    [[ -f /usr/local/bin/gridpane-update.timer ]] &&
      rm /usr/local/bin/gridpane-update.timer

    if [[ $pull_origin_response == 0 && "$pull_origin" != *"Already up to date."* ]] ||
      [[ $pull_origin_response == 0 && "$pull_origin" == *"Already up to date."* && $force_update == "true" ]]; then

      cp -rf /opt/gridpane/repos/server-scripts /opt/gridpane/server-scripts

      echo "------------------------------------------------------------------"

      [[ -f /opt/gridpane/server-scripts/.editorconfig ]] &&
        rm /opt/gridpane/server-scripts/.editorconfig

      [[ -f /opt/gridpane/server-scripts/.gitignore ]] &&
        rm /opt/gridpane/server-scripts/.gitignore

      [[ -f /opt/gridpane/server-scripts/export-gl-issues.pl ]] &&
        rm /opt/gridpane/server-scripts/export-gl-issues.pl

      [[ ! -d /opt/gridpane/php-timezones ]] &&
        mkdir -p /opt/gridpane/php-timezones
      if ! cmp -s /opt/gridpane/php-timezones/php-timezones.env /opt/gridpane/server-scripts/php-timezones.env; then
        echo "updating /opt/gridpane/php-timezones/php-timezones.env";
        cp /opt/gridpane/server-scripts/php-timezones.env /opt/gridpane/php-timezones/php-timezones.env
      fi
      [[ -f /opt/gridpane/server-scripts/php-timezones.env ]] &&
        rm /opt/gridpane/server-scripts/php-timezones.env

      [[ ! -d /opt/gridpane/phpma ]] &&
        mkdir -p /opt/gridpane/phpma
      if ! cmp -s /opt/gridpane/phpma/phpma.config.inc.php /opt/gridpane/server-scripts/phpma.config.inc.php; then
        echo "updating /opt/gridpane/phpma/phpma.config.inc.php";
        cp /opt/gridpane/server-scripts/phpma.config.inc.php /opt/gridpane/phpma/phpma.config.inc.php
      fi
      [[ -f /opt/gridpane/server-scripts/phpma.config.inc.php ]] &&
        rm /opt/gridpane/server-scripts/phpma.config.inc.php
      if ! cmp -s /opt/gridpane/phpma/phpma.signon.php /opt/gridpane/server-scripts/phpma.signon.php; then
        echo "updating /opt/gridpane/phpma/phpma.signon.php";
        cp /opt/gridpane/server-scripts/phpma.signon.php /opt/gridpane/phpma/phpma.signon.php
      fi
      [[ -f /opt/gridpane/server-scripts/phpma.signon.php ]] &&
        rm /opt/gridpane/server-scripts/phpma.signon.php

      local reload_systemd_daemon
      if [[ -f /opt/gridpane/server-scripts/gridpane-startup.service ]]; then
        if [[ ! -f /etc/systemd/system/gridpane-startup.service ]]; then
          cp /opt/gridpane/server-scripts/gridpane-startup.service /etc/systemd/system/gridpane-startup.service
          echo "updating /etc/systemd/system/gridpane-startup.service";
          reload_systemd_daemon="true"
        fi

        if ! cmp -s /etc/systemd/system/gridpane-startup.service /opt/gridpane/server-scripts/gridpane-startup.service; then
          echo "updating /etc/systemd/system/gridpane-startup.service";
          cp /opt/gridpane/server-scripts/gridpane-startup.service /etc/systemd/system/gridpane-startup.service
          reload_systemd_daemon="true"
        fi

        [[ ! -d /opt/gridpane/systemd ]] &&
          mkdir -p /opt/gridpane/systemd
        cp /opt/gridpane/server-scripts/gridpane-startup.service /opt/gridpane/systemd/gridpane-startup.service
      fi
      [[ -f /opt/gridpane/server-scripts/gridpane-startup.service ]] &&
       rm /opt/gridpane/server-scripts/gridpane-startup.service

      if [[ -f /opt/gridpane/server-scripts/gridpane-update.service ]]; then
        if [[ ! -f /etc/systemd/system/gridpane-update.service ]]; then
          echo "updating /etc/systemd/system/gridpane-update.service";
          cp /opt/gridpane/server-scripts/gridpane-update.service /etc/systemd/system/gridpane-update.service
          reload_systemd_daemon="true"
        fi

        if ! cmp -s /etc/systemd/system/gridpane-update.service /opt/gridpane/server-scripts/gridpane-update.service; then
          echo "updating /etc/systemd/system/gridpane-update.service";
          cp /opt/gridpane/server-scripts/gridpane-update.service /etc/systemd/system/gridpane-update.service
          reload_systemd_daemon="true"
        fi

        [[ ! -d /opt/gridpane/systemd ]] && mkdir -p /opt/gridpane/systemd
        cp /opt/gridpane/server-scripts/gridpane-update.service /opt/gridpane/systemd/gridpane-update.service
      fi
      [[ -f /opt/gridpane/server-scripts/gridpane-update.service ]] &&
       rm /opt/gridpane/server-scripts/gridpane-update.service

      local start_systemd_update_timer
      if [[ -f /opt/gridpane/server-scripts/gridpane-update.timer ]]; then
        if [[ ! -f /etc/systemd/system/gridpane-update.timer ]]; then
          echo "updating /etc/systemd/system/gridpane-update.timer";
          cp /opt/gridpane/server-scripts/gridpane-update.timer /etc/systemd/system/gridpane-update.timer
          reload_systemd_daemon="true"
        fi

        if ! cmp -s /etc/systemd/system/gridpane-update.timer /opt/gridpane/server-scripts/gridpane-update.timer; then
          echo "updating /etc/systemd/system/gridpane-update.timer";
          cp /opt/gridpane/server-scripts/gridpane-update.timer /etc/systemd/system/gridpane-update.timer
          reload_systemd_daemon="true"
        fi

        [[ ! -d /opt/gridpane/systemd ]] && mkdir -p /opt/gridpane/systemd
        cp /opt/gridpane/server-scripts/gridpane-update.timer /opt/gridpane/systemd/gridpane-update.timer
        rm /opt/gridpane/server-scripts/gridpane-update.timer
      fi
      [[ -f /opt/gridpane/server-scripts/gridpane-update.timer ]] &&
       rm /opt/gridpane/server-scripts/gridpane-update.timer

      if [[ -f /opt/gridpane/server-scripts/gridpane-daily-update.service ]]; then
        if [[ ! -f /etc/systemd/system/gridpane-daily-update.service ]]; then
          echo "updating /etc/systemd/system/gridpane-daily-update.service";
          cp /opt/gridpane/server-scripts/gridpane-daily-update.service /etc/systemd/system/gridpane-daily-update.service
          reload_systemd_daemon="true"
        fi

        if ! cmp -s /etc/systemd/system/gridpane-daily-update.service /opt/gridpane/server-scripts/gridpane-daily-update.service; then
          echo "updating /etc/systemd/system/gridpane-daily-update.service";
          cp /opt/gridpane/server-scripts/gridpane-daily-update.service /etc/systemd/system/gridpane-daily-update.service
          reload_systemd_daemon="true"
        fi

        [[ ! -d /opt/gridpane/systemd ]] && mkdir -p /opt/gridpane/systemd
        cp /opt/gridpane/server-scripts/gridpane-daily-update.service /opt/gridpane/systemd/gridpane-daily-update.service
      fi
      [[ -f /opt/gridpane/server-scripts/gridpane-daily-update.service ]] &&
       rm /opt/gridpane/server-scripts/gridpane-daily-update.service

      local start_systemd_daily_update_timer
      if [[ -f /opt/gridpane/server-scripts/gridpane-daily-update.timer ]]; then
        if [[ ! -f /etc/systemd/system/gridpane-daily-update.timer ]]; then
          echo "updating /etc/systemd/system/gridpane-daily-update.timer";
          cp /opt/gridpane/server-scripts/gridpane-daily-update.timer /etc/systemd/system/gridpane-daily-update.timer
          reload_systemd_daemon="true"
        fi

        if ! cmp -s /etc/systemd/system/gridpane-daily-update.timer /opt/gridpane/server-scripts/gridpane-daily-update.timer; then
          echo "updating /etc/systemd/system/gridpane-daily-update.timer";
          cp /opt/gridpane/server-scripts/gridpane-daily-update.timer /etc/systemd/system/gridpane-daily-update.timer
          reload_systemd_daemon="true"
        fi

        [[ ! -d /opt/gridpane/systemd ]] && mkdir -p /opt/gridpane/systemd
        cp /opt/gridpane/server-scripts/gridpane-daily-update.timer /opt/gridpane/systemd/gridpane-daily-update.timer
        rm /opt/gridpane/server-scripts/gridpane-daily-update.timer
      fi
      [[ -f /opt/gridpane/server-scripts/gridpane-daily-update.timer ]] &&
       rm /opt/gridpane/server-scripts/gridpane-daily-update.timer

      find "/opt/gridpane/server-scripts" -name '*.sh' -not -perm 755 -type f -exec chmod 755 {} \;
      find "/opt/gridpane/server-scripts" -mindepth 1 -maxdepth 1 -name '*.sh' -type f -exec sh -c 'i="$1"; mv "$i" "${i%.sh}"' _ {} \;

      # For post processing - so the currently running file is copied after completion of this run
      if ! cmp -s /opt/gridpane/server-scripts/gpupdate /usr/local/bin/gpupdate; then
        echo "updating /usr/local/bin/gpupdate";
        cp /opt/gridpane/server-scripts/gpupdate /opt/gridpane/gpupdate
      fi

      find "/opt/gridpane/server-scripts" -mindepth 1 -maxdepth 1 -not -name "*gpupdate" -type f -exec sh -c \
        'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /usr/local/bin/"$x"; then echo "updating /usr/local/bin/$x"; cp "$i" /usr/local/bin/"$x"; fi' _ {} \;

      [[ ! -d /usr/local/bin/lib ]] &&
        mkdir -p /usr/local/bin/lib
      [[ -d /opt/gridpane/server-scripts/lib ]] &&
        find "/opt/gridpane/server-scripts/lib" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /usr/local/bin/lib/"$x"; then echo "updating /usr/local/bin/lib/$x"; cp "$i" /usr/local/bin/lib/"$x"; fi' _ {} \;

      [[ ! -d /usr/local/bin/monit-scripts ]] &&
        mkdir -p /usr/local/bin/monit-scripts
      if [[ -d /opt/gridpane/server-scripts/monit-scripts ]]; then
        find "/opt/gridpane/server-scripts/monit-scripts" -mindepth 1 -maxdepth 1 -not -perm 755 -type f -exec chmod 755 {} \;
        find "/opt/gridpane/server-scripts/monit-scripts" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /usr/local/bin/monit-scripts/"$x"; then echo "updating /usr/local/bin/monit-scripts/$x"; cp "$i" /usr/local/bin/monit-scripts/"$x"; fi' _ {} \;
        if grep "XXXDISKUSTHREADSXXX" /usr/local/bin/monit-scripts/monit_dir_sizecheck >/dev/null; then
          local diskus_threads
          local procNo
          procNo=$(nproc --all)
          case $procNo in
            1) diskus_threads=2 ;;
            2|3|4) diskus_threads=4 ;;
            *) diskus_threads=6 ;;
          esac
          sed -i "s/XXXDISKUSTHREADSXXX/${diskus_threads}/g" /usr/local/bin/monit-scripts/monit_dir_sizecheck
        fi
      fi
      [[ -d /usr/local/bin/monit-scripts/monit-scripts ]] &&
        rm -rf /usr/local/bin/monit-scripts/monit-scripts

      [[ ! -d /usr/local/bin/lib/ols ]] &&
        mkdir -p /usr/local/bin/lib/ols
      [[ -d /opt/gridpane/server-scripts/lib/ols ]] &&
        find "/opt/gridpane/server-scripts/lib/ols" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /usr/local/bin/lib/ols/"$x"; then echo "updating /usr/local/bin/lib/ols/$x"; cp "$i" /usr/local/bin/lib/ols/"$x"; fi' _ {} \;

      [[ ! -d /usr/local/bin/lib/ols/7g ]] &&
        mkdir -p /usr/local/bin/lib/ols/7g
      [[ -d /opt/gridpane/server-scripts/lib/ols/7g ]] &&
        find "/opt/gridpane/server-scripts/lib/ols/7g" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /usr/local/bin/lib/ols/7g/"$x"; then echo "updating /usr/local/bin/lib/ols/7g/$x"; cp "$i" /usr/local/bin/lib/ols/7g/"$x"; fi' _ {} \;

      [[ ! -d /usr/local/bin/lib/ols/source ]] &&
        mkdir -p /usr/local/bin/lib/ols/source
      [[ -d /opt/gridpane/server-scripts/lib/ols/source ]] &&
        find "/opt/gridpane/server-scripts/lib/ols/source" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /usr/local/bin/lib/ols/source/"$x"; then echo "updating /usr/local/bin/lib/ols/source/$x"; cp "$i" /usr/local/bin/lib/ols/source/"$x"; fi' _ {} \;

      [[ ! -d /opt/gridpane/monit-settings ]] &&
        mkdir -p /opt/gridpane/monit-settings
      [[ -d /opt/gridpane/server-scripts/monit-settings ]] &&
        find "/opt/gridpane/server-scripts/monit-settings" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /opt/gridpane/monit-settings/"$x"; then echo "updating /opt/gridpane/monit-settings/$x"; cp "$i" /opt/gridpane/monit-settings/"$x"; fi' _ {} \;

      [[ ! -d /opt/gridpane/monit-settings/conf-available ]] &&
        mkdir -p /opt/gridpane/monit-settings/conf-available
      [[ -d /opt/gridpane/server-scripts/monit-settings/conf-available ]] &&
        find "/opt/gridpane/server-scripts/monit-settings/conf-available" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /opt/gridpane/monit-settings/conf-available/"$x"; then echo "updating /opt/gridpane/monit-settings/conf-available/$x"; cp "$i" /opt/gridpane/monit-settings/conf-available/"$x"; fi' _ {} \;

      [[ ! -d /opt/gridpane/monit-settings/conf.d ]] &&
        mkdir -p /opt/gridpane/monit-settings/conf.d
      [[ -d /opt/gridpane/server-scripts/monit-settings/conf.d ]] &&
        find "/opt/gridpane/server-scripts/monit-settings/conf.d" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /opt/gridpane/monit-settings/conf.d/"$x"; then echo "updating /opt/gridpane/monit-settings/conf.d/$x"; cp "$i" /opt/gridpane/monit-settings/conf.d/"$x"; fi' _ {} \;

      [[ ! -d /opt/gridpane/monit-settings/monitrc.d ]] &&
        mkdir -p /opt/gridpane/monit-settings/monitrc.d
      [[ -d /opt/gridpane/server-scripts/monit-settings/monitrc.d ]] &&
        find "/opt/gridpane/server-scripts/monit-settings/monitrc.d" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /opt/gridpane/monit-settings/monitrc.d/"$x"; then echo "updating /opt/gridpane/monit-settings/monitrc.d/$x"; cp "$i" /opt/gridpane/monit-settings/monitrc.d/"$x"; fi' _ {} \;

      [[ ! -d /opt/gridpane/monit-settings/templates ]] &&
        mkdir -p /opt/gridpane/monit-settings/templates
      [[ -d /opt/gridpane/server-scripts/monit-settings/templates ]] &&
        find "/opt/gridpane/server-scripts/monit-settings/templates" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /opt/gridpane/monit-settings/templates/"$x"; then echo "updating /opt/gridpane/monit-settings/templates/$x"; cp "$i" /opt/gridpane/monit-settings/templates/"$x"; fi' _ {} \;

      [[ -d /opt/gridpane/server-scripts-last-update ]] &&
        rm -rf /opt/gridpane/server-scripts-last-update
      mv /opt/gridpane/server-scripts /opt/gridpane/server-scripts-last-update

      chmod -R u-x /usr/local/bin/lib/ols/7g
      scripts_updated="true"
    fi

    local current_scripts_version
    case ${scripts_branch} in
      master)
        current_scripts_version=$(grep -w "scripts-version:.*" /opt/gridpane/repos/server-scripts/gridpane.ver | cut -f 2 -d ':') || true
        ;;
      *) current_scripts_version="${scripts_branch}" ;;
    esac
    echo "------------------------------------------------------------------"
    echo "GridPane Scripts - Version: ${current_scripts_version}"
    echo "------------------------------------------------------------------"

    if [[ $scripts_updated == "true" ]] ||
      [[ $current_scripts_version != "$local_version" ]]; then
      gridpane::conf_write scripts-version "${current_scripts_version}"
      gridpane::conf_write script-update-processing complete
    fi

    if ! grep "gridpane-update.service" <<<"$(systemctl list-timers --all)" >/dev/null; then
      /bin/systemctl start gridpane-update.timer
      reload_systemd_daemon="true"
    fi

    if ! grep "gridpane-daily-update.service" <<<"$(systemctl list-timers --all)" >/dev/null; then
      /bin/systemctl start gridpane-daily-update.timer
      reload_systemd_daemon="true"
    fi

    if [[ $reload_systemd_daemon == "true" ]]; then
      /bin/systemctl daemon-reload
    fi
  else
    update_scripts_failed="true"
  fi

  if [[ -n $update_scripts_failed ]]; then
    echo "------------------------------------------------------------------"
    echo "GridPane Scripts Update failed"
    echo "------------------------------------------------------------------"
  fi
  cd ~ || exit
}

#######################################
# Update the Nginx Configs
#
#######################################
gpupdate::configs() {
  # shellcheck disable=SC2154,SC2181
  if [[ "$webserver" = nginx ]]; then

    if grep "#load_module /etc/nginx/modules/ngx_http_geoip2_module.so" /etc/nginx/modules-enabled/ngx_http_geoip2_module.so.conf >/dev/null; then
      sed -i "s/#load_module \/etc\/nginx\/modules\/ngx_http_geoip2_module.so;/load_module \/etc\/nginx\/modules\/ngx_http_geoip2_module.so;/g" \
        /etc/nginx/modules-enabled/ngx_http_geoip2_module.so.conf
      gridpane::conf_write geoip-module disabled -nginx.env
    fi

    if grep "#load_module /etc/nginx/modules/ngx_http_modsecurity_module.so" /etc/nginx/modules-enabled/ngx_http_modsecurity_module.so.conf >/dev/null; then
      sed -i "s/#load_module \/etc\/nginx\/modules\/ngx_http_modsecurity_module.so;/load_module \/etc\/nginx\/modules\/ngx_http_modsecurity_module.so;/g" \
        /etc/nginx/modules-enabled/ngx_http_modsecurity_module.so.conf
      gridpane::conf_write modsec-module disabled -nginx.env
    fi

    if grep "#load_module /etc/nginx/modules/ngx_http_image_filter_module.so" /etc/nginx/modules-enabled/ngx_http_image_filter_module.so.conf >/dev/null; then
      sed -i "s/#load_module \/etc\/nginx\/modules\/ngx_http_image_filter_module.so;/load_module \/etc\/nginx\/modules\/ngx_http_image_filter_module.so;/g" \
        /etc/nginx/modules-enabled/ngx_http_modsecurity_module.so.conf
      gridpane::conf_write img-filter-module disabled -nginx.env
    fi

    if grep "#load_module /etc/nginx/modules/ngx_http_xslt_filter_module.so" /etc/nginx/modules-enabled/ngx_http_xslt_filter_module.so.conf >/dev/null; then
      sed -i "s/#load_module \/etc\/nginx\/modules\/ngx_http_xslt_filter_module.so;/load_module \/etc\/nginx\/modules\/ngx_http_xslt_filter_module.so;/g" \
        /etc/nginx/modules-enabled/ngx_http_modsecurity_module.so.conf
      gridpane::conf_write xslt-filter-module disabled -nginx.env
    fi

    cd /opt/gridpane || true

    [[ -d /opt/gridpane/nginx-configs ]] &&
      mv /opt/gridpane/nginx-configs /opt/gridpane/nginx-configs-last-update
    [[ -d /opt/gridpane/nginx-configs ]] &&
      rm -rf /opt/gridpane/nginx-configs

    [[ ! -d /opt/gridpane/repos ]] &&
      mkdir -p /opt/gridpane/repos

    cd /opt/gridpane/repos || exit

    local force_update
    if [[ ! -d /opt/gridpane/repos/nginx-configs ]] ||
      [[ $1 == "force" || $1 == *"force"* && $1 == *"configs"* ]]; then
      [[ -d /opt/gridpane/repos/nginx-configs ]] &&
        rm -rf /opt/gridpane/repos/nginx-configs
      git clone --depth 1 --single-branch -b "${configs_branch}" \
        https://gitlab+deploy-token-14:C4n3u13NWyyNKzTbtHXt@gitlab.gridpane.net/gp-public/nginx-configs
      force_update="true"
    fi

    local update_configs_failed
    if [[ -d /opt/gridpane/repos/nginx-configs ]]; then

      cd /opt/gridpane/repos/nginx-configs || exit

      git checkout "${configs_branch}"

      if [ $? != 0 ]; then
        cd /opt/gridpane/repos || exit
        rm -rf /opt/gridpane/repos/nginx-configs
        git clone --depth 1 --single-branch -b "${configs_branch}" \
          https://gitlab+deploy-token-16:FgMHPGqfyVDLw_ixsB-Y@gitlab.gridpane.net/gp-public/nginx-configs
        cd /opt/gridpane/repos/nginx-configs || exit
        force_update="true"
      fi

      local pull_origin
      local pull_origin_response
      pull_origin=$(git pull origin "${configs_branch}")
      pull_origin_response="$?"
      echo "$pull_origin"
      if [[ $pull_origin_response != 0 ]]; then
        cd /opt/gridpane/repos || exit
        rm -rf /opt/gridpane/repos/nginx-configs
        git clone --depth 1 --single-branch -b "${configs_branch}" \
          https://gitlab+deploy-token-16:FgMHPGqfyVDLw_ixsB-Y@gitlab.gridpane.net/gp-public/nginx-configs;
        cd /opt/gridpane/repos/nginx-configs || exit
        pull_origin=$(git pull origin "${configs_branch}")
        pull_origin_response="$?"
        force_update="true"
        echo "$pull_origin"
        [[ "$pull_origin_response" != 0 ]] &&
          update_configs_failed="true"
      fi

      if [[ $pull_origin_response == 0 && "$pull_origin" != *"Already up to date."* ]] ||
        [[ $pull_origin_response == 0 && "$pull_origin" == *"Already up to date."* && $force_update == "true" ]]; then

        [[ -d /opt/gridpane/tmp/nginx-configs ]] &&
          rm -rf /opt/gridpane/tmp/nginx-configs
        mkdir -p /opt/gridpane/tmp/nginx-configs

        find "/opt/gridpane/repos/nginx-configs" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /opt/gridpane/nginx-configs-last-update/"$x"; then cp "$i" /opt/gridpane/tmp/nginx-configs/"$x"; fi' _ {} \;

        [[ ! -d /opt/gridpane/tmp/nginx-configs/extra.d ]] &&
          mkdir -p /opt/gridpane/tmp/nginx-configs/extra.d
        find "/opt/gridpane/repos/nginx-configs/extra.d" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /opt/gridpane/nginx-configs-last-update/extra.d/"$x"; then cp "$i" /opt/gridpane/tmp/nginx-configs/extra.d/"$x"; fi' _ {} \;

        [[ ! -d /opt/gridpane/tmp/nginx-configs/modules-enabled ]] &&
          mkdir -p /opt/gridpane/tmp/nginx-configs/modules-enabled
        find "/opt/gridpane/repos/nginx-configs/modules-enabled" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /opt/gridpane/nginx-configs-last-update/modules-enabled/"$x"; then cp "$i" /opt/gridpane/tmp/nginx-configs/modules-enabled/"$x"; fi' _ {} \;

        [[ ! -d /opt/gridpane/tmp/nginx-configs/modsec ]] &&
          mkdir -p /opt/gridpane/tmp/nginx-configs/modsec
        find "/opt/gridpane/repos/nginx-configs/modsec" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /opt/gridpane/nginx-configs-last-update/modsec/"$x"; then cp "$i" /opt/gridpane/tmp/nginx-configs/modsec/"$x"; fi' _ {} \;

        [[ ! -d /opt/gridpane/tmp/nginx-configs/conf.d ]] &&
          mkdir -p /opt/gridpane/tmp/nginx-configs/conf.d
        find "/opt/gridpane/repos/nginx-configs/conf.d" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /opt/gridpane/nginx-configs-last-update/conf.d/"$x"; then cp "$i" /opt/gridpane/tmp/nginx-configs/conf.d/"$x"; fi' _ {} \;

        [[ ! -d /opt/gridpane/tmp/nginx-configs/common ]] &&
          mkdir -p /opt/gridpane/tmp/nginx-configs/common
        find "/opt/gridpane/repos/nginx-configs/common" -mindepth 1 -maxdepth 1 -type f -exec sh -c \
          'i="$1"; x="$(basename "$i")"; if ! cmp -s "$i" /opt/gridpane/nginx-configs-last-update/common/"$x"; then cp "$i" /opt/gridpane/tmp/nginx-configs/common/"$x"; fi' _ {} \;

        [[ -d /opt/gridpane/nginx-configs-last-update ]] &&
          rm -rf /opt/gridpane/nginx-configs-last-update
        mkdir /opt/gridpane/nginx-configs-last-update
        cp -rf /opt/gridpane/repos/nginx-configs/* /opt/gridpane/nginx-configs-last-update/

        echo "------------------------------------------------------------------"
        find "/opt/gridpane/tmp/nginx-configs" -type f -exec sh -c 'i="$1";  x="$(basename "$i")"; echo "Updating $x";' _ {} \;

        [[ ! -d /opt/gridpane/tmp/nginx-configs/modsec/owasp ]] &&
          mkdir /opt/gridpane/tmp/nginx-configs/modsec/owasp
        [[ -f /opt/gridpane/tmp/nginx-configs/modsec/crs-setup.conf ]] &&
          cp /opt/gridpane/tmp/nginx-configs/modsec/crs-setup.conf \
            /opt/gridpane/tmp/nginx-configs/modsec/owasp/crs-setup.conf
        [[ -f /opt/gridpane/tmp/nginx-configs/modsec/crs-paranoia-level.conf ]] &&
          cp /opt/gridpane/tmp/nginx-configs/modsec/crs-paranoia-level.conf \
            /opt/gridpane/tmp/nginx-configs/modsec/owasp/crs-paranoia-level.conf
        [[ -f /opt/gridpane/tmp/nginx-configs/modsec/crs-anomaly-blocking-threshold.conf ]] &&
          cp /opt/gridpane/tmp/nginx-configs/modsec/crs-anomaly-blocking-threshold.conf \
            /opt/gridpane/tmp/nginx-configs/modsec/owasp/crs-anomaly-blocking-threshold.conf
        [[ -f /opt/gridpane/tmp/nginx-configs/modsec/crs-geoip-block.conf ]] &&
          cp /opt/gridpane/tmp/nginx-configs/modsec/crs-geoip-block.conf \
            /opt/gridpane/tmp/nginx-configs/modsec/owasp/crs-geoip-block.conf
        [[ -f /opt/gridpane/tmp/nginx-configs/modsec/crs-honeypot.conf ]] &&
          cp /opt/gridpane/tmp/nginx-configs/modsec/crs-honeypot.conf \
            /opt/gridpane/tmp/nginx-configs/modsec/owasp/crs-honeypot.conf
        [[ -f /opt/gridpane/tmp/nginx-configs/modsec/crs-ip-reputation-block.conf ]] &&
          cp /opt/gridpane/tmp/nginx-configs/modsec/crs-ip-reputation-block.conf \
            /opt/gridpane/tmp/nginx-configs/modsec/owasp/crs-ip-reputation-block.conf
        [[ -f /opt/gridpane/tmp/nginx-configs/modsec/crs-aa-dos-protection.conf ]] &&
          cp /opt/gridpane/tmp/nginx-configs/modsec/crs-aa-dos-protection.conf \
            /opt/gridpane/tmp/nginx-configs/modsec/owasp/crs-aa-dos-protection.conf

        [[ ! -d /etc/nginx/extra.d/_custom ]] &&
          mkdir -p /etc/nginx/extra.d/_custom
        [[ ! -d /etc/nginx/extra.d/_default ]] &&
          mkdir -p /etc/nginx/extra.d/_default
        for extrad_config_file_in_update in $(dir /opt/gridpane/tmp/nginx-configs/extra.d); do
          local extrad_config_filename_in_update
          extrad_config_filename_in_update=$(basename "${extrad_config_file_in_update}")
          if [[ -f /etc/nginx/extra.d/_custom/"${extrad_config_filename_in_update}" ]]; then
            if [[ $1 == "force" || $1 == *"force"* && $1 == *"configs"*  ]]; then
              echo "force updating extra.d config file... /etc/nginx/extra.d/${extrad_config_filename_in_update}"
              rm /etc/nginx/extra.d/_custom/"${extrad_config_filename_in_update}"
              [[ -f /etc/nginx/extra.d/_default/"${extrad_config_filename_in_update}" ]] &&
                rm /etc/nginx/extra.d/_default/"${extrad_config_filename_in_update}"
              mv /opt/gridpane/tmp/nginx-configs/extra.d/"${extrad_config_filename_in_update}" \
                /etc/nginx/extra.d/"${extrad_config_filename_in_update}"
            else
              echo "Maintaining use of custom extra.d ${extrad_config_filename_in_update}"
              [[ -f /etc/nginx/extra.d/_default/"${extrad_config_filename_in_update}" ]] &&
                rm /etc/nginx/extra.d/_default/"${extrad_config_filename_in_update}"
              mv /opt/gridpane/tmp/nginx-configs/extra.d/"${extrad_config_filename_in_update}" \
                /etc/nginx/extra.d/_default/"${extrad_config_filename_in_update}"
            fi
          fi
        done

        [[ ! -d /etc/nginx/common/_custom ]] &&
          mkdir -p /etc/nginx/common/_custom
        [[ ! -d /etc/nginx/common/_default ]] &&
          mkdir -p /etc/nginx/common/_default
        for common_config_file_in_update in $(dir /opt/gridpane/tmp/nginx-configs/common); do
          local common_config_filename_in_update
          common_config_filename_in_update=$(basename "${common_config_file_in_update}")
          if [[ -f /etc/nginx/common/_custom/"${common_config_filename_in_update}" ]]; then
            if [[ $1 == "force" || $1 == *"force"* && $1 == *"configs"*  ]]; then
              echo "force updating common config file... /etc/nginx/common/${common_config_filename_in_update}"
              rm /etc/nginx/common/_custom/"${common_config_filename_in_update}"
              [[ -f /etc/nginx/common/_default/"${common_config_filename_in_update}" ]] &&
                rm /etc/nginx/common/_default/"${common_config_filename_in_update}"
              mv /opt/gridpane/tmp/nginx-configs/common/"${common_config_filename_in_update}" \
                /etc/nginx/common/"${common_config_filename_in_update}"
            else
              echo "Maintaining use of custom ${common_config_filename_in_update}"
              [[ -f /etc/nginx/common/_default/"${common_config_filename_in_update}" ]] &&
                rm /etc/nginx/common/_default/"${common_config_filename_in_update}"
              mv /opt/gridpane/tmp/nginx-configs/common/"${common_config_filename_in_update}" \
                /etc/nginx/common/_default/"${common_config_filename_in_update}"
            fi
          fi
        done

        echo "------------------------------------------------------------------"

        local update_nginx_conf
        [[ -f /opt/gridpane/tmp/nginx-configs/nginx.conf ]] && update_nginx_conf="true"
        local update_fastcgi_params
        [[ -f /opt/gridpane/tmp/nginx-configs/fastcgi_params ]] && update_fastcgi_params="true"
        local update_basics
        [[ -f /opt/gridpane/tmp/nginx-configs/common/basics.conf ]] && update_basics="true"
        local update_limits
        [[ -f /opt/gridpane/tmp/nginx-configs/common/limits.conf ]] && update_limits="true"
        local update_fastcgi
        [[ -f /opt/gridpane/tmp/nginx-configs/conf.d/fastcgi.conf ]] && update_fastcgi="true"
        local update_proxy
        [[ -f /opt/gridpane/tmp/nginx-configs/conf.d/proxy.conf ]] && update_proxy="true"

        cp -rf /opt/gridpane/tmp/nginx-configs/* /etc/nginx/
        rm -rf /opt/gridpane/tmp/nginx-configs/

        ### update nginx.conf commands
        if [[ $update_nginx_conf == "true" ]]; then
          local worker_rlimit_nofile_state
          worker_rlimit_nofile_state=$(grep -w "worker_rlimit_nofile:.*" /root/gridenv/nginx.env | cut -f 2 -d ':') || true
          if [[ -n ${worker_rlimit_nofile_state} ]]; then
            local current_worker_rlimit_nofile_state
            current_worker_rlimit_nofile_state=$(grep "worker_rlimit_nofile" /etc/nginx/nginx.conf | tr ';' ' ' | xargs)
            current_worker_rlimit_nofile_state=${current_worker_rlimit_nofile_state#worker_rlimit_nofile }
            [[ "${worker_rlimit_nofile_state}" != "$current_worker_rlimit_nofile_state" ]] &&
              /usr/local/bin/gp stack nginx worker -rlimit-nofile "${worker_rlimit_nofile_state}"
          fi

          local worker_connections_state
          worker_connections_state=$(grep -w "worker_connections:.*" /root/gridenv/nginx.env | cut -f 2 -d ':') || true
          if [[ -n ${worker_connections_state} ]]; then
            local current_worker_connections_state
            current_worker_connections_state=$(grep "worker_connections" /etc/nginx/nginx.conf | tr ';' ' ' | xargs)
            current_worker_connections_state=${current_worker_connections_state#worker_connections }
            [[ "${worker_connections_state}" != "$current_worker_connections_state" ]] &&
              /usr/local/bin/gp stack nginx worker -connections "${worker_connections_state}"
          fi
        fi

        ### update fastcgi_params commands
        if [[ $update_nginx_conf == "true" ]]; then
          [[ "$(gridpane::conf_read geoip-module -nginx.env)" != "disabled" ]] &&
            /usr/local/bin/gp stack nginx -geoip on
        fi

        ### update basics commands
        if [[ $update_basics == "true" ]]; then
          [[ "$(grep -w "server_name_in_redirect:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')" != "off" ]] &&
            /usr/local/bin/gp stack nginx server-name -in-redirect on

          local server_names_hash_bucket_size_state
          server_names_hash_bucket_size_state="$(grep -w "server_names_hash_bucket_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')"
          [[ ${server_names_hash_bucket_size_state} != "256" ]] &&
            /usr/local/bin/gp stack nginx server-name -hash-bucket-size "${server_names_hash_bucket_size_state}"
        fi

        ### update limits commands
        if [[ $update_limits == "true" ]]; then
          if [[ $(grep -w "open_file_cache:.*" /root/gridenv/nginx.env | cut -f 2 -d ':') == "on" ]]; then
            local open_file_cache_max_elements_state
            open_file_cache_max_elements_state=$(grep -w "open_file_cache_max_elements:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
            if [[ -z ${open_file_cache_max_elements_state} ]]; then
              open_file_cache_max_elements_state="10000"
              gridpane::conf_write open_file_cache_max_elements "${open_file_cache_max_elements_state}" -nginx.env
            fi

            local open_file_cache_inactive_timeout_state
            open_file_cache_inactive_timeout_state=$(grep -w "open_file_cache_inactive_timeout:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
            if [[ -z ${open_file_cache_inactive_timeout_state} ]]; then
              open_file_cache_inactive_timeout_state="300"
              gridpane::conf_write open_file_cache_inactive_timeout "${open_file_cache_inactive_timeout_state}" -nginx.env
            fi

            /usr/local/bin/gp stack nginx limits \
              -open-file-cache "on" \
              --max "${open_file_cache_max_elements_state}" \
              --inactive "${open_file_cache_inactive_timeout_state}"
          fi

          local open_file_cache_valid_state
          open_file_cache_valid_state=$(grep -w "open_file_cache_valid:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ -n ${open_file_cache_valid_state} ]] &&
            /usr/local/bin/gp stack nginx limits -open-file-cache --valid "${open_file_cache_valid_state}"

          local open_file_cache_min_uses_state
          open_file_cache_min_uses_state=$(grep -w "open_file_cache_min_uses:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ -n ${open_file_cache_min_uses_state} ]] &&
            /usr/local/bin/gp stack nginx limits -open-file-cache --min-uses "${open_file_cache_min_uses_state}"

          local open_file_cache_errors_state
          open_file_cache_errors_state=$(grep -w "open_file_cache_errors:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ -n ${open_file_cache_errors_state} ]] &&
            /usr/local/bin/gp stack nginx limits -open-file-cache --errors "on"

          local reset_timedout_connection_state
          reset_timedout_connection_state=$(grep -w "reset_timedout_connection:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ "$reset_timedout_connection_state" != "on" ]] &&
            /usr/local/bin/gp stack nginx limits -reset-timedout-conn "off"

          local send_timeout_state
          send_timeout_state=$(grep -w "send_timeout:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${send_timeout_state} != "30" ]] &&
            /usr/local/bin/gp stack nginx limits -send-timeout "${send_timeout_state}"

          local keepalive_requests_state
          keepalive_requests_state=$(grep -w "keepalive_requests:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${keepalive_requests_state} != "5000" && -n ${keepalive_requests_state} ]] &&
            /usr/local/bin/gp stack nginx limits -keepalive-requests "${keepalive_requests_state}"
          if [[ -z ${keepalive_requests_state} ]]; then
            keep_alive_requests_state=$(grep "keepalive_requests" /etc/nginx/common/limits.conf | xargs)
            if [[ ${keep_alive_requests_state} == *"5000;" ]]; then
              gridpane::conf_write keepalive_requests "5000" -nginx.env
            else
              keep_alive_requests_state=${keep_alive_requests_state%\;}
              keep_alive_requests_state=${keep_alive_requests_state#keepalive_requests }
              /usr/local/bin/gp stack nginx limits -keepalive-requests "${keepalive_requests_state}"
            fi
          fi

          local keepalive_timeout_state
          keepalive_timeout_state=$(grep -w "keepalive_timeout:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${keepalive_timeout_state} != "30" ]] &&
            /usr/local/bin/gp stack nginx limits -keepalive-timeout "${keepalive_timeout_state}"

          local client_body_timeout_state
          client_body_timeout_state=$(grep -w "client_body_timeout:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${client_body_timeout_state} != "30" ]] &&
            /usr/local/bin/gp stack nginx limits -client-body-timeout "${client_body_timeout_state}"

          local client_header_timeout_state
          client_header_timeout_state=$(grep -w "client_header_timeout:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${client_header_timeout_state} != "30" ]] &&
            /usr/local/bin/gp stack nginx limits -client-header-timeout "${client_header_timeout_state}"

          local types_hash_max_size_state
          types_hash_max_size_state=$(grep -w "types_hash_max_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${types_hash_max_size_state} != "2048" ]] &&
            /usr/local/bin/gp stack nginx limits -types-hash-max-size "${types_hash_max_size_state}"

          local client_body_buffer_size_state
          client_body_buffer_size_state=$(grep -w "client_body_buffer_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${client_body_buffer_size_state} != "128" ]] &&
            /usr/local/bin/gp stack nginx limits -client-body-buffer-size "${client_body_buffer_size_state}"

          local client_header_buffer_size_state
          client_header_buffer_size_state=$(grep -w "client_header_buffer_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${client_header_buffer_size_state} != "13" ]] &&
            /usr/local/bin/gp stack nginx limits -client-header-buffer-size "${client_header_buffer_size_state}"

          local large_client_header_buffers_state
          large_client_header_buffers_state=$(grep -w "large_client_header_buffers:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          local large_client_header_buffers_size_state
          large_client_header_buffers_size_state=$(grep -w "large_client_header_buffers_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${large_client_header_buffers_state} != "4" || ${large_client_header_buffers_size_state} != "52" ]] &&
            /usr/local/bin/gp stack nginx limits -large-client-header-buffers "${large_client_header_buffers_state}" "${large_client_header_buffers_size_state}"

          local limit_req_zone_one_size_state
          limit_req_zone_one_size_state=$(grep -w "limit_req_zone_one_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          local limit_req_zone_one_rate_state
          limit_req_zone_one_rate_state=$(grep -w "limit_req_zone_one_rate:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${limit_req_zone_one_size_state} != "10" || ${limit_req_zone_one_rate_state} != "1" ]] &&
            /usr/local/bin/gp stack nginx -limits -req-zone-one "${limit_req_zone_one_size_state}" "${limit_req_zone_one_rate_state}"

          local limit_req_zone_wp_size_state
          limit_req_zone_wp_size_state=$(grep -w "limit_req_zone_wp_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          local limit_req_zone_wp_rate_state
          limit_req_zone_wp_rate_state=$(grep -w "limit_req_zone_wp_rate:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${limit_req_zone_wp_size_state} != "10" || ${limit_req_zone_wp_rate_state} != "3" ]] &&
            /usr/local/bin/gp stack nginx -limits -req-zone-wp "${limit_req_zone_wp_size_state}" "${limit_req_zone_wp_rate_state}"
        fi

        ### update fastcgi Commands
        if [[ $update_fastcgi == "true" ]]; then
          [[ "$(grep -w "fastcgi_cache_background_update:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')" != "on" ]] &&
            /usr/local/bin/gp stack nginx fastcgi -cache-background-update off

          [[ "$(grep -w "fastcgi_cache_revalidate:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')" != "on" ]] &&
            /usr/local/bin/gp stack nginx fastcgi -cache-revalidate off

          [[ "$(grep -w "fastcgi_buffering:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')" != "on" ]] &&
            /usr/local/bin/gp stack nginx fastcgi -buffering off

          [[ "$(grep -w "fastcgi_keep_conn:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')" != "on" ]] &&
            /usr/local/bin/gp stack nginx fastcgi -keep-connection off

          local fastcgi_buffer_size_state
          fastcgi_buffer_size_state=$(grep -w "fastcgi_buffer_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${fastcgi_buffer_size_state} != "128" ]] &&
            /usr/local/bin/gp stack nginx fastcgi -buffer-size "${fastcgi_buffer_size_state}"

          local fastcgi_busy_buffers_size_state
          fastcgi_busy_buffers_size_state=$(grep -w "fastcgi_busy_buffers_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${fastcgi_busy_buffers_size_state} != "256" ]] &&
            /usr/local/bin/gp stack nginx fastcgi -busy-buffer-size "${fastcgi_busy_buffers_size_state}"

          local fastcgi_buffers_state
          fastcgi_buffers_state=$(grep -w "fastcgi_buffers:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          local fastcgi_buffers_size_state
          fastcgi_buffers_size_state=$(grep -w "fastcgi_buffers_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${fastcgi_buffers_state} != "8" || ${fastcgi_buffers_size_state} != "64" ]] &&
            /usr/local/bin/gp stack nginx fastcgi -buffers "${fastcgi_buffers_state}" "${fastcgi_buffers_size_state}"

          local fastcgi_cache_size_state
          fastcgi_cache_size_state=$(grep -w "fastcgi_cache_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          if [[ -n ${fastcgi_cache_size_state} ]]; then
            /usr/local/bin/gp stack nginx fastcgi -max-cache-size "${fastcgi_cache_size_state}"
          else
            cache_ram=$(cat /opt/gridpane/cache.ram)
            /usr/local/bin/gp stack nginx fastcgi -max-cache-size "${cache_ram}"
          fi

          local fastcgi_connect_timeout_state
          fastcgi_connect_timeout_state=$(grep -w "fastcgi_connect_timeout:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${fastcgi_connect_timeout_state} != "60" ]] &&
            /usr/local/bin/gp stack nginx fastcgi -connect-timeout "${fastcgi_connect_timeout_state}"

          local fastcgi_send_timeout_state
          fastcgi_send_timeout_state=$(grep -w "fastcgi_send_timeout:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${fastcgi_send_timeout_state} != "60" ]] &&
            /usr/local/bin/gp stack nginx fastcgi -send-timeout "${fastcgi_send_timeout_state}"

          local fastcgi_read_timeout_state
          fastcgi_read_timeout_state=$(grep -w "fastcgi_read_timeout:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${fastcgi_read_timeout_state} != "60" ]] &&
            /usr/local/bin/gp stack nginx fastcgi -read-timeout "${fastcgi_read_timeout_state}"
        fi

        ### update proxy Commands
        if [[ $update_proxy == "true" ]]; then
          [[ "$(grep -w "proxy_cache_background_update:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')" != "on" ]] &&
            /usr/local/bin/gp stack nginx proxy -cache-background-update off

          [[ "$(grep -w "proxy_cache_revalidate:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')" != "on" ]] &&
            usr/local/bin/gp stack nginx -proxy -cache-revalidate off

          [[ "$(grep -w "proxy_buffering:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')" != "on" ]] &&
            /usr/local/bin/gp stack nginx proxy -buffering off

          [[ "$(grep -w "proxy_keep_conn:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')" != "on" ]] &&
            /usr/local/bin/gp stack nginx proxy -keep-connection off

          local proxy_buffer_size_state
          proxy_buffer_size_state=$(grep -w "proxy_buffer_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${proxy_buffer_size_state} != "128" ]] &&
            /usr/local/bin/gp stack nginx proxy -buffer-size "${proxy_buffer_size_state}"

          local proxy_busy_buffers_size_state
          proxy_busy_buffers_size_state=$(grep -w "proxy_busy_buffers_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${proxy_busy_buffers_size_state} != "256" ]] &&
            /usr/local/bin/gp stack nginx proxy -busy-buffer-size "${proxy_busy_buffers_size_state}"

          local proxy_buffers_state
          proxy_buffers_state=$(grep -w "proxy_buffers:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          local proxy_buffers_size_state
          proxy_buffers_size_state=$(grep -w "proxy_buffers_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${proxy_buffers_state} != "8" || ${proxy_buffers_size_state} != "64" ]] &&
            /usr/local/bin/gp stack nginx proxy -buffers "${proxy_buffers_state}" "${proxy_buffers_size_state}"

          local proxy_cache_size_state
          proxy_cache_size_state=$(grep -w "proxy_cache_size:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          if [[ -n ${proxy_cache_size_state} ]]; then
            /usr/local/bin/gp stack nginx proxy -max-cache-size "${proxy_cache_size_state}"
          else
            cache_ram=$(cat /opt/gridpane/cache.ram)
            /usr/local/bin/gp stack nginx proxy -max-cache-size "${cache_ram}"
          fi

          local proxy_connect_timeout_state
          proxy_connect_timeout_state=$(grep -w "proxy_connect_timeout:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${proxy_connect_timeout_state} != "60" ]] &&
            /usr/local/bin/gp stack nginx proxy -connect-timeout "${proxy_connect_timeout_state}"

          local proxy_send_timeout_state
          proxy_send_timeout_state=$(grep -w "proxy_send_timeout:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${proxy_send_timeout_state} != "60" ]] &&
            /usr/local/bin/gp stack nginx proxy -send-timeout "${proxy_send_timeout_state}"

          local proxy_read_timeout_state
          proxy_read_timeout_state=$(grep -w "proxy_read_timeout:.*" /root/gridenv/nginx.env | cut -f 2 -d ':')
          [[ ${proxy_read_timeout_state} != "60" ]] &&
            /usr/local/bin/gp stack nginx proxy -read-timeout "${proxy_read_timeout_state}"
        fi

        /usr/sbin/nginx -t && /usr/sbin/service nginx restart
        configs_updated="true"
      fi

      local current_configs_version
      case ${configs_branch} in
        master)
          current_configs_version=$(grep -w "configs-version:.*" /opt/gridpane/repos/nginx-configs/gridpane.ver | cut -f 2 -d ':') || true
          ;;
        *) current_configs_version="${configs_branch}" ;;
      esac
      echo "------------------------------------------------------------------"
      echo "GridPane Nginx Configs - Version: ${current_configs_version}"
      echo "------------------------------------------------------------------"

      if [[ $configs_updated == "true" ]] ||
        [[ $current_configs_version != "$local_version" ]]; then
        gridpane::conf_write configs-version "${current_configs_version}"
        gridpane::conf_write configs-update-processing complete
      fi
    else
      update_configs_failed="true"
    fi

    if [[ -n $update_configs_failed ]]; then
      echo "------------------------------------------------------------------"
      echo "GridPane Nginx Configs Update failed"
      echo "------------------------------------------------------------------"
    fi
  else
    [[ ! -d /usr/local/lsws/modsec ]] && /usr/local/bin/gpols modsec | tee -a /var/log/gridpane.log
  fi
  cd ~ || exit
}

#######################################
# Run the Postprocess script
#
# Accepted Args:
#  absolute filepath to script
#######################################
gpupdate::postprocess() {
  [[ -f /opt/gridpane/index.html ]] &&
    rm /opt/gridpane/index.html*

  if [[ -n ${scripts_updated} ||
      -n ${configs_updated} ]]; then
    if [[ -n ${scripts_updated} &&
        -n ${configs_updated} ]]; then
      update="full"
    else
      if [[ -n ${scripts_updated} ]]; then
        update="scripts"
      else
        update="configs"
      fi
    fi
    echo "bash /usr/local/bin/gpupdate-postprocess ${update}" | at now + 0 minute
    echo "#####################################################################################################" | tee -a /var/log/gridpane.log
  else
    echo "No updates were needed.... exiting" | tee -a /var/log/gridpane.log
    echo "#####################################################################################################" | tee -a /var/log/gridpane.log
  fi
}
