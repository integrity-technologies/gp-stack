#!/bin/bash
#
#

#. gridpane.sh

#######################################
# Check PHP versions against Gridpane supported
#
# Outputs:
#  Writes status to STDOUT
#######################################
php::process::version() {

  gridpane::displayfunc

  local ver
  ver="$1"
  local php_nginx_only

  [[ "$webserver" = nginx ]] && php_nginx_only=7.1

  if [[ ! -f /opt/gridpane/default_extra_php ]]; then
    /bin/cat >/opt/gridpane/default_extra_php <<EOF
$php_nginx_only
7.2
7.3
7.4
EOF
  fi

  if [[ ${ver} == "7.1" ||
        ${ver} == "7.2" ||
        ${ver} == "7.3" ||
        ${ver} == "7.4" ||
        $(/bin/cat /opt/gridpane/default_extra_php) == *"${ver}"* ]]; then

    if [[ "$webserver" = nginx ]]; then
      echo "Installing PHP version ${ver}..."
    else
      [[ "$ver" = 7.1 ]] && ver=7.3
      echo "Installing lsphp${ver/./}..."
    fi
  else
    echo "unsupported PHP version.... exiting"
    exit 187
  fi
}

#######################################
# install PHP versions
#
# Outputs:
#  Writes status to STDOUT
#######################################
php::install::version() {

  gridpane::displayfunc

  local ver
  ver="$1"
  local GP_OLS_LSPHP
  GP_OLS_LSPHP="lsphp${ver/./}"

  if [[ "$webserver" = openlitespeed && "$GP_OLS_LSPHP" = lsphp71 ]]; then
    GP_OLS_LSPHP=lsphp73
    echo "############################################################ WARNING: lsphp71 will be replaced by $GP_OLS_LSPHP" |& tee -a /opt/gridpane/logs/php-build.log
    echo "############################################################ Installing $GP_OLS_LSPHP and extensions" |& tee -a /opt/gridpane/logs/php-build.log
  else
    echo "############################################################ Installing PHP ${ver} and extensions" |& tee -a /opt/gridpane/logs/php-build.log
  fi

  gridpane::preinstallaptcheck |& tee -a /opt/gridpane/logs/php-build.log

  if [[ "$webserver" = nginx ]]; then

    sudo apt-get -y install \
    php${ver} php${ver}-common php${ver}-cli php${ver}-fpm php${ver}-curl \
    php${ver}-gd php${ver}-imap php${ver}-readline php${ver}-mysql php${ver}-sqlite3 \
    php${ver}-mbstring php${ver}-bcmath php${ver}-mysql php${ver}-opcache php${ver}-zip \
    php${ver}-xml php${ver}-soap php${ver}-gmp php${ver}-intl php${ver}-xmlrpc php${ver}-tidy \
    php${ver}-imagick graphviz php-pear php-msgpack |& tee -a /opt/gridpane/logs/php-build.log /opt/gridpane/logs/${nonce}-check-php-install.log

  else

    apt-get install -y "$GP_OLS_LSPHP" \
      "$GP_OLS_LSPHP"-curl \
      "$GP_OLS_LSPHP"-imagick \
      "$GP_OLS_LSPHP"-json \
      "$GP_OLS_LSPHP"-mysql \
      "$GP_OLS_LSPHP"-redis |& tee -a /opt/gridpane/logs/php-build.log /opt/gridpane/logs/${nonce}-check-php-install.log

  fi

  local check_php_install
  check_php_install=$(cat /opt/gridpane/logs/${nonce}-check-php-install.log)

  rm /opt/gridpane/logs/"$nonce"-check-php-install.log

  if [[ "$check_php_install" == *"Could not get lock"* ||
        "$check_php_install" == *"Resource temporarily unavailable"* ||
        "$webserver" = openlitespeed && "$check_php_install" != *"Unpacking $GP_OLS_LSPHP"* ]]; then

    gridpane::breakaptlock |& tee -a /opt/gridpane/logs/php-build.log

    if [[ "$webserver" = nginx ]]; then

      sudo apt-get -y install \
      php${ver} php${ver}-common php${ver}-cli php${ver}-fpm php${ver}-curl \
      php${ver}-gd php${ver}-imap php${ver}-readline php${ver}-mysql php${ver}-sqlite3 \
      php${ver}-mbstring php${ver}-bcmath php${ver}-mysql php${ver}-opcache php${ver}-zip \
      php${ver}-xml php${ver}-soap php${ver}-gmp php${ver}-intl php${ver}-xmlrpc php${ver}-tidy \
      php${ver}-imagick graphviz php-pear php-msgpack |& tee -a /opt/gridpane/logs/php-build.log

    else

      apt-get install -y "$GP_OLS_LSPHP" \
        "$GP_OLS_LSPHP"-curl \
        "$GP_OLS_LSPHP"-imagick \
        "$GP_OLS_LSPHP"-json \
        "$GP_OLS_LSPHP"-mysql \
        "$GP_OLS_LSPHP"-redis |& tee -a /opt/gridpane/logs/php-build.log

    fi

  fi

  if [[ ${ver} != "7.4" ]]; then

    local php_ver_tmp="$ver"
    [[ "$webserver" = openlitespeed ]] && ver=7.3

    echo "############################################################ Installing Recode on non PHP ${ver} versions" |& tee -a /opt/gridpane/logs/php-build.log

    sudo apt-get -y install php${ver}-recode |& tee -a /opt/gridpane/logs/php-build.log /opt/gridpane/logs/${nonce}-check-php-install.log
    check_php_install=$(cat /opt/gridpane/logs/${nonce}-check-php-install.log)
    rm /opt/gridpane/logs/${nonce}-check-php-install.log

    if [[ ${check_php_install} == *"Could not get lock"* ]] || [[ ${check_php_install} == *"Resource temporarily unavailable"* ]]; then
      gridpane::breakaptlock |& tee -a /opt/gridpane/logs/php-build.log
      sudo apt-get -y install php${ver}-recode |& tee -a /opt/gridpane/logs/php-build.log
    fi

    [[ "$webserver" = openlitespeed ]] && ver="$php_ver_tmp"
  fi

  if [[ "$webserver" = nginx && ${ver} == "7.1" ]]; then

    echo "############################################################ Installing Mcrypt on PHP ${ver} versions" |& tee -a /opt/gridpane/logs/php-build.log

    sudo apt-get -y install php${ver}-mcrypt |& tee -a /opt/gridpane/logs/php-build.log /opt/gridpane/logs/${nonce}-check-php-install.log
    check_php_install=$(cat /opt/gridpane/logs/${nonce}-check-php-install.log)
    rm /opt/gridpane/logs/${nonce}-check-php-install.log

    if [[ ${check_php_install} == *"Could not get lock"* ]] || [[ ${check_php_install} == *"Resource temporarily unavailable"* ]]; then
      gridpane::breakaptlock |& tee -a /opt/gridpane/logs/php-build.log
      sudo apt-get -y install php${ver}-mcrypt |& tee -a /opt/gridpane/logs/php-build.log
    fi
  fi

  if [[ "$webserver" = nginx ]]; then

    echo "############################################################ Copying PHP ${ver} php.ini and www.conf to /opt/prometheus/templates/source/" |& tee -a /opt/gridpane/logs/php-build.log

    if [[ ! -d /opt/prometheus/templates/source/php/${ver}/fpm/pool.d ]]; then
      mkdir -p /opt/prometheus/templates/source/php/${ver}/fpm/pool.d
    fi

    sudo cp /etc/php/${ver}/fpm/php.ini /opt/prometheus/templates/source/php/${ver}/fpm
    sudo cp /etc/php/${ver}/fpm/pool.d/www.conf /opt/prometheus/templates/source/php/${ver}/fpm/pool.d

  fi

}

#######################################
# Configure PHP ini and pools
#
# Outputs:
#  Writes status to STDOUT
#######################################
php::optimise::version() {
  if [[ "$webserver" = nginx ]]; then

    gridpane::displayfunc

    local ver
    ver="$1"

    echo "############################################################ Update PHP ini parameters" |& tee -a /opt/gridpane/logs/php-build.log

    local maxuploads
    maxuploads="512"

    sudo sed -i '/memory_limit =/c\memory_limit = 256M' /etc/php/${ver}/fpm/php.ini
    sudo sed -i '/max_execution_time =/c\max_execution_time = 600' /etc/php/${ver}/fpm/php.ini
    sudo sed -i '/max_input_vars =/c\max_input_vars = 5000' /etc/php/${ver}/fpm/php.ini
    sudo sed -i '/expose_php =/c\expose_php = Off' /etc/php/${ver}/fpm/php.ini
    sudo sed -i "/upload_max_filesize =/c\upload_max_filesize = ${maxuploads}M" /etc/php/${ver}/fpm/php.ini
    sudo sed -i "/post_max_size =/c\post_max_size = ${maxuploads}M" /etc/php/${ver}/fpm/php.ini
    sudo sed -i '/max_file_uploads =/c\max_file_uploads = 20' /etc/php/${ver}/fpm/php.ini
    sudo sed -i '/session.cookie_httponly =/c\session.cookie_httponly = 1' /etc/php/${ver}/fpm/php.ini

    touch /root/gridenv/php${ver}.env

    echo "memory_limit:256M" >>/root/gridenv/php${ver}.env
    echo "max_execution_time:600" >>/root/gridenv/php${ver}.env
    echo "max_input_vars:5000" >>/root/gridenv/php${ver}.env
    echo "upload_max_filesize:512M" >>/root/gridenv/php${ver}.env
    echo "post_max_size:512M" >>/root/gridenv/php${ver}.env
    echo "max_file_uploads:20" >>/root/gridenv/php${ver}.env

    cat >>/etc/php/${ver}/fpm/php.ini <<EOF
opcache.enable=1
opcache.enable_cli=0
opcache.validate_timestamps=1
opcache.revalidate_freq=60
opcache.max_accelerated_files=10000
opcache.memory_consumption=64
opcache.interned_strings_buffer=8
opcache.fast_shutdown=1
EOF

    echo "opcache.enable:1" >>/root/gridenv/php${ver}.env
    echo "opcache.enable_cli:0" >>/root/gridenv/php${ver}.env
    echo "opcache.revalidate_freq:60" >>/root/gridenv/php${ver}.env
    echo "opcache.max_accelerated_files:10000" >>/root/gridenv/php${ver}.env
    echo "opcache.memory_consumption:128" >>/root/gridenv/php${ver}.env

    cp /etc/php/${ver}/fpm/php.ini /opt/gridpane/php${ver}.ini.bak

    echo "############################################################ Set PHP-FPM logging" |& tee -a /opt/gridpane/logs/php-build.log

    sudo mkdir -p /var/log/php/${ver}
    sudo touch /var/log/php/${ver}/fpm.log
    sudo sed -i "/error_log =/c\error_log = /var/log/php/${ver}/fpm.log" /etc/php/${ver}/fpm/php-fpm.conf
    sudo sed -i '/log_level =/c\log_level = notice' /etc/php/${ver}/fpm/php-fpm.conf

    echo "############################################################ Set PHP www.conf and ports" |& tee -a /opt/gridpane/logs/php-build.log

    cores=$(nproc --all)
    double_cores=$((${cores} * 2))
    quad_cores=$((${cores} * 4))

    sudo sed -i '/pm =/c\pm = dynamic' /etc/php/${ver}/fpm/pool.d/www.conf
    sudo sed -i '/request_terminate_timeout =/c\;request_terminate_timeout = 900' /etc/php/${ver}/fpm/pool.d/www.conf
    sudo sed -i "/pm.max_spare_servers =/c\pm.max_spare_servers = ${quad_cores}" /etc/php/${ver}/fpm/pool.d/www.conf
    sudo sed -i "/pm.min_spare_servers =/c\pm.min_spare_servers = ${cores}" /etc/php/${ver}/fpm/pool.d/www.conf
    sudo sed -i "/pm.start_servers =/c\pm.start_servers = ${double_cores}" /etc/php/${ver}/fpm/pool.d/www.conf
    sudo sed -i "/pm.max_children =/c\pm.max_children = ${quad_cores}" /etc/php/${ver}/fpm/pool.d/www.conf
    sudo sed -i '/pm.max_requests =/c\pm.max_requests = 500' /etc/php/${ver}/fpm/pool.d/www.conf
    sudo sed -i '/pm.status_path =/c\pm.status_path = /status' /etc/php/${ver}/fpm/pool.d/www.conf
    sudo sed -i '/ping.path =/c\ping.path = /ping' /etc/php/${ver}/fpm/pool.d/www.conf

    echo "pm:dynamic" >>/root/gridenv/php${ver}.env
    echo "pm.max_children:${quad_cores}" >>/root/gridenv/php${ver}.env
    echo "pm.max_requests:500" >>/root/gridenv/php${ver}.env
    echo "pm.start_servers:${double_cores}" >>/root/gridenv/php${ver}.env
    echo "pm.min_spare_servers:${cores}" >>/root/gridenv/php${ver}.env
    echo "pm.max_spare_servers:${quad_cores}" >>/root/gridenv/php${ver}.env

    cp /etc/php/${ver}/fpm/pool.d/www.conf /opt/gridpane/php${ver}.www.conf.bak

    cat /root/gridenv/php${ver}.env | sort >/tmp/${nonce}-sorter
    cat /tmp/${nonce}-sorter >/root/gridenv/php${ver}.env
    rm /tmp/${nonce}-sorter

    cp /root/gridenv/php${ver}.env /opt/gridpane/php${ver}.env.bak
    cp /root/gridenv/php${ver}.env /opt/gridpane/env-baks/php${ver}.env.og.bak

    cat /root/gridenv/php${ver}.env |& tee -a /opt/gridpane/logs/php-build.log

    chattr +i /root/gridenv/php${ver}.env

    case ${ver} in
    7.1)
      port_no="9002"
      debug_port_no="9003"
      ;;
    7.2)
      port_no="9000"
      debug_port_no="9001"
      ;;
    7.3)
      port_no="9004"
      debug_port_no="9053"
      ;;
    7.4)
      port_no="9006"
      debug_port_no="9007"
      ;;
    esac

    sudo sed -i "/listen = /c\listen = 127.0.0.1:${port_no}" /etc/php/${ver}/fpm/pool.d/www.conf

    echo "############################################################ Setup PHP slow logging and debug.conf" |& tee -a /opt/gridpane/logs/php-build.log

    sudo touch /var/log/php/${ver}/slow.log
    sudo cp /etc/php/${ver}/fpm/pool.d/www.conf /etc/php/${ver}/fpm/pool.d/debug.conf
    sudo sed -i '/\[www\]/c\[debug]' /etc/php/${ver}/fpm/pool.d/debug.conf
    sudo sed -i '/rlimit_core =/c\rlimit_core = unlimited' /etc/php/${ver}/fpm/pool.d/debug.conf
    sudo sed -i "/slowlog =/c\slowlog = /var/log/php/${ver}/slow.log" /etc/php/${ver}/fpm/pool.d/debug.conf
    sudo sed -i '/request_slowlog_timeout =/c\request_slowlog_timeout = 10s' /etc/php/${ver}/fpm/pool.d/debug.conf

    sudo sed -i "/listen = /c\listen = 127.0.0.1:${debug_port_no}" /etc/php/${ver}/fpm/pool.d/debug.conf

    sudo echo 'php_admin_flag[xdebug.profiler_enable] = off' >>/etc/php/${ver}/fpm/pool.d/debug.conf
    sudo echo 'php_admin_flag[xdebug.profiler_enable_trigger] = on' >>/etc/php/${ver}/fpm/pool.d/debug.conf
    sudo echo 'php_admin_value[xdebug.profiler_output_name] = cachegrind.out.%p-%H-%R' >>/etc/php/${ver}/fpm/pool.d/debug.conf
    sudo echo 'php_admin_value[xdebug.profiler_output_dir] = /tmp/' >>/etc/php/${ver}/fpm/pool.d/debug.conf
    sudo sed -i '/zend_extension=/c\;zend_extension=xdebug.so' /etc/php/${ver}/mods-available/xdebug.ini

    sudo service php${ver}-fpm reload |& tee -a /opt/gridpane/logs/php-build.log
    sudo service php${ver}-fpm start |& tee -a /opt/gridpane/logs/php-build.log /opt/gridpane/provision2.log

  fi
}

#######################################
# Some verbose description here...
#
# Globals:
#   monit_restart
#######################################
php::addto::monit() {
  if [[ "$webserver" = nginx ]]; then

    gridpane::displayfunc

    local ver
    ver="$1"
    local monitVer
    monitVer=${ver//./}

    #TODO (Jeff) Look at moving server system values to gridpane func
    local procNo
    procNo=$(nproc)
    local seventy_percent_of_proc
    seventy_percent_of_proc=$(($procNo * 70))
    local ninety_percent_of_proc
    ninety_percent_of_proc=$(($procNo * 90))

    if [[ ! -f /etc/monit/conf.d/php${monitVer} ]]; then
      cat >/etc/monit/conf.d/php${monitVer} <<EOF
check process php${monitVer}-fpm with pidfile /var/run/php/php${ver}-fpm.pid
    start program = "/usr/sbin/service php${ver}-fpm start"
    stop program  = "/usr/sbin/service php${ver}-fpm stop"
    if cpu is greater than XXXSEVENTYPERCENTXXX% for 10 cycles
        then exec "/usr/local/bin/gpmonitor PHP${ver} HOT warning"
        AND repeat every 10 cycles
    if cpu > XXXNINETYPERCENTXXX% for 5 cycles
        then restart
    if 3 restarts within 5 cycles
        then exec "/usr/local/bin/gpmonitor PHP${ver} FAILED error"
EOF
    fi

    monit_php_config=$(cat /etc/monit/conf.d/php${monitVer})

    if [[ ${monit_php_config} == *"XXXSEVENTYPERCENTXXX"* ]]; then
      sed -i "s/XXXSEVENTYPERCENTXXX/${seventy_percent_of_proc}/g" /etc/monit/conf.d/php${monitVer}
      monit_restart="true"
    fi

    if [[ ${monit_php_config} == *"XXXNINETYPERCENTXXX"* ]]; then
      sed -i "s/XXXNINETYPERCENTXXX/${ninety_percent_of_proc}/g" /etc/monit/conf.d/php${monitVer}
      monit_restart="true"
    fi

    if [[ ${monit_restart} != "true" ]]; then
      monit_restart="false"
    fi

    gridpane::conf_write monit-php${ver} true

  fi
}

#######################################
# Some verbose description here...
#
# Globals:
#   monit_restart
#######################################
php::report::status() {

  gridpane::displayfunc

  local php_ver="$1"
  [[ "$webserver" = openlitespeed && "$php_ver" = 7.1 ]] && php_ver=7.3
  local lsphp_ver
  lsphp_ver=lsphp"${php_ver/./}"
  local check_status

  if [[ "$webserver" = nginx ]]; then
    check_status="$(/usr/sbin/service php${php_ver}-fpm status)"
  fi

  if [[ "$webserver" = nginx && ${check_status} == *"could not be found"* ||
        "$webserver" = openlitespeed && ! -f /usr/local/lsws/"$lsphp_ver"/bin/lsphp ]]; then
    local title

    if [[ "$webserver" = nginx ]]; then
      title="PHP${php_ver}-fpm installation failed!"
    else
      title="$lsphp_ver installation failed!"
    fi

    local body
    body="Post installation check reported:<br>Please contact support for help. Do not attempt to use PHP. Thank you."
    local channel
    channel="popup_and_center"
    local icon
    icon="fa-exclamation"
    local duration
    duration="infinity"

    gridpane::notify::app "${title}" "${body}" "${channel}" "${icon}" "${duration}"

    body=${body//<br>/ \\n}

    gridpane::notify::slack "${serverIP}" "error" "${title}" "${body}"
  fi
}

php::sync::site_to_app() {
  local site="$1"
  # version
  local php_version
  php_version=$(gridpane::get::site::php ${site})
  local pool_d_conf_path
  local pool_d_conf
  if [[ "$webserver" = nginx ]]; then
    pool_d_conf_path="/etc/php/${php_version}/fpm/pool.d/${site}.conf"
    pool_d_conf=$(cat ${pool_d_conf_path})
  fi
  local site_ini_path
  site_ini_path="/var/www/${site}/htdocs/.user.ini"
  local site_ini
  site_ini=$(cat ${site_ini_path})
  local cores
  cores=$(nproc --all)
  local quad_cores
  quad_cores=$((${cores} * 4))

  ensureLogExists ${site} php
  logIt gridpane-general sync-php Begin ${site}
  output_and_log="| tee -a /var/www/${site}/logs/gridpane-general.log"
  output_to_log=">>/var/www/${site}/logs/gridpane-general.log"

  local site_pm
  local site_pm_max_children
  local site_pm_start_servers
  local site_pm_max_spare_servers
  local site_pm_min_spare_servers
  local site_pm_max_requests
  local site_pm_process_idle_timeout
  local is_php_slowlog
  local site_php_slowlog_timeout
  local site_php_slowlog_trace_depth
  # set lsapi stuff and send back too now...
  local lsapi
  local lsapi_max_connections
  local lsapi_children
  local lsapi_app_instances
  local lsapi_max_idle
  local lsapi_max_reqs
  local lsapi_initial_request_timeout
  local lsapi_retry_timeout

  if [[ "$webserver" = nginx ]]; then
    # pm
    site_pm=$(grep "pm =" "${pool_d_conf_path}")
    site_pm=${site_pm#pm =}
    site_pm="$(echo -e "${site_pm}" | tr -d '[:space:]')"
    if [[ ${site_pm} != "dynamic" && ${site_pm} != "static" && ${site_pm} != "ondemand" ]]; then
      site_pm="dynamic"
      sed -i "/pm =/c\pm = ${site_pm}" ${pool_d_conf_path}
    fi
    gridpane::conf_write pm "${site_pm}" -site.env ${site}
    grep "pm =" "${pool_d_conf_path}" | tee -a /var/www/${site}/logs/gridpane-general.log

    # pm.max_children
    site_pm_max_children=$(grep "pm.max_children =" "${pool_d_conf_path}")
    site_pm_max_children=${site_pm_max_children#pm.max_children =}
    site_pm_max_children="$(echo -e "${site_pm_max_children}" | tr -d '[:space:]')"
    if [[ ! ${site_pm_max_children} =~ ^[0-9]+$ ]]; then
      site_pm_max_children="${quad_cores}"
      sed -i "/pm.max_children =/c\pm.max_children = ${site_pm_max_children}" ${pool_d_conf_path}
    fi
    gridpane::conf_write pm.max_children ${site_pm_max_children} -site.env ${site}
    grep "pm.max_children =" "${pool_d_conf_path}" | tee -a /var/www/${site}/logs/gridpane-general.log

    # pm.start_servers
    site_pm_start_servers=$(grep "pm.start_servers =" "${pool_d_conf_path}")
    site_pm_start_servers=${site_pm_start_servers#pm.start_servers =}
    site_pm_start_servers="$(echo -e "${site_pm_start_servers}" | tr -d '[:space:]')"
    if [[ ! ${site_pm_start_servers} =~ ^[0-9]+$ ]]; then
      site_pm_start_servers="1"
      sed -i "/pm.start_servers =/c\pm.start_servers = ${site_pm_start_servers}" ${pool_d_conf_path}
    fi
    gridpane::conf_write pm.start_servers ${site_pm_start_servers} -site.env ${site}
    grep "pm.start_servers =" "${pool_d_conf_path}" | tee -a /var/www/${site}/logs/gridpane-general.log

    # pm.max_spare_servers
    site_pm_max_spare_servers=$(grep "pm.max_spare_servers =" "${pool_d_conf_path}")
    site_pm_max_spare_servers=${site_pm_max_spare_servers#pm.max_spare_servers =}
    site_pm_max_spare_servers="$(echo -e "${site_pm_max_spare_servers}" | tr -d '[:space:]')"
    if [[ ! ${site_pm_max_spare_servers} =~ ^[0-9]+$ ]]; then
      site_pm_max_spare_servers="1"
      sed -i "/pm.max_spare_servers =/c\pm.max_spare_servers = ${site_pm_max_spare_servers}" ${pool_d_conf_path}
    fi
    gridpane::conf_write pm.max_spare_servers ${site_pm_max_spare_servers} -site.env ${site}
    grep "pm.max_spare_servers =" "${pool_d_conf_path}" | tee -a /var/www/${site}/logs/gridpane-general.log

    # pm.min_spare_servers
    site_pm_min_spare_servers=$(grep "pm.min_spare_servers =" "${pool_d_conf_path}")
    site_pm_min_spare_servers=${site_pm_min_spare_servers#pm.min_spare_servers =}
    site_pm_min_spare_servers="$(echo -e "${site_pm_min_spare_servers}" | tr -d '[:space:]')"
    if [[ ! ${site_pm_min_spare_servers} =~ ^[0-9]+$ ]]; then
      site_pm_min_spare_servers="1"
      sed -i "/pm.min_spare_servers =/c\pm.min_spare_servers = ${site_pm_min_spare_servers}" ${pool_d_conf_path}
    fi
    gridpane::conf_write pm.min_spare_servers ${site_pm_min_spare_servers} -site.env ${site}
    grep "pm.min_spare_servers =" "${pool_d_conf_path}" | tee -a /var/www/${site}/logs/gridpane-general.log

    # pm.max_requests
    site_pm_max_requests=$(grep "pm.max_requests =" "${pool_d_conf_path}")
    site_pm_max_requests=${site_pm_max_requests#pm.max_requests =}
    site_pm_max_requests="$(echo -e "${site_pm_max_requests}" | tr -d '[:space:]')"
    if [[ ! ${site_pm_max_requests} =~ ^[0-9]+$ ]]; then
      site_pm_max_requests="500"
      sed -i "/pm.max_requests =/c\pm.max_requests = ${site_pm_max_requests}" ${pool_d_conf_path}
    fi
    gridpane::conf_write pm.max_requests ${site_pm_max_requests} -site.env ${site}
    grep "pm.max_requests =" "${pool_d_conf_path}" | tee -a /var/www/${site}/logs/gridpane-general.log

    # pm.max_requests
    site_pm_process_idle_timeout=$(grep "pm.process_idle_timeout =" "${pool_d_conf_path}")
    site_pm_process_idle_timeout=${site_pm_process_idle_timeout#pm.process_idle_timeout =}
    site_pm_process_idle_timeout="$(echo -e "${site_pm_process_idle_timeout}" | tr -d '[:space:]')"
    if [[ ! ${site_pm_process_idle_timeout} =~ ^[0-9]+$ ]]; then
      site_pm_process_idle_timeout="10"
      sed -i "/pm.process_idle_timeout =/c\pm.process_idle_timeout = ${site_pm_process_idle_timeout}" ${pool_d_conf_path}
    fi
    gridpane::conf_write pm.process_idle_timeout ${site_pm_process_idle_timeout} -site.env ${site}
    grep "pm.process_idle_timeout =" "${pool_d_conf_path}" | tee -a /var/www/${site}/logs/gridpane-general.log

    site_php_slowlog_timeout=$(grep "request_slowlog_timeout =" "${pool_d_conf_path}")
    if [[ ${site_php_slowlog_timeout} == ";request_slowlog_timeout ="* ]]; then
      is_php_slowlog="false"
    elif [[ ${site_php_slowlog_timeout} == "request_slowlog_timeout ="* ]]; then
      is_php_slowlog="true"
    else
      is_php_slowlog="false"
    fi
    gridpane::conf_write php_slowlog ${is_php_slowlog} -site.env ${site}

    site_php_slowlog_timeout="$(echo -e "${site_php_slowlog_timeout}" | tr -d ';')"
    site_php_slowlog_timeout=${site_php_slowlog_timeout#request_slowlog_timeout =}
    site_php_slowlog_timeout="$(echo -e "${site_php_slowlog_timeout}" | tr -d '[:space:]')"
    if [[ ! ${site_php_slowlog_timeout} =~ ^[0-9]+$ ]]; then
      site_php_slowlog_timeout="10"
    fi
    gridpane::conf_write php_slowlog_timeout ${site_php_slowlog_timeout} -site.env ${site}

    site_php_slowlog_trace_depth=$(grep "request_slowlog_trace_depth =" "${pool_d_conf_path}")
    site_php_slowlog_trace_depth="$(echo -e "${site_php_slowlog_trace_depth}" | tr -d ';')"
    site_php_slowlog_trace_depth=${site_php_slowlog_trace_depth#request_slowlog_trace_depth =}
    site_php_slowlog_trace_depth="$(echo -e "${site_php_slowlog_trace_depth}" | tr -d '[:space:]')"
    if [[ ! ${site_php_slowlog_trace_depth} =~ ^[0-9]+$ ]]; then
      site_php_slowlog_trace_depth="10"
    fi
    gridpane::conf_write php_slowlog_trace_depth ${site_php_slowlog_trace_depth} -site.env ${site}

  else

    lsapi="$(/usr/local/bin/gpols get "$site" lsapi)"
    if [[ -z "$lsapi" ]]; then
      lsapi=processgroup
      gridpane::conf_write lsapi "$lsapi" -site.env "$site"
    fi

    lsapi_max_connections="$(/usr/local/bin/gpols get "$site" lsapi_max_connections)"
    if [[ -z "$lsapi_max_connections" ]]; then
      lsapi_max_connections=35
      gridpane::conf_write lsapi_max_connections "$lsapi_max_connections" -site.env "$site"
    fi

    if [[ "$lsapi" = processgroup ]]; then
      lsapi_children="$lsapi_max_connections"
      lsapi_app_instances="$(/usr/local/bin/gpols get "$site" lsapi_app_instances)"
      if [[ -z "$lsapi_app_instances" ]]; then
        lsapi_app_instances=1
      fi
    elif [[ "$lsapi" = worker ]]; then
      lsapi_children=1
      lsapi_app_instances="$lsapi_max_connections"
    fi

    lsapi_max_idle="$(/usr/local/bin/gpols get "$site" lsapi_max_idle)"
    if [[ -z "$lsapi_max_idle" ]]; then
      lsapi_max_idle=300
      gridpane::conf_write lsapi_max_idle "$lsapi_max_idle" -site.env "$site"
    fi

    lsapi_max_reqs="$(/usr/local/bin/gpols get "$site" lsapi_max_reqs)"
    if [[ -z "$lsapi_max_reqs" ]]; then
      lsapi_max_reqs=5000
      gridpane::conf_write lsapi_max_reqs "$lsapi_max_reqs" -site.env "$site"
    fi

    lsapi_initial_request_timeout="$(/usr/local/bin/gpols get "$site" lsapi_initial_request_timeout)"
    if [[ -z "$lsapi_initial_request_timeout" ]]; then
      lsapi_initial_request_timeout=60
      gridpane::conf_write lsapi_initial_request_timeout "$lsapi_initial_request_timeout" -site.env "$site"
    fi

    lsapi_retry_timeout="$(/usr/local/bin/gpols get "$site" lsapi_retry_timeout)"
    if [[ -z "$lsapi_retry_timeout" ]]; then
      lsapi_retry_timeout
      gridpane::conf_write lsapi_retry_timeout "$lsapi_retry_timeout" -site.env "$site"
    fi

    lsapi_divide="$(/bin/echo "scale=2; $lsapi_children / 3" | /usr/bin/bc)"
    # Round the value to nearest
    lsapi_divide_round="$(/usr/bin/printf "%.0f\n" "$lsapi_divide")"
    lsapi_max_idle_children="$lsapi_divide_round"
    gridpane::conf_write lsapi_max_idle_children "$lsapi_divide_round" -site.env "$site"
  fi

  # date.timezone
  local site_date_timezone
  if [[ ${site_ini} == *"date.timezone ="* ]]; then
    site_date_timezone=$(grep "date.timezone =" "${site_ini_path}")
    if [[ ${site_date_timezone} == ";date.timezone ="* ]]; then
      site_date_timezone="NULL"
    else
      site_date_timezone="${site_date_timezone#date.timezone =}"
      site_date_timezone="$(echo -e "${site_date_timezone}" | tr -d '[:space:]')"
      local accepted_timezones
      accepted_timezones=$(grep "${site_date_timezone}" /opt/gridpane/php-timezones/php-timezones.env)
      if [[ -z ${accepted_timezones} ]]; then
        echo "${accepted_timezones}"
        echo "not acceptable timezone - ${site_date_timezone}"
        site_date_timezone="NULL"
        sed -i "/date.timezone =/c\;date.timezone =" ${site_ini_path}
      fi
    fi
  else
    site_date_timezone="NULL"
    echo ";date.timezone =" >>${site_ini_path}
  fi
  gridpane::conf_write date.timezone ${site_date_timezone} -site.env ${site}
  grep "date.timezone =" "${site_ini_path}"  | tee -a /var/www/${site}/logs/gridpane-general.log

  # session.gc_maxlifetime
  local site_default_socket_timeout
  if [[ ${site_ini} == *"default_socket_timeout ="* ]]; then
    site_default_socket_timeout=$(grep "default_socket_timeout =" "${site_ini_path}")
    site_default_socket_timeout=${site_default_socket_timeout#default_socket_timeout =}
    site_default_socket_timeout="$(echo -e "${site_default_socket_timeout}" | tr -d '[:space:]')"
    if [[ ! ${site_default_socket_timeout} =~ ^(-)?[0-9]+$ ]]; then
      site_default_socket_timeout="60"
      sed -i "/default_socket_timeout =/c\default_socket_timeout = ${site_default_socket_timeout}" ${site_ini_path}
    fi
  else
    site_default_socket_timeout="60"
    echo "default_socket_timeout = ${site_default_socket_timeout}" >>${site_ini_path}
  fi
  gridpane::conf_write default_socket_timeout ${site_default_socket_timeout} -site.env ${site}
  grep "default_socket_timeout =" "${site_ini_path}"  | tee -a /var/www/${site}/logs/gridpane-general.log

  # session.gc_maxlifetime
  local site_session_gc_maxlifetime
  if [[ ${site_ini} == *"session.gc_maxlifetime ="* ]]; then
    site_session_gc_maxlifetime=$(grep "session.gc_maxlifetime =" "${site_ini_path}")
    site_session_gc_maxlifetime=${site_session_gc_maxlifetime#session.gc_maxlifetime =}
    site_session_gc_maxlifetime="$(echo -e "${site_session_gc_maxlifetime}" | tr -d '[:space:]')"
    if [[ ! ${site_session_gc_maxlifetime} =~ ^[0-9]+$ ]]; then
      site_session_gc_maxlifetime="1440"
      sed -i "/session.gc_maxlifetime =/c\session.gc_maxlifetime = ${site_session_gc_maxlifetime}" ${site_ini_path}
    fi
  else
    site_session_gc_maxlifetime="1440"
    echo "session.gc_maxlifetime = ${site_session_gc_maxlifetime}" >>${site_ini_path}
  fi
  gridpane::conf_write session.gc_maxlifetime ${site_session_gc_maxlifetime} -site.env ${site}
  grep "session.gc_maxlifetime =" "${site_ini_path}"  | tee -a /var/www/${site}/logs/gridpane-general.log

  # session.cookie_lifetime
  local site_session_cookie_lifetime
  if [[ ${site_ini} == *"session.cookie_lifetime ="* ]]; then
    site_session_cookie_lifetime=$(grep "session.cookie_lifetime =" "${site_ini_path}")
    site_session_cookie_lifetime=${site_session_cookie_lifetime#session.cookie_lifetime =}
    site_session_cookie_lifetime="$(echo -e "${site_session_cookie_lifetime}" | tr -d '[:space:]')"
    if [[ ! ${site_session_cookie_lifetime} =~ ^[0-9]+$ ]]; then
      site_session_cookie_lifetime="0"
      sed -i "/session.cookie_lifetime =/c\session.cookie_lifetime = ${site_session_cookie_lifetime}" ${site_ini_path}
    fi
  else
    site_session_cookie_lifetime="0"
    echo "session.cookie_lifetime = ${site_session_cookie_lifetime}" >>${site_ini_path}
  fi
  gridpane::conf_write session.cookie_lifetime ${site_session_cookie_lifetime} -site.env ${site}
  grep "session.cookie_lifetime =" "${site_ini_path}"  | tee -a /var/www/${site}/logs/gridpane-general.log

  # short_open_tag
  local site_short_open_tag
  if [[ ${site_ini} == *"short_open_tag ="* ]]; then
    site_short_open_tag=$(grep "short_open_tag =" "${site_ini_path}")
    site_short_open_tag=${site_short_open_tag#short_open_tag =}
    site_short_open_tag="$(echo -e "${site_short_open_tag}" | tr -d '[:space:]')"
    if [[ ${site_short_open_tag} != "On" && ${site_short_open_tag} != "on" && ${site_short_open_tag} != "Off" && ${site_short_open_tag} != "off" ]]; then
      site_short_open_tag="Off"
      sed -i "/short_open_tag =/c\short_open_tag = ${site_short_open_tag}" ${site_ini_path}
    fi
  else
    site_short_open_tag="Off"
    echo "short_open_tag = ${site_short_open_tag}" >>${site_ini_path}
  fi
  gridpane::conf_write short_open_tag ${site_short_open_tag} -site.env ${site}
  if [[ ${site_short_open_tag} == "Off" || ${site_short_open_tag} == "off" ]]; then
    site_short_open_tag="false"
  else
    site_short_open_tag="true"
  fi
  grep "short_open_tag =" "${site_ini_path}"  | tee -a /var/www/${site}/logs/gridpane-general.log

  # post_max_size
  local site_post_max_size
  if [[ ${site_ini} == *"post_max_size ="* ]]; then
    site_post_max_size=$(grep "post_max_size =" "${site_ini_path}")
    site_post_max_size=${site_post_max_size#post_max_size =}
    site_post_max_size=${site_post_max_size%M}
    site_post_max_size=${site_post_max_size%G}
    site_post_max_size="$(echo -e "${site_post_max_size}" | tr -d '[:space:]')"
    if [[ ! ${site_post_max_size} =~ ^[0-9]+$  ]]; then
      site_post_max_size="512"
      sed -i "/post_max_size =/c\post_max_size = ${site_post_max_size}M" ${site_ini_path}
    fi
  else
    site_post_max_size="512"
    echo "post_max_size = ${site_post_max_size}M" >>${site_ini_path}
  fi
  gridpane::conf_write post_max_size ${site_post_max_size}M -site.env ${site}
  grep "post_max_size =" "${site_ini_path}"  | tee -a /var/www/${site}/logs/gridpane-general.log

  # max_file_uploads
  local site_max_file_uploads
  if [[ ${site_ini} == *"max_file_uploads ="* ]]; then
    site_max_file_uploads=$(grep "max_file_uploads =" "${site_ini_path}")
    site_max_file_uploads=${site_max_file_uploads#max_file_uploads =}
    site_max_file_uploads="$(echo -e "${site_max_file_uploads}" | tr -d '[:space:]')"
    if [[ ! ${site_max_file_uploads} =~ ^[0-9]+$  ]]; then
      site_max_file_uploads="20"
      sed -i "/max_file_uploads =/c\max_file_uploads = ${site_max_file_uploads}" ${site_ini_path}
    fi
  else
    site_max_file_uploads="20"
    echo "max_file_uploads = ${site_max_file_uploads}M" >>${site_ini_path}
  fi
  gridpane::conf_write max_file_uploads ${site_max_file_uploads} -site.env ${site}
  grep "max_file_uploads =" "${site_ini_path}"  | tee -a /var/www/${site}/logs/gridpane-general.log

  # upload_max_filesize
  local site_upload_max_filesize
  if [[ ${site_ini} == *"upload_max_filesize ="* ]]; then
    site_upload_max_filesize=$(grep "upload_max_filesize =" "${site_ini_path}")
    site_upload_max_filesize=${site_upload_max_filesize#upload_max_filesize =}
    site_upload_max_filesize=${site_upload_max_filesize%M}
    site_upload_max_filesize=${site_upload_max_filesize%G}
    site_upload_max_filesize="$(echo -e "${site_upload_max_filesize}" | tr -d '[:space:]')"
    if [[ ! ${site_upload_max_filesize} =~ ^[0-9]+$  ]]; then
      site_upload_max_filesize="512"
      sed -i "/upload_max_filesize =/c\upload_max_filesize = ${site_upload_max_filesize}M" ${site_ini_path}
    fi
  else
    site_upload_max_filesize="512"
    echo "upload_max_filesize = ${site_upload_max_filesize}M" >>${site_ini_path}
  fi
  gridpane::conf_write upload_max_filesize ${site_upload_max_filesize}M -site.env ${site}
  grep "upload_max_filesize =" "${site_ini_path}"  | tee -a /var/www/${site}/logs/gridpane-general.log

  # max_input_time
  local site_max_input_time
  if [[ ${site_ini} == *"max_input_time ="* ]]; then
    site_max_input_time=$(grep "max_input_time =" "${site_ini_path}")
    site_max_input_time=${site_max_input_time#max_input_time =}
    site_max_input_time="$(echo -e "${site_max_input_time}" | tr -d '[:space:]')"
    if [[ ! ${site_max_input_time} =~ ^(-)?[0-9]+$  ]]; then
      site_max_input_time="60"
      sed -i "/max_input_time =/c\max_input_time = ${site_max_input_time}" ${site_ini_path}
    fi
  else
    site_max_input_time="60"
    echo "max_input_time = ${site_max_input_time}" >>${site_ini_path}
  fi
  gridpane::conf_write max_input_time ${site_max_input_time} -site.env ${site}
  grep "max_input_time =" "${site_ini_path}"  | tee -a /var/www/${site}/logs/gridpane-general.log

  # max_input_vars
  local site_max_input_vars
  if [[ ${site_ini} == *"max_input_vars ="* ]]; then
    site_max_input_vars=$(grep "max_input_vars =" "${site_ini_path}")
    site_max_input_vars=${site_max_input_vars#max_input_vars =}
    site_max_input_vars="$(echo -e "${site_max_input_vars}" | tr -d '[:space:]')"
    if [[ ! ${site_max_input_vars} =~ ^[0-9]+$  ]]; then
      site_max_input_vars="5000"
      sed -i "/max_input_vars =/c\max_input_vars = ${site_max_input_vars}" ${site_ini_path}
    fi
  else
    site_max_input_vars="5000"
    echo "max_input_vars = ${site_max_input_vars}"  >>"${site_ini_path}"
  fi
  gridpane::conf_write max_input_vars ${site_max_input_vars} -site.env ${site}
  grep "max_input_vars =" "${site_ini_path}"  | tee -a /var/www/${site}/logs/gridpane-general.log

  # max_execution_time
  local site_max_execution_time
  if [[ ${site_ini} == *"max_execution_time ="* ]]; then
    site_max_execution_time=$(grep "max_execution_time =" "${site_ini_path}")
    site_max_execution_time=${site_max_execution_time#max_execution_time =}
    site_max_execution_time="$(echo -e "${site_max_execution_time}" | tr -d '[:space:]')"
    if [[ ! ${site_max_execution_time} =~ ^[0-9]+$  ]]; then
      site_max_execution_time="300"
      sed -i "/max_execution_time =/c\max_execution_time = ${site_max_execution_time}" ${site_ini_path}
    fi
  else
    site_max_execution_time="300"
    echo "max_execution_time = ${site_max_execution_time}"  >>"${site_ini_path}"
  fi
  gridpane::conf_write max_execution_time ${site_max_execution_time} -site.env ${site}
  grep "max_execution_time =" "${site_ini_path}"  | tee -a /var/www/${site}/logs/gridpane-general.log

  # memory_limit
  local site_memory_limit
  if [[ ${site_ini} == *"memory_limit ="* ]]; then
    site_memory_limit=$(grep "memory_limit =" "${site_ini_path}")
    site_memory_limit=${site_memory_limit#memory_limit =}
    site_memory_limit=${site_memory_limit%M}
    site_memory_limit=${site_memory_limit%G}
    site_memory_limit="$(echo -e "${site_memory_limit}" | tr -d '[:space:]')"
    if [[ ! ${site_memory_limit} =~ ^[0-9]+$  ]]; then
      site_memory_limit="256"
      sed -i "/memory_limit =/c\memory_limit = ${site_memory_limit}M" ${site_ini_path}
    fi
  else
    site_memory_limit="256"
    echo "memory_limit = ${site_memory_limit}M"  >>"${site_ini_path}"
  fi
  gridpane::conf_write memory_limit ${site_memory_limit} -site.env ${site}
  grep "memory_limit =" "${site_ini_path}"  | tee -a /var/www/${site}/logs/gridpane-general.log

  if [[ "$webserver" = openlitespeed ]]; then

    echo "lsapi = $lsapi" | tee -a /var/www/${site}/logs/gridpane-general.log
    echo "lsapi_max_connections = $lsapi_max_connections" | tee -a /var/www/${site}/logs/gridpane-general.log
    echo "lsapi_children = $lsapi_children" | tee -a /var/www/${site}/logs/gridpane-general.log
    echo "lsapi_app_instances = $lsapi_app_instances" | tee -a /var/www/${site}/logs/gridpane-general.log
    echo "lsapi_initial_request_timeout = $lsapi_initial_request_timeout" | tee -a /var/www/${site}/logs/gridpane-general.log
    echo "lsapi_retry_timeout = $lsapi_retry_timeout" | tee -a /var/www/${site}/logs/gridpane-general.log
    echo "lsapi_max_reqs = $lsapi_max_reqs" | tee -a /var/www/${site}/logs/gridpane-general.log
    echo "lsapi_max_idle = $lsapi_max_idle" | tee -a /var/www/${site}/logs/gridpane-general.log
    echo "lsapi_max_idle_children = $lsapi_max_idle_children" | tee -a /var/www/${site}/logs/gridpane-general.log
    echo "php_version = ${php_version}" | tee -a /var/www/${site}/logs/gridpane-general.log

    # based on same defaults as site build temp...
    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=${site}" \
      "server_ip=${serverIP}" \
      "-s" "php_version=${php_version}" \
      "lsapi=${lsapi}" \
      "-s" "lsapi_max_connections=${lsapi_max_connections}" \
      "-s" "lsapi_children=${lsapi_children}" \
      "-s" "lsapi_initial_request_timeout=${lsapi_initial_request_timeout}" \
      "-s" "lsapi_retry_timeout=${lsapi_retry_timeout}" \
      "-s" "lsapi_app_instances=${lsapi_app_instances}" \
      "-s" "lsapi_max_reqs=${lsapi_max_reqs}" \
      "-s" "lsapi_max_idle=${lsapi_max_idle}" \
      "-s" "date.timezone=${site_date_timezone}" \
      "-s" "default_socket_timeout=${site_default_socket_timeout}" \
      "-s" "session.gc_maxlifetime=${site_session_gc_maxlifetime}" \
      "-s" "session.cookie_lifetime=${site_session_cookie_lifetime}" \
      "-s" "post_max_size=${site_post_max_size}" \
      "-s" "max_file_uploads=${site_max_file_uploads}" \
      "-s" "upload_max_filesize=${site_upload_max_filesize}" \
      "-s" "max_input_time=${site_max_input_time}" \
      "-s" "max_input_vars=${site_max_input_vars}" \
      "-s" "max_execution_time=${site_max_execution_time}" \
      "-s" "memory_limit=${site_memory_limit}" \
      "short_open_tag@${site_short_open_tag}" \
      "php_settings_synced_at@true" \
      "silent@true"

  else

    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=${site}" \
      "server_ip=${serverIP}" \
      "-s" "php_version=${php_version}" \
      "pm=${site_pm}" \
      "-s" "pm.max_children=${site_pm_max_children}" \
      "-s" "pm.max_requests=${site_pm_max_requests}" \
      "-s" "pm.start_servers=${site_pm_start_servers}" \
      "-s" "pm.min_spare_servers=${site_pm_min_spare_servers}" \
      "-s" "pm.max_spare_servers=${site_pm_max_spare_servers}" \
      "-s" "pm.process_idle_timeout=${site_pm_process_idle_timeout}" \
      "is_php_slowlog@${is_php_slowlog}" \
      "-s" "php_slowlog_timeout=${site_php_slowlog_timeout}" \
      "-s" "php_slowlog_trace_depth=${site_php_slowlog_trace_depth}" \
      "-s" "date.timezone=${site_date_timezone}" \
      "-s" "default_socket_timeout=${site_default_socket_timeout}" \
      "-s" "session.gc_maxlifetime=${site_session_gc_maxlifetime}" \
      "-s" "session.cookie_lifetime=${site_session_cookie_lifetime}" \
      "-s" "post_max_size=${site_post_max_size}" \
      "-s" "max_file_uploads=${site_max_file_uploads}" \
      "-s" "upload_max_filesize=${site_upload_max_filesize}" \
      "-s" "max_input_time=${site_max_input_time}" \
      "-s" "max_input_vars=${site_max_input_vars}" \
      "-s" "max_execution_time=${site_max_execution_time}" \
      "-s" "memory_limit=${site_memory_limit}" \
      "short_open_tag@${site_short_open_tag}" \
      "php_settings_synced_at@true" \
      "silent@false"

      gridpane::callback::app \
        "/site/site-update" \
        "--" \
        "site_url=${site}" \
        "server_ip=${serverIP}" \
        "is_php_slowlog@${is_php_slowlog}" \
        "-s" "php_slowlog_timeout=${site_php_slowlog_timeout}" \
        "-s" "php_slowlog_trace_depth=${site_php_slowlog_trace_depth}" \
        "silent@false"

  fi

 logIt gridpane-general sync-php Continue ${site}

}

php::configure() {
  local php_ver
  local configure_command
  local parameter_value
  local env_type
  local site_or_ver
  local ini_file

  local sitesConfigArray
  if [[ "$webserver" = nginx ]]; then
    sitesConfigArray=$(dir /etc/nginx/sites-enabled)
  elif [[ "$webserver" = openlitespeed ]]; then
    sitesConfigArray=$(dir /usr/local/lsws/conf/vhosts)
  fi

  if [[ $1 != *"7.1"* && $1 != *"7.2"* && $1 != *"7.3"* && $1 != *"7.4"* ]]; then
    if [[ $1 == "site-"* ]] || [[ $1 == "-site-"* ]]; then
      # gp stack php -site-pm ondemand site.tld
      configure_command="$1"
      if [[ ${configure_command} == *"reset" ]]; then
        site_to_configure_user_ini="$2"
      else
        parameter_value="$2"
        site_to_configure_user_ini="$3"
      fi

      if [[ -z ${site_to_configure_user_ini} ]]; then
        echo "If you want to configure a PHP setting per site, then you need to pass the site domain, exiting..."
        exit 187
      fi

      if [[ ${sitesConfigArray} != *"${site_to_configure_user_ini}"* ]]; then
        echo "Site domain passed ${site_to_configure_user_ini} is not an active and enabled site, exiting..."
        exit 187
      fi

      php_ver=$(gridpane::get::site::php ${site_to_configure_user_ini})

      if [[ ${configure_command} == *"site-pm"* ||
            ${configure_command} == *"site-slowlog"* ]]; then
        if [[ "$webserver" = openlitespeed ]]; then
          echo "This is a GridPane OpenLiteSpeed server, PHP-FPM settings are not an option, exiting..."
          exit 187
        fi
        ini_file="/etc/php/${php_ver}/fpm/pool.d/${site_to_configure_user_ini}.conf"
      else
        if [[ ! -f /var/www/${site_to_configure_user_ini}/htdocs/.user.ini ]]; then
          touch /var/www/${site_to_configure_user_ini}/htdocs/.user.ini
        fi
        ini_file="/var/www/${site_to_configure_user_ini}/htdocs/.user.ini"
      fi

      env_type="-site.env"
      site_or_ver="${site_to_configure_user_ini}"

      ensureLogExists ${site_or_ver} gridpane-general
      logIt gridpane-general configure-php Begin ${site_or_ver}
    else
      echo "No valid PHP version passed, and this isn't a site level PHP configuration, unable to comply, exiting..."
      exit 187
    fi
  else
    if [[ "$webserver" = openlitespeed ]]; then
      #TODO (Jeff) We will need per version PHP ini commands to match fpm gp-cli
      echo "This is a GridPane OpenLiteSpeed server, changing PHP.ini by FPM version is not possible, exiting..."
      exit 187
    fi
    php_ver="$1"
    configure_command="$2"
    parameter_value="$3"
    env_type="-php.env"
    site_or_ver="${php_ver}"
    ini_file="/etc/php/${php_ver}/fpm/php.ini"
  fi

  local check_for_parameter
  check_for_parameter=$(cat ${ini_file})
  local current_pm

  if [[ ${configure_command} == "date-timezone" ]] || [[ ${configure_command} == "-date-timezone" ]] ||
    [[ ${configure_command} == "site-date-timezone" ]] || [[ ${configure_command} == "-site-date-timezone" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a supported timezone to be passed as a string, please see https://www.php.net/manual/en/timezones.php"
    else
      accepted_timezones=$(cat /opt/gridpane/php-timezones/php-timezones.env)
      if [[ ${accepted_timezones} != *"${parameter_value}"* ]] && [[ ${parameter_value} != "UTC" ]]; then
        echo "${configure_command} requires a supported timezone to be passed, ${parameter_value} is not supported."
        echo "Please see https://www.php.net/manual/en/timezones.php for a full list of supported timezones."
        [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
      else
        if [[ ${parameter_value} == "UTC" ]]; then
          if [[ ${check_for_parameter} == *"date.timezone ="* ]]; then
            sed -i "/date.timezone =/c\;date.timezone =" ${ini_file}
          else
            echo ";date.timezone =" >>${ini_file}
          fi
          echo ";date.timezone =" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          gridpane::conf_write date.timezone "UTC" ${env_type} ${site_or_ver}
          if [[ ! -z ${site_to_configure_user_ini} ]]; then
            gridpane::callback::app \
              "/site/site-update" \
              "--" \
              "site_url=${site_or_ver}" \
              "server_ip=${serverIP}" \
              "-s" "date.timezone=NULL" \
              "silent@false"
            gridpane::notify::app \
              "PHP INI Notice" \
              "${site_or_ver}<br>${serverIP}<br>date.timezone: UTC (NULL)" \
              "popup_and_center" \
              "fa-cog" \
              "default" \
              "${site_or_ver}"
            logIt gridpane-general configure-php Continue ${site_or_ver}
          fi
        else
          if [[ ${check_for_parameter} == *"date.timezone ="* ]]; then
            sed -i "/date.timezone =/c\date.timezone = ${parameter_value}" ${ini_file}
          else
            echo "date.timezone = ${parameter_value}" >>${ini_file}
          fi
          echo "date.timezone = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          gridpane::conf_write date.timezone "${parameter_value}" ${env_type} ${site_or_ver}
          if [[ ! -z ${site_to_configure_user_ini} ]]; then
            gridpane::callback::app \
              "/site/site-update" \
              "--" \
              "site_url=${site_or_ver}" \
              "server_ip=${serverIP}" \
              "-s" "date.timezone=${parameter_value}" \
              "silent@false"
            gridpane::notify::app \
              "PHP INI Notice" \
              "${site_or_ver}<br>${serverIP}<br>date.timezone: ${parameter_value}" \
              "popup_and_center" \
              "fa-cog" \
              "default" \
              "${site_or_ver}"
            logIt gridpane-general configure-php Continue ${site_or_ver}
          fi
        fi
      fi
    fi
  elif [[ ${configure_command} == "date-timezone-reset" ]] || [[ ${configure_command} == "-date-timezone-reset" ]] ||
    [[ ${configure_command} == "site-date-timezone-reset" ]] || [[ ${configure_command} == "-site-date-timezone-reset" ]]; then
    if [[ ${check_for_parameter} == *"date.timezone ="* ]]; then
      sed -i "/date.timezone =/c\;date.timezone =" ${ini_file}
    else
      echo ";date.timezone =" >>${ini_file}
    fi
    echo ";date.timezone =" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    gridpane::conf_write date.timezone "UTC" ${env_type} ${site_or_ver}
    if [[ ! -z ${site_to_configure_user_ini} ]]; then
      gridpane::callback::app \
        "/site/site-update" \
        "--" \
        "site_url=${site_or_ver}" \
        "server_ip=${serverIP}" \
        "-s" "date.timezone=NULL" \
        "silent@false"
      gridpane::notify::app \
        "PHP INI Notice" \
        "${site_or_ver}<br>${serverIP}<br>date.timezone: UTC (NULL)" \
        "popup_and_center" \
        "fa-cog" \
        "default" \
        "${site_or_ver}"
      logIt gridpane-general configure-php Continue ${site_or_ver}
    fi
  elif [[ ${configure_command} == "short-open-tag" ]] || [[ ${configure_command} == "-short-open-tag" ]] ||
    [[ ${configure_command} == "site-short-open-tag" ]] || [[ ${configure_command} == "-site-short-open-tag" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires on/On/ON, off/Off/OFF command to be send, nothing sent..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ${parameter_value} != "On" ]] && [[ ${parameter_value} != "on" ]] &&
        [[ ${parameter_value} != "Off" ]] && [[ ${parameter_value} != "off" ]] &&
        [[ ${parameter_value} != "ON" ]] && [[ ${parameter_value} != "OFF" ]]; then
        echo "${configure_command} requires on/On/ON, off/Off/OFF command to be send not ${parameter_value}..."
        [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
      else
        if [[ ${parameter_value} == "on" ]] || [[ ${parameter_value} == "ON" ]]; then
          parameter_value="On"
          callback_value="true"
        elif [[ ${parameter_value} == "off" ]] || [[ ${parameter_value} == "OFF" ]]; then
          parameter_value="Off"
          callback_value="false"
        fi
        if [[ ${check_for_parameter} == *"short_open_tag ="* ]]; then
          sed -i "/short_open_tag =/c\short_open_tag = ${parameter_value}" ${ini_file}
        else
          echo "short_open_tag = ${parameter_value}" >>${ini_file}
        fi
        echo "short_open_tag = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        gridpane::conf_write short_open_tag ${parameter_value} ${env_type} ${site_or_ver}
        if [[ ! -z ${site_to_configure_user_ini} ]]; then
          gridpane::callback::app \
            "/site/site-update" \
            "--" \
            "site_url=${site_or_ver}" \
            "server_ip=${serverIP}" \
            "short_open_tag@${callback_value}" \
            "silent@false"
          gridpane::notify::app \
            "PHP INI Notice" \
            "${site_or_ver}<br>${serverIP}<br>short_open_tag: ${callback_value}" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
          logIt gridpane-general configure-php Continue ${site_or_ver}
        fi
      fi
    fi
  elif [[ ${configure_command} == "short-open-tag-reset" ]] || [[ ${configure_command} == "-short-open-tag-reset" ]] ||
    [[ ${configure_command} == "site-short-open-tag-reset" ]] || [[ ${configure_command} == "-site-short-open-tag-reset" ]]; then
    if [[ ${check_for_parameter} == *"short_open_tag ="* ]]; then
      sed -i "/short_open_tag =/c\short_open_tag = Off" ${ini_file}
    else
      echo "short_open_tag = Off" >>${ini_file}
    fi
    echo "short_open_tag = Off" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    gridpane::conf_write short_open_tag off ${env_type} ${site_or_ver}
    if [[ ! -z ${site_to_configure_user_ini} ]]; then
      gridpane::callback::app \
        "/site/site-update" \
        "--" \
        "site_url=${site_or_ver}" \
        "server_ip=${serverIP}" \
        "short_open_tag@false" \
      gridpane::notify::app \
        "PHP INI Notice" \
        "${site_or_ver}<br>${serverIP}<br>short_open_tag: Off" \
        "popup_and_center" \
        "fa-cog" \
        "default" \
        "${site_or_ver}"
        "silent@false"
      logIt gridpane-general configure-php Continue ${site_or_ver}
    fi
  elif [[ ${configure_command} == "default-socket-timeout" ]] || [[ ${configure_command} == "-default-socket-timeout" ]] ||
    [[ ${configure_command} == "site-default-socket-timeout" ]] || [[ ${configure_command} == "-site-default-socket-timeout" ]]; then
    if [[ ! "${parameter_value}" =~ ^(-)?[0-9]+$ ]]; then
      echo "${configure_command} only accepts Integers, we automatically set this to Seconds - ${parameter_value} cannot be processed, exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ${check_for_parameter} == *"default_socket_timeout ="* ]]; then
        sed -i "/default_socket_timeout =/c\default_socket_timeout = ${parameter_value}" ${ini_file}
      else
        echo "default_socket_timeout = ${parameter_value}" >>${ini_file}
      fi
      echo "default_socket_timeout = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      gridpane::conf_write default_socket_timeout ${parameter_value} ${env_type} ${site_or_ver}
      if [[ ! -z ${site_to_configure_user_ini} ]]; then
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "default_socket_timeout=${parameter_value}" \
          "silent@false"
        gridpane::notify::app \
          "PHP INI Notice" \
          "${site_or_ver}<br>${serverIP}<br>default_socket_timeout: ${parameter_value} Sec" \
          "popup_and_center" \
          "fa-cog" \
          "default" \
          "${site_or_ver}"
        logIt gridpane-general configure-php Continue ${site_or_ver}
      fi
    fi
  elif [[ ${configure_command} == "default-socket-timeout-reset" ]] || [[ ${configure_command} == "-default-socket-timeout-reset" ]] ||
    [[ ${configure_command} == "site-default-socket-timeout-reset" ]] || [[ ${configure_command} == "-site-default-socket-timeout-reset" ]]; then
    if [[ ${check_for_parameter} == *"default_socket_timeout ="* ]]; then
      sed -i "/default_socket_timeout =/c\default_socket_timeout = 60" ${ini_file}
    else
      echo "default_socket_timeout = 60" >>${ini_file}
    fi
    echo "default_socket_timeout = 60" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    gridpane::conf_write default_socket_timeout 60 ${env_type} ${site_or_ver}
    if [[ ! -z ${site_to_configure_user_ini} ]]; then
      gridpane::callback::app \
        "/site/site-update" \
        "--" \
        "site_url=${site_or_ver}" \
        "server_ip=${serverIP}" \
        "-s" "default_socket_timeout=60" \
        "silent@false"
      gridpane::notify::app \
        "PHP INI Notice" \
        "${site_or_ver}<br>${serverIP}<br>default_socket_timeout: 60 Sec" \
        "popup_and_center" \
        "fa-cog" \
        "default" \
        "${site_or_ver}"
      logIt gridpane-general configure-php Continue ${site_or_ver}
    fi
  elif [[ ${configure_command} == "session-cookie-lifetime" ]] || [[ ${configure_command} == "-session-cookie-lifetime" ]] ||
    [[ ${configure_command} == "site-session-cookie-lifetime" ]] || [[ ${configure_command} == "-site-session-cookie-lifetime" ]]; then
    if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
      echo "${configure_command} only accepts Integers, we automatically set this to Seconds - ${parameter_value} cannot be processed, exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ${check_for_parameter} == *"session.cookie_lifetime ="* ]]; then
        sed -i "/session.cookie_lifetime =/c\session.cookie_lifetime = ${parameter_value}" ${ini_file}
      else
        echo "session.cookie_lifetime = ${parameter_value}" >>${ini_file}
      fi
      echo "session.cookie_lifetime = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      gridpane::conf_write session.cookie_lifetime ${parameter_value} ${env_type} ${site_or_ver}
      if [[ ! -z ${site_to_configure_user_ini} ]]; then
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "session.cookie_lifetime=${parameter_value}" \
          "silent@false"
        gridpane::notify::app \
          "PHP INI Notice" \
          "${site_or_ver}<br>${serverIP}<br>session.cookie_lifetime: ${parameter_value} Sec" \
          "popup_and_center" \
          "fa-cog" \
          "default" \
          "${site_or_ver}"
        logIt gridpane-general configure-php Continue ${site_or_ver}
      fi
    fi
  elif [[ ${configure_command} == "session-cookie-lifetime-reset" ]] || [[ ${configure_command} == "-session-cookie-lifetime-reset" ]] ||
    [[ ${configure_command} == "site-session-cookie-lifetime-reset" ]] || [[ ${configure_command} == "-site-session-cookie-lifetime-reset" ]]; then
    if [[ ${check_for_parameter} == *"session.cookie_lifetime ="* ]]; then
      sed -i "/session.cookie_lifetime =/c\session.cookie_lifetime = 0" ${ini_file}
    else
      echo "session.cookie_lifetime = 0" >>${ini_file}
    fi
    echo "session.cookie_lifetime = 0" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    gridpane::conf_write session.cookie_lifetime 0 ${env_type} ${site_or_ver}
    if [[ ! -z ${site_to_configure_user_ini} ]]; then
      gridpane::callback::app \
        "/site/site-update" \
        "--" \
        "site_url=${site_or_ver}" \
        "server_ip=${serverIP}" \
        "-s" "session.cookie_lifetime=0" \
        "silent@false"
      gridpane::notify::app \
        "PHP INI Notice" \
        "${site_or_ver}<br>${serverIP}<br>session.cookie_lifetime: 0 <br>(Until browser closes)" \
        "popup_and_center" \
        "fa-cog" \
        "default" \
        "${site_or_ver}"
      logIt gridpane-general configure-php Continue ${site_or_ver}
    fi
  elif [[ ${configure_command} == "session-gc-maxlifetime" ]] || [[ ${configure_command} == "-session-gc-maxlifetime" ]] ||
    [[ ${configure_command} == "site-session-gc-maxlifetime" ]] || [[ ${configure_command} == "-site-session-gc-maxlifetime" ]]; then
    if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
      echo "${configure_command} only accepts Integers, we automatically set this to Seconds - ${parameter_value} cannot be processed, exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ${check_for_parameter} == *"session.gc_maxlifetime ="* ]]; then
        sed -i "/session.gc_maxlifetime =/c\session.gc_maxlifetime = ${parameter_value}" ${ini_file}
      else
        echo "session.gc_maxlifetime = ${parameter_value}" >>${ini_file}
      fi
      echo "session.gc_maxlifetime = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      gridpane::conf_write session.gc_maxlifetime ${parameter_value} ${env_type} ${site_or_ver}
      if [[ ! -z ${site_to_configure_user_ini} ]]; then
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "session.gc_maxlifetime=${parameter_value}" \
          "silent@false"
        gridpane::notify::app \
          "PHP INI Notice" \
          "${site_or_ver}<br>${serverIP}<br>session.gc_maxlifetime: ${parameter_value} Sec" \
          "popup_and_center" \
          "fa-cog" \
          "default" \
          "${site_or_ver}"
        logIt gridpane-general configure-php Continue ${site_or_ver}
      fi
    fi
  elif [[ ${configure_command} == "session-gc-maxlifetime-reset" ]] || [[ ${configure_command} == "-session-gc-maxlifetime-reset" ]] ||
    [[ ${configure_command} == "site-session-gc-maxlifetime-reset" ]] || [[ ${configure_command} == "-site-session-gc-maxlifetime-reset" ]]; then
    if [[ ${check_for_parameter} == *"session.gc_maxlifetime ="* ]]; then
      sed -i "/session.gc_maxlifetime =/c\session.gc_maxlifetime = 1440" ${ini_file}
    else
      echo "session.gc_maxlifetime = 1440" >>${ini_file}
    fi
    echo "session.gc_maxlifetime = 1440" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    gridpane::conf_write session.gc_maxlifetime 1440 ${env_type} ${site_or_ver}
    if [[ ! -z ${site_to_configure_user_ini} ]]; then
      gridpane::callback::app \
        "/site/site-update" \
        "--" \
        "site_url=${site_or_ver}" \
        "server_ip=${serverIP}" \
        "-s" "session.gc_maxlifetime=1440" \
        "silent@false"
      gridpane::notify::app \
        "PHP INI Notice" \
        "${site_or_ver}<br>${serverIP}<br>session.gc_maxlifetime: 1440 Sec" \
        "popup_and_center" \
        "fa-cog" \
        "default" \
        "${site_or_ver}"
      logIt gridpane-general configure-php Continue ${site_or_ver}
    fi
  elif [[ ${configure_command} == "allow-url-include" ]] || [[ ${configure_command} == "-allow-url-include" ]] ||
    [[ ${configure_command} == "site-allow-url-include" ]] || [[ ${configure_command} == "-site-allow-url-include" ]]; then
    if [[ ${configure_command} == *"site-allow-url-include"* ]]; then
      echo "allow_url_include can only be set at a PHP.ini level, exiting...."
    else
      if [[ -z "${parameter_value}" ]]; then
        echo "${configure_command} requires a recognised value to be passed, either a Boolean, ON/On/on, or OFF/Off/off"
        [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
      else
        if [[ ${parameter_value} != "ON" ]] || [[ ${parameter_value} != "On" ]] ||
          [[ ${parameter_value} != "OFF" ]] || [[ ${parameter_value} != "off" ]] ||
          [[ ${parameter_value} != "On" ]] || [[ ${parameter_value} != "Off" ]] ||
          [[ ${parameter_value} != "1" ]] || [[ ${parameter_value} != "0" ]] ||
          [[ ${parameter_value} != *"rue" ]] || [[ ${parameter_value} != *"alse" ]]; then
          echo "${configure_command} requires a recognised value to be passed, either a Boolean, ON/On/on, or OFF/Off/off"
        else
          if [[ ${parameter_value} == "ON" ]] || [[ ${parameter_value} == "on" ]] || [[ ${parameter_value} == "On" ]] ||
            [[ ${parameter_value} == "1" ]] || [[ ${parameter_value} == *"rue" ]]; then
            parameter_value="on"
          fi
          if [[ ${parameter_value} == "OFF" ]] || [[ ${parameter_value} == "off" ]] || [[ ${parameter_value} == "Off" ]] ||
            [[ ${parameter_value} == "0" ]] || [[ ${parameter_value} == *"alse" ]]; then
            parameter_value="off"
          fi
        fi
      fi
      if [[ ${check_for_parameter} == *"allow_url_include ="* ]]; then
        sed -i "/allow_url_include =/c\allow_url_include = ${parameter_value}" ${ini_file}
      else
        echo "allow_url_include = ${parameter_value}" >>${ini_file}
      fi
      echo "allow_url_include = ${parameter_value}"
      echo "${ini_file}"
      gridpane::conf_write allow_url_include ${parameter_value} ${env_type} ${site_or_ver}
      #TODO (JEFF) Callback to APP - site|server table
    fi
  elif [[ ${configure_command} == "allow-url-include-reset" ]] || [[ ${configure_command} == "-allow-url-include-reset" ]] ||
    [[ ${configure_command} == "site-allow-url-include-reset" ]] || [[ ${configure_command} == "-site-allow-url-include-reset" ]]; then
    if [[ ${configure_command} == *"site-allow-url-include"* ]]; then
      echo "allow_url_include can only be set at a PHP.ini level, exiting...."
    else
      if [[ ${check_for_parameter} == *"allow_url_include ="* ]]; then
        sed -i "/allow_url_include =/c\allow_url_include = off" ${ini_file}
      else
        echo "allow_url_include = off" >>${ini_file}
      fi
      echo "allow_url_include = off"
      echo "${ini_file}"
      gridpane::conf_write allow_url_include Off ${env_type} ${site_or_ver}
      #TODO (JEFF) Callback to APP - site|server table
    fi
  elif [[ ${configure_command} == "allow-url-fopen" ]] || [[ ${configure_command} == "-allow-url-fopen" ]] ||
    [[ ${configure_command} == "site-allow-url-fopen" ]] || [[ ${configure_command} == "-site-allow-url-fopen" ]]; then
    if [[ ${configure_command} == *"site-allow-url-fopen"* ]]; then
      echo "allow_url_fopen can only be set at a PHP.ini level, exiting...."
    else
      if [[ -z "${parameter_value}" ]]; then
        echo "${configure_command} requires a recognised value to be passed, either a Boolean, ON/On/on, or OFF/Off/off"
      else
        if [[ ${parameter_value} != "ON" ]] || [[ ${parameter_value} != "On" ]] ||
          [[ ${parameter_value} != "OFF" ]] || [[ ${parameter_value} != "off" ]] ||
          [[ ${parameter_value} != "On" ]] || [[ ${parameter_value} != "Off" ]] ||
          [[ ${parameter_value} != "1" ]] || [[ ${parameter_value} != "0" ]] ||
          [[ ${parameter_value} != *"rue" ]] || [[ ${parameter_value} != *"alse" ]]; then
          echo "${configure_command} requires a recognised value to be passed, either a Boolean, ON/On/on, or OFF/Off/off"
        else
          if [[ ${parameter_value} == "ON" ]] || [[ ${parameter_value} == "on" ]] || [[ ${parameter_value} == "On" ]] ||
            [[ ${parameter_value} == "1" ]] || [[ ${parameter_value} == *"rue" ]]; then
            parameter_value="on"
          fi
          if [[ ${parameter_value} == "OFF" ]] || [[ ${parameter_value} == "off" ]] || [[ ${parameter_value} == "Off" ]] ||
            [[ ${parameter_value} == "0" ]] || [[ ${parameter_value} == *"alse" ]]; then
            parameter_value="off"
          fi
        fi
      fi
      if [[ ${check_for_parameter} == *"allow_url_fopen ="* ]]; then
        sed -i "/allow_url_fopen =/c\allow_url_fopen = ${parameter_value}" ${ini_file}
      else
        echo "allow_url_fopen = ${parameter_value}" >>${ini_file}
      fi
      echo "allow_url_fopen = ${parameter_value}"
      echo "${ini_file}"
      gridpane::conf_write allow_url_fopen ${parameter_value} ${env_type} ${site_or_ver}
      #TODO (JEFF) Callback to APP - site|server table
    fi
  elif [[ ${configure_command} == "allow-url-fopen-reset" ]] || [[ ${configure_command} == "-allow-url-fopen-reset" ]] ||
    [[ ${configure_command} == "site-allow-url-fopen-reset" ]] || [[ ${configure_command} == "-site-allow-url-fopen-reset" ]]; then
    if [[ ${configure_command} == *"site-allow-url-fopen"* ]]; then
      echo "allow_url_fopen can only be set at a PHP.ini level, exiting...."
    else
      if [[ ${check_for_parameter} == *"allow_url_fopen ="* ]]; then
        sed -i "/allow_url_fopen =/c\allow_url_fopen = on" ${ini_file}
      else
        echo "allow_url_fopen = on" >>${ini_file}
      fi
      echo "allow_url_fopen = on"
      echo "${ini_file}"
      gridpane::conf_write allow_url_fopen on ${env_type} ${site_or_ver}
      #TODO (JEFF) Callback to APP - site|server table
    fi
  elif [[ ${configure_command} == "disable-functions" ]] || [[ ${configure_command} == "-disable-functions" ]] ||
    [[ ${configure_command} == "site-disable-functions" ]] || [[ ${configure_command} == "-site-disable-functions" ]]; then
    if [[ ${configure_command} == *"site-disable-functions"* ]]; then
      echo "disable_functions can only be set at a PHP.ini level, exiting...."
    else
      if [[ -z "${parameter_value}" ]]; then
        echo "${configure_command} requires a comma separated list of functions to be passed..."
      fi
      default_disabled_functions="pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited,pcntl_wifstopped,pcntl_wifsignaled,pcntl_wifcontinued,pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal,pcntl_signal_get_handler,pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror,pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec,pcntl_getpriority,pcntl_setpriority,pcntl_async_signals,"
      deduplicated_parameter_value=","
      IFS=","
      for parameter in ${parameter_value}; do
        if [[ ${default_disabled_functions} == *"${parameter}"* ]]; then
          continue
        fi
        deduplicated_parameter_value="${deduplicated_parameter_value},${parameter}"
      done
      if [[ ${deduplicated_parameter_value} == ","* ]]; then
        deduplicated_parameter_value=${deduplicated_parameter_value#,}
      fi
      if [[ ${deduplicated_parameter_value} != *"," ]]; then
        deduplicated_parameter_value="${deduplicated_parameter_value},"
      fi
      parameter_value_to_use="${default_disabled_functions}${deduplicated_parameter_value}"
      if [[ ${check_for_parameter} == *"disable_functions ="* ]]; then
        sed -i "/disable_functions =/c\disable_functions = ${parameter_value_to_use}" ${ini_file}
      else
        echo "disable_functions = ${parameter_value_to_use}" >>${ini_file}
      fi
      echo "disable_functions = ${parameter_value_to_use}"
      echo "${ini_file}"
      gridpane::conf_write disable_functions ${parameter_value_to_use} ${env_type} ${site_or_ver}
      #TODO (JEFF) Callback to APP - site|server table
    fi
  elif [[ ${configure_command} == "disable-functions-reset" ]] || [[ ${configure_command} == "-disable-functions-reset" ]] ||
    [[ ${configure_command} == "site-disable-functions-reset" ]] || [[ ${configure_command} == "-site-disable-functions-reset" ]]; then
    if [[ ${configure_command} == *"site-disable-functions"* ]]; then
      echo "disable_functions can only be set at a PHP.ini level, exiting...."
    else
      default_disabled_functions="pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited,pcntl_wifstopped,pcntl_wifsignaled,pcntl_wifcontinued,pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal,pcntl_signal_get_handler,pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror,pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec,pcntl_getpriority,pcntl_setpriority,pcntl_async_signals,"
      if [[ ${check_for_parameter} == *"disable_functions ="* ]]; then
        sed -i "/disable_functions =/c\disable_functions = ${default_disabled_functions}" ${ini_file}
      else
        echo "disable_functions = ${default_disabled_functions}" >>${ini_file}
      fi
      echo "disable_functions = ${default_disabled_functions}"
      echo "${ini_file}"
      gridpane::conf_write disable_functions ${default_disabled_functions} ${env_type} ${site_or_ver}
      #TODO (JEFF) Callback to APP - site|server table
    fi
  elif [[ ${configure_command} == "expose-php" ]] || [[ ${configure_command} == "-expose-php" ]] ||
    [[ ${configure_command} == "site-expose-php" ]] || [[ ${configure_command} == "-site-expose-php" ]]; then
    if [[ ${configure_command} == *"site-expose-php"* ]]; then
      echo "expose_php can only be set at a PHP.ini level, exiting...."
    else
      if [[ -z "${parameter_value}" ]]; then
        echo "${configure_command} requires a recognised value to be passed, either ON/on or OFF/off"
      else
        if [[ ${parameter_value} != "ON" ]] || [[ ${parameter_value} != "On" ]] ||
          [[ ${parameter_value} != "OFF" ]] || [[ ${parameter_value} != "off" ]] ||
          [[ ${parameter_value} != "On" ]] || [[ ${parameter_value} != "Off" ]]; then
          echo "${configure_command} requires a recognised value to be passed, either ON/on or OFF/off"
        else
          if [[ ${parameter_value} == "ON" ]] || [[ ${parameter_value} == "on" ]]; then
            parameter_value="On"
          fi
          if [[ ${parameter_value} == "OFF" ]] || [[ ${parameter_value} == "off" ]]; then
            parameter_value="Off"
          fi
        fi
      fi
      if [[ ${check_for_parameter} == *"expose_php ="* ]]; then
        sed -i "/expose_php =/c\expose_php = ${parameter_value}" ${ini_file}
      else
        echo "expose_php = ${parameter_value}" >>${ini_file}
      fi
      echo "expose_php = ${parameter_value}"
      echo "${ini_file}"
      gridpane::conf_write expose_php ${parameter_value} ${env_type} ${site_or_ver}
      #TODO (JEFF) Callback to APP - site|server table
    fi
  elif [[ ${configure_command} == "expose-php-reset" ]] || [[ ${configure_command} == "-expose-php-reset" ]] ||
    [[ ${configure_command} == "site-expose-php-reset" ]] || [[ ${configure_command} == "-site-expose-php-reset" ]]; then
    if [[ ${configure_command} == *"site-expose-php"* ]]; then
      echo "expose_php can only be set at a PHP.ini level, exiting...."
    else
      if [[ ${check_for_parameter} == *"expose_php ="* ]]; then
        sed -i "/expose_php =/c\expose_php = Off" ${ini_file}
      else
        echo "expose_php = Off" >>${ini_file}
      fi
      echo "expose_php = Off"
      echo "${ini_file}"
      gridpane::conf_write expose_php Off ${env_type} ${site_or_ver}
      #TODO (JEFF) Callback to APP - site|server table
    fi
  elif [[ ${configure_command} == "mem-limit" ]] || [[ ${configure_command} == "-mem-limit" ]] ||
    [[ ${configure_command} == "site-mem-limit" ]] || [[ ${configure_command} == "-site-mem-limit" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting value to be passed, none was... exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers, we automatically set this to Megabytes - ${parameter_value} cannot be processed, exiting..."
        [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
      else
        if [[ ${check_for_parameter} == *"memory_limit ="* ]]; then
          sed -i "/memory_limit =/c\memory_limit = ${parameter_value}M" ${ini_file}
        else
          echo "memory_limit = ${parameter_value}M" >>${ini_file}
        fi
        echo "memory_limit = ${parameter_value}M" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        gridpane::conf_write memory_limit ${parameter_value}M ${env_type} ${site_or_ver}
        if [[ ! -z ${site_to_configure_user_ini} ]]; then
          gridpane::callback::app \
            "/site/site-update" \
            "--" \
            "site_url=${site_or_ver}" \
            "server_ip=${serverIP}" \
            "-s" "memory_limit=${parameter_value}" \
            "silent@false"
          gridpane::notify::app \
            "PHP INI Notice" \
            "${site_or_ver}<br>${serverIP}<br>memory_limit: ${parameter_value}M" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
          logIt gridpane-general configure-php Continue ${site_or_ver}
        fi
      fi
    fi
  elif [[ ${configure_command} == "mem-limit-reset" ]] || [[ ${configure_command} == "-mem-limit-reset" ]] ||
    [[ ${configure_command} == "site-mem-limit-reset" ]] || [[ ${configure_command} == "-site-mem-limit-reset" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} resets back to GridPane default - ${parameter_value} will not be processed, use -mem-limit/-site-mem-limit instead, exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ${check_for_parameter} == *"memory_limit ="* ]]; then
        sed -i "/memory_limit =/c\memory_limit = 256M" ${ini_file}
      else
        echo "memory_limit = 256M" >>${ini_file}
      fi
      echo "memory_limit = 256M | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      gridpane::conf_write memory_limit 256M ${env_type} ${site_or_ver}
      if [[ ! -z ${site_to_configure_user_ini} ]]; then
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "memory_limit=256" \
          "silent@false"
        gridpane::notify::app \
          "PHP INI Notice" \
          "${site_or_ver}<br>${serverIP}<br>memory_limit: 256M" \
          "popup_and_center" \
          "fa-cog" \
          "default" \
          "${site_or_ver}"
        logIt gridpane-general configure-php Continue ${site_or_ver}
      fi
    fi
  elif [[ ${configure_command} == "max-exec-time" ]] || [[ ${configure_command} == "-max-exec-time" ]] ||
    [[ ${configure_command} == "site-max-exec-time" ]] || [[ ${configure_command} == "-site-max-exec-time" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting value to be passed, none was... exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers, we automatically set this to Seconds - ${parameter_value} cannot be processed, exiting..."
        [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
      else
        if [[ ${check_for_parameter} == *"max_execution_time ="* ]]; then
          sed -i "/max_execution_time =/c\max_execution_time = ${parameter_value}" ${ini_file}
        else
          echo "max_execution_time = ${parameter_value}" >>${ini_file}
        fi
        echo "max_execution_time = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        gridpane::conf_write max_execution_time ${parameter_value} ${env_type} ${site_or_ver}
        if [[ ! -z ${site_to_configure_user_ini} ]]; then
          gridpane::callback::app \
            "/site/site-update" \
            "--" \
            "site_url=${site_or_ver}" \
            "server_ip=${serverIP}" \
            "-s" "max_execution_time=${parameter_value}" \
            "silent@false"
          gridpane::notify::app \
            "PHP INI Notice" \
            "${site_or_ver}<br>${serverIP}<br>max_execution_time: ${parameter_value} Sec" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
          logIt gridpane-general configure-php Continue ${site_or_ver}
        fi
      fi
    fi
  elif [[ ${configure_command} == "max-exec-time-reset" ]] || [[ ${configure_command} == "-max-exec-time-reset" ]] ||
    [[ ${configure_command} == "site-max-exec-time-reset" ]] || [[ ${configure_command} == "-site-max-exec-time-reset" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} resets back to GridPane default - ${parameter_value} will not be processed, use -max-exec-time/-site-max-exec-time instead, exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ${check_for_parameter} == *"max_execution_time ="* ]]; then
        sed -i "/max_execution_time =/c\max_execution_time = 300" ${ini_file}
      else
        echo "max_execution_time = 300" >>${ini_file}
      fi
      echo "max_execution_time = 300 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      gridpane::conf_write max_execution_time 300 ${env_type} ${site_or_ver}
      if [[ ! -z ${site_to_configure_user_ini} ]]; then
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "max_execution_time=300" \
          "silent@false"
        gridpane::notify::app \
          "PHP INI Notice" \
          "${site_or_ver}<br>${serverIP}<br>max_execution_time: 300 Sec" \
          "popup_and_center" \
          "fa-cog" \
          "default" \
          "${site_or_ver}"
        logIt gridpane-general configure-php Continue ${site_or_ver}
      fi
    fi
  elif [[ ${configure_command} == "max-input-vars" ]] || [[ ${configure_command} == "-max-input-vars" ]] ||
    [[ ${configure_command} == "site-max-input-vars" ]] || [[ ${configure_command} == "-site-max-input-vars" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting value to be passed, none was... exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers - ${parameter_value} cannot be processed, exiting..."
        [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
      else
        if [[ ${check_for_parameter} == *"max_input_vars ="* ]]; then
          sed -i "/max_input_vars =/c\max_input_vars = ${parameter_value}" ${ini_file}
        else
          echo "max_input_vars = ${parameter_value}" >>${ini_file}
        fi
        echo "max_input_vars = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        gridpane::conf_write max_input_vars ${parameter_value} ${env_type} ${site_or_ver}
        if [[ ! -z ${site_to_configure_user_ini} ]]; then
          gridpane::callback::app \
            "/site/site-update" \
            "--" \
            "site_url=${site_or_ver}" \
            "server_ip=${serverIP}" \
            "-s" "max_input_vars=${parameter_value}" \
            "silent@false"
          gridpane::notify::app \
            "PHP INI Notice" \
            "${site_or_ver}<br>${serverIP}<br>max_input_vars: ${parameter_value}" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
          logIt gridpane-general configure-php Continue ${site_or_ver}
        fi
      fi
    fi
  elif [[ ${configure_command} == "max-input-vars-reset" ]] || [[ ${configure_command} == "-max-input-vars-reset" ]] ||
    [[ ${configure_command} == "site-input-vars-reset" ]] || [[ ${configure_command} == "-site-max-input-vars-reset" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} resets back to GridPane default - ${parameter_value} will not be processed, use -max-input-vars/-site-max-input-vars instead, exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ${check_for_parameter} == *"max_input_vars ="* ]]; then
        sed -i "/max_input_vars =/c\max_input_vars = 5000" ${ini_file}
      else
        echo "max_input_vars = 5000" >>${ini_file}
      fi
      echo "max_input_vars = 5000 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      gridpane::conf_write max_input_vars 5000 ${site_or_ver}
      if [[ ! -z ${site_to_configure_user_ini} ]]; then
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "max_input_vars=5000" \
          "silent@false"
        gridpane::notify::app \
          "PHP INI Notice" \
          "${site_or_ver}<br>${serverIP}<br>max_input_vars: 5000" \
          "popup_and_center" \
          "fa-cog" \
          "default" \
          "${site_or_ver}"
        logIt gridpane-general configure-php Continue ${site_or_ver}
      fi
    fi
  elif [[ ${configure_command} == "max-input-time" ]] || [[ ${configure_command} == "-max-input-time" ]] ||
    [[ ${configure_command} == "site-max-input-time" ]] || [[ ${configure_command} == "-site-max-input-time" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting value to be passed, none was... exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ! "${parameter_value}" =~ ^(-)?[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers, we automatically set this to Seconds - ${parameter_value} cannot be processed, exiting..."
        [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
      else
        if [[ ${check_for_parameter} == *"max_input_time ="* ]]; then
          sed -i "/max_input_time =/c\max_input_time = ${parameter_value}" ${ini_file}
        else
          echo "max_input_time = ${parameter_value}" >>${ini_file}
        fi
        echo "max_input_time = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        gridpane::conf_write max_input_time ${parameter_value} ${env_type} ${site_or_ver}
        if [[ ! -z ${site_to_configure_user_ini} ]]; then
          gridpane::callback::app \
            "/site/site-update" \
            "--" \
            "site_url=${site_or_ver}" \
            "server_ip=${serverIP}" \
            "-s" "max_input_time=${parameter_value}" \
            "silent@false"
          gridpane::notify::app \
            "PHP INI Notice" \
            "${site_or_ver}<br>${serverIP}<br>max_input_time: ${parameter_value} Sec" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
          logIt gridpane-general configure-php Continue ${site_or_ver}
        fi
      fi
    fi
  elif [[ ${configure_command} == "max-input-time-reset" ]] || [[ ${configure_command} == "-max-input-time-reset" ]] ||
    [[ ${configure_command} == "site-max-input-time-reset" ]] || [[ ${configure_command} == "-site-max-input-time-reset" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} resets back to GridPane default - ${parameter_value} will not be processed, use -max-input-time/-site-max-input-time instead, exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ${check_for_parameter} == *"max_input_time ="* ]]; then
        sed -i "/max_input_time =/c\max_input_time = 60" ${ini_file}
      else
        echo "max_input_time = 60" >>${ini_file}
      fi
      echo "max_input_time = 60 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      gridpane::conf_write max_input_time 60 ${env_type} ${site_or_ver}
      if [[ ! -z ${site_to_configure_user_ini} ]]; then
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "max_input_time=60" \
          "silent@false"
        gridpane::notify::app \
          "PHP INI Notice" \
          "${site_or_ver}<br>${serverIP}<br>max_input_time: 60 Sec" \
          "popup_and_center" \
          "fa-cog" \
          "default" \
          "${site_or_ver}"
        logIt gridpane-general configure-php Continue ${site_or_ver}
      fi
    fi
  elif [[ ${configure_command} == "upload-max-filesize" ]] || [[ ${configure_command} == "-upload-max-filesize" ]] ||
    [[ ${configure_command} == "site-upload-max-filesize" ]] || [[ ${configure_command} == "-site-upload-max-filesize" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting value to be passed, none was... exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers, we automatically set this to Megabytes - ${parameter_value} cannot be processed, exiting..."
        [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
      else
        if [[ ${check_for_parameter} == *"upload_max_filesize ="* ]]; then
          sed -i "/upload_max_filesize =/c\upload_max_filesize = ${parameter_value}M" ${ini_file}
        else
          echo "upload_max_filesize = ${parameter_value}M" >>${ini_file}
        fi
        echo "upload_max_filesize = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        gridpane::conf_write upload_max_filesize ${parameter_value} ${env_type} ${site_or_ver}
        if [[ ! -z ${site_to_configure_user_ini} ]]; then
          gridpane::callback::app \
            "/site/site-update" \
            "--" \
            "site_url=${site_or_ver}" \
            "server_ip=${serverIP}" \
            "-s" "upload_max_filesize=${parameter_value}" \
            "silent@false"
          gridpane::notify::app \
            "PHP INI Notice" \
            "${site_or_ver}<br>${serverIP}<br>upload_max_filesize: ${parameter_value}M" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
          logIt gridpane-general configure-php Continue ${site_or_ver}
        fi
      fi
    fi
  elif [[ ${configure_command} == "upload-max-filesize-reset" ]] || [[ ${configure_command} == "-upload-max-filesize-reset" ]] ||
    [[ ${configure_command} == "site-upload-max-filesize-reset" ]] || [[ ${configure_command} == "-site-upload-max-filesize-reset" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} resets back to GridPane default - ${parameter_value} will not be processed, use -upload-max-filesize/-site-upload-max-filesize instead, exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ${check_for_parameter} == *"upload_max_filesize ="* ]]; then
        sed -i "/upload_max_filesize =/c\upload_max_filesize = 512M" ${ini_file}
      else
        echo "upload_max_filesize = 512M" >>${ini_file}
      fi
      echo "upload_max_filesize = 512M | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      gridpane::conf_write upload_max_filesize 512M ${env_type} ${site_or_ver}
      if [[ ! -z ${site_to_configure_user_ini} ]]; then
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "upload_max_filesize=512" \
          "silent@false"
        gridpane::notify::app \
          "PHP INI Notice" \
          "${site_or_ver}<br>${serverIP}<br>upload_max_filesize: 512M" \
          "popup_and_center" \
          "fa-cog" \
          "default" \
          "${site_or_ver}"
        logIt gridpane-general configure-php Continue ${site_or_ver}
      fi
    fi
  elif [[ ${configure_command} == "max-file-uploads" ]] || [[ ${configure_command} == "-max-file-uploads" ]] ||
    [[ ${configure_command} == "site-max-file-uploads" ]] || [[ ${configure_command} == "-site-max-file-uploads" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting value to be passed, none was... exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers - ${parameter_value} cannot be processed, exiting..."
        [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
      else
        if [[ "${parameter_value}" -gt 20 ]]; then
          if [[ "${parameter_value}" -lt 30 ]]; then
            echo "Setting the maximum number of concurrent file upload to ${parameter_value}"
            echo "This is greater than the default 20..."
          else
            if [[ "${parameter_value}" -lt 50 ]]; then
              echo "Setting the maximum number of concurrent file upload to ${parameter_value}, be careful as you go..."
            else
              if [[ "${parameter_value}" -lt 100 ]]; then
                echo "Setting the maximum number of concurrent file upload to to ${parameter_value}, I hope you have the resources available..."
              else
                echo "Setting the maximum number of concurrent file upload to to ${parameter_value}, good luck with that..."
              fi
            fi
          fi
        fi
        if [[ ${check_for_parameter} == *"max_file_uploads ="* ]]; then
          sed -i "/max_file_uploads =/c\max_file_uploads = ${parameter_value}" ${ini_file}
        else
          echo "max_file_uploads = ${parameter_value}" >>${ini_file}
        fi
        echo "max_file_uploads = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        gridpane::conf_write max_file_uploads ${parameter_value} ${env_type} ${site_or_ver}
        if [[ ! -z ${site_to_configure_user_ini} ]]; then
          gridpane::callback::app \
            "/site/site-update" \
            "--" \
            "site_url=${site_or_ver}" \
            "server_ip=${serverIP}" \
            "-s" "max_file_uploads=${parameter_value}" \
            "silent@false"
          gridpane::notify::app \
            "PHP INI Notice" \
            "${site_or_ver}<br>${serverIP}<br>max_file_uploads: ${parameter_value}" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
          logIt gridpane-general configure-php Continue ${site_or_ver}
        fi
      fi
    fi
  elif [[ ${configure_command} == "max-file-uploads-reset" ]] || [[ ${configure_command} == "-max-file-uploads-reset" ]] ||
    [[ ${configure_command} == "site-max-file-uploads-reset" ]] || [[ ${configure_command} == "-site-max-file-uploads-reset" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} resets back to GridPane default - ${parameter_value} will not be processed, use -max-file-uploads/-site-max-file-uploads instead, exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ${check_for_parameter} == *"max_file_uploads ="* ]]; then
        sed -i "/max_file_uploads =/c\max_file_uploads = 20" ${ini_file}
      else
        echo "max_file_uploads = 20" >>${ini_file}
      fi
      echo "max_file_uploads = 20" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      gridpane::conf_write max_file_uploads 20 ${env_type} ${site_or_ver}
      if [[ ! -z ${site_to_configure_user_ini} ]]; then
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "max_file_uploads=20" \
          "silent@false"
        gridpane::notify::app \
          "PHP INI Notice" \
          "${site_or_ver}<br>${serverIP}<br>max_file_uploads: 20" \
          "popup_and_center" \
          "fa-cog" \
          "default" \
          "${site_or_ver}"
        logIt gridpane-general configure-php Continue ${site_or_ver}
      fi
    fi
  elif [[ ${configure_command} == "post-max-size" ]] || [[ ${configure_command} == "-post-max-size" ]] ||
    [[ ${configure_command} == "site-post-max-size" ]] || [[ ${configure_command} == "-site-post-max-size" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting value to be passed, none was... exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers, we automatically set this to Megabytes - ${parameter_value} cannot be processed, exiting..."
        [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
      else
        if [[ ${check_for_parameter} == *"post_max_size ="* ]]; then
          sed -i "/post_max_size =/c\post_max_size = ${parameter_value}M" ${ini_file}
        else
          echo "post_max_size = ${parameter_value}M" >>${ini_file}
        fi
        echo "post_max_size = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        gridpane::conf_write post_max_size ${parameter_value} ${env_type} ${site_or_ver}
        if [[ ! -z ${site_to_configure_user_ini} ]]; then
          gridpane::callback::app \
            "/site/site-update" \
            "--" \
            "site_url=${site_or_ver}" \
            "server_ip=${serverIP}" \
            "-s" "post_max_size=${parameter_value}" \
            "silent@false"
          gridpane::notify::app \
            "PHP INI Notice" \
            "${site_or_ver}<br>${serverIP}<br>post_max_size: ${parameter_value}M" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
          logIt gridpane-general configure-php Continue ${site_or_ver}
        fi
      fi
    fi
  elif [[ ${configure_command} == "post-max-size-reset" ]] || [[ ${configure_command} == "-post-max-size-reset" ]] ||
    [[ ${configure_command} == "site-post-max-size-reset" ]] || [[ ${configure_command} == "-site-post-max-size-reset" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} resets back to GridPane default - ${parameter_value} will not be processed, use -post-max-size/-site-post-max-size instead, exiting..."
      [[ ! -z ${site_to_configure_user_ini} ]] && logIt gridpane-general configure-php Continue ${site_or_ver}
    else
      if [[ ${check_for_parameter} == *"post_max_size ="* ]]; then
        sed -i "/post_max_size =/c\post_max_size = 512M" ${ini_file}
      else
        echo "post_max_size = 512M" >>${ini_file}
      fi
      echo "post_max_size = 512M  | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      gridpane::conf_write post_max_size 512M ${env_type} ${site_or_ver}
      if [[ ! -z ${site_to_configure_user_ini} ]]; then
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "post_max_size=512" \
          "silent@false"
        gridpane::notify::app \
          "PHP INI Notice" \
          "${site_or_ver}<br>${serverIP}<br>post_max_size: 512M" \
          "popup_and_center" \
          "fa-cog" \
          "default" \
          "${site_or_ver}"
        logIt gridpane-general configure-php Continue ${site_or_ver}
      fi
    fi
  elif [[ ${configure_command} == "reset" ]] || [[ ${configure_command} == "-reset" ]] ||
    [[ ${configure_command} == "site-reset" ]] || [[ ${configure_command} == "-site-reset" ]]; then

    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} resets everything back to GridPane default - ${parameter_value} will not be processed"
    fi

    echo ";date.timezone = | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    if [[ ${check_for_parameter} == *"date.timezone ="* ]]; then
      sed -i "/date.timezone =/c\;date.timezone =" ${ini_file}
    else
      echo ";date.timezone =" >>${ini_file}
    fi

    echo "short_open_tag = Off | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    if [[ ${check_for_parameter} == *"short_open_tag ="* ]]; then
      sed -i "/short_open_tag =/c\short_open_tag = Off" ${ini_file}
    else
      echo "short_open_tag = Off" >>${ini_file}
    fi

    echo "default_socket_timeout = 60 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    if [[ ${check_for_parameter} == *"default_socket_timeout ="* ]]; then
      sed -i "/default_socket_timeout =/c\default_socket_timeout = 60" ${ini_file}
    else
      echo "default_socket_timeout = 60" >>${ini_file}
    fi

    echo "session.cookie_lifetime = 0 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    if [[ ${check_for_parameter} == *"session.cookie_lifetime ="* ]]; then
      sed -i "/session.cookie_lifetime =/c\session.cookie_lifetime = 0" ${ini_file}
    else
      echo "session.cookie_lifetime = 0" >>${ini_file}
    fi

    echo "session.gc_maxlifetime = 1440 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    if [[ ${check_for_parameter} == *"session.gc_maxlifetime ="* ]]; then
      sed -i "/session.gc_maxlifetime =/c\session.gc_maxlifetime = 1440" ${ini_file}
    else
      echo "session.gc_maxlifetime = 1440" >>${ini_file}
    fi

    echo "memory_limit = 256M | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    if [[ ${check_for_parameter} == *"memory_limit ="* ]]; then
      sed -i "/memory_limit =/c\memory_limit = 256M" ${ini_file}
    else
      echo "memory_limit = 256M" >>${ini_file}
    fi

    echo "max_execution_time = 300 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    if [[ ${check_for_parameter} == *"max_execution_time ="* ]]; then
      sed -i "/max_execution_time =/c\max_execution_time = 300" ${ini_file}
    else
      echo "max_execution_time = 300" >>${ini_file}
    fi

    echo "max_input_vars = 5000 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    if [[ ${check_for_parameter} == *"max_input_vars ="* ]]; then
      sed -i "/max_input_vars =/c\max_input_vars = 5000" ${ini_file}
    else
      echo "max_input_vars = 5000" >>${ini_file}
    fi

    echo "max_input_time = 60 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    if [[ ${check_for_parameter} == *"max_input_time ="* ]]; then
      sed -i "/max_input_time =/c\max_input_time = 60" ${ini_file}
    else
      echo "max_input_time = 60" >>${ini_file}
    fi

    echo "upload_max_filesize = 512M | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    if [[ ${check_for_parameter} == *"upload_max_filesize ="* ]]; then
      sed -i "/upload_max_filesize =/c\upload_max_filesize = 512M" ${ini_file}
    else
      echo "upload_max_filesize = 512M" >>${ini_file}
    fi

    echo "upload_max_filesize = 512M | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    if [[ ${check_for_parameter} == *"max_file_uploads ="* ]]; then
      sed -i "/max_file_uploads =/c\max_file_uploads = 20" ${ini_file}
    else
      echo "max_file_uploads = 20" >>${ini_file}
    fi

    echo "post_max_size = 512M | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    if [[ ${check_for_parameter} == *"post_max_size ="* ]]; then
      sed -i "/post_max_size =/c\post_max_size = 512M" ${ini_file}
    else
      echo "post_max_size = 512M" >>${ini_file}
    fi

    if [[ ${ini_file} != *"user.ini"* ]]; then
      echo "allow_url_include = Off | reset"
      if [[ ${check_for_parameter} == *"allow_url_fopen ="* ]]; then
        sed -i "/allow_url_fopen =/c\allow_url_fopen = On" ${ini_file}
      else
        echo "allow_url_fopen = On" >>${ini_file}
      fi

      default_disabled_functions="pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited,pcntl_wifstopped,pcntl_wifsignaled,pcntl_wifcontinued,pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal,pcntl_signal_get_handler,pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror,pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec,pcntl_getpriority,pcntl_setpriority,pcntl_async_signals,"
      echo "disable_functions = ${default_disabled_functions} | reset"
      if [[ ${check_for_parameter} == *"disable_functions ="* ]]; then
        sed -i "/disable_functions =/c\disable_functions = ${default_disabled_functions}" ${ini_file}
      else
        echo "disable_functions = ${default_disabled_functions}" >>${ini_file}
      fi

      gridpane::conf_write allow_url_include Off ${env_type} ${site_or_ver}
      gridpane::conf_write allow_url_fopen On ${env_type} ${site_or_ver}
    fi

    echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log

    gridpane::conf_write date.timezone null ${env_type} ${site_or_ver}
    gridpane::conf_write short_open_tag Off ${env_type} ${site_or_ver}
    gridpane::conf_write session.gc_maxlifetime 1440 ${env_type} ${site_or_ver}
    gridpane::conf_write session.cookie_lifetime 0 ${env_type} ${site_or_ver}
    gridpane::conf_write post_max_size 512 ${env_type} ${site_or_ver}
    gridpane::conf_write max_file_uploads 20 ${env_type} ${site_or_ver}
    gridpane::conf_write upload_max_filesize 512 ${env_type} ${site_or_ver}
    gridpane::conf_write max_input_time 60 ${env_type} ${site_or_ver}
    gridpane::conf_write max_input_vars 5000 ${env_type} ${site_or_ver}
    gridpane::conf_write max_execution_time 300 ${env_type} ${site_or_ver}
    gridpane::conf_write memory_limit 256 ${env_type} ${site_or_ver}

    if [[ ${ini_file} == *"user.ini"* ]]; then
      if [[ ! -z ${site_to_configure_user_ini} ]]; then
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "post_max_size=512" \
          "-s" "date.timezone=NULL" \
          "-s" "session.gc_maxlifetime=1440" \
          "-s" "session.cookie_lifetime=0" \
          "-s" "post_max_size=512" \
          "-s" "max_file_uploads=20" \
          "-s" "upload_max_filesize=512" \
          "-s" "max_input_time=60" \
          "-s" "max_input_vars=5000" \
          "-s" "max_execution_time=300" \
          "-s" "memory_limit=256" \
          "short_open_tag@false" \
          "silent@false"
          gridpane::notify::app \
            "PHP INI Notice" \
            "${site_or_ver}<br>${serverIP}<br>All INI settings returned to defaults!" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
        logIt gridpane-general configure-php Continue ${site_or_ver}
      fi
    fi
  elif [[ ${configure_command} == "opcache-enable" ]] || [[ ${configure_command} == "-opcache-enable" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} requires no value - ${parameter_value} will not be processed..."
    fi
    if [[ ${check_for_parameter} == *"opcache.enable="* ]]; then
      sed -i "/opcache.enable=/c\opcache.enable=1" ${ini_file}
    else
      echo "opcache.enable=1" >>${ini_file}
    fi
    echo "opcache.enable=1"
    echo "${ini_file}"
    gridpane::conf_write opcache.enable 1 ${env_type} ${site_or_ver}
    #TODO (JEFF) Callback to APP - site|server table
  elif [[ ${configure_command} == "opcache-enable-cli" ]] || [[ ${configure_command} == "-opcache-enable-cli" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} requires no value - ${parameter_value} will not be processed..."
    fi
    if [[ ${check_for_parameter} == *"opcache.enable_cli="* ]]; then
      sed -i "/opcache.enable_cli=/c\opcache.enable_cli=1" ${ini_file}
    else
      echo "opcache.enable_cli=1" >>${ini_file}
    fi
    echo "opcache.enable_cli=1"
    echo "${ini_file}"
    gridpane::conf_write opcache.enable_cli 1 ${env_type} ${site_or_ver}
    #TODO (JEFF) Callback to APP - site|server table
  elif [[ ${configure_command} == "opcache-disable" ]] || [[ ${configure_command} == "-opcache-disable" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} requires no value - ${parameter_value} will not be processed..."
    fi
    if [[ ${check_for_parameter} == *"opcache.enable="* ]]; then
      sed -i "/opcache.enable=/c\opcache.enable=0" ${ini_file}
    else
      echo "opcache.enable=0" >>${ini_file}
    fi
    echo "opcache.enable=0"
    echo "${ini_file}"
    gridpane::conf_write opcache.enable 0 ${env_type} ${site_or_ver}
    #TODO (JEFF) Callback to APP - site|server table
  elif [[ ${configure_command} == "opcache-disable-cli" ]] || [[ ${configure_command} == "-opcache-disable-cli" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} requires no value - ${parameter_value} will not be processed..."
    fi
    if [[ ${check_for_parameter} == *"opcache.enable_cli="* ]]; then
      sed -i "/opcache.enable_cli=/c\opcache.enable_cli=0" ${ini_file}
    else
      echo "opcache.enable_cli=0" >>${ini_file}
    fi
    echo "opcache.enable_cli=0"
    echo "${ini_file}"
    gridpane::conf_write opcache.enable_cli 0 ${env_type} ${site_or_ver}
    #TODO (JEFF) Callback to APP - site|server table
  elif [[ ${configure_command} == "opcache-memory" ]] || [[ ${configure_command} == "-opcache-memory" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting value to be passed, none was... exiting..."
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers, we automatically set this to Megabytes - ${parameter_value} cannot be processed, exiting..."
      else
        if [[ ${check_for_parameter} == *"opcache.memory_consumption="* ]]; then
          sed -i "/opcache.memory_consumption=/c\opcache.memory_consumption=${parameter_value}" ${ini_file}
        else
          echo "opcache.memory_consumption=${parameter_value}" >>${ini_file}
        fi
        echo "opcache.memory_consumption=${parameter_value}"
        echo "${ini_file}"
        gridpane::conf_write opcache.memory_consumption ${parameter_value} ${env_type} ${site_or_ver}
        #TODO (JEFF) Callback to APP - site|server table
      fi
    fi
  elif [[ ${configure_command} == "opcache-memory-reset" ]] || [[ ${configure_command} == "-opcache-memory-reset" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} resets back to GridPane default 128M - ${parameter_value} will not be processed"
    fi
    if [[ ${check_for_parameter} == *"opcache.memory_consumption="* ]]; then
      sed -i "/opcache.memory_consumption=/c\opcache.memory_consumption=128" ${ini_file}
    else
      echo "opcache.memory_consumption=128" >>${ini_file}
    fi
    echo "opcache.memory_consumption=128"
    echo "${ini_file}"
    gridpane::conf_write opcache.memory_consumption 128 ${env_type} ${site_or_ver}
    #TODO (JEFF) Callback to APP - site|server table
  elif [[ ${configure_command} == "opcache-max-accel-files" ]] || [[ ${configure_command} == "-opcache-max-accel-files" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting value to be passed, none was... exiting..."
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers - ${parameter_value} cannot be processed, exiting..."
      else
        if [[ ${check_for_parameter} == *"opcache.max_accelerated_files="* ]]; then
          sed -i "/opcache.max_accelerated_files=/c\opcache.max_accelerated_files=${parameter_value}" ${ini_file}
        else
          echo "opcache.max_accelerated_files=${parameter_value}" >>${ini_file}
        fi
        echo "opcache.max_accelerated_files=${parameter_value}"
        echo "${ini_file}"
        gridpane::conf_write opcache.max_accelerated_files ${parameter_value} ${env_type} ${site_or_ver}
        #TODO (JEFF) Callback to APP - site|server table
      fi
    fi
  elif [[ ${configure_command} == "opcache-max-accel-files-reset" ]] || [[ ${configure_command} == "-opcache-max-accel-files-reset" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} resets back to GridPane default 10000- ${parameter_value} will not be processed"
    fi
    if [[ ${check_for_parameter} == *"opcache.max_accelerated_files="* ]]; then
      sed -i "/opcache.max_accelerated_files=/c\opcache.max_accelerated_files=10000" ${ini_file}
    else
      echo "opcache.max_accelerated_files=10000" >>${ini_file}
    fi
    echo "opcache.max_accelerated_files=10000"
    echo "${ini_file}"
    gridpane::conf_write opcache.max_accelerated_files 10000 ${env_type} ${site_or_ver}
    #TODO (JEFF) Callback to APP - site|server table
  elif [[ ${configure_command} == "opcache-reval-freq" ]] || [[ ${configure_command} == "-opcache-reval-freq" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting value to be passed, none was... exiting..."
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers, GridPane sets this to Seconds - ${parameter_value} cannot be processed, exiting..."
      else
        if [[ ${check_for_parameter} == *"opcache.revalidate_freq="* ]]; then
          sed -i "/opcache.revalidate_freq=/c\opcache.revalidate_freq=${parameter_value}" ${ini_file}
        else
          echo "opcache.revalidate_freq=${parameter_value}" >>${ini_file}
        fi
        echo "opcache.revalidate_freq=${parameter_value}"
        echo "${ini_file}"
        gridpane::conf_write opcache.revalidate_freq ${parameter_value} ${env_type} ${site_or_ver}
        #TODO (JEFF) Callback to APP - site|server table
      fi
    fi
  elif [[ ${configure_command} == "opcache-reval-freq-reset" ]] || [[ ${configure_command} == "-opcache-reval-freq-reset" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} resets back to GridPane default 60 Seconds - ${parameter_value} will not be processed"
    fi
    if [[ ${check_for_parameter} == *"opcache.revalidate_freq="* ]]; then
      sed -i "/opcache.revalidate_freq=/c\opcache.revalidate_freq=60" ${ini_file}
    else
      echo "opcache.revalidate_freq=60" >>${ini_file}
    fi
    echo "opcache.revalidate_freq=60"
    echo "${ini_file}"
    gridpane::conf_write opcache.revalidate_freq 60 ${env_type} ${site_or_ver}
    #TODO (JEFF) Callback to APP - site|server table
  elif [[ ${configure_command} == "opcache-reset" ]] || [[ ${configure_command} == "-opcache-reset" ]]; then
    echo "opcache.enable=1 | reset"
    if [[ ${check_for_parameter} == *"opcache.enable="* ]]; then
      sed -i "/opcache.enable=/c\opcache.enable=1" ${ini_file}
    else
      echo "opcache.enable=1" >>${ini_file}
    fi
    echo "opcache.enable_cli=0 | reset"
    if [[ ${check_for_parameter} == *"opcache.enable_cli="* ]]; then
      sed -i "/opcache.enable_cli=/c\opcache.enable_cli=0" ${ini_file}
    else
      echo "opcache.enable_cli=0" >>${ini_file}
    fi
    echo "opcache.memory_consumption=128 | reset"
    if [[ ${check_for_parameter} == *"opcache.memory_consumption="* ]]; then
      sed -i "/opcache.memory_consumption=/c\opcache.memory_consumption=128" ${ini_file}
    else
      echo "opcache.memory_consumption=128" >>${ini_file}
    fi
    echo "opcache.max_accelerated_files=10000 | reset"
    if [[ ${check_for_parameter} == *"opcache.max_accelerated_files="* ]]; then
      sed -i "/opcache.max_accelerated_files=/c\opcache.max_accelerated_files=10000" ${ini_file}
    else
      echo "opcache.max_accelerated_files=10000" >>${ini_file}
    fi
    echo "opcache.revalidate_freq=60 | reset"
    if [[ ${check_for_parameter} == *"opcache.revalidate_freq="* ]]; then
      sed -i "/opcache.revalidate_freq=/c\opcache.revalidate_freq=60" ${ini_file}
    else
      echo "opcache.revalidate_freq=60" >>${ini_file}
    fi
    echo "${ini_file}"
    gridpane::conf_write opcache.revalidate_freq 60 ${env_type} ${site_or_ver}
    gridpane::conf_write opcache.max_accelerated_files 10000 ${env_type} ${site_or_ver}
    gridpane::conf_write opcache.memory_consumption 128 ${env_type} ${site_or_ver}
    gridpane::conf_write opcache.enable_cli 0 ${env_type} ${site_or_ver}
    gridpane::conf_write opcache.enable 1 ${env_type} ${site_or_ver}
    #TODO (JEFF) Callback to APP - site|server table
  elif [[ ${configure_command} == "site-pm" ]] || [[ ${configure_command} == "-site-pm" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting value to be passed, none was... exiting..."
    else
      if [[ "${parameter_value}" != "ondemand" ]] &&
        [[ "${parameter_value}" != "dynamic" ]] &&
        [[ "${parameter_value}" != "static" ]]; then
        echo "${configure_command} only accepts ondemand/dynamic/static - ${parameter_value} cannot be processed, exiting..."
      else
        if [[ ${check_for_parameter} == *"pm ="* ]]; then
          sed -i "/pm =/c\pm = ${parameter_value}" ${ini_file}
        else
          echo "pm = ${parameter_value}" >>${ini_file}
        fi
        echo "pm = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        gridpane::conf_write pm ${parameter_value} ${env_type} ${site_or_ver}
        if [[ "${parameter_value}" == "ondemand" ||
              "${parameter_value}" == "static" ]]; then
          if [[ ${check_for_parameter} == *"pm ="* ]]; then
            sed -i "/pm.start_servers =/c\pm.start_servers = 1" ${ini_file}
          else
            echo "pm.start_servers = 1" >>${ini_file}
          fi
          if [[ ${check_for_parameter} == *"pm ="* ]]; then
            sed -i "/pm.min_spare_servers =/c\pm.min_spare_servers = 1" ${ini_file}
          else
            echo "pm.min_spare_servers = 1" >>${ini_file}
          fi
          if [[ ${check_for_parameter} == *"pm ="* ]]; then
            sed -i "/pm.max_spare_servers =/c\pm.max_spare_servers = 1" ${ini_file}
          else
            echo "pm.max_spare_servers = 1" >>${ini_file}
          fi
          echo "pm.start_servers = 1 (setting safe - unused with static|ondemand)" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          echo "pm.min_spare_servers = 1 (setting safe - unused with static|ondemand)" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          echo "pm.max_spare_servers = 1 (setting safe - unused with static|ondemand)" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          gridpane::conf_write pm.start_servers 1 ${env_type} ${site_or_ver}
          gridpane::conf_write pm.min_spare_servers 1 ${env_type} ${site_or_ver}
          gridpane::conf_write pm.max_spare_servers 1 ${env_type} ${site_or_ver}
          gridpane::callback::app \
            "/site/site-update" \
            "--" \
            "site_url=${site_or_ver}" \
            "server_ip=${serverIP}" \
            "pm=${parameter_value}" \
            "-s" "pm.start_servers=1" \
            "-s" "pm.min_spare_servers=1" \
            "-s" "pm.max_spare_servers=1" \
            "silent@false"
          gridpane::notify::app \
            "PHP-FPM Notice" \
            "${site_or_ver}<br>${serverIP}<br>pm: ${parameter_value}<br>pm.start_servers: 1 (safe)<br>pm.min_spare_servers: 1 (safe)<br>pm.max_spare_servers: 1 (safe)" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
        else
          echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          gridpane::callback::app \
            "/site/site-update" \
            "--" \
            "site_url=${site_or_ver}" \
            "server_ip=${serverIP}" \
            "pm=${parameter_value}" \
            "silent@false"
          gridpane::notify::app \
            "PHP-FPM Notice" \
            "${site_or_ver}<br>${serverIP}<br>pm: ${parameter_value}" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
        fi
      fi
    fi
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "site-pm-reset" ]] || [[ ${configure_command} == "-site-pm-reset" ]]; then
    if [[ ! -z "${parameter_value}" ]]; then
      echo "${configure_command} requires no setting to be passed - ignoring ${parameter_value}"
    fi
    if [[ ${check_for_parameter} == *"pm ="* ]]; then
      sed -i "/pm =/c\pm = dynamic" ${ini_file}
    else
      echo "pm = dynamic" >>${ini_file}
    fi
    echo "pm = dynamic | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    gridpane::conf_write pm dynamic ${env_type} ${site_or_ver}
    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=${site_or_ver}" \
      "server_ip=${serverIP}" \
      "pm=dynamic" \
      "silent@false"
    gridpane::notify::app \
      "PHP-FPM Notice" \
      "${site_or_ver}<br>${serverIP}<br>pm: dynamic" \
      "popup_and_center" \
      "fa-cog" \
      "default" \
      "${site_or_ver}"
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "site-pm-max-children" ]] || [[ ${configure_command} == "-site-pm-max-children" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting integer value to be passed, none was... exiting..."
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers - ${parameter_value} cannot be processed, exiting..."
      else
        local old_min_spare_servers
        old_min_spare_servers=$(gridpane::conf_read pm-min-spare-servers ${env_type} ${site_or_ver})
        if [[ -n ${old_min_spare_servers} ]]; then
          gridpane::conf_delete pm-min-spare-servers ${env_type} ${site_or_ver}
          gridpane::conf_write pm.min_spare_servers "${old_min_spare_servers}" ${env_type} ${site_or_ver}
        fi
        local old_max_spare_servers
        old_max_spare_servers=$(gridpane::conf_read pm-max-spare-servers ${env_type} ${site_or_ver})
        if [[ -n ${old_max_spare_servers} ]]; then
          gridpane::conf_delete pm-max-spare-servers ${env_type} ${site_or_ver}
          gridpane::conf_write pm.max_spare_servers "${old_max_spare_servers}" ${env_type} ${site_or_ver}
        fi
        local old_start_servers
        old_start_servers=$(gridpane::conf_read pm-start-servers ${env_type} ${site_or_ver})
        if [[ -n ${old_start_servers} ]]; then
          gridpane::conf_delete pm-start-servers ${env_type} ${site_or_ver}
          gridpane::conf_write pm.start_servers "${old_start_servers}" ${env_type} ${site_or_ver}
        fi
        if [[ $4 != "-force" ]]; then
          local min_spare_servers
          min_spare_servers=$(gridpane::conf_read pm.min_spare_servers ${env_type} ${site_or_ver})
          local max_spare_servers
          max_spare_servers=$(gridpane::conf_read pm.max_spare_servers ${env_type} ${site_or_ver})
          local start_servers
          start_servers=$(gridpane::conf_read pm.start_servers ${env_type} ${site_or_ver})
          local is_done
          if [[ ${parameter_value} -lt "${min_spare_servers}" ]]; then
            echo "pm.min_spare_servers(${min_spare_servers}) cannot be greater than pm.max_children(${parameter_value})"
            is_done="done"
          fi
          if [[ ${parameter_value} -lt "${max_spare_servers}" ]]; then
            echo "pm.max_spare_servers(${max_spare_servers}) cannot be greater than pm.max_children(${parameter_value})"
            is_done="done"
          fi
          if [[ ${parameter_value} -lt "${start_servers}" ]]; then
            echo "pm.start_servers(${start_servers}) cannot be greater than pm.max_children(${parameter_value})"
            is_done="done"
          fi
        fi
        if [[ ${is_done} != "done" ]]; then
          if [[ ${check_for_parameter} == *"pm.max_children ="* ]]; then
            sed -i "/pm.max_children =/c\pm.max_children = ${parameter_value}" ${ini_file}
          else
            echo "pm.max_children = ${parameter_value}" >>${ini_file}
          fi
          echo "pm.max_children = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          gridpane::conf_write pm.max_children ${parameter_value} ${env_type} ${site_or_ver}
          gridpane::callback::app \
            "/site/site-update" \
            "--" \
            "site_url=${site_or_ver}" \
            "server_ip=${serverIP}" \
            "-s" "pm.max_children=${parameter_value}" \
            "silent@false"
          gridpane::notify::app \
            "PHP-FPM Notice" \
            "${site_or_ver}<br>${serverIP}<br>pm.max_children: ${parameter_value}" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
        fi
      fi
    fi
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "site-pm-max-children-reset" ]] || [[ ${configure_command} == "-site-pm-max-children-reset" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires no setting to be passed - ${parameter_value} will be ignored..."
    fi
    local old_min_spare_servers
    old_min_spare_servers=$(gridpane::conf_read pm-min-spare-servers ${env_type} ${site_or_ver})
    if [[ -n ${old_min_spare_servers} ]]; then
      gridpane::conf_delete pm-min-spare-servers ${env_type} ${site_or_ver}
      gridpane::conf_write pm.min_spare_servers "${old_min_spare_servers}" ${env_type} ${site_or_ver}
    fi
    local old_max_spare_servers
    old_max_spare_servers=$(gridpane::conf_read pm-max-spare-servers ${env_type} ${site_or_ver})
    if [[ -n ${old_max_spare_servers} ]]; then
      gridpane::conf_delete pm-max-spare-servers ${env_type} ${site_or_ver}
      gridpane::conf_write pm.max_spare_servers "${old_max_spare_servers}" ${env_type} ${site_or_ver}
    fi
    local old_start_servers
    old_start_servers=$(gridpane::conf_read pm-start-servers ${env_type} ${site_or_ver})
    if [[ -n ${old_start_servers} ]]; then
      gridpane::conf_delete pm-start-servers ${env_type} ${site_or_ver}
      gridpane::conf_write pm.start_servers "${old_start_servers}" ${env_type} ${site_or_ver}
    fi
    if [[ $4 != "-force" ]]; then
      local min_spare_servers
      min_spare_servers=$(gridpane::conf_read pm.min_spare_servers ${env_type} ${site_or_ver})
      local max_spare_servers
      max_spare_servers=$(gridpane::conf_read pm.max_spare_servers ${env_type} ${site_or_ver})
      local start_servers
      start_servers=$(gridpane::conf_read pm.start_servers ${env_type} ${site_or_ver})
      local is_done
      local cores
      cores=$(nproc --all)
      local quad_cores
      quad_cores=$((${cores} * 4))
      if [[ "${min_spare_servers}" -gt ${quad_cores} ]]; then
        echo "Default pm.max_children(${quad_cores}) cannot be less than pm.min_spare_servers(${min_spare_servers}), unable to reset..."
        is_done="done"
      fi
      if [[ "${max_spare_servers}" -gt ${quad_cores} ]]; then
        echo "Default pm.max_children(${quad_cores}) cannot be less than pm.max_spare_servers(${max_spare_servers}), unable to reset..."
        is_done="done"
      fi
      if [[ "${start_servers}" -gt ${quad_cores} ]]; then
        echo "pm.start_servers(${start_servers}) cannot be greater than default pm.max_children(${quad_cores}), unable to reset..."
        is_done="done"
      fi
    fi
    if [[ ${is_done} != "done" ]]; then
      if [[ ${check_for_parameter} == *"pm.max_children ="* ]]; then
        sed -i "/pm.max_children =/c\pm.max_children = ${quad_cores}" ${ini_file}
      else
        echo "pm.max_children = ${quad_cores}" >>${ini_file}
      fi
      echo "pm.max_children = ${quad_cores} | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      gridpane::conf_write pm.max_children "${quad_cores}" ${env_type} ${site_or_ver}
      gridpane::callback::app \
        "/site/site-update" \
        "--" \
        "site_url=${site_or_ver}" \
        "server_ip=${serverIP}" \
        "-s" "pm.max_children=${quad_cores}" \
        "silent@false"
      gridpane::notify::app \
        "PHP-FPM Notice" \
        "${site_or_ver}<br>${serverIP}<br>pm.max_children: ${quad_cores}" \
        "popup_and_center" \
        "fa-cog" \
        "default" \
        "${site_or_ver}"
    fi
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "site-pm-max-requests" ]] || [[ ${configure_command} == "-site-pm-max-requests" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting integer value to be passed, none was... exiting..."
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers - ${parameter_value} cannot be processed, exiting..."
      else
        if [[ ${check_for_parameter} == *"pm.max_requests ="* ]]; then
          sed -i "/pm.max_requests =/c\pm.max_requests = ${parameter_value}" ${ini_file}
        else
          echo "pm.max_requests = ${parameter_value}" >>${ini_file}
        fi
        echo "pm.max_requests = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        gridpane::conf_write pm.max_requests ${parameter_value} ${env_type} ${site_or_ver}
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "pm.max_requests=${parameter_value}" \
          "silent@false"
        gridpane::notify::app \
          "PHP-FPM Notice" \
          "${site_or_ver}<br>${serverIP}<br>pm.max_requests: ${parameter_value}" \
          "popup_and_center" \
          "fa-cog" \
          "default" \
          "${site_or_ver}"
      fi
    fi
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "site-pm-max-requests-reset" ]] || [[ ${configure_command} == "-site-pm-max-requests-reset" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires no setting to be passed - ${parameter_value} will be ignored..."
    fi
    if [[ ${check_for_parameter} == *"pm.max_requests ="* ]]; then
      sed -i "/pm.max_requests =/c\pm.max_requests = 500" ${ini_file}
    else
      echo "pm.max_requests = 500" >>${ini_file}
    fi
    echo "pm.max_requests = 500 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    gridpane::conf_write pm.max_requests "500" ${env_type} ${site_or_ver}
    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=${site_or_ver}" \
      "server_ip=${serverIP}" \
      "-s" "pm.max_requests=500" \
      "silent@false"
    gridpane::notify::app \
      "PHP-FPM Notice" \
      "${site_or_ver}<br>${serverIP}<br>pm.max_requests: 500" \
      "popup_and_center" \
      "fa-cog" \
      "default" \
      "${site_or_ver}"
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "site-pm-start-servers" ]] || [[ ${configure_command} == "-site-pm-start-servers" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting integer value to be passed, none was... exiting..."
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers - ${parameter_value} cannot be processed, exiting..."
      else
        local old_min_spare_servers
        old_min_spare_servers=$(gridpane::conf_read pm-min-spare-servers ${env_type} ${site_or_ver})
        if [[ -n ${old_min_spare_servers} ]]; then
          gridpane::conf_delete pm-min-spare-servers ${env_type} ${site_or_ver}
          gridpane::conf_write pm.min_spare_servers "${old_min_spare_servers}" ${env_type} ${site_or_ver}
        fi
        local old_max_spare_servers
        old_max_spare_servers=$(gridpane::conf_read pm-max-spare-servers ${env_type} ${site_or_ver})
        if [[ -n ${old_max_spare_servers} ]]; then
          gridpane::conf_delete pm-max-spare-servers ${env_type} ${site_or_ver}
          gridpane::conf_write pm.max_spare_servers "${old_max_spare_servers}" ${env_type} ${site_or_ver}
        fi
        local old_start_servers
        old_start_servers=$(gridpane::conf_read pm-start-servers ${env_type} ${site_or_ver})
        if [[ -n ${old_start_servers} ]]; then
          gridpane::conf_delete pm-start-servers ${env_type} ${site_or_ver}
          gridpane::conf_write pm.start_servers "${old_start_servers}" ${env_type} ${site_or_ver}
        fi
        if [[ $4 != "-force" ]]; then
          local max_children
          max_children=$(gridpane::conf_read pm.max_children ${env_type} ${site_or_ver})
          local min_spare_servers
          min_spare_servers=$(gridpane::conf_read pm.min_spare_servers ${env_type} ${site_or_ver})
          local max_spare_servers
          max_spare_servers=$(gridpane::conf_read pm.max_spare_servers ${env_type} ${site_or_ver})
          local start_servers
          start_servers=$(gridpane::conf_read pm.start_servers ${env_type} ${site_or_ver})
          local is_done
          if [[ "${parameter_value}" -lt "${min_spare_servers}" ]]; then
            echo "pm.start_servers(${parameter_value}) must not be less than pm.min_spare_servers(${min_spare_servers})"
            is_done="done"
          fi
          if [[ "${parameter_value}" -gt "${max_spare_servers}" ]]; then
            echo "pm.start_servers(${parameter_value}) must not be greater than pm.max_spare_servers(${max_spare_servers})"
            is_done="done"
          fi
          if [[ "${parameter_value}" -gt "${max_children}" ]]; then
            echo "pm.start_servers(${parameter_value}) must not be greater than pm.max_children(${max_children})"
            is_done="done"
          fi
        fi
        if [[ ${is_done} != "done" ]]; then
          if [[ ${check_for_parameter} == *"pm.start_servers ="* ]]; then
            sed -i "/pm.start_servers =/c\pm.start_servers = ${parameter_value}" ${ini_file}
          else
            echo "pm.start_servers = ${parameter_value}" >>${ini_file}
          fi
          echo "pm.start_servers = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          current_pm=$(gridpane::conf_read pm ${env_type} ${site_or_ver})
          if [[ ${current_pm} != *"dynamic"* ]]; then
            echo "-------------------------------------------------------------"
            echo "pm.start_servers is only used when pm is set to 'dynamic'"
            echo "${site_or_ver} pm is currently set to ${current_pm}..."
            echo "-------------------------------------------------------------"
          fi
          gridpane::conf_write pm.start_servers ${parameter_value} ${env_type} ${site_or_ver}
          gridpane::callback::app \
            "/site/site-update" \
            "--" \
            "site_url=${site_or_ver}" \
            "server_ip=${serverIP}" \
            "-s" "pm.start_servers=${parameter_value}" \
            "silent@false"
          gridpane::notify::app \
            "PHP-FPM Notice" \
            "${site_or_ver}<br>${serverIP}<br>pm.start_servers: ${parameter_value}" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
        fi
      fi
    fi
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "pm-start-servers-reset" ]] || [[ ${configure_command} == "-pm-start-servers-reset" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires no setting to be passed - ${parameter_value} will be ignored..."
    fi
    local old_min_spare_servers
    old_min_spare_servers=$(gridpane::conf_read pm-min-spare-servers ${env_type} ${site_or_ver})
    if [[ -n ${old_min_spare_servers} ]]; then
      gridpane::conf_delete pm-min-spare-servers ${env_type} ${site_or_ver}
      gridpane::conf_write pm.min_spare_servers "${old_min_spare_servers}" ${env_type} ${site_or_ver}
    fi
    local old_max_spare_servers
    old_max_spare_servers=$(gridpane::conf_read pm-max-spare-servers ${env_type} ${site_or_ver})
    if [[ -n ${old_max_spare_servers} ]]; then
      gridpane::conf_delete pm-max-spare-servers ${env_type} ${site_or_ver}
      gridpane::conf_write pm.max_spare_servers "${old_max_spare_servers}" ${env_type} ${site_or_ver}
    fi
    local old_start_servers
    old_start_servers=$(gridpane::conf_read pm-start-servers ${env_type} ${site_or_ver})
    if [[ -n ${old_start_servers} ]]; then
      gridpane::conf_delete pm-start-servers ${env_type} ${site_or_ver}
      gridpane::conf_write pm.start_servers "${old_start_servers}" ${env_type} ${site_or_ver}
    fi
    if [[ $4 != "-force" ]]; then
      local max_children
      max_children=$(gridpane::conf_read pm.max_children ${env_type} ${site_or_ver})
      local min_spare_servers
      min_spare_servers=$(gridpane::conf_read pm.min_spare_servers ${env_type} ${site_or_ver})
      local max_spare_servers
      max_spare_servers=$(gridpane::conf_read pm.max_spare_servers ${env_type} ${site_or_ver})
      local start_servers
      start_servers=$(gridpane::conf_read pm.start_servers ${env_type} ${site_or_ver})
      local is_done
      if [[ "${min_spare_servers}" -gt 1 ]]; then
        echo "Default pm.start_servers(1) must not be less than pm.min_spare_servers(${min_spare_servers}), unable to reset..."
        is_done="done"
      fi
      if [[ "${max_spare_servers}" -lt 1 ]]; then
        echo "Default pm.start_servers(1) must not be greater than pm.max_spare_servers(${max_spare_servers}), unable to reset..."
        is_done="done"
      fi
      if [[ "${max_children}" -lt 1 ]]; then
        echo "Default pm.start_servers(1) must not be greater than pm.max_children(${max_children}), unable to reset..."
        is_done="done"
      fi
    fi
    if [[ ${is_done} != "done" ]]; then
      if [[ ${check_for_parameter} == *"pm.start_servers ="* ]]; then
        sed -i "/pm.start_servers =/c\pm.start_servers = 1" ${ini_file}
      else
        echo "pm.start_servers = 1" >>${ini_file}
      fi
      echo "pm.start_servers = 1 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      current_pm=$(gridpane::conf_read pm ${env_type} ${site_or_ver})
      if [[ ${current_pm} != *"dynamic"* ]]; then
        echo "-------------------------------------------------------------"
        echo "pm.start_servers is only used when pm is set to 'dynamic'"
        echo "${site_or_ver} pm is currently set to ${current_pm}..."
        echo "-------------------------------------------------------------"
      fi
      gridpane::conf_write pm.start_servers "1" ${env_type} ${site_or_ver}
      gridpane::callback::app \
        "/site/site-update" \
        "--" \
        "site_url=${site_or_ver}" \
        "server_ip=${serverIP}" \
        "-s" "pm.start_servers=1" \
        "silent@false"
      gridpane::notify::app \
        "PHP-FPM Notice" \
        "${site_or_ver}<br>${serverIP}<br>pm.start_servers: 1" \
        "popup_and_center" \
        "fa-cog" \
        "default" \
        "${site_or_ver}"
    fi
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "site-pm-min-spare-servers" ]] || [[ ${configure_command} == "-site-pm-min-spare-servers" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting integer value to be passed, none was... exiting..."
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers - ${parameter_value} cannot be processed, exiting..."
      else
        local old_min_spare_servers
        old_min_spare_servers=$(gridpane::conf_read pm-min-spare-servers ${env_type} ${site_or_ver})
        if [[ -n ${old_min_spare_servers} ]]; then
          gridpane::conf_delete pm-min-spare-servers ${env_type} ${site_or_ver}
          gridpane::conf_write pm.min_spare_servers "${old_min_spare_servers}" ${env_type} ${site_or_ver}
        fi
        local old_max_spare_servers
        old_max_spare_servers=$(gridpane::conf_read pm-max-spare-servers ${env_type} ${site_or_ver})
        if [[ -n ${old_max_spare_servers} ]]; then
          gridpane::conf_delete pm-max-spare-servers ${env_type} ${site_or_ver}
          gridpane::conf_write pm.max_spare_servers "${old_max_spare_servers}" ${env_type} ${site_or_ver}
        fi
        local old_start_servers
        old_start_servers=$(gridpane::conf_read pm-start-servers ${env_type} ${site_or_ver})
        if [[ -n ${old_start_servers} ]]; then
          gridpane::conf_delete pm-start-servers ${env_type} ${site_or_ver}
          gridpane::conf_write pm.start_servers "${old_start_servers}" ${env_type} ${site_or_ver}
        fi
        if [[ $4 != "-force" ]]; then
          local max_children
          max_children=$(gridpane::conf_read pm.max_children ${env_type} ${site_or_ver})
          local min_spare_servers
          min_spare_servers=$(gridpane::conf_read pm.min_spare_servers ${env_type} ${site_or_ver})
          local max_spare_servers
          max_spare_servers=$(gridpane::conf_read pm.max_spare_servers ${env_type} ${site_or_ver})
          local start_servers
          start_servers=$(gridpane::conf_read pm.start_servers ${env_type} ${site_or_ver})
          local is_done
          if [[ "${parameter_value}" -gt "${start_servers}" ]]; then
            echo "pm.min_spare_servers(${parameter_value}) must not be greater than pm.start_servers(${start_servers})"
            is_done="done"
          fi
          if [[ "${parameter_value}" -gt "${max_spare_servers}" ]]; then
            echo "pm.min_spare_servers(${parameter_value}) must not be greater than pm.max_spare_servers(${max_spare_servers})"
            is_done="done"
          fi
          if [[ "${parameter_value}" -gt "${max_children}" ]]; then
            echo "pm.min_spare_servers(${parameter_value}) must not be greater than pm.max_children(${max_children})"
            is_done="done"
          fi
        fi
        if [[ ${is_done} != "done" ]]; then
          if [[ ${check_for_parameter} == *"pm.min_spare_servers ="* ]]; then
            sed -i "/pm.min_spare_servers =/c\pm.min_spare_servers = ${parameter_value}" ${ini_file}
          else
            echo "pm.min_spare_servers = ${parameter_value}" >>${ini_file}
          fi
          echo "pm.min_spare_servers = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          current_pm=$(gridpane::conf_read pm ${env_type} ${site_or_ver})
          if [[ ${current_pm} != *"dynamic"* ]]; then
            echo "-------------------------------------------------------------"
            echo "pm.min_spare_servers is only used when pm is set to 'dynamic'"
            echo "${site_or_ver} pm is currently set to ${current_pm}..."
            echo "-------------------------------------------------------------"
          fi
          gridpane::conf_write pm.min_spare_servers ${parameter_value} ${env_type} ${site_or_ver}
          gridpane::callback::app \
            "/site/site-update" \
            "--" \
            "site_url=${site_or_ver}" \
            "server_ip=${serverIP}" \
            "-s" "pm.min_spare_servers=${parameter_value}" \
            "silent@false"
          gridpane::notify::app \
            "PHP-FPM Notice" \
            "${site_or_ver}<br>${serverIP}<br>pm.min_spare_servers: ${parameter_value}" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
        fi
      fi
    fi
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "site-pm-min-spare-servers-reset" ]] || [[ ${configure_command} == "-site-pm-min-spare-servers-reset" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires no setting to be passed - ${parameter_value} will be ignored..."
    fi
    local old_min_spare_servers
    old_min_spare_servers=$(gridpane::conf_read pm-min-spare-servers ${env_type} ${site_or_ver})
    if [[ -n ${old_min_spare_servers} ]]; then
      gridpane::conf_delete pm-min-spare-servers ${env_type} ${site_or_ver}
      gridpane::conf_write pm.min_spare_servers "${old_min_spare_servers}" ${env_type} ${site_or_ver}
    fi
    local old_max_spare_servers
    old_max_spare_servers=$(gridpane::conf_read pm-max-spare-servers ${env_type} ${site_or_ver})
    if [[ -n ${old_max_spare_servers} ]]; then
      gridpane::conf_delete pm-max-spare-servers ${env_type} ${site_or_ver}
      gridpane::conf_write pm.max_spare_servers "${old_max_spare_servers}" ${env_type} ${site_or_ver}
    fi
    local old_start_servers
    old_start_servers=$(gridpane::conf_read pm-start-servers ${env_type} ${site_or_ver})
    if [[ -n ${old_start_servers} ]]; then
      gridpane::conf_delete pm-start-servers ${env_type} ${site_or_ver}
      gridpane::conf_write pm.start_servers "${old_start_servers}" ${env_type} ${site_or_ver}
    fi
    if [[ $4 != "-force" ]]; then
      local max_children
      max_children=$(gridpane::conf_read pm.max_children ${env_type} ${site_or_ver})
      local min_spare_servers
      min_spare_servers=$(gridpane::conf_read pm.min_spare_servers ${env_type} ${site_or_ver})
      local max_spare_servers
      max_spare_servers=$(gridpane::conf_read pm.max_spare_servers ${env_type} ${site_or_ver})
      local start_servers
      start_servers=$(gridpane::conf_read pm.start_servers ${env_type} ${site_or_ver})
      local is_done
      if [[ "${start_servers}" -lt 1 ]]; then
        echo "pm.min_spare_servers(1) must not be greater than pm.start_servers(${start_servers}), unable to reset..."
        is_done="done"
      fi
      if [[ "${max_spare_servers}" -lt 1 ]]; then
        echo "pm.min_spare_servers(1) must not be greater than pm.max_spare_servers(${max_spare_servers}), unable to reset..."
        is_done="done"
      fi
      if [[ "${max_children}" -lt 1 ]]; then
        echo "pm.min_spare_servers(1) must not be greater than pm.max_children(${max_children}), unable to reset..."
        is_done="done"
      fi
    fi
    if [[ ${is_done} != "done" ]]; then
      if [[ ${check_for_parameter} == *"pm.min_spare_servers ="* ]]; then
        sed -i "/pm.min_spare_servers =/c\pm.min_spare_servers = 1" ${ini_file}
      else
        echo "pm.min_spare_servers = 1" >>${ini_file}
      fi
      echo "pm.min_spare_servers = 1 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      current_pm=$(gridpane::conf_read pm ${env_type} ${site_or_ver})
      if [[ ${current_pm} != *"dynamic"* ]]; then
        echo "-------------------------------------------------------------"
        echo "pm.min_spare_servers is only used when pm is set to 'dynamic'"
        echo "${site_or_ver} pm is currently set to ${current_pm}..."
        echo "-------------------------------------------------------------"
      fi
      gridpane::conf_write pm.min_spare_servers "1" ${env_type} ${site_or_ver}
      gridpane::callback::app \
        "/site/site-update" \
        "--" \
        "site_url=${site_or_ver}" \
        "server_ip=${serverIP}" \
        "-s" "pm.min_spare_servers=1" \
        "silent@false"
      gridpane::notify::app \
        "PHP-FPM Notice" \
        "${site_or_ver}<br>${serverIP}<br>pm.min_spare_servers: 1" \
        "popup_and_center" \
        "fa-cog" \
        "default" \
        "${site_or_ver}"
    fi
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "site-pm-max-spare-servers" ]] || [[ ${configure_command} == "-site-pm-max-spare-servers" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting integer value to be passed, none was... exiting..."
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers - ${parameter_value} cannot be processed, exiting..."
      else
        local old_min_spare_servers
        old_min_spare_servers=$(gridpane::conf_read pm-min-spare-servers ${env_type} ${site_or_ver})
        if [[ -n ${old_min_spare_servers} ]]; then
          gridpane::conf_delete pm-min-spare-servers ${env_type} ${site_or_ver}
          gridpane::conf_write pm.min_spare_servers "${old_min_spare_servers}" ${env_type} ${site_or_ver}
        fi
        local old_max_spare_servers
        old_max_spare_servers=$(gridpane::conf_read pm-max-spare-servers ${env_type} ${site_or_ver})
        if [[ -n ${old_max_spare_servers} ]]; then
          gridpane::conf_delete pm-max-spare-servers ${env_type} ${site_or_ver}
          gridpane::conf_write pm.max_spare_servers "${old_max_spare_servers}" ${env_type} ${site_or_ver}
        fi
        local old_start_servers
        old_start_servers=$(gridpane::conf_read pm-start-servers ${env_type} ${site_or_ver})
        if [[ -n ${old_start_servers} ]]; then
          gridpane::conf_delete pm-start-servers ${env_type} ${site_or_ver}
          gridpane::conf_write pm.start_servers "${old_start_servers}" ${env_type} ${site_or_ver}
        fi
        if [[ $4 != "-force" ]]; then
          local max_children
          max_children=$(gridpane::conf_read pm.max_children ${env_type} ${site_or_ver})
          local min_spare_servers
          min_spare_servers=$(gridpane::conf_read pm.min_spare_servers ${env_type} ${site_or_ver})
          local max_spare_servers
          max_spare_servers=$(gridpane::conf_read pm.max_spare_servers ${env_type} ${site_or_ver})
          local start_servers
          start_servers=$(gridpane::conf_read pm.start_servers ${env_type} ${site_or_ver})
          local is_done
          if [[ "${parameter_value}" -lt "${start_servers}" ]]; then
            echo "pm.max_spare_servers(${parameter_value}) must not be less than pm.start_servers(${start_servers})"
            is_done="done"
          fi
          if [[ "${parameter_value}" -lt "${min_spare_servers}" ]]; then
            echo "pm.max_spare_servers(${parameter_value}) must not be less than pm.min_spare_servers(${min_spare_servers})"
            is_done="done"
          fi
          if [[ "${parameter_value}" -gt "${max_children}" ]]; then
            echo "pm.max_spare_servers(${parameter_value}) must not be greater than pm.max_children(${max_children})"
            is_done="done"
          fi
        fi
        if [[ ${is_done} != "done" ]]; then
          if [[ ${check_for_parameter} == *"pm.max_spare_servers ="* ]]; then
            sed -i "/pm.max_spare_servers =/c\pm.max_spare_servers = ${parameter_value}" ${ini_file}
          else
            echo "pm.max_spare_servers = ${parameter_value}" >>${ini_file}
          fi
          echo "pm.max_spare_servers = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
          current_pm=$(gridpane::conf_read pm ${env_type} ${site_or_ver})
          if [[ ${current_pm} != *"dynamic"* ]]; then
            echo "-------------------------------------------------------------"
            echo "pm.max_spare_servers is only used when pm is set to 'dynamic'"
            echo "${site_or_ver} pm is currently set to ${current_pm}..."
            echo "-------------------------------------------------------------"
          fi
          gridpane::conf_write pm.max_spare_servers ${parameter_value} ${env_type} ${site_or_ver}
          gridpane::callback::app \
            "/site/site-update" \
            "--" \
            "site_url=${site_or_ver}" \
            "server_ip=${serverIP}" \
            "-s" "pm.max_spare_servers=${parameter_value}" \
            "silent@false"
          gridpane::notify::app \
            "PHP-FPM Notice" \
            "${site_or_ver}<br>${serverIP}<br>pm.max_spare_servers: ${parameter_value}" \
            "popup_and_center" \
            "fa-cog" \
            "default" \
            "${site_or_ver}"
        fi
      fi
    fi
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "site-pm-max-spare-servers-reset" ]] || [[ ${configure_command} == "-site-pm-max-spare-servers-reset" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires no setting to be passed - ${parameter_value} will be ignored..."
    fi
    local old_min_spare_servers
    old_min_spare_servers=$(gridpane::conf_read pm-min-spare-servers ${env_type} ${site_or_ver})
    if [[ -n ${old_min_spare_servers} ]]; then
      gridpane::conf_delete pm-min-spare-servers ${env_type} ${site_or_ver}
      gridpane::conf_write pm.min_spare_servers "${old_min_spare_servers}" ${env_type} ${site_or_ver}
    fi
    local old_max_spare_servers
    old_max_spare_servers=$(gridpane::conf_read pm-max-spare-servers ${env_type} ${site_or_ver})
    if [[ -n ${old_max_spare_servers} ]]; then
      gridpane::conf_delete pm-max-spare-servers ${env_type} ${site_or_ver}
      gridpane::conf_write pm.max_spare_servers "${old_max_spare_servers}" ${env_type} ${site_or_ver}
    fi
    local old_start_servers
    old_start_servers=$(gridpane::conf_read pm-start-servers ${env_type} ${site_or_ver})
    if [[ -n ${old_start_servers} ]]; then
      gridpane::conf_delete pm-start-servers ${env_type} ${site_or_ver}
      gridpane::conf_write pm.start_servers "${old_start_servers}" ${env_type} ${site_or_ver}
    fi
    if [[ $4 != "-force" ]]; then
      local max_children
      max_children=$(gridpane::conf_read pm.max_children ${env_type} ${site_or_ver})
      local min_spare_servers
      min_spare_servers=$(gridpane::conf_read pm.min_spare_servers ${env_type} ${site_or_ver})
      local max_spare_servers
      max_spare_servers=$(gridpane::conf_read pm.max_spare_servers ${env_type} ${site_or_ver})
      local start_servers
      start_servers=$(gridpane::conf_read pm.start_servers ${env_type} ${site_or_ver})
      local is_done
      if [[ "${start_servers}" -gt 1 ]]; then
        echo "pm.max_spare_servers(1) must not be less than pm.start_servers(${start_servers})"
        is_done="done"
      fi
      if [[ "${min_spare_servers}" -gt 1 ]]; then
        echo "pm.max_spare_servers(1) must not be less than pm.min_spare_servers(${min_spare_servers})"
        is_done="done"
      fi
      if [[ "${max_children}" -lt 1 ]]; then
        echo "pm.max_spare_servers(1) must not be greater than pm.max_children(${max_children})"
        is_done="done"
      fi
    fi
    if [[ ${is_done} != "done" ]]; then
      if [[ ${check_for_parameter} == *"pm.max_spare_servers ="* ]]; then
        sed -i "/pm.max_spare_servers =/c\pm.max_spare_servers = 1" ${ini_file}
      else
        echo "pm.max_spare_servers = 1" >>${ini_file}
      fi
      echo "pm.max_spare_servers = 1 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      current_pm=$(gridpane::conf_read pm ${env_type} ${site_or_ver})
      if [[ ${current_pm} != *"dynamic"* ]]; then
        echo "-------------------------------------------------------------"
        echo "pm.max_spare_servers is only used when pm is set to 'dynamic'"
        echo "${site_or_ver} pm is currently set to ${current_pm}..."
        echo "-------------------------------------------------------------"
      fi
      gridpane::conf_write pm.max_spare_servers "1" ${env_type} ${site_or_ver}
      gridpane::callback::app \
        "/site/site-update" \
        "--" \
        "site_url=${site_or_ver}" \
        "server_ip=${serverIP}" \
        "-s" "pm.max_spare_servers=1" \
        "silent@false"
      gridpane::notify::app \
        "PHP-FPM Notice" \
        "${site_or_ver}<br>${serverIP}<br>pm.max_spare_servers: 1" \
        "popup_and_center" \
        "fa-cog" \
        "default" \
        "${site_or_ver}"
    fi
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "site-pm-process-idle-timeout" ]] || [[ ${configure_command} == "-site-pm-process-idle-timeout" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires a setting integer value to be passed, none was... exiting..."
    else
      if [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
        echo "${configure_command} only accepts Integers which we convert to seconds - ${parameter_value} cannot be processed, exiting..."
      else
        if [[ ${check_for_parameter} == *"pm.process_idle_timeout ="* ]]; then
          sed -i "/pm.process_idle_timeout =/c\pm.process_idle_timeout = ${parameter_value}" ${ini_file}
        else
          echo "pm.process_idle_timeout = ${parameter_value}" >>${ini_file}
        fi
        echo "pm.process_idle_timeout = ${parameter_value}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
        current_pm=$(gridpane::conf_read pm ${env_type} ${site_or_ver})
        if [[ ${current_pm} != *"ondemand"* ]]; then
          echo "-------------------------------------------------------------"
          echo "pm.process_idle_timeout is only used when pm is set to 'ondemand'"
          echo "${site_or_ver} pm is currently set to ${current_pm}..."
          echo "-------------------------------------------------------------"
        fi
        gridpane::conf_write pm.process_idle_timeout ${parameter_value} ${env_type} ${site_or_ver}
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "pm.process_idle_timeout=${parameter_value}" \
          "silent@false"
        gridpane::notify::app \
          "PHP-FPM Notice" \
          "${site_or_ver}<br>${serverIP}<br>pm.process_idle_timeout: ${parameter_value} Sec" \
          "popup_and_center" \
          "fa-cog" \
          "default" \
          "${site_or_ver}"
      fi
    fi
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "site-pm-process-idle-timeout-reset" ]] || [[ ${configure_command} == "-site-pm-process-idle-timeout-reset" ]]; then
    if [[ -z "${parameter_value}" ]]; then
      echo "${configure_command} requires no setting to be passed - ${parameter_value} will be ignored..."
    fi
    if [[ ${check_for_parameter} == *"pm.process_idle_timeout ="* ]]; then
      sed -i "/pm.process_idle_timeout =/c\pm.process_idle_timeout = 10" ${ini_file}
    else
      echo "pm.process_idle_timeout = 10" >>${ini_file}
    fi
    echo "pm.process_idle_timeout = 10 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
    current_pm=$(gridpane::conf_read pm ${env_type} ${site_or_ver})
    if [[ ${current_pm} != *"ondemand"* ]]; then
      echo "-------------------------------------------------------------"
      echo "pm.process_idle_timeout is only used when pm is set to 'ondemand'"
      echo "${site_or_ver} pm is currently set to ${current_pm}..."
      echo "-------------------------------------------------------------"
    fi
    gridpane::conf_write pm.process_idle_timeout "10" ${env_type} ${site_or_ver}
    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=${site_or_ver}" \
      "server_ip=${serverIP}" \
      "-s" "pm.process_idle_timeout=10" \
      "silent@false"
      gridpane::notify::app \
        "PHP-FPM Notice" \
        "${site_or_ver}<br>${serverIP}<br>pm.process_idle_timeout: 10 Sec" \
        "popup_and_center" \
        "fa-cog" \
        "default" \
        "${site_or_ver}"
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "site-pm-reset-all" ]] || [[ ${configure_command} == "-site-pm-reset-all" ]]; then
    local cores
    cores=$(nproc --all)
    local quad_cores
    quad_cores=$((cores * 4))

    sed -i "/pm =/c\pm = dynamic" ${ini_file}
    echo "pm = dynamic | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log

    sed -i "/pm.max_children =/c\pm.max_children = ${quad_cores}" ${ini_file}
    echo "pm.max_children = ${quad_cores} | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log

    sed -i "/pm.max_requests =/c\pm.max_requests = 500" ${ini_file}
    echo "pm.max_requests = 500 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log

    sed -i "/pm.start_servers =/c\pm.start_servers = 1" ${ini_file}
    echo "pm.start_servers = 1 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log

    sed -i "/pm.min_spare_servers =/c\pm.min_spare_servers = 1" ${ini_file}
    echo "pm.min_spare_servers = 1 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log

    sed -i "/pm.max_spare_servers =/c\pm.max_spare_servers = 1" ${ini_file}
    echo "pm.max_spare_servers = 1 | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log

    sed -i "/pm.process_idle_timeout =/c\pm.process_idle_timeout = 10" ${ini_file}
    echo "pm.process_idle_timeout = 10s | reset" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log

    gridpane::conf_write pm dynamic ${env_type} ${site_or_ver}
    gridpane::conf_write pm.max_children "${quad_cores}" ${env_type} ${site_or_ver}
    gridpane::conf_write pm.max_requests "500" ${env_type} ${site_or_ver}
    gridpane::conf_write pm.start_servers "1" ${env_type} ${site_or_ver}
    gridpane::conf_write pm.min_spare_servers "1" ${env_type} ${site_or_ver}
    gridpane::conf_write pm.max_spare_servers "1" ${env_type} ${site_or_ver}
    gridpane::conf_write pm.process_idle_timeout "10" ${env_type} ${site_or_ver}

    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=${site_or_ver}" \
      "server_ip=${serverIP}" \
      "pm=dynamic" \
      "-s" "pm.max_children=${quad_cores}" \
      "-s" "pm.max_requests=500" \
      "-s" "pm.start_servers=1" \
      "-s" "pm.min_spare_servers=1" \
      "-s" "pm.max_spare_servers=1" \
      "-s" "pm.process_idle_timeout=10" \
      "silent@true"
    gridpane::notify::app \
      "PHP-FPM Notice" \
      "${site_or_ver}<br>${serverIP}<br>PHP-FPM Directives reset to defaults!" \
      "popup_and_center" \
      "fa-cog" \
      "default" \
      "${site_or_ver}"
    logIt gridpane-general configure-php Continue ${site_or_ver}
  elif [[ ${configure_command} == "site-slowlog" ]] || [[ ${configure_command} == "-site-slowlog" ]]; then
    if [[ -z "${parameter_value}" ]] ||
       [[ ${parameter_value} != "true" && ${parameter_value} != "false"  ]]; then
      echo "${configure_command} requires either true|false passing, exiting..."
    else
      local default_timeout=10
      local default_depth=20
      local request_slowlog_timeout
      local request_slowlog_trace_depth
      request_slowlog_timeout="request_slowlog_timeout = ${default_timeout}"
      request_slowlog_trace_depth="request_slowlog_trace_depth = ${default_depth}"
      if [[ ${parameter_value} == "false" ]]; then
        request_slowlog_timeout=";${request_slowlog_timeout}"
        request_slowlog_trace_depth=";${request_slowlog_trace_depth}"
      fi
      if [[ ${parameter_value} == "true" ]]; then
        if [[ ${check_for_parameter} == *"\$pool.log.slow"* ]]; then
          sed -i "/\$pool.log.slow/c\slowlog = \/var\/log\/\$pool.log.slow" ${ini_file}
        else
          echo "slowlog = /var/log/\$pool.log.slow" >>${ini_file}
        fi
      elif [[ ${parameter_value} == "false" ]]; then
        if [[ ${check_for_parameter} == *"\$pool.log.slow"* ]]; then
          sed -i "/\$pool.log.slow/c\;slowlog = \/var\/log\/\$pool.log.slow" ${ini_file}
        else
          echo ";slowlog = /var/log/\$pool.log.slow" >>${ini_file}
        fi
      fi
      if [[ ${check_for_parameter} == *"request_slowlog_timeout ="* ]]; then
        sed -i "/request_slowlog_timeout =/c\\${request_slowlog_timeout}" ${ini_file}
      else
        echo "${request_slowlog_timeout}" >>${ini_file}
      fi
      if [[ ${check_for_parameter} == *"request_slowlog_trace_depth ="* ]]; then
        sed -i "/request_slowlog_trace_depth =/c\\${request_slowlog_trace_depth}" ${ini_file}
      else
        echo "${request_slowlog_trace_depth}" >>${ini_file}
      fi
      echo "${request_slowlog_timeout}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${request_slowlog_trace_depth}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      gridpane::conf_write php_slowlog ${parameter_value} ${env_type} ${site_or_ver}
      gridpane::conf_write php_slowlog_timeout ${default_timeout} ${env_type} ${site_or_ver}
      gridpane::conf_write php_slowlog_trace_depth ${default_depth} ${env_type} ${site_or_ver}
      gridpane::callback::app \
        "callback" \
        "/site/site-update" \
        "--" \
        "site_url=${site_or_ver}" \
        "server_ip=${serverIP}" \
        "-s" "php_slowlog_timeout=${default_timeout}" \
        "-s" "php_slowlog_trace_depth=${default_depth}" \
        "is_php_slowlog@${parameter_value}" \
        "silent@false"
      local notification_body
      if [[ ${parameter_value} == "false" ]]; then
        notification_body="${site_or_ver}<br>${serverIP}<br>;php_slowlog_timeout<br>;php_slowlog_trace_depth<br>PHP-FPM Slowlog disabled"
      else
        notification_body="${site_or_ver}<br>${serverIP}<br>php_slowlog_timeout: ${default_timeout} Sec<br>php_slowlog_trace_depth: ${default_depth}<br>PHP-FPM Slowlog enabled"
      fi
      gridpane::notify::app \
        "PHP-FPM Notice" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-cog" \
        "default" \
        "${site_or_ver}"
    fi
  elif [[ ${configure_command} == "site-slowlog-timeout" ]] || [[ ${configure_command} == "-site-slowlog-timeout" ]]; then
    if [[ -z "${parameter_value}" ]] ||
       [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
      echo "${configure_command} requires an integer to be passed for seconds, exiting..."
    else
      local request_slowlog_timeout
      request_slowlog_timeout="request_slowlog_timeout = ${parameter_value}"
      [[ "${parameter_value}" == "0" ]] && request_slowlog_timeout=";${request_slowlog_timeout}"
      if [[ ${check_for_parameter} == *"request_slowlog_timeout ="* ]]; then
        sed -i "/request_slowlog_timeout =/c\\${request_slowlog_timeout}" ${ini_file}
      else
        echo "${request_slowlog_timeout}" >>${ini_file}
      fi
      echo "${request_slowlog_timeout}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      gridpane::conf_write php_slowlog_timeout ${parameter_value} ${env_type} ${site_or_ver}
      if [[ "${parameter_value}" == "0" ]]; then
        gridpane::conf_write php_slowlog false ${env_type} ${site_or_ver}
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "php_slowlog_timeout=${parameter_value}" \
          "is_php_slowlog@false" \
          "silent@false"
        gridpane::notify::app \
          "PHP-FPM Notice" \
          "${site_or_ver}<br>${serverIP}<br>php_slowlog_timeout: 0 Sec<br>PHP-FPM Slowlog disabled" \
          "popup_and_center" \
          "fa-cog" \
          "default" \
          "${site_or_ver}"
      else
        gridpane::callback::app \
          "/site/site-update" \
          "--" \
          "site_url=${site_or_ver}" \
          "server_ip=${serverIP}" \
          "-s" "php_slowlog_timeout=${parameter_value}" \
          "silent@false"
        gridpane::notify::app \
          "PHP-FPM Notice" \
          "${site_or_ver}<br>${serverIP}<br>php_slowlog_timeout: ${parameter_value} Sec" \
          "popup_and_center" \
          "fa-cog" \
          "default" \
          "${site_or_ver}"
      fi
    fi
  elif [[ ${configure_command} == "site-slowlog-trace-depth" ]] || [[ ${configure_command} == "-site-slowlog-trace-depth" ]]; then
    if [[ -z "${parameter_value}" ]] ||
       [[ ! "${parameter_value}" =~ ^[0-9]+$ ]]; then
      echo "${configure_command} requires an integer to be passed for depth, exiting..."
    else
      local request_slowlog_trace_depth
      request_slowlog_trace_depth="request_slowlog_trace_depth = ${parameter_value}"
      if [[ ${check_for_parameter} == *"request_slowlog_trace_depth ="* ]]; then
        sed -i "/request_slowlog_trace_depth =/c\\${request_slowlog_trace_depth}" ${ini_file}
      else
        echo "${request_slowlog_trace_depth}" >>${ini_file}
      fi
      echo "${request_slowlog_trace_depth}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      echo "${ini_file}" | tee -a /var/www/${site_or_ver}/logs/gridpane-general.log
      gridpane::conf_write php_slowlog_trace_depth ${parameter_value} ${env_type} ${site_or_ver}
      gridpane::callback::app \
        "/site/site-update" \
        "--" \
        "site_url=${site_or_ver}" \
        "server_ip=${serverIP}" \
        "-s" "php_slowlog_trace_depth=${parameter_value}" \
        "silent@false"
      gridpane::notify::app \
        "PHP-FPM Notice" \
        "${site_or_ver}<br>${serverIP}<br>php_slowlog_trace_depth: ${parameter_value}" \
        "popup_and_center" \
        "fa-cog" \
        "default" \
        "${site_or_ver}"
    fi
  ## TODO (JEFF) Need notifications confirming settings to feedback for lsapi setting changes...
  elif [[ "$configure_command" == "site-lsapi" || "$configure_command" == "-site-lsapi" ]]; then
    echo "lsapi = ${parameter_value}" | tee -a /var/www/"$site_or_ver"/logs/php.log
    echo "$ini_file" | tee -a /var/www/"$site_or_ver"/logs/php.log
    gridpane::conf_write lsapi "$parameter_value" "$env_type" "$site_or_ver"

    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=$site_or_ver" \
      "server_ip=$serverIP" \
      "-s" "lsapi=$parameter_value" \
      "silent@true" >>/var/www/"$site_or_ver"/logs/php.log
      logIt gridpane-general configure-php Continue "$site_or_ver"
  elif [[ "$configure_command" == "site-lsapi-children" || "$configure_command" == "-site-lsapi-children" ]]; then
    echo "lsapi_children = ${parameter_value}" | tee -a /var/www/"$site_or_ver"/logs/php.log
    echo "$ini_file" | tee -a /var/www/"$site_or_ver"/logs/php.log
    gridpane::conf_write lsapi_children "$parameter_value" "$env_type" "$site_or_ver"

    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=$site_or_ver" \
      "server_ip=$serverIP" \
      "-s" "lsapi_children=$parameter_value" \
      "silent@true" >>/var/www/"$site_or_ver"/logs/php.log
      logIt gridpane-general configure-php Continue "$site_or_ver"
  elif [[ "$configure_command" == "site-lsapi-app-instances" || "$configure_command" == "-site-lsapi-app-instances" ]]; then
    echo "lsapi_app_instances = ${parameter_value}" | tee -a /var/www/"$site_or_ver"/logs/php.log
    echo "$ini_file" | tee -a /var/www/"$site_or_ver"/logs/php.log
    gridpane::conf_write lsapi_app_instances "$parameter_value" "$env_type" "$site_or_ver"

    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=$site_or_ver" \
      "server_ip=$serverIP" \
      "-s" "lsapi_app_instances=$parameter_value" \
      "silent@true" >>/var/www/"$site_or_ver"/logs/php.log
      logIt gridpane-general configure-php Continue "$site_or_ver"
  elif [[ "$configure_command" == "site-lsapi-max-connections" ||
          "$configure_command" == "-site-lsapi-max-connections" ||
          "$configure_command" == "site-lsapi-app-max-connections" ||
          "$configure_command" == "-site-lsapi-app-max-connections" ]]; then
    echo "lsapi_max_connections = ${parameter_value}" | tee -a /var/www/"$site_or_ver"/logs/php.log
    echo "$ini_file" | tee -a /var/www/"$site_or_ver"/logs/php.log
    gridpane::conf_write lsapi_max_connections "$parameter_value" "$env_type" "$site_or_ver"

    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=$site_or_ver" \
      "server_ip=$serverIP" \
      "-s" "lsapi_max_connections=$parameter_value" \
      "silent@true" >>/var/www/"$site_or_ver"/logs/php.log
      logIt gridpane-general configure-php Continue "$site_or_ver"
  elif [[ "$configure_command" == "site-lsapi-initial-request-timeout" || "$configure_command" == "-site-lsapi-initial-request-timeout" ]]; then
    echo "lsapi_initial_request_timeout = ${parameter_value}" | tee -a /var/www/"$site_or_ver"/logs/php.log
    echo "$ini_file" | tee -a /var/www/"$site_or_ver"/logs/php.log
    gridpane::conf_write lsapi_initial_request_timeout "$parameter_value" "$env_type" "$site_or_ver"

    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=$site_or_ver" \
      "server_ip=$serverIP" \
      "-s" "lsapi_initial_request_timeout=$parameter_value" \
      "silent@true" >>/var/www/"$site_or_ver"/logs/php.log
      logIt gridpane-general configure-php Continue "$site_or_ver"
  elif [[ "$configure_command" == "site-lsapi-retry-timeout" || "$configure_command" == "-site-lsapi-retry-timeout" ]]; then
    echo "lsapi_retry_timeout = ${parameter_value}" | tee -a /var/www/"$site_or_ver"/logs/php.log
    echo "$ini_file" | tee -a /var/www/"$site_or_ver"/logs/php.log
    gridpane::conf_write lsapi_retry_timeout "$parameter_value" "$env_type" "$site_or_ver"

    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=$site_or_ver" \
      "server_ip=$serverIP" \
      "-s" "lsapi_retry_timeout=$parameter_value" \
      "silent@true" >>/var/www/"$site_or_ver"/logs/php.log
      logIt gridpane-general configure-php Continue "$site_or_ver"
  elif [[ "$configure_command" == "site-lsapi-max-reqs" || "$configure_command" == "-site-lsapi-max-reqs" ]]; then
    echo "lsapi_max_reqs = ${parameter_value}" | tee -a /var/www/"$site_or_ver"/logs/php.log
    echo "$ini_file" | tee -a /var/www/"$site_or_ver"/logs/php.log
    gridpane::conf_write lsapi_max_reqs "$parameter_value" "$env_type" "$site_or_ver"

    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=$site_or_ver" \
      "server_ip=$serverIP" \
      "-s" "lsapi_max_reqs=$parameter_value" \
      "silent@true" >>/var/www/"$site_or_ver"/logs/php.log
      logIt gridpane-general configure-php Continue "$site_or_ver"
  elif [[ "$configure_command" == "site-lsapi-max-idle" || "$configure_command" == "-site-lsapi-max-idle" ]]; then
    echo "lsapi_max_idle = ${parameter_value}" | tee -a /var/www/"$site_or_ver"/logs/php.log
    echo "$ini_file" | tee -a /var/www/"$site_or_ver"/logs/php.log
    gridpane::conf_write lsapi_max_idle "$parameter_value" "$env_type" "$site_or_ver"

    gridpane::callback::app \
      "/site/site-update" \
      "--" \
      "site_url=$site_or_ver" \
      "server_ip=$serverIP" \
      "-s" "lsapi_max_idle=$parameter_value" \
      "silent@true" >>/var/www/"$site_or_ver"/logs/php.log
      logIt gridpane-general configure-php Continue "$site_or_ver"
  else
    echo "Unrecognized configuration parameter passed - ${configure_command} - exiting..."
    exit 187
  fi

  if [[ "$*" != *"no-reload"* ]]; then
    if [[ "$webserver" = nginx ]]; then
      /usr/local/bin/gp php ${php_ver} reload
    else
      /usr/local/bin/gpols site "$site_to_configure_user_ini"
    fi
  fi

  if [[ ${configure_command} == *"site-pm"* ||
        ${configure_command} == *"site-slowlog"* ]]; then
    logIt gridpane-general configure-php Continue ${site_or_ver}
  fi
}