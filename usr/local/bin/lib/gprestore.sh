#!/bin/bash
#
#

gprestore::getuniqueidentifier() {
  local site_to_restore="$1"
  local backup_identifier_string="$2"
  local backup_identifier_array=()
  local backup_identifier_array_len
  local backup_identifier
  local list_of_backups_array=()

  while read -r line; do
    IFS="-"
    backup_identifier_array+=(${line})
  done <<< "${backup_identifier_string}"
  unset IFS

  backup_identifier_array_len="${#backup_identifier_array[@]}"

  if [[ "${backup_identifier_array[0]}" == "MANUAL"* && ${backup_identifier_array_len} -eq 8  ]]; then
    # grab our unique manual backup identifier in the 8th element
    backup_identifier="${backup_identifier_array[7]}"
  fi

  echo ${backup_identifier}

}

gprestore::getidentifierrevision() {
  local site_to_restore="$1"
  local backup_identifier_string="$2"
  local remote_storage_service="$3"
  #local backup_unique_identifier="$(gprestore::getuniqueidentifier ${site_to_restore} ${backup_identifier_string})"
  local backup_unique_identifier
  local backup_revision
  local list_of_backups_array=()
  local counter=0

  # strip prepended AUTO-/MANUAL- leaving behind only a date.
  backup_unique_identifier=${backup_identifier_string#"AUTO-"}
  backup_unique_identifier=${backup_unique_identifier#"MANUAL-"}

  if [[ -z ${backup_unique_identifier} ]]; then
    return 1
  fi

  cd /var/www/${site_to_restore}/htdocs

  IFS=$'\n'
  if [[ -z ${remote_storage_service} || "${remote_storage_service}" == "local" ]]; then
    list_of_backups_array=($(/usr/local/bin/duplicacy list))
  else
    list_of_backups_array=($(/usr/local/bin/duplicacy list -storage ${remote_storage_service}))
  fi
  unset IFS

  # re add space back to identifier
  backup_unique_identifier="$(echo ${backup_unique_identifier} | sed 's/./\ /11')"

  for line in "${list_of_backups_array[@]}"; do
    if [[ "$line" == *"${backup_unique_identifier}"* ]]; then
      echo "${line}" | grep -o "revision\ [0-9]\+" | awk '{print $2}'
    fi
  done

}

#######################################
#
#  Run restore labeled $backup_indentifier_string
#  for $site_to_restore on #remote_storage_service.
#
#  Inputs:
#   1: site_to_restore (string)
#   2: backup_identifier_string (string)
#   3: remote_storage_service (string; optional)
#
#  Outputs:
#   Writes status to STDOUT
#
#
#######################################
gpbup::restore() {

    local site_to_restore="$1"
    local backup_identifier_string="$2"
    local remote_storage_service="$3"
    local site_to_restore_stripped=$(echo ${site_to_restore/\-remote/})

    export DUPLICACY_PASSWORD="${UUID}"

    cd /var/www/${site_to_restore}/htdocs

    if [[ "${backup_identifier_string}" == *".tar.gz" ]]; then
      cd "/var/www/${site_to_restore}"
      if [[ -f "${backup_identifier_string}" ]]; then
        gunzip -vt "${backup_identifier_string}" || \
          { echo "There was an issue when verifying the archive." && exit 1; }
        echo "Decompressing archive ${backup_identifier_string}..."
        tar -zxvf "${backup_identifier_string}" >/dev/null || \
          { echo "There was an issue decompressing the archive." && exit 1; }
      else
        echo "Backup file specified does not exist (${backup_identifier_string})..."
        exit 1
      fi
      cd -
      return 0
    fi

    if [[ "${backup_identifier_string}" != "GP-"* ]]; then
      revision_to_restore="$(gprestore::getidentifierrevision ${site_to_restore} ${backup_identifier_string} ${remote_storage_service})"
    else
      revision_to_restore="${backup_identifier_string#GP-}"
    fi

    echo "Restoring site ${site_to_restore} using revision ${revision_to_restore}." | updatesafely::showandlogoutputindented ${LOG_FILE}
    if [[ -z ${remote_storage_service} || "${remote_storage_service}" == "local" ]]; then
      duplicacy restore -r ${revision_to_restore} -overwrite | updatesafely::showandlogoutputindentednocolor ${LOG_FILE}
    else
      duplicacy restore -storage ${remote_storage_service} -r ${revision_to_restore} -overwrite -delete | updatesafely::showandlogoutputindentednocolor ${LOG_FILE}
      if [[ ${site_to_restore} == *"-remote" ]]; then
        rsync -avzh /var/www/${site_to_restore_stripped}/wp-config.php /var/www/${site_to_restore}/wp-config.php
        rsync -avzh /var/www/${site_to_restore}/htdocs/ /var/www/${site_to_restore_stripped}/htdocs
      fi
    fi

}