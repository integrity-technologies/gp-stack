#!/bin/bash
#

gpmonitor::process() {

  local service="$1"
  local aspect="$2"
  local title
  local details
  local notification_icon
  local notification_duration

  case "${service}" in
  FAIL2BAN)
    case "${aspect}" in
    FAILED)
      title="${service} Service Failure"
      details="The ${service} service has failed on ${host} ${serverIP}, and Monit is unable to restart it successfully, please contact support."
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      ;;
    LOG)
      title="${service} Error|Warning Logged"
      details="The ${service} service has failed on ${host} ${serverIP} has logged an Error or Warning...."
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    esac
    ;;
  FILESYSTEM)
    case "${aspect}" in
    90)
      title="${service} Capacity Warning > 90%"
      details="The Filesystem on ${host} ${serverIP} has surpassed 90% disk capacity. Please consider upgrading your disk space."
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    95)
      title="${service} Capacity Warning > 95%"
      details="The Filesystem on ${host} ${serverIP} has surpassed 95% disk capacity. Really, it would be a great idea to scale your disk up now..."
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    98)
      title="${service} Capacity Warning > 98%"
      details="The Filesystem on ${host} ${serverIP} has surpassed 98% disk capacity. This is bad, things are about to go south... please scale up before it is too late..."
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    99)
      title="${service} Capacity Warning > 99%"
      details="The Filesystem on ${host} ${serverIP} has surpassed 99% disk capacity. Okay, this is where things are going to cascade fail... so long and thanks for all the fish!!!"
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      ;;
    esac
    ;;
  MYSQL)
    case "${aspect}" in
    CPU_HOT)
      title="${service} ${aspect} Warning"
      details="${service} on ${host} ${serverIP} has been using 70% of CPU for over 20 minutes... Things are running hot!"
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    CPU_RESTART)
      title="${service} ${aspect} Error"
      details="${service} on ${host} ${serverIP} was using 90% of CPU for over 30 minutes... Things were too hot, we have restarted ${service}!"
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      ;;
    MEM_HIGH)
      title="${service} ${aspect} Warning"
      details="${service} on ${host} ${serverIP} has been using a lot of RAM for at least the last 10 minutes... it is exceeding the Monit allowed threshold!"
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    MEM_RESTART)
      title="${service} ${aspect} Error"
      details="${service} on ${host} ${serverIP} exceeded the Monit RAM threshold for the service for over 30 minutes. We have restarted ${service} to protect system integrity."
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      ;;
    FAILED)
      title="${service} Service ${aspect} Error"
      details="${service} on ${host} ${serverIP} has failed and Monit is unable to restart it successfully... please contact support."
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      ;;
    esac
    ;;
  NGINX)
    case "${aspect}" in
    CPU_HOT)
      title="${service} ${aspect} Warning"
      details="${service} on ${host} ${serverIP} has been using 50% of CPU for over 20 minutes... Things are running a bit hot!"
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    CPU_RESTART)
      title="${service} ${aspect} Error"
      details="${service} on ${host} ${serverIP} was using 60% of CPU for over 10 minutes... Things were too hot, we have restarted ${service}!"
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      ;;
    MEM_HIGH)
      title="${service} ${aspect} Warning"
      details="Nginx on ${host} ${serverIP} has been using a lot of RAM for at least the last 10 minutes..."
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    FAILED)
      title="${service} Service ${aspect} Error"
      details="${service} on ${host} ${serverIP} has failed and Monit is unable to restart it successfully... please contact support."
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      ;;
    CONF_SYNTAX)
      title="${service} Service ${aspect} Error"
      details="Your ${service} Configuration File on ${host} ${serverIP} has a syntax error... please check your server and/or contact support."
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      ;;
    esac
    ;;
  PHP*)
    case "${aspect}" in
    CPU_HOT)
      title="${service} ${aspect} Warning"
      details="${service} on ${host} ${serverIP} has been using 70% of CPU for over 20 minutes... Things are running a bit hot!"
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    FAILED)
      title="${service} ${aspect} Error"
      details="${service} on ${host} ${serverIP} has failed and Monit is unable to restart it successfully... please contact support."
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      ;;
    esac
    ;;
  REDIS)
    case "${aspect}" in
    CPU_HOT)
      title="${service} ${aspect} Warning"
      details="${service} on ${host} ${serverIP} has been using 60% of CPU for over 20 minutes... Things are running hot!"
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    CPU_RESTART)
      title="${service} ${aspect} Error"
      details="${service} on ${host} ${serverIP} was using 90% of CPU for over 10 minutes... Things were too hot, we have restarted ${service}!"
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      ;;
    MEM_HIGH)
      title="${service} ${aspect} Warning"
      details="${service} on ${host} ${serverIP} has been using a lot of RAM for at least the last 10 minutes... it is exceeding the Monit allowed threshold!"
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    FAILED)
      title="${service} Service ${aspect} Error"
      details="${service} on ${host} ${serverIP} has failed and Monit is unable to restart it successfully... please contact support."
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      ;;
    esac
    ;;
  SYS_LOAD_AVG_15)
    case "${aspect}" in
    70)
      duration="1 hour"
      float="0.7"
      ;;
    100)
      duration="30 minutes"
      float="1.0"
      ;;
    200)
      duration="10 minutes"
      float="2.0"
      ;;
    esac
    title="${service} ${aspect}% CPU Warning"
    details="${host} ${serverIP} 15 Minute Load average has been running at over ${aspect}% (${float}) per core for over ${duration}"
    notification_icon="fa-exclamation"
    notification_duration="long"
    ;;
  SYS_MEM_USAGE)
    case "${aspect}" in
    70) duration="1 hour" ;;
    80) duration="30 minutes" ;;
    90) duration="10 minutes" ;;
    esac
    title="${service} ${aspect}% RAM Warning"
    details="${host} ${serverIP} System Memory utilisation has exceeded ${aspect}% RAM for over ${duration}"
    notification_icon="fa-exclamation"
    notification_duration="long"
    ;;
  SYS_CPU_USAGE)
    title="${service} ${4} ${aspect}% CPU Warning"
    details="${host} ${serverIP} ${4} CPU utilisation has exceeded ${aspect}%"
    notification_icon="fa-exclamation"
    notification_duration="long"
    ;;
  SYS_SWAP_MEM)
    title="${service} ${aspect}% Usage Warning"
    details="${host} ${serverIP} System Swap Memory usage has exceeded ${aspect}% of allocation!"
    notification_icon="fa-exclamation"
    notification_duration="long"
    ;;
  esac

  gridpane::notify::app \
    "${title}" \
    "${details}" \
    "popup_and_center" \
    "${notification_icon}" \
    "${notification_duration}" \
    "NULL"

  gridpane::notify::slack \
    "${slack_type}" \
    "${title}" \
    "${details}"

}
