#!/bin/bash
#
#
#

#. gridpane.sh

#######################################
# Check PHP versions against Gridpane supported
#
# Outputs:
#  Writes status to STDOUT
#######################################
installphp::process::version() {

  gridpane::displayfunc

  local ver="$1"

  if [[ ! -f /opt/gridpane/default_extra_php ]]; then
    cat >/opt/gridpane/default_extra_php <<EOF
7.1
7.2
7.3
7.4
EOF
  fi

  if [[ ${ver} == "7.1" ]] || [[ ${ver} == "7.3" ]] || [[ ${ver} == "7.2" ]] || [[ ${ver} == "7.4" ]] ||
    [[ $(cat /opt/gridpane/default_extra_php) == *"${ver}"* ]]; then
    echo "Installing PHP version ${ver}..."
  else
    echo "unsupported PHP version.... exiting"
    exit 187
  fi
}

#######################################
# install PHP versions
#
# Outputs:
#  Writes status to STDOUT
#######################################
installphp::install::version() {

  gridpane::displayfunc

  local ver="$1"

  echo "############################################################ Installing PHP ${ver} and extensions" |& tee -a /opt/gridpane/logs/php-build.log

  gridpane::preinstallaptcheck |& tee -a /opt/gridpane/logs/php-build.log

  sudo apt-get -y install \
    php${ver} php${ver}-common php${ver}-cli php${ver}-fpm php${ver}-curl \
    php${ver}-gd php${ver}-imap php${ver}-readline php${ver}-mysql php${ver}-sqlite3 \
    php${ver}-mbstring php${ver}-bcmath php${ver}-mysql php${ver}-opcache php${ver}-zip \
    php${ver}-xml php${ver}-soap php${ver}-gmp php${ver}-intl php${ver}-xmlrpc php${ver}-tidy \
    php${ver}-imagick graphviz php-pear php-msgpack |& tee -a /opt/gridpane/logs/php-build.log /opt/gridpane/logs/${nonce}-check-php-install.log

  local check_php_install=$(cat /opt/gridpane/logs/${nonce}-check-php-install.log)

  rm /opt/gridpane/logs/${nonce}-check-php-install.log

  if [[ ${check_php_install} == *"Could not get lock"* ]] || [[ ${check_php_install} == *"Resource temporarily unavailable"* ]]; then

    gridpane::breakaptlock |& tee -a /opt/gridpane/logs/php-build.log
    sudo apt-get -y install \
      php${ver} php${ver}-common php${ver}-cli php${ver}-fpm php${ver}-curl \
      php${ver}-gd php${ver}-imap php${ver}-readline php${ver}-mysql php${ver}-sqlite3 \
      php${ver}-mbstring php${ver}-bcmath php${ver}-mysql php${ver}-opcache php${ver}-zip \
      php${ver}-xml php${ver}-soap php${ver}-gmp php${ver}-intl php${ver}-xmlrpc php${ver}-tidy \
      php${ver}-imagick graphviz php-pear php-msgpack |& tee -a /opt/gridpane/logs/php-build.log

  fi

  if [[ ${ver} != "7.4" ]]; then

    echo "############################################################ Installing Recode on non PHP ${ver} versions" |& tee -a /opt/gridpane/logs/php-build.log

    sudo apt-get -y install php${ver}-recode |& tee -a /opt/gridpane/logs/php-build.log /opt/gridpane/logs/${nonce}-check-php-install.log
    check_php_install=$(cat /opt/gridpane/logs/${nonce}-check-php-install.log)
    rm /opt/gridpane/logs/${nonce}-check-php-install.log

    if [[ ${check_php_install} == *"Could not get lock"* ]] || [[ ${check_php_install} == *"Resource temporarily unavailable"* ]]; then
      gridpane::breakaptlock |& tee -a /opt/gridpane/logs/php-build.log
      sudo apt-get -y install php${ver}-recode |& tee -a /opt/gridpane/logs/php-build.log
    fi
  fi

  if [[ ${ver} == "7.1" ]]; then

    echo "############################################################ Installing Mcrypt on PHP ${ver} versions" |& tee -a /opt/gridpane/logs/php-build.log

    sudo apt-get -y install php${ver}-mcrypt |& tee -a /opt/gridpane/logs/php-build.log /opt/gridpane/logs/${nonce}-check-php-install.log
    check_php_install=$(cat /opt/gridpane/logs/${nonce}-check-php-install.log)
    rm /opt/gridpane/logs/${nonce}-check-php-install.log

    if [[ ${check_php_install} == *"Could not get lock"* ]] || [[ ${check_php_install} == *"Resource temporarily unavailable"* ]]; then
      gridpane::breakaptlock |& tee -a /opt/gridpane/logs/php-build.log
      sudo apt-get -y install php${ver}-mcrypt |& tee -a /opt/gridpane/logs/php-build.log
    fi
  fi

  echo "############################################################ Copying PHP ${ver} php.ini and www.conf to /opt/prometheus/templates/source/" |& tee -a /opt/gridpane/logs/php-build.log

  if [[ ! -d /opt/prometheus/templates/source/php/${ver}/fpm/pool.d ]]; then
    mkdir -p /opt/prometheus/templates/source/php/${ver}/fpm/pool.d
  fi

  sudo cp /etc/php/${ver}/fpm/php.ini /opt/prometheus/templates/source/php/${ver}/fpm
  sudo cp /etc/php/${ver}/fpm/pool.d/www.conf /opt/prometheus/templates/source/php/${ver}/fpm/pool.d

}

#######################################
# Configure PHP ini and pools
#
# Outputs:
#  Writes status to STDOUT
#######################################
installphp::optimise::version() {

  gridpane::displayfunc

  local ver="$1"

  echo "############################################################ Update PHP ini parameters" |& tee -a /opt/gridpane/logs/php-build.log

  local maxuploads="512"

  sudo sed -i '/memory_limit =/c\memory_limit = 256M' /etc/php/${ver}/fpm/php.ini
  sudo sed -i '/max_execution_time =/c\max_execution_time = 600' /etc/php/${ver}/fpm/php.ini
  sudo sed -i '/max_input_vars =/c\max_input_vars = 5000' /etc/php/${ver}/fpm/php.ini
  sudo sed -i '/expose_php =/c\expose_php = Off' /etc/php/${ver}/fpm/php.ini
  sudo sed -i "/upload_max_filesize =/c\upload_max_filesize = ${maxuploads}M" /etc/php/${ver}/fpm/php.ini
  sudo sed -i "/post_max_size =/c\post_max_size = ${maxuploads}M" /etc/php/${ver}/fpm/php.ini
  sudo sed -i '/max_file_uploads =/c\max_file_uploads = 20' /etc/php/${ver}/fpm/php.ini
  sudo sed -i '/session.cookie_httponly =/c\session.cookie_httponly = 1' /etc/php/${ver}/fpm/php.ini

  touch /root/gridenv/php${ver}.env

  echo "memory_limit:256M" >>/root/gridenv/php${ver}.env
  echo "max_execution_time:600" >>/root/gridenv/php${ver}.env
  echo "max_input_vars:5000" >>/root/gridenv/php${ver}.env
  echo "upload_max_filesize:512M" >>/root/gridenv/php${ver}.env
  echo "post_max_size:512M" >>/root/gridenv/php${ver}.env
  echo "max_file_uploads:20" >>/root/gridenv/php${ver}.env

  cat >>/etc/php/${ver}/fpm/php.ini <<EOF
opcache.enable=1
opcache.enable_cli=0
opcache.validate_timestamps=1
opcache.revalidate_freq=60
opcache.max_accelerated_files=10000
opcache.memory_consumption=64
opcache.interned_strings_buffer=8
opcache.fast_shutdown=1
EOF

  echo "opcache.enable:1" >>/root/gridenv/php${ver}.env
  echo "opcache.enable_cli:0" >>/root/gridenv/php${ver}.env
  echo "opcache.revalidate_freq:60" >>/root/gridenv/php${ver}.env
  echo "opcache.max_accelerated_files:10000" >>/root/gridenv/php${ver}.env
  echo "opcache.memory_consumption:128" >>/root/gridenv/php${ver}.env

  cp /etc/php/${ver}/fpm/php.ini /opt/gridpane/php${ver}.ini.bak

  echo "############################################################ Set PHP FPM logging" |& tee -a /opt/gridpane/logs/php-build.log

  sudo mkdir -p /var/log/php/${ver}
  sudo touch /var/log/php/${ver}/fpm.log
  sudo sed -i "/error_log =/c\error_log = /var/log/php/${ver}/fpm.log" /etc/php/${ver}/fpm/php-fpm.conf
  sudo sed -i '/log_level =/c\log_level = notice' /etc/php/${ver}/fpm/php-fpm.conf

  echo "############################################################ Set PHP www.conf and ports" |& tee -a /opt/gridpane/logs/php-build.log

  cores=$(nproc --all)
  double_cores=$((${cores} * 2))
  quad_cores=$((${cores} * 4))

  sudo sed -i '/pm =/c\pm = dynamic' /etc/php/${ver}/fpm/pool.d/www.conf
  sudo sed -i '/request_terminate_timeout =/c\;request_terminate_timeout = 900' /etc/php/${ver}/fpm/pool.d/www.conf
  sudo sed -i "/pm.max_spare_servers =/c\pm.max_spare_servers = 1" /etc/php/${ver}/fpm/pool.d/www.conf
  sudo sed -i "/pm.min_spare_servers =/c\pm.min_spare_servers = 1" /etc/php/${ver}/fpm/pool.d/www.conf
  sudo sed -i "/pm.start_servers =/c\pm.start_servers = 1" /etc/php/${ver}/fpm/pool.d/www.conf
  sudo sed -i "/pm.max_children =/c\pm.max_children = ${quad_cores}" /etc/php/${ver}/fpm/pool.d/www.conf
  sudo sed -i '/pm.max_requests =/c\pm.max_requests = 500' /etc/php/${ver}/fpm/pool.d/www.conf
  sudo sed -i '/pm.status_path =/c\pm.status_path = /status' /etc/php/${ver}/fpm/pool.d/www.conf
  sudo sed -i '/ping.path =/c\ping.path = /ping' /etc/php/${ver}/fpm/pool.d/www.conf

  echo "pm:dynamic" >>/root/gridenv/php${ver}.env
  echo "pm.max_children:${quad_cores}" >>/root/gridenv/php${ver}.env
  echo "pm.max_requests:500" >>/root/gridenv/php${ver}.env
  echo "pm.start_servers:1" >>/root/gridenv/php${ver}.env
  echo "pm.min_spare_servers:1" >>/root/gridenv/php${ver}.env
  echo "pm.max_spare_servers:1" >>/root/gridenv/php${ver}.env

  cp /etc/php/${ver}/fpm/pool.d/www.conf /opt/gridpane/php${ver}.www.conf.bak

  cat /root/gridenv/php${ver}.env | sort >/tmp/${nonce}-sorter
  cat /tmp/${nonce}-sorter >/root/gridenv/php${ver}.env
  rm /tmp/${nonce}-sorter

  cp /root/gridenv/php${ver}.env /opt/gridpane/php${ver}.env.bak
  cp /root/gridenv/php${ver}.env /opt/gridpane/env-baks/php${ver}.env.og.bak

  cat /root/gridenv/php${ver}.env |& tee -a /opt/gridpane/logs/php-build.log

  chattr +i /root/gridenv/php${ver}.env

  case ${ver} in
  7.1)
    port_no="9002"
    debug_port_no="9003"
    ;;
  7.2)
    port_no="9000"
    debug_port_no="9001"
    ;;
  7.3)
    port_no="9004"
    debug_port_no="9005"
    ;;
  7.4)
    port_no="9006"
    debug_port_no="9007"
    ;;
  esac

  sudo sed -i "/listen = /c\listen = 127.0.0.1:${port_no}" /etc/php/${ver}/fpm/pool.d/www.conf

  echo "############################################################ Setup PHP slow logging and debug.conf" |& tee -a /opt/gridpane/logs/php-build.log

  sudo touch /var/log/php/${ver}/slow.log
  sudo cp /etc/php/${ver}/fpm/pool.d/www.conf /etc/php/${ver}/fpm/pool.d/debug.conf
  sudo sed -i '/\[www\]/c\[debug]' /etc/php/${ver}/fpm/pool.d/debug.conf
  sudo sed -i '/rlimit_core =/c\rlimit_core = unlimited' /etc/php/${ver}/fpm/pool.d/debug.conf
  sudo sed -i "/slowlog =/c\slowlog = /var/log/php/${ver}/slow.log" /etc/php/${ver}/fpm/pool.d/debug.conf
  sudo sed -i '/request_slowlog_timeout =/c\request_slowlog_timeout = 10s' /etc/php/${ver}/fpm/pool.d/debug.conf

  sudo sed -i "/listen = /c\listen = 127.0.0.1:${debug_port_no}" /etc/php/${ver}/fpm/pool.d/debug.conf

  sudo echo 'php_admin_flag[xdebug.profiler_enable] = off' >>/etc/php/${ver}/fpm/pool.d/debug.conf
  sudo echo 'php_admin_flag[xdebug.profiler_enable_trigger] = on' >>/etc/php/${ver}/fpm/pool.d/debug.conf
  sudo echo 'php_admin_value[xdebug.profiler_output_name] = cachegrind.out.%p-%H-%R' >>/etc/php/${ver}/fpm/pool.d/debug.conf
  sudo echo 'php_admin_value[xdebug.profiler_output_dir] = /tmp/' >>/etc/php/${ver}/fpm/pool.d/debug.conf
  sudo sed -i '/zend_extension=/c\;zend_extension=xdebug.so' /etc/php/${ver}/mods-available/xdebug.ini

  sudo service php${ver}-fpm reload |& tee -a /opt/gridpane/logs/php-build.log
  sudo service php${ver}-fpm start |& tee -a /opt/gridpane/logs/php-build.log /opt/gridpane/provision2.log

}

#######################################
# Some verbose description here...
#
# Globals:
#   monit_restart
#######################################
installphp::addto::monit() {

  gridpane::displayfunc

  local ver="$1"
  local monitVer=${ver//./}

  #TODO (Jeff) Look at moving server system values to gridpane func
  local procNo=$(nproc)
  local seventy_percent_of_proc=$(($procNo * 70))
  local ninety_percent_of_proc=$(($procNo * 90))

  if [[ ! -f /etc/monit/conf.d/php${monitVer} ]]; then
    cat >/etc/monit/conf.d/php${monitVer} <<EOF
check process php${monitVer}-fpm with pidfile /var/run/php/php${ver}-fpm.pid
    start program = "/usr/sbin/service php${ver}-fpm start"
    stop program  = "/usr/sbin/service php${ver}-fpm stop"
    if cpu is greater than XXXSEVENTYPERCENTXXX% for 10 cycles
        then exec "/usr/local/bin/gpmonitor PHP${ver} HOT warning"
        AND repeat every 10 cycles
    if cpu > XXXNINETYPERCENTXXX% for 5 cycles
        then restart
    if 3 restarts within 5 cycles
        then exec "/usr/local/bin/gpmonitor PHP${ver} FAILED error"
EOF
  fi

  monit_php_config=$(cat /etc/monit/conf.d/php${monitVer})

  if [[ ${monit_php_config} == *"XXXSEVENTYPERCENTXXX"* ]]; then
    sed -i "s/XXXSEVENTYPERCENTXXX/${seventy_percent_of_proc}/g" /etc/monit/conf.d/php${monitVer}
    monit_restart="true"
  fi

  if [[ ${monit_php_config} == *"XXXNINETYPERCENTXXX"* ]]; then
    sed -i "s/XXXNINETYPERCENTXXX/${ninety_percent_of_proc}/g" /etc/monit/conf.d/php${monitVer}
    monit_restart="true"
  fi

  if [[ ${monit_restart} != "true" ]]; then
    monit_restart="false"
  fi

  gridpane::conf_write monit-php${ver} true
}

#######################################
# Some verbose description here...
#
# Globals:
#   monit_restart
#######################################
installphp::report::status() {

  gridpane::displayfunc

  local php_ver="$1"
  local check_status="$(/usr/sbin/service php${php_ver}-fpm status)"

  if [[ ${check_status} == *"could not be found"* ]]; then
    local title="PHP${php_ver}-fpm installation failed!"
    local body="Post installation check reported:<br>Please contact support for help. Do not attempt to use PHP. Thank you."
    local channel="popup_and_center"
    local icon="fa-exclamation"
    local duration="infinity"

    gridpane::notify::app "${title}" "${body}" "${channel}" "${icon}" "${duration}"

    body=${body//<br>/ \\n}

    gridpane::notify::slack "${serverIP}" "error" "${title}" "${body}"
  fi
}
