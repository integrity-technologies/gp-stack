#!/bin/bash

# shellcheck disable=SC2120
gpfailsync::end::failsync_pause() {\
  {
    if [[ -n $1 ]]; then
      echo "$1"
    fi
    echo "gpfailsync-server-worker-pause deleted, server tasks can resume as normal..." | tee -a /var/log/gridpane.log
    echo "******************************************************************************"
    echo "******************************************************************************"
  } | tee -a /opt/gridpane/gpclone.log
  gridpane::conf_delete gpfailsync-server-worker-pause
  gridpane::conf_write gpfailsync-running false
}

gpfailsync::check_arg_number() {
  if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
    echo "Requires at least three command line variables 1. source/failover 2. REMOTEIP 3. SSH Key or Interval" | tee -a /opt/gridpane/gpclone.log
    gpclone::end::failsync_pause::exit
  fi
}

# shellcheck disable=SC2120,SC2029,SC2044
gpfailsync::run::source_commands::stop() {
  crontab -l | grep -v "gpclone" | crontab - | tee -a /opt/gridpane/gpclone.log
  echo "Removed ${remote_ip} as a failover server from cron!" | tee -a /opt/gridpane/gpclone.log

  # shellcheck disable=SC2154
  gridpane::callback::app \
    "/server/server-update" \
    "ip=${serverIP}" \
    "high_availability_sync_interval=0"

  echo "Stopping failover, deleting synced-to/from logs..." | tee -a /opt/gridpane/gpclone.log
  for currfolder in $(find /var/www -maxdepth 1 -mindepth 1 -type d); do
    managed_domain="$(gridpane::get_managed_domain)"
    entry=$(basename "${currfolder}")
    case $entry in
    22222|default|html|*-remote|canary.*)
      echo "Skipping $entry" | tee -a /opt/gridpane/gpclone.log
      continue
      ;;
    *."${managed_domain}")
      echo "Skipping Administrative $entry (Stats/PHPMA) Site..." | tee -a /opt/gridpane/gpclone.log
      continue
      ;;
    *)
      echo "remove /var/www/${entry}/logs/synced-to.log locally" | tee -a /opt/gridpane/gpclone.log
      if [[ -f /var/www/${entry}/logs/synced-to.log ]]; then
        rm /var/www/"${entry}"/logs/synced-to.log
      fi
      echo "remove /var/www/${entry}/logs/synced-from.log on ${remote_ip}" | tee -a /opt/gridpane/gpclone.log
      ssh root@"${remote_ip}" "[[ -f /var/www/${entry}/logs/synced-from.log]] && rm /var/www/${entry}/logs/synced-from.log" | tee -a /opt/gridpane/gpclone.log
      ;;
    esac
  done

  {
    echo "Updating server conf and resetting gpfailsync..."

    gridpane::conf_write gpfailsync-running false
    gridpane::conf_delete gpfailsync-frequency
    gridpane::conf_delete gpfailsync-role
    gridpane::conf_delete gpfailsync-destination

    echo "Updating server conf and resetting gpfailsync on ${remote_ip}"
    ssh root@"${remote_ip}" "sudo /usr/local/bin/gp conf write gpfailsync-running false"
    ssh root@"${remote_ip}" "sudo /usr/local/bin/gp conf delete gpfailsync-role"
    ssh root@"${remote_ip}" "sudo /usr/local/bin/gp conf delete gpfailsync-server-worker-pause"
  } | tee -a /opt/gridpane/gpclone.log

  gpclone::end::failsync_pause::exit
}

gpfailsync::run::server_pairing() {
  local notification_body="Pairing Initiated<br>Source Server:${serverIP}<br>Destination Server:${remote_ip}<br>Initial Server SSH Pairing beginning..."
  gridpane::notify::app \
    "Sync Notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-refresh" \
    "long" \
    "NULL"
  echo "${notification_body}" | tee -a /opt/gridpane/migrate-clone.log

  if [[ -f /root/.ssh/id_rsa.pub ]]; then
    echo "Root SSH Public ID exists..." | tee -a /opt/gridpane/gpclone.log
  else
    echo "We need to create a public key pair..." | tee -a /opt/gridpane/gpclone.log
    echo "CREATING SSH!!!" | tee -a /opt/gridpane/gpclone.log
    ssh-keygen -t rsa -b 4096 -N "" -f /root/.ssh/id_rsa
  fi

  echo "Adding Remote ${remote_ip} to ~/.ssh/known_hosts" | tee -a /opt/gridpane/gpclone.log
  ssh_keyscan=$(ssh-keyscan "${remote_ip}" >>/root/.ssh/known_hosts)
  echo "${ssh_keyscan}" | tee -a /opt/gridpane/gpclone.log
  echo " " | tee -a /opt/gridpane/gpclone.log
  ssh=$(cat /root/.ssh/id_rsa.pub)
  echo "Sending Public Key to Remote:" | tee -a /opt/gridpane/gpclone.log
  echo "${ssh}" | tee -a /opt/gridpane/gpclone.log
  echo " " | tee -a /opt/gridpane/gpclone.log

  # shellcheck disable=SC2154
  curl=$(
    curl \
      -F "ssh_key=@/root/.ssh/id_rsa.pub" \
      -F "source_ip=${serverIP}" \
      -F "failover_ip=${remote_ip}" \
      https://"${GPURL}"/api/pair-servers?api_token="${gridpanetoken}" \
      2>&1
  )
  echo "${curl}" | tee -a /opt/gridpane/gpclone.log
  echo " " | tee -a /opt/gridpane/gpclone.log
}

gpfailsync::check::active_sites() {
  local check_sites
  check_sites=$(cat /root/gridenv/active-sites.env)
  if [[ $1 == *"singlesite"* &&
        ${check_sites} != *"$2"* ]]; then

    # shellcheck disable=SC2012,SC2010
    siteDirectoryArray=$(ls -l /var/www | grep -E '^d' | awk '{print $9}')

    # shellcheck disable=SC2154
    if [[ "$webserver" = nginx ]]; then
      siteConfigsArray=$(dir /etc/nginx/sites-enabled)
    else
      siteConfigsArray="$(/bin/dir /usr/local/lsws/conf/vhosts)"
    fi

    if [[ ${siteDirectoryArray} == *"$2"* &&
          ${siteConfigsArray} == *"$2"* ]]; then
      {
        echo "$2 missing from active sites env, healing..."
        gridpane::conf_write ":$2:" active -active-sites.env
      } | tee -a /opt/gridpane/gpclone.log
    else
      notification_body="$2 is not being detected as an active site on this origin server, exiting..."
      gridpane::notify::app \
        "Cloning Fail" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "${2}"
      echo "$2 is not an active site on this server... exiting..." | tee -a /opt/gridpane/gpclone.log
      gpclone::end::failsync_pause::exit
    fi
  fi
}

gpfailsync::set::site() {
  if [[ -n $1 ]]; then
    site_to_clone="$1"
    check_for_multisite=$(gridpane::check::multisite "${site_to_clone}")
    currUser=$(gridpane::conf_read sys-user -site.env "${site_to_clone}")
    if [[ -z ${currUser} ]]; then
      currUser=$(gridpane::get::set::site::user "${site_to_clone}")
    fi
  fi
}

gpfailsync::check::remote_access() {
  check_for_remote_access=$(ssh root@"${remote_ip}" "sleep 1 && ls -l" 2>&1)
  echo "Remote | ${check_for_remote_access}" | tee -a /opt/gridpane/gpclone.log

  RETRY=0
  while [[ ${check_for_remote_access} == *"denied"* ||
          ${check_for_remote_access} == *"onnection timed out"* ]]; do
    echo "-------------------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log
    echo "${check_for_remote_access}" | tee -a /opt/gridpane/gpclone.log
    echo "Waiting for remote access, retry ${RETRY}s (max wait 300s) | root@${remote_ip} " | tee -a /opt/gridpane/gpclone.log
    echo "-------------------------------------------------------------------------------" | tee -a /opt/gridpane/gpclone.log
    sleep 1

    if [[ ${RETRY} -gt 300 ]]; then
      echo "Clone/migrate failed, unable to connect to destination server ${remote_ip}." | tee -a /opt/gridpane/gpclone.log
      gridpane::notify::app \
        "Cloning Fail" \
        "Clone/migrate failed, unable to connect to destination server ${remote_ip} after 300 seconds!" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "${site_to_clone}"
      gpclone::end::failsync_pause::exit
    fi

    if [[ $((RETRY % 15)) -eq 0 ]]; then
      echo "Clone/Migrate unable to connect to destination server ${remote_ip}... yet..." | tee -a /opt/gridpane/gpclone.log
      gridpane::notify::app \
        "Cloning/Migration Connection Notice" \
        "Connection Retry | ${RETRY}s<br>Attempting to connect to ${remote_ip}, will continue to retry for up to 300s." \
        "popup_and_center" \
        "fa-exclamation" \
        "long" \
        "${site_to_clone}"
    fi
    check_for_remote_access=$(ssh root@"${remote_ip}" 2>&1)
    RETRY=$((RETRY + 1))
  done
}

gpfailsync::check::multisite() {
  account_type="$(gridpane::get_account_type)"
  if [[ ${check_for_multisite} == "true"* ]] &&
     [[ ${account_type} != *"Developer"* &&
        ${account_type} != *"Agency"* ]]; then
    echo "${site_to_clone} is a multisite installation, GridPane cloning does not currently support this, we recommend AIO Migration Multisite Extension currently" | tee -a /opt/gridpane/gpclone.log
    gridpane::notify::app \
      "Cloning Fail" \
      "${site_to_clone} is a multisite installation, GridPane cloning does not currently support this, we recommend AIO Migration Multisite Extension currently" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    gpclone::end::failsync_pause::exit
  fi
}

gpfailsync::check::remote_user() {
  local check_for_remote_user
  # shellcheck disable=SC2029
  check_for_remote_user=$(ssh root@"${remote_ip}" "sleep 1 && sudo id -u ${currUser}" 2>&1)
  echo "${check_for_remote_user}" | tee -a /opt/gridpane/gpclone.log
  if [[ ${check_for_remote_user} == *"no such user"* ]]; then
    echo "${site_to_clone} system user ${currUser} does not exist on destination server, manual creation of user and transferring of site ownership will be required after the migration/clone completes." | tee -a /opt/gridpane/gpclone.log
    gridpane::notify::app \
      "Cloning Notice" \
      "${site_to_clone} system user ${currUser} does not exist on destination server.<br>To complete the migration/clone please create system user on destination server and transfer site ownership once this process completes." \
      "popup_and_center" \
      "fa-exclamation" \
      "long" \
      "${site_to_clone}"
  fi
}

gpfailsync::run::single_site::same_url() {
  gpfailsync::check::multisite
  #  gpfailsync::check::remote_user
  gridpane::notify::app \
    "Sync Notice" \
    "Clone Update<br>Source:${site_to_clone}<br>Destination:${site_to_clone}<br>Source Server:${serverIP}<br>Destination Server:${remote_ip}<br>SSH Pairing completed, clone migration will begin momentarily..." \
    "popup_and_center" \
    "fa-exclamation" \
    "long" \
    "${site_to_clone}"

  gpfailsync::end::failsync_pause

  echo "Running:" | tee -a /opt/gridpane/gpclone.log
  echo "/usr/local/bin/gpclone ${remote_ip} -singlesite ${site_to_clone} migration" | tee -a /opt/gridpane/gpclone.log
  /usr/local/bin/gpclone "${remote_ip}" -singlesite "${site_to_clone}" migration
}

gpfailsync::check::new_url() {
  if [[ -z $1 ]]; then
    notification_body="Missing the new site URL, exiting!"
    fail_exit="true"
  elif [[ $1 != *"."* ]]; then
    notification_body="Passed new site URL $1 is not a URL, exiting!"
    fail_exit="true"
  elif [[ $1 == "www."* ]]; then
    notification_body="Passed new site URL $1 includes the www host, exiting!"
    fail_exit="true"
  fi

  if [[ -n ${fail_exit} ]]; then
    echo "${notification_body}" | tee -a /opt/gridpane/gpclone.log
    gridpane::notify::app \
      "Cloning fail!" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    gpclone::end::failsync_pause::exit
  fi
}

gpfailsync::run::single_site::new_url() {
  gpfailsync::check::multisite
  #  gpfailsync::check::remote_user
  gpfailsync::check::new_url "$@"

  readonly new_url="$1"

  gridpane::notify::app \
    "Sync Notice" \
    "Clone/Migrate Update<br>Source:${site_to_clone}<br>Destination:${new_url}<br>Source Server:${serverIP}<br>Destination Server:${remote_ip}<br>SSH Pairing completed, clone migration will begin momentarily..." \
    "popup_and_center" \
    "fa-refresh" \
    "long" \
    "${site_to_clone}"

  gpfailsync::end::failsync_pause

  echo "Running:" | tee -a /opt/gridpane/gpclone.log
  echo "/usr/local/bin/gpclone ${remote_ip} -singlesite-new-url ${site_to_clone} ${new_url} migration" | tee -a /opt/gridpane/gpclone.log
  /usr/local/bin/gpclone "${remote_ip}" -singlesite-new-url "${site_to_clone}" "${new_url}" migration
}

gpfailsync::run::all_sites::same_url() {
  gridpane::notify::app \
    "Sync Notice" \
    "Clone/Migrate Update<br>Source Server:${serverIP}<br>Destination Server:${remote_ip}<br>SSH Pairing completed, clone migration of all sites will begin momentarily..." \
    "popup_and_center" \
    "fa-refresh" \
    "long" \
    "${site_to_clone}"

  gpfailsync::end::failsync_pause

  workers::checkinstall::dep "at"

  echo "Running:" | tee -a /opt/gridpane/gpclone.log
  echo "/usr/local/bin/gpclone ${remote_ip} migration" | tee -a /opt/gridpane/gpclone.log
  echo "/usr/local/bin/gpclone ${remote_ip} migration" | at now + 0 minute
}

gpfailsync::process::one_time_cloning() {

  echo "Doing a onetime clone/migration to remote server ${remote_ip}..." | tee -a /opt/gridpane/gpclone.log

  if [[ $1 == *"singlesite"* ]] && [[ -z $2 ]]; then
    echo "Missing the site URL, exiting!" | tee -a /opt/gridpane/gpclone.log
    gpclone::end::failsync_pause::exit
  fi

  gpfailsync::check::active_sites "$@"
  gpfailsync::set::site "$2"
  gpfailsync::check::remote_access

  if [[ $1 == "-singlesite" ]]; then
    gpfailsync::run::single_site::same_url "$@"
  elif [[ $1 == "-singlesite-new-url" ]]; then
    shift
    shift
    gpfailsync::run::single_site::new_url "$@"
  else
    gpfailsync::run::all_sites::same_url
  fi

}

gpfailsync::process::failover() {
  crontab -l >/tmp/tempcron
  if grep -q "gpclone ${remote_ip}" "/tmp/tempcron"; then
    echo "GridPane Failover Syncing is already enabled to this remote server" | tee -a /opt/gridpane/gpclone.log
  else

    if [ "$1" -lt 24 ] || [ "$1" -gt 24 ]; then
      min=$((1 + RANDOM % 59))
      echo "$min */$1 * * * /usr/local/bin/gpclone ${remote_ip}" >>/tmp/tempcron
    elif [ "$1" -eq 24 ]; then
      min=$((1 + RANDOM % 59))
      hour=$((1 + RANDOM % 23))
      echo "$min $hour * * * /usr/local/bin/gpclone ${remote_ip}" >>/tmp/tempcron
    fi
    crontab /tmp/tempcron

    gridpane::callback::app \
      "/server/server-update" \
      "ip=${serverIP}" \
      "high_availability_sync_interval=${1}"

    gridpane::conf_write gpfailsync-frequency "${1}"
  fi
  rm /tmp/tempcron

  {
    echo "---------------------------------------------------------"
    crontab -l
    echo "---------------------------------------------------------"
    gridpane::conf_write gpfailsync-role source
    gridpane::conf_write gpfailsync-destination "${remote_ip}"
  } | tee -a /opt/gridpane/gpclone.log

  workers::checkinstall::dep "at"
  echo "/usr/local/bin/gpclone ${remote_ip}" | at now + 0 minute
}

gpfailsync::run::source_commands() {

  if [[ -z $1 ]]; then
    echo "No value was passed for the remote server IP..." | tee -a /opt/gridpane/gpclone.log
    gpclone::end::failsync_pause::exit
  fi

  check_ip=$(gridpane::ip_check "$1")
  echo "${check_ip}" | tee -a /opt/gridpane/gpclone.log
  remote_ip="$1"

  shift
  if [[ "$1" == "stop" ]]; then
    gpfailsync::run::source_commands::stop
  else
    #Do the source work...
    echo "This is a source server, the remote IP is ${remote_ip} and the interval is $1" | tee -a /opt/gridpane/gpclone.log
    gpfailsync::run::server_pairing
    if [[ $1 == "once" ]]; then
      shift
      gpfailsync::process::one_time_cloning "$@"
    else
      gpfailsync::process::failover "$@"
    fi
  fi
}

gpfailsync::failover::stop() {
  rm /opt/gridpane/failover/synced-from-"$1".log
  {
    echo "gpfailsync-running:false, server tasks can resume as normal..."
    gridpane::conf_write gpfailsync-running false
    gridpane::conf_delete gpfailsync-role
    gridpane::conf_delete gpfailsync-server-worker-pause
  } | tee -a /opt/gridpane/gpclone.log
  gpfailsync::end::failsync_pause
  echo "/usr/local/bin/gpclone $1 failover-stop" | tee -a /opt/gridpane/gpclone.log
  /usr/local/bin/gpclone "$1" failover-stop
  gpclone::end::failsync_pause::exit
}

gpfailsync::failover::set_known_host() {
  local source_ip="$1"
  shift
  {
    echo "This is a failover destination server."
    echo "Source Server IP: ${source_ip}"
    echo "Source SSH key:"
    echo "$*"
  } | tee -a /opt/gridpane/gpclone.log

  if [[ "$(cat /root/.ssh/authorized_keys)" != *"$*"* ]]; then
    {
      echo " "
      echo "#GridPane Failover Sync from Source ${source_ip}"
      echo "$@"
    } >>/root/.ssh/authorized_keys
  fi

  if [[ "$(cat /root/.ssh/known_hosts)" != *"${source_ip}"* ]]; then
    ssh-keyscan "${source_ip}" >>/root/.ssh/known_hosts
  fi

  service ssh restart

  gridpane::callback::app \
    "/pair-sync-status" \
    "source_ip=${source_ip}" \
    "failover_ip=${serverIP}" \
    "status=success" | tee -a /opt/gridpane/gpclone.log

  echo "We have successfully called back to the GridPane API endpoint with Status: Success" | tee -a /opt/gridpane/gpclone.log
  touch /opt/gridpane/failover/synced-from-"${source_ip}".log

  gpclone::end::failsync_pause::exit
}

gpfailsync::run::failover_commands() {
  if [[ "$2" == "stop" ]]; then
    gpfailsync::failover::stop "$1"
  else
    gpfailsync::failover::set_known_host "$@"
  fi
}

gpfailsync::process() {
  if [[ "$1" == "source" ]]; then
    shift
    gpfailsync::run::source_commands "$@"
  elif [[ "$1" == "failover" ]]; then
    shift
    gpfailsync::run::failover_commands "$@"
  fi
}
