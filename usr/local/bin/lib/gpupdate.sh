#!/bin/bash
#

#######################################
# Clears any remnants of previous gzip fallbacks
#
#######################################
gpupdate::clearold() {

  for scripts_version in /opt/gridpane/scripts-version.txt*; do
    if [[ -f "${scripts_version}" ]]; then
      rm ${scripts_version}
    fi
  done

  for scripts_gzip in /opt/gridpane/gridpane.gz*; do
    if [[ -f "${scripts_gzip}" ]]; then
      rm ${scripts_gzip}
    fi
  done

  for configs_version in /opt/gridpane/configs-version.txt*; do
    if [[ -f "${configs_version}" ]]; then
      rm ${configs_version}
    fi
  done

  for configs_gzip in /opt/gridpane/nginx-configs/nginx-configs.gz*; do
    if [[ -f "${configs_gzip}" ]]; then
      rm ${configs_gzip}
    fi
  done
}

#######################################
# Returns value to determine if to force update
#
# Outputs:
#  Boolean true|false
#  Writes status to STDOUT
#######################################
gpupdate::determine::forced() {

  if [[ $1 == "scripts" ]]; then
    if [[ -f /root/scripts.development.test.branch ]]; then
      echo "true"
    else
      if [[ $2 == *"force"* && $2 == *"scripts"* ]]; then
        echo "true"
      else
        echo "false"
      fi
    fi
  elif [[ $1 == "configs" ]]; then
    if [[ -f /root/configs.development.test.branch ]]; then
      echo "true"
    else
      if [[ $2 == *"force"* && $2 == *"configs"* ]]; then
        echo "true"
      else
        echo "false"
      fi
    fi
  fi
}

#######################################
# returns the branch to use to update scripts
#
# Sets Globals:
#  scripts_branch
#  scripts_path_part
#  is_dev_scripts
# Outputs:
#  Writes to STDOUT
#######################################
gpupdate::branch() {

  if [[ -f /root/$1.development.test.branch ]]; then
    echo "$1 development test branch token found..."
    echo "$1 will be force updated to latest from branch:"
    if [[ $1 == "scripts" ]]; then
      scripts_branch=$(cat /root/$1.development.test.branch)
      scripts_path_part="/${scripts_branch}/"
      is_dev_scripts="$(cat /root/grid.source) "
      echo "${scripts_branch}"
    else
      configs_branch=$(cat /root/$1.development.test.branch)
      configs_path_part="/${configs_branch}/"
      is_dev_configs="$(cat /root/grid.source) "
      echo "${configs_branch}"
    fi
  else
    if [[ $1 == "scripts" ]]; then
      scripts_branch="master"
      scripts_path_part="/"
    else
      configs_branch="master"
      configs_path_part="/"
    fi
  fi
}

#######################################
# returns the version of the scripts updating to
#
# Sets Globals:
#  current_scripts_version
#######################################
gpupdate::version() {

  if [[ $1 == "scripts" ]]; then
    if [[ ${scripts_branch} == "master" ]]; then
      cd /opt/gridpane
      wget --timeout=30 https://gridpane.org${scripts_path_part}prometheus/scripts-version.txt
      if [[ ! -f /opt/gridpane/scripts-version.txt ]]; then
        wget --timeout=30 https://gridpane.net${scripts_path_part}prometheus/scripts-version.txt
      fi
      cd -
      current_scripts_version=$(awk '{print $1; exit}' /opt/gridpane/scripts-version.txt)
    else
      current_scripts_version="${scripts_branch}"
    fi
  elif [[ $1 == "configs" ]]; then
    if [[ ${configs_branch} == "master" ]]; then
      cd /opt/gridpane
      wget --timeout=30 https://gridpane.org${configs_path_part}prometheus/configs-version.txt
      if [[ ! -f /opt/gridpane/configs-version.txt ]]; then
        wget --timeout=30 https://gridpane.net${configs_path_part}prometheus/configs-version.txt
      fi
      cd -
      current_configs_version=$(awk '{print $1; exit}' /opt/gridpane/configs-version.txt)
    else
      current_configs_version="${configs_branch}"
    fi
  fi
}

#######################################
# Process globals and runs the update command if needed
#
# Outputs:
#  Writes output to STDOUT
#######################################
gpupdate::process() {

  local scripts_or_configs="$1"
  local current_version="$2"
  local local_version="$3"
  local force_update="$4"

  case ${scripts_or_configs} in
  scripts)
    is_dev="${is_dev_scripts}"
    scripts_updated="true"
    ;;
  configs)
    is_dev="${is_dev_configs}"
    configs_updated="true"
    ;;
  esac

  if [[ ${current_version} == "${local_version}" ]] && [[ ${force_update} == "false" ]]; then
    echo "You have the most current version of GridPane ${is_dev}${scripts_or_configs}, v${current_version}" | tee -a /var/log/gridpane.log
    if [[ -f /opt/gridpane/${scripts_or_configs}-version.txt ]]; then
      rm /opt/gridpane/${scripts_or_configs}-version.txt
    fi
  else
    local forcing
    if [[ ! -z ${force_update} ]]; then
      forcing="Force "
    fi
    echo "${forcing}Updating GridPane ${is_dev_scripts}${scripts_or_configs} to v${current_version}" | tee -a /var/log/gridpane.log
    if [[ -f /opt/gridpane/${scripts_or_configs}-version.txt ]]; then
      rm /opt/gridpane/${scripts_or_configs}-version.txt
    fi
    gpupdate::${scripts_or_configs} ${force_update} ${5}
  fi
}

#######################################
# Update the GridPane Scripts
#
#######################################
gpupdate::scripts() {

  cd /opt/gridpane

  [[ -f /opt/gridpane/gpupdate-postprocess ]] && rm /opt/gridpane/gpupdate-postprocess | tee -a /var/log/gridpane.log

  git clone --single-branch -b ${scripts_branch} https://gitlab+deploy-token-16:FgMHPGqfyVDLw_ixsB-Y@gitlab.gridpane.net/gp-public/server-scripts | tee -a /var/log/gridpane.log

  if [[ ! -d /opt/gridpane/server-scripts ]]; then
    wget --timeout=15 https://gridpane.org${scripts_path_part}gridpane.gz | tee -a /var/log/gridpane.log
    [[ ! -f /opt/gridpane/gridpane.gz ]] && wget --timeout=15 https://gridpane.net${scripts_path_part}gridpane.gz | tee -a /var/log/gridpane.log

    if [[ -f /opt/gridpane/gridpane.gz ]]; then
      mkdir /opt/gridpane/server-scripts | tee -a /var/log/gridpane.log
      tar -C /opt/gridpane/server-scripts -xzf /opt/gridpane/gridpane.gz | tee -a /var/log/gridpane.log
      rm gridpane.gz | tee -a /var/log/gridpane.log
    fi
  else
    for script in "/opt/gridpane/server-scripts/"*; do
      script_filename=$(basename ${script})
      case ${script_filename} in
      "apptest.sequence.txt") continue ;;
      "monit-settings") continue ;;
      "phpma.config"*) continue ;;
      "phpma.signon"*) continue ;;
      "php-timezones.env") continue ;;
      "gpsitecron") continue ;;
      "duplicacy"*) continue ;;
      "backerupper") continue ;;
      "export-gl-issues.pl") continue ;;
      "extensions.txt") continue ;;
      "lib") continue ;;
      *)
        new_script_filename=$(echo "${script_filename//.sh/}")
        mv /opt/gridpane/server-scripts/${script_filename} /opt/gridpane/server-scripts/${new_script_filename} | tee -a /var/log/gridpane.log
        ;;
      esac
    done
  fi

  for script in "/opt/gridpane/server-scripts/"*; do
    gp_script=$(basename ${script})
    case ${gp_script} in
    "monit-settings") continue ;;
    "phpma.config"*) continue ;;
    "phpma.signon"*) continue ;;
    "php-timezones.env") continue ;;
    "gpsitecron") continue ;;
    "duplicacy"*) continue ;;
    "backerupper") continue ;;
    "export-gl-issues.pl") continue ;;
    "extensions.txt") continue ;;
    "lib") continue ;;
    *)
      chmod -R u+x /opt/gridpane/server-scripts/${gp_script} &&
        mv /opt/gridpane/server-scripts/${gp_script} /usr/local/bin/${gp_script} | tee -a /var/log/gridpane.log
      ;;
    esac
  done

  [[ ! -d /usr/local/bin/lib/ ]] && mkdir -p /usr/local/bin/lib

  for script in "/opt/gridpane/server-scripts/lib/"*; do
    lib_script=$(basename ${script})
    mv /opt/gridpane/server-scripts/lib/${lib_script} /usr/local/bin/lib/${lib_script} | tee -a /var/log/gridpane.log
  done

  [[ ! -d /opt/gridpane/monit-settings ]] && mkdir -p /opt/gridpane/monit-settings
  cp -rf /opt/gridpane/server-scripts/monit-settings/* /opt/gridpane/monit-settings

  [[ ! -d /opt/gridpane/php-timezones ]] && mkdir -p /opt/gridpane/php-timezones
  [[ "$(cat /opt/gridpane/php-timezones/php-timezones.env)" != "$(cat /opt/gridpane/server-scripts/php-timezones.env)" ]] &&
    cp /opt/gridpane/server-scripts/php-timezones.env /opt/gridpane/php-timezones/php-timezones.env

  [[ ! -d /opt/gridpane/phpma ]] && mkdir -p /opt/gridpane/phpma
  [[ "$(cat /opt/gridpane/phpma/phpma.config.inc.php)" != "$(cat /opt/gridpane/server-scripts/phpma.config.inc.php)" ]] &&
    cp /opt/gridpane/server-scripts/phpma.config.inc.php /opt/gridpane/phpma/phpma.config.inc.php
  [[ "$(cat /opt/gridpane/phpma/phpma.signon.php)" != "$(cat /opt/gridpane/server-scripts/phpma.signon.php)" ]] &&
    cp /opt/gridpane/server-scripts/phpma.signon.php /opt/gridpane/phpma/phpma.signon.php

  [[ ! -d /opt/gridpane/duplicacy-remote-configs/ ]] && mkdir -p /opt/gridpane/duplicacy-remote-configs/
  cp /opt/gridpane/server-scripts/duplicacy_* /opt/gridpane/duplicacy-remote-configs/

  rm -rf /opt/gridpane/server-scripts | tee -a /var/log/gridpane.log
  chmod -R u+x /usr/local/bin | tee -a /var/log/gridpane.log

  gridpane::conf_write scripts-version "${current_scripts_version}"
  gridpane::conf_write script-update-processing complete
}

#######################################
# Update the Nginx Configs
#
#######################################
gpupdate::configs() {

  cd /opt/gridpane

  [[ -d /opt/gridpane/nginx-configs ]] && rm -rf /opt/gridpane/nginx-configs | tee -a /var/log/gridpane.log

  git clone --single-branch -b ${configs_branch} https://gitlab+deploy-token-14:C4n3u13NWyyNKzTbtHXt@gitlab.gridpane.net/gp-public/nginx-configs | tee -a /var/log/gridpane.log

  if [[ ! -d /opt/gridpane/nginx-configs ]]; then
    wget --timeout=30 https://gridpane.org${configs_path_part}nginx-configs.gz | tee -a /var/log/gridpane.log
    [[ ! -f /opt/gridpane/nginx-configs.gz ]] && wget --timeout=30 https://gridpane.net${configs_path_part}nginx-configs.gz | tee -a /var/log/gridpane.log

    if [[ -f /opt/gridpane/nginx-configs.gz ]]; then
      mkdir /opt/gridpane/nginx-configs | tee -a /var/log/gridpane.log
      tar -C /opt/gridpane/nginx-configs -xzf /opt/gridpane/nginx-configs.gz | tee -a /var/log/gridpane.log
      rm /opt/gridpane/nginx-configs.gz | tee -a /var/log/gridpane.log
    fi
  fi

  [[ ! -d /etc/nginx/extra.d/_custom ]] && mkdir -p /etc/nginx/extra.d/_custom | tee -a /var/log/gridpane.log
  [[ ! -d /etc/nginx/extra.d/_default ]] && mkdir -p /etc/nginx/extra.d/_default | tee -a /var/log/gridpane.log

  for extrad_config_file_in_update in $(dir /opt/gridpane/nginx-configs/extra.d); do

    extrad_config_filename_in_update=$(basename ${extrad_config_file_in_update})
    [[ ! -f /etc/nginx/extra.d/_custom/${extrad_config_filename_in_update} ]] && continue

    if [[ $1 == *"force"* && "$1" == *"${extrad_config_filename_in_update}"* ]] || [[ $1 == *"force"* && "$2" == *"all"* ]]; then
      if [[ -f /etc/nginx/extra.d/_custom/${extrad_config_filename_in_update} ]]; then
        echo "force updating extra.d config file... /etc/nginx/extra.d/${extrad_config_filename_in_update}"
        rm /etc/nginx/extra.d/_custom/${extrad_config_filename_in_update}
      fi

      [[ -f /etc/nginx/extra.d/_default/${extrad_config_filename_in_update} ]] && rm /etc/nginx/extra.d/_default/${extrad_config_filename_in_update}
    else
      mv /opt/gridpane/nginx-configs/extra.d/${extrad_config_filename_in_update} /etc/nginx/extra.d/_default/${extrad_config_filename_in_update}
      cp /etc/nginx/extra.d/_custom/${extrad_config_filename_in_update} /etc/nginx/extra.d/${extrad_config_filename_in_update}
    fi

  done

  [[ ! -d /etc/nginx/common/_custom ]] && mkdir -p /etc/nginx/common/_custom | tee -a /var/log/gridpane.log
  [[ ! -d /etc/nginx/common/_default ]] && mkdir -p /etc/nginx/common/_default | tee -a /var/log/gridpane.log

  for common_config_file_in_update in $(dir /opt/gridpane/nginx-configs/common); do
    common_config_filename_in_update=$(basename ${common_config_file_in_update})

    [[ ! -f /etc/nginx/common/_custom/${common_config_filename_in_update} ]] && continue

    if [[ $1 == *"force"* && "$2" == *"${common_config_filename_in_update}"* ]] || [[ $1 == *"force"* && "$2" == *"all"* ]]; then
      if [[ -f /etc/nginx/common/_custom/${common_config_filename_in_update} ]]; then
        echo "force updating common config file... /etc/nginx/common/${common_config_filename_in_update}"
        rm /etc/nginx/common/_custom/${common_config_filename_in_update}
      fi

      [[ -f /etc/nginx/common/_default/${common_config_filename_in_update} ]] && rm /etc/nginx/common/_default/${common_config_filename_in_update}
    else
      echo "Maintaining use of custom ${common_config_filename_in_update}"
      mv /opt/gridpane/nginx-configs/common/${common_config_filename_in_update} /etc/nginx/common/_default/${common_config_filename_in_update}
      cp /etc/nginx/common/_custom/${common_config_filename_in_update} /etc/nginx/common/${common_config_filename_in_update}
    fi
  done

  cp -rf /opt/gridpane/nginx-configs/* /etc/nginx/ | tee -a /var/log/gridpane.log
  [[ -d /opt/gridpane/nginx-configs-last-update ]] && rm -rf /opt/gridpane/nginx-configs-last-update
  mv /opt/gridpane/nginx-configs /opt/gridpane/nginx-configs-last-update | tee -a /var/log/gridpane.log

  #### Now make sure incoming configs are reset to stored ENV values...

  ## Modules

  [[ "$(gridpane::conf_read geoip-module -nginx.env)" != "disabled" ]] && /usr/local/bin/gp stack nginx -geoip on
  [[ "$(gridpane::conf_read modsec-module -nginx.env)" != "disabled" ]] && /usr/local/bin/gp stack nginx -modsec-on
  [[ "$(gridpane::conf_read img-filter-module -nginx.env)" != "disabled" ]] && /usr/local/bin/gp stack nginx -img-filter-on
  [[ "$(gridpane::conf_read xslt-filter-module -nginx.env)" != "disabled" ]] && /usr/local/bin/gp stack nginx -xslt-filter-on

  ### Worker commands

  worker_rlimit_nofile_state=$(gridpane::conf_read worker_rlimit_nofile -nginx.env)
  [[ ! -z ${worker_rlimit_nofile_state} ]] && [[ "${worker_rlimit_nofile_state}" != *"$(gridpane::conf_read worker_rlimit_nofile -nginx-bak.env -q)"* ]] &&
    /usr/local/bin/gp stack nginx worker -rlimit-nofile ${worker_rlimit_nofile_state}

  worker_connections_state=$(gridpane::conf_read worker_connections -nginx.env)
  [[ ! -z ${worker_connections_state} ]] && [[ "${worker_connections_state}" != *"$(gridpane::conf_read worker_connections -nginx-bak.env -q)"* ]] &&
    /usr/local/bin/gp stack nginx worker -connections ${worker_connections_state}

  ### Server Name Commands

  [[ "$(gridpane::conf_read server_name_in_redirect -nginx.env)" != "off" ]] &&
    /usr/local/bin/gp stack nginx server-name -in-redirect on

  server_names_hash_bucket_size_state=$(gridpane::conf_read server_names_hash_bucket_size -nginx.env)
  [[ ${server_names_hash_bucket_size_state} != "256" ]] &&
    /usr/local/bin/gp stack nginx server-name -hash-bucket-size "${server_names_hash_bucket_size_state}"

  ### Limits Commands

  open_file_cache_state=$(gridpane::conf_read open_file_cache -nginx.env)
  if [[ ${open_file_cache_state} == "on" ]]; then
    open_file_cache_max_elements_state=$(gridpane::conf_read open_file_cache_max_elements -nginx.env)
    open_file_cache_inactive_timeout_state=$(gridpane::conf_read open_file_cache_inactive_timeout -nginx.env)

    if [[ -z ${open_file_cache_max_elements_state} ]]; then
      open_file_cache_max_elements_state="10000"
      gridpane::conf_write open_file_cache_max_elements "${open_file_cache_max_elements_state}" -nginx.env
    fi

    if [[ -z ${open_file_cache_inactive_timeout_state} ]]; then
      open_file_cache_inactive_timeout_state="300"
      gridpane::conf_write open_file_cache_inactive_timeout "${open_file_cache_inactive_timeout_state}" -nginx.env
    fi

    /usr/local/bin/gp stack nginx limits \
      -open-file-cache "${open_file_cache_state}" \
      --max "${open_file_cache_max_elements_state}" \
      --inactive "${open_file_cache_inactive_timeout_state}"
  fi

  open_file_cache_valid_state=$(gridpane::conf_read open_file_cache_valid -nginx.env)
  [[ ! -z ${open_file_cache_valid_state} ]] &&
    /usr/local/bin/gp stack nginx limits -open-file-cache --valid "${open_file_cache_valid_state}"

  open_file_cache_min_uses_state=$(gridpane::conf_read open_file_cache_min_uses -nginx.env)
  [[ ! -z ${open_file_cache_min_uses_state} ]] &&
    /usr/local/bin/gp stack nginx limits -open-file-cache --min-uses "${open_file_cache_min_uses_state}"

  [[ ! -z "$(gridpane::conf_read open_file_cache_errors -nginx.env)" ]] &&
    /usr/local/bin/gp stack nginx limits -open-file-cache --errors "on"

  [[ "$(gridpane::conf_read reset_timedout_connection -nginx.env)" != "on" ]] &&
    /usr/local/bin/gp stack nginx limits -reset-timedout-conn "off"

  send_timeout_state=$(gridpane::conf_read send_timeout -nginx.env)
  [[ ${send_timeout_state} != "30" ]] &&
    /usr/local/bin/gp stack nginx limits -send-timeout "${send_timeout_state}"

  keepalive_requests_state=$(gridpane::conf_read keepalive_requests -nginx.env)
  [[ ${keepalive_requests_state} != "5000" && ! -z ${keepalive_requests_state} ]] && /usr/local/bin/gp stack nginx limits -keepalive-requests "${keepalive_requests_state}"
  if [[ -z ${keepalive_requests_state} ]]; then
    keep_alive_requests_state=$(grep "keepalive_requests" /etc/nginx/common/limits.conf)
    if [[ ${keep_alive_requests_state} == *"5000;" ]]; then
      gridpane::conf_write keepalive_requests "5000" -nginx.env
    else
      keep_alive_requests_state=${keep_alive_requests_state%\;}
      keep_alive_requests_state=${keep_alive_requests_state#keepalive_requests }
      /usr/local/bin/gp stack nginx limits -keepalive-requests "${keepalive_requests_state}"
    fi
  fi

  keepalive_timeout_state=$(gridpane::conf_read keepalive_timeout -nginx.env)
  [[ ${keepalive_timeout_state} != "30" ]] &&
    /usr/local/bin/gp stack nginx limits -keepalive-timeout "${keepalive_timeout_state}"

  client_body_timeout_state=$(gridpane::conf_read client_body_timeout -nginx.env)
  [[ ${client_body_timeout_state} != "30" ]] &&
    /usr/local/bin/gp stack nginx limits -client-body-timeout "${client_body_timeout_state}"

  client_header_timeout_state=$(gridpane::conf_read client_header_timeout -nginx.env)
  [[ ${client_header_timeout_state} != "30" ]] &&
    /usr/local/bin/gp stack nginx limits -client-header-timeout "${client_header_timeout_state}"

  types_hash_max_size_state=$(gridpane::conf_read types_hash_max_size -nginx.env)
  [[ ${types_hash_max_size_state} != "2048" ]] &&
    /usr/local/bin/gp stack nginx limits -types-hash-max-size "${types_hash_max_size_state}"

  client_body_buffer_size_state=$(gridpane::conf_read client_body_buffer_size -nginx.env)
  [[ ${client_body_buffer_size_state} != "128" ]] &&
    /usr/local/bin/gp stack nginx limits -client-body-buffer-size "${client_body_buffer_size_state}"

  client_header_buffer_size_state=$(gridpane::conf_read client_header_buffer_size -nginx.env)
  [[ ${client_header_buffer_size_state} != "13" ]] &&
    /usr/local/bin/gp stack nginx limits -client-header-buffer-size "${client_header_buffer_size_state}"

  large_client_header_buffers_state=$(gridpane::conf_read large_client_header_buffers -nginx.env)
  large_client_header_buffers_size_state=$(gridpane::conf_read large_client_header_buffers_size -nginx.env)
  [[ ${large_client_header_buffers_state} != "4" || ${large_client_header_buffers_size_state} != "52" ]] &&
    /usr/local/bin/gp stack nginx limits -large-client-header-buffers "${large_client_header_buffers_state}" "${large_client_header_buffers_size_state}"

  limit_req_zone_one_size_state=$(gridpane::conf_read limit_req_zone_one_size -nginx.env)
  limit_req_zone_one_rate_state=$(gridpane::conf_read limit_req_zone_one_rate -nginx.env)
  [[ ${limit_req_zone_one_size_state} != "10" || ${limit_req_zone_one_rate_state} != "1" ]] &&
    /usr/local/bin/gp stack nginx -limits -req-zone-one "${limit_req_zone_one_size_state}" "${limit_req_zone_one_rate_state}"

  limit_req_zone_wp_size_state=$(gridpane::conf_read limit_req_zone_wp_size -nginx.env)
  limit_req_zone_wp_rate_state=$(gridpane::conf_read limit_req_zone_wp_rate -nginx.env)
  [[ ${limit_req_zone_wp_size_state} != "10" || ${limit_req_zone_wp_rate_state} != "3" ]] &&
    /usr/local/bin/gp stack nginx -limits -req-zone-wp "${limit_req_zone_wp_size_state}" "${limit_req_zone_wp_rate_state}"

  ### Fastcgi Commands

  [[ "$(gridpane::conf_read fastcgi_cache_background_update -nginx.env)" != "on" ]] &&
    /usr/local/bin/gp stack nginx fastcgi -cache-background-update off

  [[ "$(gridpane::conf_read fastcgi_cache_revalidate -nginx.env)" != "on" ]] &&
    /usr/local/bin/gp stack nginx fastcgi -cache-revalidate off

  [[ "$(gridpane::conf_read fastcgi_buffering -nginx.env)" != "on" ]] &&
    /usr/local/bin/gp stack nginx fastcgi -buffering off

  [[ "$(gridpane::conf_read fastcgi_keep_conn -nginx.env)" != "on" ]] &&
    /usr/local/bin/gp stack nginx fastcgi -keep-connection off

  fastcgi_buffer_size_state=$(gridpane::conf_read fastcgi_buffer_size -nginx.env)
  [[ ${fastcgi_buffer_size_state} != "128" ]] &&
    /usr/local/bin/gp stack nginx fastcgi -buffer-size "${fastcgi_buffer_size_state}"

  fastcgi_busy_buffers_size_state=$(gridpane::conf_read fastcgi_busy_buffers_size -nginx.env)
  [[ ${fastcgi_busy_buffers_size_state} != "256" ]] &&
    /usr/local/bin/gp stack nginx fastcgi -busy-buffer-size "${fastcgi_busy_buffers_size_state}"

  fastcgi_buffers_state=$(gridpane::conf_read fastcgi_buffers -nginx.env)
  fastcgi_buffers_size_state=$(gridpane::conf_read fastcgi_buffers_size -nginx.env)
  [[ ${fastcgi_buffers_state} != "8" || ${fastcgi_buffers_size_state} != "64" ]] &&
    /usr/local/bin/gp stack nginx fastcgi -buffers "${fastcgi_buffers_state}" "${fastcgi_buffers_size_state}"

  fastcgi_cache_size_state=$(gridpane::conf_read fastcgi_cache_size -nginx.env -q)
  if [[ ! -z ${fastcgi_cache_size_state} ]]; then
    /usr/local/bin/gp stack nginx fastcgi -max-cache-size "${fastcgi_cache_size_state}"
  else
    cache_ram=$(cat /opt/gridpane/cache.ram)
    /usr/local/bin/gp stack nginx fastcgi -max-cache-size "${cache_ram}"
  fi

  fastcgi_connect_timeout_state=$(gridpane::conf_read fastcgi_connect_timeout -nginx.env)
  [[ ${fastcgi_connect_timeout_state} != "60" ]] &&
    /usr/local/bin/gp stack nginx fastcgi -connect-timeout "${fastcgi_connect_timeout_state}"

  fastcgi_send_timeout_state=$(gridpane::conf_read fastcgi_send_timeout -nginx.env)
  [[ ${fastcgi_send_timeout_state} != "60" ]] &&
    /usr/local/bin/gp stack nginx fastcgi -send-timeout "${fastcgi_send_timeout_state}"

  fastcgi_read_timeout_state=$(gridpane::conf_read fastcgi_read_timeout -nginx.env)
  [[ ${fastcgi_read_timeout_state} != "60" ]] &&
    /usr/local/bin/gp stack nginx fastcgi -read-timeout "${fastcgi_read_timeout_state}"

  ### Proxy Commands

  [[ "$(gridpane::conf_read proxy_cache_background_update -nginx.env)" != "on" ]] &&
    /usr/local/bin/gp stack nginx proxy -cache-background-update off

  [[ "$(gridpane::conf_read proxy_cache_revalidate -nginx.env)" != "on" ]] &&
    usr/local/bin/gp stack nginx -proxy -cache-revalidate off

  [[ "$(gridpane::conf_read proxy_buffering -nginx.env)" != "on" ]] &&
    /usr/local/bin/gp stack nginx proxy -buffering off

  [[ "$(gridpane::conf_read proxy_keep_conn -nginx.env)" != "on" ]] &&
    /usr/local/bin/gp stack nginx proxy -keep-connection off

  proxy_buffer_size_state=$(gridpane::conf_read proxy_buffer_size -nginx.env)
  [[ ${proxy_buffer_size_state} != "128" ]] &&
    /usr/local/bin/gp stack nginx proxy -buffer-size "${proxy_buffer_size_state}"

  proxy_busy_buffers_size_state=$(gridpane::conf_read proxy_busy_buffers_size -nginx.env)
  [[ ${proxy_busy_buffers_size_state} != "256" ]] &&
    /usr/local/bin/gp stack nginx proxy -busy-buffer-size "${proxy_busy_buffers_size_state}"

  proxy_buffers_state=$(gridpane::conf_read proxy_buffers -nginx.env)
  proxy_buffers_size_state=$(gridpane::conf_read proxy_buffers_size -nginx.env)
  [[ ${proxy_buffers_state} != "8" || ${proxy_buffers_size_state} != "64" ]] &&
    /usr/local/bin/gp stack nginx proxy -buffers "${proxy_buffers_state}" "${proxy_buffers_size_state}"

  proxy_cache_size_state=$(gridpane::conf_read proxy_cache_size -nginx.env)
  if [[ ! -z ${proxy_cache_size_state} ]]; then
    /usr/local/bin/gp stack nginx proxy -max-cache-size "${proxy_cache_size_state}"
  else
    cache_ram=$(cat /opt/gridpane/cache.ram)
    /usr/local/bin/gp stack nginx proxy -max-cache-size "${cache_ram}"
  fi

  proxy_connect_timeout_state=$(gridpane::conf_read proxy_connect_timeout -nginx.env)
  [[ ${proxy_connect_timeout_state} != "60" ]] &&
    /usr/local/bin/gp stack nginx proxy -connect-timeout "${proxy_connect_timeout_state}"

  proxy_send_timeout_state=$(gridpane::conf_read proxy_send_timeout -nginx.env)
  [[ ${proxy_send_timeout_state} != "60" ]] &&
    /usr/local/bin/gp stack nginx proxy -send-timeout "${proxy_send_timeout_state}"

  proxy_read_timeout_state=$(gridpane::conf_read proxy_read_timeout -nginx.env)
  [[ ${proxy_read_timeout_state} != "60" ]] &&
    /usr/local/bin/gp stack nginx proxy -read-timeout "${proxy_read_timeout_state}"

  gridpane::conf_write configs-version ${current_configs_version}
  gridpane::conf_write configs-update-processing complete
}

#######################################
# Run the Postprocess script
#
# Accepted Args:
#  absolute filepath to script
#######################################
gpupdate::postprocess() {

  if [[ ! -z ${scripts_updated} ]] || [[ ! -z ${configs_updated} ]]; then
    if [[ ! -z ${scripts_updated} ]] && [[ ! -z ${configs_updated} ]]; then
      update="full"
    else
      if [[ ! -z ${scripts_updated} ]]; then
        update="scripts"
      else
        update="configs"
      fi
    fi
    echo "bash /usr/local/bin/gpupdate-postprocess ${update}" | at now + 2 minute
    echo "#####################################################################################################" | tee -a /var/log/gridpane.log
  else
    echo "No updates were needed.... exiting" | tee -a /var/log/gridpane.log
    echo "#####################################################################################################" | tee -a /var/log/gridpane.log
  fi
}
