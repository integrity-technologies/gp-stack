#!/bin/bash

nginx::sync::additional_security() {
  local site="$1"

  if [[ ! -f /opt/gridpane/security.tab.update ]]; then
    /usr/local/bin/gpupdate force-scripts-configs
    touch /opt/gridpane/security.tab.update
  fi

  local webserver_dir
  # shellcheck disable=SC2154
  if [[ ${webserver} = nginx ]]; then
    webserver_dir="${webserver}"
  else
    webserver_dir="ols"
  fi

  local disable_xmlrpc
  if [[ -f /var/www/"${site}"/"${webserver_dir}"/disable-xmlrpc-main-context.conf ]]; then
    disable_xmlrpc="true"
    echo "${disable_xmlrpc}" >/var/www/"${site}"/logs/disable.xmlrpc.env
  else
    disable_xmlrpc="false"
    [[ -f /var/www/"${site}"/logs/disable.xmlrpc.env ]] &&
      rm /var/www/"${site}"/logs/disable.xmlrpc.env
  fi
  gridpane::conf_write disable-xmlrpc "${disable_xmlrpc}" -site.env "${site}"

  local block_load_scripts
  if [[ -f /var/www/"${site}"/"${webserver_dir}"/disable-load-scripts-concatenation-main-context.conf ]]; then
    block_load_scripts="true"
    echo "${block_load_scripts}" >/var/www/"${site}"/logs/disable.wpadmin.concatenation.env
  else
    block_load_scripts="false"
    [[ -f /var/www/"${site}"/logs/disable.wpadmin.concatenation.env ]] &&
      rm /var/www/"${site}"/logs/disable.wpadmin.concatenation.env
  fi
   gridpane::conf_write disable-concat "${block_load_scripts}" -site.env "${site}"

  local block_wpcontent_php
  if [[ -f /var/www/"${site}"/"${webserver_dir}"/disable-wp-content-php-main-context.conf ]]; then
    block_wpcontent_php="true"
    echo "${block_wpcontent_php}" >/var/www/"${site}"/logs/disable.wpcontent.php.env
  else
    block_wpcontent_php="false"
    [[ -f /var/www/"${site}"/logs/disable.wpcontent.php.env ]] &&
      rm /var/www/"${site}"/logs/disable.wpcontent.php.env
  fi
  gridpane::conf_write block-wp-content-php "${block_wpcontent_php}" -site.env "${site}"

  local block_comments
  if [[ -f /var/www/"${site}"/"${webserver_dir}"/disable-wp-comments-post-main-context.conf ]]; then
    block_comments="true"
    echo "${block_comments}" >/var/www/"${site}"/logs/disable.wp.comments.post.env
  else
    block_comments="false"
    [[ -f /var/www/"${site}"/logs/disable.wp.comments.post.env ]] &&
      rm /var/www/"${site}"/logs/disable.wp.comments.post.env
  fi
  gridpane::conf_write block-wp-comments-post "${block_comments}" -site.env "${site}"

  local block_opml_links
  if [[ -f /var/www/"${site}"/"${webserver_dir}"/disable-wp-links-opml-main-context.conf ]]; then
    block_opml_links="true"
    echo "${block_opml_links}" >/var/www/"${site}"/logs/disable.wp.links.opml.env
  else
    block_opml_links="false"
    [[ -f /var/www/"${site}"/logs/disable.wp.links.opml.env ]] &&
      rm /var/www/"${site}"/logs/disable.wp.links.opml.env
  fi
  gridpane::conf_write block-wp-links-opml "${block_opml_links}" -site.env "${site}"

  local block_trackbacks
  if [[ -f /var/www/"${site}"/"${webserver_dir}"/disable-wp-trackbacks-main-context.conf ]]; then
    block_trackbacks="true"
    echo "${block_trackbacks}" >/var/www/"${site}"/logs/disable.wp.trackbacks.env
  else
    block_trackbacks="false"
    [[ -f /var/www/"${site}"/logs/disable.wp.trackbacks.env ]] &&
      rm /var/www/"${site}"/logs/disable.wp.trackbacks.env
  fi
  gridpane::conf_write block-wp-trackbacks "${block_trackbacks}" -site.env "${site}"

  local block_install_php
  if [[ -f /var/www/"${site}"/"${webserver_dir}"/block-install-file-main-context.conf ]]; then
    block_install_php="true"
    echo "${block_install_php}" >/var/www/"${site}"/logs/disable.wpadmin.install.env
  else
    block_install_php="false"
    [[ -f /var/www/"${site}"/logs/disable.wpadmin.install.env ]] &&
      rm /var/www/"${site}"/logs/disable.wpadmin.install.env
  fi
  gridpane::conf_write block-install.php "${block_install_php}" -site.env "${site}"

  local block_upgrade_php
  if [[ -f /var/www/"${site}"/"${webserver_dir}"/block-upgrade-file-main-context.conf ]]; then
    block_upgrade_php="true"
    echo "${block_upgrade_php}" >/var/www/"${site}"/logs/disable.wpadmin.upgrade.env
  else
    block_upgrade_php="false"
    [[ -f /var/www/"${site}"/logs/disable.wpadmin.upgrade.env ]] &&
      rm /var/www/"${site}"/logs/disable.wpadmin.upgrade.env
  fi
  gridpane::conf_write block-upgrade.php "${block_upgrade_php}" -site.env "${site}"

  site_mu_plugin_dir=$(ls /var/www/${site}/htdocs/wp-content/mu-plugins)

  local disable_wpscan_agent
  if [[ ${site_mu_plugin_dir} == *"gridpane-block-wpscan-agent"* ]]; then
    disable_wpscan_agent="true"
  else
    disable_wpscan_agent="false"
  fi
  gridpane::conf_write gridpane-block-wpscan-agent-plugin ${disable_wpscan_agent} -site.env "${site}"

  local disable_emoji
  if [[ ${site_mu_plugin_dir} == *"gridpane-kill-all-emoji"* ]]; then
    disable_emoji="true"
  else
    disable_emoji="false"
  fi
  gridpane::conf_write gridpane-kill-all-emoji-plugin ${disable_emoji} -site.env "${site}"

  local disable_username_enumaration
  if [[ ${site_mu_plugin_dir} == *"gridpane-block-username-enumeration"* ]]; then
    disable_username_enumaration="true"
  else
    disable_username_enumaration="false"
  fi
  gridpane::conf_write gridpane-block-username-enumeration-plugin ${disable_username_enumaration} -site.env "${site}"

  local disable_wpversion
  if [[ ${site_mu_plugin_dir} == *"gridpane-remove-wp-version"* ]]; then
    disable_wpversion="true"
  else
    disable_wpversion="false"
  fi
  gridpane::conf_write gridpane-remove-wp-version-plugin ${disable_wpversion} -site.env "${site}"

  local disable_rss
  if [[ ${site_mu_plugin_dir} == *"gridpane-disable-rss"* ]]; then
    disable_rss="true"
  else
    disable_rss="false"
  fi
  gridpane::conf_write gridpane-disable-rss-plugin ${disable_rss} -site.env "${site}"

  gridpane::callback::app \
    "callback" \
    "/site/site-update" \
    "--" \
    "site_url=${site}" \
    "server_ip=${serverIP}" \
    "disable_xmlrpc@${disable_xmlrpc}" \
    "disable_emoji@${disable_emoji}" \
    "disable_rss@${disable_rss}" \
    "disable_username_enumaration@${disable_username_enumaration}" \
    "disable_wpscan_agent@${disable_wpscan_agent}" \
    "disable_wpversion@${disable_wpversion}" \
    "block_wpcontent_php@${block_wpcontent_php}" \
    "block_install_php@${block_install_php}" \
    "block_upgrade_php@${block_upgrade_php}" \
    "block_load_scripts@${block_load_scripts}" \
    "block_comments@${block_comments}" \
    "block_trackbacks@${block_trackbacks}" \
    "block_opml_links@${block_opml_links}" \
    "additional_settings_synced@true" \
    "silent@false"

}

nginx::sync::access_settings() {
  local site="$1"

  if [[ ! -f /opt/gridpane/access.tab.update ]]; then
    /usr/local/bin/gpupdate force-scripts-configs
    touch /opt/gridpane/access.tab.update
  fi

  # shellcheck disable=SC2154
  if [[ "$webserver" = nginx ]]; then
    site_config=$(cat /etc/nginx/sites-available/"${site}")
  else
    site_config="$(/usr/local/bin/gpols get "$site" vhconf)"
  fi

  local suspend_site
  if [[ ! -f /var/www/"${site}"/htdocs/index.php &&
        -f /var/www/"${site}"/htdocs/index.php.suspend &&
        -f /var/www/"${site}"/htdocs/index.html ]]; then
    suspend_site="true"
    suspend_env="on"
    if [[ ! -f /var/www/"${site}"/logs/suspend.env ]]; then
      echo "${suspend_env}" >/var/www/"${site}"/logs/suspend.env
    fi
  else
    [[ -f /var/www/"${site}"/logs/suspend.env ]] &&
      rm /var/www/"${site}"/logs/suspend.env
    suspend_site="false"
    suspend_env="off"
  fi
  gridpane::conf_write suspend "${suspend_env}" -site.env "${site}"

  local httpauth
  if [[ "$webserver" = nginx &&
        ${site_config} == *"#include /etc/nginx/common/acl.conf;"* ||
        "$webserver" = openlitespeed &&
        ${site_config} != *"/usr/local/lsws/conf/.htpasswd"* ]]; then
    httpauth="false"
    [[ -f /var/www/"${site}"/logs/http.auth.env ]] &&
      rm /var/www/"${site}"/logs/http.auth.env
  else
    httpauth="true"
    echo "${httpauth}" >/var/www/"${site}"/logs/http.auth.env
  fi
  gridpane::conf_write http-auth "${httpauth}" -site.env "${site}"

  gridpane::callback::app \
    "callback" \
    "/site/site-update" \
    "--" \
    "site_url=${site}" \
    "server_ip=${serverIP}" \
    "suspend_site@${suspend_site}" \
    "httpauth@${httpauth}" \
    "access_settings_synced@true" \
    "silent@false"
}

nginx::sync::cache_settings() {
  [[ ! -f /opt/gridpane/ui.cache.update ]] &&
    /usr/local/bin/gpupdate force-scripts &&
    touch /opt/gridpane/ui.cache.update

  local site="$1"
  local callback_or_not="$2"
  local cache_type
  cache_type=$(gridpane::check::site::nginx::cache "${site}")
  local conf_type
  conf_type=$(gridpane::check::site::nginx::config "${site}")

  local scheme
  if [[ ${conf_type} == *"https"* ]]; then
    scheme="https"
  else
    scheme="http"
  fi

  local cache_valid_ttl
  local cache_var_cache_ttl
  if [[ ${cache_type} == "fastcgi" ]]; then

    if [[ ${conf_type} == *"proxy"* ]]; then
      cache_valid_ttl=$(grep "set \$cache_ttl" /etc/nginx/common/"${site}-proxy-${scheme}-proxycache.conf")
      cache_valid_ttl=${cache_valid_ttl#set \$cache_ttl }
      cache_valid_ttl=${cache_valid_ttl%;}
      cache_var_cache_ttl=$(grep "proxy_cache_valid 200" /etc/nginx/common/"${site}"-proxy-cache-var.conf)
      cache_var_cache_ttl=${cache_var_cache_ttl#proxy_cache_valid 200 }
      cache_var_cache_ttl=${cache_var_cache_ttl%;s}
      if [[ $cache_valid_ttl != "$cache_var_cache_ttl" ]]; then
        sed -i "/proxy_cache_valid/c\proxy_cache_valid 200 ${cache_valid_ttl}s;" /etc/nginx/common/"${site}"-proxy-cache-var.conf
      fi
      gridpane::conf_write ngx-proxy-cache-valid "${cache_valid_ttl}" -site.env "${site}"
    else
      cache_valid_ttl=$(grep "set \$cache_ttl" /etc/nginx/common/"${site}"-wpfc.conf)
      cache_valid_ttl=${cache_valid_ttl#set \$cache_ttl }
      cache_valid_ttl=${cache_valid_ttl%;}
      cache_var_cache_ttl=$(grep "fastcgi_cache_valid 200" /etc/nginx/common/"${site}"-fcgi-cache-var.conf)
      cache_var_cache_ttl=${cache_var_cache_ttl#fastcgi_cache_valid 200 }
      cache_var_cache_ttl=${cache_var_cache_ttl%;s}
      if [[ $cache_valid_ttl != "$cache_var_cache_ttl" ]]; then
        sed -i "/fastcgi_cache_valid/c\fastcgi_cache_valid 200 ${cache_valid_ttl}s;" /etc/nginx/common/"${site}"-fcgi-cache-var.conf
      fi
      gridpane::conf_write ngx-fcgi-cache-valid "${cache_valid_ttl}" -site.env "${site}"
    fi

  elif [[ ${cache_type} == "redis" ]]; then

    if [[ ${conf_type} == *"proxy"* ]]; then
      cache_valid_ttl=$(grep "set \$cache_ttl" /etc/nginx/common/"${site}"-proxy-${scheme}-rediscache.conf)
      cache_valid_ttl=${cache_valid_ttl#set \$cache_ttl }
      cache_valid_ttl=${cache_valid_ttl%;}
      gridpane::conf_write ngx-proxy-cache-valid "${cache_valid_ttl}" -site.env "${site}"
    else
      cache_valid_ttl=$(grep "set \$cache_ttl" /etc/nginx/common/"${site}"-wp-redis.conf)
      cache_valid_ttl=${cache_valid_ttl#set \$cache_ttl }
      cache_valid_ttl=${cache_valid_ttl%;}
      gridpane::conf_write ngx-redis-cache-valid "${cache_valid_ttl}" -site.env "${site}"
    fi

  fi

  gridpane::conf_write nginx-caching-ttl "${cache_valid_ttl}" -site.env "${site}"

  if [[ $callback_or_not != *"no-callback" ]]; then
    case ${cache_type} in
      fastcgi|redis)
        gridpane::callback::app \
        "callback" \
        "/site/site-update" \
        "--" \
        "site_url=${site}" \
        "server_ip=${serverIP}" \
        "caching=${cache_type}" \
        "nginx_caching=${cache_type}" \
        "nginx_caching_ttl=${cache_valid_ttl}" \
        "nginx_cache_settings_synced@true"
        ;;
      *)
        gridpane::callback::app \
        "callback" \
        "/site/site-update" \
        "--" \
        "site_url=${site}" \
        "server_ip=${serverIP}" \
        "caching=off" \
        "nginx_caching=off" \
        "nginx_cache_settings_synced@true"
        ;;
    esac
  fi
}

nginx::sync::site::settings() {
  local site="$1"
  local site_nginx_conf_file
  site_nginx_conf_file="/var/www/${site}/nginx/gp-nginx-main-context.conf"
  server_nginx_fastcgi_conf_file="/etc/nginx/conf.d/fastcgi.conf"

  [[ ! -f "${site_nginx_conf_file}" ]] &&
    touch "${site_nginx_conf_file}"

  local current_site_fastcgi_send_timeout
  current_site_fastcgi_send_timeout=$(grep "fastcgi_send_timeout" "${site_nginx_conf_file}" | uniq)
  if [[ -z ${current_site_fastcgi_send_timeout} ]]; then
    local current_server_fastcgi_send_timeout
    current_server_fastcgi_send_timeout=$(grep "fastcgi_send_timeout" "${server_nginx_fastcgi_conf_file}")
    echo "${current_server_fastcgi_send_timeout}" >>"${site_nginx_conf_file}"
    current_server_fastcgi_send_timeout=${current_server_fastcgi_send_timeout#fastcgi_send_timeout }
    current_site_fastcgi_send_timeout=${current_server_fastcgi_send_timeout%s;}
  else
    current_site_fastcgi_send_timeout=${current_site_fastcgi_send_timeout#fastcgi_send_timeout }
    current_site_fastcgi_send_timeout=${current_site_fastcgi_send_timeout%s;}
  fi
  [[ -n ${current_site_fastcgi_send_timeout} &&
    ${current_site_fastcgi_send_timeout} =~ ^[0-9]+$ ]] &&
    gridpane::conf_write "fastcgi-send-timeout" "${current_site_fastcgi_send_timeout}" -site.env "${site}"

  local current_site_fastcgi_read_timeout
  current_site_fastcgi_read_timeout=$(grep "fastcgi_read_timeout" "${site_nginx_conf_file}")
  if [[ -z ${current_site_fastcgi_read_timeout} ]]; then
    local current_server_fastcgi_read_timeout
    current_server_fastcgi_read_timeout=$(grep "fastcgi_read_timeout" "${server_nginx_fastcgi_conf_file}" | uniq)
    echo "${current_server_fastcgi_read_timeout}" >>"${site_nginx_conf_file}"
    current_server_fastcgi_read_timeout=${current_server_fastcgi_read_timeout#fastcgi_read_timeout }
    current_site_fastcgi_read_timeout=${current_server_fastcgi_read_timeout%s;}
  else
    current_site_fastcgi_read_timeout=${current_site_fastcgi_read_timeout#fastcgi_read_timeout }
    current_site_fastcgi_read_timeout=${current_site_fastcgi_read_timeout%s;}
  fi
  [[ -n ${current_site_fastcgi_read_timeout} &&
    ${current_site_fastcgi_read_timeout} =~ ^[0-9]+$ ]] &&
    gridpane::conf_write "fastcgi-read-timeout" "${current_site_fastcgi_read_timeout}" -site.env "${site}"

  local current_site_zone_one_burst
  current_site_zone_one_burst=$(grep "limit_req zone=one" "/etc/nginx/common/${site}-wpcommon.conf" | uniq | xargs)
  current_site_zone_one_burst=${current_site_zone_one_burst#limit_req zone=one burst=}
  current_site_zone_one_burst=${current_site_zone_one_burst% nodelay;}
  [[ -n ${current_site_zone_one_burst} &&
    ${current_site_zone_one_burst} =~ ^[0-9]+$ ]] &&
    gridpane::conf_write zone-one-burst "${current_site_zone_one_burst}" -site.env "${site}"

  local current_site_zone_wp_burst
  current_site_zone_wp_burst=$(grep "limit_req zone=wp" "/etc/nginx/common/${site}-wpcommon.conf" | uniq | xargs)
  current_site_zone_wp_burst=${current_site_zone_wp_burst#limit_req zone=wp burst=}
  current_site_zone_wp_burst=${current_site_zone_wp_burst% nodelay;}
  [[ -n ${current_site_zone_wp_burst} &&
    ${current_site_zone_wp_burst} =~ ^[0-9]+$ ]] &&
    gridpane::conf_write zone-wp-burst "${current_site_zone_wp_burst}" -site.env "${site}"

  local current_site_client_max_body_size
  current_site_client_max_body_size=$(grep "client_max_body_size" "/etc/nginx/common/${site}-wpcommon.conf" | uniq | xargs)
  current_site_client_max_body_size=${current_site_client_max_body_size#client_max_body_size }
  current_site_client_max_body_size=${current_site_client_max_body_size%;}
  current_site_client_max_body_size=${current_site_client_max_body_size%m}
  current_site_client_max_body_size=${current_site_client_max_body_size%k}
  current_site_client_max_body_size=${current_site_client_max_body_size%g}
  [[ -n ${current_site_client_max_body_size} &&
    ${current_site_client_max_body_size} =~ ^[0-9]+$ ]] &&
    gridpane::conf_write ngx-max-body "${current_site_client_max_body_size}m" -site.env "${site}"

  gridpane::callback::app \
    "callback" \
    "/site/site-update" \
    "--" \
    "site_url=${site}" \
    "server_ip=${serverIP}" \
    "nginx_fastcgi_send_timeout=${current_site_fastcgi_send_timeout}" \
    "nginx_fastcgi_read_timeout=${current_site_fastcgi_read_timeout}" \
    "nginx_limit_req_zone_one=${current_site_zone_one_burst}" \
    "nginx_limit_req_zone_wp=${current_site_zone_wp_burst}" \
    "nginx_client_max_body_size=${current_site_client_max_body_size}" \
    "nginx_site_settings_synced@true"

}

nginx::configure::site::fastcgi::timeout() {
  local site="$1"
  local timeout_type="$2"

  if [[ -z ${timeout_type} ||
        -z ${site} ]]; then
    echo "No timeout or site passed... exiting..."
    exit 187
  fi

  ensureLogDirExists "${site}"
  ensureLogExists "${site}" gridpane-general
  logIt gridpane-general nginx-fastcgi-timeout Begin "${site}"

  local amount_of_time="$3"
  if [[ ! ${amount_of_time} =~ ^[0-9]+$ ||
      -z ${amount_of_time} ]]; then
    amount_of_time="60"
    echo "FastCGI ${timeout_type} timeout passed is not an integer, setting to default ${amount_of_time}m" | tee -a "/var/www/${site}/logs/gridpane-general.log"
  fi

  local conf_file
  conf_file="/var/www/${site}/nginx/gp-nginx-main-context.conf"
  if [[ ! -f "${conf_file}" ]]; then
    touch "${conf_file}"
  fi

  local current_site_timeout
  current_site__timeout=$(grep "fastcgi_${timeout_type}_timeout" "${conf_file}")
  {
    if [[ -z ${current_site_timeout} ]]; then
      echo "fastcgi_${timeout_type}_timeout ${amount_of_time}s;" >>"${conf_file}"
    else
      sed -i "/fastcgi_${timeout_type}_timeout/c\fastcgi_${timeout_type}_timeout ${amount_of_time}s;" "${conf_file}"
    fi
    grep "fastcgi_${timeout_type}_timeout" "${conf_file}"
    echo "${conf_file}"
  } | tee -a "/var/www/${site}/logs/gridpane-general.log"

  gridpane::conf_write "fastcgi-${timeout_type}-timeout" "${amount_of_time}" -site.env "${site}"

  gridpane::callback::app \
    "callback" \
    "/site/site-update" \
    "--" \
    "site_url=${site}" \
    "server_ip=${serverIP}" \
    "nginx_fastcgi_${timeout_type}_timeout=${amount_of_time}"

  logIt gridpane-general nginx-fastcgi-timeout Continue "${site}"
}
