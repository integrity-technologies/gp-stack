#!/bin/bash

#main () {
#
#  local site_url="$1"
#  declare -g UUID
#  gpbup::uuidgen
#  UUID="$(cat /root/gridcreds/gridpane.uuid)"
#
#  if [[ $# -gt 0 ]]; then
#    while true; do
#      local var="$1"
#      case "${var}" in
#        -rebuild-binds) gpmulti::rebuild_binds ;;
#        -update-blueprint) 
#          shift
#          local blueprint_revision="$1"
#          gpmulti::update_blueprint "${blueprint_revision}" 
#          ;;
#        -rollback-blueprint) gpmulti::update_blueprint rollback ;;
#      esac
#
#      shift 1 || true
#
#      if [[ -z "${var}" ]]; then
#        break
#      fi
#
#    done
#  else
#    echo "Do nothing..."
#  fi
#
#}

gpmulti::rebuild_binds() {

  local site_url
  local site_list=${1:-$(gpbup::getallrunnablesites)}
  local file_path
  local active_php
  local php_version

  for site_url in ${site_list}; do

    echo "Creating mounts for ${site_url}..."

    sed -i "/^\/var\/www\/core\/.*\ \ \ \ \/var\/www\/${site_url}.*/d" /etc/fstab

    for file_path in $(find /var/www/core -maxdepth 1 -type f | cut -d "/" -f 5-); do
      umount /var/www/${site_url}/htdocs/${file_path} > /dev/null || true
      rm /var/www/${site_url}/htdocs/${file_path} || true
      touch /var/www/${site_url}/htdocs/${file_path}
      echo "/var/www/core/${file_path}    /var/www/${site_url}/htdocs/${file_path} none bind 0 0" | sudo tee -a /etc/fstab > /dev/null
    done

    for file_path in $(find /var/www/core/wp-content -maxdepth 1 -type f | cut -d "/" -f 5-); do
      umount /var/www/${site_url}/htdocs/${file_path} > /dev/null || true
      rm /var/www/${site_url}/htdocs/${file_path} || true
      touch /var/www/${site_url}/htdocs/${file_path}
      echo "/var/www/core/${file_path}    /var/www/${site_url}/htdocs/${file_path} none bind 0 0" | sudo tee -a /etc/fstab > /dev/null
    done

    for file_path in $(find /var/www/core/wp-content -maxdepth 1 -type d | sed 1d | cut -d "/" -f 5-); do
      umount /var/www/${site_url}/htdocs/${file_path} > /dev/null || true
      rm -fr /var/www/${site_url}/htdocs/${file_path} || true
      mkdir -p /var/www/${site_url}/htdocs/${file_path}
      echo "/var/www/core/${file_path}    /var/www/${site_url}/htdocs/${file_path} none bind 0 0" | sudo tee -a /etc/fstab > /dev/null
    done

    umount /var/www/${site_url}/htdocs/wp-admin > /dev/null || true
    rm -fr /var/www/${site_url}/htdocs/wp-admin
    mkdir -p /var/www/${site_url}/htdocs/wp-admin
    echo "/var/www/core/wp-admin    /var/www/${site_url}/htdocs/wp-admin none bind 0 0" | sudo tee -a /etc/fstab > /dev/null

    umount /var/www/${site_url}/htdocs/wp-includes > /dev/null || true
    rm -fr /var/www/${site_url}/htdocs/wp-includes
    mkdir -p /var/www/${site_url}/htdocs/wp-includes
    echo "/var/www/core/wp-includes    /var/www/${site_url}/htdocs/wp-includes none bind 0 0" | sudo tee -a /etc/fstab > /dev/null

  done

  echo "Ensuring core files are owned by root..."
  chown -R root. /var/www/core

  echo "Mounting new binds..."
  mount -a

  echo "Reloading PHP-FPM to clear OPCache..."
  # please ignore how hacky this is...
  active_php=($(ps auxww | grep "php-fpm:" | awk -F'/' '{print $4}' | grep ^[0-9]))

  for php_version in ${active_php[@]}; do
    service php${php_version}-fpm reload
  done

}

gpmulti::update_blueprint() {

  local blueprint_name=$(gridpane::conf_read multitenancy-immutable-blueprint-name)
  #local blueprint_url=$(gridpane::conf_read multitenancy-immutable-blueprint-link)
  local blueprint_url="$(cat /root/gridenv/promethean.env  | grep multitenancy-immutable-blueprint-link | awk -F: '{print $2":"$3}' || true)"
  local revision="$1"

  if [[ -z "${blueprint_name}" || -z "${blueprint_url}" ]]; then
    echo "There's no blueprint set..."
    return 1
  fi

  echo "Attempting to update ${blueprint_name}..."

  rm -r /var/www/core.bak || true
  mv /var/www/core /var/www/core.bak || true
  git clone "${blueprint_url}" /var/www/core

  if [[ ! -z ${revision} ]]; then
    if [[ ${revision} == "rollback" ]]; then
      git reset HEAD~1 --hard
    else
      git reset ${revision} --hard
    fi
  fi

  echo "Rebuilding binds for all sites to ensure we've included any new core files..."
  gpmulti::rebuild_binds

}

#main "$@"