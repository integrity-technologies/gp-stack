#!/bin/bash

#. gridpane.sh

gpclone::end::failsync_pause::exit() {
  echo "gpfailsync-server-worker-pause deleted, server tasks can resume as normal..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  gridpane::conf_delete gpfailsync-server-worker-pause
  exit 187
}

gpclone::failsync_pause() {
  echo "gpfailsync-server-worker-pause true, server tasks will be paused..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  gridpane::conf_write gpfailsync-server-worker-pause true
}

gpclone::check::htdocs_config() {
  local siteurl="$1"
  local serverIP_to_use

  if [[ -z $2 ]]; then
    serverIP_to_use="${serverIP}"
  else
    serverIP_to_use="$2"
  fi

  if [[ -f /var/www/${siteurl}/htdocs/wp-config.php ]]; then
    notification_body="WP-Config Check <br>Site Url:${siteurl}<br>Source Server:${serverIP_to_use}<br>The htdocs folder for ${siteurl} has a wp-config.php file, exiting..."
    gridpane::notify::app \
      "Cloning/Migrate Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${siteurl}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gpclone::end::failsync_pause::exit
  fi
}

gpclone::export::site() {
  if [[ -z $1 ]]; then
    echo "We need to know which site to export... exiting..."
    gpclone::end::failsync_pause::exit
  fi

  local site_to_clone=$1
  local alternate

  if [[ $2 == "alternate" ]]; then
    alternate=".$2"
  fi

  echo "Check if /var/www/${site_to_clone}/htdocs has a wp-config.php ..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  gpclone::check::htdocs_config ${site_to_clone}

  echo "Processing /var/www/${site_to_clone}/htdocs ..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  cd /var/www/${site_to_clone}/htdocs

  if [[ "$(cat /var/www/${site_to_clone}/htdocs/index.php)" != *"Front to the WordPress application. This file doesn't do anything"* ]]; then

    notification_body="Process Failed<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>${site_to_clone} is not a valid WordPress site... skipping"
    gridpane::notify::app \
      "Cloning/Migrate Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  else

    echo "Exporting ${site_to_clone} database..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    local WPDBNAME=$(cat /var/www/${site_to_clone}/wp-config.php | grep DB_NAME | cut -d \' -f 4)
    local WPDBUSER=$(cat /var/www/${site_to_clone}/wp-config.php | grep DB_USER | cut -d \' -f 4)
    local WPDBPASS=$(cat /var/www/${site_to_clone}/wp-config.php | grep DB_PASSWORD | cut -d \' -f 4)
    mysqldump -u${WPDBUSER} -p${WPDBPASS} ${WPDBNAME} >/var/www/${site_to_clone}/htdocs/database.sql

    if [ "$?" -eq 0 ]; then
      echo "MySQL Dump Success..."
    else
      echo "MySQL Dump Error..."
      rm /var/www/${site_to_clone}/htdocs/database.sql
    fi

    if [[ -f /var/www/${site_to_clone}/htdocs/database.sql ]]; then
      ls -l /var/www/${site_to_clone}/htdocs/database.sql | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      chmod 400 /var/www/${site_to_clone}/htdocs/database.sql
      echo "DB Exported..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    else
      echo "Failed to export DB..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      touch /tmp/${nonce}.failed.db.export
    fi

    #Need to get the DB prefix from wp-config...
    table_prefix=$(sed -n -e '/$table_prefix/p' /var/www/${site_to_clone}/wp-config.php)
    echo ${table_prefix} >/var/www/${site_to_clone}/htdocs/table.prefix
    chmod 400 /var/www/${site_to_clone}/htdocs/table.prefix

    currUser=$(gridpane::get::set::site::user ${site_to_clone})
    echo ${currUser} >/var/www/${site_to_clone}/htdocs/grid.user
    chmod 400 /var/www/${site_to_clone}/htdocs/grid.user

    chattr -i /var/www/${site_to_clone}/logs/${site_to_clone}.env
    cp /var/www/${site_to_clone}/logs/${site_to_clone}.env /var/www/${site_to_clone}/htdocs/${site_to_clone}.env.clone${alternate}

    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "Creating archive package of site ${site_to_clone} htdocs containing:" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    ls -l $PWD | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    tar -cf /var/www/${site_to_clone}/GPBUP-${site_to_clone}-CLONE.gz . --exclude '*.zip' --exclude '*.gz' --exclude 'wp-config.php' |
      tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Site archive for ${site_to_clone} created, cleaning up a bit..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    rm /var/www/${site_to_clone}/htdocs/database.sql | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    rm /var/www/${site_to_clone}/htdocs/table.prefix | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    rm /var/www/${site_to_clone}/htdocs/grid.user | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    rm /var/www/${site_to_clone}/htdocs/*.env.* | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    chattr +i /var/www/${site_to_clone}/logs/${site_to_clone}.env

    [[ -e ${site_to_clone}.key ]] && rm ${site_to_clone}.key && echo "Removed source backup of key..."
    [[ -e ${site_to_clone}.cert ]] && rm ${site_to_clone}.cert && echo "Removed source backup of cert..."
    [[ -e ${site_to_clone}.trust ]] && rm ${site_to_clone}.trust && echo "Removed source backup of trust..."
    [[ -e ${site_to_clone}.nginx ]] && rm ${site_to_clone}.nginx && echo "Removed source backup of nginx..."
    [[ -e ${site_to_clone}.renew ]] && rm ${site_to_clone}.renew && echo "Removed source backup of renewal config..."

    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "Export of ${site_to_clone} Done!" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sleep 3
  fi
}

gpclone::migrate_to_remote() {
  local site_to_clone="$1"
  local new_url="$2"

  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "Site to Clone: ${site_to_clone}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "New URL: ${new_url}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "Remote IP ${remote_IP}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  RETRY=0
  while ssh -n root@${remote_IP} [ ! -d /var/www/${new_url}/htdocs/wp-content/plugins/nginx-helper ]; do
    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "Waiting for remote site to completely provision..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sleep 1

    RETRY=$(($RETRY + 1))

    if [[ ${RETRY} -gt 300 ]]; then
      notification_body="Building new ${new_url} site on ${remote_IP} stalled, exiting process..."
      gridpane::notify::app \
        "Cloning/Migrate Notice" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      gpclone::end::failsync_pause::exit
    fi
  done

  if [[ $3 == "true"* ]]; then
    if [[ $3 == *"sudomain" ]]; then
      multisite_type="-multisubdom"
    elif [[ $3 == *"subdirectory" ]]; then
      multisite_type="-multisubdir"
    fi
    echo "Converting ${remote_IP} ${new_url} to multisite $3..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    ssh root@${remote_IP} "sleep 1 && /usr/local/bin/gp site ${new_url} ${multisite_type}" | tee -a /opt/gridpane/migrate-clone.log
  fi

  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  scp /var/www/${site_to_clone}/GPBUP-${site_to_clone}-CLONE.gz root@${remote_IP}:/var/www/${new_url}/GPBUP-${site_to_clone}-CLONE.gz |
    tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  ssh root@${remote_IP} "sleep 1 && ls -l /var/www/${new_url}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  echo "Copied local WP export file for ${site_to_clone} to /var/www/${new_url}/GPBUP-${site_to_clone}-CLONE.gz on remote system ${remote_IP}" |
    tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  check_for_promethean_remote=$(ssh root@${remote_IP} "sleep 1 && sudo /usr/local/bin/gp conf read server-build -q" 2>&1)

  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "Destination is: ${check_for_promethean_remote}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  if [[ ${check_for_promethean_remote} == *"prometheus"* ]]; then
    if [[ -f /var/www/${site_to_clone}/user-configs.php ]]; then
      echo "Sending over user wp-config.php includes..."
      scp /var/www/${site_to_clone}/user-configs.php root@${remote_IP}:/var/www/${new_url}/user-configs.php |
        tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    fi

    is_modsec=$(gridpane::conf_read waf -site.env.clone ${site_to_clone})
    if [[ ${is_modsec} == *"modsec"* ]]; then
      echo "Syncing Modsec" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      ssh root@${remote_IP} "sleep 1 && sudo mkdir -p /var/www/${new_url}/modsec" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      scp /var/www/${site_to_clone}/modsec/${site_to_clone}-crs-anomaly-blocking-threshold.conf root@${remote_IP}:/var/www/${new_url}/modsec/${new_url}-crs-anomaly-blocking-threshold.conf.source |
        tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      scp /var/www/${site_to_clone}/modsec/${site_to_clone}-crs-paranoia-level.conf root@${remote_IP}:/var/www/${new_url}/modsec/${new_url}-crs-paranoia-level.conf.source |
        tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      scp /var/www/${site_to_clone}/modsec/${site_to_clone}-runtime-exceptions.conf root@${remote_IP}:/var/www/${new_url}/modsec/${new_url}-runtime-exceptions.conf.source |
        tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      scp /var/www/${site_to_clone}/modsec/${site_to_clone}-runtime-whitelist.conf root@${remote_IP}:/var/www/${new_url}/modsec/${new_url}-runtime-whitelist.conf.source |
        tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      ssh root@${remote_IP} "sleep 1 && ls -l /var/www/${new_url}/modsec" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    fi

    is_smtp=$(gridpane::conf_read smtp -site.env ${site_to_clone})
    if [[ ${is_smtp} == *"rue"* ]] || [[ ${is_smtp} == *"on"* ]]; then
      if [[ -f /var/www/${site_to_clone}/sendgrid-wp-configs.php ]]; then
        echo "SMTP enabled for origin site.... syncing required configs to set SMTP up on destination..." |
          tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

        scp /var/www/${site_to_clone}/sendgrid-wp-configs.php root@${remote_IP}:/var/www/${new_url}/sendgrid-wp-configs.php.source |
          tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

        ssh root@${remote_IP} "sleep 1 && ls -l /var/www/${new_url}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      else
        echo "SMTP enabled for origin site.... but no SMTP configs, will be unable to enable at destination..." |
          tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      fi
      echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    fi

    echo "Syncing Nginx" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    ssh root@${remote_IP} "sleep 1 && sudo mkdir -p /var/www/${new_url}/nginx" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    ssh root@${remote_IP} "sleep 1 && sudo rm -rf /var/www/${new_url}/nginx/*" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/bin/rsync -avz /var/www/${site_to_clone}/nginx/* root@${remote_IP}:/var/www/${new_url}/nginx |
      tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    ssh root@${remote_IP} "sleep 1 && mv /var/www/${new_url}/nginx/${site_to_clone}-headers-csp.conf /var/www/${new_url}/nginx/${new_url}-headers-csp.conf.source" |
      tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi

  http_or_https=$(/usr/local/bin/gp conf -ngx type-check ${site_to_clone} -q)
  if [[ ${http_or_https} == *"https"* ]]; then
    echo "Origin is HTTPS, will send along SSL request..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    scheme="https"
  else
    scheme="http"
  fi

  notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${new_url}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>${site_to_clone} Clone package migrated to destination..."
  gridpane::notify::app \
    "Cloning/Migrate Notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${site_to_clone}"
  echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  ssh root@${remote_IP} "sleep 1 && cd /var/www/${new_url}/htdocs && sudo /usr/local/bin/gprestore -new-url ${site_to_clone} ${new_url} ${scheme} && sudo /usr/local/bin/gpbup force ${new_url}" |
    tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
}

gpclone::clone_to_remote() {
  if [[ -z $1 ]]; then
    echo "We need the original site url passing... exiting" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gpclone::end::failsync_pause::exit
  fi

  site_to_clone="$1"

  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "Site to Clone: ${site_to_clone}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "Remote IP ${remote_IP}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  if [[ $2 == "site-clone" ]]; then
    echo "Single Site Clone" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  else
    echo "Failover Clone" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi
  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  RETRY=0
  while ssh -n root@${remote_IP} [ ! -d /var/www/${site_to_clone}/htdocs/wp-content/plugins/nginx-helper ]; do
    echo "Waiting for remote site to provision (max 300s)... ${RETRY}s" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sleep 1

    RETRY=$(($RETRY + 1))
    if [[ ${RETRY} -gt 300 ]]; then
      if [[ $2 == "site-clone" ]]; then
        notification_body="Building new ${site_to_clone} site on ${remote_IP} stalled, exiting process..."
        notification_duration="infinity"
        next="gpclone::end::failsync_pause::exit"
      else
        notification_body="Building new '${site_to_clone}' site on '${remote_IP}' stalled, moving on to next..."
        notification_duration="long"
        next="break"
      fi

      gridpane::notify::app \
        "Cloning/Migrate Notice" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "${notification_duration}" \
        "${site_to_clone}"
      echo "Cloning/Migrate Notice" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      "${next}"
    fi
  done

  if [[ $3 == "true"* ]]; then
    if [[ $3 == *"sudomain" ]]; then
      multisite_type="-multisubdom"
    elif [[ $3 == *"subdirectory" ]]; then
      multisite_type="-multisubdir"
    fi
    echo "Converting ${remote_IP} ${site_to_clone} to multisite ${3}..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    ssh root@${remote_IP} "sleep 1 && /usr/local/bin/gp site ${site_to_clone} ${multisite_type}" | tee -a /opt/gridpane/migrate-clone.log
  fi

  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  scp /var/www/${site_to_clone}/GPBUP-${site_to_clone}-CLONE.gz root@${remote_IP}:/var/www/${site_to_clone}/GPBUP-${site_to_clone}-CLONE.gz |
    tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "Copied local WP export file for ${site_to_clone} to remote system ${remote_IP}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  ssh root@${remote_IP} "ls -l /var/www/${site_to_clone}" | tee -a /opt/gridpane/migrate-clone.log

  check_for_promethean_remote=$(ssh root@${remote_IP} "/usr/local/bin/gp conf read server-build -q" 2>&1)
  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "Destination is: ${check_for_promethean_remote}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  if [[ ${check_for_promethean_remote} == "prometheus" ]]; then
    if [[ $2 == "site-clone" ]]; then
      if [[ -f /var/www/${site_to_clone}/user-configs.php ]]; then
        echo "Sending over user wp-config.php includes..." | tee -a /opt/gridpane/migrate-clone.log
        scp /var/www/${site_to_clone}/user-configs.php root@${remote_IP}:/var/www/${site_to_clone}/user-configs.php |
          tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      fi

      if [[ $(cat /etc/nginx/sites-available/${site_to_clone}) == *"HTTPS"* ]]; then
        [[ "$(gridpane::conf_read ssl -site.env ${site_to_clone})" != "true" ]] &&
          gridpane::conf_write ssl true -site.env ${site_to_clone}
        echo "SSL enabled for origin site.... syncing temporary SSL certificates..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

        if [[ -d /etc/letsencrypt/archive/${site_to_clone} ]]; then
          ssh root@${remote_IP} "sudo mkdir -p /etc/letsencrypt/archive/${site_to_clone} && sudo rm -rf /etc/letsencrypt/archive/${site_to_clone}/*" |
            tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          /usr/bin/rsync -avz /etc/letsencrypt/archive/${site_to_clone}/* root@${remote_IP}:/etc/letsencrypt/archive/${site_to_clone} | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        fi

        if [[ -d /etc/letsencrypt/live/${site_to_clone} ]]; then
          ssh root@${remote_IP} "sudo mkdir -p /etc/letsencrypt/live/${site_to_clone} && sudo rm -rf /etc/letsencrypt/live/${site_to_clone}/*" |
            tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          /usr/bin/rsync -avz /etc/letsencrypt/live/${site_to_clone}/* root@${remote_IP}:/etc/letsencrypt/live/${site_to_clone} | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        fi

        echo "Synching ACME SSL" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        if [[ -d /etc/nginx/ssl/${site_to_clone} ]]; then
          ssh root@${remote_IP} "sudo mkdir -p /etc/nginx/ssl/${site_to_clone} && sudo rm -rf /etc/nginx/ssl/${site_to_clone}/*" |
            tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          /usr/bin/rsync -avz /etc/nginx/ssl/${site_to_clone}/* root@${remote_IP}:/etc/nginx/ssl/${site_to_clone} | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        fi

        if [[ -d /root/gridenv/acme-wildcard-configs/${site_to_clone} ]]; then
          ssh root@${remote_IP} "mkdir -p /root/gridenv/acme-wildcard-configs/${site_to_clone} && sudo rm -rf /root/gridenv/acme-wildcard-configs/${site_to_clone}/*" |
            tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          /usr/bin/rsync -avz --exclude '*.acme.sh.log' /root/gridenv/acme-wildcard-configs/${site_to_clone}/* root@${remote_IP}:/root/gridenv/acme-wildcard-configs/${site_to_clone} |
            tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        fi

        echo "Syncing ACME CRON" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        crontab -l >/tmp/tempcron
        rm /tmp/tempcron

        if [[ "$(cat /tmp/tempcron)" == *"${site_to_clone}"* ]]; then
          echo "${site_to_clone} CRON" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          min=$(echo $((1 + RANDOM % 59)))
          hr=$(echo $((1 + RANDOM % 23)))
          ssh root@${remote_IP} "crontab -l > /tmp/tempcron && echo \"${min} ${hr} * * * \"/root/.acme.sh\"/acme.sh --cron --home \"/root/.acme.sh\" --config-home /root/gridenv/acme-wildcard-configs/${site_to_clone} > /dev/null\" >> /tmp/tempcron && crontab /tmp/tempcron && rm /tmp/tempcron" |
            tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        fi

        echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      fi

      if [[ "$(gridpane::conf_read waf -site.env ${site_to_clone})" == "modsec" ]]; then
        echo "Sync ModSec files" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

        ssh root@${remote_IP} "sudo mkdir -p /var/www/${site_to_clone}/modsec" |
          tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

        scp /var/www/${site_to_clone}/modsec/${site_to_clone}-crs-anomaly-blocking-threshold.conf root@${remote_IP}:/var/www/${site_to_clone}/modsec/${site_to_clone}-crs-anomaly-blocking-threshold.conf.source |
          tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        scp /var/www/${site_to_clone}/modsec/${site_to_clone}-crs-paranoia-level.conf root@${remote_IP}:/var/www/${site_to_clone}/modsec/${site_to_clone}-crs-paranoia-level.conf.source |
          tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        scp /var/www/${site_to_clone}/modsec/${site_to_clone}-runtime-exceptions.conf root@${remote_IP}:/var/www/${site_to_clone}/modsec/${site_to_clone}-runtime-exceptions.conf.source |
          tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        scp /var/www/${site_to_clone}/modsec/${site_to_clone}-runtime-whitelist.conf root@${remote_IP}:/var/www/${site_to_clone}/modsec/${site_to_clone}-runtime-whitelist.conf.source |
          tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

        echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      fi

      is_smtp=$(gridpane::conf_read smtp -site.env ${site_to_clone})
      if [[ ${is_smtp} == "true" ]] || [[ ${is_smtp} == "on" ]]; then
        if [[ -f /var/www/${site_to_clone}/sendgrid-wp-configs.php ]]; then
          echo "SMTP enabled for origin site.... syncing required configs to set SMTP up on destination..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          scp /var/www/${site_to_clone}/sendgrid-wp-configs.php root@${remote_IP}:/var/www/${site_to_clone}/sendgrid-wp-configs.php.source |
            tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        else
          echo "SMTP enabled for origin site.... but no SMTP configs, will be unable to enable at destination..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        fi
      fi

      echo "Sync Nginx Files" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      ssh root@${remote_IP} "sudo mkdir -p /var/www/${site_to_clone}/nginx"
      ssh root@${remote_IP} "sudo rm -rf /var/www/${site_to_clone}/nginx/*"
      /usr/bin/rsync -avz /var/www/${site_to_clone}/nginx/* root@${remote_IP}:/var/www/${site_to_clone}/nginx | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      if [[ -d /var/www/${site_to_clone}/dns ]]; then
        echo "Sync DNS creds" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        ssh root@${remote_IP} "sudo mkdir -p /var/www/${site_to_clone}/dns"
        ssh root@${remote_IP} "sudo rm -rf /var/www/${site_to_clone}/dns/*"
        /usr/bin/rsync -avz /var/www/${site_to_clone}/dns/* root@${remote_IP}:/var/www/${site_to_clone}/dns | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      fi
    fi
  fi

  notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${site_to_clone}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>${site_to_clone} Clone package migrated to destination..."
  gridpane::notify::app \
    "Cloning/Migrate Notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${site_to_clone}"
  echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  ssh root@${remote_IP} "cd /var/www/${site_to_clone}/htdocs && sudo /usr/local/bin/gprestore && sudo /usr/local/bin/gpbup force ${site_to_clone}" |
    tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  echo "Restored ${site_to_clone} export/clone to remote system ${remote_IP}, and performed a backup." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  if [[ -f /opt/gridpane/jeff.says.swap ]]; then
    echo "Checking for ${site_to_clone} Add on domains..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridclone::process::clone_domains alias "${site_to_clone}"
    gridclone::process::clone_domains redirect "${site_to_clone}"
    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi

  gridpane::callback::app \
    "$(
      /usr/bin/jo \
        site_url=${site_to_clone} \
        server_ip=${remote_IP} \
        failover_set_at=now \
        silent@True
    )" \
    "/site/site-update" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  sleep 1
}

gridclone::process::clone_domains() {
  if [[ $1 == "alias" ]]; then
    domain_env="/var/www/$2/logs/$2-additional-domains.env"
  else
    domain_env="/var/www/$2/logs/$2-301-domains.env"
  fi

  if [[ -f ${domain_env} ]]; then
    while read add_on_domain_from_original; do
      add_on_domain_from_original=${add_on_domain_from_original#:}
      add_on_domain_from_original=${add_on_domain_from_original%:}

      if [[ -f /var/www/${site_to_clone}/dns/${add_on_domain_from_original}.creds ]]; then
        add_on_domain_creds=$(cat /var/www/${site_to_clone}/dns/${add_on_domain_from_original}.creds)

        if [[ ${add_on_domain_creds} == *"dnsme"* ]]; then
          dns_management="dnsme"
        elif [[ ${add_on_domain_creds} == *"cloudflare"* ]]; then
          dns_management="cloudflare"
        fi

        if [[ ${add_on_domain_creds} == *"challenge-domain"* ]]; then
          dns_management="${dns_management}_challenge"
        else
          dns_management="${dns_management}_full"
        fi
      else
        dns_management="none_none"
      fi

      echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      echo "Adding ${add_on_domain_from_original}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      echo "Type: $1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      echo "DNS Management: ${dns_management}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      curl \
        -d '{ "site_url":"'${site_to_clone}'", "server_ip":"'${remote_IP}'", "domain_url":"'${add_on_domain_from_original}'", "type":"'$1'", "dns_management":"'${dns_management}'" }' \
        -H "Content-Type: application/json" \
        -X POST https://${GPURL}/api/domain/add-domain?api_token=${gridpanetoken} | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      if [[ $(cat /etc/nginx/sites-available/${add_on_domain_from_original}) == *"HTTPS"* ]]; then
        echo "SSL enabled for ${add_on_domain_from_original} syncing temporary SSL certificates..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

        if [[ -d /etc/letsencrypt/archive/${add_on_domain_from_original} ]]; then
          ssh root@${remote_IP} "sleep 1 && sudo mkdir -p /etc/letsencrypt/archive/${add_on_domain_from_original} && sudo rm -rf /etc/letsencrypt/archive/${add_on_domain_from_original}/*" |
            tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          /usr/bin/rsync -avz /etc/letsencrypt/archive/${add_on_domain_from_original}/* root@${remote_IP}:/etc/letsencrypt/archive/${add_on_domain_from_original} | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        fi

        if [[ -d /etc/letsencrypt/live/${add_on_domain_from_original} ]]; then
          ssh root@${remote_IP} "sleep 1 && sudo mkdir -p /etc/letsencrypt/live/${add_on_domain_from_original} && sudo rm -rf /etc/letsencrypt/live/${add_on_domain_from_original}/*" |
            tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          /usr/bin/rsync -avz /etc/letsencrypt/live/${add_on_domain_from_original}/* root@${remote_IP}:/etc/letsencrypt/live/${add_on_domain_from_original} | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        fi

        echo "Synching ACME SSL" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

        if [[ -d /etc/nginx/ssl/${add_on_domain_from_original} ]]; then
          ssh root@${remote_IP} "sleep 1 && sudo mkdir -p /etc/nginx/ssl/${add_on_domain_from_original} && sudo rm -rf /etc/nginx/ssl/${add_on_domain_from_original}/*" |
            tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          /usr/bin/rsync -avz /etc/nginx/ssl/${add_on_domain_from_original}/* root@${remote_IP}:/etc/nginx/ssl/${add_on_domain_from_original} | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        fi

        if [[ -d /root/gridenv/acme-wildcard-configs/${add_on_domain_from_original} ]]; then
          ssh root@${remote_IP} "sleep 1 && mkdir -p /root/gridenv/acme-wildcard-configs/${add_on_domain_from_original} && sudo rm -rf /root/gridenv/acme-wildcard-configs/${add_on_domain_from_original}/*" |
            tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          /usr/bin/rsync -avz --exclude '*.acme.sh.log' /root/gridenv/acme-wildcard-configs/${add_on_domain_from_original}/* root@${remote_IP}:/root/gridenv/acme-wildcard-configs/${add_on_domain_from_original} |
            tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        fi

        echo "Syncing ACME CRON" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        crontab -l >/tmp/tempcron
        rm /tmp/tempcron

        if [[ "$(cat /tmp/tempcron)" == *"${add_on_domain_from_original}"* ]]; then
          echo "${add_on_domain_from_original} CRON" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          min=$(echo $((1 + RANDOM % 59)))
          hr=$(echo $((1 + RANDOM % 23)))
          ssh root@${remote_IP} "sleep 1 && crontab -l > /tmp/tempcron && echo \"${min} ${hr} * * * \"/root/.acme.sh\"/acme.sh --cron --home \"/root/.acme.sh\" --config-home /root/gridenv/acme-wildcard-configs/${add_on_domain_from_original} > /dev/null\" >> /tmp/tempcron && crontab /tmp/tempcron && rm /tmp/tempcron" |
            tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        fi

        RETRY=0
        local domain_added="false"
        while [[ ${domain_added} == "false" ]]; do
          enabled_remote_virtual_servers=$(ssh -n root@${remote_IP} "ls /etc/nginx/sites-enabled/${add_on_domain_from_original}")

          if [[ ${enabled_remote_virtual_servers} == *" ${add_on_domain_from_original} "* ]]; then
            echo "Domain ${add_on_domain_from_original} added to remote ${remote_IP}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
            domain_added="true"
          else
            echo "Waiting for domain ${add_on_domain_from_original} to be added to remote ${remote_IP}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
            sleep 1
          fi

          RETRY=$(($RETRY + 1))
          if [[ ${RETRY} -gt 300 ]]; then
            break
          fi
        done

        if [[ ${domain_added} == "true" ]]; then
          if [[ $1 == "alias" ]]; then
            gen_remote_config=$(ssh root@${remote_IP} "sleep 1 && /usr/local/bin/gp conf nginx generate $(/usr/local/bin/gp conf nginx type-check ${site_to_clone}-add-on return-https return-no-wildcard -q) $(/usr/local/bin/gp conf nginx cache-check ${site_to_clone} -q) ${site_to_clone} ${add_on_domain_from_original}")
          else
            gen_remote_config=$(ssh root@${remote_IP} "sleep 1 && /usr/local/bin/gp conf nginx generate $(/usr/local/bin/gp conf nginx type-check ${site_to_clone}-add-on return-https return-no-wildcard -q) ${site_to_clone} ${add_on_domain_from_original}")
          fi

          if [[ ${gen_remote_config} != *"pain"* ]]; then
            echo "Reconfiguring Nginx..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
            local check_remote_https_domain=$(ssh root@${remote_IP} "sleep 1 && cat /etc/nginx/sites-enabled/${add_on_domain_from_original}")
            reload_nginx="true"

            if [[ ${check_remote_https_domain} == *"HTTPS"* ]]; then
              curl \
                -d '{ "site_url":"'${site_to_clone}'", "domain_url":"'"${add_on_domain_from_original}"'", "server_ip":"'"${remote_IP}"'", "ssl":true, "chain_ssl":false, "status":"success", "details":"SSL for '"${add_on_domain_from_original}"' on '"${remote_IP}"' Success!"}' \
                -H "Content-Type: application/json" \
                -X POST https://${GPURL}/api/domain/domain-update?api_token=${gridpanetoken} | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
            fi
            check_remote_https_domain=""
          fi
        fi
      fi
    done </var/www/${site_to_clone}/logs/${site_to_clone}-additional-domains.env
  else
    echo "No ${site_to_clone} $1 domains found..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi
}

gpclone::add::remote_site() {
  if [[ -z $1 ]] || [[ -z $2 ]]; then
    echo "We need the original site url and the remote IP address passing... exiting"
    gpclone::end::failsync_pause::exit
  fi

  local site_to_clone="$1"
  #  local remote_IP="$2"
  local failover="$3"

  local dns_management=$(gridpane::conf_read dns-management -site.env ${site_to_clone})
  if [[ -z ${dns_management} ]] || [[ ${dns_management} == *"${site_to_clone} doesn't exist... do nothing"* ]]; then
    gpclone::process::site_dns ${site_to_clone}
    gridpane::conf_write dns-management ${dns_management} -site.env ${site_to_clone}
  fi

  if [[ -d "/var/www/staging.${site_to_clone}" && -d "/var/www/canary.${site_to_clone}" ]]; then
    echo "Unsynced site ${site_to_clone} found with staging and updates, building three remote sites..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    if [[ ${failover} == "true" ]]; then
      echo "This is a Snapshot Failover Sync... creating synced-to/synced-from logs.." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      gpcurl=$(curl -d '{"server_ip":"'${remote_IP}'", "source_ip":"'${serverIP}'", "dns_management":"'${dns_management}'", "failover":"'${failover}'", "url":"'${site_to_clone}'", "checkedAdvancedOptions":["staging", "canary"]}' -H "Content-Type: application/json" -X POST https://${GPURL}/api/add-site?api_token=${gridpanetoken} 2>&1)

      mkdir -p /var/www/staging.${site_to_clone}/logs
      touch /var/www/staging.${site_to_clone}/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/staging.${site_to_clone}/logs/synced-to.log

      mkdir -p /var/www/canary.${site_to_clone}/logs
      touch /var/www/canary.${site_to_clone}/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/canary.${site_to_clone}/logs/synced-to.log

      mkdir -p /var/www/${site_to_clone}/logs
      touch /var/www/${site_to_clone}/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/${site_to_clone}/logs/synced-to.log

      ssh root@${remote_IP} "mkdir -p /var/www/staging.${site_to_clone}/logs && touch /var/www/staging.${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/staging.${site_to_clone}/logs/synced-from.log"
      ssh root@${remote_IP} "mkdir -p /var/www/canary.${site_to_clone}/logs && touch /var/www/canary.${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/canary.${site_to_clone}/logs/synced-from.log"
      ssh root@${remote_IP} "mkdir -p /var/www/${site_to_clone}/logs && touch /var/www/${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/${site_to_clone}/logs/synced-from.log"
    else
      echo "This is NOT a Snapshot Failover Sync, this is a migration, not creating synced-to/synced-from logs..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      gpcurl=$(curl -d '{"server_ip":"'${remote_IP}'", "source_ip":"'${serverIP}'", "url":"'${site_to_clone}'", "dns_management":"'${dns_management}'", "checkedAdvancedOptions":["staging", "canary"]}' -H "Content-Type: application/json" -X POST https://${GPURL}/api/add-site?api_token=${gridpanetoken} 2>&1)
    fi

    echo "${gpcurl}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sleep 1
  elif [[ -d "/var/www/staging.${site_to_clone}" ]]; then
    echo "Unsynced site ${site_to_clone} found with staging, building two remote sites..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    if [[ ${failover} == "true" ]]; then
      echo "This is a Snapshot Failover Sync... creating synced-to/synced-from logs.." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      gpcurl=$(curl -d '{"server_ip":"'${remote_IP}'", "source_ip":"'${serverIP}'", "dns_management":"'${dns_management}'", "failover":"'${failover}'", "url":"'${site_to_clone}'", "checkedAdvancedOptions":["staging"]}' -H "Content-Type: application/json" -X POST https://${GPURL}/api/add-site?api_token=${gridpanetoken} 2>&1)

      mkdir -p /var/www/staging.${site_to_clone}/logs
      touch /var/www/staging.${site_to_clone}/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/staging.${site_to_clone}/logs/synced-to.log

      mkdir -p /var/www/${site_to_clone}/logs
      touch /var/www/${site_to_clone}/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/${site_to_clone}/logs/synced-to.log

      ssh root@${remote_IP} "mkdir -p /var/www/staging.${site_to_clone}/logs && touch /var/www/staging.${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/staging.${site_to_clone}/logs/synced-from.log"
      ssh root@${remote_IP} "mkdir -p /var/www/${site_to_clone}/logs && touch /var/www/${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/${site_to_clone}/logs/synced-from.log"
    else
      echo "This is NOT a Snapshot Failover Sync, this is a migration, not creating synced-to/synced-from logs..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      gpcurl=$(curl -d '{"server_ip":"'${remote_IP}'", "source_ip":"'${serverIP}'", "dns_management":"'${dns_management}'", "url":"'${site_to_clone}'", "checkedAdvancedOptions":["staging"]}' -H "Content-Type: application/json" -X POST https://${GPURL}/api/add-site?api_token=${gridpanetoken} 2>&1)
    fi

    echo "${gpcurl}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sleep 1
  elif [[ -d "/var/www/canary.${site_to_clone}" ]]; then
    echo "Unsynced site ${site_to_clone} found with updates, building two remote sites..."
    if [[ ${failover} == "true" ]]; then
      echo "This is a Snapshot Failover Sync... creating synced-to/synced-from logs.." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      gpcurl=$(curl -d '{"server_ip":"'${remote_IP}'", "source_ip":"'${serverIP}'", "dns_management":"'${dns_management}'", "failover":"'${failover}'", "url":"'${site_to_clone}'", "checkedAdvancedOptions":["canary"]}' -H "Content-Type: application/json" -X POST https://${GPURL}/api/add-site?api_token=${gridpanetoken} 2>&1)

      mkdir -p /var/www/canary.${site_to_clone}/logs
      touch /var/www/canary.${site_to_clone}/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/canary.${site_to_clone}/logs/synced-to.log

      mkdir -p /var/www/${site_to_clone}/logs
      touch /var/www/${site_to_clone}/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/${site_to_clone}/logs/synced-to.log

      ssh root@${remote_IP} "mkdir -p /var/www/canary.${site_to_clone}/logs && touch /var/www/canary.${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/canary.${site_to_clone}/logs/synced-from.log"
      ssh root@${remote_IP} "mkdir -p /var/www/${site_to_clone}/logs && touch /var/www/${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/${site_to_clone}/logs/synced-from.log"
    else
      echo "This is NOT a Snapshot Failover Sync, this is a migration, not creating synced-to/synced-from logs..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      gpcurl=$(curl -d '{"server_ip":"'${remote_IP}'", "source_ip":"'${serverIP}'", "dns_management":"'${dns_management}'", "url":"'${site_to_clone}'", "checkedAdvancedOptions":["canary"]}' -H "Content-Type: application/json" -X POST https://${GPURL}/api/add-site?api_token=${gridpanetoken} 2>&1)
    fi

    echo "${gpcurl}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sleep 1
  else
    echo "Unsynced site ${site_to_clone} found with no staging or updates, building one remote site..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    if [[ ${failover} == "true" ]]; then
      echo "This is a Snapshot Failover Sync... creating synced-to/synced-from logs.." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      gpcurl=$(curl -d '{"server_ip":"'${remote_IP}'", "source_ip":"'${serverIP}'", "dns_management":"'${dns_management}'", "failover":"'${failover}'", "url":"'${site_to_clone}'"}' -H "Content-Type: application/json" -X POST https://${GPURL}/api/add-site?api_token=${gridpanetoken} 2>&1)

      mkdir -p /var/www/${site_to_clone}/logs
      touch /var/www/${site_to_clone}/logs/synced-to.log
      echo "${remote_IP}" >>/var/www/${site_to_clone}/logs/synced-to.log

      ssh root@${remote_IP} "mkdir -p /var/www/${site_to_clone}/logs && touch /var/www/${site_to_clone}/logs/synced-from.log && echo ${serverIP} >> /var/www/${site_to_clone}/logs/synced-from.log"
    else
      echo "This is NOT a Snapshot Failover Sync, this is a migration, not creating synced-to/synced-from logs..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      gpcurl=$(curl -d '{"server_ip":"'${remote_IP}'", "source_ip":"'${serverIP}'", "dns_management":"'${dns_management}'", "url":"'${site_to_clone}'"}' -H "Content-Type: application/json" -X POST https://${GPURL}/api/add-site?api_token=${gridpanetoken} 2>&1)
    fi

    echo "${gpcurl}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sleep 1
  fi
}

gpclone::add::new_site() {
  if [[ -z $1 ]] || [[ -z $2 ]]; then
    echo "We need to know if you want to make a site on the same server or a new server, and the url of the site to make... exiting"
    gpclone::end::failsync_pause::exit
  fi

  same_or_different_server="$1"
  site_to_add="$2"

  local dns_management=$(gridpane::conf_read dns-management -site.env ${site_to_add})
  if [[ -z ${dns_management} ]] || [[ ${dns_management} == *"${site_to_add} doesn't exist... do nothing"* ]]; then
    gpclone::process::site_dns ${site_to_clone}
    gridpane::conf_write dns-management ${dns_management} -site.env ${site_to_clone}
  fi

  [[ -d /var/www/${site_to_add}/htdocs ]] && gridpane::conf_write dns-management ${dns_management} -site.env ${site_to_add}

  if [[ ${same_or_different_server} == *"same-server" ]]; then
    server_IP="${serverIP}"
    source_IP="${serverIP}"
  elif [[ ${same_or_different_server} == *"different-server" ]]; then
    if [[ -z $3 ]]; then
      echo "No remote IP passed, we can't make this site on a different server... exiting..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      gpclone::end::failsync_pause::exit
    fi

    check_ip=$(gridpane::ip_check $3 "clone")
    echo "${check_ip}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    server_IP="$3"
    source_IP="${serverIP}"
  else
    echo "Unsure if you want to clone to the same server or a different server, exiting..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gpclone::end::failsync_pause::exit
  fi

  echo "server_ip ${server_IP}"
  echo "source_ip ${source_IP}"
  echo "dns_management ${dns_management}"
  echo "url ${site_to_add}"

  make_new_site_curl=$(curl -d '{"server_ip":"'"${server_IP}"'", "source_ip":"'"${source_IP}"'", "dns_management":"'"${dns_management}"'", "url":"'"${site_to_add}"'" }' -H "Content-Type: application/json" -X POST https://${GPURL}/api/add-site?api_token=${gridpanetoken})
  echo "${make_new_site_curl}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  sleep 1
}

gpclone::process::site_dns() {
  if [[ -f /var/www/$1/dns/$1.creds ]]; then

    dns_creds_file=$(cat /var/www/$1/dns/$1.creds)

    if [[ ${dns_creds_file} == *"dnsme"* ]]; then
      dns_management="dnsme"
    elif [[ ${dns_creds_file} == *"cloudflare"* ]]; then
      dns_management="cloudflare"
    fi

    if [[ ${dns_creds_file} == *"challenge"* ]]; then
      dns_management="${dns_management}_challenge"
    else
      dns_management="${dns_management}_full"
    fi

  else
    dns_management="none_none"
  fi
}

gpclone::extract::replace() {
  if [[ -z $1 ]] || [[ -z $2 ]]; then
    echo "We need to know the source and destination urls.. exiting..."
    gpclone::end::failsync_pause::exit
  fi

  source="$1"
  dest="$2"
  is_multisite="$3"
  dest_siteowner=$(gridpane::get::set::site::user ${dest})

  if [[ ! -f /var/www/${dest}/GPBUP-${source}-CLONE.gz ]]; then
    echo "Error! Archive missing, Exiting!!!" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gpclone::end::failsync_pause::exit
  fi

  rm -rf /var/www/${dest}/htdocs/*
  tar -xf /var/www/${dest}/GPBUP-${source}-CLONE.gz -C /var/www/${dest}/htdocs --overwrite | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  echo "Files extracted to ${dest} folder..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  ls -l /var/www/${dest}/htdocs | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  echo "Move clone env..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  mv /var/www/${dest}/htdocs/${source}.env.clone /var/www/${dest}/logs/${source}.env.clone
  chown -R ${dest_siteowner}:${dest_siteowner} /var/www/${dest}/htdocs/
  cd /var/www/${dest}/htdocs

  tableprefix=$(cat /var/www/${dest}/htdocs/table.prefix)
  if [[ -z ${tableprefix} ]]; then
    echo "Table prefix missing, sticking to the defaults..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  else
    sed -i "/\$table_prefix =/c\\${tableprefix}" /var/www/${dest}/wp-config.php
    echo "Table Prefixes Updated..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi

  if [[ -f /var/www/${dest}/htdocs/database.gz ]]; then
    echo "Extracting /var/www/${dest}/htdocs/database.gz DB and importing now..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    tar -xzf /var/www/${dest}/htdocs/database.gz | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  elif [[ -f /var/www/${dest}/database.gz ]]; then
    echo "Extracting /var/www/${dest}/database.gz DB and importing now..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    tar -xzf /var/www/${dest}/database.gz | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    mv /var/www/${dest}/database.sql /var/www/${dest}/htdocs/database.sql
  fi

  if [[ ! -f /var/www/${dest}/htdocs/database.sql ]]; then
    notification_body="Process Failed<br>Source:${source}<br>Destination:${dest}<br>Source Server:${serverIP}<br>Destination Server:${serverIP}<br>Database File is missing..."
    gridpane::notify::app \
      "Cloning/Migrate Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  else
    echo "Importing DB /var/www/${dest}/htdocs/database.sql now..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Drop Database for ${dest}..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 300 /usr/local/bin/wp db drop --yes --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Creating new Database for ${dest}..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 300 /usr/local/bin/wp db create --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Importing ${source} db to ${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    chown ${dest_siteowner}:${dest_siteowner} /var/www/${dest}/htdocs/database.sql
    import_db=$(sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp db import /var/www/${dest}/htdocs/database.sql --path=/var/www/${dest}/htdocs 2>&1")
    echo "${import_db}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi

  if [[ ${import_db} == *"uccess"* ]]; then
    echo "Database Imported..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    chmod 600 /var/www/${dest}/wp-config.php

    if [[ -f /var/www/${dest}/htdocs/database.gz ]]; then
      rm /var/www/${dest}/htdocs/database.gz
      echo "Removing the compressed DB..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    fi

    if [[ -f /var/www/${dest}/htdocs/database.sql ]]; then
      echo "Removed DB Backup..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      rm /var/www/${dest}/htdocs/database.sql
    else
      echo "No DB file to remove!" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    fi

    rm /var/www/${dest}/htdocs/table.prefix
    rm /var/www/${dest}/htdocs/GPBUP-${source}-CLONE.gz

    scheme="http"

    echo "Search replace - http://${source} -> ${scheme}://${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'http://${source}' '${scheme}://${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Search replace - http://www.${source} -> ${scheme}://${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'http://www.${source}' '${scheme}://${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Search replace - http:\/\/${source} -> ${scheme}:\/\/${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'http:\/\/${source}' '${scheme}:\/\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Search replace - http:\/\/www.${source} -> ${scheme}:\/\/${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'http:\/\/www.${source}' '${scheme}:\/\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Search replace - http%3A%2F%2F${source} -> ${scheme}%3A%2F%2F${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'http%3A%2F%2F${source}' '${scheme}%3A%2F%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Search replace - http%3A%2F%2Fwww.${source} -> ${scheme}%3A%2F%2F${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'http%3A%2F%2Fwww.${source}' '${scheme}%3A%2F%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Search replace - https://${source} -> ${scheme}://${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'https://${source}' '${scheme}://${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Search replace - https://www.${source} -> ${scheme}://${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'https://www.${source}' '${scheme}://${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Search replace - https:\/\/${source} -> ${scheme}:\/\/${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'https:\/\/${source}' '${scheme}:\/\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Search replace - https:\/\/www.${source} -> ${scheme}:\/\/${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'https:\/\/www.${source}' '${scheme}:\/\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Search replace - https%3A%2F%2F${source} -> ${scheme}%3A%2F%2F${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'https%3A%2F%2F${source}' '${scheme}%3A%2F%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Search replace - https%3A%2F%2Fwww.${source} -> ${scheme}%3A%2F%2F${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'https%3A%2F%2Fwww.${source}' '${scheme}%3A%2F%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Search replace - www/${source} -> www/${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'www/${source}' 'www/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Search replace - www\/${source} -> www\/${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'www\/${source}' 'www\/${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Search replace - www%2F${source} -> www%2F${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace 'www%2F${source}' 'www%2F${dest}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    if [[ ${is_multisite} == "true"* ]]; then
      echo "------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      echo "Multisite Clone Search Replace - WP_SITE WP_BLOGS Tables" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      echo "Pattern to search replace:  ${source} -> ${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace '${source}' '${dest}' wp_site wp_blogs --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      echo "------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      echo "Multisite Primary Domain Swap - WP_*_Options Tables" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      echo "Pattern to search replace:  .${source} -> .${dest}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      sudo su - ${dest_siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace '.${source}' '.${dest}' 'wp_*options' --report-changed-only --precise --path=/var/www/${dest}/htdocs 2>&1" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    fi

    echo "Reset Permissions and Clear Cache" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp fix cached ${dest} | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp fix perms ${dest} | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  else
    echo "Failed to import DB..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    touch /tmp/${nonce}.failed.db.export
  fi
}

gpclone::export::db_only() {
  if [[ -z $1 ]]; then
    echo "We need to know the source site to export the db from... exiting..."
    gpclone::end::failsync_pause::exit
  fi

  local site_to_export_db="$1"

  echo "Processing DB only export at /var/www/${site_to_export_db}/htdocs ..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  cd /var/www/${site_to_export_db}/htdocs

  local checkwp=$(cat /var/www/${site_to_export_db}/htdocs/index.php)
  if [[ ${checkwp} != *"Front to the WordPress application. This file doesn't do anything"* ]]; then
    echo "This is not a valid WordPress install, skipping!!!" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  else
    echo "We have a config problem and WP-CLI can't run - attempting manual mysqldump..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    WPDBNAME=$(cat /var/www/${site_to_export_db}/wp-config.php | grep DB_NAME | cut -d \' -f 4)
    WPDBUSER=$(cat /var/www/${site_to_export_db}/wp-config.php | grep DB_USER | cut -d \' -f 4)
    WPDBPASS=$(cat /var/www/${site_to_export_db}/wp-config.php | grep DB_PASSWORD | cut -d \' -f 4)
    mysqldump -u${WPDBUSER} -p${WPDBPASS} ${WPDBNAME} >/var/www/${site_to_export_db}/database.sql

    if [ "$?" -eq 0 ]; then
      echo "MySQL Dump Success..."
      chmod 400 /var/www/${site_to_export_db}/database.sql
      echo "DB Export of ${site_to_export_db} Done!" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    else
      rm /var/www/${site_to_clone}/htdocs/database.sql
      echo "DB Export of ${site_to_export_db} Failed!" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    fi
  fi
}

gpclone::sync::to_remote() {
  if [[ -z $1 ]] || [[ -z $2 ]]; then
    echo "We need to know the site to sync and the remote IP to sync to... exiting..."
    gpclone::end::failsync_pause::exit
  fi

  check_ip=$(gridpane::ip_check $2 "clone")
  echo "${check_ip}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  site_to_sync="$1"
  #  remote_IP="$2"

  echo "Copying DB only..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  scp /var/www/${site_to_sync}/user-configs.php root@${remote_IP}:/var/www/${site_to_sync}/user-configs.php | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  scp /var/www/${site_to_sync}/database.sql root@${remote_IP}:/var/www/${site_to_sync}/database.sql | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  rm /var/www/${site_to_sync}/database.sql

  echo "Syncing site files for ${site_to_sync} to remote server ${remote_IP} via unison..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  DoUni=$(unison /var/www/${site_to_sync}/htdocs ssh://${remote_IP}//var/www/${site_to_sync}/htdocs -prefer newer -batch)

  echo "${DoUni}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  if [[ ${DoUni} == *"unison: command not found"* ]]; then
    echo "Unison not installed on remote, resolving..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    ssh root@${remote_IP} "sleep 1 && apt-get install -y unison" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "Reattempting copy..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    DoUni=$(unison /var/www/${site_to_sync}/htdocs ssh://${remote_IP}//var/www/${site_to_sync}/htdocs -prefer newer -batch)
    echo "${DoUni}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi

  echo "Syncing site nginx files for ${site_to_sync} to remote server ${remote_IP} via unison..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  unison /var/www/${site_to_sync}/nginx ssh://${remote_IP}//var/www/${site_to_sync}/nginx -prefer newer -batch | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  echo "Syncing site modsec files for ${site_to_sync} to remote server ${remote_IP} via unison..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  unison /var/www/${site_to_sync}/modsec ssh://${remote_IP}//var/www/${site_to_sync}/modsec -prefer newer -batch | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  echo "Unison sync done... restoring" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  ssh root@${remote_IP} "sleep 1 && cd /var/www/${site_to_sync}/htdocs && /usr/local/bin/gprestore -sync-only" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  echo "Site ${site_to_sync} has been synced to remote system ${remote_IP}..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  sleep 1

  gridpane::callback::app \
    "$(
      /usr/bin/jo \
        site_url=${site_to_sync} \
        server_ip=${remote_IP} \
        failover_set_at=now \
        silent@True
    )" \
    "/site/site-update" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
}

gpclone::sync::ssl() {
  echo "Unison sync ssl..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  if [[ -z $1 ]]; then
    echo "We need to know the remote IP to sync ssl to... exiting..."
    gpclone::end::failsync_pause::exit
  fi

  check_ip=$(gridpane::ip_check $1 "clone")
  echo "${check_ip}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  #  remote_ip="$1"

  check_remote_rsync=$(ssh root@${remote_IP} "which rsync")
  if [[ -z ${check_remote_rsync} ]]; then
    echo "Installing rsync on destination..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    ssh root@${remote_IP} "apt-get -y install rsync" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi

  ssh root@${remote_IP} "sleep 1 && mkdir -p /etc/letsencrypt" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  echo "Syncing Certbot SSL" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  grid_subdom=$(cat /root/grid.subdom)
  if [[ ! -z ${grid_subdom} ]]; then
    grid_subdom_prefix=${grid_subdom%.gridpanevps.com}
    exclude_subdom="--exclude '${grid_subdom_prefix}*'"
  fi

  echo "exclude subdomain: ${exclude_subdom}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  /usr/bin/rsync -avz --exclude 'accounts' --exclude 'renewal' ${exclude_subdom} /etc/letsencrypt/ root@${remote_IP}:/etc/letsencrypt | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  echo "Syncing ACME SSL Configs" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  ssh root@${remote_IP} "sleep 1 && mkdir -p /root/gridenv/acme-wildcard-configs" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  /usr/bin/rsync -avz --exclude '*.acme.sh.log' --exclude '*.account.conf' /root/gridenv/acme-wildcard-configs/* root@${remote_IP}:/root/gridenv/acme-wildcard-configs | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  echo "Syncing ACME SSL" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  ssh root@${remote_IP} "sleep 1 && mkdir -p /etc/nginx/ssl" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  /usr/bin/rsync -avz /etc/nginx/ssl/* root@${remote_IP}:/etc/nginx/ssl | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  echo "Syncing ACME CRON" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  crontab -l >/tmp/tempcron
  current_cron=$(cat /tmp/tempcron)
  active_sites=$(cat /root/gridenv/active-sites.env)

  for currfolder in $(find /root/gridenv/acme-wildcard-configs -maxdepth 1 -mindepth 1 -type d); do

    entry=$(basename ${currfolder})

    if [[ ${current_cron} == *"${entry}"* ]] && [[ ${active_sites} == *"${entry}"* ]]; then

      echo "Checking remote ${entry} CRON" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      check_cron=$(ssh root@${remote_IP} "sleep 1 && crontab -l")
      if [[ ${check_cron} != *"/root/gridenv/acme-wildcard-configs/${entry}"* ]]; then
        echo "No cronjob for ${entry} SSL adding..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        min=$(echo $((1 + RANDOM % 59)))
        hr=$(echo $((1 + RANDOM % 23)))
        ssh root@${remote_IP} "sleep 1 && crontab -l > /tmp/tempcron && echo \"${min} ${hr} * * * \"/root/.acme.sh\"/acme.sh --cron --home \"/root/.acme.sh\" --config-home /root/gridenv/acme-wildcard-configs/${entry} > /dev/null\" >> /tmp/tempcron && crontab /tmp/tempcron && rm /tmp/tempcron" |
          tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      fi
    fi
  done
  echo "Rsync ssl done..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
}

gpclone::ensure::logs() {
  [[ -f /root/migrate-test.log ]] && mv /root/migrate-test.log /opt/gridpane/migrate-clone.log
  if [[ ! -f /opt/gridpane/migrate-clone.log ]]; then
    touch /opt/gridpane/migrate-clone.log
  else
    echo "" >/opt/gridpane/migrate-clone.log
  fi
}

gpclone::process::migrate() {
  site_to_clone="$1"
  destination="$2"

  if [[ "$(cat /root/gridenv/active-sites.env)" != *"${site_to_clone}"* ]]; then
    gridpane::site_loop::directories

    if [[ ${sitesDirectoryArray} == *"${site_to_clone}"* ]] && [[ ${sitesConfigsArray} == *"${site_to_clone}"* ]]; then
      echo Active site "${site_to_clone} missing from active sites env, healing..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      gridpane::conf_write ":${site_to_clone}:" active -active-sites.env
    else
      notification_body="Cloning Unable to proceed!<br>${site_to_clone} is reported as not being an active WordPress site on the server, please contact support to proceed..."
      gridpane::notify::app \
        "Cloning Fail" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      gpclone::end::failsync_pause::exit
    fi
  fi

  check_for_multisite=$(gridpane::check::multisite ${site_to_clone})
  if [[ ${check_for_multisite} == "true"* ]] && [[ ! -f /opt/gridpane/jeff.says.swap ]]; then
    notification_body="Cloning Unable to proceed!<br>${site_to_clone} is a multisite installation, GridPane cloning does not currently support this, we recommend AIO Migration Multisite Extension currently"
    gridpane::notify::app \
      "Cloning Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    gpclone::end::failsync_pause::exit
  fi

  if [[ -d "/var/www/${site_to_clone}" ]]; then
    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Building new ${destination} site directory and db table structure.."
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    gpclone::add::new_site -same-server ${destination}

    RETRY=0
    while [[ ! -d /var/www/${destination}/htdocs/wp-content/plugins/nginx-helper ]]; do
      echo "Waiting for ${destination} to provision (max 300s)... ${RETRY}s " | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      sleep 1

      RETRY=$(($RETRY + 1))
      if [[ ${RETRY} -gt 300 ]]; then
        notification_body="Building new '${destination}' site stalled, exiting process..."
        gridpane::notify::app \
          "Cloning Fail" \
          "${notification_body}" \
          "popup_and_center" \
          "fa-exclamation" \
          "infinity" \
          "${site_to_clone}"
        echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

        gpclone::end::failsync_pause::exit
      fi
    done

    echo "Created new site ${destination}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    if [[ ${check_for_multisite} == "true"* ]]; then
      if [[ ${check_for_multisite} == *"sudomain" ]]; then
        multisite_type="-multisubdom"
      elif [[ ${check_for_multisite} == *"subdirectory" ]]; then
        multisite_type="-multisubdir"
      fi
      echo "Converting ${destination} to multisite ${check_for_multisite}..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      /usr/local/bin/gp site ${destination} ${multisite_type} | tee -a /opt/gridpane/migrate-clone.log
    fi

    cd /var/www/${site_to_clone}/htdocs

    gpclone::export::site ${site_to_clone}

    if [[ -f /tmp/${nonce}.failed.db.export ]]; then
      notification_title="Cloning Fail"
      notification_body="Process Failed<br>Source:${site_to_clone}<br>Destination:${destination}<br>Source ${site_to_clone} DB Failed to export..."
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      rm /tmp/${nonce}.failed.db.export
    else
      notification_title="Cloning Notice"
      notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Source ${site_to_clone} exported.."
      notification_icon="fa-clone"
      notification_duration="default"
    fi

    gridpane::notify::app \
      "${notification_title}" \
      "${notification_body}" \
      "popup_and_center" \
      "${notification_icon}" \
      "${notification_duration}" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    mv /var/www/${site_to_clone}/GPBUP-${site_to_clone}-CLONE.gz /var/www/${destination}/GPBUP-${site_to_clone}-CLONE.gz | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    ls -l /var/www/${destination} | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    gpclone::extract::replace ${site_to_clone} ${destination} ${check_for_multisite}

    if [[ ! -f /tmp/${nonce}.failed.db.import ]]; then
      echo "Copying ${site_to_clone} nginx dir to ${destination}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      cp /var/www/${site_to_clone}/nginx/* /var/www/${destination}/nginx/ | tee -a /opt/gridpane/migrate-clone.log mv /var/www/${destination}/nginx/${site_to_clone}-headers-csp.conf /var/www/${destination}/nginx/${destination}-headers-csp.conf | tee -a /opt/gridpane/migrate-clone.log
      echo "Copying ${site_to_clone} user-configs.php to ${destination}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      cp /var/www/${site_to_clone}/user-configs.php /var/www/${destination}/user-configs.php

      gpclone::match::state ${site_to_clone} ${destination}
    else
      notification_body="Process Update<br>Origin:${site_to_clone}<br>Destination:${destination}<br>Failed to clone site to new URL, database import failed."
      gridpane::notify::app \
        "Cloning Fail" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    fi
  else
    echo "This is a migration from a different server. Pulling down the most recent full B2 hourly backup but not anymore..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi

  gpclone::end::failsync_pause::exit
}

gpclone::match::state() {
  local site_to_clone="$1"
  local destination="$2"

  notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Source ${site_to_clone} WordPress core/db cloned to ${destination}, proceeding to match state..."
  gridpane::notify::app \
    "Cloning Notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${site_to_clone}"
  echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  echo "Site ${site_to_clone} has been cloned to the ${destination} directory..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  if [[ -f /var/www/${destination}/htdocs/grid.user ]]; then
    matchedSiteUser=$(awk '{print $1; exit}' /var/www/${destination}/htdocs/grid.user)
    currentSiteUser=$(gridpane::get::set::site::user ${destination})

    userID=$(id -u ${matchedSiteUser} 2>&1)
    if [[ $? -eq 0 ]] && [[ ${matchedSiteUser} != "${currentSiteUser}" ]]; then
      echo "Changing ${destination} site ownership from ${currentSiteUser} to ${matchedSiteUser}..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      /usr/local/bin/gpswitch ${destination} ${matchedSiteUser} | tee -a /opt/gridpane/migrate-clone.log
    fi

    rm /var/www/${destination}/htdocs/grid.user | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi

  source_cache_type=$(/usr/local/bin/gp conf -ngx -cache-check ${site_to_clone} -q)
  destination_cache_type=$(/usr/local/bin/gp conf -ngx -cache-check ${destination} -q)

  if [[ ${destination_cache_type} != "${source_cache_type}" ]]; then
    if [[ ${source_cache_type} == "php" ]]; then
      cache_command="-cache-off"
      source_cache_type="none"
    elif [[ ${source_cache_type} == "fastcgi" ]]; then
      cache_command="-fastcgi-cache"
    elif [[ ${source_cache_type} == "redis" ]]; then
      cache_command="-redis-cache"
    fi

    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Adjusting ${destination} caching to ${source_cache_type}"
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Adjusting caching for ${destination} to ${source_cache_type}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} ${cache_command} | tee -a /opt/gridpane/migrate-clone.log
  fi

  source_conf_type=$(/usr/local/bin/gp conf -nginx -type-check ${site_to_clone} -q)
  destination_conf_type=$(/usr/local/bin/gp conf -nginx -type-check ${destination} -q)

  if [[ ${source_conf_type} == *"https"* ]] && [[ ${destination_conf_type} != *"https"* ]]; then
    echo "Grabbing SSL for ${destination}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -primary-ssl-on | tee -a /opt/gridpane/migrate-clone.log
  fi

  source_php_ver=$(gridpane::get::site::php ${site_to_clone})
  destination_php_ver=$(gridpane::get::site::php ${destination})

  if [[ ${source_php_ver} != "${destination_php_ver}" ]]; then
    echo "Adjusting PHP for ${destination} to ${source_php_ver}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -switch-php ${source_php_ver} | tee -a /opt/gridpane/migrate-clone.log
  fi

  if [[ ${source_conf_type} == *"www"* ]] && [[ ${destination_conf_type} != *"www"* ]]; then
    route="www"

    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Adjusting ${destination} routing to ${route} needs to be done manually!"
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  elif [[ ${source_conf_type} == *"root"* ]] && [[ ${destination_conf_type} != *"root"* ]]; then
    route="root"

    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Adjusting ${destination} routing to ${route} needs to be done manually!"
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  fi

  source_waf_type=$(gridpane::conf_read waf -site.env "${site_to_clone}")
  destination_waf_type=$(gridpane::conf_read waf -site.env "${destination}")

  if [[ ${source_waf_type} == *"modsec"* ]] && [[ ${destination_waf_type} != *"modsec"* ]]; then
    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Enabling ModSec WAF for ${destination}"
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Enabling Modsec for ${destination}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -modsec-on | tee -a /opt/gridpane/migrate-clone.log

    cp /var/www/${site_to_clone}/modsec/${site_to_clone}-crs-anomaly-blocking-threshold.conf.source \
      /var/www/${destination}/modsec/${destination}-crs-anomaly-blocking-threshold.conf
    cp /var/www/${site_to_clone}/modsec/${site_to_clone}-crs-paranoia-level.conf.source \
      /var/www/${destination}/modsec/${destination}-crs-paranoia-level.conf
    cp /var/www/${site_to_clone}/modsec/${site_to_clone}-runtime-exceptions.conf.source \
      /var/www/${destination}/modsec/${destination}-runtime-exceptions.conf
    cp /var/www/${site_to_clone}/modsec/${site_to_clone}-runtime-whitelist.conf.source \
      /var/www/${destination}/modsec/${destination}-runtime-whitelist.conf

  elif [[ ${source_waf_type} == "6G" ]] && [[ ${destination_waf_type} != "6g" ]]; then
    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Enabling 6G WAF for ${destination}"
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Enabling 6G for ${destination}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -6g-on | tee -a /opt/gridpane/migrate-clone.log
  fi

  smtp=$(gridpane::conf_read smtp -site.env "${site_to_clone}")
  if [[ ${smtp} == "true" ]] || [[ ${smtp} == "on" ]]; then
    if [[ -f /var/www/${site_to_clone}/sendgrid-wp-configs.php ]]; then
      notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Enabling SendGrid SMTP for ${destination}"
      gridpane::notify::app \
        "Cloning Notice" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-clone" \
        "long" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      echo "Enabling SMTP for ${destination}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      api_key=$(grep "SMTP_PASS" /var/www/${site_to_clone}/sendgrid-wp-configs.php)
      api_key=${api_key//\"/}
      api_key=${api_key// /}
      api_key=${api_key#define(SMTP_PASS,}
      api_key=${api_key%');'}
      /usr/local/bin/gp site ${destination} -mailer enable "SendGrid" "${api_key}" "simple" | tee -a /opt/gridpane/migrate-clone.log
    fi
  fi

  http_auth=$(gridpane::conf_read http-auth -site.env "${site_to_clone}")
  if [[ ${http_auth} == "true" ]] || [[ ${http_auth} == "on" ]]; then
    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Enabling HTTP Auth for ${destination}"
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Enabling HTTP Auth for ${destination}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -http-auth | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_allow_url_include=$(gridpane::conf_read allow_url_include -site.env ${site_to_clone})
  if [[ ! -z ${site_allow_url_include} ]]; then
    php_migrations="allow_url_include, "
    echo "Matching ${destination} php allow_url_include" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write allow_url_include ${site_allow_url_include} -site.env ${destination}
  fi

  site_allow_url_fopen=$(gridpane::conf_read allow_url_fopen -site.env ${site_to_clone})
  if [[ ! -z ${site_allow_url_fopen} ]]; then
    php_migrations="${php_migrations}allow_url_fopen, "
    echo "Matching ${destination} php allow_url_fopen" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write allow_url_fopen ${site_allow_url_fopen} -site.env ${destination}
  fi

  site_date_timezone=$(gridpane::conf_read date.timezone -site.env ${site_to_clone})
  if [[ ! -z ${site_date_timezone} ]]; then
    accepted_timezones=$(cat /opt/gridpane/php-timezones/php-timezones.env)
    if [[ ${accepted_timezones} != *"${site_date_timezone}"* ]]; then
      echo "date.timezone requires a supported timezone to be passed, stored value ${site_date_timezone} is not supported." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      echo "Please see https://www.php.net/manual/en/timezones.php for a full list of supported timezones." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    else
      php_migrations="${php_migrations}date.timezone, "
      echo "Matching ${destination} php date.timezone" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      gridpane::conf_write date.timezone ${site_date_timezone} -site.env ${destination}
    fi
  fi

  site_default_socket_timeout=$(gridpane::conf_read default_socket_timeout -site.env ${site_to_clone})
  if [[ ! -z ${site_default_socket_timeout} ]]; then
    php_migrations="${php_migrations}default_socket_timeout, "
    echo "Matching ${destination} php default_socket_timeout" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write default_socket_timeout ${site_default_socket_timeout} -site.env ${destination}
  fi

  site_disable_functions=$(gridpane::conf_read disable_functions -site.env ${site_to_clone})
  if [[ ! -z ${site_disable_functions} ]]; then
    php_migrations="${php_migrations}disable_functions, "
    echo "Matching ${destination} php disable_functions" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write disable_functions ${site_disable_functions} -site.env ${destination}
  fi

  site_expose_php=$(gridpane::conf_read expose_php -site.env ${site_to_clone})
  if [[ ! -z ${site_expose_php} ]]; then
    php_migrations="${php_migrations}expose_php, "
    echo "Matching ${destination} php expose_php" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write expose_php ${site_expose_php} -site.env ${destination}
  fi

  site_max_execution_time=$(gridpane::conf_read max_execution_time -site.env ${site_to_clone})
  if [[ ! -z ${site_max_execution_time} ]]; then
    php_migrations="${php_migrations}max_execution_time, "
    echo "Matching ${destination} php max_execution_time" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write max_execution_time ${site_max_execution_time} -site.env ${destination}
  fi

  site_max_file_uploads=$(gridpane::conf_read max_file_uploads -site.env ${site_to_clone})
  if [[ ! -z ${site_max_file_uploads} ]]; then
    php_migrations="${php_migrations}max_file_uploads, "
    echo "Matching ${destination} php max_file_uploads" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write max_file_uploads ${site_max_file_uploads} -site.env ${destination}
  fi

  site_max_input_time=$(gridpane::conf_read max_input_time -site.env ${site_to_clone})
  if [[ ! -z ${site_max_input_time} ]]; then
    php_migrations="${php_migrations}max_input_time, "
    echo "Matching ${destination} php max_input_time" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write max_input_time ${site_max_input_time} -site.env ${destination}
  fi

  site_max_input_vars=$(gridpane::conf_read max_input_vars -site.env ${site_to_clone})
  if [[ ! -z ${site_max_input_vars} ]]; then
    php_migrations="${php_migrations}max_input_vars, "
    echo "Matching ${destination} php max_input_vars" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write max_input_vars ${site_max_input_vars} -site.env ${destination}
  fi
  site_memory_limit=$(gridpane::conf_read memory_limit -site.env ${site_to_clone})
  if [[ ! -z ${site_memory_limit} ]]; then
    php_migrations="${php_migrations}memory_limit, "
    echo "Matching ${destination} php memory_limit" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write memory_limit ${site_memory_limit} -site.env ${destination}
  fi

  site_post_max_size=$(gridpane::conf_read post_max_size -site.env ${site_to_clone})
  if [[ ! -z ${site_post_max_size} ]]; then
    php_migrations="${php_migrations}post_max_size, "
    echo "Matching ${destination} php post_max_size" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write post_max_size ${site_post_max_size} -site.env ${destination}
  fi

  site_session_cookie_lifetime=$(gridpane::conf_read session.cookie_lifetime -site.env ${site_to_clone})
  if [[ ! -z ${site_session_cookie_lifetime} ]]; then
    php_migrations="${php_migrations}session.cookie_lifetime, "
    echo "Matching ${destination} php session.cookie_lifetime" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write session.cookie_lifetime ${site_session_cookie_lifetime} -site.env ${destination}
  fi

  site_session_gc_maxlifetime=$(gridpane::conf_read session.gc_maxlifetime -site.env ${site_to_clone})
  if [[ ! -z ${site_session_gc_maxlifetime} ]]; then
    php_migrations="${php_migrations}session.gc_maxlifetime, "
    echo "Matching ${destination} php session.gc_maxlifetime" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write session.gc_maxlifetime ${site_session_gc_maxlifetime} -site.env ${destination}
  fi

  site_short_open_tag=$(gridpane::conf_read short_open_tag -site.env ${site_to_clone})
  if [[ ! -z ${site_short_open_tag} ]]; then
    php_migrations="${php_migrations}short_open_tag, "
    echo "Matching ${destination} php short_open_tag" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write short_open_tag ${site_short_open_tag} -site.env ${destination}
  fi

  site_upload_max_filesize=$(gridpane::conf_read upload_max_filesize -site.env ${site_to_clone})
  if [[ ! -z ${site_upload_max_filesize} ]]; then
    php_migrations="${php_migrations}upload_max_filesize, "
    echo "Matching ${destination} php upload_max_filesize" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gridpane::conf_write upload_max_filesize ${site_upload_max_filesize} -site.env ${destination}
  fi

  site_pm=$(gridpane::conf_read pm -site.env ${site_to_clone})
  if [[ ! -z ${site_pm} ]]; then
    php_migrations="${php_migrations}pm, "
    echo "Matching ${destination} php pm" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp stack php -site-pm ${site_pm} ${destination} | tee -a /opt/gridpane/migrate-clone.log
  fi
  site_pm_max_children=$(gridpane::conf_read pm.max_children -site.env ${site_to_clone})
  if [[ ! -z ${site_pm_max_children} ]]; then
    php_migrations="${php_migrations}pm.max_children, "
    echo "Matching ${destination} php pm.max_children" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp stack php -site-pm-max-children ${site_pm_max_children} ${destination} | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_pm_max_requests=$(gridpane::conf_read pm.max_requests -site.env ${site_to_clone})
  if [[ ! -z ${site_pm_max_requests} ]]; then
    php_migrations="${php_migrations}pm.max_requests, "
    echo "Matching ${destination} php pm.max_requests" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp stack php -site-pm-max-requests ${site_pm_max_requests} ${destination} | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_pm_start_servers=$(gridpane::conf_read pm.start_servers -site.env ${site_to_clone})
  if [[ ! -z ${site_pm_start_servers} ]]; then
    php_migrations="${php_migrations}pm.start_servers, "
    echo "Matching ${destination} php pm.start_servers" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp stack php -site-pm-start-servers ${site_pm_start_servers} ${destination} | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_pm_min_spare_servers=$(gridpane::conf_read pm.min_spare_servers -site.env ${site_to_clone})
  if [[ ! -z ${site_pm_min_spare_servers} ]]; then
    php_migrations="${php_migrations}pm.min_spare_servers, "
    echo "Matching ${destination} php pm.min_spare_servers" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp stack php -site-pm-min-spare-servers ${site_pm_min_spare_servers} ${destination} | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_pm_max_spare_servers=$(gridpane::conf_read pm.max_spare_servers -site.env ${site_to_clone})
  if [[ ! -z ${site_pm_max_spare_servers} ]]; then
    php_migrations="${php_migrations}pm.max_spare_servers, "
    echo "Matching ${destination} php pm.max_spare_servers" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp stack php -site-pm-max-spare-servers ${site_pm_max_spare_servers} ${destination} | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_pm_process_idle_timeout=$(gridpane::conf_read pm.process_idle_timeout -site.env ${site_to_clone})
  if [[ ! -z ${site_pm_process_idle_timeout} ]]; then
    php_migrations="${php_migrations}pm.process_idle_timeout, "
    echo "Matching ${destination} php pm.process_idle_timeout" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp stack php -site-pm-process-idle-timeout ${site_pm_process_idle_timeout} ${destination} | tee -a /opt/gridpane/migrate-clone.log
  fi

  echo "${php_migrations}" | tee -a /opt/gridpane/migrate-clone.log
  if [[ ! -z ${php_migrations} ]]; then
    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Matched PHP INI and Pool.D Configurations for ${destination}"
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi

  destination_conf_type_recheck=$(/usr/local/bin/gp conf -nginx -type-check ${destination} -q)
  if [[ ${destination_conf_type_recheck} != *"proxy"* ]]; then
    site_nginx_fcgi_cache_valid=$(gridpane::conf_read nginx-fcgi-cache-valid -site.env ${site_to_clone})
    if [[ ! -z ${site_nginx_fcgi_cache_valid} ]] && [[ ${site_nginx_fcgi_cache_valid} != "1" ]]; then
      nginx_migrations="fcgi-cache-valid, "
      echo "Matching ${destination} nginx fastcgi -cache-valid" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      /usr/local/bin/gp site ${destination} nginx fastcgi -cache-valid ${site_nginx_fcgi_cache_valid} | tee -a /opt/gridpane/migrate-clone.log
    fi
  else
    site_nginx_proxy_cache_valid=$(gridpane::conf_read nginx-proxy-cache-valid -site.env ${site_to_clone})
    if [[ ! -z ${site_nginx_proxy_cache_valid} ]] && [[ ${site_nginx_proxy_cache_valid} != "1" ]]; then
      nginx_migrations="proxy-cache-valid, "
      echo "Matching ${destination} nginx proxy -cache-valid" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      /usr/local/bin/gp site ${destination} nginx proxy -cache-valid ${site_nginx_proxy_cache_valid} | tee -a /opt/gridpane/migrate-clone.log
    fi
  fi

  site_nginx_redis_cache_valid=$(gridpane::conf_read nginx-redis-cache-valid -site.env ${site_to_clone})
  if [[ ! -z ${site_nginx_redis_cache_valid} ]] && [[ ${site_nginx_redis_cache_valid} != "2592000" ]]; then
    nginx_migrations="redis-cache-valid, "
    echo "Matching ${destination} nginx redis -cache-valid" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} nginx redis -cache-valid ${site_nginx_redis_cache_valid} | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_zone_one_burst=$(gridpane::conf_read zone-one-burst -site.env ${site_to_clone})
  if [[ ! -z ${site_zone_one_burst} ]] && [[ ${site_zone_one_burst} != "1" ]]; then
    nginx_migrations="${nginx_migrations}zone-one-burst, "
    echo "Matching ${destination} nginx zone-one-burst" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -nginx-zone-burst one ${site_zone_one_burst} | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_zone_wp_burst=$(gridpane::conf_read zone-wp-burst -site.env ${site_to_clone})
  if [[ ! -z ${site_zone_wp_burst} ]] && [[ ${site_zone_wp_burst} != "6" ]]; then
    nginx_migrations="${nginx_migrations}zone-wp-burst, "
    echo "Matching ${destination} nginx zone-wp-burst" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -nginx-zone-burst wp ${site_zone_wp_burst} | tee -a /opt/gridpane/migrate-clone.log
  fi

  if [[ ! -z ${nginx_migrations} ]]; then
    notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Matched Nginx Configurations for ${destination}"
    gridpane::notify::app \
      "Cloning Notice" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi

  if [[ ${destination_conf_type_recheck} == *"https"* ]]; then
    site_brotli=$(gridpane::conf_read brotli -site.env ${site_to_clone})
    if [[ ${site_brotli} == *"rue"* ]] || [[ ${site_brotli} == *"on"* ]]; then
      other_optims="brotli enabled, "
      echo "Enabling ${destination} brotli compression" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      /usr/local/bin/gp site ${destination} -brotli-on | tee -a /opt/gridpane/migrate-clone.log
    fi
    site_http2_push=$(gridpane::conf_read http2-push -site.env ${site_to_clone})
    if [[ ${site_http2_push} == *"rue"* ]] || [[ ${site_http2_push} == *"on"* ]]; then
      other_optims="${other_optims}http2 push preload enabled, "
      echo "Enabling ${destination} http2 push preload" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      /usr/local/bin/gp site ${destination} -http2-push-preload-on | tee -a /opt/gridpane/migrate-clone.log
    fi
  fi

  site_csfp_headers=$(gridpane::conf_read csfp-headers -site.env ${site_to_clone})
  if [[ ${site_csfp_headers} == *"rue"* ]] || [[ ${site_csfp_headers} == *"on"* ]]; then
    other_optims="${other_optims}csp headers enabled, "
    echo "Enabling ${destination} CSP and FP Headers" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -csp-header-on | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_disable_concatenation=$(gridpane::conf_read disable-concat -site.env ${site_to_clone})
  if [[ ${site_disable_concatenation} == *"rue"* ]] || [[ ${site_disable_concatenation} == *"on"* ]]; then
    other_optims="${other_optims}load scripts concatenation disabled, "
    echo "Disabling ${destination} load scripts concatenation" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -disable-concatenate-load-scripts | tee -a /opt/gridpane/migrate-clone.log
  fi

  check_xlmrpc_mistake=$(gridpane::conf_read xlmrpc-disabled -site.env ${site_to_clone})
  if [[ ! -z ${check_xlmrpc_mistake} ]]; then
    gridpane::conf_write xmlrpc-disabled ${check_xlmrpc_mistake} -site.env ${site_to_clone}
    gridpane::conf_delete xlmrpc-disabled -site.env ${site_to_clone}
  fi

  site_disable_xmlrpc=$(gridpane::conf_read xmlrpc-disabled -site.env ${site_to_clone})
  if [[ ${site_disable_xmlrpc} == *"rue"* ]] || [[ ${site_disable_xmlrpc} == *"on"* ]]; then
    other_optims="${other_optims}xmlrpc disabled, "
    echo "Disabling ${destination} xmlrpc" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -disable-xmlrpc | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_cron=$(gridpane::conf_read cron -site.env ${site_to_clone})
  if [[ ${site_cron} == *"rue"* ]] || [[ ${site_cron} == *"on"* ]]; then
    other_optims="${other_optims}gpcron enabled, "
    echo "Enabling ${destination} gpcron" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -gpcron-on | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_wpfail2ban=$(gridpane::conf_read wp-fail2ban -site.env ${site_to_clone})
  if [[ ${site_wpfail2ban} == *"rue"* ]] || [[ ${site_wpfail2ban} == *"on"* ]]; then
    other_optims="${other_optims}wpfail2ban enabled, "

    echo "Enabling ${destination} wpfail2ban" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -enable-wp-fail2ban | tee -a /opt/gridpane/migrate-clone.log

    site_wpfail2ban_enumeration=$(gridpane::conf_read wp-fail2ban-enumeration -site.env ${site_to_clone})
    if [[ ${site_wpfail2ban_enumeration} != *"rue"* ]] || [[ ${site_wpfail2ban_enumeration} != *"on"* ]]; then
      echo "Disabling ${destination} wpfail2ban user enumeration protection" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      /usr/local/bin/gp site ${destination} -configure-wp-fail2ban -unblock-user-enumeration | tee -a /opt/gridpane/migrate-clone.log
    fi

    site_wpfail2ban_usernames=$(gridpane::conf_read wp-fail2ban-usernames -site.env ${site_to_clone})
    if [[ ${site_wpfail2ban_usernames} != *"rue"* ]] || [[ ${site_wpfail2ban_usernames} != *"on"* ]]; then
      echo "Disabling ${destination} wpfail2ban stupid username protection" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      /usr/local/bin/gp site ${destination} -configure-wp-fail2ban -unblock-stupid-usernames | tee -a /opt/gridpane/migrate-clone.log
    fi

    site_wpfail2ban_comments=$(gridpane::conf_read wp-fail2ban-comments -site.env ${site_to_clone})
    if [[ ${site_wpfail2ban_comments} != *"rue"* ]] || [[ ${site_wpfail2ban_comments} != *"on"* ]]; then
      echo "Disabling ${destination} wpfail2ban comments protection" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      /usr/local/bin/gp site ${destination} -configure-wp-fail2ban -unguard-comments | tee -a /opt/gridpane/migrate-clone.log
    fi

    site_wpfail2ban_passwords=$(gridpane::conf_read wp-fail2ban-passwords -site.env ${site_to_clone})
    if [[ ${site_wpfail2ban_comments} != *"rue"* ]] || [[ ${site_wpfail2ban_comments} != *"on"* ]]; then
      echo "Disabling ${destination} wpfail2ban password reset protection" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      /usr/local/bin/gp site ${destination} -configure-wp-fail2ban -unguard-password-resets | tee -a /opt/gridpane/migrate-clone.log
    fi

    site_wpfail2ban_spam=$(gridpane::conf_read wp-fail2ban-spam -site.env ${site_to_clone})
    if [[ ${site_wpfail2ban_spam} != *"rue"* ]] || [[ ${site_wpfail2ban_spam} != *"on"* ]]; then
      echo "Disabling ${destination} wpfail2ban spam protection" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      /usr/local/bin/gp site ${destination} -configure-wp-fail2ban -unguard-spam | tee -a /opt/gridpane/migrate-clone.log
    fi

    site_wpfail2ban_pingbacks=$(gridpane::conf_read wp-fail2ban-pingbacks -site.env ${site_to_clone})
    if [[ ${site_wpfail2ban_pingbacks} != *"rue"* ]] || [[ ${site_wpfail2ban_pingbacks} != *"on"* ]]; then
      echo "Disabling ${destination} wpfail2ban pingbacks protection" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      /usr/local/bin/gp site ${destination} -configure-wp-fail2ban -unguard-pingbacks | tee -a /opt/gridpane/migrate-clone.log
    fi
  fi

  site_clickjacking_protection_off=$(gridpane::conf_read clickjacking-protection -site.env ${site_to_clone})
  if [[ ${site_wpfail2ban_pingbacks} == *"alse"* ]] || [[ ${site_wpfail2ban_pingbacks} == *"off"* ]]; then
    other_optims="${other_optims}x-frame-options header disabled, "
    echo "Disabling ${destination} x-frame-options clickjacking protection" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -clickjacking-protection-off | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_block_install_php=$(gridpane::conf_read block-install.php -site.env ${site_to_clone})
  if [[ ${site_block_install_php} == *"true"* ]] || [[ ${site_block_install_php} == *"on"* ]]; then
    other_optims="${other_optims}block install.php enabled, "
    echo "Enabling ${destination} install.php nginx protection" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -block-install.php | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_block_upgrade_php=$(gridpane::conf_read block-upgrade.php -site.env ${site_to_clone})
  if [[ ${site_block_upgrade_php} == *"rue"* ]] || [[ ${site_block_upgrade_php} == *"on"* ]]; then
    other_optims="${other_optims}block upgrade.php enabled, "
    echo "Enabling ${destination} upgrade.php nginx protection" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -block-upgrade.php | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_disable_emoji=$(gridpane::conf_read emoji-disabled -site.env ${site_to_clone})
  if [[ ${site_disable_emoji} == *"rue"* ]] || [[ ${site_disable_emoji} == *"on"* ]]; then
    other_optims="${other_optims}wp emoji disabled, "
    echo "Disabling ${destination} WordPress Emoji" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -disable-emoji | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_block_wp_scan_agent=$(gridpane::conf_read wpscan-agent-blocked -site.env ${site_to_clone})
  if [[ ${site_block_wp_scan_agent} == *"rue"* ]] || [[ ${site_block_wp_scan_agent} == *"on"* ]]; then
    other_optims="${other_optims}wp scan protect enabled, "
    echo "Enabling ${destination} WordPress Scan Agent protection" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -block-wpscan-agent | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_disable_rss=$(gridpane::conf_read rss-disabled -site.env ${site_to_clone})
  if [[ ${site_disable_rss} == *"rue"* ]] || [[ ${site_disable_rss} == *"on"* ]]; then
    other_optims="${other_optims}rss feeds disabled, "
    echo "Disabling ${destination} RSS feeds" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -disable-rss | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_disable_username_enum=$(gridpane::conf_read username-enum-disabled -site.env ${site_to_clone})
  if [[ ${site_disable_username_enum} == *"rue"* ]] || [[ ${site_disable_username_enum} == *"on"* ]]; then
    other_optims="${other_optims}username enumeration disabled, "
    echo "Disabling ${destination} username enumeration" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -disable-username-enum | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_wp_version_disabled=$(gridpane::conf_read wp-version-disabled -site.env ${site_to_clone})
  if [[ ${site_wp_version_disabled} == *"rue"* ]] || [[ ${site_wp_version_disabled} == *"on"* ]]; then
    other_optims="${other_optims}wp version disabled, "
    echo "Disabling ${destination} WordPress version visibility" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -disable-wp-version | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_block_wp_content_php=$(gridpane::conf_read block-wp-content-php -site.env ${site_to_clone})
  if [[ ${site_block_wp_content_php} == "true" ]]; then
    other_optims="${other_optims}Block WP Content PHP enabled, "
    echo "Disabling ${destination} WP Content dir PHP execution... " | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -block-wp-content.php | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_block_wp_comments_post=$(gridpane::conf_read block-wp-comments-post -site.env ${site_to_clone})
  if [[ ${site_block_wp_comments_post} == "true" ]]; then
    other_optims="${other_optims}Block WP Comments posting enabled, "
    echo "Disabling ${destination} WP Comments Post execution... " | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -block-wp-comments-post.php | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_block_wp_links_opml=$(gridpane::conf_read block-wp-links-opml -site.env ${site_to_clone})
  if [[ ${site_block_wp_links_opml} == "true" ]]; then
    other_optims="${other_optims}Block WP Links OPML enabled, "
    echo "Disabling ${destination} WP Links Opml execution " | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -block-wp-links-opml.php | tee -a /opt/gridpane/migrate-clone.log
  fi

  site_block_wp_links_trackbacks=$(gridpane::conf_read block-wp-trackbacks -site.env ${site_to_clone})
  if [[ ${site_block_wp_links_trackbacks} == "true" ]]; then
    other_optims="${other_optims}Block WP Trackbacks enabled, "
    echo "Disabling ${destination} WP Trackbacks execution " | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    /usr/local/bin/gp site ${destination} -block-wp-trackbacks.php | tee -a /opt/gridpane/migrate-clone.log
  fi

  if [[ ! -z ${other_optims} ]]; then
    notification_body="Process Complete<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Matched GridPane optimisations and settings for ${destination}"
    gridpane::notify::app \
      "Cloning Complete" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  else
    notification_body="Process Complete<br>Source:${site_to_clone}<br>Destination:${destination}<br>Server:${serverIP}<br>Cloning complete, no optimisations to match."
    gridpane::notify::app \
      "Cloning Complete" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-clone" \
      "long" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi

  rm /var/www/${destination}/logs/${site_to_clone}.env.clone*
}

gpclone::set::remote_ip() {
  check_ip=$(gridpane::ip_check $1 "clone")
  echo "${check_ip}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  readonly remote_IP="$1"
}

gpclone::set::site_to_clone() {
  if [[ -z $1 ]]; then
    echo "No site detail passed, exiting!" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gpclone::end::failsync_pause::exit
  else
    if [[ "$(cat /root/gridenv/active-sites.env)" != *"$1"* ]]; then
      gridpane::site_loop::directories

      if [[ ${sitesDirectoryArray} == *"$1"* ]] && [[ ${sitesConfigsArray} == *"$1"* ]]; then
        echo "$1 missing from active sites env, healing..." | tee -a /var/log/gridpane.log /opt/gridpane/failsync.log
        gridpane::conf_write ":$1:" active -active-sites.env | tee -a /var/log/gridpane.log /opt/gridpane/failsync.log
      else
        notification_body="Process Failed<br>Source Server:${serverIP}<br>$1 is not an active site on this server... exiting..."
        gridpane::notify::app \
          "Clone/Migrate Fail" \
          "${notification_body}" \
          "popup_and_center" \
          "fa-clone" \
          "long" \
          "${site_to_clone}"
        echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

        gpclone::end::failsync_pause::exit
      fi
    fi
    site_to_clone=$1
  fi
}

gpclone::clone::single_site::same_url() {
  check_for_multisite=$(gridpane::check::multisite ${site_to_clone})
  if [[ ${check_for_multisite} == "true"* ]]; then
    notification_body="Process Failed<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>${site_to_clone} is a multisite installation, GridPane cloning does not currently support this, we recommend AIO Migration Multisite Extension currently"
    gridpane::notify::app \
      "Clone/Migrate Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    gpclone::end::failsync_pause::exit
  fi

  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "Doing single site clone of ${site_to_clone} to ${remote_IP}..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  notification_body="Process Update<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>Building new ${site_to_clone} site directory and app entry."
  gridpane::notify::app \
    "Cloning notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${site_to_clone}"
  echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  echo "Making ${site_to_clone} on remote..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  gpclone::add::remote_site ${site_to_clone} ${remote_IP}

  if [[ $? -ne 0 ]]; then
    notification_body="Process Failed<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>Building new ${site_to_clone} site on ${remote_IP} failed..."
    gridpane::notify::app \
      "Clone/Migrate Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Error during previous phase, skipping!" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  else
    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "Doing export of ${site_to_clone}..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    gpclone::export::site ${site_to_clone}

    if [[ -f /tmp/${nonce}.failed.db.export ]]; then
      notification_body="Process Failed<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>Failed to export '${site_to_clone}' database..."
      gridpane::notify::app \
        "Clone/Migrate Fail" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      rm /tmp/${nonce}.failed.db.export
    else
      notification_body="Process Update<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>${site_to_clone} Clone Package created for export..."
      gridpane::notify::app \
        "Cloning notice" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-clone" \
        "long" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    fi

    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "Cloning ${site_to_clone} to remote..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    gpclone::clone_to_remote ${site_to_clone} site-clone ${check_for_multisite}

    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi
}

gpclone::clone::single_site::new_url() {
  check_for_multisite=$(gridpane::check::multisite ${site_to_clone})
  if [[ ${check_for_multisite} == "true"* ]]; then
    notification_body="Process Failed<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>${site_to_clone} is a multisite installation, GridPane cloning does not currently support this, we recommend AIO Migration Multisite Extension currently"
    gridpane::notify::app \
      "Clone/Migrate Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    gpclone::end::failsync_pause::exit
  fi

  if [[ -z $1 ]]; then
    echo "Missing the new site URL, exiting!" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    gpclone::end::failsync_pause::exit
  elif [[ $1 != *"."* ]]; then
    notification_body="$1 is not a url, Cloning can not proceed..."
    gridpane::notify::app \
      "Clone/Migrate Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    gpclone::end::failsync_pause::exit
  elif [[ $1 == "www."* ]]; then
    notification_body="URL Passed - $1 - contains the www host, Cloning can not proceed..."
    gridpane::notify::app \
      "Clone/Migrate Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    gpclone::end::failsync_pause::exit
  fi

  readonly new_url=$1

  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "Doing single site clone of ${site_to_clone} to ${remote_IP} with new url ${new_url}..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "Now checking/cloning site: ${site_to_clone}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${new_url}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>Building new ${new_url} site directory and app entry."
  gridpane::notify::app \
    "Cloning notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-clone" \
    "long" \
    "${site_to_clone}"
  echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  echo "Making ${new_url} on remote..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

  gpclone::add::new_site -different-server ${new_url} ${remote_IP}

  if [[ $? -ne 0 ]]; then
    notification_body="Process Failed<br>Source:${site_to_clone}<br>Destination:${new_url}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>Building new ${destination} site on ${remote_IP} failed..."
    gridpane::notify::app \
      "Clone/Migrate Fail" \
      "${notification_body}" \
      "popup_and_center" \
      "fa-exclamation" \
      "infinity" \
      "${site_to_clone}"
    echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Error during previous phase - making new site..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  else
    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "Doing export of ${site_to_clone}..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    gpclone::export::site ${site_to_clone} alternate

    if [[ -f /tmp/${nonce}.failed.db.export ]]; then
      notification_body="Process Failed<br>Source:${site_to_clone}<br>Destination:${new_url}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>Source ${site_to_clone} DB Failed to export..."
      gridpane::notify::app \
        "Clone/Migrate Fail" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-exclamation" \
        "infinity" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      rm /tmp/${nonce}.failed.db.export
    else
      notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${new_url}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>${site_to_clone} Clone Package created for export..."
      gridpane::notify::app \
        "Cloning notice" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-clone" \
        "long" \
        "${site_to_clone}"
      echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    fi

    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "Cloning ${site_to_clone} to remote..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    gpclone::migrate_to_remote ${site_to_clone} ${new_url} ${check_for_multisite}

    echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi
  echo "Site was not in sync list, added to remote server, exported and updated to remote with new url." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
}

gpclone::clone::all_sites::same_url() {
  if [[ $1 == "-failover-stop" ]] || [[ $1 == "failover-stop" ]]; then
    echo "Stopping failover, deleting synced-to/from logs..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    for currfolder in $(find /var/www -maxdepth 1 -mindepth 1 -type d); do
      entry=$(basename ${currfolder})1
      case $entry in
      22222)
        echo "Skipping 22222" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        continue
        ;;
      default)
        echo "Skipping default" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        continue
        ;;
      gridpanevps)
        echo "Skipping Administrative $entry (Stats/PHPMA) GridPane Site..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        continue
        ;;
      html)
        echo "Skipping html..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        continue
        ;;
      *)
        site_to_clone="$entry"
        echo "remove /var/www/${site_to_clone}/logs/synced-to.log locally" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        rm /var/www/${site_to_clone}/logs/synced-to.log
        echo "remove /var/www/${site_to_clone}/logs/synced-from.log on ${remote_IP}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        ssh root@${remote_IP} "rm /var/www/${site_to_clone}/logs/synced-from.log"
        ;;
      esac
    done

    echo "remove gpclone cronjob" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    crontab -l | grep -v "gpclone" | crontab - | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "Updating server conf and resetting gpfailsync..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    gridpane::conf_write gpfailsync-running false
    gridpane::conf_delete gpfailsync-role
    gridpane::conf_delete gpfailsync-server-worker-pause

    ssh root@${remote_IP} "sudo /usr/local/bin/gp conf write gpfailsync-running false" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    ssh root@${remote_IP} "sudo /usr/local/bin/gp conf delete gpfailsync-role" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    ssh root@${remote_IP} "sudo /usr/local/bin/gp conf delete gpfailsync-server-worker-pause" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  else

    gpclone::failsync_pause
    ssh root@${remote_IP} "sudo /usr/local/bin/gp conf write gpfailsync-server-worker-pause true" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    is_failover_sync=$(gridpane::conf_read gpfailsync-role)
    echo "gpfailsync-role: ${is_failover_sync}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    if [[ ${is_failover_sync} == "source" ]]; then

      if [[ -f "/opt/gridpane/failover/syncsites-to-${remote_IP}.log" ]]; then
        echo "Synced sites log for ${remote_IP} ready..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      else
        mkdir -p /opt/gridpane/failover
        touch /opt/gridpane/failover/syncsites-to-${remote_IP}.log
      fi

      failover="true"
      check_destination=$(ssh root@${remote_IP} "sudo /usr/local/bin/gp conf read gpfailsync-running" 2>&1)
      echo "Destination gpfailsync-running: ${check_destination}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      if [[ ${check_destination} != "true" ]]; then
        echo "Enabling Destination gpfailsync-running" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        ssh root@${remote_IP} "sudo /usr/local/bin/gp conf write gpfailsync-running true" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      fi

      check_destination=$(ssh root@${remote_IP} "sudo /usr/local/bin/gp conf read gpfailsync-role" 2>&1)
      echo "Destination gpfailsync-role: ${check_destination}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      if [[ ${check_destination} != "destination" ]]; then
        echo "Ensuing Destination gpfailsync-role" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        ssh root@${remote_IP} "sudo /usr/local/bin/gp conf write gpfailsync-role destination" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
      fi

      echo "Pausing Worker Functions during sync" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

      if [[ ${server_build} == *"prometheus"* ]]; then
        gpclone::sync::ssl ${remote_IP}
      fi
    fi

    active_sites=$(cat /root/gridenv/active-sites.env)

    gridpane::site_loop::directories
    for currfolder in $(find /var/www -maxdepth 1 -mindepth 1 -type d); do
      entry=$(basename ${currfolder})
      check_for_multisite=$(gridpane::check::multisite ${entry})

      if [[ ${check_for_multisite} == "true"* ]] && [[ ! -f /opt/gridpane/jeff.says.swap ]]; then
        echo "${site_to_clone} is a multisite installation, GridPane cloning does not currently support this, we recommend AIO Migration Multisite Extension currently" | tee -a /var/log/gridpane.log /opt/gridpane/failsync.log
        curl -d '{ "title":"Cloning '${site_to_clone}' Fail", "body":"'${site_to_clone}' is a multisite installation, GridPane cloning does not currently support this, we recommend AIO Migration Multisite Extension currently", "channel":"popup_and_center", "icon":"fa-exclamation" }' -H "Content-Type: application/json" -X POST https://${GPURL}/api/notification?api_token=${gridpanetoken}
        continue
      fi

      case $entry in
      2222)
        echo "Skipping 22222" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        continue
        ;;
      default)
        echo "Skipping default" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        continue
        ;;
      *gridpanevps*)
        echo "Skipping Administrative $entry (Stats/PHPMA) GridPane Site..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        continue
        ;;
      html)
        echo "Skipping html..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        continue
        ;;
      *canary*)
        echo "$entry is a canary site... skipping!" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        continue
        ;;
      *staging*)
        echo "$entry is a staging site... skipping!" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        continue
        ;;
      *)
        if [[ "${active_sites}" != *"${entry}"* ]]; then
          if [[ ${sitesDirectoryArray} == *"${entry}"* ]] && [[ ${sitesConfigsArray} == *"${entry}"* ]]; then
            echo "Active site ${entry} missing from active sites env, healing..." | tee -a /var/log/gridpane.log /opt/gridpane/failsync.log
            gridpane::conf_write ":${entry}:" active -active-sites.env | tee -a /var/log/gridpane.log /opt/gridpane/failsync.log
          else
            echo "Skipping ${entry} - not in active sites list and inactive... probably a remnant..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
            continue
          fi
        fi

        site_to_clone="$entry"

        if [[ $1 == "migration" ]]; then
          echo "Ensure site ${site_to_clone} doesn't exist on the destination server already... avoid destructively overwriting anything..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          check_for_extant_site=$(ssh root@${remote_IP} "sudo /usr/local/bin/gp site ${site_to_clone} -pre-clone-check" 2>&1)
          echo "${check_for_extant_site}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          if [[ ${check_for_extant_site} != *"roceed"* ]]; then
            continue
          fi
        fi

        echo "Now checking/cloning site: ${site_to_clone}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

        sync_done="not done"

        if [[ $1 != "migration" ]]; then
          if [[ -f /var/www/${site_to_clone}/logs/swapped-domain.failover ]]; then
            [[ -f /var/www/${site_to_clone}/logs/synced-to.log ]] && rm /var/www/${site_to_clone}/logs/synced-to.log
          fi

          if grep -Fxq "${remote_IP}" /var/www/${site_to_clone}/logs/synced-to.log; then
            echo "Doing export of ${site_to_clone}..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
            gpclone::export::db_only ${site_to_clone}
            echo "Cloning ${site_to_clone} to remote..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
            gpclone::sync::to_remote ${site_to_clone} ${remote_IP}
            echo "Site was already in sync list, exported and updated on remote." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
            sync_done="done"
          fi
        fi

        if [[ ${sync_done} != "done" ]]; then

          if [[ -f /var/www/${site_to_clone}/logs/swapped-domain.failover ]]; then
            echo "-------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
            local failover_site_original_domain=$(cat /var/www/${site_to_clone}/logs/swapped-domain.failover)
            rm /var/www/${site_to_clone}/logs/swapped-domain.failover
            notification_body="Failover Site ${site_to_clone} has had primary domain swapped! Cloning new domain site over. Site with previous domain ${failover_site_original_domain} will need manual deletion from ${remote_IP}."
            gridpane::notify::app \
              "Failover Clone Notification" \
              "${notification_body}" \
              "popup_and_center" \
              "fa-exclamation" \
              "infinity" \
              "${site_to_clone}"
            echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          fi

          echo "-------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

          notification_body="Process Update<br>Source:${site_to_clone}<br>Destination:${new_url}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>Building new ${destination} site directory and app entry."
          gridpane::notify::app \
            "Cloning/Migrate Notice" \
            "${notification_body}" \
            "popup_and_center" \
            "fa-clone" \
            "long" \
            "${site_to_clone}"
          echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

          echo "Making ${site_to_clone} on remote..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

          if [[ $1 != "migration" ]]; then
            gpclone::add::remote_site ${site_to_clone} ${remote_IP} true
          else
            gpclone::add::remote_site ${site_to_clone} ${remote_IP}
          fi

          echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          echo "Doing export of ${site_to_clone}..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

          gpclone::export::site ${site_to_clone}

          echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

          if [[ -f /tmp/${nonce}.failed.db.export ]]; then
            notification_body="Process Failed<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>Source ${site_to_clone} DB Failed to export..."
            notification_icon="fa-exclamation"
            notification_duration="infinity"
            rm /tmp/${nonce}.failed.db.export
          else
            notification_body="Process Update<br>Source:${site_to_clone}<br>Source Server:${serverIP}<br>Destination Server:${remote_IP}<br>Source ${site_to_clone} export package created.."
            notification_icon="fa-clone"
            notification_duration="long"
          fi

          gridpane::notify::app \
            "Cloning Notice" \
            "${notification_body}" \
            "popup_and_center" \
            "${notification_icon}" \
            "${notification_duration}" \
            "${site_to_clone}"
          echo "${notification_body}" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

          if [[ -z ${failover} ]]; then
            echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
            echo "Cloning ${site_to_clone} to remote..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
            echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

            gpclone::clone_to_remote ${site_to_clone} site-clone ${check_for_multisite}
            echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          else
            echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
            echo "Cloning Failover clone ${site_to_clone} to remote..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
            echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

            gpclone::clone_to_remote ${site_to_clone} failover-clone ${check_for_multisite}
            echo "--------------------------------------------------------------------" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
          fi
          echo "Site was not in sync list, added to remote server, exported and updated to remote." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
        fi
        ;;
      esac
    done

    echo "Sync complete, unpausing Worker Functions..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    gridpane::conf_delete gpfailsync-server-worker-pause | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
    ssh root@${remote_IP} "sudo /usr/local/bin/gp conf delete gpfailsync-server-worker-pause" | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log

    echo "gpfailsync-server-worker-pause deleted, server tasks can resume as normal..." | tee -a /var/log/gridpane.log /opt/gridpane/migrate-clone.log
  fi
}
