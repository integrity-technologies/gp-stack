#!/bin/bash

#. gridpane.sh

server::update::timezone() {
  local all_timezones
  local searched_timezones
  local current_timezone

  if [[ $1 == "search" ||
        $1 == "-search" ]]; then

    if [[ -z $2 ]]; then

      all_timezones=$(/usr/bin/timedatectl list-timezones)

      echo "All Available Timezones include:"
      echo "-------------------------------------"
      echo "${all_timezones}"
      echo "-------------------------------------"
      echo "You failed to enter a search argument"
      echo "All available Timezones above"
      echo "-------------------------------------"

    else

      searched_timezones=$(/usr/bin/timedatectl list-timezones | grep -i ${2})

      if [[ ! -z ${searched_timezones} ]]; then

        echo "Search results for ${2}"
        echo "-------------------------------------"
        echo "${searched_timezones}"
        echo "-------------------------------------"
        echo "Search results for ${2}"
        echo "Please select from list above"
        echo "-------------------------------------"

      else

        all_timezones=$(/usr/bin/timedatectl list-timezones)

        echo "All Available Timezones include:"
        echo "-------------------------------------"
        echo "${all_timezones}"
        echo "-------------------------------------"
        echo "Your search returned nothing."
        echo "All available Timezones above"
        echo "-------------------------------------"

      fi

    fi

  elif [[ $1 == "list-all" ]] || [[ $1 == "-list-all" ]]; then

    all_timezones=$(/usr/bin/timedatectl list-timezones)

    echo "All Available Timezones include:"
    echo "-------------------------------------"
    echo "${all_timezones}"
    echo "-------------------------------------"
    echo "All available Timezones above"
    echo "You can search with:"
    echo "gp server update-timezone -search {string}"
    echo "-------------------------------------"

  else

    local current_timezone
    local all_timezones
    all_timezones=$(/usr/bin/timedatectl list-timezones)

    if [[ ${all_timezones} != *"$1"* ]]; then

      echo "All Available Timezones include:"
      echo "-------------------------------------"
      echo "${all_timezones}"
      echo "-------------------------------------"
      echo "$1 is not available as a Timezone"
      echo "Please select one from the list above"
      echo "-------------------------------------"

    else

      /usr/bin/timedatectl set-timezone "$1"

      current_timezone=$(/usr/bin/timedatectl)

      echo "Timezone Updated"
      echo "-------------------------------------"
      echo "$current_timezone"
      echo "-------------------------------------"
    fi

    current_timezone=$(cat /etc/timezone)

    # shellcheck disable=SC2154
    gridpane::callback::app \
      "callback" \
      "/server/server-update" \
      "server_ip=${serverIP}" \
      "server_timezone=${current_timezone}"
  fi

}

server::update::unattended_upgrades() {
  if [ "$#" -lt 2 ]; then
    echo "Unattended upgrades requires at least a setting to update and value... exiting"
    exit 187
  fi

  if [[ ! -f /etc/apt/apt.conf.d/50unattended-upgrades ]]; then
    echo "Unattended upgrades file /etc/apt/apt.conf.d/50unattended-upgrades is missing... exiting..."
    exit 187
  fi
  local reboot
  local reboot_time
  while true; do
    case $1 in
      -reboot|reboot)
        shift
        reboot="$1"
        ;;
      -reboot-time)
        shift
        reboot_time="$1"
        ;;
    esac
    shift 1 || true
    [[ -z "$1" ]] && break
  done

  if [[ -z $reboot ]] ||
    [[ ${reboot} != "true" &&
      ${reboot} != "false" ]]; then
    [[ -n $reboot ]] &&
      echo "Unattended-Upgrade::Automatic-Reboot only accepts true/false - ${reboot} cannot be processed"
    reboot=$(grep "Unattended-Upgrade::Automatic-Reboot " /etc/apt/apt.conf.d/50unattended-upgrades)
    reboot=${reboot#Unattended-Upgrade::Automatic-Reboot \"}
    reboot=${reboot%\";}
  else
    echo "Setting: Unattended-Upgrade::Automatic-Reboot \"${reboot}\""
    sudo sed -i "/Unattended-Upgrade::Automatic-Reboot \"/c \Unattended-Upgrade::Automatic-Reboot \"${reboot}\";" \
      /etc/apt/apt.conf.d/50unattended-upgrades
  fi

  if [[ -z $reboot ]] ||
    [[ ${reboot} != "true" &&
      ${reboot} != "false" ]]; then
    [[ -n $reboot ]] &&
      echo "Unattended-Upgrade::Automatic-Reboot only accepts true/false - ${reboot} cannot be processed"
    reboot=$(grep "Unattended-Upgrade::Automatic-Reboot " /etc/apt/apt.conf.d/50unattended-upgrades)
    reboot=${reboot#Unattended-Upgrade::Automatic-Reboot \"}
    reboot=${reboot%\";}
  else
    echo "Setting: Unattended-Upgrade::Automatic-Reboot \"${reboot}\""
    sudo sed -i "/Unattended-Upgrade::Automatic-Reboot \"/c \Unattended-Upgrade::Automatic-Reboot \"${reboot}\";" \
      /etc/apt/apt.conf.d/50unattended-upgrades
  fi

  if [[ -z $reboot_time ]] ||
    [[ ! ${reboot_time} =~ ^[0-9][0-9]:[0-9][0-9]$ ]]; then
    [[ ! ${reboot_time} =~ ^[0-9][0-9]:[0-9][0-9]$ ]] &&
      echo "Unattended-Upgrade::Automatic-Reboot-Time only accepts 24 hour 00:00 patterns - ${reboot_time} cannot be processed"
    reboot_time=$(grep "Unattended-Upgrade::Automatic-Reboot-Time " /etc/apt/apt.conf.d/50unattended-upgrades)
    reboot_time=${reboot_time#Unattended-Upgrade::Automatic-Reboot-Time \"}
    reboot_time=${reboot_time%\";}
  else
    echo "Setting: Unattended-Upgrade::Automatic-Reboot-Time \"${reboot_time}\""
    sudo sed -i "/Unattended-Upgrade::Automatic-Reboot-Time \"/c \Unattended-Upgrade::Automatic-Reboot-Time \"${reboot_time}\";" \
      /etc/apt/apt.conf.d/50unattended-upgrades
  fi

  gridpane::conf_write Unattended-Upgrade-Automatic-Reboot "${reboot}"
  local formatted_reboot_time
  formatted_reboot_time=${reboot_time/:/-}
  gridpane::conf_write Unattended-Upgrade-Automatic-Reboot-time "${formatted_reboot_time}"

  echo "-------------------------------------------------------"
  echo "Unattended Upgrades Config:"
  echo "/etc/apt/apt.conf.d/50unattended-upgrades"
  echo "-------------------------------------------------------"
  cat /etc/apt/apt.conf.d/50unattended-upgrades
  echo "-------------------------------------------------------"

  local security_updates_reboot_required
  security_updates_reboot_required="false"
  [[ -f /var/run/reboot-required ]] &&
    security_updates_reboot_required="true"

  # Set reboot required to false until we figure it.
  # shellcheck disable=SC2154
  gridpane::callback::app \
    "/server/server-update" \
    "--" \
    "server_ip=${serverIP}" \
    "security_updates_reboot@${reboot}" \
    "-s" "security_updates_reboot_time=${reboot_time}" \
    "security_updates_reboot_required@false"
}

server::sync::system_settings() {
  local was_this_a_reboot="$1"
  local current_timezone
  current_timezone=$(cat /etc/timezone)
  local reboot
  reboot=$(grep "Unattended-Upgrade::Automatic-Reboot " /etc/apt/apt.conf.d/50unattended-upgrades)
  reboot=${reboot#Unattended-Upgrade::Automatic-Reboot \"}
  reboot=${reboot%\";}
  local reboot_time
  reboot_time=$(grep "Unattended-Upgrade::Automatic-Reboot-Time " /etc/apt/apt.conf.d/50unattended-upgrades)
  reboot_time=${reboot_time#Unattended-Upgrade::Automatic-Reboot-Time \"}
  reboot_time=${reboot_time%\";}

  if [[ ! -f /opt/gridpane/initial.random.upgrade.time.set &&
        $reboot_time == "02:30" ]]; then
    # Assume this is the og default GP of 02.30 since new builds randomise between 2am to 4.59am
    # And this is the first time that the sync function has been run since:
    # - Function is not publicly available
    # - All new servers have that token.
    # So lets randomise that shit before we sync back, as if it's a new server.
    # Since the UI has no previous state anyway
    local reboot_hour
    reboot_hour=$((RANDOM % 3 + 2))
    local reboot_min
    reboot_min=$((RANDOM % 58 + 1))
    [[ $reboot_min -lt 10 ]] &&
      reboot_min="0$reboot_min"
    reboot_time="0${reboot_hour}:${reboot_min}"
    /bin/sed -i "/Unattended-Upgrade::Automatic-Reboot-Time \"/c \Unattended-Upgrade::Automatic-Reboot-Time \"${reboot_time}\";" \
      /etc/apt/apt.conf.d/50unattended-upgrades
    # Now lets grep that shit again... its one time only
    reboot_time=$(/bin/grep "Unattended-Upgrade::Automatic-Reboot-Time " /etc/apt/apt.conf.d/50unattended-upgrades)
    reboot_time=${reboot_time#Unattended-Upgrade::Automatic-Reboot-Time \"}
    reboot_time=${reboot_time%\";}
    # And create the token this is a one time only deal...
    touch /opt/gridpane/initial.random.upgrade.time.set
    # Now the initial stqte for the fleet should be that once this feature becomes active
    # the fleet has a randomised starting callback period covering 10K seconds, before users start to adjust.
    # We can add this into build defaults and server build form.
  fi

  local callback
  callback="now"
  if [[ $was_this_a_reboot == "reboot" &&
        $reboot == "true" ]]; then
    local currenttime
    currenttime=$(date +%H:%M)
    local reboot_hour
    reboot_hour=${reboot_time%:*}
    local reboot_min
    reboot_min=${reboot_time#*:}
    local hour_ahead_of_reboot_time
    hour_ahead_of_reboot_time=$((reboot_hour + 1))
    if [[ $hour_ahead_of_reboot_time -le 9 ]];then
      hour_ahead_of_reboot_time="0$hour_ahead_of_reboot_time"
    fi
    hour_ahead_of_reboot_time="$hour_ahead_of_reboot_time:$reboot_min"
    # Check to see if the time now is between the set auto reboot time
    # and an hour ahead. If it is, this is likely an auto reboot
    # randomise the callback time over the next 4 hours
    # since we don't want to DDoS the app from all the security updating servers
    # from users with matching reboot times
    [[ "$currenttime" > "${reboot_time}" &&
      "$currenttime" < "${hour_ahead_of_reboot_time}" ]] ||
    [[ "$currenttime" = "${reboot_time}" ]] &&
      callback="randomised"
  fi

  local security_updates_reboot_required
  security_updates_reboot_required="false"
  [[ -f /var/run/reboot-required ]] &&
    security_updates_reboot_required="true"

  # This will be enabled once the system settings have been in production
  # for a while and people have had a chance to set their servers to their timezones
#  if [[ $callback == "now" ]]; then
#    /bin/date >/run/rebooted-sync
#    echo "reboot system setting sync" >>/run/rebooted-sync
#    gridpane::callback::app \
#      "callback" \
#      "/server/server-update" \
#      "--" \
#      "server_ip=${serverIP}" \
#      "security_updates_reboot@${reboot}" \
#      "-s" "security_updates_reboot_time=${reboot_time}" \
#      "security_updates_reboot_required@${security_updates_reboot_required}" \
#      "server_timezone=${current_timezone}" \
#      "system_settings_synced@true" >>/run/rebooted-sync
#  elif [[ $callback == "randomised" ]]; then
#    local seconds_to_sleep
#    seconds_to_sleep=$((RANDOM % 59 + 1))
#    local minutes_to_at_wait
#    minutes_to_at_wait=$((RANDOM % 179 + 1))
#    /bin/date >/run/rebooted-sync
#    echo "reboot system setting sync" >>/run/rebooted-sync
#    echo "sleep ${minutes_to_at_wait}m ${seconds_to_sleep}s" >>/run/rebooted-sync
#    echo "sleep ${seconds_to_sleep}; \
#      /usr/local/bin/gp callback \
#        /server/server-update \
#        -- \
#        server_ip=${serverIP} \
#        security_updates_reboot@${reboot} \
#        -s security_updates_reboot_time=${reboot_time} \
#        security_updates_reboot_required@${security_updates_reboot_required} \
#        server_timezone=${current_timezone} \
#        system_settings_synced@true >>/run/rebooted-sync" | at now + ${minutes_to_at_wait} minutes
#  fi

  # Before then we will just not include the callback for the reboot pending
  /bin/date >/run/rebooted-sync
  echo "reboot system setting sync" >>/run/rebooted-sync
  gridpane::callback::app \
    "callback" \
    "/server/server-update" \
    "--" \
    "server_ip=${serverIP}" \
    "security_updates_reboot@${reboot}" \
    "-s" "security_updates_reboot_time=${reboot_time}" \
    "server_timezone=${current_timezone}" \
    "system_settings_synced@true" >>/run/rebooted-sync
}