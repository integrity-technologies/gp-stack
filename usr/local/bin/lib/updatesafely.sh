#!/bin/bash
#
#

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#######################################
updatesafely::getargsetvar() {

  ((DEBUG)) && gridpane::displayfunc

  if [[ $# -gt 0 ]]; then
    while true; do
      local var="$1"
      case "${var}" in
      -manual) MANUAL_RUN=1 ;;
      -threshold)
        shift 1
        FAILURE_THRESHOLD=$1
        ;;
      -single-site)
        shift 1
        SINGLE_SITE=$1
        ;;
      -report-only)
        if [[ -n $SINGLE_SITE ]]; then
          shift 1
          if [[ $1 != "false" ]]; then
            REPORT_ONLY=1
          else
            REPORT_ONLY=0
          fi
        else
          REPORT_ONLY=1
        fi
        ;;
      -delay)
        shift 1
        DELAY=$1
        ;;
      -limit-urls)
        shift 1
        LIMIT_URLS=$1
        ;;
      -viewport)
        shift 1
        WIDTH=$1
        HEIGHT=$2
        ;;
      -wget-crawl) WGET_CRAWL=1 ;;
      -async-capture)
        shift 1
        ASYNC_CAPTURE=$1
        ;;
      -async-compare)
        shift 1
        ASYNC_COMPARE=$1
        ;;
      -debug) DEBUG=1 ;;
      -only-user-urls)
        if [[ -z $SINGLE_SITE ]]; then
          shift 1
          if [[ $1 != "false" ]]; then
            ONLY_USER_URLS=1
          else
            ONLY_USER_URLS=0
          fi
        else
          ONLY_USER_URLS=1
        fi
        ;;
      -set-defaults) SET_DEFAULTS=1 ;;
      -h | -help)
        updatesafely::usage
        exit 0
        ;;
      esac

      shift 1 || true

      if [[ -z "${var}" ]]; then
        break
      fi

    done
  fi

}

#######################################
# Print usage.
#
#  Outputs:
#   Writes status to STDOUT
#######################################
updatesafely::isupdateable() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"

  managed_domain="$(gridpane::get_managed_domain)"
  if [[ ${app_name} == "staging."* ||
    ${app_name} == "canary."* ||
    ${app_name} == "22222" ||
    ${app_name} == "default" ||
    ${app_name} == "html" ||
    ${app_name} == *"-remote" ||
    ${app_name} == *".${managed_domain}" ]]; then
    return 1
  elif [[ -d "/var/www/canary.${app_name}" ]]; then
    if [[ -f /var/www/${app_name}/no.updates ]]; then
      echo "Automatic Updates have been disabled for ${app_name}, moving on..."
      return 1
    else

      local siteowner=$(gridpane::get::set::site::user "${app_name}") || true
      local checkWP=$(sudo su - ${siteowner} -c "timeout 900 /usr/local/bin/wp core version --url=${app_name} --path=/var/www/${app_name}/htdocs 2>&1")

      if [[ ${checkWP} == *"rror"* ]]; then
        echo "Automatic Updates for ${app_name} are not possible, wp-cli error: ${checkWP}. Moving on..."
        return 1
      fi

      echo "System is nominal, proceeding to check available updates for ${app_name}..."
      return 0
    fi
  fi
}

updatesafely::setdefaults() {

  ((DEBUG)) && gridpane::displayfunc

  echo "-----------------------------"
  echo "Setting UpdateSafely Defaults"
  echo "-----------------------------"

  local site="$1"
  local conf_options=(WGET_CRAWL FAILURE_THRESHOLD REPORT_ONLY DELAY LIMIT_URLS WIDTH HEIGHT ASYNC_CAPTURE ASYNC_COMPARE ONLY_USER_URLS)
  local conf_element
  local conf_element_lowercase
  local conf_in_env
  local site
  local single_site_env
  local payload=()
  local payload_key
  local typecast
  local assignment
  local payload_value

  if [[ -n ${site} ]]; then
    single_site_env=" -site.env ${site}"
    payload+=("site_url=${site}")
    payload+=("server_ip=${serverIP}")
    payload+=("--")
  fi

  for conf_element in "${conf_options[@]}"; do

    if [[ -n ${!conf_element} ]]; then
      case ${conf_element} in
      FAILURE_THRESHOLD)
        typecast=1
        payload_key="us_threshold"
        assignment="="
        payload_value="${!conf_element}"
        ;;
      REPORT_ONLY)
        typecast=0
        payload_key="us_report_only"
        assignment="@"
        if [[ ${!conf_element} == "1" ]]; then
          payload_value="true"
        else
          payload_value="false"
        fi
        ;;
      DELAY)
        typecast=1
        payload_key="us_delay"
        assignment="="
        payload_value="${!conf_element}"
        ;;
      LIMIT_URLS)
        typecast=1
        payload_key="us_limit_urls"
        assignment="="
        payload_value="${!conf_element}"
        ;;
      WIDTH)
        typecast=1
        payload_key="us_viewport_width"
        assignment="="
        payload_value="${!conf_element}"
        ;;
      HEIGHT)
        typecast=1
        payload_key="us_viewport_height"
        assignment="="
        payload_value="${!conf_element}"
        ;;
      ASYNC_CAPTURE)
        typecast=1
        payload_key="us_async_capture"
        assignment="="
        payload_value="${!conf_element}"
        ;;
      ASYNC_COMPARE)
        typecast=1
        payload_key="us_async_compare"
        assignment="="
        payload_value="${!conf_element}"
        ;;
      ONLY_USER_URLS)
        typecast=0
        payload_key="us_user_urls_only"
        assignment="@"
        if [[ ${!conf_element} == "1" ]]; then
          payload_value="true"
        else
          payload_value="false"
        fi
        ;;
      esac

      conf_element_lowercase="$(echo ${conf_element} | tr [:upper:] [:lower:])"
      gridpane::conf_write updatesafely-${conf_element_lowercase} ${payload_value}${single_site_env}
      echo "${conf_element}:${payload_value}"

      if [[ -n ${site} ]]; then
        [[ ${typecast} == "1" ]] && payload+=("-s")
        payload+=("${payload_key}${assignment}${payload_value}")
      fi
    fi
    typecast=""
  done

  if [[ -n ${site} ]]; then
    payload=$(/usr/bin/jo "${payload[@]}")
    echo "--------------------------------------------------------"
    echo "Callback"
    echo "--------------------------------------------------------"
    echo "Endpoint:"
    echo "https://${GPURL}/api/site/site-update"
    echo "Payload:"
    echo "${payload}"
    curl \
      -d "${payload}" \
      -H "Content-Type: application/json" \
      -X POST https://${GPURL}/api/site/site-update?api_token=${gridpanetoken}
  fi

  # If we are setting defaults we don't want to do anything else. This is only for app to run really
  exit
}

updatesafely::getdefaults() {

  ((DEBUG)) && gridpane::displayfunc

  local site="$1"
  local log_file="$2"
  local conf_options=(WGET_CRAWL FAILURE_THRESHOLD REPORT_ONLY DELAY LIMIT_URLS WIDTH HEIGHT ASYNC_CAPTURE ASYNC_COMPARE ONLY_USER_URLS)
  local conf_element
  local conf_element_lowercase
  local conf_in_env
  ## seriously... strip it in function above and then readd it here... my oh my
  local single_site_env=" -site.env canary.${site}"

  for conf_element in "${conf_options[@]}"; do

    local conf_element_fallback_default
    # Set default values if none are passed and there is an empty server and site env
    case ${conf_element} in
    WGET_CRAWL) conf_element_fallback_default=0 ;;
    FAILURE_THRESHOLD) conf_element_fallback_default=2 ;;
    REPORT_ONLY) conf_element_fallback_default=1 ;;
    DELAY) conf_element_fallback_default=1000 ;;
    LIMIT_URLS) conf_element_fallback_default=5 ;;
    WIDTH) conf_element_fallback_default=1440 ;;
    HEIGHT) conf_element_fallback_default=900 ;;
    ASYNC_CAPTURE) conf_element_fallback_default=1 ;;
    ASYNC_COMPARE) conf_element_fallback_default=5 ;;
    ONLY_USER_URLS) conf_element_fallback_default=1 ;;
    esac

    conf_element_lowercase="$(echo ${conf_element} | tr [:upper:] [:lower:])"

    if [[ -z "$(echo ${conf_element})" ]]; then
      # inside the conf_reads we want to callback to app if empty of value to check and fix
      conf_element_value=$(gridpane::conf_read updatesafely-${conf_element_lowercase} ${single_site_env} >/dev/null) || true
      if [[ -z ${conf_element_value} ]]; then
        # No site specific value is set, fallback to the server value... (assumes that no site value is correct)
        conf_element_value=$(gridpane::conf_read updatesafely-${conf_element_lowercase} >/dev/null) || true
      fi
      if [[ -z ${conf_element_value} ]]; then
        # No server value seems to be set - set it to our defaults. These would all check against app and callback to stop config drift.
        conf_element_value=${conf_element_fallback_default}
        gridpane::conf_write updatesafely-${conf_element_lowercase} ${conf_element_value}
      fi
      export ${conf_element}=${conf_element_value}
      echo "${conf_element}: ${conf_element_value}" | updatesafely::showandlogoutputindentednocolor "${log_file}"
    else
      echo "${conf_element}: $(echo ${!conf_element})" | updatesafely::showandlogoutputindentednocolor "${log_file}"
    fi
  done
}

#######################################
# Print usage.
#
#  Outputs:
#   Writes status to STDOUT
#######################################
updatesafely::usage() {

  echo -e "\

  Usage: updatesafely [options]

  Options:

  -manual\t\t\t Run in manual mode.
  -single-site <domain>\t\t Run against specified domain.
  -report-only\t\t\t Do not Run updates, only run report.
  -delay <milliseconds>\t\t How long to wait before taking a snapshot, after page has loaded (default: ${DELAY}).
  -limit-urls <integer>\t\t Limit the number of urls tested of each post type (default: ${LIMIT_URLS}),
  \t\t\t\t this is in addition to any URLs included via the updatesafely.urls file.
  -only-user-urls \t\t Only test using the urls provided in the updatesafely.urls file.
  -wget-crawl \t\t\t Crawl site externally vs internally.
  -threshold <percentage>\t The difference between sites, in percentage, before considered a failure (default: ${FAILURE_THRESHOLD}).
  -viewport <width> <height>\t The viewport size to use during testing (default: ${WIDTH} ${HEIGHT}).
  -async-capture <integer>\t Limit the number of asynchronous screen captures (default: ${ASYNC_CAPTURE}).
  -async-compare <integer>\t Limit the number of asynchronous screen capture comparisons (default: ${ASYNC_COMPARE}).
  -debug\t\t\t Show debug messages (default: ${DEBUG}).

  Useful files:

  /var/www/<domain>/updatesafely.urls\t\t Add any URLs you'd like to include in this file, each on a new line.
  /var/www/<domain>/updatesafely.blacklist\t Add any URLs or Post Types you'd like to not include in this file, each on a new line.
  /var/www/<domain>/logs/updates.log\t\t Full process logging for each site.
  "

}

#######################################
# Check if there's updates on the primary
# site and set $return_code accordingly.
#
#  Outputs:
#   Writes status to STDOUT
#######################################
updatesafely::arethereupdates() {

  #can't return function name
  #gridpane::displayfunc

  local app_name="$1"
  local siteowner="$2"
  local return_code=0

  local plugin_updates
  plugin_updates="$(sudo su - "${siteowner}" -c "timeout 1200 /usr/local/bin/wp plugin list --path=/home/${siteowner}/sites/${app_name}/htdocs/")"
  local theme_updates
  theme_updates="$(sudo su - "${siteowner}" -c "timeout 1200 /usr/local/bin/wp theme list --path=/home/${siteowner}/sites/${app_name}/htdocs/")"
  local core_updates
  core_updates="$(sudo su - "${siteowner}" -c "timeout 1200 /usr/local/bin/wp core check-update --path=/home/${siteowner}/sites/${app_name}/htdocs/")"

  if [[ "${plugin_updates}" == *"available"* ]]; then
    echo "${plugin_updates}" >>"/var/www/${app_name}/logs/updates.log" 2>&1
    return_code=$((return_code + 10))
  fi

  if [[ "${theme_updates}" == *"available"* ]]; then
    echo "${theme_updates}" >>"/var/www/${app_name}/logs/updates.log" 2>&1
    return_code=$((return_code + 30))
  fi

  if [[ "${core_updates}" != "WordPress is at the latest version." ]]; then
    echo "${core_updates}" >>"/var/www/${app_name}/logs/updates.log" 2>&1
    return_code=$((return_code + 50))
  fi

  echo ${return_code}

}

#######################################
# Check GridPane subdomain NginX config
# and update if neccessary.
#
#  Outputs:
#   Writes status to STDOUT
#######################################
updatesafely::checkgridsubconf() {

  ((DEBUG)) && gridpane::displayfunc

  local grid_subdom_config="$(cat "/etc/nginx/sites-enabled/${grid_subdom}")"

  if [[ ${grid_subdom_config} != *"location /updatesafely/"* ]]; then

    # put your seatbelts on and get ready for some hacky ass shit
    local original_local_line_num="$(echo "$grid_subdom_config" | grep -n "location / {" | awk -F':' '{print $1}')"
    local insert_line_num=$((original_local_line_num + 3))
    sed -i "${insert_line_num}i \ \n \ \ \ location /updatesafely/ {\n \ \ \ \ \ \ \ alias /var/www/${grid_subdom}/updatesafely/;\n \ \ \ \ \ \ \ try_files \$uri \$uri/ /index.php?\$args;\n \ \ \ }" /etc/nginx/sites-available/${grid_subdom}

  fi

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#  Original:
#   SafelyAllSites
#######################################
updatesafely::run() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"
  local siteowner="$2"
  local update_status="$3"
  local log_file="/var/www/${app_name}/logs/updates.log"

  loop_start="$(date +%s)"

  echo "Setting up hosts redirect for canary..." | updatesafely::showandlogoutputindented "${log_file}"
  updatesafely::setup::localcanary "${app_name}" 2>&1 | updatesafely::showandlogoutputindentednocolor "${log_file}"

  echo "Setting parameters..." | updatesafely::showandlogoutputindented "${log_file}"
  updatesafely::getdefaults "${app_name}" "${log_file}"

  echo "Collecting list of pages to test..." | updatesafely::showandlogoutputindented "${log_file}"
#  mapfile -t page_links < <(updatesafely::setup::getpages "${app_name}" "${siteowner}")
  page_links=($(updatesafely::setup::getpages "${app_name}" "${siteowner}"))

  echo "Duplicating live site to canary site..." | updatesafely::showandlogoutputindented "${log_file}"
  updatesafely::setup::dupelive "${app_name}" 2>&1 | updatesafely::showandlogoutputindentednocolor "${log_file}"

  echo "Building backstopjs configuration..." | updatesafely::showandlogoutputindented "${log_file}"
  updatesafely::setup::buildconfig "${app_name}" "${page_links[@]}" 2>&1 | updatesafely::showandlogoutputindentednocolor "${log_file}"

  echo "Updating canary site..." | updatesafely::showandlogoutputindented "${log_file}"
  updatesafely::setup::updatecanary "${app_name}" "${siteowner}" "${update_status}" 2>&1 | updatesafely::showandlogoutputindentednocolor "${log_file}"

  if [[ ${SINGLE_SITE} ]]; then
    gridpane::notify::app \
      "UpdateSafely Notice" \
      "Canary Cloning of ${app_name} and configuration completed.<br>Proceeding to begin visual regression testing, this may take some time..." \
      "popup_and_center" \
      "fa-shield" \
      "long"
  fi

  echo "Performing visual comparisons..." | updatesafely::showandlogoutputindented "${log_file}"
  updatesafely::setup::comparesites "${app_name}" "${siteowner}" "${update_status}" > >(updatesafely::showandlogoutputindentednocolor "${log_file}") 2>&1

  echo "Remove Canary Site..." | updatesafely::showandlogoutputindented "${log_file}"
  updatesafely::clean::cleanupcanary "${app_name}" "${siteowner}" 2>&1 | updatesafely::showandlogoutputindentednocolor "${log_file}"

  echo "Remove Hosts redirect..." | updatesafely::showandlogoutputindented "${log_file}"
  updatesafely::clean::localcanary "${app_name}" 2>&1 | updatesafely::showandlogoutputindentednocolor "${log_file}"

  loop_end="$(date +%s)"
  loop_runtime=$((loop_end - loop_start))

  echo "-----------------------------------------------------------" | updatesafely::showandlogoutput "${log_file}"
  echo "Process started at $loop_start..." | updatesafely::showandlogoutput "${log_file}"
  echo "Process ended at $loop_end..." | updatesafely::showandlogoutput "${log_file}"
  echo "Completed upgrades and testing for ${app_name} - Total runtime: ${loop_runtime} seconds." | updatesafely::showandlogoutput "${log_file}"
  echo "-----------------------------------------------------------" | updatesafely::showandlogoutput "${log_file}"

}

#######################################
# Install BackstopJS if not installed
#
#  Outputs:
#   Writes status to STDOUT
#######################################
updatesafely::install() {

  ((DEBUG)) && gridpane::displayfunc

  local current_time=$(date +%s)
  local last_apt_update=$(stat -c %Y /var/lib/apt/periodic/update-success-stamp)
  local time_since_last_apt_update=$((${current_time} - ${last_apt_update}))

  # Don't waste time running if it's ran in the last 5 days...
  if [[ ${time_since_last_apt_update} -gt 432000 ]]; then
    apt-get update
  fi

  if ! type npm >/dev/null; then
    gridpane::preinstallaptcheck >/dev/null
    echo "Installing npm and backstop dependencies..."
    apt-get -y install mydumper npm libnss3-dev libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 \
      libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 \
      libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 \
      libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 \
      libnss3 | tee -a /var/log/gridpane.log

    gridpane::notify::app \
      "UpdateSafely Notice" \
      "NPM and Backstop dependencies installed...." \
      "popup_and_center" \
      "fa-shield" \
      "long"
  fi

  if ! type backstop >/dev/null; then
    echo "Installing backstopjs..."
    npm -g install backstopjs@5.0.0 | tee -a /var/log/gridpane.log

    gridpane::notify::app \
      "UpdateSafely Notice" \
      "Backstop JS installed...." \
      "popup_and_center" \
      "fa-shield" \
      "long"
  fi

  if ! backstop -v | grep "v5.0.0" >/dev/null; then
    echo "Installing backstopjs v5.0.0"
    npm -g install backstopjs@5.0.0 | tee -a /var/log/gridpane.log

    gridpane::notify::app \
      "UpdateSafely Notice" \
      "Backstop JS v5.0.0 installed...." \
      "popup_and_center" \
      "fa-shield" \
      "long"
  fi

  if ! type mydumper >/dev/null; then
    gridpane::preinstallaptcheck >/dev/null
    echo "Installing mydumper..."
    apt-get -y install mydumper | tee -a /var/log/gridpane.log

    gridpane::notify::app \
      "UpdateSafely Notice" \
      "MyDumper installed...." \
      "popup_and_center" \
      "fa-shield" \
      "long"
  fi

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#  Original:
#   GoodTime
#######################################
updatesafely::goodtime() {

  ((DEBUG)) && gridpane::displayfunc

  local hour=$(date +"%-H")

  if [[ ${hour} -le 5 ]]; then
    echo "We are inside an optimal maintenance window..."
  else
    echo "You're trying to update outside of optimal maintenance hours..."
    read -t 10 -n 1 -s -r -p "Please confirm with Y (Case Sensitive) or this update will be skipped!" </dev/tty

    if [[ $REPLY =~ ^[Y]$ ]]; then
      echo "Proceeding with manual (off hours) update!"
    else
      exit 187
    fi
  fi

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#  Original:
#   StorageCheck
#######################################
updatesafely::check::storage() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"
  local disk_free
  local disk_usage
  local canary_size
  local disk_after_canary
  local disk_free_after_canary
  local acceptable_free_disk
  local log_file="/var/www/${app_name}/logs/updates.log"

  cd /var/www/${app_name}/htdocs

  echo "Verifying we have enough space to run..." | updatesafely::showandlogoutputindented "${log_file}"

  disk_usage=$(df)
  disk_free=$(echo "${disk_usage}" | awk '$NF=="/"{printf "%d", $4}')
  disk_size=$(echo "${disk_usage}" | awk '$NF=="/"{printf "%d", $2}')
  disk_used=$(echo "${disk_usage}" | awk '$NF=="/"{printf "%d", $3}')
  if ! type diskus >/dev/null; then
    echo "no"
    canary_size=$(/usr/bin/ionice -c 2 -n 2 /usr/bin/du -s /var/www/${app_name} | /usr/bin/awk '{print $1}')
  else
    echo "yes"
    canary_size=$(/usr/bin/ionice -c 2 -n 2 /usr/bin/diskus /var/www/${app_name} | /usr/bin/awk '{print $1}')
    canary_size=$((canary_size / 1024))
  fi
  disk_after_canary=$((${disk_used} + ${canary_size}))
  disk_free_after_canary=$((${disk_size} - ${disk_after_canary}))
  acceptable_free_disk=2048000

  echo "Acceptable Free Disk After UpdateSafely: ${acceptable_free_disk} KB" | updatesafely::showandlogoutputindentednocolor "${log_file}"
  echo "Predicted Disk Free After UpdateSafely: ${disk_free_after_canary} KB" | updatesafely::showandlogoutputindentednocolor "${log_file}"

  if [[ "${disk_free_after_canary}" -lt "${acceptable_free_disk}" ]]; then
    echo "UpdateSafely is unavailable - Free Disk Space (Under 2GB) falls outside of acceptable parameters to canary!" | updatesafely::showandlogoutputindentednocolor "${log_file}"

    if [[ ${SINGLE_SITE} ]]; then
      gridpane::notify::app \
        "UpdateSafely Notice" \
        "UpdateSafely is unavailable - Free Disk Space (Under 2GB) falls outside of acceptable parameters to canary!" \
        "popup_and_center" \
        "fa-exclamation" \
        "long"
    fi

    return 1
  else
    echo "Disk Space Free within acceptable parameters for canary (greater than 2GB)" | updatesafely::showandlogoutputindentednocolor "${log_file}"
    return 0
  fi
}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#  Original:
#   LocalHostsRedirect
#######################################
updatesafely::setup::localcanary() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"

  echo "Modifying local hosts file to make local visual confirmation checking possible..."

  echo "We need to add ${app_name} and canary.${app_name} to point to 127.0.0.1..."

  sed -i '1s/^/# End UpdateSafely Local Hosts Redirect \n/' /etc/hosts
  sed -i "1s/^/127.0.0.1   ${app_name}  \n/" /etc/hosts
  sed -i "1s/^/127.0.0.1   canary.${app_name}  \n/" /etc/hosts
  sed -i '1s/^/# Begin UpdateSafely Local Hosts Redirect  \n/' /etc/hosts

  echo "Local hosts file is redirecting for site ${app_name} and canary.${app_name}..."

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#  Original:
#   LocalHostsRedirect
#######################################
updatesafely::setup::getpages() {

  #gridpane::displayfunc

  local app_name="$1"
  local siteowner="$2"
  local app_path
  app_path="/var/www/${app_name}/htdocs"
  local conf_type
  conf_type=$(gridpane::check::site::nginx::config "${app_name}" -q)
  local post_list
  post_list=("$(sudo su - "${siteowner}" -c "timeout 300 /usr/local/bin/wp option get home --path=${app_path}")")

  if ! ((ONLY_USER_URLS)); then
    if ((WGET_CRAWL)); then
      post_list=($(wget -w 1 --spider -e robots=off --reject css,js,jpg,jpeg,png,gif,txt,xml,index.php --reject-regex '(/?feed.*|xmlrpc.php.*)' -D ${app_name} -r ${conf_type}://${app_name}/ 2>&1 | grep '^--' | awk '{ print $3 }' | sort | uniq | sed "s|${conf_type}://${app_name}||g" | sed -n 1,${LIMIT_URLS}p))
    else
      local post_type_list=($(sudo su - "${siteowner}" -c "timeout 300 /usr/local/bin/wp post-type list --field=name --public=1 --path=${app_path}"))
      echo "Available Post Types:" >>"/var/www/${app_name}/logs/updates.log" 2>&1
      echo "${post_type_list[@]}" >>"/var/www/${app_name}/logs/updates.log" 2>&1

      for post_type in "${post_type_list[@]}"; do
        if [[ -f "/var/www/${app_name}/updatesafely.blacklist" ]]; then
          while read blacklist_type; do
            if [[ ${post_type} == "${blacklist_type}" ]]; then
              echo "User Blacklisted Post Types: ${blacklist_type}" >>/var/www/${app_name}/logs/updates.log 2>&1
              continue
            fi
            post_list+=($(sudo su - ${siteowner} -c "timeout 300 /usr/local/bin/wp post list --post_type=${post_type} --field=url --path=${app_path}")) || true
#            post_list+=($(sudo su - ${siteowner} -c "timeout 300 /usr/local/bin/wp post list --post_type=${post_type} --field=url --path=${app_path}" | sed "s|${conf_type}://${app_name}||g" | grep -v \?post_type\=[a-z0-9]\\+\&[0-9]\* | sed -n 1,${LIMIT_URLS}p)) || true
          done </var/www/${app_name}/updatesafely.blacklist
        else
          post_list+=($(sudo su - ${siteowner} -c "timeout 300 /usr/local/bin/wp post list --post_type=${post_type} --field=url --path=${app_path}")) || true
#          post_list+=($(sudo su - ${siteowner} -c "timeout 300 /usr/local/bin/wp post list --post_type=${post_type} --field=url --path=${app_path}" | sed "s|${conf_type}://${app_name}||g" | grep -v \?post_type\=[a-z0-9]\\+\&[0-9]\* | sed -n 1,${LIMIT_URLS}p)) || true
        fi
      done
    fi
  fi

  if [[ -f "/var/www/${app_name}/updatesafely.urls" ]]; then
    while read user_url; do
      echo "User Specified URL: ${user_url}" >>/var/www/${app_name}/logs/updates.log 2>&1
      post_list+=(${user_url})
    done </var/www/${app_name}/updatesafely.urls
    post_list=($(printf "%s\n" "${post_list[@]}" | sort -u))
  fi

  if [[ -f "/var/www/${app_name}/updatesafely.blacklist" ]]; then
    while read blacklist_url; do
      echo "User Blacklisted URL: ${blacklist_url}" >>/var/www/${app_name}/logs/updates.log 2>&1
      post_list=("${post_list[@]/$blacklist_url/}")
    done </var/www/${app_name}/updatesafely.blacklist
  fi

  echo "URLs to test:" >>/var/www/${app_name}/logs/updates.log 2>&1
  echo "${post_list[@]}" | tee -a /var/www/${app_name}/logs/updates.log 2>&1

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#  Original:
#   BuildConfigs
#######################################
updatesafely::setup::buildconfig() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"
  shift
  local page_links
#  mapfile -t page_links <<< "$*"
  page_links=($*)
  local page_links_len
  page_links_len=${#page_links[@]}
  local backstopjs_config
  backstopjs_config="/opt/gridpane/${app_name}-backstop.json"
  local conf_type
  conf_type=$(gridpane::check::site::nginx::config "${app_name}" -q)
  local canary_conf_type
  canary_conf_type=$(gridpane::check::site::nginx::config "canary.${app_name}" -q)
  local ready_event
  ready_event="null"
  local on_ready_script
  on_ready_script="null"
  local on_before_script
  on_before_script="null"

  local scheme
  scheme="http"
  [[ $conf_type == *"https"* ]] &&
    scheme="https"

  local reference_url_app_name
  reference_url_app_name="$app_name"
  [[ $conf_type == *"www"* ]] &&
    reference_url_app_name="www.$reference_url_app_name"

  local canary_scheme
  canary_scheme="http"
  [[ $canary_conf_type == *"https"* ]] &&
    canary_scheme="https"

  local canary_base_url
  canary_base_url="${canary_scheme}://canary.${app_name}"
  local app_base_url
  app_base_url="${scheme}://${reference_url_app_name}"

  cat <<EOF >"${backstopjs_config}"
{
  "viewports": [
    {
      "name": "Screen",
      "width": ${WIDTH},
      "height": ${HEIGHT}
    }
  ],
  "scenarios": [
EOF

  for page in "${page_links[@]}"; do
    local canary_page
    local label
    canary_page="$canary_base_url"
    if [[ $page != "$app_base_url" ]]; then
      canary_path=${page#$app_base_url}
      canary_page="$canary_page$canary_path"
      label="$canary_path"
    else
      label="/"
    fi
    cat <<EOF >>"${backstopjs_config}"
    {
      "label": "${label}",
      "url": "${canary_page}",
      "referenceUrl": "${page}",
      "delay": ${DELAY},
      "misMatchThreshold": "${FAILURE_THRESHOLD}",
      "selectors": [
        "document"
      ],
      "hideSelectors": [
        "iframe"
      ]
EOF

    if [[ ${page_links_len} -gt 1 ]]; then
      echo "    }," | tee -a "${backstopjs_config}" >/dev/null
    else
      echo "    }" | tee -a "${backstopjs_config}" >/dev/null
    fi

    ((page_links_len--))
  done

  cat <<EOF >>"${backstopjs_config}"
  ],
  "paths": {
    "bitmaps_reference": "/opt/gridpane/${app_name}-backstop_data/bitmaps_reference",
    "bitmaps_test": "/opt/gridpane/${app_name}-backstop_data/bitmaps_test",
    "engine_scripts": "/opt/gridpane/${app_name}-backstop_data/engine_scripts",
    "html_report": "/opt/gridpane/${app_name}-backstop_data/${app_name}",
    "ci_report": "/opt/gridpane/${app_name}-backstop_data/ci_report"
  },
  "report": ["browser"],
  "engine": "puppeteer",
  "engineOptions": {
    "args": ["--no-sandbox"]
  },
  "asyncCaptureLimit": ${ASYNC_CAPTURE},
  "asyncCompareLimit": ${ASYNC_COMPARE},
  "debug": false,
  "debugWindow": false
}
EOF

  echo "${backstopjs_config}"
  cat "${backstopjs_config}"

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#  Original:
#   DupeLive
#######################################
updatesafely::setup::dupelive() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"
  local epoch_time="$(date +%s)"

  local primary_DB_name=$(cat /var/www/${app_name}/wp-config.php | grep DB_NAME | cut -d \' -f 4)
  local primary_DB_user=$(cat /var/www/${app_name}/wp-config.php | grep DB_USER | cut -d \' -f 4)
  local primary_DB_pass=$(cat /var/www/${app_name}/wp-config.php | grep DB_PASSWORD | cut -d \' -f 4)

  mydumper -B "${primary_DB_name}" -u "${primary_DB_user}" -p "${primary_DB_pass}" -o "/var/www/canary.${app_name}/${app_name}.sql" --lock-all-tables

  canary_table_prefix=$(sed -n -e '/$table_prefix/p' "/var/www/${app_name}/wp-config.php")

  #tar -czf /var/www/canary.${app_name}/htdocs/update-test.gz -C /var/www/${app_name}/htdocs . --exclude '*.zip' --exclude '*.gz' --exclude 'wp-config.php' || true
  rsync -aHAXx --numeric-ids --delete --exclude={'*.zip','*.gz','wp-config.php'} "/var/www/${app_name}/htdocs/" "/var/www/canary.${app_name}/htdocs"
  rsync -aHAXx --numeric-ids --delete --exclude={'*.zip','*.gz','wp-config.php','*-sockfile.conf'}" /var/www/${app_name}/nginx/" "/var/www/canary.${app_name}/nginx"

  find "/var/www/canary.${app_name}/nginx/" -mindepth 1 -maxdepth 1 -name '*.conf' -type f -exec sh -c \
    'i="$1"; sed -i "s|www/'"${app_name}"'/nginx|www/canary.'"${app_name}"'/nginx|g" $i' _ {} \;

  cd /var/www/canary.${app_name}/htdocs

  #tar -xzf update-test.gz --overwrite

  sed -i "/\$table_prefix =/c${canary_table_prefix}" /var/www/canary.${app_name}/wp-config.php

  canary_DB_name=$(cat /var/www/canary.${app_name}/wp-config.php | grep DB_NAME | cut -d \' -f 4)
  canary_DB_user=$(cat /var/www/canary.${app_name}/wp-config.php | grep DB_USER | cut -d \' -f 4)
  canary_DB_pass=$(cat /var/www/canary.${app_name}/wp-config.php | grep DB_PASSWORD | cut -d \' -f 4)

  echo "canary_DB_name: ${canary_DB_name}"
  echo "canary_DB_user: ${canary_DB_user}"
  echo "canary_DB_pass: ${canary_DB_pass}"

  #mysql -u${canary_DB_user} -p${canary_DB_pass} ${canary_DB_name} </var/www/canary.${app_name}/${epoch_time}-database.sql
  myloader -B "${canary_DB_name}" -u "${canary_DB_user}" -p "${canary_DB_pass}" -d /var/www/canary.${app_name}/${app_name}.sql -o

  app_conf_type=$(gridpane::check::site::nginx::config "${app_name}" -q)
  if [[ ${app_conf_type} == *"https"* ]]; then

    [[ ! -d /etc/nginx/ssl/canary.${app_name} ]] && mkdir -p /etc/nginx/ssl/canary.${app_name}

    openssl req -newkey rsa:4096 \
      -x509 \
      -sha256 \
      -days 3650 \
      -nodes \
      -out /etc/nginx/ssl/canary.${app_name}/cert.pem \
      -keyout /etc/nginx/ssl/canary.${app_name}/key.pem \
      -subj "/C=US/ST=Michigan/L=Bath/O=GridPane/OU=Dev/CN=canary.${app_name}"

    [[ -f /etc/nginx/ssl/canary.${app_name}/key.pem ]] &&
      echo "ssl_stapling off;" >/var/www/canary.${app_name}/nginx/ssl-stapling-main-context.conf &&
      /usr/local/bin/gp conf nginx generate https redis canary.${app_name}

    nginx -t && /usr/local/bin/gp nginx reload

  fi

  updatesafely::siteurls "${app_name}" "${canary_DB_name}" "${canary_DB_user}" "${canary_DB_pass}"

  #  updatesafely::setup::siteurls "${app_name}"

  #rm /var/www/${app_name}/htdocs/table.prefix
  #rm /var/www/canary.${app_name}/${epoch_time}-database.sql
  rm -r /var/www/canary.${app_name}/${app_name}.sql

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#  Original:
#   updateSiteUrls
#######################################
updatesafely::siteurls() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"
  local db_name="$2"
  local db_user="$3"
  local db_pass="$4"

  if [[ ! -f /usr/local/bin/Search-Replace-DB/srdb.cli.php ]]; then
    cd /root
    git clone https://github.com/interconnectit/Search-Replace-DB.git
    mv /root/Search-Replace-DB /usr/local/bin/Search-Replace-DB
    cd -
  fi

  echo "----------------------------------------------------------------------"

  echo "'/${app_name}' -> '/canary.${app_name}'"

  search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "localhost" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -s "/${app_name}" -r "/canary.${app_name}" --exclude-cols=guid --allow-old-php 2>&1)
  search_replace_response=$?
  echo "${search_replace_db_output}"
  if [[ $search_replace_response != 0 ]]; then
    sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace '/${app_name}' '/canary.${app_name}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/canary.${app_name}/htdocs 2>&1"
  fi

  echo "----------------------------------------------------------------------"

  echo "'\/${app_name}' -> '\/canary.${app_name}'"

  search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "localhost" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -s "\\\/${app_name}" -r "\\\/canary.${app_name}" --exclude-cols=guid --allow-old-php 2>&1)
  search_replace_response=$?
  echo "${search_replace_db_output}"
  if [[ $search_replace_response != 0 ]]; then
    sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace '\/${app_name}' '\/canary.${app_name}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/canary.${app_name}/htdocs 2>&1"
  fi

  echo "----------------------------------------------------------------------"

  echo "'%2F${app_name}' -> '%2Fcanary.${app_name}'"

  search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "localhost" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -s "%2F${app_name}" -r "%2Fcanary.${app_name}" --exclude-cols=guid --allow-old-php 2>&1)
  search_replace_response=$?
  echo "${search_replace_db_output}"
  if [[ $search_replace_response != 0 ]]; then
    sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace '%2F${app_name}' '%2Fcanary.${app_name}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/canary.${app_name}/htdocs 2>&1"
  fi

  echo "----------------------------------------------------------------------"

  echo "'/www.${app_name}' -> '/canary.${app_name}'"

  search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "localhost" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -s "/www.${app_name}" -r "/canary.${app_name}" --exclude-cols=guid --allow-old-php 2>&1)
  search_replace_response=$?
  echo "${search_replace_db_output}"
  if [[ $search_replace_response != 0 ]]; then
    sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace '/www.${app_name}' '/canary.${app_name}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/canary.${app_name}/htdocs 2>&1"
  fi

  echo "----------------------------------------------------------------------"

  echo "'\/www.${app_name}' -> '\/canary.${app_name}'"

  search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "localhost" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -s "\\\/www.${app_name}" -r "\\\/canary.${app_name}" --exclude-cols=guid --allow-old-php 2>&1)
  search_replace_response=$?
  echo "${search_replace_db_output}"
  if [[ $search_replace_response != 0 ]]; then
    sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace '\/www.${app_name}' '\/canary.${app_name}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/canary.${app_name}/htdocs 2>&1"
  fi

  echo "----------------------------------------------------------------------"

  echo "'%2Fwww.${app_name}' -> '%2Fcanary.${app_name}'"

  search_replace_db_output=$(php /usr/local/bin/Search-Replace-DB/srdb.cli.php -h "localhost" -n "${db_name}" -u "${db_user}" -p "${db_pass}" -s "%2Fwww.${app_name}" -r "%2Fcanary.${app_name}" --exclude-cols=guid --allow-old-php 2>&1)
  search_replace_response=$?
  echo "${search_replace_db_output}"
  if [[ $search_replace_response != 0 ]]; then
    sudo su - "${dest_siteowner}" -c "timeout 7200 /usr/local/bin/wp search-replace '%2Fwww.${app_name}' '%2Fcanary.${app_name}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/canary.${app_name}/htdocs 2>&1"
  fi

  echo "----------------------------------------------------------------------"

  local siteowner=$(gridpane::get::set::site::user "${app_name}") || true

  sudo su - "${siteowner}" -c "timeout 30 /usr/local/bin/wp cache flush --path=/var/www/canary.${app_name}/htdocs 2>&1"

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#  Original:
#   updateSiteUrls
#######################################
updatesafely::setup::siteurls() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"
  local siteowner=$(gridpane::get::set::site::user ${app_name}) || true
  local canary_app_name="canary.${app_name}"

  echo "----------------------------------------------------------------------"

  echo "/${app_name} -> /${canary_app_name}"

  sudo su - ${siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace '/${app_name}' '/${canary_app_name}' --all-tables --skip-columns=guid --report-changed-only --precise  --path=/var/www/${app_name}/htdocs 2>&1"

  echo "----------------------------------------------------------------------"

  echo "\/${app_name} -> \/canary.${app_name}"

  sudo su - ${siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace '\/${app_name}' '\/${canary_app_name}' --all-tables --skip-columns=guid --report-changed-only --precise  --path=/var/www/${app_name}/htdocs 2>&1"

  echo "----------------------------------------------------------------------"

  echo "%2F${app_name} -> %2Fcanary.${app_name}"

  sudo su - ${siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace '%2F${app_name}' '%2F${canary_app_name}' --all-tables --skip-columns=guid --report-changed-only --precise  --path=/var/www/${app_name}/htdocs 2>&1"

  echo "----------------------------------------------------------------------"

  echo "/www.${app_name} -> /canary.${app_name}"

  sudo su - ${siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace '/www.${app_name}' '/${canary_app_name}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${app_name}/htdocs 2>&1"

  echo "----------------------------------------------------------------------"

  echo "\/www.${app_name} -> \/canary.${app_name}"

  sudo su - ${siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace '\/www.${app_name}' '\/${canary_app_name}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${app_name}/htdocs 2>&1"

  echo "----------------------------------------------------------------------"

  echo "%2Fwww.${app_name} -> %2Fcanary.${app_name}"

  sudo su - ${siteowner} -c "timeout 7200 /usr/local/bin/wp search-replace '%2Fwww.${app_name}' '%2F${canary_app_name}' --all-tables --skip-columns=guid --report-changed-only --precise --path=/var/www/${app_name}/htdocs 2>&1"

  echo "----------------------------------------------------------------------"

  sudo su - ${siteowner} -c "timeout 30 /usr/local/bin/wp cache flush --path=/var/www/${app_name}/htdocs 2>&1"

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#  Original:
#   UpdateCanary
#######################################
updatesafely::setup::updatecanary() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"
  local siteowner="$2"
  local update_status="$3"

  # is this convoluted enough?
  if [[ ${update_status} -eq 40 || ${update_status} -eq 80 || ${update_status} -eq 90 ]]; then
    echo "Updating all Themes..."
    sudo su - ${siteowner} -c "timeout 900 /usr/local/bin/wp theme update --all --path=/var/www/canary.${app_name}/htdocs --url=${app_name}"
  fi

  if [[ ${update_status} -eq 10 || ${update_status} -eq 40 || ${update_status} -eq 90 ]]; then
    echo "Updating all Plugins..."
    sudo su - ${siteowner} -c "timeout 900 /usr/local/bin/wp plugin update --all --path=/var/www/canary.${app_name}/htdocs --url=${app_name}"
  fi

  if [[ ${update_status} -eq 50 || ${update_status} -eq 60 || ${update_status} -eq 90 ]]; then
    echo "Updating WP Core..."
    sudo su - ${siteowner} -c "timeout 900 /usr/local/bin/wp core update --path=/var/www/canary.${app_name}/htdocs --url=${app_name}"
  fi

  export QT_QPA_PLATFORM=

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#######################################
updatesafely::setup::comparesites() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"
  local siteowner="$2"
  local update_status="$3"
  local success_state
  local notification_body

  if [[ -d /opt/gridpane/${app_name}-backstop_data/ ]]; then
    rm -fr /opt/gridpane/${app_name}-backstop_data/
  fi

  gridpane::cache::clear "canary.${app_name}"

  backstop --config=/opt/gridpane/${app_name}-backstop.json reference || true
  local backstop_test="$(backstop --config=/opt/gridpane/${app_name}-backstop.json test || true)"

  if [[ "${backstop_test}" == *"compare | ERROR"* ]] || ((REPORT_ONLY)); then
    if [[ "${backstop_test}" == *"compare | ERROR"* ]]; then
      echo "Visual regression test failed..."
      success_state="failed"
      if ((REPORT_ONLY)) && [[ ${SINGLE_SITE} ]]; then
        notification_body="Updates for ${app_name} failed visual regression testing... report only run, compiling report..."
      elif ! ((REPORT_ONLY)) && [[ ${SINGLE_SITE} ]]; then
        notification_body="Updates for ${app_name} failed visual regression testing, unable to proceed to update site... compiling report..."
      fi
    fi
    if [[ "${backstop_test}" == *"report | 0 Failed"* ]] && ((REPORT_ONLY)); then
      success_state="succeed"
      notification_body="Updates for ${app_name} passed visual regression testing... report only run, compiling report..."
    fi
    if [[ ${SINGLE_SITE} ]]; then
      gridpane::notify::app \
        "UpdateSafely Notice" \
        "${notification_body}" \
        "popup_and_center" \
        "fa-shield" \
        "long"
    fi
    updatesafely::setup::copyreport "${app_name}"
  elif [[ "${backstop_test}" == *"report | 0 Failed"* ]]; then
    if [[ ${SINGLE_SITE} ]]; then
      gridpane::notify::app \
        "UpdateSafely Notice" \
        "Updates for ${app_name} passed visual regression testing... progressing to update site..." \
        "popup_and_center" \
        "fa-shield" \
        "long"
    fi
    updatesafely::setup::copyreport "${app_name}"
    updatesafely::setup::updateprimary "${app_name}" "${siteowner}" "${update_status}"
    success_state="succeed"
  else
    #Not sure what this is for... if error on loop, need to keep going through... I think lol
    exit 187
  fi

  gridpane::callback::app \
    "callback" \
    "/auto-update/check-status" \
    "domain=canary.${app_name}" \
    "server_ip=${serverIP}" \
    "status=${success_state}" \
    "link=http://${grid_subdom}/updatesafely/${app_name}"

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#######################################
updatesafely::setup::copyreport() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"

  REPORT_URLS+=(${app_name})

  rsync -avzh /opt/gridpane/${app_name}-backstop_data/ /var/www/${grid_subdom}/updatesafely

  if [[ ! -f /opt/gridpane/gridpane_logo.png ]]; then
    wget https://gridpane.com/gridpane_logo.png
    mv gridpane_logo.png /opt/gridpane/gridpane_logo.png
  fi

  [[ -f /opt/gridpane/gridpane_logo.png ]] &&
    cp /opt/gridpane/gridpane_logo.png /var/www/${grid_subdom}/updatesafely/${app_name} &&
    sed -i "/# sourceURL=webpack:\/\/\/.\/compare\/src\/assets\/images\/logo.png?/c\eval(\"module.exports = __webpack_require__.p + \\\\\"gridpane_logo.png\\\\\";\\\n\\\n\/\/# sourceURL=webpack:\/\/\/.\/compare\/src\/assets\/images\/logo.png?\");" \
      /var/www/${grid_subdom}/updatesafely/${app_name}/index_bundle.js

  nginx -t && silent_reload=$(/usr/local/bin/gp ngx reload)

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#  Original:
#   UpdatePrimary
#######################################
updatesafely::setup::updateprimary() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"
  local siteowner="$2"
  local update_status="$3"

  echo "Visual Testing Passed! Now updating live site ${app_name}..."
  cd /var/www/${app_name}/htdocs

  echo "Running manual Borg backup..."
  /usr/local/bin/gpbup ${app_name} local-manual

  echo "Site backed up, proceeding with updates..."

  if [[ ${update_status} -eq 40 || ${update_status} -eq 80 || ${update_status} -eq 90 ]]; then
    echo "Updating all Themes..."
    sudo su - ${siteowner} -c "timeout 900 /usr/local/bin/wp theme update --all --path=/var/www/${app_name}/htdocs --url=${app_name}"
  fi

  if [[ ${update_status} -eq 10 || ${update_status} -eq 40 || ${update_status} -eq 90 ]]; then
    echo "Updating all Plugins..."
    sudo su - ${siteowner} -c "timeout 900 /usr/local/bin/wp plugin update --all --path=/var/www/${app_name}/htdocs --url=${app_name}"
  fi

  if [[ ${update_status} -eq 50 || ${update_status} -eq 60 || ${update_status} -eq 90 ]]; then
    echo "Updating WP Core..."
    sudo su - ${siteowner} -c "timeout 900 /usr/local/bin/wp core update --path=/var/www/${app_name}/htdocs --url=${app_name}"
  fi

  echo "Live site ${app_name} fully updated!"
}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#  Original:
#   CleanupCanary
#######################################
updatesafely::clean::cleanupcanary() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"
  local siteowner="$2"

  echo "Removing duplicate site content for ${app_name} from canary.${app_name} site folders..."
  cd /var/www/canary.${app_name}/htdocs
  rm -R /var/www/canary.${app_name}/htdocs/wp-content/*
  sudo su - ${siteowner} -c "timeout 1200 /usr/local/bin/wp core download --path=/var/www/canary.${app_name}/htdocs --force"
  sudo su - ${siteowner} -c "timeout 1200 /usr/local/bin/wp db reset --path=/var/www/canary.${app_name}/htdocs --yes"
  if [[ -d /etc/nginx/ssl/canary.${app_name}/ ]]; then
    rm /etc/nginx/ssl/canary.${app_name}/*
  fi
  /usr/local/bin/gp conf nginx generate http redis canary.${app_name}

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#  Original:
#   EndHostsRedirect
#######################################
updatesafely::clean::localcanary() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"

  echo "Removing local hosts file redirect for site ${app_name} and canary.${app_name}..."
  sed -i '/^# Begin UpdateSafely/,/# End UpdateSafely/d' /etc/hosts

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#######################################
updatesafely::process() {

  ((DEBUG)) && gridpane::displayfunc

  local app_name="$1"
  local isupdatable_response
  local isupdatable_return

  isupdatable_response=$(updatesafely::isupdateable ${app_name})
  isupdatable_return=$?

  if $(exit ${isupdatable_return}); then

    echo "${isupdatable_response}" | tee -a "/var/www/${app_name}/logs/updates.log" 2>&1

    if updatesafely::check::storage "${app_name}"; then
      local siteowner
      siteowner=$(gridpane::get::set::site::user "${app_name}") || true
      local update_status
      update_status=$(updatesafely::arethereupdates "${app_name}" "${siteowner}")

      if [[ ${update_status} -gt 0 ]]; then
        updatesafely::run "${app_name}" "${siteowner}" "${update_status}"
      else
        if [[ ${SINGLE_SITE} ]]; then
          gridpane::notify::app \
            "UpdateSafely Notice" \
            "No available updates detected!" \
            "popup_and_center" \
            "fa-shield" \
            "long"
        fi
      fi
    fi
  else
    echo "${isupdatable_response}"
    if [[ ${SINGLE_SITE} ]]; then
      gridpane::notify::app \
        "UpdateSafely Notice" \
        "Updatesafely Issue: ${isupdatable_response}" \
        "popup_and_center" \
        "fa-exclamation" \
        "long"
    fi
  fi

}

#######################################
#
#
#  Outputs:
#   Writes status to STDOUT
#######################################
updatesafely::showandlogoutput() {

  local log_file="$1"
  shift
  local input

  while read -r input; do
    local time="$(date +'%H:%M:%S.%3N %F')"
    printf "[%s] %s\n" "${time}" "${input}" >>"${log_file}"
    printf "\e[38;5;%sm%s\e[0m\n" 129 "${input}"
  done

}

updatesafely::showandlogoutputindented() {

  local log_file="$1"
  shift
  local input

  while read -r input; do
    local time="$(date +'%H:%M:%S.%3N %F')"
    printf "[%s] %s\n" "${time}" "${input}" >>"${log_file}"
    printf " * \e[38;5;%sm%s\e[0m\n" 127 "${input}"
  done
}

updatesafely::showandlogoutputindentednocolor() {

  local log_file="$1"
  shift
  local input

  while read -r input; do
    local time="$(date +'%H:%M:%S.%3N %F')"
    printf "[%s] %s\n" "${time}" "${input}" >>"${log_file}"
    if ((DEBUG)); then
      printf "   \e[38;5;%sm%s\e[0m\n" 245 "${input}"
    fi
  done
}

updatesafely::showoutput() {

  local input

  while read -r input; do
    local time="$(date +'%H:%M:%S.%3N %F')"
    printf "\e[38;5;%sm%s\e[0m\n" 129 "${input}"
  done

}

updatesafely::check_for_canary() {
  local app_name="$1"
  local log_file="$2"
  if [[ ! -d /var/www/canary.${app_name}/htdocs ]]; then
    echo "No canary.${app_name}, unable to run updatesafely for ${app_name}" | updatesafely::showandlogoutput "${log_file}"
    echo "-----------------------------------------------------------" | updatesafely::showandlogoutput "${log_file}"
    return 1
  else
    return 0
  fi
}

updatesafely::enable() {
  local true_or_false="$1"
  local return_code=0
  local payload_return
  local cpu_count
  local system_memory_kb
  local resource_issue

  cpu_count=$(nproc --all)
  [[ $cpu_count -lt 4 ]] && resource_issue="true"
  system_memory_kb=$(grep MemTotal /proc/meminfo | awk '{print $2}')
  [[ $system_memory_kb -lt 4000000 ]] && resource_issue="true"

  if [[ ${true_or_false} == "true" ]]; then
    [[ -z ${resource_issue} ||
      -f /opt/gridpane/allow.update.safely ]] && touch /root/updatesafely.beta
  elif [[ ${true_or_false} == "false" ]]; then
    [[ -f /root/updatesafely.beta ]] && rm /root/updatesafely.beta
  fi

  local return_code=0
  local update_notification_insert
  if [[ -f /root/updatesafely.beta ]]; then
    payload_return="true"
    if [[ ${true_or_false} == "false" ]]; then
      return_code=1
      update_notification_insert="Failed to disable Update Safely"
    else
      if [[ ! -f /opt/gridpane/allow.update.safely ]]; then
        update_notification_insert="Update Safely initial state has been enabled... adding dependencies in background..."
      else
        update_notification_insert="Update Safely initial state has been force enabled for testing on low resources... adding dependencies in background..."
      fi
    fi
  else
    payload_return="false"
    if [[ ${true_or_false} == "true" ]]; then
      return_code=1
      update_notification_insert="Failed to enable Update Safely"
      [[ ${resource_issue} == "true" ]] &&
        update_notification_insert="$update_notification_insert (Server does not match the Minimum requirements of at least 4 CPU Cores and 4GB Memory)"
    else
      update_notification_insert="Update Safely has been disabled"
    fi
  fi

  updatesafely::install

  local notification_body
  # shellcheck disable=SC2154
  notification_body="${update_notification_insert} on ${host} ${server_IP}"
  echo "${notification_body}"

  gridpane::conf_write updatesafely-enabled "${payload_return}"

  # Use callback arg to output json for debug
  gridpane::callback::app \
    "callback" \
    "/server/server-update" \
    "--" \
    "-s" "server_ip=${serverIP}" \
    "us_enabled@${payload_return}"

  gridpane::notify::app \
    "UpdateSafely Notice" \
    "${notification_body}" \
    "popup_and_center" \
    "fa-shield" \
    "long"

  if [[ ${notification_body} == *"adding dependencies in background"* ]]; then
    updatesafely::install
  fi

  return ${return_code}
}

updatesafely::update_frequency() {
  local day_frequency="$1"
  local return_code=0
  local cron_pattern
  local min
  local hour
  local notfication_insert

  min=$((RANDOM % 58 + 1))
  hour=$((RANDOM % 3 + 1))
  if [[ ${day_frequency} == "1" ||
        ${day_frequency} == "2" ||
        ${day_frequency} == "3" ]]; then
    cron_pattern="${min} ${hour} */${day_frequency} * *"
    [[ ${day_frequency} == "1" ]] && notification_insert="Daily"
    [[ ${day_frequency} == "2" ]] && notification_insert="Every 48 Hours"
    [[ ${day_frequency} == "3" ]] && notification_insert="Every 72 Hours"
  elif [[ ${day_frequency} == "7" ]]; then
    cron_pattern="${min} ${hour} * * ${day_frequency}"
    notification_insert="Weekly (Sundays)"
  else
    echo "unrecognized frequency... defaulting to daily..."
    cron_pattern="${min} ${hour} */1 * *"
    notification_insert="Daily (fallback)"
  fi

  echo "Adjusting UpdateSafely Cron"

  crontab -l | grep -v "/usr/local/bin/updatesafely" | crontab -
  crontab -l >/tmp/tempcron
  echo "${cron_pattern} /usr/local/bin/updatesafely" | tee -a /tmp/tempcron
  crontab /tmp/tempcron
  rm /tmp/tempcron

  # TODO (JEFF) Add a check of the crontab to ensure it was updated correctly

  gridpane::conf_write updatesafely-frequency "${day_frequency}"

  # Use callback arg to output json for debug
  gridpane::callback::app \
    "callback" \
    "/server/server-update" \
    "--" \
    "-s" "server_ip=${serverIP}" \
    "us_frequency=${day_frequency}"

  gridpane::notify::app \
    "UpdateSafely Notice" \
    "UpdateSafely Frequency set to:<br>${notification_insert}<br>Cron Pattern:<br>${cron_pattern} /usr/local/bin/updatesafely" \
    "popup_and_center" \
    "fa-shield" \
    "long"
}

#######################################
####### Backwards compatability #######
#######################################
SafelyAllSites() {
  updatesafely::run $*
}

GoodTime() {
  updatesafely::goodtime $*
}

StorageCheck() {
  updatesafely::check::storage $*
}

LocalHostsRedirect() {
  updatesafely::setup::localcanary $*
}

GetPages() {
  updatesafely::setup::getpages $*
}

BuildConfigs() {
  updatesafely::setup::buildconfig $*
}

updateSiteUrls() {
  updatesafely::setup::siteurls $*
}

DupeLive() {
  updatesafely::setup::dupelive $*
}

UpdateCanary() {
  updatesafely::setup::updatecanary $*
}

CompareSitesAndUpdatePrimary() {
  updatesafely::setup::comparesitesandupdateprimary $*
}

UpdatePrimary() {
  updatesafely::setup::updateprimary $*
}

CleanupCanary() {
  updatesafely::clean::cleanupcanary $*
}

EndHostsRedirect() {
  updatesafely::clean::localcanary $*
}
