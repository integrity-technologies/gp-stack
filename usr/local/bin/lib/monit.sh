#!/bin/bash
#

monit::set::default::resource_variables() {
  procNo=$(nproc --all)
  fourty_percent_of_proc=$((procNo * 40))
  fifty_percent_of_proc=$((procNo * 50))
  sixty_percent_of_proc=$((procNo * 60))
  seventy_percent_of_proc=$((procNo * 70))
  eighty_percent_of_proc=$((procNo * 80))
  ninety_percent_of_proc=$((procNo * 90))
  ram_KB=$(grep MemTotal /proc/meminfo | awk '{print $2}')
  ram_MB=$(echo "scale=0; ${ram_KB} / 1000" | bc)
  ten_percent_ram_MB=$(echo "scale=0; ${ram_KB} / 10000" | bc)
  twenty_percent_ram_MB=$(echo "scale=0; ${ram_KB} / 5000" | bc)
  thirty_percent_ram_MB=$(echo "scale=0; ${ram_KB} / 3000" | bc)
  fourty_percent_ram_MB=$(echo "scale=0; ${ram_KB} / 2500" | bc)
  fifty_percent_ram_MB=$(echo "scale=0; ${ram_KB} / 2000" | bc)
  sixty_percent_ram_MB=$((thirty_percent_ram_MB + thirty_percent_ram_MB))
  seventy_percent_ram_MB=$((fifty_percent_ram_MB + twenty_percent_ram_MB))
  eighty_percent_ram_MB=$((fifty_percent_ram_MB + thirty_percent_ram_MB))
  ninety_percent_ram_MB=$((fifty_percent_ram_MB + fourty_percent_ram_MB))
  if [[ ${ram_MB} -lt 1500 ]]; then
    mysqlRAM="500"
  fi
  if [[ ${ram_MB} -ge 1500 ]]; then
    mysqlRAM="$(echo "scale=0; ${seventy_percent_ram_MB} / 2" | bc)"
  fi
  if [[ ${ram_MB} -ge 8000 ]]; then
    mysqlRAM="$(echo "scale=0; ${ninety_percent_ram_MB} / 2" | bc)"
  fi
  if [[ ${ram_MB} -ge 16000 ]]; then
    mysqlRAM="${fifty_percent_ram_MB}"
  fi

  checkMaxMemory=$(/bin/grep -c "maxmemory .*mb" /etc/redis/redis.conf)
  if [[ $checkMaxMemory != 1 ]]; then
    if [[ $checkMaxMemory -gt 1 ]]; then
      redisRAM=$(/bin/grep -m 1 "maxmemory .*mb" /etc/redis/redis.conf | xargs)
      sed -i "/$redisRAM/d" /etc/redis/redis.conf
      redisRAM=${redisRAM#maxmemory }
      redisRAM=${redisRAM%mb}
    else
      if [[ ${ram_MB} -lt 2000 ]]; then
        if [[ ${ram_MB} -lt 1100 ]]; then
          redisRAM="150"
        else
          redisRAM="250"
        fi
      fi
      if [[ ${ram_MB} -ge 2000 ]]; then
        redisRAM="$(echo "scale=0; ${thirty_percent_ram_MB} / 2" | bc)"
      fi
      if [[ ${ram_MB} -ge 4000 ]]; then
        redisRAM="${twenty_percent_ram_MB}"
      fi
    fi
    echo "maxmemory ${redisRAM}mb" >>/etc/redis/redis.conf
  else
    redisRAM=$(/bin/grep "maxmemory .*mb" /etc/redis/redis.conf)
    redisRAM=${redisRAM#maxmemory }
    redisRAM=${redisRAM%mb}
  fi
  redisRAM=$((redisRAM + 100))
  redisRAMRestart=$((redisRAM + 300))
}

monit::set::missing_to_default() {
  if [[ -z $cpu_warning_percent && -f /root/gridenv/monit.env ]]; then
    cpu_warning_percent=$(grep -w "$service-cpu-warning-threshold-percent:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
  fi
  if [[ -z $cpu_warning_percent || ! $cpu_warning_percent =~ ^[0-9]+$ ]]; then
    # using case to make it easy to update defaults if needed
    case $service in
    mysql) cpu_warning_percent="$seventy_percent_of_proc" ;;
    nginx) cpu_warning_percent="$fifty_percent_of_proc" ;;
    openlitespeed) cpu_warning_percent="$fifty_percent_of_proc" ;;
    php7*) cpu_warning_percent="$sixty_percent_of_proc" ;;
    redis) cpu_warning_percent="$sixty_percent_of_proc" ;;
    esac
    cpu_warning_percent_default=" (default)"
  fi

  if [[ -z $cpu_warning_cycles && -f /root/gridenv/monit.env ]]; then
    cpu_warning_cycles=$(grep -w "$service-cpu-warning-cycles:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
  fi
  if [[ -z $cpu_warning_cycles || ! $cpu_warning_cycles =~ ^[0-9]+$ ]]; then
    # using case to make it easy to update defaults if needed
    case $service in
    mysql) cpu_warning_cycles="10" ;;
    nginx) cpu_warning_cycles="5" ;;
    openlitespeed) cpu_warning_cycles="5" ;;
    php7*) cpu_warning_cycles="10" ;;
    redis) cpu_warning_cycles="10" ;;
    esac
    cpu_warning_cycles_default=" (default)"
  fi

  if [[ -z $cpu_restart_percent && -f /root/gridenv/monit.env ]]; then
    cpu_restart_percent=$(grep -w "$service-cpu-restart-threshold-percent:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
  fi
  if [[ -z $cpu_restart_percent || ! $cpu_restart_percent =~ ^[0-9]+$ ]]; then
    # using case to make it easy to update defaults if needed
    case $service in
    mysql) cpu_restart_percent="$ninety_percent_of_proc" ;;
    nginx) cpu_restart_percent="$sixty_percent_of_proc" ;;
    openlitespeed) cpu_restart_percent="$sixty_percent_of_proc" ;;
    php7*) cpu_restart_percent="$ninety_percent_of_proc" ;;
    redis) cpu_restart_percent="$eighty_percent_of_proc" ;;
    esac
    cpu_restart_percent_default=" (default)"
  fi

  if [[ -z $cpu_restart_cycles && -f /root/gridenv/monit.env ]]; then
    cpu_restart_cycles=$(grep -w "$service-cpu-restart-cycles:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
  fi
  if [[ -z $cpu_restart_cycles || ! $cpu_restart_cycles =~ ^[0-9]+$ ]]; then
    # using case to make it easy to update defaults if needed
    case $service in
    mysql) cpu_restart_cycles="15" ;;
    nginx) cpu_restart_cycles="10" ;;
    openlitespeed) cpu_restart_cycles="10" ;;
    php7*) cpu_restart_cycles="5" ;;
    redis) cpu_restart_cycles="5" ;;
    esac
    cpu_restart_cycles_default=" (default)"
  fi

  if [[ -z $mem_high_mb && -f /root/gridenv/monit.env ]]; then
    mem_high_mb=$(grep -w "$service-mem-warning-threshold-mb:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
  fi
  if [[ -z $mem_high_mb || ! $mem_high_mb =~ ^[0-9]+$ ]]; then
    # using case to make it easy to update defaults if needed
    case $service in
    mysql) mem_high_mb="${mysqlRAM}" ;;
    nginx) mem_high_mb="${fourty_percent_ram_MB}" ;;
    openlitespeed) mem_high_mb="${fourty_percent_ram_MB}" ;;
    php7*) mem_high_mb="${sixty_percent_ram_MB}" ;;
    redis) mem_high_mb="${redisRAM}" ;;
    esac
    mem_high_mb_default=" (default)"
  fi

  if [[ -z $mem_high_cycles && -f /root/gridenv/monit.env ]]; then
    mem_high_cycles=$(grep -w "$service-mem-warning-cycles:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
  fi
  if [[ -z $mem_high_cycles || ! $mem_high_cycles =~ ^[0-9]+$ ]]; then
    # using case to make it easy to update defaults if needed
    case $service in
    mysql) mem_high_cycles="10" ;;
    nginx) mem_high_cycles="5" ;;
    openlitespeed) mem_high_cycles="5" ;;
    php7*) mem_high_cycles="10" ;;
    redis) mem_high_cycles="5" ;;
    esac
    mem_high_cycles_default=" (default)"
  fi

  if [[ -z $mem_restart_mb && -f /root/gridenv/monit.env ]]; then
    mem_restart_mb=$(grep -w "$service-mem-restart-threshold-mb:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
  fi
  if [[ -z $mem_restart_mb || ! $mem_restart_mb =~ ^[0-9]+$ ]]; then
    # using case to make it easy to update defaults if needed
    case $service in
    mysql) mem_restart_mb="${mysqlRAM}" ;;
    nginx) mem_restart_mb="${seventy_percent_ram_MB}" ;;
    openlitespeed) mem_restart_mb="${seventy_percent_ram_MB}" ;;
    php7*) mem_restart_mb="${eighty_percent_ram_MB}" ;;
    redis) mem_restart_mb="${redisRAMRestart}" ;;
    esac
    mem_restart_mb_default=" (default)"
  fi

  if [[ -z $mem_restart_cycles && -f /root/gridenv/monit.env ]]; then
    mem_restart_cycles=$(grep -w "$service-mem-restart-cycles:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
  fi
  if [[ -z $mem_restart_cycles || ! $mem_restart_cycles =~ ^[0-9]+$ ]]; then
    # using case to make it easy to update defaults if needed
    case $service in
    mysql) mem_restart_cycles="20" ;;
    nginx) mem_restart_cycles="10" ;;
    openlitespeed) mem_restart_cycles="10" ;;
    php7*) mem_restart_cycles="10" ;;
    redis) mem_restart_cycles="10" ;;
    esac
    mem_restart_cycles_default=" (default)"
  fi

  if [[ -n $debug ]]; then
    echo "-------------------------------------"
    echo "Passed Args:"
    echo "-------------------------------------"
    echo "service: $service"
    echo "cpu warning percent: $cpu_warning_percent $cpu_warning_percent_default"
    echo "cpu warning cycles: $cpu_warning_cycles $cpu_warning_cycles_default"
    echo "cpu restart percent: $cpu_restart_percent $cpu_restart_percent_default"
    echo "cpu restart cycles: $cpu_restart_cycles $cpu_restart_cycles_default"
    echo "mem high mb: $mem_high_mb $mem_high_mb_default"
    echo "mem high cycles: $mem_high_cycles $mem_high_cycles_default"
    echo "mem restart mb: $mem_restart_mb $mem_restart_mb_default"
    echo "mem restart cycles: $mem_restart_cycles $mem_restart_cycles_default"
  fi
}

monit::process_args() {
  case $1 in
  mysql|nginx|openlitespeed|php71|php72|php73|php74|redis)
    service="$1"
    ;;
  fail2ban|filesystem|system|programs)
    echo "Unconfigurable service: $1"
    echo "Exiting..."
    exit 187
    ;;
  *history)
    if [[ -n $2 ]]; then
      case $2 in
      fail2ban|filesystem|mysql|nginx|redis|openlitespeed)
        journalctl | grep -i "GridPane Monitoring - $2"
        ;;
      sys_load_avg*|sys_mem_usage|sys_cpu_usage|sys_swap_mem|system)
        case $2 in
        system) journalctl | grep -i "GridPane Monitoring - sys_" ;;
        *) journalctl | grep -i "GridPane Monitoring - $2" ;;
        esac
        ;;
      php7.1|php7.2|php7.3|php7.4|php)
        case $2 in
        php) journalctl | grep -i "GridPane Monitoring - php" ;;
        *) journalctl | grep -i "GridPane Monitoring - $2" ;;
        esac
        ;;
      *)
        echo "Unknown service: $2"
        echo "Exiting..."
        exit 187
        ;;
      esac
    else
      journalctl | grep "GridPane Monitoring"
    fi
    exit 0
    ;;
  *)
    echo "Unknown service: $1"
    echo "Exiting..."
    exit 187
    ;;
  esac

  shift 1

  if [[ $# -gt 0 ]]; then
    while true; do
      local var="$1"
      case "${var}" in
      --no-reload) no_reload=1 ;;
      --debug) debug=1 ;;
      -cpu-warning-percent)
        shift 1
        if [[ ! $1 =~ ^[0-9]+$  ]]; then
          echo "cpu warning percent needs to be an integer - $1 passed..."
        else
          cpu_warning_percent="$1"
        fi
        ;;
      -cpu-warning-cycles)
        shift 1
        if [[ ! $1 =~ ^[0-9]+$  ]]; then
          echo "cpu warning cycles needs to be an integer - $1 passed..."
        else
          cpu_warning_cycles="$1"
        fi
        ;;
      -cpu-restart-percent)
        shift 1
        if [[ ! $1 =~ ^[0-9]+$  ]]; then
          echo "cpu restart percent needs to be an integer - $1 passed..."
        else
          cpu_restart_percent="$1"
        fi
        ;;
      -cpu-restart-cycles)
        shift 1
        if [[ ! $1 =~ ^[0-9]+$  ]]; then
          echo "cpu restart percent needs to be an integer - $1 passed..."
        else
          cpu_restart_cycles="$1"
        fi
        ;;
      -mem-high-mb)
        shift 1
        if [[ ! $1 =~ ^[0-9]+$  ]]; then
          echo "mem high mb needs to be an integer - $1 passed..."
        else
          mem_high_mb="$1"
        fi
        ;;
      -mem-high-cycles)
        shift 1
        if [[ ! $1 =~ ^[0-9]+$  ]]; then
          echo "mem high cycles needs to be an integer - $1 passed..."
        else
          mem_high_cycles="$1"
        fi
        ;;
      -mem-restart-mb)
        shift 1
        if [[ ! $1 =~ ^[0-9]+$  ]]; then
          echo "mem restart mb needs to be an integer - $1 passed..."
        else
          mem_restart_mb="$1"
        fi
        ;;
      -mem-restart-cycles)
        shift 1
        if [[ ! $1 =~ ^[0-9]+$  ]]; then
          echo "mem restart cycles needs to be an integer - $1 passed..."
        else
          mem_restart_cycles="$1"
        fi
        ;;
      esac

      shift 1 || true
      [[ -z "${var}" ]] &&
        break
    done
  fi

  monit::set::missing_to_default
}

monit::check_stored_configs() {
  local service="$1"
  if [[ $service == "mysql" ]]; then
    if ! grep "# gridpane monit mysql v2" "/opt/gridpane/monit-settings/mysql" >/dev/null; then
      cat >/opt/gridpane/monit-settings/mysql <<EOF
# gridpane monit mysql v2
check process mysql with pidfile /var/run/mysqld/mysqld.pid
    group database
    group mysql
    start program = "/usr/sbin/service mysql start"
    stop  program = "/usr/sbin/service mysql stop"
    if cpu > XXXCPUHIGHPERCENTXXX% for XXXCPUHIGHCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor MYSQL CPU_HOT warning" AND repeat every XXXCPUHIGHCYCLESXXX cycles
    if cpu > XXXCPURESTARTPERCENTXXX% for XXXCPURESTARTCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor MYSQL CPU_RESTART error"
    if mem > XXXMEMHIGHRAMXXX MB for XXXMEMHIGHCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor MYSQL MEM_HIGH warning" AND repeat every XXXMEMHIGHCYCLESXXX cycles
    if mem > XXXMEMRESTARTRAMXXX MB for XXXMEMRESTARTCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor MYSQL MEM_RESTART error"
    if failed host localhost port 3306 protocol mysql with timeout 15 seconds for 3 times within 4 cycles then restart
    if failed unixsocket /var/run/mysqld/mysqld.sock protocol mysql for 3 times within 4 cycles then restart
    if 3 restarts within 5 cycles then exec "/usr/local/bin/gpmonitor MYSQL FAILED error" AND repeat every 1 cycles
EOF
    fi
  elif [[ $service == "nginx" ]]; then
    if ! grep "# gridpane monit nginx v2" "/opt/gridpane/monit-settings/nginx" >/dev/null; then
      cat >/opt/gridpane/monit-settings/nginx <<EOF
# gridpane monit nginx v2
check process nginx with pidfile /var/run/nginx.pid
    group nginx
    start program = "/usr/local/bin/gp ngx start"
    stop program = "/usr/local/bin/gp ngx stop"
    if failed host 127.0.0.1 port 80 then restart
    if cpu > XXXCPUHIGHPERCENTXXX% for XXXCPUHIGHCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor NGINX CPU_HOT warning" AND repeat every XXXCPUHIGHCYCLESXXX cycles
    if cpu > XXXCPURESTARTPERCENTXXX% for XXXCPURESTARTCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor NGINX CPU_RESTART error"
    if mem > XXXMEMHIGHRAMXXX MB for XXXMEMHIGHCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor NGINX MEM_HIGH warning" AND repeat every XXXMEMHIGHCYCLESXXX cycles
    if mem > XXXMEMRESTARTRAMXXX MB for XXXMEMRESTARTCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor NGINX MEM_RESTART error"
    if 3 restarts within 5 cycles then exec "/usr/local/bin/gpmonitor NGINX FAILED error" AND repeat every 1 cycles
EOF
    fi
  elif [[ $service == "openlitespeed" ]]; then
    if ! grep "# gridpane monit openlitespeed v2" "/opt/gridpane/monit-settings/openlitespeed" >/dev/null; then
      cat >/opt/gridpane/monit-settings/openlitespeed <<EOF
# gridpane monit openlitespeed v2
check process openlitespeed with pidfile /tmp/lshttpd/lshttpd.pid
    group root
    start program = "/bin/systemctl start lshttpd"
    stop program = "/bin/systemctl stop lshttpd"
    if failed host 127.0.0.1 port 80 then restart
    if cpu > XXXCPUHIGHPERCENTXXX% for XXXCPUHIGHCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor OPENLITESPEED CPU_HOT warning" AND repeat every XXXCPUHIGHCYCLESXXX cycles
    if cpu > XXXCPURESTARTPERCENTXXX% for XXXCPURESTARTCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor OPENLITESPEED CPU_RESTART error"
    if mem > XXXMEMHIGHRAMXXX MB for XXXMEMHIGHCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor OPENLITESPEED MEM_HIGH warning" AND repeat every XXXMEMHIGHCYCLESXXX cycles
    if mem > XXXMEMRESTARTRAMXXX MB for XXXMEMRESTARTCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor OPENLITESPEED MEM_RESTART error"
    if 3 restarts within 5 cycles then exec "/usr/local/bin/gpmonitor OPENLITESPEED FAILED error" AND repeat every 1 cycles
EOF
    fi
  elif [[ $service == "redis" ]]; then
    if ! grep "# gridpane monit redis v2" "/opt/gridpane/monit-settings/redis" >/dev/null; then
      cat >/opt/gridpane/monit-settings/redis <<EOF
# gridpane monit redis v2
check process redis-server
    with pidfile "/var/run/redis/redis-server.pid"
    start program = "/usr/sbin/service redis-server start"
    stop program = "/usr/sbin/service redis-server stop"
    if cpu usage > XXXCPUHIGHPERCENTXXX% for XXXCPUHIGHCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor REDIS CPU_HOT warning" AND repeat every XXXCPUHIGHCYCLESXXX cycles
    if cpu usage > XXXCPURESTARTPERCENTXXX% for XXXCPURESTARTCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor REDIS CPU_RESTART error"
    if totalmem > XXXMEMHIGHRAMXXX MB for XXXMEMHIGHCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor REDIS MEM_HIGH warning" AND repeat every XXXMEMHIGHCYCLESXXX cycles
    if totalmem > XXXMEMRESTARTRAMXXX MB for XXXMEMRESTARTCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor REDIS MEM_RESTART error"
    if failed host 127.0.0.1 port 6379 then restart
    if children > 255 for 5 cycles then restart
    if 5 restarts within 5 cycles then exec "/usr/local/bin/gpmonitor REDIS FAILED error" AND repeat every 1 cycles
EOF
    fi
  elif [[ $service == "php7"* ]]; then
    case $service in
    php71) dot_version="7.1" ;;
    php72) dot_version="7.2" ;;
    php73) dot_version="7.3" ;;
    php74) dot_version="7.4" ;;
    esac
    if ! grep "# gridpane monit $service v2" "/opt/gridpane/monit-settings/$service" >/dev/null; then
      cat >"/opt/gridpane/monit-settings/$service" <<EOF
# gridpane monit $service v2
check process $service-fpm with pidfile /var/run/php/php$dot_version-fpm.pid
    start program = "/usr/sbin/service php$dot_version-fpm start"
    stop program  = "/usr/sbin/service php$dot_version-fpm stop"
    if cpu > XXXCPUHIGHPERCENTXXX% for XXXCPUHIGHCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor PHP$dot_version CPU_HOT warning" AND repeat every XXXCPUHIGHCYCLESXXX cycles
    if cpu > XXXCPURESTARTPERCENTXXX% for XXXCPURESTARTCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor PHP$dot_version CPU_RELOAD error"
    if mem > XXXMEMHIGHRAMXXX MB for XXXMEMHIGHCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor PHP$dot_version MEM_HIGH warning" AND repeat every XXXMEMHIGHCYCLESXXX cycles
    if mem > XXXMEMRESTARTRAMXXX MB for XXXMEMRESTARTCYCLESXXX cycles then exec "/usr/local/bin/gpmonitor PHP$dot_version MEM_RELOAD error"
    if 3 restarts within 5 cycles then exec "/usr/local/bin/gpmonitor PHP$dot_version FAILED error" AND repeat every 1 cycles
EOF
    fi
  fi
}

monit::configure_service() {
  local service="$1"

  [[ ! -f /opt/gridpane/monit-settings/$service ]] &&
    monit::check_stored_configs "${service}"

  cp "/opt/gridpane/monit-settings/$service" "/opt/gridpane/monit-temp-$service"

  if [[ -f "/opt/gridpane/monit-temp-$service" ]]; then
    sed -i "s|XXXCPUHIGHPERCENTXXX|${cpu_warning_percent}|g" "/opt/gridpane/monit-temp-$service"
    sed -i "s|XXXCPUHIGHCYCLESXXX|${cpu_warning_cycles}|g" "/opt/gridpane/monit-temp-$service"
    sed -i "s|XXXCPURESTARTPERCENTXXX|${cpu_restart_percent}|g" "/opt/gridpane/monit-temp-$service"
    sed -i "s|XXXCPURESTARTCYCLESXXX|${cpu_restart_cycles}|g" "/opt/gridpane/monit-temp-$service"
    sed -i "s|XXXMEMHIGHRAMXXX|${mem_high_mb}|g" "/opt/gridpane/monit-temp-$service"
    sed -i "s|XXXMEMHIGHCYCLESXXX|${mem_high_cycles}|g" "/opt/gridpane/monit-temp-$service"
    sed -i "s|XXXMEMRESTARTRAMXXX|${mem_restart_mb}|g" "/opt/gridpane/monit-temp-$service"
    sed -i "s|XXXMEMRESTARTCYCLESXXX|${mem_restart_cycles}|g" "/opt/gridpane/monit-temp-$service"
  fi

  if ! cp "/opt/gridpane/monit-temp-$service" "/etc/monit/conf.d/$service"; then
    echo "Unable to rewrite /etc/monit/conf.d/$service with /opt/gridpane/monit-temp-$service"
    exit 187
  fi
  rm "/opt/gridpane/monit-temp-$service"

  gridpane::conf_write  "$service-cpu-warning-threshold-percent" "${cpu_warning_percent}" -monit.env
  gridpane::conf_write  "$service-cpu-warning-cycles" "${cpu_warning_cycles}" -monit.env
  gridpane::conf_write  "$service-cpu-restart-threshold-percent" "${cpu_restart_percent}" -monit.env
  gridpane::conf_write  "$service-cpu-restart-cycles" "${cpu_restart_cycles}" -monit.env
  gridpane::conf_write  "$service-mem-warning-threshold-mb" "${mem_high_mb}" -monit.env
  gridpane::conf_write  "$service-mem-warning-cycles" "${mem_high_cycles}" -monit.env
  gridpane::conf_write  "$service-mem-restart-threshold-mb" "${mem_restart_mb}" -monit.env
  gridpane::conf_write  "$service-mem-restart-cycles" "${mem_restart_cycles}" -monit.env

  echo "-------------------------------------"
  echo "/root/gridenv/monit.env:"
  echo "-------------------------------------"
  grep "$service-" "/root/gridenv/monit.env"
  echo "-------------------------------------"
  echo "/etc/monit/conf.d/$service:"
  echo "-------------------------------------"
  cat "/etc/monit/conf.d/$service"
  echo "-------------------------------------"
}

