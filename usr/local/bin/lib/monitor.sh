#!/bin/bash
#
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
gpmonitor::process() {
  local service="$1"
  local aspect="$2"
  local slack_type="$3"
  local title
  local details
  local notification_icon
  local notification_duration
  local preemptive_support
  preemptive_support="false"

  case "${service}" in
  RUN_RAMDISK)
    case "${aspect}" in
    97)
      title="${service} ${aspect}% Capacity Warning"
      # shellcheck disable=SC2154
      details="${host} ${serverIP} System /run Ramdisk has reached ${aspect}% capacity!"
      ;;
    98)
      # shellcheck disable=SC2154
      if [[ $webserver == "nginx" ]]; then
        title="${service} ${aspect}% Capacity Warning - space recovery initiated!"
        # shellcheck disable=SC2154
        details="${host} ${serverIP} System /run Ramdisk has reached ${aspect}% capacity - attempting automated space recovery from caches."
        [[ -d /var/run/nginx-proxy-cache ]] && rm -rf /var/run/nginx-proxy-cache
        [[ -d /var/run/nginx-cache ]] && rm -rf /var/run/nginx-cache
        /usr/sbin/nginx -t && /usr/sbin/service nginx restart
      else
        title="${service} ${aspect}% Capacity Reached - Please take action!"
        # shellcheck disable=SC2154
        details="${host} ${serverIP} System /run Ramdisk has reached ${aspect}% capacity - please check your server and/or reach out to support."
      fi
      ;;
    esac
    notification_icon="fa-exclamation"
    notification_duration="long"
    preemptive_support="true"
    ;;
  FAIL2BAN)
    case "${aspect}" in
    FAILED)
      title="${service} Service Failure"
      # shellcheck disable=SC2154
      details="The ${service} service has failed on ${host} ${serverIP}, and Monit is unable to restart it successfully, please contact support."
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      preemptive_support="true"
      ;;
    LOG)
      title="${service} Error|Warning Logged"
      # shellcheck disable=SC2154
      details="The ${service} service on ${host} ${serverIP} has logged an Error or Warning...."
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    esac
    ;;
  FILESYSTEM)
    case "${aspect}" in
    80)
      title="${service} Capacity Warning > ${aspect}%"
      # shellcheck disable=SC2154
      details="The Filesystem on ${host} ${serverIP} has surpassed ${aspect}% disk capacity. Please consider upgrading your disk space."
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    85)
      title="${service} Capacity Warning > ${aspect}%"
      # shellcheck disable=SC2154
      details="The Filesystem on ${host} ${serverIP} has surpassed ${aspect}% disk capacity. Really, it would be a great idea to scale your disk up now..."
      notification_icon="fa-exclamation"
      notification_duration="long"
      preemptive_support="true"
      ;;
    90)
      title="${service} Capacity Warning > ${aspect}%"
      # shellcheck disable=SC2154
      details="The Filesystem on ${host} ${serverIP} has surpassed ${aspect}% disk capacity. Please consider upgrading your disk space."
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    95)
      title="${service} Capacity Warning > ${aspect}%"
      # shellcheck disable=SC2154
      details="The Filesystem on ${host} ${serverIP} has surpassed ${aspect}% disk capacity. Really, it would be a great idea to scale your disk up now..."
      notification_icon="fa-exclamation"
      notification_duration="long"
      preemptive_support="true"
      ;;
    98)
      title="${service} Capacity Warning > ${aspect}%"
      # shellcheck disable=SC2154
      details="The Filesystem on ${host} ${serverIP} has surpassed ${aspect}% disk capacity. This is bad, things are about to go south... please scale up before it is too late..."
      notification_icon="fa-exclamation"
      notification_duration="long"
      preemptive_support="true"
      ;;
    99)
      title="${service} Capacity Warning > ${aspect}%"
      # shellcheck disable=SC2154
      details="The Filesystem on ${host} ${serverIP} has surpassed ${aspect}% disk capacity. Okay, this is where things are going to cascade fail... so long and thanks for all the fish!!!"
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      preemptive_support="true"
      ;;
    esac
    ;;
  MYSQL|NGINX|REDIS|OPENLITESPEED)
    case "${aspect}" in
    CPU_HOT)
      case $service in
      MYSQL) env_service="mysql" ;;
      NGINX) env_service="nginx" ;;
      REDIS) env_service="redis" ;;
      OPENLITESPEED) env_service="openlitespeed" ;;
      esac
      percent=$(grep -w "$env_service-cpu-warning-threshold-percent:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      cycles=$(grep -w "$env_service-cpu-warning-cycles:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      minutes=$((cycles * 2))
      title="${service} ${aspect} Warning"
      # shellcheck disable=SC2154
      details="${service} on ${host} ${serverIP} has been using ${percent}% of CPU for over ${minutes} minutes..."
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    CPU_RESTART)
      case $service in
      MYSQL)
        env_service="mysql"
        /usr/local/bin/gp mysql restart
        ;;
      NGINX)
        env_service="nginx"
        /usr/sbin/nginx -t && /usr/sbin/service nginx restart
        ;;
      REDIS)
        env_service="redis"
        /usr/sbin/service redis-server restart
        ;;
      OPENLITESPEED)
        env_service="openlitespeed"
        /usr/local/bin/gpols httpd
        ;;
      esac
      percent=$(grep -w "$env_service-cpu-restart-threshold-percent:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      cycles=$(grep -w "$env_service-cpu-restart-cycles:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      minutes=$((cycles * 2))
      title="${service} ${aspect} Error"
      # shellcheck disable=SC2154
      details="${service} on ${host} ${serverIP} was using ${percent}% of CPU for over ${minutes} minutes...  restarting ${service}!"
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      preemptive_support="true"
      ;;
    MEM_HIGH)
      case $service in
      MYSQL) env_service="mysql" ;;
      NGINX) env_service="nginx" ;;
      REDIS) env_service="redis" ;;
      OPENLITESPEED) env_service="openlitespeed" ;;
      esac
      mb=$(grep -w "$env_service-mem-warning-threshold-mb:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      cycles=$(grep -w "$env_service-mem-warning-cycles:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      minutes=$((cycles * 2))
      title="${service} ${aspect} Warning"
      # shellcheck disable=SC2154
      details="${service} on ${host} ${serverIP} has been using at least ${mb}MB RAM for at least the last ${minutes} minutes..."
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    MEM_RESTART)
      case $service in
      MYSQL)
        env_service="mysql"
        /usr/local/bin/gp mysql restart
        ;;
      NGINX)
        env_service="nginx"
        /usr/sbin/nginx -t && /usr/sbin/service nginx restart
        ;;
      REDIS)
        env_service="redis"
        /usr/sbin/service redis-server restart
        ;;
      OPENLITESPEED)
        env_service="openlitespeed"
        /usr/local/bin/gpols httpd
        ;;
      esac
      mb=$(grep -w "$env_service-mem-restart-threshold-mb:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      cycles=$(grep -w "$env_service-mem-restart-cycles:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      minutes=$((cycles * 2))
      title="${service} ${aspect} Error"
      # shellcheck disable=SC2154
      details="${service} on ${host} ${serverIP} has been using at least ${mb}MB RAM for at least the last ${minutes} minutes.... restarting ${service}!"
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      preemptive_support="true"
      ;;
    FAILED)
      title="${service} Service ${aspect} Error"
      # shellcheck disable=SC2154
      details="${service} on ${host} ${serverIP} has failed and Monit is unable to restart it successfully... please contact support."
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      preemptive_support="true"
      ;;
    CONF_SYNTAX)
      case $service in
      MYSQL)
        if [[ "$(mysqld --validate-config 2>&1)" == *"[ERROR]"* ]]; then
          sleep 2
          [[ "$(mysqld --validate-config 2>&1)" != *"[ERROR]"* ]] &&
            callback="false"
        else
          callback="false"
        fi
        # Old MySQL Bug might lead to too many tickets - old percona version failed syntax checks, updating version fixes it
        # Need to run a system update to bring all percona up to date
        callback="false"
        ;;
      NGINX)
        if [[ "$(nginx -t 2>&1)" != *"test is successful"* ]]; then
          sleep 2
          [[ "$(nginx -t 2>&1)" == *"test is successful"* ]] &&
            callback="false"
        else
          callback="false"
        fi
        ;;
      OPENLITESPEED)
        if [[ -n "$(/usr/local/lsws/bin/openlitespeed -t 2>&1)" ]]; then
          sleep 2
          [[ -z "$(/usr/local/lsws/bin/openlitespeed -t 2>&1)" ]] &&
            callback="false"
        else
          callback="false"
        fi
        ;;
      esac
      if [[ -z $callback ]]; then
        title="${service} Service ${aspect} Error"
        # shellcheck disable=SC2154
        details="Your ${service} Configuration File on ${host} ${serverIP} has a syntax error... please check your server and/or contact support."
        notification_icon="fa-exclamation"
        notification_duration="infinity"
        preemptive_support="true"
      else
        if [[ $service != "MYSQL" ]]; then
          title="${service} Service ${aspect} - False Alarm"
        else
          title="${service} Service ${aspect} - Check Percona Version - config syntax check might be running into bug"
        fi
        echo "$title"
      fi
      ;;
    esac
    ;;
  PHP*)
    case $service in
    PHP7.1*)
      env_service="php7.1"
      version="7.1"
      ;;
    PHP7.2*)
      env_service="php7.2"
      version="7.2"
      ;;
    PHP7.3*)
      env_service="php7.3"
      version="7.3"
      ;;
    PHP7.4*)
      env_service="php7.4"
      version="7.4"
      ;;
    esac
    case "${aspect}" in
    CPU_HOT)
      percent=$(grep -w "$env_service-cpu-warning-threshold-percent:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      cycles=$(grep -w "$env_service-mem-restart-cycles:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      minutes=$((cycles * 2))
      title="${service} ${aspect} Warning"
      # shellcheck disable=SC2154
      details="${service} on ${host} ${serverIP} has been using ${percent}% of CPU for over ${minutes} minutes..."
      notification_icon="fa-exclamation"
      notification_duration="long"
      preemptive_support="true"
      ;;
    CPU_RELOAD)
      percent=$(grep -w "$env_service-cpu-restart-threshold-percent:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      cycles=$(grep -w "$env_service-cpu-restart-cycles:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      minutes=$((cycles * 2))
      title="${service} ${aspect} Error"
      # shellcheck disable=SC2154
      details="${service} on ${host} ${serverIP} was using ${percent}% of CPU for over ${minutes} minutes... Reloading ${service}!"
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      preemptive_support="true"
      php-fpm"${version}" -t && /usr/local/bin/gp php "${version}" reload
      ;;
    MEM_HIGH)
      mb=$(grep -w "$env_service-mem-warning-threshold-mb:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      cycles=$(grep -w "$env_service-mem-warning-cycles:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      minutes=$((cycles * 2))
      title="${service} ${aspect} Warning"
      # shellcheck disable=SC2154
      details="${service} on ${host} ${serverIP} has been using at least ${mb}MB RAM for at least the last ${minutes} minutes..."
      notification_icon="fa-exclamation"
      notification_duration="long"
      ;;
    MEM_RELOAD)
      mb=$(grep -w "$env_service-mem-restart-threshold-mb:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      cycles=$(grep -w "$env_service-mem-restart-cycles:.*" /root/gridenv/monit.env | cut -f 2 -d ':') || true
      minutes=$((cycles * 2))
      title="${service} ${aspect} Error"
      # shellcheck disable=SC2154
      details="${service} on ${host} ${serverIP} has been using at least ${mb}MB RAM for at least the last ${minutes} minutes.... Reloading ${service}!"
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      preemptive_support="true"
      version=${service#PHP}
      php-fpm"${version}" -t && /usr/local/bin/gp php "${version}" reload
      ;;
    CONF_SYNTAX)
      if [[ $(php-fpm"${version}" -t 2>&1) != *"test is successful"* ]]; then
        sleep 2
        [[ $(php-fpm"${version}" -t 2>&1) == *"test is successful"* ]] &&
          callback="false"
      else
        callback="false"
      fi
      if [[ -z $callback ]]; then
        title="${service} ${aspect} Error"
        # shellcheck disable=SC2154
        details="Your ${service} Configuration File on ${host} ${serverIP} has a syntax error... please check your server and/or contact support."
        notification_icon="fa-exclamation"
        notification_duration="infinity"
        preemptive_support="true"
      else
        title="${service} Service ${aspect} False Alarm"
        echo "$title"
      fi
      ;;
    FAILED)
      title="${service} ${aspect} Error"
      # shellcheck disable=SC2154
      details="${service} on ${host} ${serverIP} has failed and Monit is unable to restart it successfully... please contact support."
      notification_icon="fa-exclamation"
      notification_duration="infinity"
      preemptive_support="true"
      ;;
    esac
    ;;
  SYS_LOAD_AVG_15)
    case "${aspect}" in
    70)
      duration="1 hour"
      float="0.7"
      ;;
    100)
      duration="30 minutes"
      float="1.0"
      preemptive_support="true"
      ;;
    200)
      duration="10 minutes"
      float="2.0"
      preemptive_support="true"
      ;;
    esac
    title="${service} ${aspect}% CPU Warning"
    # shellcheck disable=SC2154
    details="${host} ${serverIP} 15 Minute Load average has been running at over ${aspect}% (${float}) per core for over ${duration}"
    notification_icon="fa-exclamation"
    notification_duration="long"
    ;;
  SYS_MEM_USAGE)
    case "${aspect}" in
    70) duration="1 hour" ;;
    80) duration="30 minutes" ;;
    90) duration="10 minutes" ;;
    esac
    title="${service} ${aspect}% RAM Warning"
    # shellcheck disable=SC2154
    details="${host} ${serverIP} System Memory utilisation has exceeded ${aspect}% RAM for over ${duration}"
    notification_icon="fa-exclamation"
    notification_duration="long"
    preemptive_support="true"
    ;;
  SYS_CPU_USAGE)
    title="${service} ${4} ${aspect}% CPU Warning"
    # shellcheck disable=SC2154
    details="${host} ${serverIP} ${4} CPU utilisation has exceeded ${aspect}%"
    notification_icon="fa-exclamation"
    notification_duration="long"
    if [[ ${aspect} == "30" ]] ||
      [[ ${4} == "user" && ${aspect} == "70" ]]; then
      details="${details}. This is nothing to be too concerned about yet..."
    else
      preemptive_support="true"
    fi
    ;;
  SYS_SWAP_MEM)
    title="${service} ${aspect}% Usage Warning"
    # shellcheck disable=SC2154
    details="${host} ${serverIP} System Swap Memory usage has exceeded ${aspect}% of allocation!"
    notification_icon="fa-exclamation"
    notification_duration="long"
    preemptive_support="true"
    ;;
  esac

  logger "***** GridPane Monitoring - ${title} ***** ${details}"

  if [[ -z $callback ]]; then
    gridpane::notify::app \
      "${title}" \
      "${details}" \
      "popup_and_center" \
      "${notification_icon}" \
      "${notification_duration}" \
      "NULL"

    gridpane::notify::slack \
      "${slack_type}" \
      "${title}" \
      "${details}" \
      "${preemptive_support}"
  fi
}
