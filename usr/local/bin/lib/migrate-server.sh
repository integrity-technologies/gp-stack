#!/bin/bash

gp_migrate_server::process_args() {
  local continue
  continue=false
  if [[ $# -gt 0 ]]; then
    while true; do
      local var="$1"
      case "${var}" in
      -update-app-url)
        shift 1
        NEW_URL="$1"
        ;;
      -update-dnsme-domain)
        shift 1
        NEW_DOMAIN="$1"
        ;;
      -update-token)
        shift 1
        NEW_TOKEN="$1"
        ;;
      -add-new-key)
        shift 1
        NEW_SSH_KEY="$1"
        ;;
      -remove-old-key)
        shift 1
        OLD_SSH_KEY="$1"
        ;;
      esac
      shift 1 || true
      [[ -z "${var}" ]] &&
        break
    done
    [[ -n $NEW_URL ||
      -n $NEW_DOMAIN ||
      -n $NEW_TOKEN ||
      -n $NEW_SSH_KEY ||
      -n $OLD_SSH_KEY ]] &&
     continue=true
  fi

  if [[ $continue == "false" ]]; then
    echo "gp-migrate-server requires at least one of the following and a value:"
    echo "-update-app-url {value}"
    echo "-update-dnsme-domain {value}"
    echo "-update-token {value}"
    echo "-add-new-key {value}"
    echo "-remove-old-key {value}"
    exit 187
  fi
}

gp_migrate_server::add_new_ssh_key() {
  local ssh_key="$1"
  {
  echo ""
  echo "#Begin Instance Key"
  echo "${ssh_key}"
  echo "#End Instance Key"
  echo ""
  } >>/root/.ssh/authorized_keys
}

gp_migrate_server::update_app_url() {
  local app_url="$1"
  echo "${app_url}" >/root/grid.source
}

gp_migrate_server::update_dnsme_domain() {
  local dnsme_domain="$1"
  local existing_tool_site_domain
  existing_tool_site_domain=$(cat /root/grid.subdom)
  local existing_tool_site_subdomain
  existing_tool_site_subdomain=${existing_tool_site_domain%.*.*}
  local new_phpma_domain
  new_phpma_domain="${existing_tool_site_subdomain}.${dnsme_domain}"
  local new_monit_domain
  new_monit_domain="${existing_tool_site_subdomain}stat.${dnsme_domain}"
  local old_phpma_domain
  old_phpma_domain=$(/usr/local/bin/gp conf read phpma-url -q)
  [[ -z $old_phpma_domain ]] &&
    old_phpma_domain="$existing_tool_site_domain"
  local old_monit_domain
  old_monit_domain=$(/usr/local/bin/gp conf read monit-url -q)
  [[ -z $old_monit_domain ]] &&
    old_monit_domain="${existing_tool_site_subdomain}stat.gridpanevps.com"
  # shellcheck disable=SC2154
  if type nginx >/dev/null; then
    echo "remove nginx virtual servers for old tools sites..."
    [[ -f /etc/nginx/sites-enabled/${old_phpma_domain} ]] &&
      rm "/etc/nginx/sites-enabled/${old_phpma_domain}"
    [[ -f "/etc/nginx/sites-enabled/${old_monit_domain}" ]] &&
      rm "/etc/nginx/sites-enabled/${old_monit_domain}"
  else
    echo "remove openlitespeed virtual servers for old tools sites..."
    [[ -d /usr/local/lsws/conf/vhosts/"$old_phpma_domain" ]] &&
      /bin/rm -rf /usr/local/lsws/conf/vhosts/"$old_phpma_domain"
    [[ -d /usr/local/lsws/conf/vhosts/"$old_monit_domain" ]] &&
      /bin/rm -rf /usr/local/lsws/conf/vhosts/"$old_monit_domain"
  fi
  echo "${new_phpma_domain}" >/root/grid.subdom
  gridpane::conf_write source-env-managed-domain "${dnsme_domain}"
  gridpane::conf_write subdomain "${new_phpma_domain}"
  gridpane::conf_write phpma-url "${new_phpma_domain}"
  gridpane::conf_write monit-url "${new_monit_domain}"
  mv "/var/www/${old_phpma_domain}" "/var/www/${new_phpma_domain}"
  chown -R www-data:www-data "/var/www/${new_phpma_domain}/htdocs"
  mv "/var/www/${old_monit_domain}" "/var/www/${new_monit_domain}"
}

gp_migrate_server::all_sites_db_creds() {
  gridpane::site_loop::directories
  # shellcheck disable=SC2154
  for entry in ${sitesDirectoryArray}; do
    [[ ${entry} == "22222" ||
      ${entry} == "html" ||
      ${entry} == "default" ||
      ${entry} == *"gridpanevps"* ||
      ${entry} == *"$(/usr/local/bin/gp conf read monit-url -q)"* ||
      ${entry} == *"$(/usr/local/bin/gp conf read phpma-url -q)"* ]] &&
      continue
    [[ ${sitesConfigsArray} != *"${entry}"* ]] &&
      continue
    /usr/local/bin/gp site "${entry}" -dbcreds
  done
}

gp_migrate_server::remove_old_ssh_key() {
  local ssh_key="$1"
  grep "${ssh_key}" /root/.ssh/authorized_keys >/dev/null &&
    sed -i "/${ssh_key}/d" /root/.ssh/authorized_keys
}

gp_migrate_away::help() {
    echo
    echo "gp-migrate-away <flags>                 Script to help migrate away from GridPane servers"
    echo
    echo "      --sites                           Optional csv list of sites to process (or single site). "
    echo "                                        If not found then it will do all sites."
    echo
    echo "      --dry-run                         Run the script as a dry run only, do not make any changes."
    echo
    echo "      --keep-ini                        Don't delete the /htdocs/.user.ini file with local PHP directives"
    echo
    echo "      --archive                         Create archives of the site files"
    echo
    echo "Script will:"
    echo "- edit out all GridPane entries from the site wp-config.php files"
    echo "- copy all site wp-config.php files into the htdocs directory (wherein it becomes active"
    echo "- remove all GridPane MU plugins"
    echo "- remove GridPane .user.ini file (optional)"
    echo "- create tarballs of every site (optional)"
    echo
    echo "Once run in non-dry-run configuration sites will not be properly configured for GridPane services"
    echo "Subsequently, all sites will be beyond GridPane support responsibility until returned to previous state."
    echo
    exit
}

gp_migrate_away::lock::trap() {
  if [[ ! -f /tmp/gp-migrate-away.lock ]]; then
    touch /tmp/gp-migrate-away.lock
    trap "rm /tmp/gp-migrate-away.lock; \
      [[ -f /tmp/migrate.away.list ]] && rm /tmp/migrate.away.list;" EXIT
  else
    echo "Lock file detected, migrate Away Process currently running... exiting"
    exit 187
  fi
}

gp_migrate_away::process_args() {
  while true; do
    case $1 in
      --dry-run) DRY_RUN="DRY RUN" ;;
      --keep-ini) KEEP_INI=1 ;;
      --archive) ARCHIVE=1 ;;
      --sites)
        shift
        SITES="$1"
        ;;
      --help) HELP=1 ;;
    esac
    shift 1 || true
    [[ -z "$1" ]] && break
  done
}

gp_migrate_away::sign_off_on_it() {
  echo
  echo "***** THIS IS NOT A DRY RUN *****"
  echo
  echo "You are about to run a script to prepare all sites on this server for outward migration."
  echo "Once run sites will not be GP standard and GP tools will cease to work properly."
  echo "We take no responsibility for the use of this script, use at your own risk!"
  echo
  while true; do
    read -r -p "Do you agree to take responsibility for the use of this script, and would like to proceed (Y/y) or (N/n)?" yn
    case $yn in
        [Yy]*) break ;;
        [Nn]*) exit ;;
        *) echo "Please answer (Y) or (N)" ;;
    esac
  done

  if [[ -n $ARCHIVE ]]; then
    echo
    echo "** You have passed the --archive flag. **"
    echo
    echo "Once the script has finished preparing your sites for outward migration,"
    echo "it will then proceed to archive your sites into compressed tarballs here:"
    echo "/root/outward-migration-archives"
    echo
    echo "Please be aware, you will need to ensure your server has enough free space to accommodate these archives."
    echo "If your server does not have enough space and this kills your server, that is on you."
    echo
    while true; do
      read -r -p "Do you agree to take responsibility for ensuring you have enough space to create archives, (Y/y) or (N/n)?" yn
      case $yn in
          [Yy]*) break ;;
          [Nn]*) exit ;;
          *) echo "Please answer (Y/y) or (N/b)";;
      esac
    done
    [[ ! -d /root/outward-migration-archives ]] &&
      mkdir -p /root/outward-migration-archives
  fi
}

gp_migrate_away::process() {
  if [[ -n $SITES ]]; then
    for site in ${SITES//,/ }
    do
      find "/var/www/${site}" -name wp-config.php >>/tmp/migrate.away.list
    done
    SITE_DIRECTORIES_WP_CONFIGS="$(cat /tmp/migrate.away.list)"
    rm /tmp/migrate.away.list
  else
    SITE_DIRECTORIES_WP_CONFIGS="$(find /var/www -name wp-config.php)"
  fi

  COUNT=0
  while read -r i; do
    COUNT=$((COUNT+1))
    dir=${i%/*}
    echo
    echo "################################################################################################"
    echo "SITE $COUNT ${DRY_RUN} BEGIN:${dir}"
    [[ ${dir} == *"/htdocs" ]] &&
      dir=${dir%/*} &&
      echo "wp-config.php found in HTDOCS - not GP standard... it will get overwritten..."
    if [[ -z $DRY_RUN ]]; then
      [[ -f ${dir}/wp-config.php ]] &&
        cp "${dir}/wp-config.php" "${dir}/htdocs/wp-config.php"
      [[ -f ${dir}/htdocs/wp-config.php ]] &&
        sed -i "/\/\* GridPane \*\//,/include __DIR__ . '\/user-configs.php';/d" "${dir}/htdocs/wp-config.php"
      [[ -f ${dir}/htdocs/.user.ini &&
        -z $KEEP_INI ]] &&
        rm "${dir}/htdocs/.user.ini"
      [[ -f ${dir}/htdocs/wp-content/mu-plugins/wp-cli-login-server.php ]] &&
        rm "${dir}/htdocs/wp-content/mu-plugins/wp-cli-login-server.php"
      [[ -f ${dir}/htdocs/wp-content/mu-plugins/wp-fail2ban.php ]] &&
        rm "${dir}/htdocs/wp-content/mu-plugins/wp-fail2ban.php"
      for gp_mu_plugin in "${dir}"/htdocs/wp-content/mu-plugins/*gridpane*; do
        [ -e "$gp_mu_plugin" ] &&
          rm -rf "$gp_mu_plugin"
      done
      echo "----------------------------------------------------"
      echo "Core dir:"
      ls -la "${dir}/htdocs"
      echo "----------------------------------------------------"
      echo "mu-plugins dir:"
      ls -la "${dir}/htdocs/wp-content/mu-plugins/"
      echo "----------------------------------------------------"
      echo "Edited ${dir}/htdocs/wp-config.php:"
      cat "${dir}/htdocs/wp-config.php"
      echo "----------------------------------------------------"
      if [[ -n $ARCHIVE ]]; then
        local site
        site=${dir##*/}
        if cd "/var/www/${site}/htdocs"; then
          echo "Creating a tarball..."
          tar -czf "/root/outward-migration-archives/${site}.gz" .
          cd - >/dev/null || return
          ls -lh "/root/outward-migration-archives/${site}.gz"
          echo "----------------------------------------------------"
        fi
      fi
    else
      [[ -f ${dir}/wp-config.php ]] &&
        echo "${dir}/wp-config.php -> ${dir}/htdocs/wp-config.php" &&
        echo "----------------------------------------------------"
      [[ -f ${dir}/htdocs/wp-config.php ]] &&
        echo "remove from ${dir}/htdocs/wp-config.php:" &&
        sed "/\/\* GridPane \*\//,/include __DIR__ . '\/user-configs.php';/d" "${dir}/wp-config.php" | diff "${dir}/wp-config.php" - &&
      echo "----------------------------------------------------"
      if [[ -f ${dir}/htdocs/.user.ini ]]; then
        local keep_or_delete
        keep_or_delete="Remove"
        [[ -n $KEEP_INI ]] &&
          keep_or_delete="Keep"
        echo "$keep_or_delete: ${dir}/htdocs/.user.ini"
        cat "${dir}/htdocs/.user.ini"
        echo "----------------------------------------------------"
     fi
      echo "mu-plugins dir items to remove:"
      [[ -f ${dir}/htdocs/wp-content/mu-plugins/wp-cli-login-server.php ]] &&
        echo "${dir}/htdocs/wp-content/mu-plugins/wp-cli-login-server.php"
      [[ -f ${dir}/htdocs/wp-content/mu-plugins/wp-fail2ban.php ]] &&
        echo "${dir}/htdocs/wp-content/mu-plugins/wp-fail2ban.php"
      for gp_mu_plugin in "${dir}"/htdocs/wp-content/mu-plugins/*gridpane*; do
        [ -e "$gp_mu_plugin" ] &&
          echo "$gp_mu_plugin"
      done
      if [[ -n $ARCHIVE ]]; then
        echo "----------------------------------------------------"
        echo "Running the TAR to /dev/null to check for errors..."
        local site
        site=${dir##*/}
        if cd "/var/www/${site}/htdocs"; then
          tar -czf /dev/null .
          cd - >/dev/null || return
        fi
      fi
      echo "----------------------------------------------------"
    fi
    echo "SITE $COUNT ${DRY_RUN} END:${dir}"
    echo "################################################################################################"
    echo
  done <<<"$SITE_DIRECTORIES_WP_CONFIGS"

  if [[ -n $ARCHIVE &&
        -z $DRY_RUN ]]; then
    echo
    echo "################################################################################################"
    echo "Available Archives:"
    echo "/root/outward-migration-archives/"
    ls -lh /root/outward-migration-archives/
    echo "################################################################################################"
    echo
  fi
}