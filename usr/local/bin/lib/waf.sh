#!/bin/bash

waf::sync::site_to_app() {
  local site="$1"
  local waf_to_sync="$2"

  if [[ ! -f /opt/gridpane/security.tab.update ]]; then
    /usr/local/bin/gpupdate force-scripts-configs
    touch /opt/gridpane/security.tab.update
  fi

  if [[ $waf_to_sync == "6g" ]]; then
    if [[ "$webserver" = nginx ]]; then
      local site_conf
      site_conf=$(cat /etc/nginx/sites-available/"${site}")
      local waf
      local six_g_bad_bots
      local six_g_bad_methods
      local six_g_bad_query_strings
      local six_g_bad_referers
      local six_g_bad_requests

      if [[ $site_conf == *"#include /etc/nginx/common/${site}-6g.conf;"* ]]; then
        six_g_bad_bots="false"
        six_g_bad_methods="false"
        six_g_bad_query_strings="false"
        six_g_bad_referers="false"
        six_g_bad_requests="false"
        local waf_state
        if [[ $site_conf == *"#include /etc/nginx/common/${site}-6g.conf;"* &&
              $site_conf != *"#PROXY"* ]]; then
          waf_state="waf=off"
        fi

        # shellcheck disable=SC2154
        gridpane::callback::app \
          "callback" \
          "/site/site-update" \
          "--" \
          "site_url=${site}" \
          "server_ip=${serverIP}" \
          "six_g_bad_bots@${six_g_bad_bots}" \
          "six_g_bad_methods@${six_g_bad_methods}" \
          "six_g_bad_query_strings@${six_g_bad_query_strings}" \
          "six_g_bad_referers@${six_g_bad_referers}" \
          "six_g_bad_requests@${six_g_bad_requests}" \
          "six_g_synced@true" \
          "silent@false" "$waf_state"

      else
        site_6g_conf=$(cat /etc/nginx/common/"${site}"-6g.conf)

        local six_g_bad_bots
        if [[ ${site_6g_conf} == *"#if (\$bad_bot)"* ]]; then
          six_g_bad_bots="false"
          gridpane::conf_write 6G-bad-bots off -site.env "${site}"
        else
          six_g_bad_bots="true"
          gridpane::conf_write 6G-bad-bots on -site.env "${site}"
        fi

        local six_g_bad_methods
        if [[ ${site_6g_conf} == *"#if (\$not_allowed_method)"* ]]; then
          six_g_bad_methods="false"
          gridpane::conf_write 6G-bad-methods off -site.env "${site}"
        else
          six_g_bad_methods="true"
          gridpane::conf_write 6G-bad-methods on -site.env "${site}"
        fi

        local six_g_bad_query_strings
        if [[ ${site_6g_conf} == *"#if (\$bad_querystring)"* ]]; then
          six_g_bad_query_strings="false"
          gridpane::conf_write 6G-bad-query-string off -site.env "${site}"
        else
          six_g_bad_query_strings="true"
          gridpane::conf_write 6G-bad-query-string on -site.env "${site}"
        fi

        local six_g_bad_referers
        if [[ ${site_6g_conf} == *"#if (\$bad_referer)"* ]]; then
          six_g_bad_referers="false"
          gridpane::conf_write 6G-bad-referer off -site.env "${site}"
        else
          six_g_bad_referers="true"
          gridpane::conf_write 6G-bad-referer on -site.env "${site}"
        fi

        local six_g_bad_requests
        if [[ ${site_6g_conf} == *"#if (\$bad_request)"* ]]; then
          six_g_bad_requests="false"
          gridpane::conf_write 6G-bad-request on -site.env "${site}"
        else
          six_g_bad_requests="true"
          gridpane::conf_write 6G-bad-request on -site.env "${site}"
        fi

        gridpane::conf_write waf 6G -site.env "${site}"

        # shellcheck disable=SC2154
        gridpane::callback::app \
          "callback" \
          "/site/site-update" \
          "--" \
          "site_url=${site}" \
          "server_ip=${serverIP}" \
          "waf=6G" \
          "six_g_bad_bots@${six_g_bad_bots}" \
          "six_g_bad_methods@${six_g_bad_methods}" \
          "six_g_bad_query_strings@${six_g_bad_query_strings}" \
          "six_g_bad_referers@${six_g_bad_referers}" \
          "six_g_bad_requests@${six_g_bad_requests}" \
          "six_g_synced@true" \
          "silent@false"
      fi
    else
      /bin/echo "6g sync isn't available for OpenLiteSpeed instances, exiting ..."
    fi
  elif [[ $waf_to_sync == "7g" ]]; then
    local site_conf
    local waf
    local seven_g_bad_bots
    local seven_g_bad_methods
    local seven_g_bad_query_strings
    local seven_g_bad_referers
    local seven_g_bad_requests
    local seven_g_bad_remote_hosts

    if [[ "$webserver" = nginx ]]; then
      site_conf=$(cat /etc/nginx/sites-available/"${site}")
    else
      site_conf="$(/usr/local/bin/gpols get "$site" vhconf)"
    fi

    if [[ "$webserver" = nginx && "$site_conf" == *"#include /etc/nginx/common/${site}-7g.conf;"* ||
          "$webserver" = openlitespeed && "$site_conf" != *"# 7G:[CORE]"* ]]; then
      seven_g_bad_bots="false"
      seven_g_bad_methods="false"
      seven_g_bad_query_strings="false"
      seven_g_bad_referers="false"
      seven_g_bad_requests="false"
      seven_g_bad_remote_hosts="false"
      local waf_state
      if [[ "$webserver" = nginx &&
            $site_conf == *"#include /etc/nginx/common/${site}-7g.conf;"* &&
            $site_conf != *"#PROXY"* ]] ||
        [[ "$webserver" = openlitespeed ]]; then
        waf_state="waf=off"
      fi

      # shellcheck disable=SC2154
      gridpane::callback::app \
        "callback" \
        "/site/site-update" \
        "--" \
        "site_url=${site}" \
        "server_ip=${serverIP}" \
        "seven_g_bad_bots@${seven_g_bad_bots}" \
        "seven_g_bad_methods@${seven_g_bad_methods}" \
        "seven_g_bad_query_strings@${seven_g_bad_query_strings}" \
        "seven_g_bad_referers@${seven_g_bad_referers}" \
        "seven_g_bad_requests@${seven_g_bad_requests}" \
        "seven_g_bad_remote_hosts@${seven_g_bad_remote_hosts}" \
        "seven_g_synced@true" \
        "silent@false" "$waf_state"

    else
      if [[ "$webserver" = nginx ]]; then
        site_7g_conf=$(cat /etc/nginx/common/"${site}"-7g.conf)
      else
        site_7g_conf="$(/usr/local/bin/gpols get "$site" vhconf)"
      fi

      local seven_g_bad_bots
      if [[ "$webserver" = nginx && ${site_7g_conf} == *"#if (\$bad_bot_7g)"* ||
            "$webserver" = openlitespeed && "$site_7g_conf" != *"# 7G:[USER AGENT]"* ]]; then
        seven_g_bad_bots="false"
        gridpane::conf_write 7G-bad-bots off -site.env "${site}"
      else
        seven_g_bad_bots="true"
        gridpane::conf_write 7G-bad-bots on -site.env "${site}"
      fi

      local seven_g_bad_methods
      if [[ "$webserver" = nginx && ${site_7g_conf} == *"#if (\$not_allowed_method_7g)"* ||
            "$webserver" = openlitespeed && "$site_7g_conf" != *"# 7G:[REQUEST METHOD]"* ]]; then
        seven_g_bad_methods="false"
        gridpane::conf_write 7G-bad-methods off -site.env "${site}"
      else
        seven_g_bad_methods="true"
        gridpane::conf_write 7G-bad-methods on -site.env "${site}"
      fi

      local seven_g_bad_query_strings
      if [[ "$webserver" = nginx && ${site_7g_conf} == *"#if (\$bad_querystring_7g)"* ||
            "$webserver" = openlitespeed && "$site_7g_conf" != *"# 7G:[QUERY STRING]"* ]]; then
        seven_g_bad_query_strings="false"
        gridpane::conf_write 7G-bad-query-string off -site.env "${site}"
      else
        seven_g_bad_query_strings="true"
        gridpane::conf_write 7G-bad-query-string on -site.env "${site}"
      fi

      local seven_g_bad_referers
      if [[ "$webserver" = nginx && ${site_7g_conf} == *"#if (\$bad_referer_7g)"* ||
            "$webserver" = openlitespeed && "$site_7g_conf" != *"# 7G:[HTTP REFERRER]"* ]]; then
        seven_g_bad_referers="false"
        gridpane::conf_write 7G-bad-referer off -site.env "${site}"
      else
        seven_g_bad_referers="true"
        gridpane::conf_write 7G-bad-referer on -site.env "${site}"
      fi

      local seven_g_bad_requests
      if [[ "$webserver" = nginx && ${site_7g_conf} == *"#if (\$bad_request_7g)"* ||
            "$webserver" = openlitespeed && "$site_7g_conf" != *"# 7G:[REQUEST URI]"* ]]; then
        seven_g_bad_requests="false"
        gridpane::conf_write 7G-bad-request off -site.env "${site}"
      else
        seven_g_bad_requests="true"
        gridpane::conf_write 7G-bad-request on -site.env "${site}"
      fi

      local seven_g_bad_remote_hosts
      if [[ "$webserver" = openlitespeed && "$site_7g_conf" != *"# 7G:[REMOTE HOST]"* ]] ||
        [[ "$webserver" = nginx ]]; then
        seven_g_bad_remote_hosts="false"
        gridpane::conf_write 7G-bad-remote-host off -site.env "${site}"
      else
        seven_g_bad_remote_hosts="true"
        gridpane::conf_write 7G-bad-remote-host on -site.env "${site}"
      fi

      gridpane::conf_write waf 7G -site.env "${site}"

      # shellcheck disable=SC2154
      gridpane::callback::app \
        "callback" \
        "/site/site-update" \
        "--" \
        "site_url=${site}" \
        "server_ip=${serverIP}" \
        "waf=7G" \
        "seven_g_bad_bots@${seven_g_bad_bots}" \
        "seven_g_bad_methods@${seven_g_bad_methods}" \
        "seven_g_bad_query_strings@${seven_g_bad_query_strings}" \
        "seven_g_bad_referers@${seven_g_bad_referers}" \
        "seven_g_bad_requests@${seven_g_bad_requests}" \
        "seven_g_bad_remote_hosts@${seven_g_bad_remote_hosts}" \
        "seven_g_synced@true" \
        "silent@false"
    fi

  elif [[ $waf_to_sync == "modsec" ]]; then
    local site_conf
    local waf
    local mod_sec_paranoia
    local mod_sec_anomaly

    if [[ "$webserver" = nginx ]]; then
      site_conf=$(cat /etc/nginx/sites-available/${site})
    else
      site_conf="$(/usr/local/bin/gpols get "$site" vhconf)"
    fi

    if [[ "$webserver" = nginx && $site_conf != *"#PROXY"* ||
          "$webserver" = openlitespeed && "$site_conf" != *"module mod_security"* ]]; then
      local waf_state
      if [[ "$webserver" = nginx &&
            $site_conf == *"#include /etc/nginx/common/${site}-7g.conf;"* &&
            $site_conf == *"#include /etc/nginx/common/${site}-6g.conf;"* ]]; then
        waf_state="waf=off"
      elif [[ "$webserver" = openlitespeed ]]; then
        waf_state="waf=off"
      fi
      mod_sec_paranoia=1
      mod_sec_anomaly=10

      gridpane::callback::app \
        "callback" \
        "/site/site-update" \
        "--" \
        "site_url=${site}" \
        "server_ip=${serverIP}" \
        "-s" "mod_sec_paranoia=${mod_sec_paranoia}" \
        "-s" "mod_sec_anomaly=${mod_sec_anomaly}" \
        "modsec_synced@true" \
        "silent@false" "$waf_state"

    else
      waf="modsec"

      mod_sec_paranoia=$(grep "setvar:tx.paranoia_level=" "/var/www/${site}/modsec/${site}-crs-paranoia-level.conf")
      mod_sec_paranoia=${mod_sec_paranoia::-1}
      mod_sec_paranoia="$(echo -e "${mod_sec_paranoia}" | tr -d '[:space:]')"
      mod_sec_paranoia=${mod_sec_paranoia#setvar:tx.paranoia_level=}
      if [[ ${mod_sec_paranoia} != "1" &&
            ${mod_sec_paranoia} != "2" &&
            ${mod_sec_paranoia} != "3" &&
            ${mod_sec_paranoia} != "4" ]]; then
        mod_sec_paranoia="1"
        if [[ "$webserver" = nginx ]]; then
          cp /etc/nginx/modsec/owasp/crs-paranoia-level.conf /var/www/${site}/modsec/${site}-crs-paranoia-level.conf
        else
          /bin/cp /usr/local/lsws/modsec/crs-paranoia-level.conf /var/www/"$site"/modsec/"$site"-crs-paranoia-level.conf
        fi
      fi

      mod_sec_anomaly=$(grep "setvar:tx.inbound_anomaly_score_threshold=" "/var/www/${site}/modsec/${site}-crs-anomaly-blocking-threshold.conf")
      mod_sec_anomaly=${mod_sec_anomaly::-2}
      mod_sec_anomaly="$(echo -e "${mod_sec_anomaly}" | tr -d '[:space:]')"
      mod_sec_anomaly=${mod_sec_anomaly#setvar:tx.inbound_anomaly_score_threshold=}
      if [[ ! ${mod_sec_anomaly} =~ ^[0-9]+$ ]]; then
        mod_sec_anomaly="10"

        if [[ "$webserver" = nginx ]]; then
          cp /etc/nginx/modsec/owasp/crs-anomaly-blocking-threshold.conf /var/www/${site}/modsec/${site}-crs-anomaly-blocking-threshold.conf
        else
          /bin/cp /usr/local/lsws/modsec/crs-anomaly-blocking-threshold.conf /var/www/"$site"/modsec/"$site"-crs-anomaly-blocking-threshold.conf
        fi
      fi

      gridpane::conf_write waf modsec -site.env ${site}
      gridpane::conf_write modsec-paranoia ${mod_sec_paranoia} -site.env ${site}
      gridpane::conf_write modsec-threshold ${mod_sec_anomaly} -site.env ${site}

      gridpane::callback::app \
        "callback" \
        "/site/site-update" \
        "--" \
        "site_url=${site}" \
        "server_ip=${serverIP}" \
        "waf=${waf}" \
        "-s" "mod_sec_paranoia=${mod_sec_paranoia}" \
        "-s" "mod_sec_anomaly=${mod_sec_anomaly}" \
        "modsec_synced@true" \
        "silent@false"

    fi

  elif [[ $waf_to_sync == "wpf2b" ]]; then
    local site_wp_config=$(cat /var/www/${site}/wp-config.php)
    local wpfail2ban_synced="true"

    local wp_f2b
    local wp_f2b_enumeration
    local wp_f2b_usernames
    local wp_f2b_comments
    local wp_f2b_passwords
    local wp_f2b_spam
    local wp_f2b_pingbacks

    if [[ ${site_wp_config} != *"//include __DIR__ . '/wp-fail2ban-configs.php';"* &&
          ${site_wp_config} == *"include __DIR__ . '/wp-fail2ban-configs.php';"* ]]; then
      wp_f2b="true"

      local site_wpf2b_config=$(cat /var/www/${site}/wp-fail2ban-configs.php)

      if [[ ${site_wpf2b_config} == *"define('WP_FAIL2BAN_BLOCK_USER_ENUMERATION', true);"* ]]; then
        wp_f2b_enumeration="true"
      else
        wp_f2b_enumeration="false"
      fi
      gridpane::conf_write wp-fail2ban-enumeration ${wp_f2b_enumeration} -site.env ${site}

      if [[ ${site_wpf2b_config} == *"//define('WP_FAIL2BAN_BLOCKED_USERS'"* ]]; then
        wp_f2b_usernames="false"
      else
        wp_f2b_usernames="true"
      fi
      gridpane::conf_write wp-fail2ban-usernames ${wp_f2b_usernames} -site.env ${site}

      if [[ ${site_wpf2b_config} == *"define('WP_FAIL2BAN_LOG_COMMENTS', true);"* ]]; then
        wp_f2b_comments="true"
      else
        wp_f2b_comments="false"
      fi
      gridpane::conf_write wp-fail2ban-comments ${wp_f2b_comments} -site.env ${site}

      if [[ ${site_wpf2b_config} == *"define('WP_FAIL2BAN_LOG_PASSWORD_REQUEST', true);"* ]]; then
        wp_f2b_passwords="true"
      else
        wp_f2b_passwords="false"
      fi
      gridpane::conf_write wp-fail2ban-passwords ${wp_f2b_passwords} -site.env ${site}

      if [[ ${site_wpf2b_config} == *"define('WP_FAIL2BAN_LOG_PINGBACKS', true);"* ]]; then
        wp_f2b_pingbacks="true"
      else
        wp_f2b_pingbacks="false"
      fi
      gridpane::conf_write wp-fail2ban-pingbacks ${wp_f2b_pingbacks} -site.env ${site}

      if [[ ${site_wpf2b_config} == *"define('WP_FAIL2BAN_LOG_SPAM', true);"* ]]; then
        wp_f2b_spam="true"
      else
        wp_f2b_spam="false"
      fi
      gridpane::conf_write wp-fail2ban-spam ${wp_f2b_spam} -site.env ${site}
    else
      wp_f2b="false"
      wp_f2b_enumeration="false"
      wp_f2b_usernames="false"
      wp_f2b_comments="false"
      wp_f2b_passwords="false"
      wp_f2b_spam="false"
      wp_f2b_pingbacks="false"
      gridpane::conf_write wp-fail2ban ${wp_f2b} -site.env ${site}
      gridpane::conf_write wp-fail2ban-enumeration ${wp_f2b_enumeration} -site.env ${site}
      gridpane::conf_write wp-fail2ban-usernames ${wp_f2b_usernames} -site.env ${site}
      gridpane::conf_write wp-fail2ban-comments ${wp_f2b_comments} -site.env ${site}
      gridpane::conf_write wp-fail2ban-passwords ${wp_f2b_passwords} -site.env ${site}
      gridpane::conf_write wp-fail2ban-spam ${wp_f2b_spam} -site.env ${site}
      gridpane::conf_write wp-fail2ban-pingbacks ${wp_f2b_pingbacks} -site.env ${site}
    fi

    gridpane::callback::app \
      "callback" \
      "/site/site-update" \
      "--" \
      "site_url=${site}" \
      "server_ip=${serverIP}" \
      "wpfail2ban_synced@${wpfail2ban_synced}" \
      "wp_f2b@${wp_f2b}" \
      "wp_f2b_usernames@${wp_f2b_usernames}" \
      "wp_f2b_user_enumeration@${wp_f2b_enumeration}" \
      "wp_f2b_spam@${wp_f2b_spam}" \
      "wp_f2b_comments@${wp_f2b_comments}" \
      "wp_f2b_pingbacks@${wp_f2b_pingbacks}" \
      "wp_f2b_passwords@${wp_f2b_passwords}" \
      "silent@false"

  fi

}