# shellcheck disable=SC2148

# Define variables
export OLS_1="${1:-}"
export OLS_2="${2:-}"
export OLS_BUILD="Build: 2021-04-28"
export OLS_EMAIL=root@localhost
export OLS_FLAG_AUTH=
export OLS_FLAG_CACHE=
export OLS_FLAG_FOLLOW=
export OLS_FLAG_LOG_ACCESS=
export OLS_FLAG_MULTISITE=
export OLS_FLAG_NO_RESTART=
export OLS_FLAG_OUTPUT=
export OLS_FLAG_PHP=
export OLS_FLAG_PRIMARY=
export OLS_FLAG_QUIET=
export OLS_FLAG_ROUTE=
export OLS_FLAG_SSL=
export OLS_FLAG_WILDCARD=
export OLS_PATH_CONF=/usr/local/lsws/conf
export OLS_PATH_DIR=/usr/local/lsws
export OLS_PATH_LIB=/usr/local/bin/lib/ols
export OLS_PATH_SERVER_CONF=/usr/local/lsws/conf/httpd_config.conf
export OLS_PATH_VHOSTS=/usr/local/lsws/conf/vhosts
export OLS_SUBDOMAIN=
export OLS_SUBDOMAIN_STAT=

if [[ -f /root/gridcreds/wp.email ]]; then
    OLS_EMAIL="$(/bin/cat < /root/gridcreds/wp.email)"
fi

OLS_SUBDOMAIN="$(/bin/grep phpma-url: /root/gridenv/promethean.env | /bin/sed "s|phpma-url:||g" || true)"
OLS_SUBDOMAIN_STAT="$(/bin/grep monit-url: /root/gridenv/promethean.env | /bin/sed "s|monit-url:||g" || true)"

if [[ -z "$OLS_SUBDOMAIN" && -f /root/grid.subdom || -z "$OLS_SUBDOMAIN_STAT" && -f /root/grid.subdom ]]; then
    OLS_SUBDOMAIN="$(/bin/cat < /root/grid.subdom)"
    OLS_SUBDOMAIN_STAT="$(/bin/cat < /root/grid.subdom | /bin/sed "s|.gridpane|stat.gridpane|g")"
fi

# Source main OLS files
OLS_SOURCE="$(ls -A "$OLS_PATH_LIB"/source)"

for OLS_SOURCE_FILE in $OLS_SOURCE; do
    . "$OLS_PATH_LIB"/source/"$OLS_SOURCE_FILE"
done

# Gatekeeper
if [[ ! -f /usr/local/lsws/bin/lswsctrl ]]; then
    ols_error "Sorry, this server isn't an OpenLiteSpeed server"
    /bin/echo "$OLS_BUILD"
fi
