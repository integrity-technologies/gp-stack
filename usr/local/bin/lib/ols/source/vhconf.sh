# shellcheck disable=SC2148

##
#   Template for 7G WAF.
#
ols_vhconf_7g() {
    # shellcheck disable=SC2153
    [[ "$OLS_1" = addon ]] && OLS_2="$OLS_FLAG_PRIMARY"
    OLS_VHCONF_7G="$(ols_get "$OLS_2" waf)"

    if [[ "$OLS_VHCONF_7G" = 7G || -f /var/www/"$OLS_2"/ols/7g-init.conf ]]; then
        # Loop cat .conf files
        OLS_VHCONF_7G_CONFS="$(/usr/bin/find /var/www/"$OLS_2"/ols -name "7g-*.conf")"

        for OLS_VHCONF_7G_CONF in $OLS_VHCONF_7G_CONFS; do
            /bin/cat < "$OLS_VHCONF_7G_CONF"
            /bin/echo -e "\n"
        done
    fi
}
##
#   Template for addon domains.
#
ols_vhconf_addon() {
    [[ -z "$OLS_FLAG_PRIMARY" ]] && ols_error "--primary flag missing, exiting ... "
    [[ ! -d "$OLS_PATH_VHOSTS"/"$OLS_2" ]] && /bin/mkdir -p "$OLS_PATH_VHOSTS"/"$OLS_2"

    /bin/echo "# $OLS_FLAG_PRIMARY - addon
$(ols_vhconf_comments)

docRoot                   \$VH_ROOT/htdocs
cgroups                   0

errorlog \$VH_ROOT/logs/${OLS_FLAG_PRIMARY}.error.log {
  useServer               0
  logLevel                DEBUG
  rollingSize             10M
}

accesslog \$VH_ROOT/logs/${OLS_FLAG_PRIMARY}.access.log {
  useServer               0
  logFormat               \"%v %h %l %u %t \"%r\" %>s %b\"
  logHeaders              7
  rollingSize             10M
  keepDays                30
  compressArchive         0
}

index  {
  useServer               1
}

scripthandler  {
  add                     lsapi:$(ols_php_latest) php
}

context / {
  allowBrowse             1
  note                    Rewrite

  rewrite  {
    enable                  1
    autoLoadHtaccess        1
    rules                   <<<END_rules
$(ols_vhconf_7g)
$(ols_vhconf_ssl rewrite)
    END_rules
  }

  addDefaultCharset       off

  phpIniOverride  {

  }
}

$(ols_vhconf_ssl vhssl)
$(ols_vhconf_modsec)

" > "$OLS_PATH_VHOSTS"/"$OLS_2"/vhconf.conf

    if [[ "$OLS_FLAG_OUTPUT" = true ]]; then
        /bin/cat < "$OLS_PATH_VHOSTS"/"$OLS_2"/vhconf.conf
    fi

    ols_echo "[vhconf] [addon] $OLS_2"
}
##
#   Template for site's vhconf comments.
#
ols_vhconf_comments() {
    OLS_VHCONF_COMMENTS_PROTO="$(ols_get "$OLS_2" proto)"

    # Process Mode
    /bin/echo "# lsapi-$(ols_get "$OLS_2" lsapi)"

    # route
    if [[ -n "$OLS_FLAG_ROUTE" ]]; then
        case "$OLS_FLAG_ROUTE" in
            root)
                /bin/echo "# route-root"
            ;;
            www)
                /bin/echo "# route-www"
            ;;
            none)
                /bin/echo "# route-none"
            ;;
        esac
    else
        OLS_VHCONF_COMMENTS_ROUTE="$(ols_get "$OLS_2" route)"
        if [[ -n "$OLS_VHCONF_COMMENTS_ROUTE" ]]; then
            /bin/echo "# route-$OLS_VHCONF_COMMENTS_ROUTE"
        else
            /bin/echo "# route-none"
        fi
    fi

    if [[   "$OLS_FLAG_WILDCARD" = true ||
            "$OLS_FLAG_WILDCARD" != false &&
            "$(ols_check_wildcard "$OLS_2")" = true ]]; then
        /bin/echo "# wildcard-${OLS_VHCONF_COMMENTS_PROTO}"
    fi
}
##
#   Output header contents from file.
#
ols_vhconf_headers() {
    OLS_VHCONF_HEADERS=/var/www/"$OLS_2"/ols/headers.conf

    if [[ ! -f "$OLS_VHCONF_HEADERS" ]]; then
        /bin/echo "# Custom User Headers
# Usage: one line per entry
# Included in ${OLS_PATH_VHOSTS}/${OLS_2}/vhconf.conf
Referrer-Policy strict-origin-when-cross-origin
Strict-Transport-Security: max-age=31536000
X-Content-Type-Options nosniff
X-Frame-Options SAMEORIGIN
X-XSS-Protection 1; mode=block
" > "$OLS_VHCONF_HEADERS"
    fi

    /bin/cat < "$OLS_VHCONF_HEADERS" | /bin/sed "/#/d"

    # Output CSP if enabled via gp command.
    if [[ -f /var/www/"$OLS_2"/ols/"$OLS_2"-headers-csp.conf ]]; then
      /bin/cat < /var/www/"$OLS_2"/ols/"$OLS_2"-headers-csp.conf | /bin/sed "/#/d"
    fi
}
##
#   Template for vhconf modsec rule block.
#
ols_vhconf_modsec() {
    # shellcheck disable=SC2153
    [[ "$OLS_1" = addon ]] && OLS_2="$OLS_FLAG_PRIMARY"
    OLS_VHCONF_MODSEC="$(ols_get "$OLS_2" waf)"

    if [[ "$OLS_VHCONF_MODSEC" = modsec ]]; then
      echo "module mod_security {
  modsecurity on
  modsecurity_rules \`
    SecRuleEngine On
\`
  modsecurity_rules_file /var/www/${OLS_2}/modsec/${OLS_2}-main.conf
}"
    fi
}
##
#   Template for phpMyAdmin.
#
ols_vhconf_pma() {
    /bin/echo "docRoot                   \$VH_ROOT/htdocs
cgroups                   0

errorlog ${OLS_PATH_DIR}/logs/${OLS_SUBDOMAIN}.error.log {
  useServer               0
  logLevel                DEBUG
  rollingSize             10M
}

accesslog ${OLS_PATH_DIR}/logs/${OLS_SUBDOMAIN}.access.log {
  useServer               0
  logFormat               \"%v %h %l %u %t \"%r\" %>s %b\"
  logHeaders              7
  rollingSize             10M
  keepDays                30
  compressArchive         0
}

index  {
  useServer               1
}

scripthandler  {
  add                     lsapi:$(ols_php_latest) php
}

context / {
  allowBrowse             1
  note                    Rewrite

  rewrite  {
    enable                1
    autoLoadHtaccess      1
    rules                 <<<OLS_RULES
      $(ols_vhconf_ssl rewrite "$OLS_SUBDOMAIN")
    OLS_RULES

  }
  addDefaultCharset       off

  phpIniOverride  {

  }
}

rewrite  {
  enable                  1
  autoLoadHtaccess        1
}

$(ols_vhconf_ssl vhssl "$OLS_SUBDOMAIN")

" > "$OLS_PATH_VHOSTS"/"$OLS_SUBDOMAIN"/vhconf.conf

    if [[ "$OLS_FLAG_OUTPUT" = true ]]; then
        ols_get "$OLS_1" vhconf
    fi

    ols_echo "[vhconf] $OLS_SUBDOMAIN"
}
##
#   Template for redirect domains.
#
ols_vhconf_redirect() {
    [[ -z "$OLS_FLAG_PRIMARY" ]] && ols_error "--primary flag missing, exiting ... "
    [[ ! -d "$OLS_PATH_VHOSTS"/"$OLS_2" ]] && /bin/mkdir -p "$OLS_PATH_VHOSTS"/"$OLS_2"

    /bin/echo "# $OLS_FLAG_PRIMARY - redirect
$(ols_vhconf_comments)

docRoot                   \$VH_ROOT/htdocs
cgroups                   0

errorlog \$VH_ROOT/logs/${OLS_FLAG_PRIMARY}.error.log {
  useServer               0
  logLevel                DEBUG
  rollingSize             10M
}

accesslog \$VH_ROOT/logs/${OLS_FLAG_PRIMARY}.access.log {
  useServer               0
  logFormat               \"%v %h %l %u %t \"%r\" %>s %b\"
  logHeaders              7
  rollingSize             10M
  keepDays                30
  compressArchive         0
}

index  {
  useServer               1
}

scripthandler  {
  add                     lsapi:$(ols_get "$OLS_FLAG_PRIMARY" lsphp) php
}

context / {
  allowBrowse             1
  note                    Rewrite

  rewrite  {
    enable                1
    autoLoadHtaccess      1
    rules                 <<<OLS_RULES
      $(ols_vhconf_ssl rewrite)
      RewriteCond %{HTTP_HOST} ^${OLS_2}$ [NC]
      RewriteRule (.*) $(ols_get "$OLS_FLAG_PRIMARY" proto)://${OLS_FLAG_PRIMARY} [R=301,L]
    OLS_RULES
  }

  addDefaultCharset       off

  phpIniOverride  {

  }
}

$(ols_vhconf_ssl vhssl)

" > "$OLS_PATH_VHOSTS"/"$OLS_2"/vhconf.conf

    if [[ "$OLS_FLAG_OUTPUT" = true ]]; then
        /bin/cat < "$OLS_PATH_VHOSTS"/"$OLS_2"/vhconf.conf
    fi

    ols_echo "[vhconf] [redirect] $OLS_2"
}
##
#   Outputs correct rewrite rules depending on WordPress type.
#
ols_vhconf_wp_rewrites() {
    OLS_REWRITE_TYPE="${1:-}"

    /bin/echo "
# BEGIN WordPress
RewriteRule ^index\.php\$ - [L]"

    case "$OLS_REWRITE_TYPE" in
        subdomain|sub-domain)
            /bin/echo "RewriteRule ^wp-admin\$ wp-admin/ [R=301,L]
RewriteCond %{REQUEST_FILENAME} -f [OR]
RewriteCond %{REQUEST_FILENAME} -d
RewriteRule ^ - [L]
RewriteRule ^(wp-(content|admin|includes).*) \$1 [L]
RewriteRule ^(.*\.php)\$ \$1 [L]"
        ;;
        subdirectory|sub-directory)
            /bin/echo "RewriteRule ^([_0-9a-zA-Z-]+/)?wp-admin\$ \$1wp-admin/ [R=301,L]
RewriteCond %{REQUEST_FILENAME} -f [OR]
RewriteCond %{REQUEST_FILENAME} -d
RewriteRule ^ - [L]
RewriteRule ^([_0-9a-zA-Z-]+/)?(wp-(content|admin|includes).*) \$2 [L]
RewriteRule ^([_0-9a-zA-Z-]+/)?(.*\.php)\$ \$2 [L]"
        ;;
        *)
            /bin/echo "RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d"
        ;;
    esac

    /bin/echo "RewriteRule . index.php [L]
# END WordPress"
}
##
#   Main function for site's rewrite rules
#
ols_vhconf_rewrites() {
    OLS_VHCONF_REWRITES_SITE_CONF_PATH=/var/www/"$OLS_2"/ols

    # Generate rewrites.conf
    if [[ ! -f "$OLS_VHCONF_REWRITES_SITE_CONF_PATH"/rewrites.conf ]]; then
        /bin/echo "# Custom User Rewrites
# Usage: one line per entry
# Included in ${OLS_PATH_VHOSTS}/${OLS_2}/vhconf.conf
" > "$OLS_VHCONF_REWRITES_SITE_CONF_PATH"/rewrites.conf
    fi

    # Loop cat .conf files
    OLS_VHCONF_REWRITES_CONFS="$(/usr/bin/find /var/www/"$OLS_2"/ols -name "*.conf")"

    for OLS_VHCONF_REWRITES_CONF in $OLS_VHCONF_REWRITES_CONFS; do
        # Skip 7g, headers, and csp conf
        [[ "$OLS_VHCONF_REWRITES_CONF" == *"7g-"* ||
            "$OLS_VHCONF_REWRITES_CONF" == *"headers.conf"* ||
            "$OLS_VHCONF_REWRITES_CONF" == *"-headers-csp.conf"* ]] && continue
        /bin/cat < "$OLS_VHCONF_REWRITES_CONF"
        /bin/echo -e "\n"
    done

    # http www route redirect
    if [[ "$(ols_env route)" = www && -z "$(ols_check_ssl_file "$OLS_2")" ]]; then
        /bin/echo "# Route www redirect
RewriteCond %{HTTP_HOST} !^www\.
RewriteRule ^(.*)$ http://www.%{HTTP_HOST}/\$1 [R=301,L]
"
    fi

    # SSL
    ols_vhconf_ssl rewrite
}
##
#   Main function for vhconf ssl.
#
ols_vhconf_ssl() {
    OLS_VHCONF_ARG1="${1:-}"
    OLS_VHCONF_ARG2="${2:-$OLS_2}"
    OLS_VHCONF_HTTP2_PUSH="$(ols_get "$OLS_2" http2-push)"
    OLS_VHCONF_KEY=
    OLS_VHCONF_CERT=
    OLS_VHCONF_PROTOCOLS="enableSpdy              10"
    OLS_VHCONF_WWW=

    if [[ "$OLS_FLAG_SSL" = true || -n "$(ols_check_ssl_file "$OLS_VHCONF_ARG2")" ]]; then
        if [[ -d "$OLS_PATH_CONF"/cert/"$OLS_VHCONF_ARG2" ]]; then
            OLS_VHCONF_KEY="$OLS_PATH_CONF"/cert/"$OLS_VHCONF_ARG2"/key.pem
            OLS_VHCONF_CERT="$OLS_PATH_CONF"/cert/"$OLS_VHCONF_ARG2"/cert.pem
        elif [[ -d /etc/letsencrypt/live/"$OLS_VHCONF_ARG2" ]]; then
            OLS_VHCONF_KEY=/etc/letsencrypt/live/"$OLS_VHCONF_ARG2"/privkey.pem
            OLS_VHCONF_CERT=/etc/letsencrypt/live/"$OLS_VHCONF_ARG2"/fullchain.pem
        else
            ols_error "No certs found for $OLS_VHCONF_ARG2" true
        fi

        # http2-push support
        if [[ -f /var/www/"$OLS_2"/logs/http2-push-preload.on || "$OLS_VHCONF_HTTP2_PUSH" = true ]]; then
          # Enabling HTTP/2 only (SPDY/2 will error Firefox)
          # Default is SPDY/3 HTTP/3
          OLS_VHCONF_PROTOCOLS="enableSpdy              4"
        fi

        case "$OLS_VHCONF_ARG1" in
            rewrite)
                [[ "$(ols_env route)" = www ]] && OLS_VHCONF_WWW=www.
                /bin/echo "
# SSL
RewriteCond %{SERVER_PORT} 80
RewriteRule ^(.*)$ https://${OLS_VHCONF_WWW}%{HTTP_HOST}%{REQUEST_URI} [R=301,L]"
            ;;
            vhssl)
                /bin/echo "vhssl {
  keyFile                 $OLS_VHCONF_KEY
  certFile                $OLS_VHCONF_CERT
  certChain               1
  sslProtocol             24
  $OLS_VHCONF_PROTOCOLS
  enableStapling          1
  ocspRespMaxAge          86400
}"
            ;;
            *)
                ols_error "Missing argument, exiting ..."
            ;;
        esac
    fi
}
##
#   Template for Monit.
#
ols_vhconf_stats() {
    OLS_VHCONF_STATS=

    if [[ -n "$(ols_check_ssl_file "$OLS_SUBDOMAIN_STAT")" ]]; then
        OLS_VHCONF_STATS="type                    proxy
  handler                 monit"
    fi

    /bin/echo "docRoot                   \$VH_ROOT/htdocs
cgroups                   0

errorlog ${OLS_PATH_DIR}/logs/${OLS_SUBDOMAIN_STAT}.error.log {
  useServer               0
  logLevel                DEBUG
  rollingSize             10M
}

accesslog ${OLS_PATH_DIR}/logs/${OLS_SUBDOMAIN_STAT}.access.log {
  useServer               0
  logFormat               \"%v %h %l %u %t \"%r\" %>s %b\"
  logHeaders              7
  rollingSize             10M
  keepDays                30
  compressArchive         0
}

index  {
  useServer               1
}

context / {
  $OLS_VHCONF_STATS
  addDefaultCharset       off
}

rewrite  {
  enable                1
  autoLoadHtaccess      1
  rules                 <<<OLS_RULES
    $(ols_vhconf_ssl rewrite "$OLS_SUBDOMAIN_STAT")
  OLS_RULES
}

$(ols_vhconf_ssl vhssl "$OLS_SUBDOMAIN_STAT")

" > "$OLS_PATH_VHOSTS"/"$OLS_SUBDOMAIN_STAT"/vhconf.conf

    if [[ "$OLS_FLAG_OUTPUT" = true ]]; then
        ols_get "$OLS_1" vhconf
    fi

    ols_echo "[vhconf] $OLS_SUBDOMAIN_STAT"
}
##
# Template for WordPress.
#
ols_vhconf_wp() {
    ols_check_wp "$OLS_2"
    [[ ! -d "$OLS_PATH_VHOSTS"/"$OLS_2" ]] && /bin/mkdir -p "$OLS_PATH_VHOSTS"/"$OLS_2"

    OLS_VHCONF_WP_BROTLI=0
    OLS_VHCONF_WP_BROTLI_CHECK=

    ### PHP SWITCHING ###
    case "$OLS_FLAG_PHP" in
        7.2)
            OLS_VHCONF_LSPHP=lsphp72
        ;;
        7.3)
            OLS_VHCONF_LSPHP=lsphp73
        ;;
        7.4)
            OLS_VHCONF_LSPHP=lsphp74
        ;;
        *)
            OLS_VHCONF_LSPHP_CHECK="$(ols_env php-ver)"

            if [[ "$OLS_VHCONF_LSPHP_CHECK" = 7.1 ]]; then
                OLS_VHCONF_LSPHP=lsphp73
            else
                OLS_VHCONF_LSPHP=lsphp"${OLS_VHCONF_LSPHP_CHECK/./}"
            fi
        ;;
    esac

    if [[ -n "$OLS_FLAG_CACHE" ]]; then
        ols_cache "$OLS_FLAG_CACHE"
    fi

    # Check for brotli
    OLS_VHCONF_WP_BROTLI_CHECK="$(ols_get "$OLS_2" brotli)"
    [[ "$OLS_VHCONF_WP_BROTLI_CHECK" = true ]] && OLS_VHCONF_WP_BROTLI=1

    /bin/echo "# PRIMARY
$(ols_vhconf_comments)

docRoot                   \$VH_ROOT/htdocs
enableBr                  $OLS_VHCONF_WP_BROTLI

errorlog \$VH_ROOT/logs/${OLS_2}.error.log {
  useServer               0
  logLevel                DEBUG
  rollingSize             10M
}

accesslog \$VH_ROOT/logs/${OLS_2}.access.log {
  useServer               0
  logFormat               \"%h %l %u %t \\\"%r\\\" %>s %b \\\"%{Referer}i\\\" \\\"%{User-agent}i\\\"
  logHeaders              7
  rollingSize             10M
  keepDays                30
  compressArchive         0
}

index  {
  useServer               1
}

$(ols_php vhconf)

scripthandler  {
  add                     lsapi:${OLS_2}-${OLS_VHCONF_LSPHP} php
}

context / {
  allowBrowse             1
  note                    Rewrite
  extraHeaders            <<<END_extraHeaders
$(ols_vhconf_headers)
  END_extraHeaders

  rewrite  {
    enable                1
$(ols_vhconf_rewrites)
$(ols_vhconf_7g)
$(ols_vhconf_wp_rewrites "${OLS_FLAG_MULTISITE:-$(ols_env multisite)}")
  }

  $(ols_auth context)

  addDefaultCharset       off

  phpIniOverride  {

  }
}

rewrite  {
  enable                  1
  autoLoadHtaccess        1
}

$(ols_vhconf_ssl vhssl)
$(ols_auth realm)
$(ols_vhconf_modsec)

" > "$OLS_PATH_VHOSTS"/"$OLS_2"/vhconf.conf

    # Symlink logs
    if [[ ! -f /var/log/ols/"$OLS_2".access.log || ! -f /var/log/ols/"$OLS_2".error.log ]]; then
        [[ ! -d /var/log/ols ]] && /bin/mkdir -p /var/log/ols
        ln -sf /var/www/"$OLS_2"/logs/"$OLS_2".access.log /var/log/ols/"$OLS_2".access.log
        ln -sf /var/www/"$OLS_2"/logs/"$OLS_2".error.log /var/log/ols/"$OLS_2".error.log
    fi

    if [[ "$OLS_FLAG_OUTPUT" = true ]]; then
        ols_get "$OLS_2" vhconf
    fi

    ols_echo "[vhconf] $OLS_2"
}
