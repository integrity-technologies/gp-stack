# shellcheck disable=SC2148

##
#   Upgrade/downgrade OLS versions
#
ols_version() {
    if [[ -z "$OLS_2" ]]; then
        ols_error "Missing version or list"
    fi

    OLS_VERSION="$(/bin/cat < /usr/local/bin/lib/ols/versions)"

    case "$OLS_2" in
        list) # Show all versions available
            /bin/echo "$OLS_VERSION"
        ;;
        *) # Error if current version is arleady running
            if [[ "$(/usr/local/lsws/bin/openlitespeed -v)" == *"$OLS_2"* ]]; then
                ols_error "$OLS_2 is already installed"
            fi

            # Check if version matches and is available from GitHub before executing script
            if [[ "$OLS_VERSION" == *"$OLS_2"* ]]; then
                /usr/bin/curl -sL https://raw.githubusercontent.com/litespeedtech/openlitespeed/master/dist/admin/misc/lsup.sh -o /tmp/lsup.sh
                /bin/chmod +x /tmp/lsup.sh
                /tmp/lsup.sh -e "$OLS_2" 2> /dev/null
                /usr/local/lsws/bin/openlitespeed -v
                /bin/rm /tmp/lsup.sh
            else
                OLS_AVAILABLE="\n\nAvailable stable versions\n==================\n${OLS_VERSION}"

                if [[ "$OLS_2" == *"1.7"* ]]; then
                    ols_error "Only stable versions are allowed $OLS_AVAILABLE"
                else
                    ols_error "$OLS_2 is not a valid version $OLS_AVAILABLE"
                fi
            fi
        ;;
    esac
}
