# shellcheck disable=SC2148

##
#   Check if there's value in the default file.
#
ols_check_default() {
    if [[ -f /var/www/openlitespeed.default.site ]]; then
        /bin/cat < /var/www/openlitespeed.default.site
    fi
}
##
#   Output keys if it exists.
#
ols_check_ssl_file() {
    OLS_CHECK_SSL_FILE="${1:-}"

    if [[ -d /etc/letsencrypt/live/"$OLS_CHECK_SSL_FILE" ]]; then
        /bin/echo /etc/letsencrypt/live/"$OLS_CHECK_SSL_FILE"/privkey.pem
    elif [[ -d /usr/local/lsws/conf/cert/"$OLS_CHECK_SSL_FILE" ]]; then
        /bin/echo /usr/local/lsws/conf/cert/"$OLS_CHECK_SSL_FILE"/key.pem
    fi
}
##
#   Outputs true if domain is wildcard.
#
ols_check_wildcard() {
    OLS_CHECK_WILDCARD="${1:-$OLS_2}"
    OLS_CHECK_WILDCARD_GREP=

    if [[ -f "$OLS_PATH_VHOSTS"/"$OLS_CHECK_WILDCARD"/vhconf.conf ]]; then
        OLS_CHECK_WILDCARD_GREP="$(/bin/grep "# wildcard-$(ols_get "$OLS_CHECK_WILDCARD" proto)" "$OLS_PATH_VHOSTS"/"$OLS_CHECK_WILDCARD"/vhconf.conf || true)"

        if [[ -n "$OLS_CHECK_WILDCARD_GREP" ]]; then
            /bin/echo true
        fi
    fi
}
##
#   Exit if it's not a valid WordPress Installation.
#
ols_check_wp() {
    OLS_CHECK_WP="${1:-$OLS_2}"

    if [[   "$OLS_CHECK_WP" != httpd && ! -f /var/www/"$OLS_CHECK_WP"/wp-config.php ||
            "$OLS_CHECK_WP" != httpd && ! -f /var/www/"$OLS_CHECK_WP"/logs/"$OLS_CHECK_WP".env ]]; then
        ols_error "Not a valid WordPress installation, exiting ..."
    fi
}
