# shellcheck disable=SC2148

##
# Execute commands based on cache-type. Defaults to redis && php.
#
ols_cache() {
    OLS_CACHE="${1:-}"
    OLS_CACHE_CHECK="$(ols_wpcli "$OLS_2" "option get litespeed.conf.cache")"

    if [[ "$OLS_CACHE_CHECK" = 1 ]]; then
        ols_wpcli "$OLS_2" "litespeed-purge all"
    fi

    case "$OLS_CACHE" in
        redis)
            ols_wpcli "$OLS_2" "option update litespeed.conf.object 1"
            ols_wpcli "$OLS_2" "option update litespeed.conf.object-kind 1"
            ols_wpcli "$OLS_2" "option update litespeed.conf.object-port 6379"
            ols_wpcli "$OLS_2" "option update litespeed.conf.object-life $(ols_env ols-redis-cache-valid)"
        ;;
        redis-off)
            ols_wpcli "$OLS_2" "option update litespeed.conf.object 0"
        ;;
        redis-only)
            ols_wpcli "$OLS_2" "option update litespeed.conf.object 1"
            ols_wpcli "$OLS_2" "option update litespeed.conf.object-kind 1"
            ols_wpcli "$OLS_2" "option update litespeed.conf.object-port 6379"
            ols_wpcli "$OLS_2" "option update litespeed.conf.object-life $(ols_env ols-redis-cache-valid)"
            ols_wpcli "$OLS_2" "option update litespeed.conf.cache 0"
        ;;
        redis-page)
            ols_wpcli "$OLS_2" "option update litespeed.conf.object 1"
            ols_wpcli "$OLS_2" "option update litespeed.conf.object-kind 1"
            ols_wpcli "$OLS_2" "option update litespeed.conf.object-port 6379"
            ols_wpcli "$OLS_2" "option update litespeed.conf.object-life $(ols_env ols-redis-cache-valid)"
            ols_wpcli "$OLS_2" "option update litespeed.conf.cache 1"
        ;;
        page)
            ols_wpcli "$OLS_2" "option update litespeed.conf.cache 1"
        ;;
        page-off)
            ols_wpcli "$OLS_2" "option update litespeed.conf.cache 0"
        ;;
        fastcgi|page-only)
            ols_wpcli "$OLS_2" "option update litespeed.conf.cache 1"
            ols_wpcli "$OLS_2" "option update litespeed.conf.object 0"
        ;;
        php|off)
            ols_wpcli "$OLS_2" "option update litespeed.conf.object 0"
            ols_wpcli "$OLS_2" "option update litespeed.conf.cache 0"
        ;;
        *)
            ols_wpcli "$OLS_2" "option update litespeed.conf.object 1"
            ols_wpcli "$OLS_2" "option update litespeed.conf.object-kind 1"
            ols_wpcli "$OLS_2" "option update litespeed.conf.object-port 6379"
            ols_wpcli "$OLS_2" "option update litespeed.conf.object-life $(ols_env ols-redis-cache-valid)"
            ols_wpcli "$OLS_2" "option update litespeed.conf.cache 1"
        ;;
    esac
}
