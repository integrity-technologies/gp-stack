# shellcheck disable=SC2148

##
#   Helper function to get site/server logs.
#
ols_log() {
    OLS_LOG=error
    OLS_LOG_COMMAND="/usr/bin/tail -500"
    OLS_LOG_PATH=

    [[ "$OLS_FLAG_LOG_ACCESS" = true ]] && OLS_LOG=access
    [[ "$OLS_FLAG_FOLLOW" = true ]] && OLS_LOG_COMMAND="/usr/bin/tail -f"
    
    if [[ "$OLS_2" = httpd ]]; then
        OLS_LOG_PATH="$OLS_PATH_DIR"/logs/"$OLS_LOG".log
    else
        OLS_LOG_PATH=/var/www/"$OLS_2"/logs/"$OLS_2"."$OLS_LOG".log
    fi

    # Fallback to server logs
    [[ ! -f "$OLS_LOG_PATH" ]] && OLS_LOG_PATH=/usr/local/lsws/logs/error.log

    eval "$OLS_LOG_COMMAND" "$OLS_LOG_PATH"
}
