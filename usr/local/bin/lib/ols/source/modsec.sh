# shellcheck disable=SC2148

##
# Downloads modsec and configure.
#
ols_modsec() {
    OLS_MODSEC_VERSION=3.3.0

    /usr/bin/wget --timeout=120 https://github.com/coreruleset/coreruleset/archive/v"$OLS_MODSEC_VERSION".tar.gz -O /tmp/v"$OLS_MODSEC_VERSION".tar.gz
    /bin/tar -xzf /tmp/v"$OLS_MODSEC_VERSION".tar.gz -C /tmp
    
    # Remove /opt/gridpane/modsec if it exists
    if [[ -d /opt/gridpane/modsec ]]; then
        /bin/rm -rf /opt/gridpane/modsec
    fi

    /bin/mv /tmp/coreruleset-"$OLS_MODSEC_VERSION" /opt/gridpane/modsec
    /usr/bin/git clone --depth 1 --single-branch -b "$(ols_branch configs)" https://gitlab+deploy-token-14:C4n3u13NWyyNKzTbtHXt@gitlab.gridpane.net/gp-public/nginx-configs /tmp/nginx-configs
    /bin/mv /tmp/nginx-configs/modsec/* /opt/gridpane/modsec
    /bin/cp /opt/gridpane/modsec/rules/REQUEST-900-EXCLUSION-RULES-BEFORE-CRS.conf.example /opt/gridpane/modsec/rules/REQUEST-900-EXCLUSION-RULES-BEFORE-CRS.conf
    /bin/cp /opt/gridpane/modsec/rules/RESPONSE-999-EXCLUSION-RULES-AFTER-CRS.conf.example /opt/gridpane/modsec/rules/RESPONSE-999-EXCLUSION-RULES-AFTER-CRS.conf
    sed -i "s|/etc/nginx/modsec/owasp|/usr/local/lsws/modsec|g" /opt/gridpane/modsec/main.conf
    /bin/cp -r /opt/gridpane/modsec /usr/local/lsws
    /bin/rm -rf /tmp/nginx-configs
    /bin/rm -f /tmp/v"$OLS_MODSEC_VERSION".tar.gz

    if [[ -n "$OLS_2" && -d /var/www/"$OLS_2"/modsec ]]; then
        ols_modsec_site
    fi
}
##
#   Fix missing site's main modsec file.
#
ols_modsec_site() {
    OLS_MODSEC_CONF=/var/www/"$OLS_2"/modsec/"$OLS_2"-main.conf

    /bin/cp /usr/local/lsws/modsec/main.conf "$OLS_MODSEC_CONF"
    /bin/sed -i "s|SecAuditLog /var/log/modsec_audit.log|SecAuditLog /var/www/${OLS_2}/modsec/modsec_audit.log|g" "$OLS_MODSEC_CONF"
    /bin/sed -i "s|#SecAuditLogStorageDir /opt/modsecurity/var/audit/|SecAuditLogStorageDir /var/www/${OLS_2}/modsec/audit-logs/audit|g" "$OLS_MODSEC_CONF"
    /bin/sed -i "s|/usr/local/lsws/modsec/crs-setup.conf|/var/www/${OLS_2}/modsec/${OLS_2}-crs-setup.conf|g" "$OLS_MODSEC_CONF"
    /bin/sed -i "s|/usr/local/lsws/modsec/crs-paranoia-level.conf|/var/www/${OLS_2}/modsec/${OLS_2}-crs-paranoia-level.conf|g" "$OLS_MODSEC_CONF"
    /bin/sed -i "s|/usr/local/lsws/modsec/crs-anomaly-blocking-threshold.conf|/var/www/${OLS_2}/modsec/${OLS_2}-crs-anomaly-blocking-threshold.conf|g" "$OLS_MODSEC_CONF"
    /bin/sed -i "s|/usr/local/lsws/modsec/runtime-whitelist.conf|/var/www/${OLS_2}/modsec/${OLS_2}-runtime-whitelist.conf|g" "$OLS_MODSEC_CONF"
    /bin/sed -i "s|/usr/local/lsws/modsec/runtime-exceptions.conf|/var/www/${OLS_2}/modsec/${OLS_2}-runtime-exceptions.conf|g" "$OLS_MODSEC_CONF"
}
