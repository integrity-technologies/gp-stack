# shellcheck disable=SC2148

##
# Template to generate OpenLiteSpeed's server configs. This function executes everytime the script runs.
#
ols_httpd() {
    /bin/echo "serverName                $(hostname)
httpdWorkers              1
user                      nobody
group                     nogroup
priority                  0
cpuAffinity               1
enableLVE                 0
inMemBufSize              60M
swappingDir               /tmp/lshttpd/swap
autoFix503                1
gracefulRestartTimeout    300
mime                      conf/mime.properties
showVersionNumber         0
enableIpGeo               1
useIpInProxyHeader        2
adminEmails               $OLS_EMAIL

errorlog logs/error.log {
  logLevel                DEBUG
  debugLevel              0
  rollingSize             10M
  enableStderrLog         1
}

accesslog logs/access.log {
  rollingSize             10M
  keepDays                30
  compressArchive         0
}
indexFiles                index.php, index.html
autoIndex                 0

expires  {
  enableExpires           1
  expiresByType           image/*=A604800,text/css=A604800,application/x-javascript=A604800,application/javascript=A604800,font/*=A604800,application/x-font-ttf=A604800
}
autoLoadHtaccess          1

tuning  {
  maxConnections          10000
  maxSSLConnections       10000
  connTimeout             300
  maxKeepAliveReq         10000
  keepAliveTimeout        5
  sndBufSize              0
  rcvBufSize              0
  maxReqURLLen            32768
  maxReqHeaderSize        65536
  maxReqBodySize          2047M
  maxDynRespHeaderSize    32768
  maxDynRespSize          2047M
  maxCachedFileSize       4096
  totalInMemCacheSize     20M
  maxMMapFileSize         256K
  totalMMapCacheSize      40M
  useSendfile             1
  fileETag                28
  enableGzipCompress      1
  compressibleTypes       application/atom+xml, application/javascript, application/json, application/ld+json, application/manifest+json, application/vnd.geo+json, application/vnd.ms-fontobject, application/x-font-opentype, application/x-font-truetype, application/x-font-ttf, application/x-javascript, application/x-web-app-manifest+json, application/xhtml+xml, application/xml, application/xml+rss, image/bmp, font/eot, font/opentype, font/otf, image/svg+xml, image/vnd.microsoft.icon, image/x-win-bitmap, image/x-icon, text/*
  enableDynGzipCompress   1
  gzipCompressLevel       6
  gzipAutoUpdateStatic    1
  gzipStaticCompressLevel 6
  brStaticCompressLevel   6
  gzipMaxFileSize         10M
  gzipMinFileSize         300

  quicEnable              1
  quicShmDir              /dev/shm
}

fileAccessControl  {
  followSymbolLink        1
  checkSymbolLink         0
  requiredPermissionMask  000
  restrictedPermissionMask 000
}

perClientConnLimit  {
  staticReqPerSec         0
  dynReqPerSec            0
  outBandwidth            0
  inBandwidth             0
  softLimit               10000
  hardLimit               10000
  gracePeriod             15
  banPeriod               300
}

CGIRLimit  {
  maxCGIInstances         20
  minUID                  11
  minGID                  10
  priority                0
  CPUSoftLimit            10
  CPUHardLimit            50
  memSoftLimit            1460M
  memHardLimit            1470M
  procSoftLimit           400
  procHardLimit           450
}

accessDenyDir  {
  dir                     /
  dir                     /etc/*
  dir                     /dev/*
  dir                     conf/*
  dir                     admin/conf/*
}

accessControl  {
  allow                   ALL, $(ols_httpd_cloudflare)
}

$(ols_php httpd)

extprocessor monit {
  type                    proxy
  address                 localhost:2812
  maxConns                100
  initTimeout             60
  retryTimeout            0
  respBuffer              0
}

scripthandler  {
  add                     lsapi:$(ols_php_latest) php
}

railsDefaults  {
  binPath                 /usr/bin/ruby
  maxConns                1
  env                     LSAPI_MAX_IDLE=60
  initTimeout             60
  retryTimeout            0
  pcKeepAliveTimeout      60
  respBuffer              0
  backlog                 50
  runOnStartUp            3
  extMaxIdleTime          300
  priority                3
  memSoftLimit            2047M
  memHardLimit            2047M
  procSoftLimit           500
  procHardLimit           600
}

wsgiDefaults  {
  maxConns                5
  env                     LSAPI_MAX_IDLE=60
  initTimeout             60
  retryTimeout            0
  pcKeepAliveTimeout      60
  respBuffer              0
  backlog                 50
  runOnStartUp            3
  extMaxIdleTime          300
  priority                3
  memSoftLimit            2047M
  memHardLimit            2047M
  procSoftLimit           500
  procHardLimit           600
}

nodeDefaults  {
  maxConns                5
  env                     LSAPI_MAX_IDLE=60
  initTimeout             60
  retryTimeout            0
  pcKeepAliveTimeout      60
  respBuffer              0
  backlog                 50
  runOnStartUp            3
  extMaxIdleTime          300
  priority                3
  memSoftLimit            2047M
  memHardLimit            2047M
  procSoftLimit           500
  procHardLimit           600
}

module cache {
  internal                1
  checkPrivateCache       1
  checkPublicCache        1
  maxCacheObjSize         10000000
  maxStaleAge             200
  qsCache                 1
  reqCookieCache          1
  respCookieCache         1
  ignoreReqCacheCtrl      1
  ignoreRespCacheCtrl     0

  enableCache             0
  expireInSeconds         3600
  enablePrivateCache      0
  privateExpireInSeconds  3600

  ls_enabled              1
}

module mod_security {
  ls_enabled              1
}

### START CORE VHOSTS

virtualhost $OLS_SUBDOMAIN {
  vhRoot                  /var/www/\$VH_NAME
  configFile              \$SERVER_ROOT/conf/vhosts/\$VH_NAME/vhconf.conf
  allowSymbolLink         1
  enableScript            1
  restrained              1
  user                    www-data
  group                   www-data
}

virtualhost $OLS_SUBDOMAIN_STAT {
  vhRoot                  /var/www/\$VH_NAME
  configFile              \$SERVER_ROOT/conf/vhosts/\$VH_NAME/vhconf.conf
  allowSymbolLink         1
  enableScript            1
  restrained              1
  user                    www-data
  group                   www-data
}

### END CORE VHOSTS

### START WORDPRESS VHOST

$(ols_httpd_vhost)

### END WORDPRESS VHOST

$(ols_httpd_listeners)

vhTemplate centralConfigLog {
  templateFile            conf/templates/ccl.conf
  listeners               https
}

vhTemplate EasyRailsWithSuEXEC {
  templateFile            conf/templates/rails.conf
  listeners               https
}
" > "$OLS_PATH_SERVER_CONF"

    if [[ "$OLS_FLAG_OUTPUT" = true && "$OLS_1" = httpd ]]; then
        ols_get httpd
    fi
}
##
#   Outputs Cloudflare IP list from file, generate file first if it doesn't exist.
#
ols_httpd_cloudflare() {
    if [[ ! -f /opt/gridpane/cf_ols.ip ]]; then
        /bin/bash /usr/local/bin/lib/cf.sh
    fi

    /bin/cat < /opt/gridpane/cf_ols.ip
}
##
# Template for http/https listeners. phpMyAdmin and monit are hardcoded by default.
#
ols_httpd_listeners() {
    if [[ ! -f "$OLS_PATH_CONF"/cert/_default/cert.pem ]]; then
        ols_init
    fi

    /bin/echo "listener http {
  address                 *:80
  secure                  0
  map                     $OLS_SUBDOMAIN $OLS_SUBDOMAIN
  map                     $OLS_SUBDOMAIN_STAT $OLS_SUBDOMAIN_STAT
$(ols_httpd_maps_http)
}

listener https {
  address                 *:443
  secure                  1
  keyFile                 ${OLS_PATH_CONF}/cert/_default/key.pem
  certFile                ${OLS_PATH_CONF}/cert/_default/cert.pem
  certChain               1
  map                     $OLS_SUBDOMAIN $OLS_SUBDOMAIN
  map                     $OLS_SUBDOMAIN_STAT $OLS_SUBDOMAIN_STAT
$(ols_httpd_maps_https)
}"
}
##
# Generate maps for http listener.
#
ols_httpd_maps_http() {
    OLS_MAPS_HTTP_DEFAULT=
    OLS_MAPS_HTTP_EXTRAS=
    OLS_MAPS_HTTP_ROUTE=

    # shellcheck disable=SC2153
    cd "$OLS_PATH_VHOSTS" || exit

    for OLS_MAPS_HTTP_I in *; do
        [[ "$OLS_MAPS_HTTP_I" = "$OLS_SUBDOMAIN" || "$OLS_MAPS_HTTP_I" = "$OLS_SUBDOMAIN_STAT" ]] && continue

        # Multisite or wildcard
        if [[   -f /var/www/"$OLS_MAPS_HTTP_I"/logs/"$OLS_MAPS_HTTP_I".env &&
                "$(ols_env multisite "$OLS_MAPS_HTTP_I")" = sub-domain ||
                "$(ols_env multisite "$OLS_MAPS_HTTP_I")" = subdomain ||
                "$(ols_check_wildcard "$OLS_MAPS_HTTP_I")" = true ]]; then
            OLS_MAPS_HTTP_EXTRAS+=", *.${OLS_MAPS_HTTP_I}"
        else
            OLS_MAPS_HTTP_EXTRAS=
        fi

        OLS_MAPS_HTTP_DEFAULT="$(ols_check_default)"
        if [[ "$OLS_MAPS_HTTP_I" = "$OLS_MAPS_HTTP_DEFAULT" ]]; then
            OLS_MAPS_HTTP_EXTRAS=", $(ols_server_ip)"
        fi

        OLS_MAPS_HTTP_ROUTE="$(ols_get "$OLS_MAPS_HTTP_I" route)"
        case "$OLS_MAPS_HTTP_ROUTE" in
            root|route-root)
                /bin/echo "  map                     $OLS_MAPS_HTTP_I ${OLS_MAPS_HTTP_I}${OLS_MAPS_HTTP_EXTRAS}"
            ;;
            www|route-www)
                /bin/echo "  map                     $OLS_MAPS_HTTP_I www.${OLS_MAPS_HTTP_I}${OLS_MAPS_HTTP_EXTRAS}"
            ;;
            *)
                OLS_MAPS_HTTP_EXTRAS+=", www.${OLS_MAPS_HTTP_I}"
                /bin/echo "  map                     $OLS_MAPS_HTTP_I ${OLS_MAPS_HTTP_I}${OLS_MAPS_HTTP_EXTRAS}"
            ;;
        esac
    done
}
##
# Generate maps for https listener.
#
ols_httpd_maps_https() {
    OLS_MAPS_HTTPS_DEFAULT=
    OLS_MAPS_HTTPS_EXTRAS=
    OLS_MAPS_HTTPS_ROUTE=

    cd "$OLS_PATH_VHOSTS" || exit

    for OLS_MAPS_HTTPS_I in *; do
        [[  "$OLS_MAPS_HTTPS_I" = "$OLS_SUBDOMAIN" ||
            "$OLS_MAPS_HTTPS_I" = "$OLS_SUBDOMAIN_STAT"
        ]] && continue

        if [[ -n "$(ols_check_ssl_file "$OLS_MAPS_HTTPS_I")" ]]; then
            # Multisite or wildcard
            if [[   -f /var/www/"$OLS_MAPS_HTTPS_I"/logs/"$OLS_MAPS_HTTPS_I".env &&
                    "$(ols_env multisite "$OLS_MAPS_HTTPS_I")" = sub-domain ||
                    "$(ols_env multisite "$OLS_MAPS_HTTPS_I")" = subdomain ||
                    "$(ols_check_wildcard "$OLS_MAPS_HTTPS_I")" = true ]]; then
                OLS_MAPS_HTTPS_EXTRAS+=", *.${OLS_MAPS_HTTPS_I}"
            else
                OLS_MAPS_HTTP_EXTRAS=
            fi

            OLS_MAPS_HTTPS_DEFAULT="$(ols_check_default)"
            if [[ "$OLS_MAPS_HTTPS_I" = "$OLS_MAPS_HTTPS_DEFAULT" ]]; then
                OLS_MAPS_HTTPS_EXTRAS=", $(ols_server_ip)"
            fi

            OLS_MAPS_HTTPS_ROUTE="$(ols_get "$OLS_MAPS_HTTPS_I" route)"
            case "$OLS_MAPS_HTTPS_ROUTE" in
                root|route-root)
                    /bin/echo "  map                     $OLS_MAPS_HTTPS_I ${OLS_MAPS_HTTPS_I}${OLS_MAPS_HTTPS_EXTRAS}"
                ;;
                www|route-www)
                    /bin/echo "  map                     $OLS_MAPS_HTTPS_I www.${OLS_MAPS_HTTPS_I}${OLS_MAPS_HTTPS_EXTRAS}"
                ;;
                *)
                    OLS_MAPS_HTTPS_EXTRAS+=", www.${OLS_MAPS_HTTPS_I}"
                    /bin/echo "  map                     $OLS_MAPS_HTTPS_I ${OLS_MAPS_HTTPS_I}${OLS_MAPS_HTTPS_EXTRAS}"
                ;;
            esac
        fi
    done
}
##
# Template for vhosts.
#
ols_httpd_vhost() {
    cd /var/www || exit

    for OLS_VHOST_I in *; do
        if [[ -f /var/www/"$OLS_VHOST_I"/wp-config.php ]]; then
            /bin/echo "virtualhost $OLS_VHOST_I {
  vhRoot                  /var/www/\$VH_NAME
  configFile              \$SERVER_ROOT/conf/vhosts/\$VH_NAME/vhconf.conf
  allowSymbolLink         1
  enableScript            1
  restrained              1
  user                    $(ols_get "$OLS_VHOST_I" user)
  group                   $(ols_get "$OLS_VHOST_I" group)
}
"

            # Addon vhosts
            if [[ -f /var/www/"$OLS_VHOST_I"/logs/"$OLS_VHOST_I"-additional-domains.env ]]; then
                OLS_VHOST_ADDONS="$(/bin/cat < /var/www/"$OLS_VHOST_I"/logs/"$OLS_VHOST_I"-additional-domains.env)"

                for OLS_VHOST_ADDON in $OLS_VHOST_ADDONS; do
                    OLS_VHOST_ADDON="${OLS_VHOST_ADDON//:/}"

                    /bin/echo "virtualhost $OLS_VHOST_ADDON {
  vhRoot                  /var/www/${OLS_VHOST_I}
  configFile              \$SERVER_ROOT/conf/vhosts/\$VH_NAME/vhconf.conf
  allowSymbolLink         1
  enableScript            1
  restrained              1
  user                    $(ols_get "$OLS_VHOST_I" user)
  group                   $(ols_get "$OLS_VHOST_I" group)
}
"
                done
            fi

            # Redirect vhosts
            if [[ -f /var/www/"$OLS_VHOST_I"/logs/"$OLS_VHOST_I"-301-domains.env ]]; then
                OLS_VHOST_REDIRECTS="$(/bin/cat < /var/www/"$OLS_VHOST_I"/logs/"$OLS_VHOST_I"-301-domains.env)"

                for OLS_VHOST_REDIRECT in $OLS_VHOST_REDIRECTS; do
                    OLS_VHOST_REDIRECT="${OLS_VHOST_REDIRECT//:/}"

                    /bin/echo "virtualhost $OLS_VHOST_REDIRECT {
  vhRoot                  /var/www/${OLS_VHOST_I}
  configFile              \$SERVER_ROOT/conf/vhosts/\$VH_NAME/vhconf.conf
  allowSymbolLink         1
  enableScript            1
  restrained              1
  user                    $(ols_get "$OLS_VHOST_I" user)
  group                   $(ols_get "$OLS_VHOST_I" group)
}
"
                done
            fi
        fi
    done
}
