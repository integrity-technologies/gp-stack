# shellcheck disable=SC2148

##
# Get lsphp/php version from configuration file instead of site's .env file.
# Can also get values from site's .env file.
#
ols_get() {
    if [[ -n "$OLS_FLAG_PRIMARY" ]]; then
        OLS_2="$OLS_FLAG_PRIMARY"
    else
        OLS_2="${1:-$OLS_2}"
    fi

    OLS_GET="${2:-$1}"

    #ols_check_wp "$OLS_2"

    case "$OLS_GET" in
        cache)
            OLS_GET_CACHE_TYPE="$(/bin/grep cache-type /var/www/"$OLS_2"/logs/"$OLS_2".env || true)"

            if [[ -n "$OLS_GET_CACHE_TYPE" ]]; then
                /bin/echo "${OLS_GET_CACHE_TYPE//cache-type:/}"
            else
                /bin/echo php
            fi
        ;;
        env)
            if [[ -f /var/www/"$OLS_2"/logs/"$OLS_2".env ]]; then
                /bin/cat < /var/www/"$OLS_2"/logs/"$OLS_2".env
            fi
        ;;
        httpd)
            /bin/cat "$OLS_PATH_SERVER_CONF"
        ;;
        lsphp)
            if [[ -f /var/www/"$OLS_2"/logs/"$OLS_2".env ]]; then
                OLS_GET_LSPHP="$(/bin/grep php-ver: /var/www/"$OLS_2"/logs/"$OLS_2".env)"
                OLS_GET_LSPHP="${OLS_GET_LSPHP//php-ver:/}"
                /bin/echo "lsphp${OLS_GET_LSPHP//./}"
            fi
        ;;
        php)
            if [[ -f /var/www/"$OLS_2"/logs/"$OLS_2".env ]]; then
                OLS_GET_PHP="$(/bin/grep php-ver: /var/www/"$OLS_2"/logs/"$OLS_2".env)"
                /bin/echo "${OLS_GET_PHP//php-ver:/}"
            fi
        ;;
        proto)
            if [[ -n "$(ols_check_ssl_file "$OLS_2")" ]]; then
                /bin/echo https
            else
                /bin/echo http
            fi
        ;;
        route)
            if [[ -f "$OLS_PATH_VHOSTS"/"$OLS_2"/vhconf.conf ]]; then
                OLS_ROUTE="$(/bin/grep "# route-" "$OLS_PATH_VHOSTS"/"$OLS_2"/vhconf.conf || true)"
            else
                OLS_ROUTE="$(/bin/grep route: /var/www/"$OLS_2"/logs/"$OLS_2".env)"
                OLS_ROUTE="${OLS_ROUTE//route:/route-}"
            fi

            if [[ "$OLS_ROUTE" == *"route-root"* ]]; then
                /bin/echo root
            elif [[ "$OLS_ROUTE" == *"route-www"* ]]; then
                /bin/echo www
            else
                /bin/echo none
            fi
        ;;
        sock)
            OLS_GET_SOCK_FILE="$(ols_get "$OLS_2" lsphp).sock"
            OLS_GET_SOCK_GET="$(/usr/bin/find /tmp/lshttpd -name "$OLS_GET_SOCK_FILE")"

            if [[ -n "$OLS_GET_SOCK_GET" ]]; then
                /bin/echo "$OLS_GET_SOCK_FILE"
            else
                ols_echo "Sock file not found."
            fi
        ;;
        user|group)
            if [[ -f /var/www/"$OLS_2"/logs/"$OLS_2".env ]]; then
                ols_env sys-user
            else
                if [[ "$OLS_GET" = user ]]; then
                    /usr/bin/stat -c '%U' /var/www/"$OLS_2"
                else
                    /usr/bin/stat -c '%G' /var/www/"$OLS_2"
                fi
            fi
        ;;
        vhconf)
            if [[ "$OLS_2" = pma ]]; then
                OLS_GET_VHCONF="$OLS_PATH_VHOSTS"/"$OLS_SUBDOMAIN"/vhconf.conf
            elif [[ "$OLS_2" = stats ]]; then
                OLS_GET_VHCONF="$OLS_PATH_VHOSTS"/"$OLS_SUBDOMAIN_STAT"/vhconf.conf
            else
                OLS_GET_VHCONF="$OLS_PATH_VHOSTS"/"$OLS_2"/vhconf.conf
            fi

            if [[ -f "$OLS_GET_VHCONF" ]]; then
                /bin/cat < "$OLS_GET_VHCONF"
            fi
        ;;
        wildcard)
            ols_check_wildcard
        ;;
        *)
            OLS_GET_ENV="$(ols_env "$OLS_GET")"

            if [[ -n "$OLS_GET_ENV" ]]; then
                /bin/echo "$OLS_GET_ENV"
            else
                ols_get "$OLS_2" vhconf
            fi
        ;;
    esac
}
