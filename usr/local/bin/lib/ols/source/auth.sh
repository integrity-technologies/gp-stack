# shellcheck disable=SC2148

##
# Generates templates for basic auth.
#
ols_auth() {
    # Generate .htpasswd if it doesn't exist
    if [[ ! -f "$OLS_PATH_CONF"/.htpasswd && -f /root/gridhttp.auth ]]; then
        /bin/echo "gridpane:$(/bin/cat < /root/gridhttp.auth)" > "$OLS_PATH_CONF"/.htpasswd
    fi

    OLS_AUTH="$1"
    if [[ "$OLS_FLAG_AUTH" = true || -f /var/www/"$OLS_2"/logs/http.auth.env || "$(ols_env http-auth)" = true ]]; then
        if [[ "$OLS_FLAG_AUTH" != false ]]; then
            if [[ "$OLS_AUTH" = context ]]; then
                /bin/echo "realm                   Basic Auth"
            elif [[ "$OLS_AUTH" = realm ]]; then
                /bin/echo "realm Basic Auth {
  userDB {
    location ${OLS_PATH_CONF}/.htpasswd
  }
}"
            fi
        fi
    fi
}
