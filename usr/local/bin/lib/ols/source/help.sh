# shellcheck disable=SC2148

##
# Template for help menu.
#
ols_help() {
    /bin/echo
    /bin/echo "gpols <command> <flags>                   Generator for OpenLiteSpeed configs"
    /bin/echo
    /bin/echo "      addon                               Generate configs for addon domains, requires --primary flag"
    /bin/echo
    /bin/echo "      build                               Output build date when it was last pushed to repo"
    /bin/echo
    /bin/echo "      delete                              Delete configs: pma, stats, domain.tld"
    /bin/echo
    /bin/echo "      get                                 Gets lsphp/php versions from vhconf.conf and site's .env file or server config"
    /bin/echo "                                          Available options: httpd, lsphp, php, route, sock, vhconf, wildcard, or any key in .env file"
    /bin/echo
    /bin/echo "      help                                This help menu"
    /bin/echo
    /bin/echo "      httpd                               Only regenerate main OpenLiteSpeed config"
    /bin/echo
    /bin/echo "      init                                Create main directories and configs"
    /bin/echo
    /bin/echo "      log                                 Show logs from site or server, defaults to error logs"
    /bin/echo "                                          Available options: httpd, domain.tld, --access|-a, --follow|-f"
    /bin/echo
    /bin/echo "      pma                                 Generate configs for phpMyAdmin"
    /bin/echo
    /bin/echo "      redirect                            Generate configs for redirect domains, requires --primary flag"
    /bin/echo
    /bin/echo "      site                                Generate configs for WordPress"
    /bin/echo
    /bin/echo "      stats                               Generate configs for Monit"
    /bin/echo
    /bin/echo "      version                             Upgrade/downgrade the OLS verion"
    /bin/echo "                                          Usage: gpols version 1.6.18, gpols version list"
    /bin/echo
    /bin/echo "      --access                            Show access log"
    /bin/echo
    /bin/echo "      --auth, --auth=true                 Enables basic auth"
    /bin/echo "      --auth=false                        Disables basic auth"
    /bin/echo
    /bin/echo "      --cache                             Configures cache options in LSCache WordPress plugin"
    /bin/echo "                                          Accepted value: redis, redis-off, page, page-off, page-only, php|off"
    /bin/echo
    /bin/echo "      --follow|-f                         Follow output of logs"
    /bin/echo
    /bin/echo "      --multisite=sub-directory           Configure WordPress for sub directory multisite"
    /bin/echo "      --multisite=sub-domain              Configure WordPress for sub domain multisite"
    /bin/echo
    /bin/echo "      --no-restart                        Don't restart the lshttpd service"
    /bin/echo
    /bin/echo "      --output                            Output the config file"
    /bin/echo
    /bin/echo "      --php=7.x                           Switch php versions (7.2 - 7.4), will default to 7.3"
    /bin/echo
    /bin/echo "      --primary                           Required flag for addon/redirect vhconf"
    /bin/echo
    /bin/echo "      --quiet|-q                          Do not output to stdout"
    /bin/echo
    /bin/echo "      --route                             Change mappings based on domain route"
    /bin/echo "                                          Accepted values: none, root, www"
    /bin/echo
    /bin/echo "      --ssl, --ssl=true                   Enable SSL"
    /bin/echo "      --ssl=false                         Disable SSL"
    /bin/echo
    /bin/echo "      --wildcard, --wildcard=true         Enables wildcard for a domain"
    /bin/echo "      --wildcard=false                    Disables wildcard for a domain"
    /bin/echo

    exit
}
