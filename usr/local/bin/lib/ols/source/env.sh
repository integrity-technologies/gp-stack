# shellcheck disable=SC2148

##
# Returns a site's value from site's env file.
#
ols_env() {
    OLS_ENV="${1:-}"

    if [[ -n "$OLS_FLAG_PRIMARY" ]]; then
        OLS_ENV_2="$OLS_FLAG_PRIMARY"
    else
        OLS_ENV_2="${2:-$OLS_2}"
    fi

    if [[ -f /var/www/"$OLS_ENV_2"/logs/"$OLS_ENV_2".env ]]; then
        OLS_ENV_GREP="$(/bin/grep "$OLS_ENV": /var/www/"$OLS_ENV_2"/logs/"$OLS_ENV_2".env)"
        /bin/echo "$OLS_ENV_GREP" | /usr/bin/awk -F '[:]' '{print $2}'
    fi
}
