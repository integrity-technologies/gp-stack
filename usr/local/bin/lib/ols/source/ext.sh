# shellcheck disable=SC2148


##
# Template to generate lsphp extprocessor.
#
ols_php() {
    OLS_PHP="${1:-}"
    OLS_PHP_LSPHP_SYNC=
    OLS_PHP_LSPHP_VERSION=
    OLS_PHP_APP_VERSION=

    if [[ "$OLS_PHP" = httpd ]]; then
        OLS_PHP_APP_INSTANCES=1
        OLS_PHP_CHILDREN=35
        OLS_PHP_CHILDREN_DIVIDE="$(/bin/echo "scale=2; $OLS_PHP_CHILDREN / 3" | /usr/bin/bc)"
        OLS_PHP_EXT_PROCESSOR="$(ols_php_latest)"
        OLS_PHP_INITIAL_REQUEST_TIMEOUT=60
        OLS_PHP_LSPHP="$OLS_PHP_EXT_PROCESSOR"
        OLS_PHP_MAX_CONNECTIONS=35
        OLS_PHP_MAX_IDLE=300
        OLS_PHP_MAX_REQUESTS=10000
        OLS_PHP_RETRY_TIMEOUT=0
        OLS_PHP_LSPHP_PATH="$OLS_PATH_DIR"/"$OLS_PHP_LSPHP"/bin/lsphp
    else
        # Cleanup
        /bin/rm -f /tmp/lshttpd/"$OLS_2"-*

        OLS_PHP_MODE="$(ols_env lsapi)"

        if [[ "$OLS_PHP_MODE" = worker ]]; then
            OLS_PHP_CHILDREN=1
            OLS_PHP_APP_INSTANCES="$(ols_env lsapi_max_connections)"
        else
            OLS_PHP_CHILDREN="$(ols_env lsapi_children)"
            OLS_PHP_APP_INSTANCES="$(ols_env lsapi_app_instances)"
        fi

        OLS_PHP_LSPHP="$OLS_VHCONF_LSPHP"
        OLS_PHP_EXT_PROCESSOR="$OLS_2"-"$OLS_PHP_LSPHP"
        OLS_PHP_MAX_CONNECTIONS="$(ols_env lsapi_max_connections)"
        OLS_PHP_MAX_IDLE="$(ols_env lsapi_max_idle)"
        OLS_PHP_MAX_REQUESTS="$(ols_env lsapi_max_reqs)"
        OLS_PHP_CHILDREN_DIVIDE="$(/bin/echo "scale=2; $OLS_PHP_CHILDREN / 3" | /usr/bin/bc)"
        OLS_PHP_INITIAL_REQUEST_TIMEOUT="$(ols_env lsapi_initial_request_timeout)"
        OLS_PHP_RETRY_TIMEOUT="$(ols_env lsapi_retry_timeout)"

        # Make sure this path exists with fallbacks
        OLS_PHP_LSPHP_PATH="$OLS_PATH_DIR"/"$OLS_PHP_LSPHP"/bin/lsphp
        if [[ ! -f "$OLS_PHP_LSPHP_PATH" ]]; then
            OLS_PHP_LSPHP_SYNC=true
            OLS_PHP_LSPHP_VERSION="$OLS_VHCONF_LSPHP"
        fi

        # Second fallback check
        if [[ ! -f "$OLS_PHP_LSPHP_PATH" ]]; then
            OLS_PHP_LSPHP_ENV="$(ols_env php-ver)"
            OLS_PHP_LSPHP_VERSION=lsphp"${OLS_PHP_LSPHP_ENV/./}"
            OLS_PHP_LSPHP_SYNC=true
        fi

        # Final fallback, default to lsphp73
        if [[ ! -f "$OLS_PHP_LSPHP_PATH" ]]; then
            OLS_PHP_LSPHP_SYNC=true
            OLS_PHP_LSPHP_VERSION="$(ols_php_latest)"
        fi

        # Sync version to app and site .env
        if [[ "$OLS_PHP_LSPHP_SYNC" = true ]]; then
            /usr/bin/chattr -i /var/www/"$OLS_2"/logs/"$OLS_2".env

            if [[ "$OLS_PHP_LSPHP_VERSION" == *"lsphp"* ]]; then
                OLS_PHP_LSPHP_VERSION="${OLS_PHP_LSPHP_VERSION/lsphp/}"
                OLS_VHCONF_LSPHP="$OLS_PHP_LSPHP_VERSION"
                OLS_PHP_APP_VERSION="${OLS_PHP_LSPHP_VERSION:0:1}"."${OLS_PHP_LSPHP_VERSION:1:1}"
            else
                OLS_PHP_APP_VERSION="$OLS_PHP_LSPHP_VERSION"
                OLS_VHCONF_LSPHP=lsphp"${OLS_PHP_LSPHP_VERSION/./}"
            fi

            OLS_PHP_LSPHP_PATH="$OLS_PATH_DIR"/"$OLS_VHCONF_LSPHP"/bin/lsphp
            OLS_PHP_EXT_PROCESSOR="$OLS_2"-"$OLS_VHCONF_LSPHP"

            /bin/sed -i "s|php-ver:.*|php-ver:${OLS_PHP_APP_VERSION}|g" /var/www/"$OLS_2"/logs/"$OLS_2".env
            /usr/bin/chattr +i /var/www/"$OLS_2"/logs/"$OLS_2".env

            /usr/bin/curl -sL \
                -d "{\"site_url\":\"${OLS_2}\",\"server_ip\":\"$(/bin/cat < /root/server.ip)\",\"php_version\":\"${OLS_PHP_APP_VERSION}\"}" \
                -H "Content-Type: application/json" \
                -X POST "https://$(/bin/cat < /root/grid.source)/api/site/site-update?api_token=$(/bin/cat < /root/gridpane.token)" &> /dev/null
        fi
    fi

    # Round the value to nearest
    OLS_PHP_CHILDREN_DIVIDE="$(/usr/bin/printf "%.0f\n" "$OLS_PHP_CHILDREN_DIVIDE")"

    /bin/echo "extprocessor $OLS_PHP_EXT_PROCESSOR {
  type                    lsapi
  address                 uds://tmp/lshttpd/${OLS_PHP_EXT_PROCESSOR}.sock
  maxConns                $OLS_PHP_MAX_CONNECTIONS
  env                     LSAPI_AVOID_FORK=0
  env                     LSAPI_CHILDREN=$OLS_PHP_CHILDREN
  env                     LSAPI_EXTRA_CHILDREN=$OLS_PHP_CHILDREN_DIVIDE
  env                     LSAPI_MAX_REQS=$OLS_PHP_MAX_REQUESTS
  env                     LSAPI_MAX_IDLE=$OLS_PHP_MAX_IDLE
  env                     LSAPI_MAX_IDLE_CHILDREN=$OLS_PHP_CHILDREN_DIVIDE
  env                     LSPHP_ENABLE_USER_INI=on
  initTimeout             $OLS_PHP_INITIAL_REQUEST_TIMEOUT
  retryTimeout            $OLS_PHP_RETRY_TIMEOUT
  persistConn             1
  respBuffer              0
  autoStart               1
  path                    $OLS_PHP_LSPHP_PATH
  backlog                 100
  instances               $OLS_PHP_APP_INSTANCES
  runOnStartUp            3
  extMaxIdleTime          10
  priority                0
  memSoftLimit            2047M
  memHardLimit            2047M
  procSoftLimit           400
  procHardLimit           500
}
"
}
