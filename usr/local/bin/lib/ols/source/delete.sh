# shellcheck disable=SC2148

##
# Delete vhost directory.
#
ols_delete() {
    OLS_DELETE="${1:-}"

    case "$OLS_DELETE" in
        pma)
            /bin/rm -rf "${OLS_PATH_VHOSTS:?}"/"$OLS_SUBDOMAIN"
            ols_echo "[delete] $OLS_SUBDOMAIN"
        ;;
        stats)
            /bin/rm -rf "${OLS_PATH_VHOSTS:?}"/"$OLS_SUBDOMAIN_STAT"
            ols_echo "[delete] $OLS_SUBDOMAIN_STAT"
        ;;
        *)
            OLS_DELETE_SOCK="$(/usr/bin/find /tmp/lshttpd -name "${OLS_DELETE}*")"

            if [[ -n "$OLS_DELETE_SOCK" ]]; then
                for OLS_DELETE_I in $OLS_DELETE_SOCK; do
                    /bin/rm -f "$OLS_DELETE_I"
                done
            fi

            /bin/rm -rf "${OLS_PATH_VHOSTS:?}"/"$OLS_DELETE"
            /bin/rm -f /var/log/ols/"$OLS_DELETE"*

            ols_echo "[delete] $OLS_DELETE"
        ;;
    esac
}
