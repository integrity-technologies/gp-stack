# shellcheck disable=SC2148

##
# Creates needed directories/files they're missing.
#
ols_init() {
    # Remove Example vhroot
    if [[ -d "$OLS_PATH_DIR"/Example ]]; then
        /bin/rm -rf "$OLS_PATH_DIR"/Example
        ols_echo "[init] Deleted ${OLS_PATH_DIR}/Example"
    fi

    # Remove Example vhost
    if [[ -d "$OLS_PATH_VHOSTS"/Example ]]; then
        /bin/rm -rf "$OLS_PATH_VHOSTS"/Example
        ols_echo "[init] Deleted ${OLS_PATH_VHOSTS}/Example"
    fi

    if [[ ! -d /var/log/ols ]]; then
        /bin/mkdir -p /var/log/ols
    fi

    # Symlink main server logs to /var/log/ols
    if [[ ! -f /var/log/ols/ols.access.log || ! -f /usr/local/lsws/logs/error.log ]]; then
        /bin/ln -sf /usr/local/lsws/logs/access.log /var/log/ols/ols.access.log
        /bin/ln -sf /usr/local/lsws/logs/error.log /var/log/ols/ols.error.log
    fi

    if [[ ! -d "$OLS_PATH_CONF"/backup ]]; then
        /bin/mkdir -p "$OLS_PATH_CONF"/backup
        ols_echo "[init] Created ${OLS_PATH_CONF}/backup"
    fi

    if [[ ! -f "$OLS_PATH_CONF"/cert/_default/cert.pem ]]; then
        /bin/mkdir -p "$OLS_PATH_CONF"/cert/_default
        ols_echo "[init] Created ${OLS_PATH_CONF}/cert/_default"

        /usr/bin/openssl req -newkey rsa:4096 \
            -x509 \
            -sha256 \
            -days 36500 \
            -nodes \
            -out "$OLS_PATH_CONF"/cert/_default/cert.pem \
            -keyout "$OLS_PATH_CONF"/cert/_default/key.pem \
            -subj "/C=US/ST=Michigan/L=Bath/O=GridPane/OU=Dev/CN=_default"
    fi

    if [[ -n "$OLS_SUBDOMAIN" ]]; then
        [[ ! -f "$OLS_PATH_VHOSTS"/"$OLS_SUBDOMAIN"/vhconf.conf ]] && /bin/mkdir -p "$OLS_PATH_VHOSTS"/"$OLS_SUBDOMAIN"
        ols_vhconf_pma
    fi

    if [[ -n "$OLS_SUBDOMAIN_STAT" ]]; then
        [[ ! -f "$OLS_PATH_VHOSTS"/"$OLS_SUBDOMAIN_STAT"/vhconf.conf ]] && /bin/mkdir -p "$OLS_PATH_VHOSTS"/"$OLS_SUBDOMAIN_STAT"
        ols_vhconf_stats
    fi

    if [[ ! -f "$OLS_PATH_CONF"/.htpasswd && -f /root/gridhttp.auth ]]; then
        cp /root/gridhttp.auth "$OLS_PATH_CONF"/.htpasswd
        ols_echo "[init] Created ${OLS_PATH_CONF}/.htpasswd"
    else
        [[ ! -f /root/gridhttp.auth ]] && ols_echo "[init] /root/gridhttp.auth is missing, broken install? Skipping ..."
    fi
}
