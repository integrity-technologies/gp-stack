# shellcheck disable=SC2148

##
# Retrieve gitlab branch.
#
ols_branch() {
    OLS_BRANCH="${1:-}"

    if [[ -f /root/"$OLS_BRANCH".development.test.branch ]]; then
        /bin/cat < /root/"$OLS_BRANCH".development.test.branch
    else
        /bin/echo master
    fi
}
##
# Returns message to stdout with prepended text.
#
ols_echo() {
    OLS_ECHO="$1"

    if [[ -z "$OLS_FLAG_QUIET" ]]; then
        /bin/echo "[$(/bin/date +%F-%T)] [gpols] $OLS_ECHO" | /usr/bin/tee -a /var/log/gridpane.log
    fi
}
##
# Returns error message to stdout with prepended text.
#
ols_error() {
    OLS_ERROR="$1"
    OLS_ERROR_LOG_ONLY="${2:-}"

    if [[ "$OLS_ERROR_LOG_ONLY" = true ]]; then
        /bin/echo -e "[$(/bin/date +%F-%T)] [gpols] [error] $OLS_ERROR" >> /var/log/gridpane.log
    else
        /bin/echo -e "[$(/bin/date +%F-%T)] [gpols] [error] $OLS_ERROR" | /usr/bin/tee -a /var/log/gridpane.log
    fi

    exit 1
}
##
# Get lsphp latest version
#
ols_php_latest() {
    # TEMPORARY
    # Hardcode lsphp73 for now

    /bin/echo lsphp73

    #for OLS_PHP_LATEST_I in $(dir "$OLS_PATH_DIR"); do
    #    if [[ "$OLS_PHP_LATEST_I" == "lsphp"* ]]; then
    #        OLS_PHP_LATEST+="$OLS_PHP_LATEST_I\\n"
    #    fi
    #done

    #/bin/echo -e "$OLS_PHP_LATEST" | /usr/bin/sort | /usr/bin/tail -1
}
##
# Performs a chown command for multiple core directories. Gets executed everytime the script runs.
#
ols_reset() {
    # Reset permissions/ownership
    chown -R lsadm:nogroup "$OLS_PATH_CONF"/httpd_config.conf \
        "$OLS_PATH_CONF"/cert \
        "$OLS_PATH_VHOSTS" \
        "$OLS_PATH_CONF"/backup \
        "$OLS_PATH_CONF"/.htpasswd
}
##
# Executes a safe restart via lswsctl and auto start lshttpd.
#
# lswsctl will "break" the status of systemctl status lshttpd
# and will show a failed message due to PID mismatch. This
# doesn't actually break OpenLiteSpeed's webserver and will
# continue to run.
#
#
ols_restart() {
    OLS_FLAG_RESTART_LSWS="$(/usr/local/lsws/bin/lswsctrl restart)"

    if [[ -z "$OLS_FLAG_QUIET" ]]; then
        ols_echo "[httpd] $OLS_FLAG_RESTART_LSWS"
    fi

    # Disable restarting the entire service for now
    #OLS_RESTART_SERVICE_CHECK="$(/bin/systemctl status lshttpd --no-pager; /bin/echo "$?")"
    #if [[ "$OLS_RESTART_SERVICE_CHECK" != 0 ]]; then
    #    /bin/systemctl start lshttpd &
    #fi
}
##
#   Gets server IP with fallback.
#
ols_server_ip() {
    if [[ -f /root/server.ip ]]; then
        /bin/cat < /root/server.ip
    else
        /usr/bin/curl http://ip4.ident.me 2>/dev/null
    fi
}
##
#   Wraps wp-cli commands.
#
ols_wpcli() {
    OLS_WPCLI_SITE="${1:-}"
    OLS_WPCLI_COMMAND="${2:-}"
    OLS_WPCLI_USER="$(ols_get "$OLS_2" user)"

    /usr/bin/sudo su - "$OLS_WPCLI_USER" -c "timeout 30 /usr/local/bin/wp $OLS_WPCLI_COMMAND --path=/var/www/${OLS_WPCLI_SITE}/htdocs 2>&1" || true
}
