##GridPane_Prometheus_vHost_Begin##
#HTTPS-WWW-ADD-ON-301#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

server {
    listen 80;
    listen [::]:80;

    server_name completelegaldefenseteam.com www.completelegaldefenseteam.com;

    include /etc/nginx/common/headers-http.conf;

    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name completelegaldefenseteam.com www.completelegaldefenseteam.com;

    ssl_certificate /etc/letsencrypt/live/completelegaldefenseteam.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/completelegaldefenseteam.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/completelegaldefenseteam.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/completelegaldefenseteam.com/chain.pem;

    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;

    return 301 https://www.criminallawyermyrtlebeach.com$request_uri;
}
##GridPane_Prometheus_vHost_End##
