##GridPane_Prometheus_vHost_Begin##
#HTTP#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/shopindigo.indigoag.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name shopindigo.indigoag.com www.shopindigo.indigoag.com;

    access_log /var/log/nginx/shopindigo.indigoag.com.access.log we_log;
    error_log /var/log/nginx/shopindigo.indigoag.com.error.log;

    root /var/www/shopindigo.indigoag.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /var/www/shopindigo.indigoag.com/nginx/*shopindigo.indigoag.com-sockfile.conf;
    include /etc/nginx/common/shopindigo.indigoag.com-compression.conf;
    include /var/www/shopindigo.indigoag.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/shopindigo.indigoag.com/nginx/shopindigo.indigoag.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/shopindigo.indigoag.com/nginx/*-main-shopindigo.indigoag.com-context.conf;
    include /var/www/shopindigo.indigoag.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/shopindigo.indigoag.com-sitemap.conf;
    include /etc/nginx/common/shopindigo.indigoag.com-static.conf;
    include /etc/nginx/common/shopindigo.indigoag.com-wpcommon.conf;
    include /etc/nginx/common/shopindigo.indigoag.com-wp-redis.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
