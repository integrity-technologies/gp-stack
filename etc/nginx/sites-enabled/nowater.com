##GridPane_Prometheus_vHost_Begin##
#HTTPS-WWW#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/nowater.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name www.nowater.com;

    #include /var/www/nowater.com/nginx/nowater.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /var/www/nowater.com/nginx/*-return-https-context.conf;

    return 301 https://www.nowater.com$request_uri;
}

server {
    listen 80;
    listen [::]:80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name nowater.com;

    ssl_certificate /etc/letsencrypt/live/nowater.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/nowater.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/nowater.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/nowater.com/chain.pem;

    #include /var/www/nowater.com/nginx/nowater.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /var/www/nowater.com/nginx/*-return-www-context.conf;

    return 301 https://www.nowater.com$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.nowater.com;

    ssl_certificate /etc/letsencrypt/live/nowater.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/nowater.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/nowater.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/nowater.com/chain.pem;

    access_log /var/log/nginx/nowater.com.access.log we_log;
    error_log /var/log/nginx/nowater.com.error.log;

    root /var/www/nowater.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    include /etc/nginx/common/nowater.com-6g.conf;
    include /var/www/nowater.com/nginx/*nowater.com-sockfile.conf;
    include /etc/nginx/common/nowater.com-compression.conf;
    include /var/www/nowater.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/nowater.com/nginx/nowater.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/nowater.com/nginx/*-main-nowater.com-context.conf;
    include /var/www/nowater.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/nowater.com-sitemap.conf;
    include /etc/nginx/common/nowater.com-static.conf;
    include /etc/nginx/common/nowater.com-wpcommon.conf;
    include /etc/nginx/common/nowater.com-php.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
