##GridPane_Prometheus_vHost_Begin##
#HTTPS-WWW#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/freedmarcroft.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name www.freedmarcroft.com;

    #include /var/www/freedmarcroft.com/nginx/freedmarcroft.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /var/www/freedmarcroft.com/nginx/*-return-https-context.conf;

    return 301 https://www.freedmarcroft.com$request_uri;
}

server {
    listen 80;
    listen [::]:80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name freedmarcroft.com;

    ssl_certificate /etc/letsencrypt/live/freedmarcroft.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/freedmarcroft.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/freedmarcroft.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/freedmarcroft.com/chain.pem;

    #include /var/www/freedmarcroft.com/nginx/freedmarcroft.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /var/www/freedmarcroft.com/nginx/*-return-www-context.conf;

    return 301 https://www.freedmarcroft.com$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.freedmarcroft.com;

    ssl_certificate /etc/letsencrypt/live/freedmarcroft.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/freedmarcroft.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/freedmarcroft.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/freedmarcroft.com/chain.pem;

    access_log /var/log/nginx/freedmarcroft.com.access.log we_log;
    error_log /var/log/nginx/freedmarcroft.com.error.log;

    root /var/www/freedmarcroft.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /var/www/freedmarcroft.com/nginx/*freedmarcroft.com-sockfile.conf;
    include /etc/nginx/common/freedmarcroft.com-compression.conf;
    include /var/www/freedmarcroft.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/freedmarcroft.com/nginx/freedmarcroft.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/freedmarcroft.com/nginx/*-main-freedmarcroft.com-context.conf;
    include /var/www/freedmarcroft.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/freedmarcroft.com-sitemap.conf;
    include /etc/nginx/common/freedmarcroft.com-static.conf;
    include /etc/nginx/common/freedmarcroft.com-wpcommon.conf;
    include /etc/nginx/common/freedmarcroft.com-php.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
