##GridPane_Prometheus_vHost_Begin##
#HTTPS-ROOT#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/pressurewashnc.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name pressurewashnc.com;

    #include /var/www/pressurewashnc.com/nginx/pressurewashnc.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /var/www/pressurewashnc.com/nginx/*-return-https-context.conf;

    return 301 https://pressurewashnc.com$request_uri;
}

server {
    listen 80;
    listen [::]:80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.pressurewashnc.com;

    ssl_certificate /etc/letsencrypt/live/pressurewashnc.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/pressurewashnc.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/pressurewashnc.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/pressurewashnc.com/chain.pem;

    #include /var/www/pressurewashnc.com/nginx/pressurewashnc.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /var/www/pressurewashnc.com/nginx/*-return-root-context.conf;

    return 301 https://pressurewashnc.com$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name pressurewashnc.com;

    ssl_certificate /etc/letsencrypt/live/pressurewashnc.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/pressurewashnc.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/pressurewashnc.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/pressurewashnc.com/chain.pem;

    access_log /var/log/nginx/pressurewashnc.com.access.log we_log;
    error_log /var/log/nginx/pressurewashnc.com.error.log;

    root /var/www/pressurewashnc.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /var/www/pressurewashnc.com/nginx/*pressurewashnc.com-sockfile.conf;
    include /etc/nginx/common/pressurewashnc.com-compression.conf;
    include /var/www/pressurewashnc.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/pressurewashnc.com/nginx/pressurewashnc.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/pressurewashnc.com/nginx/*-main-pressurewashnc.com-context.conf;
    include /var/www/pressurewashnc.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/pressurewashnc.com-sitemap.conf;
    include /etc/nginx/common/pressurewashnc.com-static.conf;
    include /etc/nginx/common/pressurewashnc.com-wpcommon.conf;
    include /etc/nginx/common/pressurewashnc.com-php.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
