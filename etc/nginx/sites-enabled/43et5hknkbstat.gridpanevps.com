##GridPane_Prometheus_vHost_Begin##
#HTTPS-STATS#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

server {
    listen 80;
    listen [::]:80;

    server_name 43et5hknkbstat.gridpanevps.com www.43et5hknkbstat.gridpanevps.com;

    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name 43et5hknkbstat.gridpanevps.com www.43et5hknkbstat.gridpanevps.com;

    root /var/www/43et5hknkbstat.gridpanevps.com/htdocs;

    ssl_certificate /etc/letsencrypt/live/43et5hknkbstat.gridpanevps.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/43et5hknkbstat.gridpanevps.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/43et5hknkbstat.gridpanevps.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/43et5hknkbstat.gridpanevps.com/chain.pem;

    access_log /var/log/nginx/43et5hknkbstat.gridpanevps.com.access.log we_log;
    error_log /var/log/nginx/43et5hknkbstat.gridpanevps.com.error.log;

    index index.php index.html index.htm;

    location ~ /\.well-known {
        allow all;
    }

    location / {
        proxy_pass  http://127.0.0.1:2812;
    }
}
##GridPane_Prometheus_vHost_End##
