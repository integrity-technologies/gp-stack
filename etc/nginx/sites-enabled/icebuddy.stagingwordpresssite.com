##GridPane_Prometheus_vHost_Begin##
#HTTPS#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/icebuddy.stagingwordpresssite.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name icebuddy.stagingwordpresssite.com www.icebuddy.stagingwordpresssite.com;

    #include /var/www/icebuddy.stagingwordpresssite.com/nginx/icebuddy.stagingwordpresssite.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /var/www/icebuddy.stagingwordpresssite.com/nginx/*-return-https-context.conf;

    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name icebuddy.stagingwordpresssite.com www.icebuddy.stagingwordpresssite.com;

    ssl_certificate /etc/letsencrypt/live/icebuddy.stagingwordpresssite.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/icebuddy.stagingwordpresssite.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/icebuddy.stagingwordpresssite.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/icebuddy.stagingwordpresssite.com/chain.pem;

    access_log /var/log/nginx/icebuddy.stagingwordpresssite.com.access.log we_log;
    error_log /var/log/nginx/icebuddy.stagingwordpresssite.com.error.log;

    root /var/www/icebuddy.stagingwordpresssite.com/htdocs;

    index index.php index.html index.htm;

    include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /var/www/icebuddy.stagingwordpresssite.com/nginx/*icebuddy.stagingwordpresssite.com-sockfile.conf;
    include /etc/nginx/common/icebuddy.stagingwordpresssite.com-compression.conf;
    include /var/www/icebuddy.stagingwordpresssite.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/icebuddy.stagingwordpresssite.com/nginx/icebuddy.stagingwordpresssite.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/icebuddy.stagingwordpresssite.com/nginx/*-main-icebuddy.stagingwordpresssite.com-context.conf;
    include /var/www/icebuddy.stagingwordpresssite.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/icebuddy.stagingwordpresssite.com-sitemap.conf;
    include /etc/nginx/common/icebuddy.stagingwordpresssite.com-static.conf;
    include /etc/nginx/common/icebuddy.stagingwordpresssite.com-wpcommon.conf;
    include /etc/nginx/common/icebuddy.stagingwordpresssite.com-php.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
