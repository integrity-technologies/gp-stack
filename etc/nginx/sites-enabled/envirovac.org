##GridPane_Prometheus_vHost_Begin##
#HTTPS-WWW#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/envirovac.org/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name www.envirovac.org;

    #include /var/www/envirovac.org/nginx/envirovac.org-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /var/www/envirovac.org/nginx/*-return-https-context.conf;

    return 301 https://www.envirovac.org$request_uri;
}

server {
    listen 80;
    listen [::]:80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name envirovac.org;

    ssl_certificate /etc/letsencrypt/live/envirovac.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/envirovac.org/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/envirovac.org.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/envirovac.org/chain.pem;

    #include /var/www/envirovac.org/nginx/envirovac.org-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /var/www/envirovac.org/nginx/*-return-www-context.conf;

    return 301 https://www.envirovac.org$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.envirovac.org;

    ssl_certificate /etc/letsencrypt/live/envirovac.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/envirovac.org/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/envirovac.org.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/envirovac.org/chain.pem;

    access_log /var/log/nginx/envirovac.org.access.log we_log;
    error_log /var/log/nginx/envirovac.org.error.log;

    root /var/www/envirovac.org/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /var/www/envirovac.org/nginx/*envirovac.org-sockfile.conf;
    include /etc/nginx/common/envirovac.org-compression.conf;
    include /var/www/envirovac.org/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/envirovac.org/nginx/envirovac.org-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/envirovac.org/nginx/*-main-envirovac.org-context.conf;
    include /var/www/envirovac.org/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/envirovac.org-sitemap.conf;
    include /etc/nginx/common/envirovac.org-static.conf;
    include /etc/nginx/common/envirovac.org-wpcommon.conf;
    include /etc/nginx/common/envirovac.org-php.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
