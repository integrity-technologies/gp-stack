##GridPane_Prometheus_vHost_Begin##
#HTTP-ADD-ON-301#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

server {
    listen 80;
    listen [::]:80;

    server_name maginnis-orthodontics.stagingwordpresssite.com www.maginnis-orthodontics.stagingwordpresssite.com;

    include /etc/nginx/common/headers-http.conf;

    return 301 http://graf-orthodontics.stagingwordpresssite.com$request_uri;
}
##GridPane_Prometheus_vHost_End##
