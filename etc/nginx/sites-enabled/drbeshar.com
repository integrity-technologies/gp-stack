##GridPane_Prometheus_vHost_Begin##
#HTTPS-WWW#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/drbeshar.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name www.drbeshar.com;

    #include /var/www/drbeshar.com/nginx/drbeshar.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /var/www/drbeshar.com/nginx/*-return-https-context.conf;

    return 301 https://www.drbeshar.com$request_uri;
}

server {
    listen 80;
    listen [::]:80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name drbeshar.com;

    ssl_certificate /etc/letsencrypt/live/drbeshar.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/drbeshar.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/drbeshar.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/drbeshar.com/chain.pem;

    #include /var/www/drbeshar.com/nginx/drbeshar.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /var/www/drbeshar.com/nginx/*-return-www-context.conf;

    return 301 https://www.drbeshar.com$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.drbeshar.com;

    ssl_certificate /etc/letsencrypt/live/drbeshar.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/drbeshar.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/drbeshar.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/drbeshar.com/chain.pem;

    access_log /var/log/nginx/drbeshar.com.access.log we_log;
    error_log /var/log/nginx/drbeshar.com.error.log;

    root /var/www/drbeshar.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /var/www/drbeshar.com/nginx/*drbeshar.com-sockfile.conf;
    include /etc/nginx/common/drbeshar.com-compression.conf;
    include /var/www/drbeshar.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/drbeshar.com/nginx/drbeshar.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/drbeshar.com/nginx/*-main-drbeshar.com-context.conf;
    include /var/www/drbeshar.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/drbeshar.com-sitemap.conf;
    include /etc/nginx/common/drbeshar.com-static.conf;
    include /etc/nginx/common/drbeshar.com-wpcommon.conf;
    include /etc/nginx/common/drbeshar.com-php.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
