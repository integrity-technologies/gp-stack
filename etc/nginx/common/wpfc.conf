# #
# GridPane vHost FastCGI caching
# Version 1.2.0
# #
include /etc/nginx/common/fcgi-cache-var.conf;

set $skip_cache 0;
set $skip_reason "";
set $cache_ttl 1;

if ($request_method = POST) {
    set $skip_cache 1;
    set $skip_reason "${skip_reason}-POST";
}

if ($query_string != "") {
    set $skip_cache 1;
    set $skip_reason "${skip_reason}-query_string";
}

if ($request_uri ~* "(/wp-admin/|/xmlrpc.php|wp-.*.php|index.php|/feed/|.*sitemap.*\.xml/store.*|/cart.*|/my-account.*|/checkout.*|/addons.*)") {
    set $skip_cache 1;
    set $skip_reason "${skip_reason}-request_uri";
}

if ($http_cookie ~* "comment_author|wordpress_[a-f0-9]+|wp-postpass|wordpress_no_cache|wordpress_logged_in|[a-z0-9]+_items_in_cart|woocommerce_cart_hash|woocommerce_items_in_cart|wp_woocommerce_session_|edd_items_in_cart") {
    set $skip_cache 1;
    set $skip_reason "${skip_reason}-http_cookie";
}

include /etc/nginx/extra.d/*skip-fcgi-cache-context.conf;
include /etc/nginx/extra.d/*-skip-cache-context.conf;

location ~ /purge(/.*) {
    #fastcgi_cache_purge FASTCGICACHE "$scheme$request_method$geoip2_data_country_code$host$1";
    fastcgi_cache_purge FASTCGICACHE "$scheme$request_method$host$1";
}

location ~ /purgeall {
    set_if_empty $sockfile php;
    fastcgi_pass $sockfile;
    #fastcgi_param HTTPS on;
    fastcgi_cache FASTCGICACHE;
    fastcgi_cache_purge PURGE purge_all from 127.0.0.1;
}

location / {
    try_files $uri $uri/ /index.php?$args;
    include /etc/nginx/extra.d/*root-context.conf;
    include /etc/nginx/extra.d/*-root-context.conf;
}

location ~ \.php$ {
    try_files $uri =404;
    fastcgi_cache_bypass $skip_cache;
    fastcgi_no_cache $skip_cache;
    fastcgi_cache FASTCGICACHE;
    more_set_headers 'X-Grid-Cache-TTL $cache_ttl';
    more_set_headers 'X-Grid-Cache-Skip $skip_reason';
    more_set_headers 'X-Grid-Cache $upstream_cache_status';
    set $http_x_accel_expires $cache_ttl;
    fastcgi_split_path_info ^(.+\.php)(.*)$;
    include fastcgi_params;
    include /etc/nginx/extra.d/*php-context.conf;
    include /etc/nginx/extra.d/*-php-context.conf;
    #fastcgi_param HTTPS on;
    set_if_empty $sockfile php;
    fastcgi_pass $sockfile;
    http2_push_preload on;
}
