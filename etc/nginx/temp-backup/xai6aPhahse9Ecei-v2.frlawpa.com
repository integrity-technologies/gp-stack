##GridPane_Prometheus_vHost_Begin##
#HTTPS-ROOT#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/v2.frlawpa.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name v2.frlawpa.com;

    #include /var/www/v2.frlawpa.com/nginx/v2.frlawpa.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /var/www/v2.frlawpa.com/nginx/*-return-https-context.conf;

    return 301 https://v2.frlawpa.com$request_uri;
}

server {
    listen 80;
    listen [::]:80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.v2.frlawpa.com;

    ssl_certificate /etc/letsencrypt/live/v2.frlawpa.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/v2.frlawpa.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/v2.frlawpa.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/v2.frlawpa.com/chain.pem;

    #include /var/www/v2.frlawpa.com/nginx/v2.frlawpa.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /var/www/v2.frlawpa.com/nginx/*-return-root-context.conf;

    return 301 https://v2.frlawpa.com$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name v2.frlawpa.com;

    ssl_certificate /etc/letsencrypt/live/v2.frlawpa.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/v2.frlawpa.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/v2.frlawpa.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/v2.frlawpa.com/chain.pem;

    access_log /var/log/nginx/v2.frlawpa.com.access.log we_log;
    error_log /var/log/nginx/v2.frlawpa.com.error.log;

    root /var/www/v2.frlawpa.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /etc/nginx/common/v2.frlawpa.com-compression.conf;
    include /var/www/v2.frlawpa.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/v2.frlawpa.com/nginx/v2.frlawpa.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/v2.frlawpa.com/nginx/*-main-v2.frlawpa.com-context.conf;
    include /var/www/v2.frlawpa.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/main-context.conf;
    include /etc/nginx/common/v2.frlawpa.com-sitemap.conf;
    include /etc/nginx/common/v2.frlawpa.com-static.conf;
    include /etc/nginx/common/v2.frlawpa.com-wpcommon.conf;
    include /etc/nginx/common/v2.frlawpa.com-php.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
