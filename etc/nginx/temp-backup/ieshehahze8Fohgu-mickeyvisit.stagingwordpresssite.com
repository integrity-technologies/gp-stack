##GridPane_Prometheus_vHost_Begin##
#HTTPS-ROOT#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/mickeyvisit.stagingwordpresssite.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name mickeyvisit.stagingwordpresssite.com;

    #include /var/www/mickeyvisit.stagingwordpresssite.com/nginx/mickeyvisit.stagingwordpresssite.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /var/www/mickeyvisit.stagingwordpresssite.com/nginx/*-return-https-context.conf;

    return 301 https://mickeyvisit.stagingwordpresssite.com$request_uri;
}

server {
    listen 80;
    listen [::]:80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.mickeyvisit.stagingwordpresssite.com;

    ssl_certificate /etc/letsencrypt/live/mickeyvisit.stagingwordpresssite.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/mickeyvisit.stagingwordpresssite.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/mickeyvisit.stagingwordpresssite.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/mickeyvisit.stagingwordpresssite.com/chain.pem;

    #include /var/www/mickeyvisit.stagingwordpresssite.com/nginx/mickeyvisit.stagingwordpresssite.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /var/www/mickeyvisit.stagingwordpresssite.com/nginx/*-return-root-context.conf;

    return 301 https://mickeyvisit.stagingwordpresssite.com$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name mickeyvisit.stagingwordpresssite.com;

    ssl_certificate /etc/letsencrypt/live/mickeyvisit.stagingwordpresssite.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/mickeyvisit.stagingwordpresssite.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/mickeyvisit.stagingwordpresssite.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/mickeyvisit.stagingwordpresssite.com/chain.pem;

    access_log /var/log/nginx/mickeyvisit.stagingwordpresssite.com.access.log we_log;
    error_log /var/log/nginx/mickeyvisit.stagingwordpresssite.com.error.log;

    root /var/www/mickeyvisit.stagingwordpresssite.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /etc/nginx/common/mickeyvisit.stagingwordpresssite.com-compression.conf;
    include /var/www/mickeyvisit.stagingwordpresssite.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/mickeyvisit.stagingwordpresssite.com/nginx/mickeyvisit.stagingwordpresssite.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/mickeyvisit.stagingwordpresssite.com/nginx/*-main-mickeyvisit.stagingwordpresssite.com-context.conf;
    include /var/www/mickeyvisit.stagingwordpresssite.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/main-context.conf;
    include /etc/nginx/common/mickeyvisit.stagingwordpresssite.com-sitemap.conf;
    include /etc/nginx/common/mickeyvisit.stagingwordpresssite.com-static.conf;
    include /etc/nginx/common/mickeyvisit.stagingwordpresssite.com-wpcommon.conf;
    include /etc/nginx/common/mickeyvisit.stagingwordpresssite.com-php.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
