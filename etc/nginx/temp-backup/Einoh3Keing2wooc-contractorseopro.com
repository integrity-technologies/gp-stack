##GridPane_Prometheus_vHost_Begin##
#HTTPS-WWW#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/contractorseopro.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name www.contractorseopro.com;

    #include /var/www/contractorseopro.com/nginx/contractorseopro.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /var/www/contractorseopro.com/nginx/*-return-https-context.conf;

    return 301 https://www.contractorseopro.com$request_uri;
}

server {
    listen 80;
    listen [::]:80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name contractorseopro.com;

    ssl_certificate /etc/letsencrypt/live/contractorseopro.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/contractorseopro.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/contractorseopro.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/contractorseopro.com/chain.pem;

    #include /var/www/contractorseopro.com/nginx/contractorseopro.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /var/www/contractorseopro.com/nginx/*-return-www-context.conf;

    return 301 https://www.contractorseopro.com$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.contractorseopro.com;

    ssl_certificate /etc/letsencrypt/live/contractorseopro.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/contractorseopro.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/contractorseopro.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/contractorseopro.com/chain.pem;

    access_log /var/log/nginx/contractorseopro.com.access.log we_log;
    error_log /var/log/nginx/contractorseopro.com.error.log;

    root /var/www/contractorseopro.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /etc/nginx/common/contractorseopro.com-compression.conf;
    include /var/www/contractorseopro.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/contractorseopro.com/nginx/contractorseopro.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/contractorseopro.com/nginx/*-main-contractorseopro.com-context.conf;
    include /var/www/contractorseopro.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/main-context.conf;
    include /etc/nginx/common/contractorseopro.com-sitemap.conf;
    include /etc/nginx/common/contractorseopro.com-static.conf;
    include /etc/nginx/common/contractorseopro.com-wpcommon.conf;
    include /etc/nginx/common/contractorseopro.com-php.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
