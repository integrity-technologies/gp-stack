##GridPane_Prometheus_vHost_Begin##
#HTTPS-ROOT#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/cetpainting.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name cetpainting.com;

    #include /var/www/cetpainting.com/nginx/cetpainting.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /var/www/cetpainting.com/nginx/*-return-https-context.conf;

    return 301 https://cetpainting.com$request_uri;
}

server {
    listen 80;
    listen [::]:80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.cetpainting.com;

    ssl_certificate /etc/letsencrypt/live/cetpainting.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/cetpainting.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/cetpainting.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/cetpainting.com/chain.pem;

    #include /var/www/cetpainting.com/nginx/cetpainting.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /var/www/cetpainting.com/nginx/*-return-root-context.conf;

    return 301 https://cetpainting.com$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name cetpainting.com;

    ssl_certificate /etc/letsencrypt/live/cetpainting.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/cetpainting.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/cetpainting.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/cetpainting.com/chain.pem;

    access_log /var/log/nginx/cetpainting.com.access.log we_log;
    error_log /var/log/nginx/cetpainting.com.error.log;

    root /var/www/cetpainting.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /var/www/cetpainting.com/nginx/*cetpainting.com-sockfile.conf;
    include /etc/nginx/common/cetpainting.com-compression.conf;
    include /var/www/cetpainting.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/cetpainting.com/nginx/cetpainting.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/cetpainting.com/nginx/*-main-cetpainting.com-context.conf;
    include /var/www/cetpainting.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/cetpainting.com-sitemap.conf;
    include /etc/nginx/common/cetpainting.com-static.conf;
    include /etc/nginx/common/cetpainting.com-wpcommon.conf;
    include /etc/nginx/common/cetpainting.com-php.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
