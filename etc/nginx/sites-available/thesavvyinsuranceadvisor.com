##GridPane_Prometheus_vHost_Begin##
#HTTPS-WWW#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/thesavvyinsuranceadvisor.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name www.thesavvyinsuranceadvisor.com;

    #include /var/www/thesavvyinsuranceadvisor.com/nginx/thesavvyinsuranceadvisor.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /var/www/thesavvyinsuranceadvisor.com/nginx/*-return-https-context.conf;

    return 301 https://www.thesavvyinsuranceadvisor.com$request_uri;
}

server {
    listen 80;
    listen [::]:80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name thesavvyinsuranceadvisor.com;

    ssl_certificate /etc/letsencrypt/live/thesavvyinsuranceadvisor.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/thesavvyinsuranceadvisor.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/thesavvyinsuranceadvisor.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/thesavvyinsuranceadvisor.com/chain.pem;

    #include /var/www/thesavvyinsuranceadvisor.com/nginx/thesavvyinsuranceadvisor.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /var/www/thesavvyinsuranceadvisor.com/nginx/*-return-www-context.conf;

    return 301 https://www.thesavvyinsuranceadvisor.com$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.thesavvyinsuranceadvisor.com;

    ssl_certificate /etc/letsencrypt/live/thesavvyinsuranceadvisor.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/thesavvyinsuranceadvisor.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/thesavvyinsuranceadvisor.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/thesavvyinsuranceadvisor.com/chain.pem;

    access_log /var/log/nginx/thesavvyinsuranceadvisor.com.access.log we_log;
    error_log /var/log/nginx/thesavvyinsuranceadvisor.com.error.log;

    root /var/www/thesavvyinsuranceadvisor.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /var/www/thesavvyinsuranceadvisor.com/nginx/*thesavvyinsuranceadvisor.com-sockfile.conf;
    include /etc/nginx/common/thesavvyinsuranceadvisor.com-compression.conf;
    include /var/www/thesavvyinsuranceadvisor.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/thesavvyinsuranceadvisor.com/nginx/thesavvyinsuranceadvisor.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/thesavvyinsuranceadvisor.com/nginx/*-main-thesavvyinsuranceadvisor.com-context.conf;
    include /var/www/thesavvyinsuranceadvisor.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/thesavvyinsuranceadvisor.com-sitemap.conf;
    include /etc/nginx/common/thesavvyinsuranceadvisor.com-static.conf;
    include /etc/nginx/common/thesavvyinsuranceadvisor.com-wpcommon.conf;
    include /etc/nginx/common/thesavvyinsuranceadvisor.com-wp-redis.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
