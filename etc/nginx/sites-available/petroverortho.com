##GridPane_Prometheus_vHost_Begin##
#HTTP#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/petroverortho.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name petroverortho.com www.petroverortho.com;

    access_log /var/log/nginx/petroverortho.com.access.log we_log;
    error_log /var/log/nginx/petroverortho.com.error.log;

    root /var/www/petroverortho.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /var/www/petroverortho.com/nginx/*petroverortho.com-sockfile.conf;
    include /etc/nginx/common/petroverortho.com-compression.conf;
    include /var/www/petroverortho.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/petroverortho.com/nginx/petroverortho.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/petroverortho.com/nginx/*-main-petroverortho.com-context.conf;
    include /var/www/petroverortho.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/petroverortho.com-sitemap.conf;
    include /etc/nginx/common/petroverortho.com-static.conf;
    include /etc/nginx/common/petroverortho.com-wpcommon.conf;
    include /etc/nginx/common/petroverortho.com-wp-redis.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
