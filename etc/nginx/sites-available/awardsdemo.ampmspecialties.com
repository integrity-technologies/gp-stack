##GridPane_Prometheus_vHost_Begin##
#HTTP#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/awardsdemo.ampmspecialties.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name awardsdemo.ampmspecialties.com www.awardsdemo.ampmspecialties.com;

    access_log /var/log/nginx/awardsdemo.ampmspecialties.com.access.log we_log;
    error_log /var/log/nginx/awardsdemo.ampmspecialties.com.error.log;

    root /var/www/awardsdemo.ampmspecialties.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    include /etc/nginx/common/awardsdemo.ampmspecialties.com-6g.conf;
    include /var/www/awardsdemo.ampmspecialties.com/nginx/*awardsdemo.ampmspecialties.com-sockfile.conf;
    include /etc/nginx/common/awardsdemo.ampmspecialties.com-compression.conf;
    include /var/www/awardsdemo.ampmspecialties.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/awardsdemo.ampmspecialties.com/nginx/awardsdemo.ampmspecialties.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/awardsdemo.ampmspecialties.com/nginx/*-main-awardsdemo.ampmspecialties.com-context.conf;
    include /var/www/awardsdemo.ampmspecialties.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/awardsdemo.ampmspecialties.com-sitemap.conf;
    include /etc/nginx/common/awardsdemo.ampmspecialties.com-static.conf;
    include /etc/nginx/common/awardsdemo.ampmspecialties.com-wpcommon.conf;
    include /etc/nginx/common/awardsdemo.ampmspecialties.com-wp-redis.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
