##GridPane_Prometheus_vHost_Begin##
#HTTPS-PHPMA#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

server {
    listen 80;
    listen [::]:80;

    server_name 43et5hknkb.gridpanevps.com;

    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl http2 reuseport;
    listen [::]:443 ssl http2;

    server_name 43et5hknkb.gridpanevps.com;

    root /var/www/43et5hknkb.gridpanevps.com/htdocs;

    ssl_certificate /etc/letsencrypt/live/43et5hknkb.gridpanevps.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/43et5hknkb.gridpanevps.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/43et5hknkb.gridpanevps.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/43et5hknkb.gridpanevps.com/chain.pem;

    access_log /var/log/nginx/43et5hknkb.gridpanevps.com.access.log we_log;
    error_log /var/log/nginx/43et5hknkb.gridpanevps.com.error.log;

    index index.php index.html index.htm;

    client_max_body_size 0;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ /\.well-known {
        allow all;
    }

    location ~ \.php$ {
        try_files /$uri =404;
        include fastcgi_params;
        fastcgi_pass php;
    }

    include common/headers-http.conf;
    include common/headers-https.conf;
    include common/headers-html.conf;
}
##GridPane_Prometheus_vHost_End##
