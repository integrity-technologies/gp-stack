##GridPane_Prometheus_vHost_Begin##
#HTTP#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/maximregen.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name maximregen.com www.maximregen.com;

    access_log /var/log/nginx/maximregen.com.access.log we_log;
    error_log /var/log/nginx/maximregen.com.error.log;

    root /var/www/maximregen.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /var/www/maximregen.com/nginx/*maximregen.com-sockfile.conf;
    include /etc/nginx/common/maximregen.com-compression.conf;
    include /var/www/maximregen.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/maximregen.com/nginx/maximregen.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/maximregen.com/nginx/*-main-maximregen.com-context.conf;
    include /var/www/maximregen.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/maximregen.com-sitemap.conf;
    include /etc/nginx/common/maximregen.com-static.conf;
    include /etc/nginx/common/maximregen.com-wpcommon.conf;
    include /etc/nginx/common/maximregen.com-wp-redis.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
