##GridPane_Prometheus_vHost_Begin##
#HTTP#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/haloheatingandcooling.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name haloheatingandcooling.com www.haloheatingandcooling.com;

    access_log /var/log/nginx/haloheatingandcooling.com.access.log we_log;
    error_log /var/log/nginx/haloheatingandcooling.com.error.log;

    root /var/www/haloheatingandcooling.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /var/www/haloheatingandcooling.com/nginx/*haloheatingandcooling.com-sockfile.conf;
    include /etc/nginx/common/haloheatingandcooling.com-compression.conf;
    include /var/www/haloheatingandcooling.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/haloheatingandcooling.com/nginx/haloheatingandcooling.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/haloheatingandcooling.com/nginx/*-main-haloheatingandcooling.com-context.conf;
    include /var/www/haloheatingandcooling.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/haloheatingandcooling.com-sitemap.conf;
    include /etc/nginx/common/haloheatingandcooling.com-static.conf;
    include /etc/nginx/common/haloheatingandcooling.com-wpcommon.conf;
    include /etc/nginx/common/haloheatingandcooling.com-wp-redis.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
