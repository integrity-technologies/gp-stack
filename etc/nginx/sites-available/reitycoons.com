##GridPane_Prometheus_vHost_Begin##
#HTTPS-ROOT#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/reitycoons.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name reitycoons.com;

    #include /var/www/reitycoons.com/nginx/reitycoons.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /var/www/reitycoons.com/nginx/*-return-https-context.conf;

    return 301 https://reitycoons.com$request_uri;
}

server {
    listen 80;
    listen [::]:80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.reitycoons.com;

    ssl_certificate /etc/letsencrypt/live/reitycoons.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/reitycoons.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/reitycoons.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/reitycoons.com/chain.pem;

    #include /var/www/reitycoons.com/nginx/reitycoons.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /var/www/reitycoons.com/nginx/*-return-root-context.conf;

    return 301 https://reitycoons.com$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name reitycoons.com;

    ssl_certificate /etc/letsencrypt/live/reitycoons.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/reitycoons.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/reitycoons.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/reitycoons.com/chain.pem;

    access_log /var/log/nginx/reitycoons.com.access.log we_log;
    error_log /var/log/nginx/reitycoons.com.error.log;

    root /var/www/reitycoons.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /var/www/reitycoons.com/nginx/*reitycoons.com-sockfile.conf;
    include /etc/nginx/common/reitycoons.com-compression.conf;
    include /var/www/reitycoons.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/reitycoons.com/nginx/reitycoons.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/reitycoons.com/nginx/*-main-reitycoons.com-context.conf;
    include /var/www/reitycoons.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/reitycoons.com-sitemap.conf;
    include /etc/nginx/common/reitycoons.com-static.conf;
    include /etc/nginx/common/reitycoons.com-wpcommon.conf;
    include /etc/nginx/common/reitycoons.com-php.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
