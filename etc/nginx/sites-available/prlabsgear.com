##GridPane_Prometheus_vHost_Begin##
#HTTPS-WWW#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/prlabsgear.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name www.prlabsgear.com;

    #include /var/www/prlabsgear.com/nginx/prlabsgear.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /var/www/prlabsgear.com/nginx/*-return-https-context.conf;

    return 301 https://www.prlabsgear.com$request_uri;
}

server {
    listen 80;
    listen [::]:80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name prlabsgear.com;

    ssl_certificate /etc/letsencrypt/live/prlabsgear.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/prlabsgear.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/prlabsgear.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/prlabsgear.com/chain.pem;

    #include /var/www/prlabsgear.com/nginx/prlabsgear.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /var/www/prlabsgear.com/nginx/*-return-www-context.conf;

    return 301 https://www.prlabsgear.com$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.prlabsgear.com;

    ssl_certificate /etc/letsencrypt/live/prlabsgear.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/prlabsgear.com/privkey.pem;
    ssl_stapling_file /etc/nginx/ocsp/prlabsgear.com.resp;
    ssl_trusted_certificate /etc/letsencrypt/live/prlabsgear.com/chain.pem;

    access_log /var/log/nginx/prlabsgear.com.access.log we_log;
    error_log /var/log/nginx/prlabsgear.com.error.log;

    root /var/www/prlabsgear.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /var/www/prlabsgear.com/nginx/*prlabsgear.com-sockfile.conf;
    include /etc/nginx/common/prlabsgear.com-compression.conf;
    include /var/www/prlabsgear.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/prlabsgear.com/nginx/prlabsgear.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-https.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/prlabsgear.com/nginx/*-main-prlabsgear.com-context.conf;
    include /var/www/prlabsgear.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/prlabsgear.com-sitemap.conf;
    include /etc/nginx/common/prlabsgear.com-static.conf;
    include /etc/nginx/common/prlabsgear.com-wpcommon.conf;
    include /etc/nginx/common/prlabsgear.com-php.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
