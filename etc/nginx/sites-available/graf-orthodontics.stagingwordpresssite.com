##GridPane_Prometheus_vHost_Begin##
#HTTP#
## Do not edit directly.
## All changes will be lost.
## Please use a location specific
## include to customise your config

include /var/www/graf-orthodontics.stagingwordpresssite.com/nginx/*-http-context.conf;
server {
    listen 80;
    listen [::]:80;

    server_name graf-orthodontics.stagingwordpresssite.com www.graf-orthodontics.stagingwordpresssite.com;

    access_log /var/log/nginx/graf-orthodontics.stagingwordpresssite.com.access.log we_log;
    error_log /var/log/nginx/graf-orthodontics.stagingwordpresssite.com.error.log;

    root /var/www/graf-orthodontics.stagingwordpresssite.com/htdocs;

    index index.php index.html index.htm;

    #include /etc/nginx/common/acl.conf;
    #include /etc/nginx/common/7g.conf;
    #include /etc/nginx/common/6g.conf;
    include /var/www/graf-orthodontics.stagingwordpresssite.com/nginx/*graf-orthodontics.stagingwordpresssite.com-sockfile.conf;
    include /etc/nginx/common/graf-orthodontics.stagingwordpresssite.com-compression.conf;
    include /var/www/graf-orthodontics.stagingwordpresssite.com/nginx/*-realip-context.conf;
    include /etc/nginx/common/gridpane-realip.conf;
    #include /var/www/graf-orthodontics.stagingwordpresssite.com/nginx/graf-orthodontics.stagingwordpresssite.com-headers-csp.conf;
    include /etc/nginx/common/headers-http.conf;
    include /etc/nginx/common/headers-html.conf;
    include /var/www/graf-orthodontics.stagingwordpresssite.com/nginx/*-main-graf-orthodontics.stagingwordpresssite.com-context.conf;
    include /var/www/graf-orthodontics.stagingwordpresssite.com/nginx/*-main-context.conf;
    include /etc/nginx/extra.d/*main-context.conf;
    include /etc/nginx/common/graf-orthodontics.stagingwordpresssite.com-sitemap.conf;
    include /etc/nginx/common/graf-orthodontics.stagingwordpresssite.com-static.conf;
    include /etc/nginx/common/graf-orthodontics.stagingwordpresssite.com-wpcommon.conf;
    include /etc/nginx/common/graf-orthodontics.stagingwordpresssite.com-php.conf;
    #include /etc/nginx/common/wpsubdir.conf;
    include /etc/nginx/common/security.conf;
}
##GridPane_Prometheus_vHost_End##
